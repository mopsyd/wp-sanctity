<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models;

/**
 * Abstract Model
 *
 * The base level Sanctity model class.
 *
 * This model logic provides access to the default Wordpress api without
 * excluding the possibility of using external data endpoints such as
 * redis, memcached, or a 3rd party database,table, api, or data structure.
 *
 * All functionality that interoperates with the Wordpress system uses
 * the native Wordpress Api as outlined in their best practices.
 *
 * Extending this abstract:
 *
 * - The class constant MODEL_SETTINGS_KEY MUST be provided
 *     to establish a domain for your model.
 *
 * - Users who have the capacity to edit theme options are automatically
 *     authenticated to work with your options. If a different access
 *     credential is required, override the class constant MODEL_USER_AUTH_LEVEL
 *     with the correct access key to work with your settings.
 *
 * - Override `declareOptions` and return an array of the options you wish to be
 *     set for your model, and they will automatically be registered when it is
 *     appropriate to work with them.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractModel
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\models\SanctityModelInterface
{

    /**
     * By default Models use the core config.
     */
    const CONFIG_FILE = 'core.json';

    /**
     * If defined, this indicates the postback prefix for the model,
     * so it can be identified by the post router.
     *
     * This constant should be overridden if a model intends to handle forms.
     * If this value is false, the underlying model will not let it register
     * a form endpoint through its normal logic.
     */
    const MODEL_POSTBACK_KEY = false;

    /**
     * The prefix used to create a nonce field key.
     */
    const NONCE_PREFIX = '_token';

    /**
     * This constant should be overridden to designate the scope of the
     * extending model, and the realm of data that it handles.
     *
     * All base level internals that fetch data for child classes will be
     * based on this constant, which enforces that their scope is constrained
     * to a specific realm of responsibility programatically.
     */
    const MODEL_SETTINGS_KEY = false;

    /**
     * This designates the permission level required to access settings pages
     * for the model. This may be overridden in any given model reflecting
     * a settings page or group to escalate or de-escalate permissions as
     * needed.
     *
     * Models are expected to use one primary permission level to reflect
     * their page display. Individual subsections of the page may have
     * alternate permissions if needed, but a user must have the
     * permission designated by this constant to view the page at all.
     */
    const MODEL_USER_AUTH_LEVEL = 'edit_theme_options';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Maintains a reference link to a consistent dedicated PDO instance
     * configured to operate seamlessly with the Wordpress api, and defer
     * to the native Wordpress database where appropriate.
     *
     * Eg: Operations that only affect this theme's tables use PDO primarily.
     * Operations that alter state of Wordpress tables use the Wordpress api
     * specifically, to minify any chance of an update breaking stuff.
     *
     * Sanctity tables are almost entirely segregated from the rest of the
     * Wordpress system, so updates shouldn't really break anything unless
     * Wordpress alters its public method api (which they typically don't).
     *
     * @var \mopsyd\sanctity\libs\pdo\Pdo
     */
    private static $database;

    /**
     * The scope that the current model is expected to constrain its behavior within.
     * This is set by the controller when a model is loaded to insure that it does not
     * fetch settings from an irrelevant section of the site.
     *
     * This is set on a per-object basis, and is not global or static,
     * as it may be appropriate for some controllers to fetch information
     * outside of their scope.
     *
     * @var string
     */
    private static $scope;
    private static $_settings_meta_keys = array(
        'dashboard',
        'settings',
        'layout',
        'style',
        'integration',
    );

    /**
     * The default Sanctity meta values are parsed separately from a
     * flat file to prevent corruption. These are always available regardless
     * of any child class logic. Extending classes must provide override logic
     * for these keys if they wish to alter their behavior, but this model class
     * does not tolerate data persistence fault points in its core operational
     * data set.
     * @var array
     */
    private static $_settings_meta_defaults;

    /**
     * Represents the ongoing set of structured model data that the
     * current class is operating against. This data can be set, but
     * cannot be erased or edited once set within the same runtime.
     *
     * This measure enforces that child classes do not commit incomplete
     * data to the public accessor.
     *
     * @var array
     */
    private $_settings_meta = array();

    /**
     * Represents an array of option keys known to the model.
     *
     * This is populated when the registration process occurs.
     * Get operations against the model will preflight the key
     * against this set, and bounce the attempt if the key is
     * not known.
     *
     * The model in turn defers to the adapter, providing a default
     * return value that is abstracted away from consideration by the
     * rest of the system, so everything else can just use a standard
     * get and set with the model and not worry about the default
     * values or registration process.
     *
     * @var array
     */
    private $options = array();

    public function __construct()
    {
        $this->_initializeDatabaseObject();
        $this->_loadSettingsMetaDefaults();
        $this->_loadSettingsMetaValues();
        parent::__construct();
    }

    /**
     * Registers options only if the current user has access rights to edit them.
     * @return void
     */
    public function register()
    {
        if ( !$this::MODEL_SETTINGS_KEY )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Instance of [%1$s] is misconfigured. '
                . 'Class constant [MODEL_SETTINGS_KEY] must '
                . 'be defined to declare model scope. Please '
                . 'contact the developer of [%1$s] and report '
                . 'the issue.', get_class( $this ) )
            );
        }
        $options = $this->declareOptions();
        if ( empty( $options->toArray() ) )
        {
            return;
        }
        foreach ( $options as
            $opt_group )
        {
            if ( is_object( $opt_group )
                && ( $opt_group instanceof \mopsyd\sanctity\interfaces\libs\container\ContainerInterface ) )
            {
                $opt_group = $opt_group->toArray();
            }
            $this::getAdapter()->register( 'option', $opt_group );
            if ( array_key_exists( 'options', $opt_group ) )
            {
                $this->_extractOptionKeys( $opt_group['options'] );
            }
        }
    }

    /**
     * Returns the settings scoped under the current model.
     *
     * All models have this method. Models dedicated to settings specifically
     * provide additional support methods for configuring their options.
     *
     * @return bool|array
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If a model extending this class does not set its scope constant,
     *     this exception will be thrown. This indicates that the class is
     *     misconfigured, and the exception should not be caught.
     */
    public function getSettings()
    {
        if ( !$this::MODEL_SETTINGS_KEY )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Instance of [%1$s] is misconfigured. '
                . 'Class constant [MODEL_SETTINGS_KEY] must '
                . 'be defined to declare model scope. Please '
                . 'contact the developer of [%1$s] and report '
                . 'the issue.', get_class( $this ) )
            );
        }
        return $this->getSettingsMeta( $this::MODEL_SETTINGS_KEY );
    }

    /**
     * Override this method to return frontend modules that should be loaded
     * dynamically by the frontend library to handle client side logic within
     * the bounds of the model scope.
     * @return array
     */
    public function getModules()
    {
        return array();
    }

    /**
     * Override this method to return frontend data for packaging frontend
     * script context that should be injected into frontend modules on page load.
     * The keys for this should correspond to the keys returned from `getModules`
     * from the same model scope.
     * @return array
     */
    public function getClientData()
    {
        return array();
    }

    /**
     * Returns model settings data by specific key on request.
     *
     * @param type $key
     * @return mixed If a setting exists by the given key, that will be returned.
     *     If no setting exists by the given key, it returns false.
     */
    public function get( $key )
    {
        $settings = $this->getSettings();
        if ( !$settings->has( $key ) )
        {
            return false;
        }
        return $settings->get( $key );
    }

    /**
     * Gets the existing value of a model option.
     *
     * This should not be confused with settings, which are only
     * configurable by the model internally, but may be externally
     * read via `getSettings`.
     *
     * @param type $key
     * @return type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If an option is requested that does not exist in
     *     the model's options set.
     */
    public function getOption( $key )
    {
        if ( $this->hasOption( $key ) )
        {
            // The default value is returned if there is no declaration.
            // This is abstracted away from public usage through the model api.
            return $this::getAdapter()->get( 'option',
                    $this->options[$key]['key'], $this->options[$key]['default'] );
        }
        throw new \mopsyd\sanctity\libs\exception\SanctityException(
        sprintf( 'Invalid option requested in [%1$s]. '
            . 'Option [%2$s] does not exist in this scope.', get_class( $this ),
            $key )
        );
    }

    /**
     * Returns a container of all options handled by the model
     * with their current values.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getOptions()
    {
        $opts = array();
        foreach ( $this->options as
            $key =>
            $option )
        {
            $opts[$key] = $this::getAdapter()->get( 'option', $option['key'],
                $option['default'] );
        }
        return $this::containerize( $opts );
    }

    /**
     * Sets an option.
     *
     * Options represent the externally configurable
     * parameters managed by the model.
     *
     * This should not be confused with settings, which are only
     * configurable by the model internally.
     *
     * @param string $key
     * @param mixed $value
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If an attempt is made to set a non-option.
     */
    public function setOption( $key, $value )
    {
        if ( !$this->has( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid request in [%1$s]. Provided key [%2$s] '
                . 'is not valid in this scope.', get_class( $this ), $key )
            );
        }
        $this::getAdapter()->set( 'option', $this->options[$key]['key'], $value );
    }

    /**
     * Checks if an option key exists.
     * This only checks options, not settings.
     *
     * This should not be confused with settings, which are only
     * configurable by the model internally.
     *
     * @param string $key
     * @return bool
     */
    public function hasOption( $key )
    {
        return array_key_exists( $key, $this->options );
    }

    public function getTemplate()
    {
        return $this::MODEL_SETTINGS_KEY;
    }

    /**
     * Sets the scope of the model objects.
     *
     * @param string $scope
     */
    public static function setScope( $scope )
    {
        if ( !is_null( self::$scope ) && !$scope === self::$scope )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Out of scope error detected in [%1$s]. '
                . 'Scope cannot be declared as [%2$s] because '
                . 'it is already set to [%3$s]', get_class( $this ), $scope,
                self::$scope )
            );
        }
        self::$scope = $scope;
    }

    /**
     * Gets the current scope defined for this model object.
     *
     * @return string
     */
    public static function getScope()
    {
        return self::$scope;
    }

    /**
     * Override this method to provide a set of options to register for any
     * given model. These settings will be automatically registered when the
     * correct page view and access rights are relevant.
     *
     * @return array
     */
    protected function declareOptions()
    {
        return array();
    }

    /**
     * Model metadata getter method.
     *
     * Returns false if a key is not found, otherwise returns the meta key.
     *
     * @param int|string $key
     * @return bool|mixed
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Occurs if an invalid key is provided.
     *     Keys must be a string or integer.
     */
    protected function getSettingsMeta( $key )
    {
        if ( !is_string( $key ) && !is_int( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$key] passed in [%2$s] at method [%3$s]. '
                . 'Expected [int|string], but received [%4$s].',
                get_class( $this ), __METHOD__, gettype( $key ) )
            );
        }
        if ( array_key_exists( $key, $this->_settings_meta ) )
        {
            return $this->_settings_meta[$key];
        }
        return false;
    }

    /**
     * Adds additional data to the meta set.
     *
     * @param int|string $key
     * @param mixed $value The value to bind to the meta key
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Occurs if an invalid key is provided, or if a key is provided
     *     that is already set. Model logic is expected to fully resolve
     *     its meta set prior to setting the key for external use in order
     *     to prevent incomplete data sets from propagating.
     */
    protected function setSettingsMeta( $key, $value )
    {
        if ( !is_string( $key ) && !is_int( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$key] passed in [%2$s] at method [%3$s]. '
                . 'Expected [int|string], but received [%4$s].',
                get_class( $this ), __METHOD__, gettype( $key ) )
            );
        }
        if ( array_key_exists( $key, $this->_settings_meta ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$key] passed in [%2$s] at method [%3$s]. '
                . 'Provided key [%4$s] is already defined.', get_class( $this ),
                __METHOD__, $key )
            );
        }
    }

    /**
     * Returns a persistent connection to the wordpress database object.
     * @return \mopsyd\sanctity\libs\pdo\Pdo
     */
    protected function getDatabase()
    {
        $this->_initializeDatabaseObject();
        return self::$database;
    }

    /**
     * Prefixes a provided setting key with the theme slug.
     *
     * @param string $setting
     * @param string $delimiter (optional) The character to use as a buffer
     *     between the provided setting and the prefix. Defaults to an underscore.
     * @return string
     */
    protected function prefixSettingKey( $setting, $delimiter = '-' )
    {
        //add the theme prefix and return the setting
        $prefix = $this->getThemePrefix();
        $prefixed_setting = $prefix . $delimiter . $setting;
        return esc_attr( $prefixed_setting );
    }

    /**
     * Strips the prefix from a setting key and returns its naturalized value.
     *
     * @param string $setting
     * @param string $delimiter (optional) The character to use as a buffer
     *     between the provided setting and the prefix. Defaults to an underscore.
     * @return string
     */
    protected function unprefixSettingKey( $prefixed_setting, $delimiter = '-' )
    {
        //remove the theme prefix and return the corrected setting
        $prefix = $this->getThemePrefix();
        if ( strpos( $prefixed_setting, $prefix ) !== false )
        {
            $prefixed_setting = ltrim( substr( $prefixed_setting,
                    strpos( $prefixed_setting, $prefix ) + strlen( $prefix ) ),
                $delimiter );
        }
        return esc_attr( $prefixed_setting );
    }

    /**
     * Stores and returns a nonce for secure transmission of POST requests.
     *
     * If $referer is not defined, it will only return the nonce itself.
     *
     * If $referer is defined, it will return a uri that resolves the nonce when it is called.
     *
     * Generally, $referer should only be defined for AJAX calls,
     * and otherwise it should be omitted.
     *
     * This method does not return the wordpress form nonce.
     * In the event of a form, the form itself should anticipate
     * the existence of a nonce and place it accordingly.
     *
     * @param string $key The action to bind the nonce to
     * @param string $referer (optional) A uri to bind the nonce to.
     *     If a relative uri is supplied, it will be resolved to the
     *     current hostname. If you are making cross-domain requests,
     *     you will need to provide the fully qualified uri.
     * @return string
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Occurs if an invalid key or referer is provided.
     *     Keys must be a string. Referers may be a string or null.
     */
    protected function getNonce( $key, $referer = null )
    {
        if ( !is_string( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$key] passed in [%2$s] at method [%3$s]. '
                . 'Expected [string], but received [%4$s].', get_class( $this ),
                __METHOD__, gettype( $key ) )
            );
        }
        if ( !is_string( $referer ) && !is_null( $referer ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$referer] passed in [%2$s] at method [%3$s]. '
                . 'Expected [string], but received [%4$s].', get_class( $this ),
                __METHOD__, gettype( $referer ) )
            );
        }
        if ( is_null( $referer ) )
        {
            return wp_create_nonce( esc_attr( $key ) ); // Move to the adapter
        }
        return wp_nonce_url( esc_url( $referer ), esc_attr( $key ) ); // Move to the adapter
    }

    /**
     * Verifies a provided nonce.
     *
     * This method deviates from the standard wordpress behavior of exiting
     * on an invalid nonce, and leaves it to the controller to mitigate failure.
     *
     * Returns true if the nonce validates, and false otherwise.
     *
     * @param string $key
     * @param string $nonce
     * @return bool
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Occurs if an invalid key or nonce is provided.
     *     Keys and nonces must be a string.
     */
    protected function validateNonce( $key, $nonce )
    {
        if ( !is_string( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$key] passed in [%2$s] at method [%3$s]. '
                . 'Expected [string], but received [%4$s].', get_class( $this ),
                __METHOD__, gettype( $key ) )
            );
        }
        if ( !is_string( $nonce ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid [$nonce] passed in [%2$s] at method [%3$s]. '
                . 'Expected [string], but received [%4$s].', get_class( $this ),
                __METHOD__, gettype( $nonce ) )
            );
        }
        return wp_verify_nonce( $nonce, $key ); // Move to the adapter
    }

    /**
     * Prefixes a nonce field key with the nonce prefix and the theme name prefix.
     *
     * @param string $prefixed_setting The nonce key
     * @param string $delimiter The delimiter used to separate
     *     the nonce prefix from the normalized expected key.
     * @return string
     * @internal
     */
    protected function prefixNonceKey( $setting, $delimiter = '-' )
    {
        $nonce_prefix = $this::NONCE_PREFIX;
        $prefixed_value = $nonce_prefix . $delimiter . $this->prefixSettingKey( $setting,
                $delimiter );
        return esc_attr( $prefixed_value ); // Move to the adapter
    }

    /**
     * Removes a nonce prefix and the theme prefix,
     * and returns the normalized nonce key name.
     *
     * @param string $prefixed_setting The nonce key
     * @param string $delimiter The delimiter used to separate
     *     the nonce prefix from the normalized expected key.
     * @return string
     * @internal
     */
    protected function unprefixNonceKey( $prefixed_setting, $delimiter = '-' )
    {
        $nonce_prefix = $this::NONCE_PREFIX;
        if ( strpos( $prefixed_setting, $nonce_prefix ) !== false )
        {
            $prefixed_setting = ltrim( substr( $prefixed_setting,
                    strpos( $prefixed_setting, $nonce_prefix ) + strlen( $nonce_prefix ) ),
                $delimiter );
        }
        $prefixed_setting = $this->unprefixSettingKey( $prefixed_setting,
            $delimiter );
        return esc_attr( $prefixed_setting ); // Move to the adapter
    }

    /**
     * Loads any default meta values required upon object instantiation.
     * @return void
     * @internal
     */
    private function _loadSettingsMetaValues()
    {
        //@todo This is where values set in the database should be obtained. This should also fire the setup method if they have not been configured yet.
    }

    /**
     * Loads the hard coded core defaults if they are not already loaded.
     * This occurs only once per runtime, the first time any model class
     * is constructed, and they persist unmodified beyond that point.
     * @return void
     * @internal
     */
    private function _loadSettingsMetaDefaults()
    {
        if ( is_null( self::$_settings_meta_defaults ) )
        {
            $file = SANCTITY_BASEDIR . 'config/settings.json';
            self::$_settings_meta_defaults = json_decode( file_get_contents( $file ),
                1 );
        }
        $this->_settings_meta = self::$_settings_meta_defaults;
    }

    /**
     * Registers an option as a known internal option,
     * so it can be accessed externally using the standard model
     * getter logic at any time after registration.
     *
     * @param array $opts
     * @return void
     * @internal
     */
    private function _extractOptionKeys( $opts )
    {
        foreach ( $opts as
            $opt )
        {
            $this->options[$opt['id']] = array(
                'key' => $opt['option_name'],
                'default' => $opt['option_args']['default']
            );
        }
    }

    private function _initializeDatabaseObject()
    {
        if ( is_null( self::$database ) )
        {
            self::$database = $this->_initializePdoWrapperObject();
        }
    }

    private function _initializePdoWrapperObject()
    {
        $pdo = new \mopsyd\sanctity\libs\pdo\Pdo();
        return $pdo;
    }

}
