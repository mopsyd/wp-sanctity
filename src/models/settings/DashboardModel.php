<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\settings;

/**
 * Dashboard Model
 * Handles Sanctity Dashboard spash page settings, viewable on the Settings tab of the admin page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class DashboardModel
extends AbstractSettingsModel
{
    const MODEL_SETTINGS_KEY = 'dashboard';
    const MODEL_POSTBACK_KEY = 'dashboard';
    const SETTINGS_PAGE_ID = 'settings-dashboard';

    public function __construct(){
        parent::__construct();
    }

    protected function declareOptions()
    {
        return parent::declareOptions();
    }

    /**
     * This is the page id. It corresponds to the theme prefix slug canonically
     * contencated to the model settings key separated by a hyphen.
     *
     * For the dashboard index, which is the primary settings page of the theme,
     * it simply returns the theme slug as its identifier, which is the prefix
     * used by all other settings page to designate themselves as sub-pages.
     *
     * @return string
     */
    protected function getSettingsPageSlug()
    {
        return $this->getThemePrefix();
    }

    /**
     * This is the human readable name for the menu.
     *
     * This needs to be set on a per-model basis.
     *
     * The controller will take care of the sanitization
     * and translation casting, just return the literal text.
     *
     * @return string
     */
    protected function getSettingsMenuTitle()
    {
        return $this->getThemeName();
    }

    /**
     * This determines both the top level icon in the dashboard menu,
     * as well as the icon on the settings index page.
     *
     * This probably ought to get set if you want
     * a brand identity logo for your theme on the back end.
     *
     * Other settings models are not used to determine top level display.
     * This model is dedicated to serving the settings index, so it is
     * trusted to represent the theme more fully than the others.
     *
     * @return string|false
     */
    protected function getSettingsPageIcon()
    {
        return 'dashicons-welcome-widgets-menus';
    }

}
