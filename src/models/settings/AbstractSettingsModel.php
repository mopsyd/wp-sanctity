<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\settings;

/**
 * Abstract Settings Model
 * The base level Sanctity model for handling forms
 * and interactive configuration of settings.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractSettingsModel
    extends \mopsyd\sanctity\models\AbstractModel
    implements \mopsyd\sanctity\interfaces\models\SanctitySettingsModelInterface
{

    /**
     * Settings Models use the settings config.
     */
    const CONFIG_FILE = 'settings.json';
    const SETTINGS_PAGE_ID = false;
    const SETTINGS_PAGE_AUTH = 'edit_theme_options';
    const SETTINGS_TEMPLATE_PATH = 'sanctity/settings/sections/';
    const SETTINGS_TEMPLATE_SUFFIX = 'twig';
    const DEFAULT_SANITIZER_CALLBACK = '\\mopsyd\\sanctity\\libs\\sanitizer\\Sanitizer::text';

    /**
     * Represents the details for the top level menu page,
     * which is shared by all settings sub-pages.
     * @var array
     */
    private static $menu_page;

    /**
     * Represents the subpages registered, sorted by model settings page id.
     * Child classes can only access the subpage and related data registered
     * for their own individual settings page id.
     * @var array
     */
    private static $menu_subpages = array();

    /**
     * Baseline scripts required for all settings pages.
     *
     * @var array
     */
    private static $settings_baseline_scripts = array(
        'jquery',
        'bootstrap',
        'fontawesome',
        'backend',
    );

    /**
     * Baseline stylesheets required for all settings pages.
     *
     * @var array
     */
    private static $settings_baseline_styles = array(
        'bootstrap-lux',
        'backend'
    );

    /**
     * Baseline scripts required for all customizer pages.
     *
     * @var array
     */
    private static $customizer_baseline_scripts = array();

    /**
     * Baseline stylesheets required for all customizer pages.
     *
     * @var array
     */
    private static $customizer_baseline_styles = array();

    /**
     * If true, will use minified sources when they are available.
     * @var bool
     */
    private $sources_minified = true;

    /**
     * An array of current settings.
     * These are cached for performance if no changes occur.
     */
    private $settings;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * This is the method that handles settings
     * being passed back for sanitization.
     *
     * This method enforces proper sanitization.
     * It will only pass values onward if they pass
     * the integrity check, and will additionally refuse to handle any
     * parameters that do not have a handler method associated with
     * them by the child model.
     *
     * @param array $payload The raw, unfiltered post data,
     *     exactly as it exists in the $_POST superglobal.
     * @return array
     */
    public function postback( $payload )
    {
        $category = $this->_lookupCategoryByPostBody( $payload );
        if ( !$category )
        {
            //Nope, nonce expired or doesn't match.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            __( 'Page not found.' ), 403
            );
        }
        $csrf_valid = $this->_validateCsrf( $category, $payload );
        if ( !$csrf_valid )
        {
            //CSRF fail.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            __( 'This request is not authorized.' ), 401
            );
        }
        $user_auth = $this->_verifyAccessRights( $category );
        if ( !$user_auth )
        {
            //Nope, nonce expired or doesn't match.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            __( 'You do not have permission to perform this action.' ), 403
            );
        }
        $payload = $this->filterFormData( $category, $payload );
//        d($payload);
        $data = $this->_sanitizeFormData( $category, $payload );
//        d($data); exit;
        $status = $this->_setFormOptions( $category, $data );
        if ( method_exists( $this, $category ) )
        {
            return $this->$category( $status );
        }
        $this->settings = null; // Clear the settings so it does not return the cached state.
        return $status;
    }

    /**
     * Returns the settings scoped under the current model.
     *
     * All models have this method. Models dedicated to settings specifically
     * provide additional support methods for configuring their options.
     *
     * @return bool|array
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If a model extending this class does not set its scope constant,
     *     this exception will be thrown. This indicates that the class is
     *     misconfigured, and the exception should not be caught.
     */
    public function getSettings()
    {
        if ( is_null( $this->settings ) )
        {
            $this->updateSettings();
        }
        $this->settings['options'] = $this->getOptions();
        return $this::containerize( $this->settings );
    }

    /**
     * Override this method to return frontend modules that should be loaded
     * dynamically by the frontend library to handle client side logic within
     * the bounds of the model scope..
     */
    public function getModules()
    {
        $modules = array();
        $settings = parent::getSettings();
        if ( is_iterable( $settings ) )
        {
            $base = $this::getAdapter()->get( 'uri', 'override' );
            foreach ( $settings as
                $section =>
                $data )
            {
                if ( is_iterable( $data )
                    && ( ( is_array( $data ) && array_key_exists( 'modules',
                        $data ) )
                    || (is_object( $data )
                    && ( $data instanceof \mopsyd\sanctity\interfaces\libs\container\ContainerInterface )
                    && $data->has( 'modules' )
                    )
                    )
                    && $data['enabled'] !== false
                )
                {
                    foreach ( $data['modules'] as
                        $key =>
                        $module )
                    {
                        $modules[$key] = $base . $module;
                    }
                }
            }
        }
        return $modules;
    }

    /**
     * Override this method to return frontend data for packaging frontend
     * script context that should be injected into frontend modules on page load.
     * The keys for this should correspond to the keys returned from `getModules`
     * from the same model scope.
     * @return array
     */
    public function getClientData()
    {
        $data = array();
        $settings = parent::getSettings();
        foreach ( $settings as
            $section =>
            $subsection )
        {
            if ( is_iterable( $subsection )
                && ( ( is_array( $subsection ) && array_key_exists( 'modules',
                    $subsection ) )
                || (is_object( $subsection )
                && ( $subsection instanceof \mopsyd\sanctity\interfaces\libs\container\ContainerInterface )
                && $subsection->has( 'modules' ) )
                )
                && $subsection['modules'] !== false
            )
            {
                foreach ( $subsection['modules'] as
                    $key =>
                    $module )
                {
                    $data[$key] = array();
                }
            }
        }
        return $data;
    }

    /**
     * Returns the stylesheets associated with the given page type.
     *
     * @param string $section The page type being visited,
     *     which should be "settings", "customizer", "dashboard", "login", or "frontend"
     * @return array
     * @note As of the current release, only the settings page is supported.
     */
    public function getScripts( $section = 'settings' )
    {
        $settings = $this->getSettings();
        if ( $section === 'settings' )
        {
            $scripts = self::$settings_baseline_scripts;
            if ( !empty( $settings ) )
            {
                $conf = $this->getConfig( $this::MODEL_SETTINGS_KEY );
                foreach ( $conf as
                    $section_key =>
                    $section )
                {
                    if ( !( array_key_exists( 'settings-page', $section )
                        && array_key_exists( 'scripts',
                            $section['settings-page'] ) ) )
                    {
                        continue;
                    }
                    foreach ( $section['settings-page']['scripts'] as
                        $script )
                    {
                        if ( !in_array( $script, $scripts ) )
                        {
                            $scripts[] = $script;
                        }
                    }
                }
            }
            return $scripts;
        } elseif ( $section === 'customizer' )
        {
            //Breakpoint for future customizer support
            d( 'update for customizer support here' );
            exit();
        } else
        {
            //Placeholder for future development for various dashboard scripts
            return array();
        }
    }

    /**
     * Returns the javascript associated with the given page type.
     *
     * @param string $section The page type being visited,
     *     which should be "settings", "customizer", "dashboard", "login", or "frontend"
     * @return array
     * @note As of the current release, only the settings page is supported.
     */
    public function getStyles( $section = 'settings' )
    {
        $settings = $this->getSettings();
        if ( $section === 'settings' )
        {
            $styles = self::$settings_baseline_styles;
            if ( !empty( $settings ) )
            {
                $conf = $this->getConfig( $this::MODEL_SETTINGS_KEY );
                foreach ( $conf as
                    $section_key =>
                    $section )
                {
                    if ( !( array_key_exists( 'settings-page', $section )
                        && array_key_exists( 'styles', $section['settings-page'] ) ) )
                    {
                        continue;
                    }
                    foreach ( $section['settings-page']['styles'] as
                        $style )
                    {
                        if ( !in_array( $style, $styles ) )
                        {
                            $styles[] = $style;
                        }
                    }
                }
            }
            return $styles;
        } elseif ( $section === 'customizer' )
        {
            //Breakpoint for future customizer support
            d( 'update for customizer support here' );
            exit();
        } else
        {
            //Placeholder for future development for various dashboard scripts
            return array();
        }
    }

    /**
     * Returns the  details required to list the settings page
     * in the Dashboard index menu.
     *
     * @return array
     */
    public function getMenuDetails()
    {
        return array(
            'menu_section' => $this->getSettingsMenuSection(),
            'page' => $this->getSettingsPageSlug(),
            'title' => $this->getSettingsPageTitle(),
            'menu_title' => $this->getSettingsMenuTitle(),
            'icon' => $this->getSettingsPageIcon(),
            'sanitizer' => $this->getSettingsSanitizerCallback(),
            'slug' => $this->getSettingsPageSlug(),
            'permission' => $this->getSettingsPageAuthLevel(),
            'template' => $this->getSettingsPageTemplate()
        );
    }

    /**
     * Returns the details required to render
     * the settings page when it is visited.
     *
     * @return array
     */
    public function getPageDetails()
    {
        $details = $this->getMenuDetails();
        $options_filtered = $this->_filterPrivileges( $this->getSettings() );
        if ( empty( $options_filtered ) )
        {
            $options_filtered = false;
        }
        if ( $details['icon'] && strpos( $details['icon'], 'dashicons' ) === 0 )
        {
            //Prefix the dashicons icon with the master css handle so it can render in the template.
            $details['icon'] = 'dashicons ' . $details['icon'];
        }
        $subsections = $this->_filterPrivileges( $this->getSettings() );
        $details['subsections'] = empty( $subsections )
            ? false
            : $subsections;
        return $details;
    }

    /**
     * Override this method to perform a preflight filter on submitted form
     * values prior to them being passed to the sanitizer.
     *
     * @param string $category The category corresponding to the form section
     *     that the form was received from.
     * @param array $values The unfiltered form values
     * @return array
     */
    public function filterFormData( $category, $values )
    {
        return $values;
    }

    /**
     * Rehashes all current settings.
     * This fires when updates occur
     * that invalidate the internal
     * cache of settings.
     *
     * @return void
     */
    protected function updateSettings()
    {
        $settings = parent::getSettings();
        $result = $this->_generateFormData( $settings );
        $result['scripts'] = $this->_generateScriptData( $settings );
        $result['styles'] = $this->_generateStyleData( $settings );
        $result = $this->_parseOptions( $result );
//        $result['enabled'] = $settings['enabled'];
        $this::debugStash( array(
            'settings_key' => $this::MODEL_SETTINGS_KEY,
            'settings' => $result ), 'model_admin_settings' );
        $this->settings = $result;
    }

    private function _filterPrivileges( $details )
    {
        if ( !$details || empty( $details ) )
        {
            return $details;
        }
        foreach ( $details as
            $key =>
            $section )
        {
            if ( !array_key_exists( 'auth-permission', $section ) )
            {
                //No access rights assigned for this section.
                continue;
            }
            $auth_perm = $section['auth-permission'];
            if ( !current_user_can( $auth_perm ) )
            {
                unset( $details[$key] );
            }
        }
        return $details;
    }

    /**
     * This determines what section the menu lives in. By default it is scoped
     * to a dedicated settings page for the menu, but this can be overridden
     * in any given model to place its settings page into another menu section.
     *
     * This doesn't need to be changed, and will automatically pick up new models
     * appropriately if it is not altered.
     *
     * @return string
     */
    protected function getSettingsMenuSection()
    {
        return $this->getThemePrefix();
    }

    /**
     * This is the page id. It corresponds to the theme prefix slug canonically
     * contencated to the model settings key separated by a hyphen.
     *
     * This doesn't need to be changed, and will automatically pick up new models
     * appropriately if it is not altered.
     *
     * @return string
     */
    protected function getSettingsPageSlug()
    {
        return $this->getThemePrefix() . '-' . $this::MODEL_SETTINGS_KEY;
    }

    /**
     * This is the human readable name for the menu.
     *
     * This needs to be set on a per-model basis.
     *
     * The controller will take care of the sanitization
     * and translation casting, just return the literal text.
     *
     * @return string
     */
    protected function getSettingsMenuTitle()
    {
        return ucwords( $this::MODEL_SETTINGS_KEY );
    }

    /**
     * This is the human readable name for the page.
     *
     * This needs to be set on a per-model basis.
     *
     * The controller will take care of the sanitization
     * and translation casting, just return the literal text.
     *
     * @return string
     */
    protected function getSettingsPageTitle()
    {
        return ucwords( $this->getThemePrefix() . ' ' . $this::MODEL_SETTINGS_KEY );
    }

    /**
     * This is an optional class to use to prefix an icon to the settings page.
     *
     * This is entirely arbitrary and can be omitted
     * without disrupting Wordpress internals.
     *
     * @return string|false
     */
    protected function getSettingsPageIcon()
    {
        return false;
    }

    /**
     * This is the callback that performs sanitization on response data.
     *
     * This doesn't need to be changed. The postback does need to be
     * routed internally though on a per-model basis.
     *
     * @return callable
     */
    protected function getSettingsSanitizerCallback()
    {
        return array(
            $this,
            'postback' );
    }

    /**
     * Returns the Wordpress authorization level required to view the settings panel.
     *
     * By default, this uses the value of the class constant `SETTINGS_PAGE_AUTH`,
     * which can be overridden in a subclass model to universally change its auth
     * permission across all of its internals.
     *
     * @return string
     */
    protected function getSettingsPageAuthLevel()
    {
        return $this::SETTINGS_PAGE_AUTH;
    }

    /**
     * Returns the expected template location for the settings page.
     *
     * This method contencates the following three three class constants
     * to determine its output:
     *
     * `SETTINGS_MODEL_PATH` to determine the folder location from the template directory,
     * `MODEL_SETTINGS_KEY` to determine the template filename, which by default is identical to the model identifier key,
     * `SETTINGS_TEMPLATE_SUFFIX` to determine the template file suffix,
     *
     * These together create a relative location for the template,
     * which is checked for from registered template folder locations
     * in both the child theme and the parent theme.
     *
     * @return string
     */
    protected function getSettingsPageTemplate()
    {
        return trailingslashit( $this::SETTINGS_TEMPLATE_PATH )
            . $this::MODEL_SETTINGS_KEY
            . '.' . $this::SETTINGS_TEMPLATE_SUFFIX;
    }

    /**
     * This method enforces that child class models provide validation by
     * ALWAYS throwing an exception if a validation method is not provided.
     * This will ALWAYS fire when the controller enacts form parsing if it
     * was omitted.
     *
     * @param array $request
     * @return void
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This MUST be an instance of \InvalidArgumentException which
     *     correctly implements the Sanctity exception interface.
     *     This exception MUST NOT be thrown for any reason if the
     *     request is valid.
     */
    public function validateRequest( $request )
    {
        throw new \mopsyd\sanctity\libs\exception\SanctityException(
        sprintf( 'No data is inherently trustworthy. Instance of [%1$s] must '
            . 'correctly configure validation. Override [%2$s] with proper '
            . 'validation in [%1$s] to disable this error.', get_class( $this ),
            __METHOD__ )
        );
    }

    /**
     * This method by default makes no assumption about what is or is not
     * correct data, and will always return false unless overridden.
     *
     * If validateRequest throws an exception and this method returns false,
     * the controller will report that the class is misconfigured and disable
     * it permanently until an update occurs.
     *
     * @param array $request
     * @return bool|array|\Traversable
     */
    public function getInvalidFields( $request )
    {
        return false;
    }

    /**
     * By default, no action is taken on requests.
     *
     * @param array $request
     * @return bool|array|\Traversable
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \InvalidArgumentException
     *     implementing the aforementioned interface if provided request
     *     data is not correct.
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \RuntimeException
     *     implementing the aforementioned interface if class logic
     *     is not correct.
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \LogicException
     *     implementing the aforementioned interface if either
     *     Wordpress or the database derp out, but the handling
     *     logic of the class operating on them is sound.
     */
    public function handleRequest( $request )
    {
        //no-op
    }

    /**
     * By default, no assumption about corrective measures is provided.
     *
     * @param array $request
     */
    public function mitigateFailure( $request )
    {
        //no-op
    }

    protected function getSettingsPageBinding( $form )
    {
        if ( $this::SETTINGS_PAGE_ID )
        {
            return $this->prefixSettingKey( $this::SETTINGS_PAGE_ID . '-' . $form );
        }
        return false;
    }

    protected function getSettingsPageAction()
    {
        if ( $this::MODEL_POSTBACK_KEY )
        {
            return esc_url( get_site_url( null,
                    'wp-admin/admin.php?page=' . $this->prefixSettingKey( $this::MODEL_POSTBACK_KEY ) ) );
        }
        return false;
    }

    protected function getSettingsPageReferrer()
    {
        return esc_url( site_url() . $_SERVER['REQUEST_URI'] );
    }

    protected function getSettingsPageMethod()
    {
        return 'POST';
    }

    protected function declareOptions()
    {
        return $this->getSettings();
    }

    private function _parseOptions( $settings )
    {
        $groups = array();
        foreach ( $settings as
            $key =>
            $value )
        {
            if ( !array_key_exists( 'fields', $value ) )
            {
                continue;
            }
            $groups[$key] = $this->_parseSettingsOptionsHeading( $value );
            if ( !$value['options'] )
            {
                continue;
            }
            $groups[$key]['options'] = $this->_parseSettingsFormFieldConfig( $value['fields'] );
        }
        return $groups;
    }

    private function _parseSettingsOptionsHeading( $data )
    {
        $result = array(
            'enabled' => $data['enabled'],
            'title' => $data['title'],
            'description' => $data['description'],
            'icon' => $data['icon'],
            'permission' => $data['auth-permission'],
            'action' => $data['action'],
            'method' => $data['method'],
            'template' => $data['template-id'],
            'name' => $data['form-id'],
            'form' => $data['form'],
            'sanitize_callback' => array_key_exists( 'sanitize-callback', $data )
            ? $data['sanitize-callback']
            : null,
        );
        return $result;
    }

    private function _parseSettingsFormFieldConfig( $fields )
    {
        $field_array = array();
        foreach ( $fields as
            $field_key =>
            $field )
        {
            $field_array[$field_key] = $this->_parseSettingsFormFieldHeading( $field_key,
                $field );
        }
        return $field_array;
    }

    private function _parseSettingsFormFieldHeading( $field_key, $field )
    {
        $opt = array(
            "id" => $field_key,
            "option_group" => $this->getThemePrefix() . '-' . $this::MODEL_SETTINGS_KEY,
            "option_name" => $this->getThemePrefix() . '-' . $field['field-id'],
            "option_args" => array(
                "type" => $this->_parseSettingsFormFieldType( $field['type'] ),
                "description" => $field['label'],
                "sanitize_callback" => $field['sanitizer-callback'],
                "show_in_rest" => $field['rest'],
                "default" => $field['default'],
            ),
            "mode" => $field['mode'],
            "component" => $field['form-component'],
            "wordpress-option" => $field['is-wordpress-option'],
            "customizer-option" => $field['is-customizer-option'],
            "values" => $this->_parseSettingsFormFieldValueTypes( $field['type'],
                array_key_exists( 'options', $field )
                ? $field['options']
                : null )
        );
        return $opt;
    }

    private function _parseSettingsFormFieldType( $type )
    {
        switch ( $type )
        {
            case "checkbox":
                return 'boolean';
                break;
            case "options":
            case "input":
            case "password":
            case "select":
            case "radio":
            case "color":
            case "textarea":
            default:
                return 'string';
                break;
        }
    }

    private function _parseSettingsFormFieldValueTypes( $type, $options )
    {
        switch ( $type )
        {
            case "options":
                return $this->_parseOptionsFields( $options );
                break;
            case "input":
                return 'string';
                break;
            case "password":
                return null;
                break;
            case "select":
                return null;
                break;
            case "radio":
                return null;
                break;
            case "checkbox":
                return array(
                    true,
                    false );
                break;
            case "color":
                return null;
                break;
            case "textarea":
                return null;
                break;
            default:
                return null;
                break;
        }
    }

    protected function getSectionData()
    {
        return array();
    }

    /**
     * Populates the form information for the settings model.
     * @param array $settings
     * @return array
     * @internal
     */
    private function _generateFormData( $settings )
    {
        foreach ( $settings as
            $key =>
            $section )
        {
            if ( array_key_exists( 'form-id', $section ) )
            {
                foreach ( $section['fields'] as
                    $field_key =>
                    $field )
                {
                    if ( !$field['is-wordpress-option'] )
                    {
                        $section['fields'][$field_key]['value'] = $field['default'];
                    } else
                    {
                        $section['fields'][$field_key]['value'] = get_option( $this->getThemePrefix() . '-' . $field['field-id'],
                            $section['fields'][$field_key]['default'] );
                    }
                }
                $section['form']['name'] = $section['form-id'];
                $section['form']['fields'] = $section['fields'];
                $section['form']['nonce'] = $this->getNonce( $section['form-id'] );
                $section['form']['nonce-field'] = $this->prefixNonceKey( $this::SETTINGS_PAGE_ID . '-' . $section['form-id'] );
                $section['form']['binding'] = $this->getSettingsPageBinding( $section['form-id'] );
                $section['form']['action'] = $this->getSettingsPageAction();
                $section['form']['referrer'] = $this->getSettingsPageReferrer();
                $section['form']['method'] = $this->getSettingsPageMethod();
                $section['form']['method'] = $this->getSettingsPageMethod();
                $settings[$key] = $section;
            }
        }
        return $settings;
    }

    private function _generateScriptData( $settings )
    {
        $scripts = array();
        foreach ( $settings as
            $section =>
            $details )
        {
            if ( !(array_key_exists( 'settings-page', $details )
                && array_key_exists( 'scripts', $details['settings-page'] ) ) )
            {
                continue;
            }
            $scripts = $this->_filterScriptData( $details['settings-page']['scripts'],
                $scripts );
        }
        return array_merge( self::$settings_baseline_scripts, $scripts );
    }

    private function _generateStyleData( $settings )
    {
        $styles = array();
        foreach ( $settings as
            $section =>
            $details )
        {
            if ( !(array_key_exists( 'settings-page', $details )
                && array_key_exists( 'styles', $details['settings-page'] ) ) )
            {
                continue;
            }
            $styles = $this->_filterStyleData( $details['settings-page']['styles'],
                $styles );
        }
        return array_merge( self::$settings_baseline_scripts, $styles );
    }

    /**
     * Compiles javascript objects to represent the required scripts for the settings page.
     *
     * @param array $scripts An array of script slug identifiers
     * @param array $existing An array of existing Javascript objects
     * @return array Returns an array of \mopsyd\sanctity\libs\media\Javascript objects
     * @internal
     */
    private function _filterScriptData( $scripts, $existing )
    {
        $updated = array();
        $min = $this->sources_minified;
        foreach ( $scripts as
            $handle =>
            $script )
        {
            $updated[$handle] = $this->load( 'library', 'media\\Javascript' )->load( $script );
        }
        return array_merge( $existing, $updated );
    }

    /**
     * Compiles stylesheet objects to represent the required styles for the settings page.
     *
     * @param array $styles An array of stylesheet slug identifiers
     * @param array $existing An array of existing Stylesheet objects
     * @return array Returns an array of \mopsyd\sanctity\libs\media\Stylesheet objects
     * @internal
     */
    private function _filterStyleData( $styles, $existing )
    {
        $updated = array();
        $min = $this->sources_minified;
        foreach ( $styles as
            $handle =>
            $style )
        {
            $updated[$handle] = $this->load( 'library', 'media\\Stylesheet' )->load( $style );
        }
        return array_merge( $existing, $updated );
    }

    private function _parseOptionsFields( $fields )
    {
        $options = array();
        foreach ( $fields as
            $field =>
            $option )
        {
            if ( !$option['is-wordpress-option'] )
            {
                continue;
            }
            $options[$field] = array(
                'name' => $option['field-id'],
                'type' => $this->_getOptionFieldType( $option['type'] ),
                'description' => $option['label'],
                'sanitize_callback' => $option['sanitizer-callback'],
                'rest' => $option['rest'],
                'default' => $option['default'],
            );
        }
        return $options;
    }

    private function _getOptionFieldType( $type )
    {
        switch ( $type )
        {
            case 'checkbox':
                return 'boolean';
                break;
            case 'number':
            case 'range':
                return 'number';
                break;
            default:
                return 'string';
                break;
        }
    }

    private function _lookupCategoryByPostBody( $payload )
    {
        $payload_keys = array_keys( $payload );
        $settings = $this->getSettings();
        foreach ( $settings as
            $category =>
            $details )
        {
            if ( !array_key_exists( 'form', $details ) || !array_key_exists( 'nonce-field',
                    $details['form'] ) )
            {
                continue;
            }
            if ( !in_array( $details['form']['nonce-field'], $payload_keys ) )
            {
                continue;
            }
            return $category;
        }
        return false;
    }

    private function _validateCsrf( $category, $payload )
    {
        $settings = $this->getSettings();
        if ( !( $settings->has( $category )
            && array_key_exists( 'form', $settings->get( $category ) )
            && array_key_exists( 'nonce-field',
                $settings->get( $category )['form'] )
            )
        )
        {
            return false;
        }
        $nonce_field = $settings[$category]['form']['nonce-field'];
        $category_id = $settings[$category]['name'];
        return (array_key_exists( $nonce_field, $payload )
            && wp_verify_nonce( $payload[$nonce_field], $category_id ) );
    }

    private function _verifyAccessRights( $category )
    {
        $settings = $this->getSettings();
        if ( !( $settings->has( $category )
            && array_key_exists( 'permission', $settings->get( $category ) ) ) )
        {
            return false;
        }
        $permission = $settings->get( $category )['permission'];
        return current_user_can( $permission );
    }

    private function _sanitizeFormData( $category, $data )
    {
        $settings = $this->getSettings();
        if ( !($settings->has( $category )
            && array_key_exists( 'form', $settings->get( $category ) ) ) )
        {
            //No end-user settings for this section.
            return array();
        }
        $option_keys = array();
        $section = $settings->get( $category )['form']['fields'];
        foreach ( $section as
            $key =>
            $field )
        {
            $option_keys[$field['field-id']] = array_key_exists( $field['field-id'],
                    $data )
                ? $data[$field['field-id']]
                : $this::getAdapter()->get( 'option',
                    $this->getThemePrefix() . '-' . $field['field-id'] );
            $sanitizer = array_key_exists( 'sanitizer-callback', $field )
                ? $field['sanitizer-callback']
                : $this::DEFAULT_SANITIZER_CALLBACK;
            if ( !is_callable( $sanitizer ) )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
                sprintf( 'Provided sanitizer for settings key [%1$s] is not callable in [%2$s].',
                    $field['field-id'], get_class( $this ) ) );
            }
            $option_keys[$field['field-id']] = $sanitizer( $option_keys[$field['field-id']] );
        }
        return $option_keys;
    }

    private function _setFormOptions( $category, $data )
    {
        $valid = array();
        foreach ( $data as
            $key =>
            $value )
        {
            $opt_key = $this->getThemePrefix() . '-' . $key;
            add_option( $opt_key, $value, '', true );  // Push to the adapter. Get all of the wordpress junk out of the mvc layer.
            $this::getAdapter()->set( 'option', $opt_key, $value );
            //Verify that the option was added
            if ( $this::getAdapter()->get( 'option', $opt_key ) === $value )
            {
                $valid[] = $key;
            }
//            d($opt_key, $value, $this::getAdapter()->get( 'option', $opt_key ), $this::getAdapter()->get( 'option', $opt_key ) === $value ); exit;
        }
        $results = array(
            "success" => count( $valid ) === count( $data ),
            "missing" => false,
            "updated" => $valid
        );
        if ( !$results['success'] )
        {
            foreach ( $valid as
                $key )
            {
                if ( in_array( $key, $data ) )
                {
                    unset( $data[$key] );
                }
            }
            $results['missing'] = array_keys( $data );
        }
        return $results;
    }

}
