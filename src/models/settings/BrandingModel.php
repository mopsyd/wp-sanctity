<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\settings;

/**
 * Branding Model
 * Handles Sanctity Branding settings, configurable on the Settings tab of the admin page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class BrandingModel
    extends AbstractSettingsModel
{

    const MODEL_SETTINGS_KEY = 'branding';
    const MODEL_POSTBACK_KEY = 'branding';
    const SETTINGS_PAGE_ID = 'settings-branding';

    private static $filter_methods = array(
        'links' => '_filterSocialLinks'
    );

    public function __construct()
    {
        parent::__construct();
    }

    protected function declareOptions()
    {
        return parent::declareOptions();
    }

    protected function getSettingsPageIcon()
    {
        return 'dashicons-id';
    }

    /**
     * Filters unsanitized branding form submission data.
     *
     * @param string $category The category corresponding to the form section
     *     that the form was received from.
     * @param array $values The unfiltered form values
     * @return array
     */
    public function filterFormData( $category, $values )
    {
        if ( array_key_exists( $category, self::$filter_methods ) )
        {
            $method = self::$filter_methods[$category];
            return $this->$method( $category, $values );
        }
        return $values;
    }

    /**
     * Returns the defined/default social settings
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getSocial()
    {
        $settings = $this->getSettings()['links']['form']['fields'];
        $options = array();
        foreach ( $this->getSettings()['links']['form']['fields'] as
            $section =>
            $data )
        {
            if ( array_key_exists( 'value', $data ) && $data['value'] )
            {
                $options[$data['value']['handle']] = $data['value'];
            } else
            {
                $options[$data['default']['handle']] = $data['default'];
            }
        }
        return $this::containerize( $options );
    }

    /**
     * Filters inbound social media data.
     *
     * @param string $category The slug of the form section.
     * @param array $values The submitted values from the form.
     * @return array
     */
    private function _filterSocialLinks( $category, $values )
    {
        $prefix = 'branding-link-';
        $suffix = '-display';
        $settings = $this->getSettings()[$category]['form']['fields'];
        $endpoints = array();
        foreach ( $values as
            $key =>
            $value )
        {
            if ( strpos( $key, $prefix ) !== 0
                || strpos( $key, $suffix ) !== false )
            {
                continue;
            }
            $k = str_replace( $prefix, null, $key );
            // Set defaults
            $endpoints[$key] = array(
                'handle' => $k,
                'icon' => $settings['link-' . $k]['icon'],
                'endpoint' => $this->_verifySocialEndpoint( $k, $value )
                ?: $settings['link-' . $k]['default']['endpoint'],
                'enabled' => false,
                'valid' => false,
                'deeplink' => false, // Deeplinking is for app integration, and is not yet implemented.
                'deeplink-uri' => null, // Deeplinking is for app integration, and is not yet implemented.
                // Whether or not the request can be automatically verified.
                // Some social platforms block curl requests to profiles.
                // This may be enabled for users who provide their own api tokens,
                // but no default access token will be provided for this purpose.
                'autoverify' => $settings['link-' . $k]['default']['autoverify']
            );
            $endpoints[$key]['enabled'] = (
                array_key_exists( $key . $suffix, $values )
                && $endpoints[$key]['endpoint'] )
                ? true
                : false;
            $endpoints[$key]['valid'] = (!is_null( $endpoints[$key]['endpoint'] )
                ? $this->_verifyEndpoint( $endpoints[$key]['endpoint'], $k,
                    $endpoints[$key]['autoverify'] )
                : false );
        }
//        d( $endpoints );
//        exit;
        return $endpoints;
    }

    private function _verifySocialEndpoint( $key, $value )
    {
        $result = false;
        if ( $value === '' || is_null( $value ) )
        {
            // Empty values and null values are always false.
            // Don't bother with the overhead of checking further.
            return false;
        }
        $result = $value;
        switch ( $key )
        {
            case 'facebook':
                // Allows for either a username or facebook.com link. Either or.
                $handle = $this->_sanitizeDomainHandle( $result, 'facebook.com/' );
                $result = $this->_checkUrl( 'https://www.facebook.com/' . $handle . '/' );
                break;
            case 'linkedin':
                // Allows for either a username or linkedin.com link. Either or.
                $handle = $this->_sanitizeDomainHandle( $result,
                    'linkedin.com/in/' );
                $result = $this->_checkUrl( 'https://www.linkedin.com/in/' . $handle . '/' );
                break;
            case 'twitter':
                // Allows for either a @username or twitter.com link. Either or.
                $result = ltrim( $result, '@' );
                $handle = $this->_sanitizeDomainHandle( $result, 'twitter.com/' );
                $result = $this->_checkUrl( 'https://twitter.com/' . ltrim( $handle,
                        '@' ) );
                break;
            case 'tumblr':
                // Allows for either a username or username.tumblr.com link. Either or.
                $handle = $this->_sanitizeSubdomainHandle( $result, 'tumblr.com' );
                $result = $this->_checkUrl( 'https://' . $handle . '.tumblr.com/' );
                break;
            case 'wordpress':
                // Allows for either a username or en.gravatar.com/username link. Either or.
                // Note: This site supports multiple languages as the subdomain prefix.
                // This logic does not support this at the moment. You get the english link
                // and that's it for the time being. It may be expanded upon later.
                $handle = $this->_sanitizeDomainHandle( $result,
                    'en.gravatar.com/' );
                $result = $this->_checkUrl( 'https://en.gravatar.com/' . $handle . '/' );
                break;
            case 'stackoverflow':
                // Allows for a stackoverflow.com/userid/username link.
                $handle = $this->_sanitizeDomainHandle( $result,
                    'stackoverflow.com/users/' );
                $result = $this->_checkUrl( 'https://stackoverflow.com/users/' . $handle . '/' );
                break;
            case 'github':
                // Allows for either a username or github.com/username link. Either or.
                $handle = $this->_sanitizeDomainHandle( $result, 'github.com/' );
                $result = $this->_checkUrl( 'https://github.com/' . $handle . '/' );
                break;
            case 'bitbucket':
                // Allows for either a username or bitbucket.org/username link. Either or.
                $handle = $this->_sanitizeDomainHandle( $result,
                    'bitbucket.org/' );
                $result = $this->_checkUrl( 'https://bitbucket.org/' . $handle . '/' );
                break;
            case 'gitlab':
                // Allows for either a username or gitlab.com/username link. Either or.
                $handle = $this->_sanitizeDomainHandle( $result, 'gitlab.com/' );
                $result = $this->_checkUrl( 'https://gitlab.com/' . $handle . '/' );
                break;
        }
        return $result;
    }

    private function _verifyEndpoint( $url, $handle, $autoverify )
    {
        if ( is_null( $url ) )
        {
            // Null values cannot be checked.
            return false;
        }
        if ( !$autoverify )
        {
            // Can't help you bub. I hope you copypasta'd it right.
            return true;
        }
        $options = array(
            CURLOPT_HEADER => true, // return headers
            CURLOPT_NOBODY => true, // don't return the body
            CURLOPT_RETURNTRANSFER => true, // grab the response headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_COOKIEFILE => '', // keep cookies in memory only
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            CURLOPT_ENCODING => "", // handle compressed
//            CURLOPT_HTTPHEADER => $this->_getUserHeaders(), // Use as much of the site admin's browser data as possible.
            CURLOPT_USERAGENT => ( array_key_exists( 'HTTP_USER_AGENT', $_SERVER ) )
            ? $_SERVER['HTTP_USER_AGENT'] // Use the user's normal user agent if possible.
            : "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3", // Use a generic user agent for cli requests.
            CURLOPT_AUTOREFERER => true, // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 10, // time-out on connect
            CURLOPT_TIMEOUT => 10, // time-out on response
        );
        $ch = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $output = curl_exec( $ch );
        $httpcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );
        return ($httpcode !== 404
            ? true
            : false );
    }

    /**
     * Preflights a uri to insure it is actually a valid structure worth
     * the overhead of checking with a remote call.
     *
     * @param type $url
     * @return bool
     */
    private function _checkUrl( $url )
    {
        if ( !$url || !is_string( $url ) || !preg_match( '/^http(s)?:\/\/[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i',
                $url ) )
        {
            return false;
        }
        return $url;
    }

    /**
     * Strips a potential expected domain prefix,
     * and returns the username appended to the end,
     * or the username as is if there is no domain.
     *
     * This is used for social profiles where the username is part of the uri
     *
     * @param string $result
     * @param string $uri
     * @return string
     * @internal
     */
    private function _sanitizeDomainHandle( $result, $uri )
    {
        $handle = $result;
        if ( strpos( $result, $uri ) !== false )
        {
            $handle = trim( substr( $result,
                    strpos( $result, $uri ) + strlen( $uri ) ), '/' );
            while ( strpos( $handle, $uri ) !== false )
            {
                $handle = trim( substr( $handle,
                        strpos( $handle, $uri ) + strlen( $uri ) ), '/' );
            }
        }
        return $handle;
    }

    /**
     * Strips a potential expected domain prefix,
     * and returns the username which is a subdomain of the domain,
     * or the username as is if there is no domain.
     *
     * This is used for social profiles where the username
     * is a subdomain instead of a uri segment.
     *
     * @param string $result
     * @param string $uri
     * @return string
     * @internal
     */
    private function _sanitizeSubdomainHandle( $result, $uri )
    {
        $handle = $result;
        if ( strpos( $result, $uri ) !== false )
        {
            $handle = trim( str_replace( 'http://', null,
                    str_replace( 'https://', null,
                        substr( $result, 0, strpos( $result, $uri ) ) ) ), '.' );
            while ( strpos( $handle, $uri ) !== false )
            {
                $handle = trim( str_replace( 'http://', null,
                        str_replace( 'https://', null,
                            trim( substr( $handle, 0, strpos( $handle, $uri ) ),
                                '/' ) ) ), '.' );
            }
        }
        return $handle;
    }

    private function _getUserHeaders()
    {
        $headers = array();
        if ( array_key_exists( 'REMOTE_ADDR', $_SERVER ) )
        {
            $headers[] = 'X-Forwarded-For: ' . $_SERVER['REMOTE_ADDR'];
        }
        if ( array_key_exists( 'HTTP_USER_AGENT', $_SERVER ) )
        {
            $headers[] = 'User-Agent: ' . $_SERVER['HTTP_USER_AGENT'];
        } else
        {
            $headers[] = 'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3';
        }
        if ( array_key_exists( 'HTTP_DNT', $_SERVER ) )
        {
            $headers[] = 'DNT: ' . $_SERVER['HTTP_DNT'];
        }
        if ( array_key_exists( 'HTTP_ACCEPT', $_SERVER ) )
        {
            $headers['Accept'] = 'Accept: ' . $_SERVER['HTTP_ACCEPT'];
        }
        if ( array_key_exists( 'HTTP_ACCEPT_ENCODING', $_SERVER ) )
        {
            $headers[] = 'Accept-Encoding: ' . $_SERVER['HTTP_ACCEPT_ENCODING'];
        }
        if ( array_key_exists( 'HTTP_ACCEPT_LANGUAGE', $_SERVER ) )
        {
            $headers[] = 'Accept-Language: ' . $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }
        if ( array_key_exists( 'HTTP_REFERER', $_SERVER ) )
        {
            $headers[] = 'Referer: ' . $this::REQUEST_URI;
        }
        if ( array_key_exists( 'HTTP_HOST', $_SERVER ) )
        {
            $headers[] = 'Host: ' . $_SERVER['HTTP_HOST'];
        }
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        return $headers;
    }

}
