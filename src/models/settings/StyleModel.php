<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\settings;

/**
 * Style Model
 * Handles stylistic options for the theme that are not branding related,
 * configurable on the settings tab of the admin page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class StyleModel
    extends AbstractSettingsModel
{

    const MODEL_SETTINGS_KEY = 'style';
    const MODEL_POSTBACK_KEY = 'style';
    const SETTINGS_PAGE_ID = 'settings-style';

    public function __construct()
    {
        parent::__construct();
    }

    protected function declareOptions()
    {
        return parent::declareOptions();
    }

    protected function getSettingsPageIcon()
    {
        return 'dashicons-art';
    }

    protected function colors( $status )
    {
        $options = $this->getOptions();
        if ( empty( $status['updated'] ) )
        {
            // Nothing to do.
            return $status;
        }
        $lead = '';
        foreach ( $options as
            $key =>
            $opt )
        {
            if ( strpos( $key, 'color-' ) !== 0 || $opt === '' )
            {
                continue;
            }
            $lead .= '$'
                . $key . ': '
                . $opt . ';'
                . PHP_EOL;
        }
        $tpl = $lead . PHP_EOL
            . file_get_contents( SANCTITY_BASEDIR . 'etc/tpl/style/style.tpl.scss' );
        $handle = 'colors';
        $sass_compiler = $this->load( 'library', 'sass\\Sass' );
        $sass_compiler->setTemplateScope( 'style' );
        $sass_compiler->save( $handle, $tpl );
        $sass_compiler->saveCompiled( $handle );
        // Send valid update response
        $result = array(
            'code' => 200,
            'status' => 'success',
            'title' => 'Color Settings Updated',
            'message' => 'Your color style profile has been updated.'
        );
        return $result;
    }

    /**
     * Provides frontend data for the style settings page to package
     * into admin settings page frontend modules.
     * @return array
     */
    public function getClientData()
    {
        $data = parent::getClientData();
        foreach ( $data as
            $key =>
            $value )
        {
            switch ( $key )
            {
                case 'bootstrap-skin':
                    $data[$key] = $this->_getSkinDetails();
                    break;
                case 'style-colors':
                    $data[$key] = $this->_getColorDetails();
                    break;
            }
        }
        return $data;
    }

    private function _getSkinDetails()
    {
        $model = $this->load( 'model', 'core\\RestModel' );
        $data = array(
            'bootswatch' => $model->send( 'bootswatch', 'get-themes' )
        );
        return $data;
    }

    private function _getColorDetails()
    {
        $config = $this->load( 'library', 'config\\ColorConfig' );
        $data = array(
            'swatches' => $config->get()->toArray()
        );
        return $data;
    }

}
