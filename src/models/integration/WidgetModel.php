<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\integration;

/**
 * Widget Integration Model
 * Handles Sanctity integrations with Wordpress Widgets.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class WidgetModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'widgets';
    const CONFIG_FILE = 'widgets.json';

    private static $widgets;
    private static $widgets_initialized = false;

    public function __construct()
    {
        $this->_initializeWidgets();
        parent::__construct();
    }

    public function registerWidgets()
    {
        foreach ( self::$widgets as
            $handle =>
            $widget )
        {
            $class = $widget['class'];
            $widget['options']['class'] = $class;
            $widget['options']['description'] = $widget['description'];
            $class::setDetails( $widget );
            register_widget( $class );
        }
    }

    /**
     * Retrieves the context required by a widget based on its
     * identifying slug, so the Widget itself does not need to
     * be equipped to interact with the rest of Sanctity.
     *
     * @param string $slug
     */
    public function getWidgetContext( $slug )
    {
        $context = array();
        $handle = false;
        foreach ( self::$widgets as
            $key =>
            $widget )
        {
            if ( $widget['slug'] === $slug )
            {
                $handle = $key;
                break;
            }
        }
        if ( $handle )
        {
            $config = $this->load( 'library', 'config\\ContextConfig' );
            foreach ( $config->get() as
                $key =>
                $element )
            {
                if ( in_array( $key, self::$widgets->get( $handle )['context'] ) )
                {
                    $context[$key] = $element;
                }
            }
        }
        return array_merge( $context,
            $this->_packageWidgetContextKeys( self::$widgets->get( $handle )['context'] ) );
    }

    private function _packageWidgetContextKeys( $keys )
    {
        $context = array();
        foreach ( $keys as
            $key )
        {
            $model_class = 'display\\' . ucfirst( $key ) . 'Model';
            $context[$key] = $this->load( 'model', $model_class )->getSettings();
        }
        return $context;
    }

    private function _initializeWidgets()
    {
        if ( !self::$widgets_initialized )
        {
            self::$widgets = $this::containerize( $this->getConfig() );
            \mopsyd\sanctity\libs\wordpress\widget\AbstractWidget::setModel( $this );
            self::$widgets_initialized = true;
        }
    }

}
