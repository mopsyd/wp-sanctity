<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\integration;

/**
 * Customizer Integration Model
 * Handles Sanctity integrations with the wordpress customizer.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class CustomizerModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'plugins';

    private $customizer;

    public function __construct( $customizer = null, $args = array() )
    {
        if ( !is_null( $customizer ) )
        {
            if ( is_object( $customizer ) && ($customizer instanceof \WP_Customize_Manager) )
            {
                $this->setCustomizer( $customizer );
            } elseif ( is_array( $customizer ) && array_key_exists( 'customizer',
                    $customizer ) )
            {
                if ( !(is_object( $customizer['customizer'] )
                    && ( $customizer['customizer'] instanceof \WP_Customize_Manager ) ) )
                {
                    throw new \mopsyd\sanctity\libs\exception\SanctityException(
                    sprintf( 'Invalid customizer passed in [%1$s]. Expected instance of [%2$s].',
                        get_class( $this ), 'WP_Customize_Manager' ) );
                }
                $this->setCustomizer( $customizer['customizer'] );
            }
        }
        parent::__construct();
    }

    public function setCustomizer( \WP_Customize_Manager $customizer )
    {
        $this->customizer = $customizer;
    }

    public function getCustomizer()
    {
        return $this->customizer;
    }

    private function _checkCustomizerCompatible( $settings )
    {
        return true;
    }

    private function _getSettingArray( array $settings )
    {
        $defaults = $this->_getCustomizerSettingDefaults();
    }

    private function _getCustomizerSettingDefaults()
    {
        return array(
            'type' => 'theme_mod', // or 'option'
            'capability' => 'edit_theme_options',
            'theme_supports' => '', // Rarely needed.
            'default' => '',
            'transport' => 'refresh', // or postMessage
            'sanitize_callback' => '',
            'sanitize_js_callback' => '', // Basically to_json.
        );
    }

}
