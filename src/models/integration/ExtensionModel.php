<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\integration;

/**
 * Extension Integration Model
 * Handles Sanctity integrations with extension classes
 * registered prior to initialization.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class ExtensionModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'extensions';

    private static $autoloader;

    /**
     * The active sanctity object.
     * @var \Sanctity
     */
    private static $sanctity;

    /**
     * All currently registered extensions.
     * @var array
     */
    private static $extensions = array();

    /**
     * If this value is false, extensions have already been parsed,
     * and cannot be added after the fact.
     *
     * @var bool
     */
    private static $extension_registration_open = true;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * The controller uses this to close registration when
     * the real system bootstrap begins, so no further
     * extension registration can occur.
     * @return void
     */
    public function closeRegistration()
    {
        self::$extension_registration_open = false;
    }

    public function initializeExtensions()
    {
        $this->_initializeExtensions();
    }

    public function registerExtension( \mopsyd\sanctity\interfaces\libs\extension\ExtensionInterface $extension )
    {
        if ( array_key_exists( $extension::getPackageNamespace(),
                self::$extensions ) )
        {
            // Already registered
            return false;
        }
        self::_verifyExtension( $extension );
        self::_registerPackageNamespaceAutoload( $extension );
        self::_registerExtension( $extension );
        return true;
    }

    public static function setSanctity( \Sanctity $sanctity )
    {
        if ( !is_null( self::$sanctity ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'The Sanctity root object has already been defined in [%1$s]',
                get_called_class()
            ) );
        }
        self::$sanctity = $sanctity;
    }

    public static function setAutoloader( \mopsyd\sanctity\libs\autoload\Autoload $autoload )
    {
        self::$autoloader = $autoload;
    }

    /**
     * Registers an individual extension into the package namespace,
     * obtains its asset declarations, and dependency injects the
     * extension so it can initialize.
     *
     * @param string $package The package root namespace.
     * @param \mopsyd\sanctity\interfaces\libs\extension\ExtensionInterface $extension The extension
     * @return void
     * @internal
     */
    private static function _registerExtension( $extension )
    {
        self::_registerExtensionActions( $extension );
        self::_registerExtensionComponents( $extension );
        self::_registerExtensionConfigs( $extension );
        self::_registerExtensionControllers( $extension );
        self::_registerExtensionFactories( $extension );
        self::_registerExtensionFilters( $extension );
        self::_registerExtensionLayouts( $extension );
        self::_registerExtensionLibs( $extension );
        self::_registerExtensionModels( $extension );
        self::_registerExtensionTemplates( $extension );
        self::_registerExtensionViews( $extension );
        self::_packageExtension( $extension );
        self::$extensions[$extension::getPackageNamespace()] = $extension;
    }

    /**
     * Initializes a new package namespace if it has not already been added.
     *
     * The package namespace will also be added to the baseline factories,
     * so it will be listed as a valid source location, and registered with
     * the Psr4 autoloader.
     *
     * @param string $package
     * @param \mopsyd\sanctity\interfaces\libs\extension\ExtensionInterface $extension The extension
     * @return void
     * @internal
     */
    private static function _initializePackageNamespace( $extension )
    {
        //Do not double-register package namespaces
        if ( !array_key_exists( $extension::EXTENSION_PACKAGE, self::$extensions ) )
        {
            self::_registerPackageNamespaceAutoload( $extension );
            self::$extensions = array(
                $extension::EXTENSION_PACKAGE => $extension ) + self::$extensions;
            \mopsyd\sanctity\factory\FactoryFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\ExtensionFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\LibraryFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\ControllerFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\ModelFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\ViewFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
            \mopsyd\sanctity\factory\RouterFactory::registerNamespacePrefix( $extension::getPackageNamespace() );
        }
    }

    /**
     * Adds the package namespace to the Psr-4 autoloader.
     *
     * @param \mopsyd\sanctity\interfaces\libs\extension\ExtensionInterface $extension The extension
     * @return void
     * @internal
     */
    private static function _registerPackageNamespaceAutoload( $extension )
    {
        $package = $extension::getPackageNamespace();
        $path = $extension::getPath();
        self::$autoloader->addNamespace( $package, $path );
    }

    private static function _verifyExtension( $extension )
    {
        if ( !self::$extension_registration_open )
        {
            throw new \mopsyd\sanctity\libs\exception\DoingItWrongException(
            sprintf( 'Sanctity Extension [%1$s] could not be registered, '
                . 'because extension registration has already been closed. '
                . 'It must be registered on an earlier action.',
                get_class( $extension ) )
            );
        }
    }

    private static function _registerExtensionComponents( $extension )
    {
        $components = $extension->getComponents();
        if (!$components)
        {
            return;
        }
        d( $extension, $components ); exit;
    }

    private static function _registerExtensionControllers( $extension )
    {
        $controllers = $extension->getControllers();
        if (!$controllers)
        {
            return;
        }
        d( $extension, $controllers ); exit;
    }

    private static function _registerExtensionModels( $extension )
    {
        $models = $extension->getModels();
        if (!$models)
        {
            return;
        }
        d( $extension, $models ); exit;
    }

    private static function _registerExtensionViews( $extension )
    {
        $views = $extension->getViews();
        if (!$views)
        {
            return;
        }
        d( $extension, $views ); exit;
    }

    private static function _registerExtensionLibs( $extension )
    {
        $libs = $extension->getLibs();
        if (!$libs)
        {
            return;
        }
        d( $extension, $libs ); exit;
    }

    private static function _registerExtensionFactories( $extension )
    {
        $factories = $extension->getFactories();
        if (!$factories)
        {
            return;
        }
        d( $extension, $factories ); exit;
    }

    private static function _registerExtensionTemplates( $extension )
    {
        $templates = $extension->getTemplates();
        if (!$templates)
        {
            return;
        }
        d( $extension, $templates ); exit;
    }

    private static function _registerExtensionActions( $extension )
    {
        $actions = $extension->getActions();
        if (!$actions)
        {
            return;
        }
        d( $extension, $actions ); exit;
    }

    private static function _registerExtensionFilters( $extension )
    {
        $filters = $extension->getFilters();
        if (!$filters)
        {
            return;
        }
        d( $extension, $filters ); exit;
    }

    private static function _registerExtensionLayouts( $extension )
    {
        $layouts = $extension->getLayouts();
        if (!$layouts)
        {
            return;
        }
        d( $extension, $layouts ); exit;
    }

    private static function _registerExtensionConfigs( $extension )
    {
        $configs = $extension->getConfigs();
        if (!$configs)
        {
            return;
        }
        d( $extension, $configs ); exit;
    }

    private static function _packageExtension( $extension )
    {
        $extension->register( self::$sanctity );
    }

    private function _initializeExtensions()
    {

    }

}
