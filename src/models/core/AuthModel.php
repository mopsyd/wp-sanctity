<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\core;

/**
 * Auth Model
 * Handles authorization, using both the Wordpress ACL as well as
 * internal logic to harden and extend upon the native approach.
 *
 * This model allows binding additional external ACL logic to Wordpress,
 * and presents the opportunity to operate with a more granular ACL layer.
 * For security purposes, the ACL of Wordpress may not be surpassed, and
 * any internal permission that denies usage rights will be honored,
 * regardless of any bindings in this class.
 *
 * This class does however present an opportunity to extend the Wordpress ACL
 * using native logic it understands, and do so with an easy to use api that
 * allows for greater flexibility, and more contextual authorization than the
 * native ACL would permit.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class AuthModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'auth';

    private static $auth_data;

    /**
     * The default user object,
     * which corresponds to the
     * currently logged in user.
     *
     * @var \WP_User
     */
    private static $default_user;

    /**
     * This is used internally, and is always
     * only scoped to the current site.
     * @var \WP_Roles
     */
    private static $roles;

    /**
     * The currently scoped user object.
     * @var \WP_User
     */
    private $user;

    /**
     * Auth Model Constructor
     *
     * By default, this object pre-initializes scoped to the current user.
     * It may be scoped to a different user by calling `loadUser`.
     *
     * A copy of the object may be obtained scoped to another user
     * without disrupting the state of the current object by using
     * the `withUser` method.
     */
    public function __construct()
    {
        $this->_initializeUser();
        parent::__construct();
    }

    public function getSettings()
    {
        if ( is_array( self::$auth_data ) )
        {
            return $this::containerize( self::$auth_data );
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = array_replace_recursive( $settings,
            $this->_getAuthDataDefaults() );
        $settings['options'] = $this->getOptions();
        self::$auth_data = $settings;
        return $this::containerize( $settings );
    }

    /**
     * Scopes the current model instance to the supplied user.
     *
     * The user may be passed by id, email, username,
     * or any other valid identifying criteria for a valid
     * user of the current site.
     *
     * Acceptable user parameters are id, username for the site, email,
     * a \WP_User object, or a \WP_Role object
     *
     * @param int|string|\WP_User|\WP_Role $user
     */
    public function loadUser( $user )
    {
        if ( is_class( $user ) && ($user instanceof \WP_Roles) )
        {
            //Do not allow global editing of roles from this theme.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Class [%2$s] may not be used to edit roles globally.',
                get_class( $this ) ) );
        }
        //Single Roles are ok, as long as they aren't the Administrator
        if ( is_class( $user ) && ($user instanceof \WP_Role ) )
        {
            if ( in_array( $class->name,
                    array(
                    'administrator',
                    'super_administrator' ) ) )
            {
                //Do not allow editing admins or super admins from this theme.
                $this->_securityNotifyAdmin( $message, debug_backtrace( 1 ) );
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Class [%2$s] may not be used to edit the administrator  or superadminroles.',
                    get_class( $this ) ) );
            }
        }
        $this->user = $this->_loadUser( $user );
    }

    /**
     * Returns an instance of the AuthModel scoped to the given user,
     * without changing its own internal user. A copy is returned,
     * so the operating logic can obtain both, or retain the original
     * in an unchanged state.
     *
     * Acceptable user parameters are id, username for the site, email,
     * a \WP_User object, or a \WP_Role object
     *
     * @param int|string|\WP_User|\WP_Role $user
     * @return \mopsyd\sanctity\models\core\AuthModel
     */
    public function withUser( $user )
    {
        $model = $this::init();
        $model->loadUser( $user );
        return $model;
    }

    /**
     * Returns a new copy of the AuthModel scoped to the current user
     *
     * @return \mopsyd\sanctity\models\core\AuthModel
     */
    public function withCurrentUser()
    {
        return $this::init();
    }

    /**
     * Returns the currently held user object
     *
     * @return \WP_User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Checks if the currently scoped user has the supplied permission.
     *
     * @param string $permission A valid Wordpress permission.
     *     Custom permissions registered by plugins are acceptable,
     *     provided that they have been registered at the time that
     *     the call is made.
     * @return bool
     */
    public function check( $permission )
    {
        if ( !$this->user )
        {
            return false;
        }
        foreach ( $this->user->role as
            $role )
        {
            if ( $role->permissions->has( $permission ) )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Grants a permission to a user or group.
     * @param string $permission
     * @return void
     */
    public function grant( $permission )
    {
        $this->user->add_cap( $permission );
    }

    /**
     * Revokes a permission from a user or group.
     * @param string $permission
     * @return void
     */
    public function revoke( $permission )
    {
        if ( ( ($this->user instanceof \WP_Role)
            && ( in_array( $this->user->name,
                array(
                'administrator',
                'super_administrator' ) )
            || in_array( 'super_administrator', $this->user->roles ) ) )
            || ( ($this->user instanceof \WP_User)
            && ( in_array( 'administrator', $this->user->roles )
            || in_array( 'super_administrator', $this->user->roles ) ) ) )
        {
            //I will not be held responsible for people destroying
            //their access rights on their website. Nope.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Class [%1$s] may not be used to revoke privileges from '
                . 'administrators or superadmins.', get_class( $this ) ) );
        }
        $this->user->remove_cap( $permission );
    }

    private function _getAuthDataDefaults()
    {
        d( $this->user->add_cap( $permission ) );
        $perms_array = $this->user->get_role_caps();
        $data = array(
            'type' => (get_class( $this->user ) === 'WP_User'
            ? 'user'
            : get_class( $this->user ) === 'WP_Role'
            ? 'role'
            : null),
            'id' => $this->user->ID,
            'site_id' => $this->user->site_id,
            'roles' => $this->user->roles,
            'permissions' => $this->user->get_role_caps(),
        );
        return $data;
    }

    private function _initializeUser()
    {
        if ( is_null( self::$default_user ) )
        {
            self::$default_user = $this->_loadUser();
        }
        $this->user = self::$default_user;
    }

    private function _loadUser( $user = null )
    {
        $model = $this->load( 'model', 'display\\UserModel' );
        $user = $model->getSettings();
        return $user;
    }

}
