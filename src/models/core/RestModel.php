<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\core;

/**
 * Rest Model
 * Handles Sanctity inbound and outbound rest calls, as well as aggregation
 * of rest endpoints into an organized set with matching api keys, tokens, etc.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class RestModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'rest';
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\EndpointContainer';
    const COLLECTION_CLASS = 'container\\EndpointCollection';

    private static $rest_endpoints;
    private static $rest_data;
    private static $rest_initialized = false;
    private static $api_keys;
    private static $request_endpoints;
    private static $response_endpoints;

    public function __construct()
    {
        $this->_initializeRest();
        parent::__construct();
    }

    /**
     * Sends a request out over the rest client, and returns the response.
     *
     * If the api or api method are not known, an exception will be raised.
     * Likewise the attempt will be halted prior to outbound communication
     * if any known required keys are not provided, or if any data for the
     * outbound request is known to be malformed.
     *
     * @param string $api The name of the api that contains the request method
     * @param string $method The name of the request method within the api
     * @param type $args The arguments for the outbound request
     * @return mixed
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the referenced api or method are not known
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any data in the request arguments is malformed
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any data is missing from the request arguments
     */
    public function send( $api, $method, $args = array() )
    {
//        d($api, $method, $args);
        $endpoints = $this->getEndpoints('request');
        $api_group = $endpoints->get($api);
        $endpoint = $api_group->get($method);
        $client = $this->load('library', 'rest\\RestClient');
        $client->package($endpoint, $args);
        $client->send();
        $result = $client->getResponse();
        $client->reset();
//        d($result, $api, $client, $client->getState(), $endpoint, $args, $api_group, $endpoint); exit;
        return $result;
    }

    public function handle( $api, $method, $args )
    {

    }

    public function getEndpoints( $type )
    {
        return $this->_buildEndpoints( $type );
    }

    public function getSettings()
    {
        if ( is_array( self::$data_data ) )
        {
            return $this::containerize( self::$data_data ); //datadatadatadatadatadatadatadatalol
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = array_replace_recursive( $settings,
            $this->_getDataDefaults() );
        $settings['options'] = $this->getOptions();
        self::$rest_data = $settings;
        return $this::containerize( $settings );
    }

    private function _getDataDefaults()
    {
        return array();
    }

    private function _initializeRest()
    {
        if ( !self::$rest_initialized )
        {
            $config = $this->load( 'library', 'config\\RestConfig' );
            self::$rest_endpoints = $config;
            self::$rest_initialized = true;
        }
    }

    /**
     * Builds the parsed set of endpoints that is suitable for containerization.
     *
     * @param string $type ["request", "response"]
     * @return \mopsyd\sanctity\interfaces\libs\container\EndpointCollectionInterface
     */
    private function _buildEndpoints( $type )
    {
        $endpoints_raw = $this->_getEndpointRaw( $type );
        $result = $this->load( 'library', $this::COLLECTION_CLASS);
        foreach ( $endpoints_raw as
            $api =>
            $details )
        {
            $res = $this->load( 'library', $this::COLLECTION_CLASS );
            if ( (is_object( $details )
                && ( $details instanceof \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface )
                && ( $details->has( 'api' ) ) ) )
            {
                $details = $details->toArray();
            }
            if ( (is_array( $details ) && array_key_exists( 'api', $details ) ) )
            {
                foreach ( $details['api'] as
                    $key =>
                    $remote )
                {
                    $res[$key] = $this->_getParsedEndpoint( $key, $details );
                }
            }
            $result[$api] = $res;
        }
        return $result;
    }

    /**
     * Fetches the raw unparsed endpoint configuration data
     * for the requested endpoint type.
     *
     * @param string $type Either "request" (for rest client endpoints) or "response" (for test server endpoints).
     * @return array
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Only "request" or "response" are valid arguments for $type
     * @internal
     */
    private function _getEndpointRaw( $type )
    {
        $valid = array(
            'request',
            'response' );
        $handle = ($type === 'request')
            ? 'client'
            : ($type === 'response'
            ? 'server'
            : false);
        if ( !$handle )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Unknown endpoint type [%2$s]. '
                . 'Valid endpoint types are [%3$s]', get_class( $this ), $type,
                implode( ', ', $valid ) )
            );
        }
        $endpoints = self::$rest_endpoints->get( $handle );
        return $endpoints;
    }

    private function _getParsedEndpoint( $handle, $endpoints )
    {
        $target = $endpoints['api'][$handle];
        $protocol = $endpoints['protocol'];
        if ( is_array( $endpoints['protocol'] ) )
        {
            // This means it accepts both http and https.
            // Use the same schema as the current server in that case.
            $protocol = substr( $this::HTTP_HOST, 0,
                strpos( $this::HTTP_HOST, ':' ) );
        }
        if ( is_null( $endpoints['host'] ) )
        {
            // This is a variant point api, which means it can be offered by
            // multiple hosts. We will append a required host field to the
            // details in this case, which indicates that the api usage
            // requires that a host be provided as part of the regular
            // api set to utilize this functionality.
            $endpoints['host'] = '{host}';
            $target['required']['host'] = array(
                'type' => 'string',
                'default' => $this::HTTP_HOST,
                'accept' => null
            );
        }
        $host = trim( $endpoints['host'], '/' );
        $raw_uri = $protocol . '://' . $host . $target['route'];
        $details = array(
            'uri' => $raw_uri,
            'method' => $target['method'],
            'headers' => array(),
            'protocol' => $protocol,
            'host' => $host,
            'method' => $target['method'],
            'auth' => $target['auth'],
            'cross-origin' => $endpoints['cross-origin'],
            'uri' => $raw_uri,
            'port' => ( $protocol === 'https'
            ? 443
            : 80),
            'format' => $endpoints['output'],
            'required' => $target['required'],
            'optional' => $target['optional']
//            'callback' => $this->getRequestCallback(),    // Provide with args
//            'key' => $this->getApplicationKey(),          // Provide with args
//            'secret' => $this->getApplicationSecret(),    // Provide with args
//            'application_id' => $this->getApplicationId() // Provide with args
        );
        return $this::containerize( $details );
    }

}
