<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\core;

/**
 * Install Model
 * Handles Sanctity installation and uninstallation.
 *
 * This installation bypasses the typical dbDelta operation,
 * because it sets specific column encodings and relations that
 * Wordpress doesn't do. It does not create any foreign key constraints
 * on any of Wordpress's existing tables, but it does create a few on
 * its own to some Wordpress tables. As the Wordpress database pretty
 * much doesn't use any existing relations at all, there's a little
 * bit of voodoo that we have (on our own tables, not anyone elses)
 * to do here to insure that this operates correctly.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class InstallModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'install';

    /**
     * The SQL install file prefixes all tables with this value,
     * so that string replace with the wp prefix can be done consistently.
     * @var string
     */
    private $install_prefix = '_sanctity';

    /**
     * The parsed SQL used for installation.
     * @var string
     */
    private $install_database_source;

    /**
     * The report of whether installation was successful or not
     * @var array
     */
    private $install_report = array();

    /**
     * The report of whether uninstallation was successful or not
     * @var array
     */
    private $uninstall_report = array();

    /**
     * Designates that Sanctity is already installed.
     * @var bool
     */
    private static $is_installed = false;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Checks if Sanctity is already installed.
     * @return bool
     */
    public function checkInstall()
    {
        return $this->_checkInstallation();
    }

    /**
     * Performs the installation process.
     * @return void
     */
    public function install()
    {
        $this->_checkInstallation();
        $this->_loadInstallSourceFile();
        $this->_formatInstallSourceFile();
        $this->_runInstaller();
    }

    /**
     * Returns a report on whether installation was successful.
     * @return array
     */
    public function getInstallReport()
    {
        return $this->install_report;
    }

    /**
     * Performs the uninstallation process.
     * @return void
     */
    public function uninstall()
    {
        //@todo
    }

    /**
     * Returns a report on whether the uninstallation was successful.
     */
    public function getUninstallReport()
    {
        return $this->uninstall_report;
    }

    /**
     * Don't install if an installation is already there.
     * @return void
     * @internal
     */
    private function _checkInstallation()
    {
        if ( self::$is_installed )
        {
            //it has already been determined that installation
            //occurred during this runtime. Do not check again.
        }
        //@todo run the check on the installation
        return self::$is_installed;
    }

    /**
     * Fetch the database file from the metadata directory.
     * @return void
     * @internal
     */
    private function _loadInstallSourceFile()
    {
        $file = SANCTITY_BASEDIR . 'config' . DIRECTORY_SEPARATOR . $this->getConfig( 'install-file' );
        if ( !file_exists( $file ) || !is_readable( $file ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Could not read the database installation source file'
                . ' at expected location [%1$s].', $file ) );
        }
        $this->install_database_source = file_get_contents( $file );
    }

    /**
     * Runs any required filters on the install file to
     * format it correctly for the current database environment.
     * @return void
     * @internal
     */
    private function _formatInstallSourceFile()
    {
        $filters = $this->_getInstallSourceFilters();
        foreach ( $filters as
            $method )
        {
            $this->install_database_source = $this->$method( $this->install_database_source );
        }
    }

    /**
     * Returns a list of methods that filter the installation SQL correctly.
     * Any such methods will be prefixed with _filterInstall as the first
     * part of their method name.
     *
     * These MUST be able to run in any order without issue.
     * This means that all install filters MUST fire exactly
     * as expected with no ambiguity whatsoever.
     *
     * @return array
     * @internal
     */
    private function _getInstallSourceFilters()
    {
        $methods = get_class_methods( $this );
        $filters = array();
        foreach ( $methods as
            $method )
        {
            if ( strpos( $method, '_filterInstall' ) === 0 )
            {
                $filters[] = $method;
            }
        }
        return $filters;
    }

    /**
     * Runs the installation.
     *
     * @return void
     * @internal
     */
    private function _runInstaller()
    {
        try
        {
            $stmt = false;
            $queries = $this->_splitSqlDumpFile( $this->install_database_source );
            $db = $this->getDatabase();
            $db->beginTransaction();
            foreach ( $queries as
                $raw )
            {
                $sql = trim( $raw, " \t\n\r\0\x0B" );
                if ( strlen( $sql ) > 0 )
                {
                    $db->exec( $sql . ';' );
                }
                $this->_updateInstallReport( array(
                    'operation' => 'Running SQL: ' . $sql,
                    'info' => $db->errorInfo(),
                    'code' => $db->errorCode(),
                ) );
            }
            $db->commit();
            $this->_updateInstallReport( array(
                'operation' => 'Database import successful',
                'debug' => 'All SQL operations successful, changes committed.',
                'info' => $db->errorInfo(),
                'code' => $db->errorCode(),
            ) );
            return;
        } catch ( \PDOException $e )
        {
            d( $e->getMessage(), $e, $sql );
            $this->_finalizeInstallReport( array(
                'operation' => 'Installation Failed.',
                'debug' => 'Error in SQL processing, changes rolled back.',
                'info' => $e->getMessage(),
                'code' => $e->getCode(),
            ) );
            $db->rollBack();
            exit();
        }
    }

    private function _updateInstallReport( $statement )
    {
        $this->install_report[] = $statement;
    }

    private function _finalizeInstallReport( $statement )
    {
        $this->install_report[] = $statement;
    }

    /**
     * Formats the installer SQL to prefix the column, index, and
     * relational bindings with the database prefix.
     * @param string $source Unformatted SQL
     * @return string Formatted SQL
     * @internal
     */
    private function _filterInstallWordpressPrefix( $source )
    {
        $db = $this->getDatabase();
        if ( strlen( $db->prefix() ) - strrpos( $db->prefix(), '_' ) === 1 )
        {
            //The prefix ends in an underscore, so we'll trim ours,
            //so they aren't doubled (unless the prefix doubles them)
            $replace = $db->prefix() . ltrim( $this->install_prefix, '_' );
        } else
        {
            //leave the underscore separator alone
            $replace = $db->prefix() . $this->install_prefix;
        }
        return str_replace( $this->install_prefix, $replace, $source );
    }

    private function _splitSqlDumpFile( $source )
    {
        return explode( ';' . PHP_EOL, $source );
    }

}
