<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Branding Model
 * Determines what color profile, typography, logo,
 * and other identifying sylistic characteristics
 * of the site to display.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class BrandingModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'branding';

    private static $branding_data;

    public function __construct()
    {
        parent::__construct();
    }

    public function getSettings()
    {
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = array_replace_recursive( $settings,
            $this->_getBrandingDataDefaults() );
        self::$branding_data = $settings;
        $settings['options'] = $this->load( 'model', 'settings\\BrandingModel' )->getOptions();
        return $this::containerize( $settings );
    }

    private function _getBrandingDataDefaults()
    {
        $results = array(
            'enable_header_logo' => true,
            'enable_header_title' => true,
            'enable_header_tagline' => true,
            'enable_header_identity' => true,
            'enable_background_color' => true,
            'enable_banner' => true,
            'enable_background' => true,
            'enable_background_color' => true,
            'logo' => $this->_getBrandingLogo(),
            'body-class' => $this::getAdapter()->get( 'site', 'body-class' ),
            'title' => $this::getAdapter()->get( 'site', 'name' ),
            'tagline' => $this::getAdapter()->get( 'site', 'description' ),
            'banner' => $this::getAdapter()->get( 'site', 'banner' ),
            'background' => $this::getAdapter()->get( 'site', 'background' ),
            'background_color' => $this::getAdapter()->get( 'site', 'background-color' ),
            'url' => $this::getAdapter()->get( 'site', 'url' ),
            'email' => $this::getAdapter()->get( 'site', 'email' ),
            'theme-support' => $this->_getBrandingThemeSupport(),
            'social' => $this->load('model', 'settings\\BrandingModel')->getSocial()
        );
        if ( !$results['logo'] )
        {
            $results['enable_header_logo'] = false;
        }
        if ( !$results['banner'] )
        {
            $results['enable_banner'] = false;
        }
        if ( !$results['background'] )
        {
            $results['enable_background'] = false;
        }
        if ( !$results['background_color'] )
        {
            $results['enable_background_color'] = false;
        }
        return $results;
    }

    private function _getBrandingThemeSupport()
    {
        return array(
            'custom-background' => true,
            'custom-logo' => array(
                'height' => 50,
                'width' => 50,
                'flex-height' => true,
                'flex-width' => true,
                'header-text' => array(
                    'site-title',
                    'site-description' ),
            )
        );
    }

    /**
     * Fetches the logo.
     *
     * This method is used in place of a direct call to allow for future extension.
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @internal
     */
    private function _getBrandingLogo()
    {
        $logo = $this::getAdapter()->get( 'logo' );
        return $logo;
    }

}
