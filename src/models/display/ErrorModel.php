<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Error Model
 * Handles Sanctity static error pages, configurable on the Settings tab of the admin page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class ErrorModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'error';

    private static $error_defaults = array(
        401 => array(
            'type' => 401,
            'title' => 'Not Authorized',
            'heading' => 'Incorrect Authorization',
            'message' => 'You probably wish you wrote that password down somewhere now, don\'t you?.',
            'error_template' => 'errors/401.twig',
        ),
        403 => array(
            'type' => 403,
            'title' => 'Forbidden',
            'heading' => 'All Kinds of Nope',
            'message' => 'You didn\'t think you could just stroll on in here without being invided now did you?',
            'error_template' => 'errors/403.twig',
        ),
        404 => array(
            'type' => 404,
            'title' => 'Page not found',
            'heading' => 'Page Not Found',
            'message' => 'Nothing to see here folks.',
            'error_template' => 'errors/404.twig',
        ),
        500 => array(
            'type' => 500,
            'title' => 'Server Error',
            'heading' => 'Looks Like Something Broke',
            'message' => 'We\'ll take a look into that and see what happened.',
            'error_template' => 'errors/500.twig',
        ),
    );
    private $type = 404;
    private $details;

    public function __construct()
    {
        parent::__construct();
    }

    public function initialize( $arguments, $details = null )
    {
        $this->type = $arguments;
        $this->details = $details;
    }

    /**
     * Returns the settings scoped under the current model.
     *
     * All models have this method. Models dedicated to settings specifically
     * provide additional support methods for configuring their options.
     *
     * @return bool|array
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If a model extending this class does not set its scope constant,
     *     this exception will be thrown. This indicates that the class is
     *     misconfigured, and the exception should not be caught.
     */
    public function getSettings()
    {
        $settings = parent::getSettings();
        if ( !$settings )
        {
            return $this::containerize( $this->_getDefaultErrorSettings() );
        }
        $settings['template'] = $this->getTemplate();
        return $this::containerize( $settings );
    }

    protected function getDetails()
    {
        return $this->details;
    }

    private function _getDefaultErrorSettings()
    {
        $defaults = self::$error_defaults[$this->type];
        $defaults['error'] = true;
        $defaults['error_details'] = $this->_handleErrorDetails();
        return $defaults;
    }

    public function getTemplate()
    {
        return 'base.twig';
    }

    /**
     * Override this method to return an array of error templates keyed
     * to http status codes. The current status code will be passed into
     * the method to assist with this determination if needed.
     *
     * @param type $type
     * @return type
     */
    protected function declareErrorTemplates( $type )
    {
        return array();
    }

    /**
     * Override this method to supply custom output details for a given status code.
     *
     * The supplied response MUST contain at least the following keys:
     *
     * - "title" - Gets printed in the HTML head
     * - "heading" - Gets printed in the h1 element of the error template
     * - "message" - Gets printed in the context body element of the error template, if one exists
     *
     * The status code will also be injected into this response,
     * there is no need to supply one. It will be overridden if you do anyhow.
     *
     * Aside from that, you may also supply any additional contextual data that
     * you want to be available in error templating. System defaults will be
     * replaced only if they exist, so you only need to supply what you want
     * to override, or what is required by custom template logic.
     *
     * @param int $type An http status code
     * @return array
     */
    protected function declareErrorPageDetails( $type )
    {
        return array();
    }

    private function _handleErrorDetails()
    {
        return $this->getDetails();
    }

    /**
     * Returns the error template for the given status code type.
     * @param type $type
     * @param type $message
     * @return string
     */
    private function _getErrorTemplate( $type, $message = null )
    {
        $custom = $this->declareErrorTemplates( $type );
        if ( array_key_exists( $type, $custom ) )
        {
            return $custom[$type];
        }
        switch ( $type )
        {
            case 401:
                //Instances in which authorization fails show this template
                return 'errors/401.twig';
                break;
            case 403:
                //Instances in which a user is not allowed access
                //and cannot be granted access show this template
                return 'errors/403.twig';
                break;
            case 404:
                //Instances in which the page is not found show this template
                return 'errors/404.twig';
                break;
            case 500:
                //All server errors display this template
                return 'errors/500.twig';
                break;
            default:
                //All undefined errors are assumed to be 500 errors if not otherwise defined.
                return $this->_getErrorTemplate( 500, $message );
                break;
        }
    }

    /**
     * Recieves the error type, and returns an array of default error details
     * containing the following keys:
     *
     * - "title" - Gets printed in the HTML head
     * - "heading" - Gets printed in the h1 element of the error template
     * - "message" - Gets printed in the context body element of the error template, if one exists
     *
     * The status code will also be injected into this response,
     * there is no need to supply one. It will be overridden if you do anyhow.
     *
     * @param type $type
     * @return array
     * @internal
     */
    private function _getErrorPageDetails( $type )
    {
        $custom = $this->declareErrorPageDetails( $type );
        if ( array_key_exists( $type, self::$error_defaults ) )
        {
            return array_replace( self::$error_defaults[$type], $custom );
        }
        if ( empty( $custom ) )
        {
            //Server error if the code has no definition, but this must be non-blocking,
            //as we've already arrived at an error point.
            return self::$error_defaults[500];
        }
        return $custom;
    }

}
