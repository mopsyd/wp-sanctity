<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Theme Model
 * Gets information about the current theme,
 * regardless of whether it is Sanctity or
 * a child theme.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class ThemeModel
extends \mopsyd\sanctity\models\AbstractModel
{
    const MODEL_SETTINGS_KEY = 'theme';

    private static $theme_data;

    public function __construct(){
        parent::__construct();
    }

    public function getSettings()
    {
        if (is_array(self::$theme_data))
        {
            return self::$theme_data;
        }
        $settings = parent::getSettings();
        if (!$settings)
        {
            $settings = array();
        }
        $settings = $this::containerize( $this->_getThemeDataDefaults() );
        $settings['options'] = $this->getOptions();
        self::$theme_data = $settings;
        return $settings;
    }

    /**
     *
     * @return array
     * @internal
     */
    private function _getThemeDataDefaults()
    {
        return array();
    }

}
