<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Archive Model
 * Handles data about the post archive.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class PostModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'posts';

    private static $posts_data;
    private static $post_types;

    public function __construct()
    {
        $this->_initializePosts();
        parent::__construct();
    }

    public function getSettings()
    {
        if ( is_array( self::$posts_data ) )
        {
            return $this::containerize( self::$posts_data );
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = $this->_getPostsDataDefaults(); // Currently defers strictly to the adapter.
        self::$posts_data = $settings;
        return $this::containerize( $settings );
    }

    /**
     * Returns posts by a standard wordpress query
     *
     * @param array $query
     */
    public function query( $query )
    {

    }

    private function _getPostsDataDefaults()
    {
        global $wp_query;
        $result = $this::getAdapter()->aggregate('post', $wp_query );
        return $result;
    }

    private function _getDefaultQuery()
    {
        $auth = $this->load( 'model', 'core\\AuthModel' ); //Do some user auth stuff to handle logged in user, visibility, and all that jazz once we get this fleshed out a bit more.
        $posts = get_post_types(); //Also do some stuff with checking the post types against the current page and other complicated model goodness when we get this fleshed out a bit more.
        $post_status = array(
            'publish' );
        if ( $auth->check( 'publish_posts' ) )
        {
            $post_status[] = 'publish';
        }
        if ( $auth->check( 'read_private_posts' ) )
        {
            $post_status[] = 'private';
        }
        $query = array(
                'post_type' => 'post',
                'post_status' => $post_status
                );
        return $query;
    }

    private function _initializePosts()
    {
        if ( is_null( self::$post_types ) )
        {
            self::$post_types = get_post_types();
        }
    }

}
