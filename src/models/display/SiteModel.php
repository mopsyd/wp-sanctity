<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Site Model
 * Gets information about the current site and the environment it is loaded on.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class SiteModel
extends \mopsyd\sanctity\models\AbstractModel
{
    const MODEL_SETTINGS_KEY = 'site';

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\SiteContainer';

    private static $site_data;

    public function __construct(){
        parent::__construct();
    }

    public function getSettings()
    {
        if (is_array(self::$site_data))
        {
            return self::$site_data;
        }
        $settings = parent::getSettings();
        if (!$settings)
        {
            $settings = array();
        }
        $settings = $this::containerize( $this->_getSiteDataDefaults() );
        $settings['options'] = $this->getOptions();
        self::$site_data = $settings;
        return $this::containerize($settings);
    }

    private function _getSiteDataDefaults()
    {
        return array(
            'id' => get_current_blog_id(),
            'url' => trailingslashit( home_url() ),
            'uri' => $this::REQUEST_URI,
            'request-method' => $this::REQUEST_METHOD,
            'runtime-fingerprint' => $this::RUNTIME_FINGERPRINT,
            'multisite' => is_multisite(),
            'name' => get_bloginfo('name'),
            'description' => get_bloginfo('description'),
            'admin-email' => get_bloginfo('admin_email'),
            'charset' => get_bloginfo('charset'),
            'wordpress-version' => get_bloginfo('version'),
            'current-plugins' => get_plugins(),
            'html-type' => get_bloginfo('html_type'),
            'language' => get_bloginfo('language'),
            'theme-directory' => get_stylesheet_directory() . DIRECTORY_SEPARATOR,
            'theme-url' => substr( get_bloginfo('stylesheet_url'), 0, strpos( get_bloginfo('stylesheet_url'), 'style.css') ),
            'parent-theme-directory' => get_template_directory() . DIRECTORY_SEPARATOR,
            'parent-theme-url' => get_bloginfo('template_url') . '/',
            'pingback' => get_bloginfo('pingback_url'),
            'atom' => get_bloginfo('atom_url'),
            'rdf' => get_bloginfo('rdf_url'),
            'rss' => get_bloginfo('rss_url'),
            'rss2' => get_bloginfo('rss2_url'),
            'comments-atom' => get_bloginfo('comments_atom_url'),
            'comments-rss2' => get_bloginfo('comments_rss2_url'),
            'php' => PHP_VERSION_ID,
            'debug-enabled' => defined('WP_DEBUG') && WP_DEBUG,
            'site-root' => ABSPATH,
            'ci' => getenv('CI')
        );
    }

}
