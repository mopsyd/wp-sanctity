<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Menu Model
 * Determines What the menu layout ought to look like.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class MenuModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'menu';

    private static $menu_data;

    public function __construct()
    {
        parent::__construct();
    }

    public function register()
    {
        $layout = $this->load( 'model', 'display\\LayoutModel' );
        $layout->setScope( $this->getScope() );
        $settings = $layout->getSettings();
        $menus = $settings->get( $this::MODEL_SETTINGS_KEY );
        foreach ( $menus as
            $menu )
        {
            $this::getAdapter()->register( $this::MODEL_SETTINGS_KEY, $menu );
        }
    }

    /**
     * Returns about the most robust set of info you
     * could ever want about a menu.
     *
     * This is all internally cached statically by pointers
     * the same way the underlying C engine that powers PHP does,
     * so you can slam the bloody hell
     * out of it with repeated requests without costing any
     * additional memory or processing time, or doing any
     * additional calls to internals or the database.
     *
     * Note that this will however give you a MASSIVE var dump,
     * which will probably run out of memory when you are debugging,
     * so there's that. It is strongly suggested to var dump
     * on a per-item basis instead of trying to dump the
     * whole menu collection. Either that or jack up the memory
     * on your dev server very high. Production does not need it though,
     * as long as you aren't `var_dump`'ing or `print_r`'ing
     * in production (you shouldn't be). The reason for this is
     * that debug functions detach the pointers and treat them like
     * unique independent elements, whereas the underlying code
     * processing them does not.
     * You can follow the internals in the adapter layer
     * if you want to see how it works.
     *
     * @return \mopsyd\sanctity\libs\container\Container
     */
    public function getSettings()
    {
        if ( is_array( self::$menu_data ) )
        {
            return self::$menu_data;
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = $this::containerize( $this->_getMenuDataDefaults() );
        self::$menu_data = $settings;
        return $settings;
    }

    /**
     * Returns a super awesome Menu container, which contains all of the nav
     * and term info, and each link also contains pretty much everything you
     * would ever want to know about it, what it points at, whoever wrote that,
     * and whatever else they wrote too.
     *
     * @note This build supports visibility for menu items, exactly the same
     *     way that regular post visibility works. This lets you set links as
     *     only visible by specific permission through some external logic,
     *     and they will not be printed into the menu if they are not visible
     *     to the current user. They are literally not even there in the source
     *     in that case, not just hidden.
     *
     * @return \array
     * @internal
     */
    private function _getMenuDataDefaults()
    {
        $layout = $this->load( 'model', 'display\\LayoutModel' );
        $layout->setScope( $this->getScope() );
        $settings = $layout->getSettings();
        $menus = $settings->get( $this::MODEL_SETTINGS_KEY );
        $result = array();
        foreach ( $menus as
            $key =>
            $menu )
        {
            try {
                $result[$menu['handle']] = $this::getAdapter()->parse( $this::MODEL_SETTINGS_KEY, $menu );
            } catch (\mopsyd\sanctity\libs\exception\SanctityException $e) {
                // This means that the menu handle does not have any menu assigned to it.
                $result[$menu['handle']] = false;
            }
        }
        return $result;
    }

}
