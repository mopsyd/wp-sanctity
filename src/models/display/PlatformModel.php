<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Platform Model
 * Determines how to handle events in accordance with the platform, and whether
 * to allow them as per standard functionality, or to bypass them and use a
 * customized approach.
 *
 * This model translates platform-specific terminology and event bindings
 * into a common format.
 *
 * If bypassed,they still get called and all of their hooks still fire,
 * but their echoed content is captured and manually injected into the
 * correct locations in your theme after being run through any custom
 * sanitization filters that are present. This presents an opportunity
 * to clean up the markup, remove duplicate scripts, minify javascript and css,
 * and disable rogue plugin scripts and styles on a case-by-case basis.
 * The end result of this is that you get a much cleaner, more performant,
 * and semantically correct page load, and have a much more stable
 * frontend display.
 *
 * If you are committed to doing things the wordpress way, turn these settings off.
 * If you want full control over your site, turn them on. They can be configured
 * through the Sanctity admin panel, there is no need to edit source here or
 * override it.
 *
 * @note This is going to get heavily refactored to be more generalized,
 *      and interact heavily with the Platform Adapter to accomplish most
 *      of its workload.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class PlatformModel
extends \mopsyd\sanctity\models\AbstractModel
{
    const MODEL_SETTINGS_KEY = 'platform';

    private static $platform_data;

    public function __construct(){
        parent::__construct();
    }

    public function getSettings()
    {
        if (is_array(self::$platform_data))
        {
            return self::$platform_data;
        }
        $settings = parent::getSettings();
        if (!$settings)
        {
            $settings = array();
        }
        $settings = array_replace_recursive($settings, $this->_getPlatformDataDefaults());
        $settings['options'] = $this->getOptions();
        self::$platform_data = $settings;
        return $settings;
    }

    /**
     * Returns the platform specific elements that should be controlled.
     *
     * @return array
     * @internal
     * @deprecated This needs to defer to the Platform Adapter to pull this,
     *      so the underlying model is entirely decoupled from direct platform
     *      representation.
     */
    private function _getPlatformDataDefaults()
    {
        return array(
            'header' => true,
            'footer' => true,
            'sidebar' => true,
            'content' => true,
        );
    }

}
