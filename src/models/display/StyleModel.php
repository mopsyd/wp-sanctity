<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Style Model
 * Determines stylistic considerations about the site that do not
 * alter page structure, but provide visual display.
 *
 * This differs from the Branding Model in that this information is not
 * explicitly used to establish site identity, but rather to determine
 * its visual composition.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class StyleModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'style';

    private static $style_data;

    private static $color_config;

    private static $style_initialized = false;

    public function __construct()
    {
        $this->_initializeStyleData();
        parent::__construct();
    }

    public function getSettings()
    {
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = array_replace_recursive( $settings,
            $this->_getStyleDataDefaults() );
        self::$style_data = $settings;
        $settings['options'] = $this->load( 'model', 'settings\\StyleModel' )->getOptions();
//        d($settings); exit;
        return $this::containerize( $settings );
    }

    private function _getStyleDataDefaults()
    {
        $results = array(
            'colors' => $this->_parseColors(),
        );

        return $results;
    }

    private function _parseColors()
    {
        $colors = array();
        foreach ( self::$color_config->get() as $key => $subset)
        {
            foreach ($subset as $section => $details)
            {
            $colors[$details['slug']] = $details['values'];

            }
        }
        return $colors;
    }

    private function _initializeStyleData()
    {
        if (!self::$style_initialized)
        {
            self::$color_config = $this->load('library', 'config\\ColorConfig');
            self::$style_initialized = true;
        }
    }

}
