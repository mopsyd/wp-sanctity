<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Layout Model
 * Determines which page layout to use within
 * the primary content section of the page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class LayoutModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    /**
     * Designates the localized scope of the model.
     * More than one model may share the same key, but they are tasked with
     * operating on different approaches to the same scope when that is the case.
     */
    const MODEL_SETTINGS_KEY = 'layouts';

    /**
     * Represents a the layout data for the currently represented layout.
     *
     * @var array
     */
    private $layout_data;

    /**
     * Represents a persistent reference to the current layout,
     * so new model instances after the first one loaded do not
     * need to make another database call to figure out what the
     * current layout is.
     *
     * @var string
     * @note There is no method in place to change this,
     *     and won't be until more layouts are completed.
     *     The default layout is currently only half done,
     *     so there's no point in overriding a half done layout,
     *     because it will all have to get refactored again anyhow.
     */
    private static $default_layout = 'default';

    /**
     * Represents the manifest settings file for the default layout.
     * This will be used as the baseline for all settings not defined
     * in other layouts.
     * @var array
     */
    private static $default_manifest;

    /**
     * Represents a list of any valid layout directories declared by this theme,
     * a child theme, or any plugin that hooks into this theme to extend its
     * layout system.
     *
     * @var array
     * @note Plugin extensibility is primarily intended for plugins to
     *     provide logic extensibility that is not appropriate for a theme
     *     in keeping with the Wordpress declaration of what constitutes a
     *     valid theme vs. plugin. It is fully possible to use a plugin
     *     to add additional layouts, but it is not recommended, because
     *     it does not honor the division of responsibility principle.
     */
    private static $layout_directories;

    /**
     * Represents a list of valid layouts, which can be referenced directly
     * without another disk lookup once entered.
     * @var array
     */
    private static $layouts;

    /**
     * Represents the current layout represented by the model.
     * @var array
     */
    private $layout;

    /**
     * Represents the name of the layout,
     * which corresponds to its folder name,
     * as well as its slug in the database.
     * @var string
     */
    private $layout_name = 'default';

    /**
     * The human readable name of the layout
     * @var string
     */
    private $layout_title = 'Default Layout';

    /**
     * A short readable description of the layout.
     * @var string
     */
    private $layout_description = 'This is the default Sanctity layout.';

    /**
     * Any javascript required by the layout beyond the
     * baseline requirements of Sanctity itself.
     * @var array
     */
    private $layout_scripts = array();

    /**
     * Any custom stylesheets required by the layout beyond the
     * baseline requirements of Sanctity itself.
     * @var array
     */
    private $layout_styles = array();

    public function __construct()
    {
        $this->_initializeDefaultManifest();
        $this->_setCurrentLayout();
        parent::__construct();
    }

    public function getSettings()
    {
        $this->_initializeDefaultManifest();
        $this->_initializeValidLayoutSet();
        if ( is_array( $this->layout ) )
        {
            return $this::containerize( $this->layout );
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        try
        {
            // If a given path has not provided any layouts, this will fail. In that case,
            // it just returns an empty array, so no paths get registered.
            $this->_getLayoutValidPathRoots();
            $this->_initializeDefaultManifest();
            $this->layout['paths'] = $this->getPaths();
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            // There are no layouts in this scope.
            $this->layout = array();
        }
        $this->layout['options'] = $this->load( 'model', 'settings\\LayoutModel' )->getOptions();
        return $this::containerize( $this->layout );
    }

    /**
     * Returns only the details for the currently applied layout.
     */
    public function getCurrent()
    {
        return $this->layout;
    }

    /**
     * Return the slug name of the given layout.
     *
     * @param string $layout The slug identifier of the layout
     * @return string
     */
    public function getName()
    {
        return $this->layout_name;
    }

    /**
     * Returns the human readable title of the current layout.
     *
     * @param string $layout The slug identifier of the layout
     * @return string
     */
    public function getTitle()
    {
        return $this->layout_title;
    }

    /**
     * Returns the human readable description of the current layout.
     *
     * @param string $layout The slug identifier of the layout
     * @return string
     */
    public function getDescription()
    {
        return $this->layout_description;
    }

    /**
     * Returns an array of any javascript required by the layout,
     * preformatted to the correct fully qualified uri(s).
     *
     * key: 'layoutname_scriptname' => '//yoursite.com/full/script/path.js'
     *
     * @param string $layout The slug identifier of the layout
     * @return array
     */
    public function getScripts()
    {
        return $this->layout_scripts;
    }

    /**
     * Returns an array of any css required by the layout,
     * preformatted to the correct fully qualified uri(s).
     *
     * key: 'layoutname_stylesheetname' => '//yoursite.com/full/script/path.css'
     *
     * @param string $layout The slug identifier of the layout
     * @return array
     */
    public function getStyles()
    {
        return $this->layout_styles;
    }

    /**
     * Returns the paths required for the current layout to load,
     * and fall back gracefully to undeclared assets.
     *
     * @return array
     */
    public function getPaths()
    {
        $default = $this->getPath( 'default' );
        $current = $this->getPath( $this->layout_name );
        $result = array(
            $default );
        if ( $current !== $default )
        {
            $result[] = $current;
        }
        return $result;
    }

    /**
     * Returns the relative path of the layout folder,
     * relative to the template directory.
     * @param string $layout The slug identifier of the layout
     * @return string
     */
    public function getPath( $layout )
    {
        return $this->declareLayoutRoot() . $this->getName( $layout ) . DIRECTORY_SEPARATOR;
    }

    /**
     * Returns the details for sidebar registration.
     *
     * @param bool $active If true, returns only currently active sidebars.
     *      If false, returns all known sidebars. Default false.
     * @return array
     */
    public function getSidebars( $active = false )
    {
        $list = array();
        $settings = $this->getSettings();
        $sidebars = $settings['sidebars'];
        foreach ( $sidebars as
            $slug =>
            $sidebar )
        {
            if ( $active && (!$sidebar['enabled'] || !$sidebar['register'] ) )
            {
                continue;
            }

            $list[$slug] = array(
                'enabled' => $sidebar['enabled'],
                'register' => $sidebar['register'],
                'name' => $sidebar['name'],
                'id' => $this->getThemePrefix() . '-' . $sidebar['id'],
                'description' => __( $sidebar['description'] ),
                'class' => $sidebar['class'],
                'before_widget' => $this->_filterElementDeclaration( $sidebar['before-widget'],
                    $sidebar, $slug ),
                'after_widget' => $this->_filterElementDeclaration( $sidebar['after-widget'],
                    $sidebar, $slug ),
                'before_title' => $this->_filterElementDeclaration( $sidebar['before-title'],
                    $sidebar, $slug ),
                'after_title' => $this->_filterElementDeclaration( $sidebar['after-title'],
                    $sidebar, $slug )
            );
        }
        return $list;
    }

    public function getSidebar( $key, $active = false )
    {
        $sidebars = $this->listSidebars( $active );
        if ( !array_key_exists( $key, $sidebars ) )
        {
            return false;
        }
        return $sidebars[$key];
    }

    public function listSidebars( $active = false )
    {
        $list = array();
        $sidebars = $this->getSidebars( $active );
        return array_keys( $sidebars );
    }

    public function registerSidebars()
    {
        $sidebars = $this->getSidebars( true );
        foreach ( $sidebars as
            $slug =>
            $sidebar )
        {
            unset( $sidebar['enabled'] );
            unset( $sidebar['register'] );
            $this::getAdapter()->register( 'sidebar', $sidebar );
        }
    }

    /**
     * Fetches the markup for sidebar content.
     *
     * @return array
     */
    public function renderSidebars()
    {
        $sidebars = array();
        foreach ( $this->getSidebars( true ) as
            $slug =>
            $sidebar )
        {
            $sidebars[$slug] = $this::getAdapter()->get( 'sidebar',
                $sidebar['id'] );
        }
        return $sidebars;
    }

    /**
     * Override this method to provide a different file suffix for templates.
     *
     * @example The static layout model returns 'html', which is used when a
     *     static error page fires, or when Twig is not available.
     *     Standard PHP parsing is pending as a fallback solution,
     *     though this will severely cripple the theme capabilities.
     * @example If you wanted to template CSS or Javascript with Twig,
     *     you could use a different suffix for that.
     * @return string
     */
    protected function declareTemplateSuffix()
    {
        return 'twig';
    }

    /**
     * Override this method to declare a different root path for your layouts.
     *
     * The layout path is assumed to exist within a folder corresponding to the
     * controller scope passed, with a fallback to "global" from the theme
     * root directory, or the child theme root directory.
     *
     * This method should return a value that corresponds to a relative path
     * from the "templates/frontend" or "templates/global/{value_from_this_method}"
     * within the theme root.
     *
     * If you are doing templating for the login,
     * it would be "templates/login/{value_from_this_method}",
     * and similarly if you were doing templates for the dashboard,
     * it would be "templates/admin/{value_from_this_method}".
     *
     * Template slugs should correspond to files located relative to the path
     * created in this manner for layouts.
     *
     * Values returned from this method should contain a trailing slash.
     *
     * @return string
     */
    protected function declareLayoutRoot()
    {
        return $this::MODEL_SETTINGS_KEY
            . DIRECTORY_SEPARATOR;
    }

    /**
     * This method constructs a set of template slugs into their
     * relative file paths. It is protected so it is internally
     * accessible from child classes, but should generally not be overridden
     * except in very specific edge cases.
     *
     * @param array $blocks
     * @return string
     */
    protected function formatLayoutBlocks( $name, array $blocks )
    {
        $results = array();
        $suffix = $this->declareTemplateSuffix();
        $root = $this->declareLayoutRoot();
        foreach ( $blocks as
            $key =>
            $value )
        {
            $results[$key] = $root . $name . DIRECTORY_SEPARATOR . $value . '.' . $suffix;
        }
        return $results;
    }

    private function _getLayouts()
    {
        return array(
            'default' );
    }

    private function _getLayoutBaseTemplates()
    {
        return array(
            'index' => 'index',
            'error' => 'error',
        );
    }

    /**
     * Returns the root default layout object if no other one is defined.
     *
     * This method does not require a database connection to resolve.
     *
     * @return array
     * @internal
     */
    private function _getLayoutData()
    {
        $layouts = array();
        foreach ( $this->_getLayouts() as
            $layout_slug )
        {
            $defaults = array(
                'name' => $this->getName( $layout_slug ),
                'title' => $this->getTitle( $layout_slug ),
                'description' => $this->getDescription( $layout_slug ),
                'path' => $this->getPath( $layout_slug ),
                'scripts' => $this->getScripts( $layout_slug ),
                'styles' => $this->getStyles( $layout_slug ),
                'templates' => $this->formatLayoutBlocks( $layout_slug,
                    $this->_getLayoutBaseTemplates() ),
            );
            $layouts[$layout_slug] = $defaults;
        }
        return $layouts;
    }

    /**
     * This method will set the current layout, when the database logic is done.
     * For now it just leaves it set to the default.
     */
    private function _setCurrentLayout( $layout = null )
    {
        if ( is_null( $layout ) )
        {
            $layout = 'default';
        }
        $scope = 'frontend';
        if ( !is_null( $this->getScope() ) )
        {
            $scope = $this->getScope();
        }
        if ( !array_key_exists( $layout, self::$layouts[$scope] ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Layout [%1$s] is not valid in [%2$s]. Please register '
                . 'the layout before requesting it.', $layout,
                get_class( $this ) )
            );
        }
        $this->layout_name = $layout;
        $this->layout = self::$layouts[$scope][$layout];
    }

    /**
     * Fetches the manifest file for the default layout,
     * which is the base template for all other layout manifests.
     *
     * @return type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the default layout manifest file cannot be found.
     *     This means someone broke the theme by deleting required files,
     *     and it will need to be deactivated.
     * @internal
     */
    private function _initializeDefaultManifest()
    {
        if ( is_null( self::$default_manifest ) )
        {
            if ( !is_null( $this->getScope() ) )
            {
                $scope = $this->getScope();
            }
            $this->_getLayoutValidPathRoots();
            $path = $this->_getLayoutRootPath( $scope,
                    $this->declareLayoutRoot() ) . self::$default_layout
                . DIRECTORY_SEPARATOR . 'manifest.json';
            if ( !is_readable( $path ) )
            {
                //This means it's the admin or login and nothing registered a layout for it.
                //This is the default behavior, and it usually indicates we're on the
                //settings page fiddling with settings.
                //It needs the default set back in that case.
                $scope = 'frontend';
                $path = $this->_getLayoutRootPath( $scope,
                        $this->declareLayoutRoot() ) . self::$default_layout
                    . DIRECTORY_SEPARATOR . 'manifest.json';
            }
            if ( !is_readable( $path ) )
            {
                //If it still chokes there's a legit problem.
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Error encountered during initialization of [%1$s]. '
                    . 'Default layout manifest.json file not found at '
                    . '[%2$s].', get_class( $this ), $path )
                );
            }
            $this->_registerLayout( 'default' );
            self::$default_manifest = json_decode( file_get_contents( $path ), 1 );
            $this->layout = self::$default_layout;
            $this->layout_data = self::$default_manifest;
        }
    }

    /**
     * Initializes all known layouts that have a valid manifest
     * in the current scope, for both this theme and any child
     * theme that extends it.
     * @return void
     * @internal
     */
    private function _initializeValidLayoutSet()
    {
        if ( is_null( self::$layouts ) )
        {
            if ( is_null( $this->getScope() ) )
            {
                //Can't do it yet, no scope.
                return;
            }
            $this->_getLayoutValidPathRoots();
            $layouts = array(
                $this->getScope() => array(
                    'default' => self::$default_manifest,
                )
            );
            $path = $this->declareLayoutRoot();
            if ( is_user_logged_in() )
            {
                d( $path, is_readable( $path ), $this, $layouts );
                exit();
            }
        }
    }

    /**
     * This method will check return the main theme path,
     * a child theme path (or paths, if multiple),
     * and any registered plugin paths that declared layout support (if any exist)
     * for valid layout root directories, and return an array of them.
     *
     */
    private function _getLayoutValidPathRoots()
    {
        if ( is_null( self::$layout_directories ) )
        {
            $scope = $this->getScope();
            self::$layout_directories = array();
            try
            {
                $root = $this->_getLayoutRootPath( $scope,
                    $this->declareLayoutRoot() );
                $this->_registerLayoutRoot( $root );
            } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
            {
                //This is an expected exception if you are in the admin or
                //login and no paths have been registered.
                $scope = 'frontend';
                $root = $this->_getLayoutRootPath( $scope,
                    $this->declareLayoutRoot() );
                $this->_registerLayoutRoot( $root );
            }
        }
    }

    private function _getLayoutRootPath( $scope, $folder = null )
    {
        return SANCTITY_BASEDIR . 'templates' . DIRECTORY_SEPARATOR
            . $scope
            . DIRECTORY_SEPARATOR . $folder;
    }

    /**
     * Registers a new layout directory to search for layouts.
     * The layouts in the given directory MUST contain
     * a valid manifest file in order to be included as a valid layout.
     *
     * @param string $path A valid directory containing layouts
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given path is not readable or not a directory.
     * @internal
     */
    private function _registerLayoutRoot( $path )
    {
        if ( !is_readable( $path ) || !is_dir( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Provided path [%1$s] is not a valid directory.', $path )
            );
        }
        if ( in_array( $path, self::$layout_directories ) )
        {
            return;
        }
        self::$layout_directories[] = $path;
    }

    /**
     * Registers a new layout, or updates an existing one.
     *
     * This method will search in all known layout roots that have been declared
     * as valid for a corresponding layout folder with a manifest.json file.
     *
     * If one is found, it will add the layout. If one already existed,
     * it will update the existing layout. Only details that need to be
     * overridden need be provided if one already exists. Prior details
     * will be retained if they are not supplied replacements, which means
     * that extension layouts can add only what they are actually modifying
     * to their manifest, and fall back to the parent layout for everything else.
     *
     * @param string $layout
     * @return void
     * @internal
     */
    private function _registerLayout( $layout )
    {
        $scope = $this->getScope();
        if ( is_null( $scope ) )
        {
            $scope = 'frontend';
        }
        $layouts = array();
        foreach ( self::$layout_directories as
            $base_layout_directories )
        {
            $layout_directories = array_filter( glob( $base_layout_directories . '/*' ),
                'is_dir' );
            foreach ( $layout_directories as
                $layout_directory )
            {
                $manifest_file = $layout_directory . DIRECTORY_SEPARATOR . 'manifest.json';
                if ( !is_readable( $manifest_file ) )
                {
                    continue;
                }
                $manifest = json_decode( file_get_contents( $manifest_file ), 1 );
                $layouts[] = $manifest;
            }
        }
        $final = array();
        if ( is_array( self::$layouts ) && array_key_exists( $scope,
                self::$layouts )
            && array_key_exists( $layout, self::$layouts[$scope] ) )
        {
            $final = self::$layouts[$scope][$layout];
        }
        foreach ( $layouts as
            $revision )
        {
            $final = array_merge_recursive( $final, $revision );
        }
        self::$layouts[$scope][$layout] = $final;
    }

    /**
     * Returns the manifest for a currently registered layout,
     * or false if it has not been registered.
     *
     * @param string $scope the scope of layouts
     *     (default is frontend, but child classes and plugins
     *     are not restricted from registering other custom types).
     *
     * @param string $layout the layout slug
     * @return bool|array
     * @internal
     */
    private function _getRegisteredLayout( $scope, $layout )
    {
        if ( array_key_exists( $layout, self::$layouts[$scope] ) )
        {
            return self::$layouts[$scope][$layout];
        }
        return false;
    }

    private function _filterElementDeclaration( $value, $component )
    {
        if ( strpos( $value, 'template:' ) === 0 )
        {
            $value = $this->_loadElementTemplate( substr( $value,
                    strlen( 'template:' ) ), $component );
        }
        return $value;
    }

    private function _loadElementTemplate( $template, $component )
    {
        $view = $this->load( 'view', 'frontend\\FrontendView' );
        $context = $this->load( 'library', 'config\\ContextConfig' )->get()->toArray();
        $context['branding'] = $this->load( 'model', 'display\\BrandingModel' )->getSettings();
        $context['style'] = $this->load( 'model', 'settings\\StyleModel' )->getSettings();
        $context['layout'] = $this->load( 'model', 'settings\\LayoutModel' )->getSettings();
        $context['settings'] = $this->getSettings();
        $context['component'] = $component;
//        d($component); exit;
        return $view->renderPart( $template, $context, true );
    }

}
