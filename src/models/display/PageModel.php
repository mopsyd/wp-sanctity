<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\models\display;

/**
 * Page Model
 * Provides a vastly simplified api for working with pages
 * by presenting an abstraction layer for WP_Query.
 *
 * Defaults to the current page, but can be scoped
 * to any default Wordpress page or post.
 *
 * This class does not handle custom post types. It is intended explicitly
 * for simplifying the operations of working with Wordpress's standard
 * runtime routine. This is an intentional design decision to prevent
 * plugins that add waaaaaay too many posts from bogging things down,
 * and that way it keeps operations quick and snappy.
 *
 * WP_Query is used at all times that it is pragmatic to do so.
 * In the event that information is requested prior to the wordpress
 * internals firing, there are some workarounds presented for
 * basic information that may be needed prior to the loop.
 *
 * In the event that a request is unsafe and would normally
 * cause a whitescreen or fatal error, an exception is thrown
 * to block the request and prevent the page from crashing,
 * so it can be mitigated programmatically.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class PageModel
    extends \mopsyd\sanctity\models\AbstractModel
{

    const MODEL_SETTINGS_KEY = 'page';

    /**
     * Represents a statically retained reference to the global WP_Query for
     * the current page load. This is used as a default for new instances loaded.
     * It loads one time on the first instantiation of this class, and remains
     * bound behind a private static property for the remainder of runtime so
     * the query never needs to be globaled again after the first load.
     * Only non-destructive operations are done against this object,
     * and this class does not alter it in any way at any point during runtime.
     * @var \WP_Query
     */
    private static $current_query;

    /**
     * This is a flat index of all of the vanilla Wordpress posts and pages that exist.
     * This is loaded one time and retained for the duration of runtime.
     * Only standard posts and pages are included in this variable.
     * This allows the class to quickly index requests for information
     * without the overhead of performing another database query.
     * Custom post types are explicitly excluded from this set,
     * and WP_Query is used for all cases where a given post ID
     * does not exist in this array.
     * @var array
     */
    private static $posts_raw;

    /**
     * This is the absolute ID of the home page. This is manually queried for
     * when the class first instantiates, so the class can produce an
     * absolutely definitive true or false on any request for whether
     * the current page (or any page) is the home page, without using
     * comparison functions that cause fatal errors very early in runtime.
     *
     * This alleviates the typical confusion as to whether a page is home
     * and not a blog, is a blog, or is home and also displays the blogroll.
     *
     * @var int|bool This value will resolve to false if the page does
     *     not have a home page, in which case the front page is the blogroll.
     *
     * @note If this value is false and the home page value is also
     *     false, the database records have been corrupted through some sort
     *     of external fiddling.
     */
    private static $home_id;

    /**
     * This is the absolute ID of the blog page. This is manually queried for
     * when the class first instantiates, so the class can produce an
     * absolutely definitive true or false on any request for whether
     * the current page (or any page) is the home page, without using
     * comparison functions that cause fatal errors very early in runtime.
     *
     * This alleviates the typical confusion as to whether a page is home
     * and not a blog, is a blog, or is home and also displays the blogroll.
     *
     * @var int|bool This value will resolve to false if the page does not
     *     have a blogroll. Otherwise it will display the post id for the
     *     blog page.
     *
     * @note If this value is false and the home page value is also
     *     false, the database records have been corrupted through some sort
     *     of external fiddling.
     */
    private static $blog_id;

    /**
     * Represents an array of common details requested about the current page,
     * which are internally generated when an object instantiates through direct
     * calls to the current WP_Query instance, along with a handful of
     * conditionals to provide an absolute determination about comon edge cases.
     * This value is obtained by calling the `getSettings` method, and will
     * always reflect the details for the instance of WP_Query held by the
     * current object instance. This resets when any call to bind any other
     * page is provided, so it always reflects the current object exactly.
     *
     * @var array
     */
    private $page;

    /**
     * Represents the page header data provided by the platform, if any exists.
     * @var array
     */
    private $head;

    /**
     * Represents the page footer data provided by the platform, if any exits.
     * @var array
     */
    private $footer;

    /**
     * Represents the instance of WP_Query held by the current active object.
     * By default, this will be identical to the current page load query,
     * but can be scoped to any query. All internal calls to determine
     * information are made against this class property, not the static one.
     * This allows the object to reflect pages other than the current one
     * if needed. All various means of scoping the model result in an instance
     * of WP_Query being generated, which in turn is used to fill out the
     * internal data presented through this class's public methods.
     *
     * @var WP_Query
     */
    private $query;

    /**
     * Page Model Constructor
     *
     * Builds a page model scoped to the current query.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This model will not allow itself to be constructed when any of the
     *     Wordpress internals it relies on would potentially cause a fatal error.
     *     It is available from the `init` hook onward.
     */
    public function __construct()
    {
        try
        {
            $this->_initializeRawPostData();
            $this->_initializeDefaultQuery();
            $this->_checkQueryExecution( __FUNCTION__ );
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Instance of [%1$s] cannot instantiate yet due to the possibility of generating '
                . 'a fatal error when referencing aspects of the Wordpress api this '
                . 'early in runtime.', get_class( $this )
            ), $e->getCode(), $e );
        }
        parent::__construct();
    }

    public function getSettings()
    {
        if ( is_array( $this->page ) )
        {
            return $this->page;
        }
        $settings = parent::getSettings();
        if ( !$settings )
        {
            $settings = array();
        }
        $settings = $this->_getPostDataDefaults();
        $settings['template'] = $this->getTemplate();
        $settings['template'] = $this->getTemplate();
        $this->page = $settings;
        return $this::containerize( $settings );
    }

    public function getOutputData()
    {
        $settings = $this->getSettings();
        $settings['head'] = $this->_parseHead();
        $settings['footer'] = $this->_parseFooter();
        return $this->aggregatePageData( $settings );
    }

    public function getTemplate()
    {
        return 'base.twig';
    }

    /**
     * Scopes the current model to a post id.
     *
     * @param int $id
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function loadId( $id )
    {
        $this->loadQuery( $this::getAdapter()->query( 'query',
                array(
                'p' => $id,
                'post_type' => 'any'
        ) ) );
        $this->_parseQuery();
        return $this;
    }

    /**
     * Creates a new model instance from a post id.
     *
     * @param int $id
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function withId( $id )
    {
        $model = $this::init();
        $model->loadId( $id );
        return $model;
    }

    /**
     * Scopes the current model to a \WP_Query instance.
     *
     * @param \WP_Query $query
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function &loadQuery( \WP_Query $query )
    {
        $this->query = &$query;
        $this->_parseQuery();
        return $this;
    }

    /**
     * Creates a new model instance from a WP_Query instance.
     *
     * @param \WP_Query $query
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function &withQuery( \WP_Query $query )
    {
        $model = $this::init();
        $model->loadQuery( $query );
        return $model;
    }

    /**
     * Scopes the current model to a wordpress post object.
     *
     * @param \WP_Post $post
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function loadPost( \WP_Post $post )
    {
        return $this->loadId( $post->ID );
    }

    /**
     * Creates a new model instance from a wordpress post object.
     *
     * @param \WP_Post $post
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function fromPost( \WP_Post $post )
    {
        $model = $this::init();
        $model->loadPost( $post );
        return $model;
    }

    /**
     * Scopes the current model instance from a full uri.
     *
     * @param string $uri
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function loadUri( $uri )
    {
        $query = $this->getQueryFromUri( $uri );
        return $this->loadQuery( $query );
    }

    /**
     * Creates a new model instance from a full uri.
     *
     * @param string $uri
     * @return \mopsyd\sanctity\models\display\PageModel
     */
    public function fromUri( $uri )
    {
        $model = $this::init();
        $model->loadUri( $uri );
        return $model;
    }

    /**
     * Gets the page type.
     *
     * This will be one of [401, 403, 404, 500, page, post, login, admin, ajax, attachment, custom, archive, robots, search, category, tag, feed]
     *
     * @return string
     */
    public function getType()
    {
        return $this->page['type'];
    }

    /**
     * Gets the query the model is currently scoped to.
     *
     * @return \WP_Query
     */
    public function getQuery()
    {
        return $this->getCurrentQuery();
    }

    /**
     * Checks if the model represents the current page load.
     *
     * If true, the model represents the current page view.
     * If false, it represents a different page.
     *
     * @return bool
     */
    public function isCurrentPage()
    {
        return !is_null( self::$current_query )
            && self::$current_query === $this->query;
    }

    /**
     * Returns a boolean determination as to whether the currently held
     * query has been executed, which determines if its values can be
     * trusted as definitive. This is useful for validating against
     * the primary query for the current page, and will tell you if
     * it is too early to tell whether its information is accurate or not.
     *
     * This is particularly useful for debugging whether or not an action
     * binding is appropriate for the currently bound method/function.
     *
     * @return false
     *
     * @note This method is used internally to determine if other methods are
     *     safe to request, which will generate an exception if that method would
     *     cause wordpress internals to potentially generate a fatal error if the
     *     request proceeded. This gives you an opportunity to debug and/or delay
     *     the request.
     */
    public function isExecuted()
    {
        //WP_Query's "query" property is null if it
        //has not made a database query as of yet.
        return !is_null( $this->query ) && !( $this->getScope() === 'frontend' &&
            is_null( $this->query->query ) );
    }

    /**
     * Returns true if the page is the home page
     *
     * @return bool
     */
    public function isHome()
    {
        return $this->getHomepageId() === $this->page['id'];
    }

    /**
     * Returns true if the page is the blog page
     *
     * @return bool
     */
    public function isBlog()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        return $this->getBlogPageId() === $this->page['id'];
    }

    /**
     * Returns true if the page is the login page
     *
     * @return bool
     */
    public function isLogin()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'login';
    }

    /**
     * Returns true if the page is the admin dashboard
     *
     * @return bool
     */
    public function isAdmin()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'admin';
    }

    /**
     * Returns true if the page is an active ajax call
     *
     * @note This does not validate potential ajax calls,
     *     only the current page will return true here within
     *     an actual active ajax call. This method cannot be
     *     used to sniff endpoints for security purposes.
     *
     * @return bool
     */
    public function isAjax()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'ajax';
    }

    /**
     * Returns true if the page is a file attachment
     *
     * @return bool
     */
    public function isAttachment()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'attachment';
    }

    /**
     * Returns true if the page is a custom post type
     *
     * @return bool
     */
    public function isCustom()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'custom';
    }

    /**
     * Returns true if the page is a standard post
     *
     * @return bool
     */
    public function isPost()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'post';
    }

    /**
     * Returns true if the page is a standard page
     *
     * @return bool
     */
    public function isPage()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'page';
    }

    /**
     * Returns true if the page is an archive
     *
     * @return bool
     */
    public function isArchive()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'archive';
    }

    /**
     * Returns true if the page is the robots.txt file
     *
     * @return bool
     */
    public function isRobots()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'robots';
    }

    /**
     * Returns true if the page is a search
     *
     * @return bool
     */
    public function isSearch()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'search';
    }

    /**
     * Returns true if the page is a tag search
     *
     * @return bool
     */
    public function isTag()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'tag';
    }

    /**
     * Returns true if the page is a category search
     *
     * @return bool
     */
    public function isCategory()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'category';
    }

    /**
     * Returns true if the page is a feed
     *
     * @return bool
     */
    public function isFeed()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $this->getType() === 'feed';
    }

    /**
     * Returns true if the page is currently published
     *
     * @return bool
     */
    public function isPublic()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        return self::$posts_raw[$this->current_post['id']]['status'] === 'publish';
    }

    protected function aggregatePageData( $data )
    {
        $data = $this->_packagePrefetch( $data );
        $data = $this->_packagePreload( $data );
        $data = $this->_packageOrigins( $data );
        $data = $this->_packageStructuredData( $data );
        return $data;
    }

    /**
     * Returns the current query.
     *
     * This is the internal mechanism that is used by the public accessor method.
     * This relay allows child classes to alter the logic here without disrupting
     * the public method.
     *
     * @return bool
     */
    protected function getCurrentQuery()
    {
        return $this->query;
    }

    /**
     * Returns the post object from the current query,
     * or false if it doesn't have one.
     *
     * @return bool|WP_Post
     */
    protected function getPost()
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        if ( is_null( $this->query->post ) )
        {
            return false;
        }
        return $this->query->post;
    }

    /**
     * Parses a page or post id into a query.
     *
     * @param int $id
     * @return \WP_Query
     */
    protected function getQueryFromId( $id )
    {
        $query = $this::getAdapter()->query( 'query',
            array(
            'p' => $id,
            'post_type' => 'any' ) );
        return $query;
    }

    /**
     * Parses a full uri into a query.
     *
     * @param string $uri
     * @return \WP_Query
     */
    protected function getQueryFromUri( $uri )
    {
        $this->_checkQueryExecution( __FUNCTION__ );
        $id = $this->url_to_postid( $uri );
        return $this->getQueryFromId( $id );
    }

    /**
     * Returns the ID of the homepage for the site by querying the database
     * directly if it has not already been loaded, which allows this check
     * to occur prior to any actions firing.
     *
     * This query only occurs one time per page load,
     * and is cached statically for the remainder of runtime.
     *
     * @return int
     * @see \mopsyd\sanctity\models\display\PageModel::_getSpecial
     *     This is the method that performs the query securely.
     *     No external data is injected into the query whatsoever.
     */
    protected function getHomePageId()
    {
        if ( !is_null( self::$home_id ) )
        {
            return self::$home_id;
        }
        $id = $this->_getSpecial( 'home' );
        self::$home_id = $id;
        return $id;
    }

    /**
     * Returns the ID of the blog page for the site by querying the database
     * directly if it has not already been loaded, which allows this check
     * to occur prior to any actions firing.
     *
     * This query only occurs one time per page load,
     * and is cached statically for the remainder of runtime.
     *
     * @return type
     * @see \mopsyd\sanctity\models\display\PageModel::_getSpecial
     *     This is the method that performs the query securely.
     *     No external data is injected into the query whatsoever.
     */
    protected function getBlogPageId()
    {
        if ( !is_null( self::$blog_id ) )
        {
            return self::$blog_id;
        }
        $id = $this->_getSpecial( 'blog' );
        self::$blog_id = $id;
        return $id;
    }

    /**
     * Scopes the object to the current query on initial instantiation,
     * and insures that the initial query is bound internally if it
     * is not already.
     * @return void
     * @internal
     */
    private function _initializeDefaultQuery()
    {
        $this->_initializeCurrentQuery();
        $this->query = self::$current_query;
    }

    /**
     * Loads and returns the initial reference to existing pages and posts
     * if they are not already loaded, and returns an array of details
     * about the current page request.
     * @return array
     * @internal
     */
    private function _getPostDataDefaults()
    {
        $this->_initializeCurrentQuery();
        $this->_parseQuery();
        return $this->page;
    }

    /**
     * Parses the query and populates the page settings results.
     * In edge cases like the login page where the query does not
     * return accurate results, this step is emulated to provide
     * a consistent result set.
     * @return void
     * @internal
     */
    private function _parseQuery()
    {
        $this->page = $this->_getPage();
    }

    /**
     * Parses the page head data, and returns a containerized subset of the data
     * provided by the platform for the document head, if any.
     *
     * The data will be cleaned up, normalized, have whitespace clarified,
     * and redundant unwanted 3rd party comment clutter removed.
     * @return string
     */
    private function _parseHead()
    {
        $this->head = $this->_getHead();
        return $this->head;
    }

    /**
     * Parses the page head data, and returns a containerized subset of the data
     * provided by the platform for the document head, if any.
     *
     * The data will be cleaned up, normalized, have whitespace clarified,
     * and redundant unwanted 3rd party comment clutter removed.
     * @return string
     */
    private function _parseFooter()
    {
        $this->footer = $this->_getFooter();
        return $this->footer;
    }

    /**
     * Populates the page object with additional metadata
     * for the template and/or controller.
     *
     * @param \WP_Query $query
     * @return array
     * @internal
     */
    private function _populateById( $id )
    {
        return $this->_populatePage( $id );
    }

    private function _getType()
    {
        $type = $this::getAdapter()->evaluate( 'type', $this->query );
        return $type;
    }

    private function _getPage()
    {
        $page = $this::getAdapter()->populate( 'page', $this->query );
        return $page;
    }

    private function _getHead()
    {
        $result = $this::getAdapter()->parse( 'head' );
        return $result;
    }

    private function _getFooter()
    {
        $result = $this::getAdapter()->parse( 'footer' );
        return $result;
    }

    private function _getPosts()
    {
        $posts = $this::getAdapter()->aggregate( 'post', $this->query );
        return $posts;
    }

    private function _packagePrefetch( $data )
    {
        $prefetch = array();
        $head = $data->get( 'head' );
        $footer = $data->get( 'footer' );
        $this->_extractLinkset( $head, $prefetch );
        $this->_extractLinkset( $footer, $prefetch );
        $head['prefetch'] = $this->_parseHostnames( $prefetch );
        $data['head'] = $head;
        $data['footer'] = $footer;
        return $data;
    }

    private function _packagePreload( $data )
    {
        $preload = array();
        $head = $data->get( 'head' );
        $footer = $data->get( 'footer' );
        $this->_extractLinkset( $head, $preload );
        $this->_extractLinkset( $footer, $preload );
        $head['preload'] = $preload;
        $data['head'] = $head;
        $data['footer'] = $footer;
        return $data;
    }

    private function _packageOrigins( $data )
    {
        $origins = $this->_parseHostnames( $data->get( 'head' )['preload'] );
        $data['origins'] = $origins;
        return $data;
    }

    private function _extractLinkset( &$data, &$prefetch )
    {
        foreach ( $data['links'] as
            $key =>
            $link )
        {
            if ( strpos( $link, 'rel="dns-prefetch"' ) !== false )
            {
                $url = $this->_extractLinkUri( $link );
                if ( !in_array( $url, $prefetch ) )
                {
                    $prefetch[] = array(
                        'href' => $url );
                }
                unset( $data['links'][$key] );
                continue;
            }
            if ( strpos( $link, 'rel="stylesheet"' ) !== false )
            {
                $url = $this->_extractLinkUri( $link );
                if ( !in_array( $url, $prefetch ) )
                {
                    $prefetch[] = array(
                        'type' => 'style',
                        'href' => $url
                    );
                }
            }
        }
        foreach ( $data['scripts'] as
            $key =>
            $script )
        {
            if ( strpos( $script, 'src="' ) === false )
            {
                continue;
            }
            $url = $this->_extractScriptUri( $script );
            if ( !in_array( $url, $prefetch ) )
            {
                $prefetch[] = array(
                    'type' => 'script',
                    'href' => $url
                );
            }
        }
    }

    private function _parseHostnames( $uris )
    {
        $hosts = array();
        foreach ( $uris as
            $url )
        {
            $parsed = parse_url( $url['href'] );
            $url = ( array_key_exists( 'scheme', $parsed )
                ? $parsed['scheme'] . '://'
                : '//' )
                . $parsed['host'] . '/';
            if ( !in_array( $url, $hosts ) )
            {
                $hosts[] = $url;
            }
        }
        return $hosts;
    }

    private function _extractLinkUri( $link )
    {
        $start = strpos( $link, 'href="' ) + 6;
        $link = substr( $link, $start );
        $link = substr( $link, 0, strpos( $link, '"' ) );
        return $link;
    }

    private function _extractScriptUri( $script )
    {
        $start = strpos( $script, 'src="' ) + 5;
        $link = substr( $script, $start );
        $link = substr( $link, 0, strpos( $link, '"' ) );
        return $link;
    }

    private function _packageStructuredData( $data )
    {
        $head = $data->get('head');
        $structured = array();
        // @todo build structured data
        $head['structured'] = $structured;
        $data['head'] = $head;
        return $data;
    }

    /**
     * Get all the posts and pages that are registed by Wordpress core.
     * Omits custom post types, and posts that have no possibility of
     * generating a public page.
     *
     * These will be cached in a static variable for the remaining runtime,
     * so they don't need to be fetched again.
     *
     * The provided details are the id, author id, slug, and visibility level.
     *
     * Any further information on the post is acquired through a WP_Query call,
     * or another standard function from the Wordpress api.
     *
     * This allows us to grab info about posts before wordpress is initialized.
     * We can filter for privacy manually, since the typical procedural calls
     * don't really work prior to the "init" filter running.
     * @return void
     * @internal
     */
    private function _initializeRawPostData()
    {
        if ( is_null( self::$posts_raw ) )
        {
            $db = $this->getDatabase();
            $table = $db->wpdb()->posts;
            $query = "SELECT `id`, `post_type` AS `type`, `post_author` AS `author`, `post_name` AS `slug`, `post_status` AS `status`
                        FROM " . $table . "
                        WHERE `post_type` IN ('page', 'post' )
                        AND `post_status` NOT IN ('draft', 'trash')
                        ORDER BY `id`;";
            $statement = $db->run( $query );
            $result = $statement->fetchAll();
            foreach ( $result as
                $row )
            {
                $row['home'] = ( $this->getHomePageId() && $this->getHomePageId()
                    === $row['id']
                    ? true
                    : false);
                $row['blog'] = ( $this->getBlogPageId() && $this->getBlogPageId()
                    === $row['id']
                    ? true
                    : false);
                self::$posts_raw[$row['id']] = $row;
            }
        }
    }

    /**
     * Locates the ID of the blog or home page directly from the database
     * with no ambiguity, as they often collide based on various settings for
     * the blogroll, and make the core Wordpress functions to determine them
     * unreliable when called before the loop is initiated.
     *
     * @param type $type
     * @return bool|int
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This method only accepts "home" or "blog" as parameters.
     *     Lower case only. Strictly checked.
     * @internal
     * @see \mopsyd\sanctity\libs\pdo\Pdo::run
     */
    private function _getSpecial( $type = 'home' )
    {
        if ( !in_array( $type,
                array(
                'home',
                'blog' ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            'This method only fetches the blog or home page.'
            . 'No other values are allowed except the absolute '
            . 'unmodified slugs denoting one or the other by '
            . 'this method.' );
        }
        $db = $this->getDatabase();
        $options_table = $db->wpdb()->options;
        $posts_table = $db->wpdb()->posts;
        $meta_key = ($type === 'home'
            ? 'page_on_front'
            : 'page_for_posts' );
        $query = "SELECT `posts`.`id` FROM `" . $options_table . "` AS `options`
            LEFT JOIN `" . $posts_table . "` AS `posts` ON `options`.`option_value` = `posts`.`id`
            WHERE `option_name`=:" . $type . " -- The blog page or the home page meta key
            LIMIT 1;";
        $result = $db->run( $query, array(
                $type => $meta_key ) )->fetch();
        if ( empty( $result ) )
        {
            //There is no registered post for this purpose.
            return false;
        }
        return $result['id'];
    }

    /**
     * Binds the current query internally one time if it has not already
     * been bound, otherwise does nothing, making it safe to call repeatedly
     * without issue.
     * @global WP_Query $wp_query
     * @return void
     * @internal
     */
    private function _initializeCurrentQuery()
    {
        if ( is_null( self::$current_query ) )
        {
            global $wp_query;
            self::$current_query = &$wp_query;
            $this->query = &self::$current_query;
        }
    }

    /**
     * Stops operation if an unsafe call is made that would cause a fatal error.
     *
     * @param string $method The method from this class that referenced
     *     this method, so that it can be entered into the exception message
     *     if an exception is thrown.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If an operation is requested that is unsafe to run prior
     *     to Wordpress internals completing their initial setup,
     *     which prevents a whitescreen fatal error from occurring,
     *     and gives some context in the exception message as to what
     *     the issue is. A method such as this really should be present
     *     in the Wordpress core, but since it is not, this fits the bill
     *     quite nicely.
     * @internal
     */
    private function _checkQueryExecution( $method )
    {
        if ( !$this->isExecuted() )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'The requested method [%1$s] in [%2$s] cannot be resolved '
                . 'yet because the underlying query has not run. This exception '
                . 'gives you an opportunity to handle the issue rather than '
                . 'face a potential whitescreen because of Wordpress Core '
                . 'throwing a fatal error. You can test for the correct '
                . 'action to run your logic in with the `%2$s::isExecuted` '
                . 'method from this object, which will return true when '
                . 'your request is safe.', $method, get_class( $this ) )
            );
        }
    }

}
