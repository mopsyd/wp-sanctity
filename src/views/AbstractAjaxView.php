<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\views;

/**
 * Abstract Ajax View
 * The base level Sanctity view class for ajax requests.
 *
 * This is pretty much your typical MVC view object, except it only handles
 * output for ajax calls. It presents output without opinion as to why,
 * and does not mutate operational state unless told to do so.
 * Strictly dependency injected, and all that good stuff.
 *
 * The Ajax view will typically serve content strictly as json data
 * for easy REST implementation.
 *
 * Sanctity uses a class as it's view base instead of the common approach of
 * treating the template like the View. This gives you a bit of flexibility if
 * you want your render not to be married to Twig for some reason, and you can
 * extend this class to define a different render method that still works
 * seamlessly with the underlying theme.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractAjaxView
    extends AbstractView
{

    /**
     * Represents paths that the View will instruct the templater to look for
     * templates when the `render` method is called.
     * @var array
     */
    private $template_paths = array();

    /**
     * Represents any contextual data passed from the controller that the
     * view is expected to pass to the template for direct representation.
     * This data will be set when the `render` method is called.
     * @var array
     */
    private $context = array();

    /**
     * Represents any headers that the View should set prior to render.
     * These are set by calling the `commitHeaders` method.
     * @var array
     */
    private $headers = array(
        // Sets the content type to json.
        'Content-Type' => 'application/json; charset=utf-8',
        // Prevents all requests not originating from the same host by default.
        // This needs to be explicitly overridden by the controller to allow
        // cross-origin ajax requests. Paranoid security is the default.
        'Access-Control-Allow-Origin' => self::HTTP_HOST,
        // By default, only typical web request methods are allowed,
        // and also head for expediency.
        'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, HEAD',
        // Max age of one day by default.
        'Access-Control-Max-Age' => 86400,
    );

    /**
     * Represents the content type of the View.
     * Default is json output.
     * @var string
     */
    private $content_type = 'application/json';

    /**
     * The default AJAX View constructor.
     *
     * Blocks instantiation if the class is not correctly configured,
     * which enforces integrity in child class logic, so abstraction
     * maintains expected integrity.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     Thrown if required view properties are not configured correctly.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Sets any headers registered with the View.
     *
     * This method returns an instance of self for method chaining.
     *
     * @return \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    public function commitHeaders()
    {
        foreach ( $this->headers as
            $key =>
            $header )
        {
            header( $key . ': ' . $header, true );
        }
        return $this;
    }

    /**
     * Renders the current template.
     *
     * If $return is true, the response body will be returned as a
     * string variable, or a stringable object that can be directly echoed.
     * If false, the View will echo the output directly, and return an instance
     * of itself for method chaining.
     *
     * @param bool $return (optional) If return is true,
     *     the render data will be returned as a value. If false,
     *     it will be echoed directly and the View will return
     *     an instance of self. Default false.
     * @return string|\mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    public function render( $return = false )
    {
        $this->_registerContext();
        $this->_buildDebugData();
        $data = json_encode( $this->context );
        if ( $return )
        {
            return $data;
        }
        $this->commitHeaders();
        echo $data;
        exit;
    }

    public function renderPart( $template, $context = array(), $return = false )
    {
        $data = json_encode( $context );
        if ( $return )
        {
            return $data;
        }
        $this->commitHeaders();
        echo $data;
        exit;
    }

}
