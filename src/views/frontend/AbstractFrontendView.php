<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\views\frontend;

/**
 * Abstract Frontend View
 * This class can be extended to provide custom render modifications
 * to the site frontend.
 *
 * This view is enabled by default, and can be configured through the Sanctity
 * settings panel in the admin dashboard. Extending this class is mostly superfluous,
 * but the capacity to do so is provided for completion. Generally, modifications
 * should be done by either plugin integration, registering an extension,
 * or overriding a controller in the majority of cases. The ability to override
 * the view is not excluded though, as it may be useful, particularly if you are
 * trying to serve content external to the wordpress system, or want to use a
 * post-processor for markdown or some similar construct to compile your html.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractFrontendView
extends \mopsyd\sanctity\views\AbstractView
implements \mopsyd\sanctity\interfaces\views\frontend\SanctityFrontendViewInterface
{
    const VIEW_SCOPE = 'frontend';

    public function __construct(){
        parent::__construct();
    }

}
