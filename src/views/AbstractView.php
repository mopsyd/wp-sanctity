<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\views;

/**
 * Abstract View
 * The base level Sanctity view class.
 *
 * This is pretty much your typical MVC view object. It presents output without
 * opinion as to why, and does not mutate operational state unless told to do so.
 * Strictly dependency injected, and all that good stuff.
 *
 * Sanctity uses a class as it's view base instead of the common approach of
 * treating the template like the View. This gives you a bit of flexibility if
 * you want your render not to be married to Twig for some reason, and you can
 * extend this class to define a different render method that still works
 * seamlessly with the underlying theme.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractView
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\views\SanctityViewInterface
{

    /**
     * By default Views use the viewmap config.
     */
    const CONFIG_FILE = 'viewmap.json';

    /**
     * The adapter used to power the render engine.
     *
     * The view defers to this object to perform the actual render step,
     * and is concerned primarily organization of the values it receives
     * from the controller, and insuring they are presented to the render
     * engine in a uniform way. The render engine simply insures that
     * output happens, without much regard to what is being output.
     *
     * The view is not concerned with what values it sets in the render engine,
     * but only that they are presented in a standardized format to the render engine.
     * The controller makes all decisions about what values the view gets,
     * and the view simply streamlines them for output.
     *
     * @note Currently this is not overridable, and the abstract view enforces
     *     this by checking the constant using "self" instead of late static binding.
     *     In a future release this will be loosened to allow for alternate render outputs.
     *     A standardized render adapter interface has to be implemented
     *     prior to this occurring though. Until that happens Twig is all you get.
     */
    const RENDER_ADAPTER = 'twig\\TwigAdapter';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * This class constant provides scope to the view,
     * just like its controller and model counterparts
     * (eg: frontend, backend, login, etc).
     */
    const VIEW_SCOPE = false;

    /**
     * Represents the baseline template used if no other template is defined.
     * This should be the default root template used for presentation in the
     * current scope if no edge case is present.
     */
    const VIEW_DEFAULT_TEMPLATE = 'base.twig';

    /**
     * Represents the base directory for templates, relative to the theme root.
     * This is used to register search directories, and is set as a class constant
     * so it can be overridden or easily altered during updates without
     * disrupting logic or requiring extensive refactor.
     */
    const VIEW_TEMPLATE_BASE = 'templates';

    /**
     * Represents the configuration used by the View to determine
     * its load parameters that are generalized enough that they
     * do not need to be a concern for other internal logic.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $view_config;

    /**
     * The render engine used to produce visible markup.
     * By default this is Twig.
     *
     *
     * @var \mopsyd\sanctity\interfaces\adapters\render\RenderAdapterInterface
     */
    private static $render_adapter;

    /**
     * Represents the currently set template that the view will render
     * when the `render` method is called.
     * @var string
     */
    private $template;

    /**
     * Represents paths that the View will instruct the templater to look for
     * templates when the `render` method is called.
     * @var array
     */
    private $template_paths = array();

    /**
     * Represents any contextual data passed from the controller that the
     * view is expected to pass to the template for direct representation.
     * This data will be set when the `render` method is called.
     * @var array
     */
    private $context = array();

    /**
     * Represents any headers that the View should set prior to render.
     * These are set by calling the `commitHeaders` method.
     * @var array
     */
    private $headers = array();

    /**
     * Represents the content type of the View.
     * Default is standard HTML output.
     * @var string
     */
    private $content_type = 'text/html';

    /**
     * Represents the \Sanctity object used to assist in rendering.
     * This should be passed in by the controller via the `setSite` method.
     * @var \Sanctity
     */
    private $site;

    /**
     * The default source locations that are automatically added as search paths.
     * This array allows this class to add additional system search paths without
     * altering the codebase, and is primarily a maintenance utility for ongoing
     * development.
     * @var array
     */
    private static $default_sources = array(
        'global'
    );

    /**
     * The default View constructor.
     *
     * Blocks instantiation if the class is not correctly configured,
     * which enforces integrity in child class logic, so abstraction
     * maintains expected integrity.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     Thrown if required view properties are not configured correctly.
     */
    public function __construct()
    {
        $this->_checkViewIntegrity();
        $this->_loadViewConfig();
        $this->_initializeRenderEngine();
        $this->_registerViewDefaults();
        parent::__construct();
        $this::getAdapter()->setView( $this );
    }

    /**
     * Sets the Sanctity Front Controller object, which is operated upon by the View.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site
     */
    public function setSite( \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site )
    {
        $this->site = $site;
        return $this;
    }

    /**
     * Sets HTTP headers that should be declared prior to render.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param type $headers
     */
    public function setHeaders( array $headers )
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * Sets the root template that the view is expected to render.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param type $template
     */
    public function setTemplate( $template )
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Sets any headers registered with the View.
     *
     * This method returns an instance of self for method chaining.
     *
     * @return \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     * @todo Integrate header setting functionality
     */
    public function commitHeaders()
    {
        return $this;
    }

    /**
     * Renders the current template.
     *
     * If $return is true, the response body will be returned as a
     * string variable, or a stringable object that can be directly echoed.
     * If false, the View will echo the output directly, and return an instance
     * of itself for method chaining.
     *
     * @param bool $return (optional) If return is true,
     *     the render data will be returned as a value. If false,
     *     it will be echoed directly and the View will return
     *     an instance of self. Default false.
     * @return string|\mopsyd\sanctity\interfaces\views\SanctityViewInterface
     * @todo Integrate render functionality
     */
    public function render( $return = false )
    {
        $this->_registerContext();
        $this->_buildDebugData();
        $this->_registerTemplateSearchPaths();
        if ( $return )
        {
            ob_start();
            self::$render_adapter->render( $this->template, $this->context );
            return ob_get_clean();
        }
        self::$render_adapter->render( $this->template, $this->context );
        return $this;
    }

    public function renderPart( $template, $context = array(), $return = false )
    {
        if ( $return )
        {
            ob_start();
            self::$render_adapter->render( $template, $context );
            return ob_get_clean();
        }
        self::$render_adapter->render( $template, $context );
        return $this;
    }

    /**
     * Provides a group of paths to the RenderAdapter, so it can map relative paths
     * to their concrete base directories, and create a hierarchical load order
     * for the templates.
     *
     * @param string $name an identifying key for the group of paths to register,
     *     eg: "template", "widgets", "plugin-name", etc.
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $pathset
     *     Represents a container of the relative paths to register.
     * @return void
     */
    public function registerPathset( $name,
        \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $pathset )
    {
        foreach ( $pathset as
            $key =>
            $path )
        {
            $name = trim( $path, '\\/' ) . '_' . $key;
            if ( !self::$render_adapter->hasPath( $name ) )
            {
                self::$render_adapter->addPath( $name, $path );
            }
        }
    }

    /**
     * Preflights the View object to insure its integrity prior to instantiation.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     Thrown if required view properties are not configured correctly.
     * @internal
     */
    private function _checkViewIntegrity()
    {
        $message = 'View [%1$s] is misconfigured. Expected %2$s [%3$s] is not defined. '
            . 'Please contact the developer of [%1$s] to correct this issue.';
        if ( !$this::VIEW_SCOPE )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( $message, get_class( $this ), 'class constant',
                'VIEW_SCOPE' )
            );
        }
    }

    /**
     * Registers the default settings for the View object,
     * if they have not already been set.
     * @return void
     * @internal
     */
    private function _registerViewDefaults()
    {
        $this->_setDefaultTemplate();
        $this->_setDefaultTemplateDirectories();
    }

    /**
     * Sets the default template upon object instatiation.
     * This can be overridden at any time.
     */
    private function _setDefaultTemplate()
    {
        if ( is_null( $this->template ) )
        {
            $this->template = $this::VIEW_DEFAULT_TEMPLATE;
        }
    }

    /**
     * Creates the baseline set of template paths, and returns a result set of
     * fully qualified paths, with the child theme holding priority over the
     * parent theme for each given path location.
     */
    private function _setDefaultTemplateDirectories()
    {
        $relative_paths = array();
        $template_prefix = $this::VIEW_TEMPLATE_BASE;
        //get the sanctity install directory
        $sources = self::$default_sources;
        if ( !in_array( $this::VIEW_SCOPE, $sources ) )
        {
            $sources[] = $this::VIEW_SCOPE;
        }
        foreach ( $sources as
            $source )
        {
            $relative_paths[] = $template_prefix
                . DIRECTORY_SEPARATOR
                . $source
                . DIRECTORY_SEPARATOR;
        }
        $this->template_paths = $this->_createTemplatePathSet( $relative_paths );
    }

    /**
     * Converts a provided set of relative paths to a
     * set of absolute disk locations to search for templates.
     *
     * This method will add both the child theme directory as well as the
     * base theme directory, if they are valid directories, and return a set
     * with the child theme directory holding priority over the parent theme
     * for each given location.
     *
     * If a new path is supplied, it will be prefixed to the set,
     * so it has a higher search priority than the existing set.
     *
     * @param array $paths Paths to create
     * @param array $template_paths (optional) A set of existing template paths.
     *     If supplied, additional search paths will be added to the existing array,
     *     but only if they do not already exist within it.
     * @return array
     * @internal
     */
    private function _createTemplatePathSet( $paths, $template_paths = array() )
    {
        $theme_dir = SANCTITY_BASEDIR;
        $child_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR;
        foreach ( $paths as
            $relative )
        {
            $fullpath = $theme_dir . $relative . DIRECTORY_SEPARATOR;
            if ( is_readable( $fullpath ) && !in_array( $fullpath,
                    $template_paths ) )
            {
                array_unshift( $template_paths, $fullpath );
            }
            if ( $child_dir !== $theme_dir )
            {
                $fullpath = $child_dir . $relative . DIRECTORY_SEPARATOR;
                if ( is_readable( $fullpath ) && !in_array( $fullpath,
                        $template_paths ) )
                {
                    array_unshift( $template_paths, $fullpath );
                }
            }
        }
        return $template_paths;
    }

    /**
     * Sets the template paths in the Render Adapter object for rendering within scope.
     * This method will replace but not remove existing paths.
     * @return void
     * @internal
     */
    private function _registerTemplateSearchPaths()
    {
        if ( array_key_exists( 'layout', $this->context ) )
        {
            $this->_mergeLayoutPaths( $this->context['layout']['paths'] );
        }
        $paths = $this->context['layout']->paths;
    }

    /**
     * Sets the local context into the Site object. Existing context data
     * may be replaced but not removed.
     * @return void
     * @internal
     */
    private function _registerContext()
    {
        $this->context = $this->getContextData()->toArray();
    }

    /**
     * Localizes the layout path to the Twig templates, if the path exists.
     *
     * This is the method that allows layouts to call their own files
     * relative to their layout root.
     *
     * @param array $layout An array of relative layout paths
     * @return void
     * @internal
     */
    private function _mergeLayoutPaths( array $layouts )
    {
        $register = array();
        foreach ( $layouts as
            $layout )
        {
            $register[] = $this::VIEW_TEMPLATE_BASE
                . DIRECTORY_SEPARATOR . $this::VIEW_SCOPE
                . DIRECTORY_SEPARATOR . $layout;
        }
        $this->template_paths = $this->_createTemplatePathSet( $register,
            $this->template_paths );
    }

    private function _loadViewConfig()
    {
        if ( is_null( self::$view_config ) )
        {
            self::$view_config = $this::containerize(
                    array_replace_recursive(
                        $this->getConfig( 'global' ),
                        $this->getConfig( $this::VIEW_SCOPE )
                    )
            );
//            d(self::$view_config, self::$view_config->toArray()); exit();
        }
    }

    /**
     * Loads the render engine if it is not already defined.
     * @return void
     * @internal
     * @deprecated This should be replaced with dependency injection.
     */
    private function _initializeRenderEngine()
    {
        if ( is_null( self::$render_adapter ) )
        {
            self::$render_adapter = $this->load( 'library',
                $this::RENDER_ADAPTER );
        }
    }

    private function _buildDebugData()
    {
        if ( defined( 'WP_DEBUG' ) && WP_DEBUG && is_user_logged_in()
            && current_user_can( 'manage_options' ) ) // <-- this needs to move to the platform adapter
        {
            $controller_debug = array_key_exists( 'debug', $this->context )
                ? $this->context['debug']
                : false;
            $debug = array(
                'view_class' => get_class( $this ),
                'view_scope' => $this::VIEW_SCOPE,
                'template_paths' => $this->template_paths,
                'page_type' => $this->context['page']['type'],
                'root_template' => $this->template,
                'render_context' => $this->context,
                'view_data' => $this::debugGet(),
                'controller_data' => $controller_debug,
                'origin' => __METHOD__
            );
            $this->context['debug'] = $debug;
            foreach ( $this->context['debug'] as
                $key =>
                $dataset )
            {
                ob_start();
                s( $dataset );
                $this->context['debug'][$key] = htmlentities( ob_get_clean() );
            }
            foreach ( $this->context as
                $key =>
                $dataset )
            {
                if ( $key === 'debug' )
                {
                    continue;
                }
                ob_start();
                s( $dataset );
                $this->context['debug'][$key] = ob_get_clean();
            }
            // This page is used for testing new features, and this will be removed when a release is offerred.
            // It really only works on my own website anyhow, so it shouldn't be an issue for anyone.
            $this->context['debug']['sandbox'] = 'https://mopsyd.com/sandbox/';
        } else
        {
            //kill any debug data passed prior to
            //render in case the controller poked
            //any through.
            $this->context['debug'] = false;
        }
    }

}
