<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\views\login;

/**
 * Abstract Login View
 * This class can be extended to provide custom render modifications
 * to the login page. The default behavior defers to the standard wordpress
 * functionality by default, unless the login theme is explicitly enabled.
 *
 * Be very careful overriding this view. If you mess it up, you may wind up
 * entirely locking yourself out of the login. It is advised to test changes
 * to the Login view extensively prior to committing them, and if possible,
 * use some manner of kill switch until they are confirmed to be stable, or
 * leave some kind of backdoor for yourself during development
 * (DO NOT LEAVE A BACKDOOR ACTIVE IN PRODUCTION CODE).
 * You have been warned.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractLoginView
extends \mopsyd\sanctity\views\AbstractView
implements \mopsyd\sanctity\interfaces\views\login\SanctityLoginViewInterface
{
    const VIEW_SCOPE = 'login';

    public function __construct(){
        parent::__construct();
    }

}
