<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\views\admin;

/**
 * Abstract Admin View
 * Provides the default rendering of Sanctity for the Admin Dashboard.
 *
 * This class can be extended to provide custom render modifications
 * to the admin dashboard. The default behavior defers to the standard wordpress
 * functionality by default, unless the dashboard theme is explicitly enabled.
 *
 * Be very careful overriding this view. You may render your dashboard
 * horribly unusable if you mess it up. It is advised to use some kind of
 * construct for testing prior to committing anything, like requiring a $_GET
 * parameter to be present to see your changes, so that you do not disrupt
 * normal backend operations, or put yourself in a position where you have to
 * disable the theme to regain dashboard access. You have been warned.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractAdminView
extends \mopsyd\sanctity\views\AbstractView
implements \mopsyd\sanctity\interfaces\views\admin\SanctityAdminViewInterface
{
    const VIEW_SCOPE = 'admin';

    public function __construct(){
        parent::__construct();
    }

}
