<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers;

/**
 * Abstract Error Controller
 * The base level Sanctity controller class for errors.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractErrorController
    extends AbstractController
    implements \mopsyd\sanctity\interfaces\controllers\SanctityErrorControllerInterface
{

    private static $error_defaults = array(
        401 => array(
            'title' => 'Not Authorized',
            'heading' => 'Incorrect Authorization',
            'message' => 'You probably wish you wrote that password down somewhere now, don\'t you?.',
        ),
        403 => array(
            'title' => 'Forbidden',
            'heading' => 'All Kinds of Nope',
            'message' => 'You didn\'t think you could just stroll on in here without being invided now did you?',
        ),
        404 => array(
            'title' => 'Page not found',
            'heading' => 'Page Not Found',
            'message' => 'Nothing to see here folks.',
        ),
        500 => array(
            'title' => 'Server Error',
            'heading' => 'Looks Like Something Broke',
            'message' => 'We\'ll take a look into that and see what happened.',
        ),
    );

    /**
     * Represents any information about the error that was called,
     * if any was supplied.
     * @var array
     */
    private $error_context = array();

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles a page or request error based on the code provided.
     *
     * @param int $type (optional) If provided, should correspond to
     *     the http status code to set for the error response.
     *     The default code is 404 (not found).
     * @param type $message (optional) If not using the default error handling,
     *     this is the message that should display to the end user. If null,
     *     the default message for the error code will be used.
     * @param array $context (optional) Any contextual information about the
     *     error that should be handled internally, but not provided for public
     *     display if debug mode is not enabled. This includes but
     *     is not limited to logging info, admin messages,
     *     identifying information about the request, etc.
     */
    public function error( $type = 404, $message = null, array $context = null )
    {
        $this->_handleErrorInfo( $type, $context );
        $this->handleError( $type, $message );
        exit();
    }

    /**
     * Handles the error. This fires immediately after the error info is handled,
     * so any modifications to that info should be available within this method
     * using `getErrorInfo`.
     * @param type $type
     * @param type $message
     */
    protected function handleError( $type, $message = null )
    {
        $model = $this->load( 'model', 'display\\LayoutModel' );
        $this->addContext('layout', $model->getSettings());
        $model = $this->load( 'model', 'display\\BrandingModel' );
        $this->addContext('branding', $model->getSettings());
        $model = $this->load( 'model', 'display\\ErrorModel' );
        $model->initialize( $type, $this->getErrorContext() );
        $this->bindDataContext( $model );
        $this->render();
    }

    /**
     * Renders the page after all validation, data aggregation,
     * and other logic and opinion has been applied.
     *
     *
     * @param int|string $page The id or slug of the page to render.
     *     If no page is provided, it will be extracted from the registered
     *     contextual information. If "error" is provided, the correct error
     *     page will be extracted from the contextual information.
     * @param mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     *     If not supplied, the default view will be used. If a view object is supplied,
     *     the provided view object will be initialized and used in place of the default.
     * @return void
     * @note This method will terminate execution upon completion.
     */
    protected function render( $page = null,
        \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view = null )
    {
        if ( !is_null( $page ) )
        {
            d( 'Update here to render arbitrary pages.', $page );
            exit();
        }
        $layout = $this->getLayout();
        $this->addContext( 'layout', $layout );
        $layout = $this->getLayout();
        $this->addContext( 'layout', $layout );
        $view = $this->getView( $view );
        $view->registerPathset( 'layout',
            $this::containerize( $layout['paths'] ) );
        $view->render();
    }

    /**
     * Returns any contextual error information supplied when the
     * error was called. This should be available from within the
     * `handleError` method.
     *
     * @return array
     */
    protected function getErrorContext()
    {
        return $this->error_context;
    }

    /**
     * Override this method to provide conditional logic as to whether to use
     * the Sanctity error page rendering engine or default to the standard
     * wordpress error handler.
     *
     * @return bool
     */
    protected function useErrorRenderer()
    {
        return true;
    }

    /**
     * Provided for future extension to possibly include Psr-7 integration.
     *
     * The default behavior is to just set the correct error status code
     * using the default PHP response code setter. There's no need to call
     * Wordpress for this, the page is done as far as Wordpress is concerned
     * if we're hitting this method anyhow.
     *
     * @param int $type An HTTP status code
     */
    protected function setErrorHeaders( $type, $message = null )
    {
        http_response_code( $type );
    }

    /**
     * Sets the error internal context data if it was supplied when the error was called.
     * @param int $type The http status code passed
     * @param array $context Any private data passed, or null if none was
     * @return void
     * @internal
     */
    private function _handleErrorInfo( $type, array $context = null )
    {
        if ( !is_null( $context ) )
        {
            $this->error_context = $context;
        }
    }

}
