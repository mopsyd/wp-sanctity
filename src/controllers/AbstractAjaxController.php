<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers;

/**
 * Abstract Ajax Controller
 * The base level Sanctity ajax controller class.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractAjaxController
    extends AbstractController
    implements \mopsyd\sanctity\interfaces\controllers\SanctityAjaxControllerInterface
{

    private $ajax_initialized = false;

    public function __construct()
    {
        parent::__construct();
        $this->_initializeAjax();
    }

    /**
     * Renders the page after all validation, data aggregation,
     * and other logic and opinion has been applied.
     *
     *
     * @param int|string $page The id or slug of the page to render.
     *     If no page is provided, it will be extracted from the registered
     *     contextual information. If "error" is provided, the correct error
     *     page will be extracted from the contextual information.
     * @param mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     *     If not supplied, the default view will be used. If a view object is supplied,
     *     the provided view object will be initialized and used in place of the default.
     * @return void
     * @note This method will terminate execution upon completion.
     */
    protected function render( $page = null,
        \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view = null )
    {
        if ( !is_null( $page ) )
        {
            d( 'Update here to render arbitrary pages.', $page );
            exit();
        }
        $layout = $this->getLayout();
        $this->addContext( 'layout', $layout );
        $layout = $this->getLayout();
        $this->addContext( 'layout', $layout );
        $view = $this->getView( $view );
        $view->registerPathset( 'layout',
            $this::containerize( $layout['paths'] ) );
        $view->render();
    }

    private function _initializeAjax()
    {
        if (!self::$ajax_initialized)
        {
            $this->_parseRequestHeaders();
            $this->_queueContentHeaders();
            self::$ajax_initialized = true;
        }
    }

    private function _parseRequestHeaders()
    {

    }

    private function _queueContentHeaders()
    {

    }

    private function _checkAuthorization()
    {

    }

    private function _checkCsrf()
    {

    }
}
