<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to it persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers;

/**
 * Abstract Sanctity Controller
 * The base level Sanctity controller class.
 *
 * The Sanctity controller maximizes interoperability with Twig,
 * Wordpress, and other external systems.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @note A lot of the existing logic here is going to get ported out
 *     to dedicated classes to handle it. This is all tenative for the
 *     sake of proof of concept for the time being.
 */
abstract class AbstractController
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\controllers\SanctityControllerInterface
{

    /**
     * By default Controllers use the core config.
     */
    const CONFIG_FILE = 'core.json';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Designates the realm of representation that a controller operates within.
     * This MUST be defined properly for the controller to operate.
     *
     * The default value of false will immediately force the program to
     * exit due to a misconfigured controller class.
     *
     *
     * @example 'frontend', 'admin', 'login', 'ajax', 'rest', 'xmlrpc', etc.
     */
    const CONTROLLER_SCOPE = false;

    /**
     * An array of Twig extensions that are always required,
     * and cannot be overridden through child class logic.
     * @var array
     */
    private static $core_twig_extensions = array(
        'Twig_Extension_StringLoader'
    );

    /**
     * Represents precompiled assets known to point to
     * valid script, stylesheet, and font resources.
     * @var array
     */
    private static $cdn_assets;

    /**
     * Represents a persistent reference to page metadata that should
     * be passed securely to frontend scripts.
     * @var array
     */
    private static $page_data = array();

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_options_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_actions_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_filters_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_taxonomies_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_post_types_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_context_bound = false;

    /**
     * Core binding only needs to occur one time
     * @var boolean
     */
    private static $core_twig_extensions_bound = false;

    /**
     * Represents the default view used if no other view is specified.
     *
     * When render operations are called, if no view object is passed,
     * this one will be used as the default.
     *
     * @var \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    private static $view;

    /**
     * Represents the Sanctity Front Controller object.
     * @var \Sanctity
     */
    private static $site;

    /**
     * Represents the order in which the typical vanilla wordpress actions
     * fire on a typical request. This is just for quick reference,
     * and is not used internally.
     * @var array
     */
    private static $actions_sequence = array(
        'muplugins_loaded',
        'plugins_loaded',
        'sanitize_comment_cookies',
        'setup_theme', //Never register theme actions until here.
        'unload_textdomain',
        'load_textdomain',
        'after_setup_theme', //Theme initial setup should be done here. Any extensions provided must be registered prior to this.
        'auth_cookie_malformed', //Render 403 and exit.
        'auth_cookie_valid',
        'set_current_user',
        'init', //Safe for checking WP_Query
        'registered_post_type',
        'registered_taxonomy',
        'widgets_init',
        'register_sidebar', //Add theme sidebar sections here.
        'wp_register_sidebar_widget', //Register widgets here.
        'wp_loaded', //Wordpress fully initialized. All internals should be safe to use.
        'parse_tax_query',
        'posts_selection',
        'template_redirect',
        'admin_bar_init',
        'add_admin_bar_menus',
        'get_header',
        'wp_head',
        'wp_enqueue_scripts', //Queue all CSS and stylesheets
        'wp_print_styles',
        'wp_print_scripts',
        'get_template_part_content',
        'begin_fetch_post_thumbnail_html',
        'end_fetch_post_thumbnail_html',
        'get_template_part_content',
        'begin_fetch_post_thumbnail_html',
        'end_fetch_post_thumbnail_html',
        'get_sidebar',
        'dynamic_sidebar_before',
        'dynamic_sidebar',
        'dynamic_sidebar_after',
        'get_footer',
        'wp_footer',
        'wp_print_footer_scripts',
        'wp_before_admin_bar_render',
        'wp_after_admin_bar_render',
        'shutdown',
    );

    /**
     * Represents an array of action bindings, and operations to call when they fire.
     * @var array
     */
    private $actions = array();

    /**
     * Represents whether the action master callback has been registered,
     * which only happens if any actions are registered.
     * @var bool
     */
    private $actions_registered = false;

    /**
     * Represents an array of actions that have fired already.
     * @var array
     */
    private static $actions_fired = array();

    /**
     * Represents an array of filter bindings, and operations to call when they fire.
     * @var array
     */
    private $filters = array();

    /**
     * Represents whether the filter master callback has been registered,
     * which only happens if any filters are registered.
     * @var bool
     */
    private $filters_registered = false;

    /**
     * Represents an array of filters that have fired already.
     * @var array
     */
    private static $filters_fired = array();

    /**
     * -------------------------------------------------------
     *                      Initialization
     * -------------------------------------------------------
     */

    /**
     * Default Sanctity Controller Constructor
     * Instantiates a Sanctity Controller object
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If an extension class breaks a method required for correct instantiation,
     *     this exception will be thrown. This indicates faulty extension logic,
     *     and should not be caught. The developer of the extension logic should
     *     be notified to correct the issue.
     */
    public function __construct()
    {
        //Forces all subsequent controller loads to load in the same scope
        //as the original determined by the router.
        //This prevents any out-of-scope controllers from being used,
        //and forces implementing logic to create controllers that are
        //appropriately scoped. This alone prevents a huge degree of
        //sloppy programming practices that lead to tons of bugs.
        //Don't use the frontend error controller on the backend for example,
        //or the backend controller on a frontend page view,
        //because that will easily allow security breaches.
        //We prevent any controllers that are not in scope
        //from loading at all by doing this to entirely
        //circumvent the issue before it even happens.
        //The adapter allows subsequent declarations
        //of identical scope, but no changes to scope once it has been set.
        \mopsyd\sanctity\adapters\AbstractAdapter::setScope( $this::CONTROLLER_SCOPE );
        try
        {
            $this->_initializeScopes();
            $this->_initializeView();
            $this->_initializeCoreBindings();
//            $this->bindClientData( 'test-data',
//                sprintf( 'This is test data for the frontend via the abstract controller [%1$s] ',
//                    __METHOD__ ) );
        } catch ( \Exception $e )
        {
            //If anyone sets up any broken overrides, this should catch it.
            $this::debugStash( $e->getMessage(), 'initialization_error' );
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException( sprintf(
                'Controller instantiation of [%1$s] could not resolve,'
                . ' due to an exception of type [%2$s] encountered during '
                . 'instantiation, with message [%3$s]. Please contact the '
                . 'developer of [%1$s] and request assistance.',
                get_class( $this ), get_class( $e ), $e->getMessage()
            ), $e->getCode(), $e );
        }
        $this::debugStash( __FUNCTION__, 'initialization_steps_completed' );
        parent::__construct();
    }

    /**
     * Performs any operations that must occur after Sanctity, Twig,
     * and any other underlying dependencies have properly instantiated.
     */
    public function initialize()
    {
        try
        {
            //Obtain a persistent reference to the context.
            $this->_bindCoreContext( '' );
            //Load the view object, for all render operations.
        } catch ( \Exception $e )
        {
            //If anyone sets up any broken overrides, this should catch it.
            $this::debugStash( $e->getMessage(), 'initialization_error' );
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException( sprintf(
                'Controller initialization of [%1$s] could not resolve,'
                . ' due to an exception of type [%2$s] encountered during '
                . 'initialization, with message [%3$s]. Please contact the '
                . 'developer of [%1$s] and request assistance.',
                get_class( $this ), get_class( $e ), $e->getMessage()
            ), $e->getCode(), $e );
        }
        $this::debugStash( __FUNCTION__, 'initialization_steps_completed' );
    }

    /**
     * -------------------------------------------------------
     *               Fixed Wordpress Bindings
     * -------------------------------------------------------
     */

    /**
     * Data provider method for the template.
     * This method is meant to pass secure data that
     * is to be consumed by javascript.
     *
     * @param int|string $key
     * @param bool $encoded (optional) if true, the value
     *     will be returned as a json_encoded string
     * @return bool|mixed returns false if no data found,
     *     otherwise returns whatever value was queued
     * @final
     */
    final public function getFrontendData( $key = null, $encoded = false )
    {
        if ( !is_null( $key ) && !array_key_exists( $key, self::$page_data ) )
        {
            return false;
        }
        $data = self::$page_data;
        if ( !is_null( $key ) && array_key_exists( $key, self::$page_data ) )
        {
            $data = $data[$key];
        }
        $value = ($encoded
            ? json_encode( $data )
            : $data);
        $this::debugStash( $value, 'frontend_data' );
        return $value;
    }

    /**
     * This method routes declared actions to the internal queue.
     *
     * It processes the queue in the same order it was registered.
     * This allows for an expected process of operation that can be
     * analyzed at any time to determine the execution plan of the
     * controller on the fly, despite the fact that Wordpress offers
     * no such functionality otherwise.
     *
     * @return void
     */
    final public function routeAction()
    {
        $key = current_filter();
        $key_index = array_keys( $this->actions[$key] );
        if ( empty( $key_index ) )
        {
            //if an action is called repeatedly and has exhausted our queue, skip it.
            return;
        }
        $priority = $key_index[0];
        $args = func_get_args();
        $queue = $this->actions[$key][$priority];
        $log_key = count( self::$actions_fired ) + 1;
        self::$actions_fired[$log_key] = array(
            'action' => $key,
            'priority' => $priority,
            'queue' => array()
        );
        foreach ( $queue as
            $method )
        {
            //Track the order of operations by
            //action sequence fired for debugging
            //if required.
            self::$actions_fired[$log_key]['queue'][] = $method;
            $this->$method( $args );
        }
        $this::debugStash( array(
            'hook' => $key,
            'priority' => $priority,
            'methods' => $this->actions[$key][$priority]
            ), 'action_hook_fired' );
        unset( $this->actions[$key][$priority] );
    }

    /**
     * This method routes declared filters to the internal queue.
     *
     * It processes the queue in the same order it was registered.
     * This allows for an expected process of operation that can be
     * analyzed at any time to determine the execution plan of the
     * controller on the fly, despite the fact that Wordpress offers
     * no such functionality otherwise.
     *
     * @param mixed $value The filter input
     * @return mixed The filtered byproduct
     */
    final public function routeFilter( $value )
    {
        $key = current_filter();
        $key_index = array_keys( $this->filters[$key] );
        $priority = $key_index[0];
        $args = func_get_args();
        $queue = $this->filters[$key][$priority];
        $log_key = count( self::$filters_fired ) + 1;
        self::$filters_fired[$log_key] = array(
            'filter' => $key,
            'priority' => $priority,
            'queue' => array()
        );
        foreach ( $queue as
            $method )
        {
            //Track the order of operations by
            //filter sequence fired for debugging
            //if required.
            self::$filters_fired[$log_key]['queue'][] = $method;
            $value = $this->$method( $value );
        }
        $this::debugStash( array(
            'hook' => $key,
            'priority' => $priority,
            'methods' => $this->filters[$key][$priority]
            ), 'filter_hook_fired' );
        return $value;
    }

    /**
     * This method is used for dependency injection of the primary
     * Sanctity front controller instance.
     *
     * This method is only used so that the front controller can
     * dependency inject a copy of itself during initial bootstrap.
     *
     * This method may not be overridden.
     *
     * @param \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site
     * @final
     */
    final public function setSiteObject( \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site )
    {
        self::$site = $site;
        $this::debugStash( get_class( $site ), 'site_object_defined' );
    }

    /**
     * This method is registered into the work queue to provide
     * theme scripts and stylesheets to Wordpress for queueing. This method
     * may not be overridden. To provide additional scripts, override
     * `declareScripts`, and to provide additional stylesheets, override
     * `declareStylesheets`.
     *
     * This method is implemented in such a way that it will fire absolutely
     * dead last in the queue, with the highest priority available by registering its
     * priority using PHP_INT_MAX, which resolves to the highest possible integer
     * value on both 32 and 64 bit architectures without issue, and prevents any
     * higher priority hooks from overriding it.
     *
     * This insures that the frontend display of any override frontend assets
     * from the theme are not disrupted by faulty
     * plugin logic, and that overrides of other scripts and stylesheets
     * previously registered can occur without obstruction.
     *
     * @return void
     * @internal
     * @see mopsyd\sanctity\controllers\AbstractController::declareScripts
     * @see mopsyd\sanctity\controllers\AbstractController::declareStylesheets
     */
    final public function queueAssets()
    {
        foreach ( $this->declareStyles() as
            $style )
        {
            $this->load( 'library', 'media\\Stylesheet' )->load( $style )->compile()->register();
            $this::debugStash( array(
                'name' => $style ), 'stylesheet', 'asset_queued' );
        }
        foreach ( $this->declareScripts() as
            $script )
        {
            $this->load( 'library', 'media\\Javascript' )->load( $script )->compile()->register();
            $this::debugStash( array(
                'name' => $script ), 'javascript', 'asset_queued' );
        }
    }

    /**
     * -------------------------------------------------------
     *                      Public Api
     * -------------------------------------------------------
     */

    /**
     * Handles a post, page, or what have you,
     * and displays the correct templating schema
     * regardless of what it is.
     *
     * @param type $post
     */
    public function display( $post )
    {
        $router = $this->load( 'router', 'WordpressRouter' );
        d( $router, $this );
        exit();
    }

    /**
     * Filters the context. This allows
     * properties to be added as needed.
     *
     * @param type $context
     * @return array
     */
    public function filterContext( $context )
    {
        $original = $context;
        $updated = $this->declareContext();
        $final = array_replace_recursive( $context, $this->declareContext() );
        $this::debugStash( get_class( $this ), 'context_filtered' );
        return $final;
    }

    public function handlePage( $page = null, $data = null )
    {
        if ( !is_null( $page ) )
        {
            $this->_parseCustomPageRequest( $page, $data );
            d( 'Handle arbitrary page selection here.', $page, $data );
            exit();
        }
        $this->render();
    }

    public function handlePart( $section = null, $data = null )
    {
        if ( !is_null( $data ) )
        {
            $this->addContext( 'partial', $data );
        }
        $view = $this->getView();
        $this::debugStash( array(
            'section' => $section,
            'view' => get_class( $view ),
            ), 'partial_render_called' );
        $view->renderPart( $section );
    }

    /**
     * Sets a value in the context cache
     *
     * @param string $key The key to set
     * @param mopsyd\sanctity\interfaces\libs\container\ContainerInterface $value
     *     The value to set. Must be a container object
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This exception is thrown if an invalid key is passed,
     *     or if the value is not a container.
     */
    public function addContext( $key, $value )
    {
        $this->load( 'library', 'config\\ContextConfig' )->set( $key, $value );
        $this::debugStash( array(
            'context' => $key ), 'context_added' );
    }

    /**
     * Deletes a key from the context cache
     *
     * @param int|string $key
     * @return boolean Returns true if the key was removed,
     *     and false if it did not exist (this can be used for sanity checks).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This exception is thrown if an invalid key is passed.
     *     This indicates external error.
     */
    public function removeContext( $key )
    {
        if ( !(is_int( $key ) || is_string( $key )) )
        {
            //Any other value is not a valid array key
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid call to [%1$s]. A key of type [%2$s] is not '
                . 'a valid array key.', __METHOD__, gettype( $key ) ) );
        }
        $context = $this->load( 'library', 'config\\ContextConfig' );
        if ( $context->delete( $key ) )
        {
            $this::debugStash( array(
                'context' => $key ), 'context_removed' );
            return true;
        }
        return false;
    }

    /**
     * This method will reset the context to its default state,
     * in the event that it is horribly mangled.
     *
     * It will also always restore the root Sanctity context,
     * but not any extension context of child classes (from declareContext).
     *
     * If you call this, and you also want the extension class contexts back,
     * then also call `$this->filterContext($this->getContext());`
     *
     * @return void
     */
    final public function resetContext()
    {
        $trace = debug_backtrace( 1 );
        $trace = $trace[1];
        $this::debugStash( array(
            'called_from' => $trace ), 'context_reset' );
        $this->initializeContext();
    }

    /**
     * Handles a page or request error based on the code provided,
     * and exits the program.
     *
     * @param int $type (optional) If provided, should correspond to
     *     the http status code to set for the error response.
     *     The default code is 404 (not found).
     * @param type $message (optional) If not using the default error handling,
     *     this is the message that should display to the end user. If null,
     *     the default message for the error code will be used.
     * @param type $context (optional) If provided, sets contextual
     *     error information for logging purposes, notifying admin, or what have you,
     *     which is not displayed publicly unless debug mode is enabled.
     * @note if `$use_wordpress` is true, the `$message` and `$template` will be ignored.
     * @return void
     */
    public function error( $type = 404, $message = null, array $context = null )
    {
        $controller = $this->load( 'controller',
            $this::CONTROLLER_SCOPE . '\\' . 'ErrorController' );
        $controller->initialize();
        $this::debugStash( array(
            'type' => $type,
            'message' => $message,
            'context' => $context,
            ), 'error_handler_called' );
        $controller->error( $type, $message, $context );
    }

    /**
     * Binds the core and declared Twig filters and extensions to Twig directly.
     * @param \Twig_Environment $twig
     * @return \Twig_Environment
     * @todo Refactor to use the binding api
     */
    final public function filterTwig( $twig )
    {
        $twig = $this->_loadCoreTwigExtensions( $twig );
        $twig = $this->_loadDeclaredTwigExtensions( $twig );
        $twig = $this->_loadDeclaredTwigFilters( $twig );
        return $twig;
    }

    /**
     * -------------------------------------------------------
     *                      Internal Api
     * -------------------------------------------------------
     */

    /**
     * Binds a class method to fire when a given action comes up.
     *
     * This method allows an internal queue of several methods to fire on
     * one filter for the same priority, which can be internally modified
     * between filters if need be, to provide granular control over logic
     * flow without exposing anything globally. All actions that fire are
     * logged internally in sequence, with the hook and priority level for
     * debugging purposes.
     *
     * This is an example of the Double Buffer pattern applied to Wordpress
     *
     * @see mopsyd\sanctity\controllers\AbstractController::routeFilter
     * @link http://gameprogrammingpatterns.com/double-buffer.html
     *
     * @param string $action A wordpress action
     * @param string $callback A method from the current object that
     *     is accessible in the current scope (generally protected or public).
     * @param int $priority (optional) The priority level for the action. The default
     *     is the same as the Wordpress default.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the callback method is not accessible in the current scope.
     * @note callbacks do not need to be public. The controller will register
     *     one inbound method that receives all bound actions, and call the
     *     appropriate internal based on that.
     * @note With filters, the automation of this cycle requires that only
     *     one parameter is ever accepted to avoid ambiguity and enforce a
     *     set of consist output conditions.
     */
    protected function bindFilter( $action, $callback, $priority = 10 )
    {
        $this->_verifyCallback( $callback );
        if ( !(array_key_exists( $action, $this->filters ) && array_key_exists( $priority,
                $this->filters[$action] ) )
            || !( $this->filters[$action][$priority][count( $this->filters[$action][$priority] ) -
            1] === $callback ) )
        {
            add_action( $action,
                array(
                $this,
                'routeFilter' ), $priority );
            $this->filters[$action][$priority][] = $callback;
            $this::debugStash( array(
                'action' => $action,
                'callback' => $callback,
                'priority' => $priority,
                ), 'filter_bound' );
            $this->filters_registered = true;
        }
    }

    /**
     * Binds a class method to fire when a given action comes up.
     *
     * This method allows an internal queue of several methods to fire on
     * one filter for the same priority, which can be internally modified
     * between filters if need be, to provide granular control over logic
     * flow without exposing anything globally. All actions that fire are
     * logged internally in sequence, with the hook and priority level for
     * debugging purposes.
     *
     * This is an example of the Double Buffer pattern applied to Wordpress
     *
     * @see mopsyd\sanctity\controllers\AbstractController::routeAction
     * @link http://gameprogrammingpatterns.com/double-buffer.html
     *
     * @param string $action A wordpress action
     * @param string $callback A method from the current object that
     *     is accessible in the current scope (generally protected or public).
     * @param int $priority (optional) The priority level for the action. The default
     *     is the same as the Wordpress default.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the callback method is not accessible in the current scope.
     * @note callbacks do not need to be public. The controller will register
     *     one inbound method that receives all bound actions, and call the
     *     appropriate internal based on that.
     */
    protected function bindAction( $action, $callback, $priority = 10 )
    {
        $this->_verifyCallback( $callback );
        if ( !(array_key_exists( $action, $this->actions ) && array_key_exists( $priority,
                $this->actions[$action] ) )
            || !( $this->actions[$action][$priority][count( $this->actions[$action][$priority] ) -
            1] === $callback ) )
        {
            add_action( $action,
                array(
                $this,
                'routeAction' ), $priority );
            $this->actions[$action][$priority][] = $callback;
            $this::debugStash( array(
                'action' => $action,
                'callback' => $callback,
                'priority' => $priority,
                ), 'action_bound' );
            $this->actions_registered = true;
        }
    }

    /**
     * Binds the data context from the model to the template context dataset.
     *
     * @param \mopsyd\sanctity\interfaces\models\SanctityModelInterface $model
     */
    protected function bindDataContext( \mopsyd\sanctity\interfaces\models\SanctityModelInterface $model )
    {
        $settings = $model->getSettings();
        $this->addContext( 'settings', $settings );
        $this->addContext( $model::MODEL_SETTINGS_KEY, $settings );
//        d($model);
//        $this->addContext( 'current_template', $model->getTemplate() );
    }

    /**
     * Renders the page after all validation, data aggregation,
     * and other logic and opinion has been applied.
     *
     *
     * @param int|string $page The id or slug of the page to render.
     *     If no page is provided, it will be extracted from the registered
     *     contextual information. If "error" is provided, the correct error
     *     page will be extracted from the contextual information.
     * @param mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     *     If not supplied, the default view will be used. If a view object is supplied,
     *     the provided view object will be initialized and used in place of the default.
     * @return void
     * @note This method will terminate execution upon completion.
     */
    protected function render( $page = null,
        \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view = null )
    {
        /**
         * Explicit calls to error pages get
         * tossed to the error controller to handle.
         */
        $errors = array(
            401,
            403,
            404,
            500 );
        if ( !is_null( $page )
            && (in_array( $page, $errors ) || in_array( intval( $page ), $errors )) )
        {
            $this->error( !is_int( $page )
                    ? intval( $page )
                    : $page  );
            exit();
        }
        if ( !is_null( $page ) )
        {
            //@todo manual page calls
            d( 'Update here to render arbitrary pages.', $page );
            exit();
        }
        $layout = $this->getLayout();
        $this->addContext( 'layout', $layout );
        $view = $this->getView( $view );
        $view->registerPathset( 'layout',
            $this::containerize( $layout['paths'] ) );
        $this::debugStash( array(
            'layout' => $layout,
            'view' => get_class( $view ),
            ), 'page_render_called' );
        $view->render();
    }

    /**
     * Override this method to provide an alternate exit handler
     * for Wordpress when `wp_die` is called.
     *
     * @param callable $exit_handler
     * @return callable
     */
    protected function exitHandler( $exit_handler )
    {
        return $exit_handler;
    }

    /**
     * Sets the HTML5 tags that we have to tell
     * the html editor to not remove for whatever reason,
     * because an html editor that arbitrarily
     * removes html totally makes sense.
     *
     * Because reasons.
     *
     * @param array $init Text editor options
     * @return array The fixed text editor options
     * @todo This needs its own model. There's a lot in here we can do to make
     *     the platform not mess up your content authoring,
     *     which is beyond the scope of one method.
     */
    protected function registerHtmlTags( $init )
    {
        $init['extended_valid_elements'] = implode( '[*],',
            $this->load( 'library', 'config\\HtmlConfig' )->get()->toArray() );
        $init['wpautop'] = false; // NO. ABSOLUTELY NO. NEVER EVER EVER.
        return $init;
    }

    /**
     * This filter sets placeholders into saved post/page content that
     * can be restored, so the content does not get utterly butchered
     * by overly opinionated filters that don't know how to retain
     * content securely. This is used to preserve backslashes,
     * custom html markup, and numerous outher things that the
     * underlying platform for whatever reason does not recognize
     * as neccessary or provide any means to retain whatsoever,
     * even when they are explicitly placed there on purpose.
     *
     * Because reasons.
     *
     * @param array $post Post content
     * @return array The fixed text editor options
     */
    protected function setContentRetentionMarkers( $post )
    {
        $post = str_replace( "\\'", "'", $post );
        $post = str_replace( '\\"', '"', $post );
        // Sets markers to retain intentionally placed backslashes.
        // Also fix excessive slash adding from the platform using
        // extremely deprecated escaping methods.
        $post = str_replace( '\\\\',
            '<br class="sanctity-backslash-placeholder">', $post );
        $post = str_replace( '\\',
            '<br class="sanctity-backslash-placeholder">', $post );
        // Clears wpautop trash.
        $post = str_replace( '<p></p>', null, $post );
        $post = str_replace( '<p>&nbsp;</p>', null, $post );
        // Clear corruption from WordPress trying to reinvent
        // the wheel as to how standard content displays.
        $post = str_replace( '&nbsp', ' ', $post );
        return $post;
    }

    /**
     * This filter removes the aforementioned placeholders, so the resulting
     * content is provided as expected after its round trip from the database.
     *
     * @param array $post Post content
     * @return array The fixed text editor options
     */
    protected function replaceContentRetentionMarkers( $post )
    {
        $post = str_replace( "\\'", "'", $post );
        $post = str_replace( "\\'", "'", $post );
        $post = str_replace( '<br class="sanctity-backslash-placeholder">',
            '\\', $post );
        return $post;
    }

    /**
     * -------------------------------------------------------
     *                Protected Getter Methods
     * -------------------------------------------------------
     */

    /**
     * Returns a fully prepared and dependency injected instance of a view,
     * based on the contextual data currently held by the Controller.
     * This should be called immediately prior to `render` in order to set up the
     * page for final display.
     *
     * @note A view does not need to be supplied to this method.
     *     It will return the default view for the current controller
     *     scope if no parameters are passed.
     *
     * @note If you want a different view, use `$this->load('view', 'SomeViewClass');`,
     *     and then pass in the result of that method.
     *
     * @param \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     * @return type
     */
    protected function getView( \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
    = null )
    {
        if ( is_null( $view ) )
        {
            $view = self::$view;
        }
        return $this->prepareView( $view );
    }

    /**
     * Returns the error controller associated with the current scope.
     * @return mopsyd\sanctity\interfaces\controllers\SanctityErrorControllerInterface
     */
    protected function getErrorController()
    {
        $ref = new \ReflectionClass( $this );
        $namespace = $ref->getNamespaceName();
        $class = $this::CONTROLLER_SCOPE . '\\' . 'ErrorController';
        $instance = $class::init();
        $instance->setSiteObject( self::$site );
        return $instance;
    }

    /**
     * Returns the Site details from the Site Model.
     *
     * This can be overridden to provide a custom site object.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     */
    protected function getSite()
    {
        $model = $this->load( 'model', 'display\\SiteModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data used to generate a search form.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\SearchModel
     */
    protected function getSearch()
    {
        $model = $this->load( 'model', 'display\\SearchModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the branding and identity scheme
     * within the current scope.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\BrandingModel
     */
    protected function getBranding()
    {
        $model = $this->load( 'model', 'display\\BrandingModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the color scheme and visual display
     * within the current scope.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\StyleModel
     */
    protected function getStyle()
    {
        $model = $this->load( 'model', 'display\\StyleModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the current theme.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\ThemeModel
     */
    protected function getTheme()
    {
        $model = $this->load( 'model', 'display\\ThemeModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns a list of the current scripts required for page load.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\ScriptsModel
     */
    protected function getScripts()
    {
        $model = $this->load( 'model', 'display\\ScriptsModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns a list of the current stylesheets required for page load.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\StylesheetModel
     */
    protected function getStyles()
    {
        $model = $this->load( 'model', 'display\\StylesheetModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the current page.
     *
     * The data returned by this model is accessible
     * completely prior to the loop firing.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\PostModel
     */
    protected function getPage()
    {
        $model = $this->load( 'model', 'display\\PageModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        $settings = $model->getOutputData();
        return $settings;
    }

    /**
     * Returns data about the site posts.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\ArchiveModel
     */
    protected function getPosts()
    {
        $model = $this->load( 'model', 'display\\PostModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the current template layout.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\LayoutModel
     */
    protected function getLayout()
    {
        $model = $this->load( 'model', 'display\\LayoutModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        $settings = $model->getSettings();
        $settings['sidebars'] = $model->renderSidebars();
        $settings['paths'] = $model->getPaths();
        return $settings;
    }

    /**
     * Returns data about the page menu.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\MenuModel
     */
    protected function getMenu()
    {
        $model = $this->load( 'model', 'display\\MenuModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns data about the current user, if any.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\UserModel
     */
    protected function getUser()
    {
        $model = $this->load( 'model', 'display\\UserModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns the current configurations about how to handle events
     * within the Wordpress loop, and whether to let them fire naturally
     * or bypass them.
     *
     * This information instructs the template whether or not to call
     * the blocks that fire these events, or skip them completely.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\display\PlatformModel
     */
    protected function getPlatform()
    {
        $model = $this->load( 'model', 'display\\PlatformModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * Returns an array of Theme Support options.
     *
     * @return array
     * @todo Move to the theme model.
     */
    protected function getThemeSupport()
    {
        $layout = $this->load( 'model', 'display\\LayoutModel' );
        $branding = $this->load( 'model', 'display\\BrandingModel' );
        $layout_support = $layout->getSettings()['theme-support'];
        $branding_support = $branding->get( 'theme-support' );
        if ( !$layout_support )
        {
            $layout_support = array();
        }
        if ( !$branding_support )
        {
            $branding_support = array();
        }
        //Branding support is the base
        //Layout settings override branding if there are conflicts
        $support = array_replace( $branding_support, $layout_support );
        //Additional custom theme override options are declared
        //in the `declareThemeSupport` method, if any child class
        //has presented customizations.
        return array_replace( $support, $this->declareThemeSupport() );
    }

    /**
     * Returns an array of theme options for the customizer.
     *
     * @return array
     * @todo Move to the theme model.
     */
    protected function getThemeOptions()
    {
        return array();
    }

    /**
     * Returns an array of theme settings for the settings page
     * that are beyond the scope of the customizer, if any.
     *
     * @return array
     * @todo Move to the theme model.
     */
    protected function getThemeSettings()
    {
        return array();
    }

    /**
     * Returns data that should be queued on the frontend securely
     * for javascript to consume.
     *
     * This process is entirely automated, and there is an api to provide this data.
     * It is then accessible on the frontend through the sanctity javascript
     * object, and is encapsulated so that only authorized scripts can view
     * or request it.
     *
     * @note The process of passing this is completed,
     *    but the frontend api for accessing it currently is not.
     *    I have to get around to putting on my frontend hat for a bit
     *    when I get around to it and develop the core object api,
     *    but it's in the works.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface
     * @see \mopsyd\sanctity\models\core\DataModel
     */
    protected function getData()
    {
        $model = $this->load( 'model', 'core\\DataModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        return $model->getSettings();
    }

    /**
     * -------------------------------------------------------
     *                Child Class Declarations
     *
     * These are used by child classes of the abstract controller
     * to automatically hook into the runtime events. These are
     * in theory the only methods you need to call from a child
     * controller to modify behavior, however you may also find
     * some of the internal getters and setters useful depending
     * on your purpose.
     *
     * -------------------------------------------------------
     */
    // -------------------------------------------------------------------------
    // Frontend Asset Declarations
    // -------------------------------------------------------------------------

    /**
     * Default Scripts
     * You may override this to provide additional scope-specific scripts.
     * @return array
     */
    protected function declareScripts( $page = null )
    {
        return array();
    }

    /**
     * Default Stylesheets
     * You may override this to provide additional scope-specific stylesheets.
     * @return array
     */
    protected function declareStyles( $page = null )
    {
        return array();
    }

    /**
     * Default CDN Assets
     * You may override this to provide additional CDN endpoints for
     * queuing scripts, stylesheets, fonts, and media. The defaults
     * distributed with this base theme will always be avaliable in
     * this list even if this method returns nothing. They may be
     * replaced as needed with alternate endpoints, but must always
     * be present.
     * @return array
     * @note This is going to move to a model prior to release.
     */
    protected function declareCdnAssets( $page = null )
    {
        return array();
    }

    // -------------------------------------------------------------------------
    // Output Declarations
    // -------------------------------------------------------------------------

    /**
     * Default Context
     * You may override this to provide additional scope-specific page context.
     * @return array
     */
    protected function declareContext( $page = null )
    {
        return array();
    }

    // -------------------------------------------------------------------------
    // Event Declarations
    // -------------------------------------------------------------------------

    /**
     * Default Actions
     * You may override this to provide additional scope-specific actions.
     * @return array
     */
    protected function declareActions()
    {
        return array();
    }

    /**
     * Default Filters
     * You may override this to provide additional scope-specific filters.
     * @return array
     */
    protected function declareFilters()
    {
        return array();
    }

    // -------------------------------------------------------------------------
    // Platform Adapter Declarations
    // -------------------------------------------------------------------------

    /**
     * Default Theme Support
     * You may override this to provide additional scope-specific context.
     * @return array
     */
    protected function declareThemeSupport()
    {
        return array();
    }

    /**
     * Default Post Types
     * You may override this to provide additional scope-specific post types.
     * @return array
     */
    protected function declarePostTypes()
    {
        return array();
    }

    /**
     * Default Taxonomies
     * You may override this to provide additional scope-specific taxonomies.
     * @return array
     */
    protected function declareTaxonomies()
    {
        return array();
    }

    // -------------------------------------------------------------------------
    // Render Adapter Declarations
    // -------------------------------------------------------------------------

    /**
     * Default Twig Filters
     * You may override this to provide additional Twig filters to load.
     * The core required Twig filters will always be persistently available.
     * @return array
     * @note This is going to move to a model prior to release.
     */
    protected function declareTwigFilters()
    {
        return array();
    }

    /**
     * Default Twig Extensions
     * You may override this to provide additional Twig extensions to load.
     * The core required Twig extensions will always be persistently available.
     * @return array
     * @note This is going to move to a model prior to release.
     */
    protected function declareTwigExtensions()
    {
        return array();
    }

    /**
     * Declares hosts and uris that should be included in the meta preload
     * set for faster script, css, and media loading clientside.
     * @return array
     */
    protected function declarePreload()
    {
        return array();
    }

    /**
     * Declares structured data to place in the document
     * head for improved search presence.
     * @return array
     */
    protected function declareStructuredData( $page = null )
    {
        return array();
    }

    // -------------------------------------------------------------------------
    // Client Side Declarations
    // -------------------------------------------------------------------------

    /**
     * Returns the data to pass to the client for javascript to consume.
     * @return void
     */
    protected function declareClientData( $page = null )
    {
        return array();
    }

    /**
     * Declares frontend javascript modules to make discoverable to the
     * frontend library for automatic scripting extension.
     * @return array
     */
    protected function declareModules( $page = null )
    {
        return array();
    }

    /**
     * Declares component data to pass to the frontend
     * for client side representation.
     * @return array
     */
    protected function declareComponents( $page = null )
    {
        return array();
    }

    /**
     * Declares the CSRF token to send to the frontend
     * for ajax cross-site request forgery protection.
     * @return boolean|string
     */
    protected function declareCsrf( $page = null )
    {
        return false;
    }

    /**
     * Declares any domains that should be allowed direct cross-origin resource
     * sharing rights, if any.
     * @return array
     */
    protected function declareCors( $page = null )
    {
        return array();
    }

    /**
     * Declares client side extensions, if any.
     * @return array
     */
    protected function declareClientExtensions( $page = null )
    {
        return array();
    }

    /**
     * Declares endpoints to pass to the frontend, if any.
     * These will be automatically accessible to frontend ajax requests.
     * @return array
     */
    protected function declareEndpoints( $page = null )
    {
        return array();
    }

    /**
     * Declares integrations with the native platform api
     * that the frontend should be made aware of.
     * @return array
     */
    protected function declareIntegrations( $page = null )
    {
        return array();
    }

    /**
     * -------------------------------------------------------
     *           Initialization Methods - Actions
     *
     * These methods fire during the wordpress event sequence.
     * This class provides a queueing system that calls them
     * dynamically, and insures the same method is not registered
     * twice within the same priority level for the same action,
     * so duplicate actions do not occur. These may all be
     * bound internally using the `bindAction` method of this class,
     * which is the standardized way that it cleanly loops through
     * action bindings. A static log of all actions taken in their
     * sequence is also kept for debugging if you use this approach,
     * which makes it much easier to figure out what you did where.
     * -------------------------------------------------------
     */

    /**
     * This method fires the initialization hooks on any extensions that have
     * been registered with the theme. This occurs at priority 0 on the `init`
     * hook, so they need to be registered prior to this point. The registration
     * method for extensions can be called statically at any time, provided the
     * theme functions.php file has loaded, and they will be queued for
     * initialization at this point. Extensions registered late will
     * throw an exception.
     *
     * @return void
     */
    protected function initializeExtensions()
    {
        $model = $this->load( 'model', 'integration\\ExtensionModel' );
        $model->closeRegistration();
        $model->initializeExtensions();
    }

    /**
     * This method fetches any options stored from in the Wordpress database,
     * and prepares them for use through the settings model(s).
     * This method fires during the "init" hook.
     * as an actionable set.
     * @return void
     */
    protected function initializeOptions()
    {
        //no-op for now.
    }

    /**
     * This method initializes and registers any theme settings
     * that need to be registered in the current context.
     * This method fires during the "init" hook.
     * @return void
     */
    protected function initializeSettings()
    {
        $models = array(
            'branding' => 'settings\\BrandingModel',
            'dashboard' => 'settings\\DashboardModel',
            'debug' => 'settings\\DebugModel',
            'integration' => 'settings\\IntegrationModel',
            'layout' => 'settings\\LayoutModel',
            'settings' => 'settings\\SettingsModel',
            'style' => 'settings\\StyleModel',
        );
        foreach ( $models as
            $key =>
            $class )
        {
            $model = $this->load( 'model', $class );
            $model->setScope( $this::CONTROLLER_SCOPE );
            $model->register();
        }
    }

    /**
     * This method is registered into the work queue to
     * provide post types. This method fires during the "init" hook.
     * To provide additional post types, override `declarePostTypes`.
     * @return void
     * @see mopsyd\sanctity\controllers\AbstractController::declarePostTypes
     */
    protected function initializePostTypes()
    {
        //no-op for now.
    }

    /**
     * This method is registered into the work queue to
     * provide taxonomies. This method fires during the "init" hook. To provide
     * additional taxonomies, override `declareTaxonomies`.
     * @return void
     * @see mopsyd\sanctity\controllers\AbstractController::declareTaxonomies
     */
    protected function initializeTaxonomies()
    {
        //no-op for now.
    }

    /**
     * This method is called to register theme mods.
     * It fires during the "init" hook.
     * @return void
     */
    protected function initializeThemeMods()
    {
        $theme = self::$site;
        $prefix = $theme::THEME_SLUG;
//        d( $prefix, $this->getThemeSupport() );
//        exit();
    }

    /**
     * This method is called to register theme mods.
     * It fires during the "after_setup_theme" hook.
     * @return void
     */
    protected function initializeThemeSupport()
    {
        $support_items = $this->getThemeSupport();
        $layout_model = $this->load( 'model', 'display\\LayoutModel' );
        $custom_post_types = $layout_model->getSettings()['integration']['post-types'];
        $post_types = get_post_types();
        foreach ( $support_items as
            $item =>
            $enabled )
        {
            if ( !$enabled )
            {
                continue;
            }
            if ( is_string( $item ) && !is_array( $enabled ) )
            {
                add_theme_support( $item );
            } else
            {
                add_theme_support( $item, $enabled );
            }
            foreach ( $post_types as
                $post_type )
            {
                if ( post_type_supports( $post_type, $item ) )
                {
//                    d( sprintf('adding post support for [%1$s] to post type [%2$s]', $item, $post_type) );
                    add_post_type_support( $post_type, $item );
                }
            }
            foreach ( $custom_post_types as
                $post_type )
            {
                add_post_type_support( $post_type, $item );
            }
        }
//        d('debug break', $post_types, $custom_post_types, $support_items ); exit;
    }

    /**
     * This method completes the page initialization once Wordpress has run its
     * internals correctly, insuring that all relevant data is present and the
     * models do not choke.
     * @return void
     */
    protected function initializePage()
    {
        //no-op for now.
    }

    /**
     * This method builds the context array for the template.
     * This fires pretty late, to insure that all of the
     * required data is present.
     * @return void
     */
    protected function initializeContext()
    {
        $this->setContextData( 'site', $this->getSite() );
        $this->setContextData( 'search', $this->getSearch() );
        $page = $this->getPage();
        if ( in_array( $page->get( 'type' ),
                array(
                401,
                403,
                404,
                500 ) ) )
        {
            $this->setContextData( 'error', $page );
//            d($page); exit;
        } else
        {

        }
        $this->setContextData( 'page', $page );
        $this->setContextData( 'menu', $this->getMenu() );
        $this->setContextData( 'posts', $this->getPosts() );
        $this->setContextData( 'theme', $this->getTheme() );
        $this->setContextData( 'branding', $this->getBranding() );
        $this->setContextData( 'layout', $this->getLayout() );
        $this->setContextData( 'style', $this->getStyle() );
        $this->setContextData( 'stylesheets', $this->getStyles() );
        $this->setContextData( 'scripts', $this->getScripts() );
        $this->setContextData( 'user', $this->getUser() );
        $this->setContextData( 'platform', $this->getPlatform() );
        $this->setContextData( 'data', $this->getData() );
    }

    /**
     * This method checks the layout setup and builds the correct layout
     * information for the current page based on the site administrators
     * settings policy for the current page load.
     * @return void
     */
    protected function initializeLayout()
    {
        //no-op for now.
    }

    /**
     * This method initializes the branding and identity information,
     * color scheme, and other related information required for the
     * current page load to render cleanly.
     * @return void
     */
    protected function initializeBranding()
    {
        //no-op for now.
    }

    /**
     * This method initializes the javascript that needs to be enqueued.
     *
     * This fires prior to the enqueue step, so that all of the resources
     * required are already present when the hook fires.
     *
     * The abstract method enqueues the metadata for the frontend script libraries,
     * and closes registration thereafter for further addition of metadata
     * to the frontend script dataset (it can't be added once it's been queued to the page).
     *
     * @note This method must be called via `parent::initializeScripts();`
     *     if this method is overridden. If you fail to do this,
     *     you will break a TON of things on the frontend.
     *
     * @return void
     */
    protected function initializeScripts( $page = null )
    {
        // Package the final frontend dataset to push to the client.
        $this->_packageClientData( $page );
        // Register the REST client.
        $this::getAdapter()->register( 'rest', null );
        $this->load( 'library', 'media\\Javascript', null,
                array(
                'handle' => 'clientside-metadata',
                'thirdparty' => false,
                'inline' => true,
                'details' => array(
                    'footer' => false,
                    'source' => '/* <![CDATA[ */' . PHP_EOL . 'window.sanctity_page_data = JSON.parse(\''
                    // Backslashes need to be double escaped so the javascript parser doesn't choke.
                    . str_replace( '\\', '\\\\',
                        json_encode( $this->load( 'library',
                                'config\\ClientDataConfig' )->get() ) )
                    . '\');' . PHP_EOL . '/* ]]> */',
                    'dependencies' => array(
                        'placeholder-header',
                    ),
                    'version' => $this::PACKAGE_VERSION,
                ),
            ) )
            ->load( 'clientside-metadata' )
            ->compile()
            ->register();
        $this->closeDataRegistration();
    }

    /**
     * This method initializes the stylesheets that needs to be enqueued.
     *
     * This fires prior to the enqueue step, so that all of the resources
     * required are already present when the hook fires.
     *
     * This method will grab all stylesheets that have been dynamically
     * created and insure that they are also registered in addition to the
     * static ones that ship with Sanctity.
     *
     * @return void
     */
    protected function initializeCss()
    {
        $sass = $this->load( 'library', 'sass\\Sass' );
        $stylesheet = $this->load( 'library', 'media\\Stylesheet' );
        foreach ( $sass->getCompiledIndex() as
            $handle =>
            $details )
        {
            $stylesheet->add( $handle, $details );
        }
    }

    /**
     * This method initializes the menus required for proper render of the layout.
     *
     * This fires during the "register_sidebar" step, so that all of the resources
     * required are already present when the hook fires.
     *
     * @return void
     */
    protected function initializeMenus()
    {
        $model = $this->load( 'model', 'display\\MenuModel' );
        $model->setScope( $this::CONTROLLER_SCOPE );
        $model->register();
    }

    /**
     * This method initializes the customizer options.
     *
     * This fires during the "customize_register" step.
     *
     * @return void
     */
    protected function initializeCustomizer()
    {
        //no-op for now.
        $customizer = func_get_args()[0][0];
        $model = $this->load( 'model', 'integration\\CustomizerModel',
            $customizer, array(
            'foo' => 'bar' ) );
        $model->setScope( 'frontend' );
        $model->setCustomizer( $customizer );
//        d( $model, $customizer );
//        exit();
    }

    /**
     * This method makes custom ajax endpoints recognizable by the
     * platform as valid endpoints,so we don't have to fight with the
     * platform to have them recognized as a valid endpoint.
     */
    protected function initializeAjax()
    {
        // @todo move this to the platform adapter
        // when the ajax loopback is reasonably complete.
        register_post_type( 'sanctity-ajax',
            // CPT Options
            array(
            'labels' => array(
                'name' => __( 'Sanctity Ajax Endpoints' ),
                'singular_name' => __( 'Sanctity Ajax Endpoint' )
            ),
            'description' => 'Sanctity custom ajax endpoints accessible from the frontend.',
            'hierarchical' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => false,
            'show_in_nav_menus' => false,
            'show_in_admin_bar' => false,
            'show_in_rest' => false,
            'rewrite' => array( 'slug' => 'sanctity-ajax', 'with_front' => false ),
            'query_var' => false,
            'public' => false,
            'has_archive' => false
            )
        );
        register_post_type( 'sanctity-secure-ajax',
            array(
            'labels' => array(
                'name' => __( 'Sanctity Secure Ajax Endpoints' ),
                'singular_name' => __( 'Sanctity Secured Ajax Endpoint' )
            ),
            'description' => 'Sanctity custom ajax endpoints accessible only to authenticated and logged in users.',
            'hierarchical' => false,
            'exclude_from_search' => true,
            'publicly_queryable' => false,
            'show_ui' => false,
            'show_in_nav_menus' => false,
            'show_in_admin_bar' => false,
            'show_in_rest' => false,
            'rewrite' => array( 'slug' => 'sanctity-secure-ajax', 'with_front' => false ),
            'query_var' => false,
            'public' => false,
            'has_archive' => false
            )
        );
    }

    /**
     * This method initializes the sidebars required
     * for proper render of the layout.
     *
     * This fires during the "widgets_init" step, so that all of the resources
     * required are already present when the hook fires.
     *
     * @return void
     */
    protected function initializeSidebars()
    {
        $layout = $this->load( 'model', 'display\\LayoutModel' );
        $this->load( 'model', 'integration\\WidgetModel' )->registerWidgets();
        $layout->setScope( $this::CONTROLLER_SCOPE );
        $layout->registerSidebars();
    }

    /**
     * -------------------------------------------------------
     *                  Filter Methods
     *
     * These methods fire in sequence with filters. This class
     * provides a sequencer that queues filters and executes
     * them appropriately internally against its own queue,
     * so only one registration for each filter name and
     * priority pair is actually registered, and they always
     * point back to the same method.
     * -------------------------------------------------------
     */
    /**
     * -------------------------------------------------------
     *                  Utility Methods
     *
     * These methods perform decision making logic within
     * the controller, but may need to be overridden in
     * child classes, so they have a visibility of protected
     * instead of private.
     * -------------------------------------------------------
     */

    /**
     * Performs the dependency injection operation when a View is fetched via `getView`.
     *
     * @param \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     * @return \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    protected function prepareView( \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view )
    {
        ;
        $view->setSite( self::$site );
        return $view;
    }

    /**
     * -------------------------------------------------------
     *                      Internals
     *
     * These methods are the private internals of this class,
     * and should not be modified. Everything below this line
     * has no relation to child class extensions whatsoever.
     * -------------------------------------------------------
     */

    /**
     * Verifies that a callback is a method that exists
     * in the current class scope.
     *
     * The provided parameter
     * must be a string method name of a method that exists
     * within the current class AND is currently callable
     * in the current scope.
     *
     * @param type $callback
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the supplied callback is not a method name of a method callable
     *     in this class instance. This method will reject callbacks that are
     *     not methods from this class or one of its children.
     */
    private function _verifyCallback( $callback )
    {
        if ( !is_string( $callback ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'The supplied method is not a string in [%1$s]',
                get_class( $this ) )
            );
        }
        if ( !method_exists( $this, $callback ) && is_callable( array(
                $this,
                $callback ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Method [%1$s] is not a callable method that exists in the scope of [%2$s]',
                $callback, get_class( $this ) )
            );
        }
    }

    /**
     * Binds the actions defined within the extension class.
     * @return void
     * @internal
     */
    private function _bindExtensionActions()
    {
        foreach ( $this->declareActions() as
            $action =>
            $callback )
        {
            $this->bindAction( $action, $callback );
        }
    }

    /**
     * Adds the filters declared in the override class.
     * @return void
     * @internal
     */
    private function _bindExtensionFilters()
    {
        foreach ( $this->declareFilters() as
            $key =>
            $filter )
        {
            add_filter( $key, $filter );
        }
    }

    /**
     * Initializes the scope expected to be adhered to
     * by models, views, and adapters.
     *
     * This insures that all controllers have access to a single
     * consistent presentation layer.
     *
     * @return void
     * @internal
     */
    private function _initializeScopes()
    {
        \mopsyd\sanctity\models\AbstractModel::setScope( $this::CONTROLLER_SCOPE );
        \mopsyd\sanctity\adapters\AbstractAdapter::setScope( $this::CONTROLLER_SCOPE );
    }

    /**
     * Initializes the context used to package data into the templates.
     *
     * This insures that all controllers have access to a single
     * consistent area to add output data.
     *
     * @return void
     * @internal
     */
    private function _initializeContext()
    {

        \mopsyd\sanctity\models\AbstractModel::setScope( $this::CONTROLLER_SCOPE );
        \mopsyd\sanctity\adapters\AbstractAdapter::setScope( $this::CONTROLLER_SCOPE );
    }

    /**
     * Initializes the view if it has not already been initialized.
     *
     * This insures that all controllers have access to a single
     * consistent presentation layer.
     *
     * @return void
     * @internal
     */
    private function _initializeView()
    {
        if ( is_null( self::$view ) )
        {
            $view = $this::CONTROLLER_SCOPE . '\\' . ucfirst( $this::CONTROLLER_SCOPE ) . 'View';
            $view = $this->load( 'view', $view );
            self::$view = $view;
        }
    }

    /**
     * Adds the default taxonomies that should always be present,
     * even if an idiot overrides the above function and forgets to include
     * parent::declareTaxonomies
     *
     * This will prevent nested child themes from disrupting the underlying
     * logic if sloppy programming practices are used.
     * @return void
     * @internal
     */
    private function _bindCoreTaxonomies()
    {
        if ( !self::$core_taxonomies_bound )
        {
            //no-op
            self::$core_taxonomies_bound = true;
        }
    }

    /**
     * Adds the default post types that should always be present,
     * even if an idiot overrides the above function and forgets to include
     * parent::declarePostTypes
     *
     * This will prevent nested child themes from disrupting the underlying
     * logic if sloppy programming practices are used.
     * @return void
     * @internal
     */
    private function _bindCorePostTypes()
    {
        if ( !self::$core_post_types_bound )
        {
            //no-op
            self::$core_post_types_bound = true;
        }
    }

    /**
     * This method is for the underlying system to queue any default mandatory
     * context data that is present on every page load, if the need arises.
     *
     * Currently there is no need to do this, but the need may realistically arise.
     *
     * This fires before the method that child classes can override.
     *
     * @return void
     * @internal
     */
    private function _bindCoreContext()
    {
        if ( !self::$core_context_bound )
        {

            // This is a placeholder for internal context that always
            // have to be queued, if any ever arises. Futureproofing ftw.
            self::$core_context_bound = true;
        }
    }

    /**
     * Binds the core required Twig extensions.
     * @param \Twig_Environment $twig
     * @return \Twig_Environment
     * @internal
     * @note This is going to get factored out into a
     *     more generalized "render layer" approach,
     *     that does not make the controller aware
     *     of the render driver, but just uses the
     *     render adapter for validation.
     *     This will facilitate integration of
     *     Blade/Smarty/etc as alternate render layers.
     */
    private function _loadCoreTwigExtensions( $twig )
    {
        if ( !self::$core_twig_extensions_bound )
        {
            //Bind the baseline required twig extensions.
            foreach ( self::$core_twig_extensions as
                $extension )
            {
                $this->_checkTwigExtensionClass( $extension );
                $twig->addExtension( new $extension() );
            }
            $twig = $this->_loadCoreTwigFilters( $twig );
            self::$core_twig_extensions_bound = true;
        }
        return $twig;
    }

    /**
     * Binds the core required Twig filters.
     * This operation occurs only once, at the same time
     * that the core extensions are bound.
     * @param \Twig_Environment $twig
     * @return \Twig_Environment
     * @internal
     * @note This is going to get factored out into a
     *     more generalized "render layer" approach,
     *     that does not make the controller aware
     *     of the render driver, but just uses the
     *     render adapter for validation.
     *     This will facilitate integration of
     *     Blade/Smarty/etc as alternate render layers.
     */
    private function _loadCoreTwigFilters( $twig )
    {
//        echo '<pre>';
//        $filter = new \Twig_SimpleFilter( 'page_data',
//            array(
//            $this,
//            'getFrontendData' ) );
//        $twig->addFilter( $filter );
        return $twig;
    }

    /**
     * Loads any declared Twig filters into Twig.
     * @param \Twig_Environment $twig
     * @return \Twig_Environment
     * @internal
     * @note This is going to get factored out into a
     *     more generalized "render layer" approach,
     *     that does not make the controller aware
     *     of the render driver, but just uses the
     *     render adapter for validation.
     *     This will facilitate integration of
     *     Blade/Smarty/etc as alternate render layers.
     */
    private function _loadDeclaredTwigFilters( $twig )
    {
        foreach ( $this->declareTwigFilters() as
            $key =>
            $options )
        {
            //@todo actually load the filters
        }
        return $twig;
    }

    /**
     * Binds the Twig extensions declared by override logic.
     * @param \Twig_Environment $twig
     * @return \Twig_Environment
     * @internal
     * @note This is going to get factored out into a
     *     more generalized "render layer" approach,
     *     that does not make the controller aware
     *     of the render driver, but just uses the
     *     render adapter for validation.
     *     This will facilitate integration of
     *     Blade/Smarty/etc as alternate render layers.
     */
    private function _loadDeclaredTwigExtensions( $twig )
    {
        $existing = array();
        foreach ( $twig->getExtensions() as
            $extension )
        {
            $existing[] = get_class( $extension );
        }
        foreach ( $this->declareTwigExtensions() as
            $extension )
        {
            if ( in_array( ltrim( $extension, '\\' ), $existing ) )
            {
                continue;
            }
            $this->_checkTwigExtensionClass( $extension );
            //If the extension itself is broken, that is out of scope
            //for this library, and any errors will be allowed to bubble up.
            if ( is_string( $extension ) )
            {
                //Instantiates the extension if it is not already
                $extension = new $extension();
            }
            $twig->addExtension( $extension );
        }
        if ( !self::$core_twig_extensions_bound )
        {
            //Bind the baseline required twig extensions.
            foreach ( self::$core_twig_extensions as
                $extension )
            {
                $this->_checkTwigExtensionClass( $extension );
                $twig->addExtension( new $extension() );
            }
            self::$core_twig_extensions_bound = true;
        }
        return $twig;
    }

    /**
     * Checks declared extensions for Twig to insure they are
     * not going to raise an error.
     * @param string|object $class An instance of \Twig_ExtensionInterface
     *     or a string representing one.
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     This exception is thrown if the declared extension is not
     *     a valid Twig extension
     * @note This is going to get factored out into a
     *     more generalized "render layer" approach,
     *     that does not make the controller aware
     *     of the render driver, but just uses the
     *     render adapter for validation.
     *     This will facilitate integration of
     *     Blade/Smarty/etc as alternate render layers.
     */
    private function _checkTwigExtensionClass( $class )
    {
        $expected = 'Twig_ExtensionInterface';
        if ( is_object( $class ) )
        {
            $class = get_class( $class );
        }
        if ( !class_exists( $class ) && in_array( $expected,
                class_implements( $class ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Instance of [%1$s] is misconfigured. Declared Twig '
                . 'extension [%2$s] is not valid. It must be a class '
                . 'that implements [%3$s]. Please report this issue '
                . 'to the developer of [%1$s] so it can be corrected.',
                get_class( $this ), $class, $expected )
            );
        }
    }

    /**
     * Registers the baseline action and filter bindings
     * that should always be present.
     * @return void
     * @internal
     */
    private function _initializeCoreBindings()
    {
        $this->_bindCoreTaxonomies();
        $this->_bindCorePostTypes();
        $this->_initializeCoreActions();
        $this->_initializeCoreFilters();
        $this::debugStash( __FUNCTION__, 'initialization_steps_completed' );
    }

    /**
     * Sets the baseline action bindings to fire in the queue when
     * they come up in the normal Wordpress action event sequence.
     *
     * This is the basic execution plan for a generic Sanctity page load.
     *
     * @return void
     * @internal
     */
    private function _initializeCoreActions()
    {
        //Baseline level bindings. Complete initialization
        //when Wordpress is fully instantiated.
        $this->bindAction( 'after_setup_theme', 'initializeExtensions',
            PHP_INT_MAX );   //Theme extension classes must have been added by this point.
        $this->bindAction( 'init', 'initializeThemeSupport' );
        $this->bindAction( 'init', 'initializeLayout' );
        $this->bindAction( 'init', 'initializeBranding' );
        $this->bindAction( 'init', 'initializeOptions' );
        $this->bindAction( 'init', 'initializeMenus' );
        $this->bindAction( 'init', 'initializePostTypes' );
        $this->bindAction( 'init', 'initializeTaxonomies' );
        $this->bindAction( 'init', 'initializeThemeMods' );
        $this->bindAction( 'init', 'initializeSettings' );
        $this->bindAction( 'init', 'initializeAjax' );
        $this->bindAction( 'widgets_init', 'initializeSidebars' );
//        $this->bindAction( 'register_sidebar', 'initializeMenus' );
        $this->bindAction( 'wp_loaded', 'initializePage' );
        $this->bindAction( 'template_redirect', 'initializeContext' );
        $this->bindAction( 'template_redirect', 'initializePage' );
        $this->bindAction( 'customize_register', 'initializeCustomizer' );
        $this->bindAction( 'wp_enqueue_scripts', 'initializeScripts',
            PHP_INT_MIN ); // All queued clientside data must have been queued by this point
        $this->bindAction( 'wp_enqueue_scripts', 'initializeCss', PHP_INT_MIN );
        $this->_bindExtensionActions();
        self::$core_actions_bound = true;
    }

    /**
     * Sets the baseline filter bindings to fire in the queue when
     * they come up in the normal Wordpress filter event sequence.
     * @return void
     * @internal
     */
    private function _initializeCoreFilters()
    {
        if ( !self::$core_filters_bound )
        {
            $this->bindFilter( 'get_twig', 'filterTwig' );
            $this->bindFilter( 'wp_die_handler', 'exitHandler' );
            $this->bindFilter( 'tiny_mce_before_init', 'registerHtmlTags' );
            $this->bindFilter( 'content_save_pre', 'setContentRetentionMarkers',
                PHP_INT_MIN );
            $this->bindFilter( 'the_content', 'replaceContentRetentionMarkers',
                PHP_INT_MIN );
            $this->bindFilter( 'content_edit_pre',
                'replaceContentRetentionMarkers', PHP_INT_MIN );
            $this->_bindExtensionFilters();
            self::$core_filters_bound = true;
        }
    }

    /**
     * Handles an arbitrary page request.
     *
     * @param string $page The type of page to render.
     *     Should correspond to a Sanctity type or a
     *     registered Wordpress post type.
     *
     * @param mixed $data (optional) Any data that got passed
     *     in with the page request.
     * @return void
     * @internal
     */
    private function _parseCustomPageRequest( $page, $data = null )
    {
        d( $page, $data,
            $this->load( 'model', 'display\\PageModel' )->getSettings() );
        exit;
    }

    /**
     * Obtains and packages the clientside dataset to pass to frontend scripts.
     *
     * Individual controllers can provide frontend data by overriding the
     * `declareClientData` method. Any values provided in this set will
     * automatically be made available to frontend scripts. The abstract
     * controller will also package some general information that is always
     * made available for the core Sanctity.js internals to parse.
     * @return void
     * @internal
     */
    private function _packageClientData( $page = null )
    {
        $core_data = $this->_fetchCoreClientData( $page );
        $data = $this->declareClientData( $page );
        // The core data has a higher priority than declared data,
        // which prevents expected data from being overridden unexpectedly.
        $client_data = array_replace_recursive( $data, $core_data );
        $config = $this->load( 'library', 'config\\ClientDataConfig' );
        foreach ( $client_data as
            $key =>
            $value )
        {
            $config->set( $key, $value );
        }
//        d( $this, $config, $core_data, $data );
//        exit;
    }

    /**
     * Fetches the core internal data that is always provided to the frontend.
     */
    private function _fetchCoreClientData( $pagename = null )
    {
        $page = $this->load( 'model', 'display\\PageModel' )->getSettings();
        $user = $this->load( 'model', 'display\\UserModel' )->getSettings();
        $role = $user->get( 'role' );
        if ( !$role )
        {
            $user_role = array(
                'slug' => 'visitor',
                'display' => 'Site Visitor'
            );
        } else
        {
            $role = $role->toArray();
            $role = array_shift( $role );
            $user_role = array(
                'slug' => $role['name'],
                'display' => $role['display']
            );
        }
        $user_role = array_shift( $user_role );
        $result = array();
        $csrf = $this->declareCsrf( $pagename );
        if ( $csrf )
        {
            $result['csrf-token'] = $csrf;
        }
        $cors = $this->declareCors( $pagename );
        if ( !empty( $cors ) )
        {
            $result['cors'] = $cors;
        }
        $modules = $this->declareModules( $pagename );
        if ( !empty( $modules ) )
        {
            $result['modules'] = $modules;
        }
        $components = $this->declareComponents( $pagename );
        if ( !empty( $components ) )
        {
            $result['components'] = $components;
        }
        $extensions = $this->declareClientExtensions( $pagename );
        if ( !empty( $extensions ) )
        {
            $result['extensions'] = $extensions;
        }
        $endpoints = $this->declareEndpoints( $pagename );
        if ( !empty( $endpoints ) )
        {
            $result['endpoints'] = $endpoints;
        }
        $integrations = $this->declareIntegrations( $pagename );
        if ( !empty( $integrations ) )
        {
            $result['integrations'] = $integrations;
        }
        $result['debug'] = $this->checkDebug();
        $result['fingerprint'] = $this::RUNTIME_FINGERPRINT;
        $result['host'] = $this::HTTP_HOST;
        $result['scope'] = $this::CONTROLLER_SCOPE;
        $result['page'] = array(
            'id' => $page->get( 'id' ),
            'uri' => $page->get( 'uri' ),
            'slug' => $page->get( 'slug' ),
            'type' => $page->get( 'type' ),
        );
        $result['user'] = array(
            'id' => $user->get( 'id' ),
            'first-name' => $user->get( 'first_name' ),
            'last-name' => $user->get( 'last_name' ),
            'email' => $user->get( 'email' ),
            'username' => $user->get( 'username' ),
            'avatar' => $user->get( 'avatar' ),
            'role' => $user_role,
        );
        return $result;
    }

}
