<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers\admin;

/**
 * Abstract Admin Controller
 *
 * Controls default functionality of Sanctity for the Admin Dashboard.
 * This class can be extended to provide additional admin area functionality.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractAdminController
    extends \mopsyd\sanctity\controllers\AbstractController
    implements \mopsyd\sanctity\interfaces\controllers\admin\SanctityAdminControllerInterface
{

    const CONTROLLER_SCOPE = 'admin';
    const SETTINGS_INDEX_KEY = 'dashboard';

    /**
     * Represents whether the core model classes have been loaded,
     * which is required to occur prior to the 'admin_init' hook,
     * so that their settings can be registered correctly without
     * missing the mark, because this is the first hook that fires
     * on any admin page load.
     *
     * @var bool
     */
    private static $core_models_loaded = false;

    /**
     * These are the default settings page models
     * loaded into the core models subset.
     *
     * These are activated immediately upon instantiation
     * if they do not already exist, and are used for routing settings pages
     * if the request coincides with one of them.
     *
     * @var array
     */
    private static $settings_models = array(
        'settings\\DashboardModel',
        'settings\\SettingsModel',
        'settings\\LayoutModel',
        'settings\\BrandingModel',
        'settings\\StyleModel',
        'settings\\IntegrationModel',
        'settings\\EditorModel',
        'settings\\DebugModel',
    );

    /**
     * Represents the models that determine settings for the theme.
     *
     * These are loaded one time and retained statically.
     *
     * Extensions may provide additional settings models
     * if they register early enough.
     *
     * @var array
     */
    private static $core_models = array();

    /**
     * Represents any admin messages that should be rendered, sorted by level.
     * @var array
     */
    private static $admin_messages = array();

    /**
     * Admin Controller Constructor
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
        $this->_enqueueMandatoryMethods();
        $this->_initializeAdminMessages();
        $this->_loadCoreModels();
    }

    /**
     * Routes page requests dynamically, so the controller does not need
     * a dedicated method for each individual page request.
     *
     * This allows a single method to defer details to a model,
     * and make a determination based on what the model reports back
     * as being appropriate for the page load.
     *
     * If there is no model registered by the designated page handle,
     * then the page 404's normally, using whatever configuration the
     * end user has set for handling admin page 404's (generally the
     * native Wordpress handler, unless a plugin has extended the theme
     * with additional functionality).
     *
     * @param type $name The page handle.
     * @param type $arguments Any arguments passed with the page call.
     */
    public function __call( $name, $arguments )
    {
        try
        {
            if ( $name === 'index' )
            {
                //The page index should go to whatever key the
                //current controller designated as its index handler.
                $name = $this::SETTINGS_INDEX_KEY;
            }
            $this->render( $name );
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            if ( defined( 'WP_DEBUG' ) && WP_DEBUG )
            {
                d( 'Method not found.', $name, $arguments, $e->getMessage(), $e,
                    array_key_exists( $name, self::$core_models ) );
            }
            $this->error( 404 );
        }
    }

    /**
     * Performs any operations that must occur after Sanctity, Twig,
     * and any other underlying dependencies have properly instantiated.
     * @return void
     */
    public function initialize()
    {
        try
        {
            //Initialize the admin dashboard.
            $this->_initializeAdmin();
        } catch ( \Exception $e )
        {
            //If anyone sets up any broken overrides, this should catch it.
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException( sprintf(
                'Controller initialization of [%1$s] could not resolve,'
                . ' due to an exception of type [%2$s] encountered during '
                . 'initialization, with message [%3$s]. Please contact the '
                . 'developer of [%1$s] and request assistance.',
                get_class( $this ), get_class( $e ), $e->getMessage()
            ), $e->getCode(), $e );
        }
        parent::initialize();
    }

    /**
     * Calls the admin error page template, if backend themes are enabled.
     *
     * This method cannot be overridden.
     *
     * This method fires absolutely last in the order of operations prior to
     * Wordpress calling wp_die(), and presents an opportunity to display a
     * different error template for the backend. Overriding this method is
     * prohibited to prevent the likelihood of accidentally introducing a
     * security risk.
     *
     * @return void
     * @final
     */
    final public function renderAdminErrorPage()
    {
        //no-op
    }

    /**
     * Displays the admin notification queue.
     *
     * This method cannot be overridden.
     *
     * @return void
     * @final
     */
    final public function renderAdminMessage()
    {
        $template = $this->getConfig( 'admin-message-template' );
        $levels = $this->getConfig( 'admin-message-levels' );
        $markup = $template['standard']['template'];
        $session = $this->load( 'library', 'session\\Session' );
        $msg = $session->get( 'admin-messages' );
        if ( $msg )
        {
            foreach ( $session->get( 'admin-messages' ) as
                $level =>
                $messages )
            {
                foreach ( $messages as
                    $message )
                {
                    $class = $levels[$level];
                    $print = sprintf( $markup, $class, $message );
                    echo $print;
                }
            }
        }
    }

    /**
     * Enqueues the css and stylesheets for the admin page.
     *
     * This method cannot be overridden.
     *
     * @return void
     * @final
     */
    final public function adminScripts( $page )
    {
        if ( is_array( $page ) )
        {
            $page = array_shift( $page );
        }
        $page = $this->_getSettingsPageKey( $page );
        if ( array_key_exists( $page, self::$core_models ) )
        {
            //This is one of our settings pages.
            //Queue the scripts for it.
            $this->_enqueueSettingsPanelScripts( $page );
        }
        //Loads the scripts that load across the entire admin area,
        //if any are enabled (by default this should do nothing,
        //but the extensibility is in place for child theming).
        $this->_enqueueDashboardScripts( $page );
    }

    /**
     * This method fires when a logged in user accesses a nonexistent admin page.
     */
    public function adminError()
    {
        //no-op, use default wordpress handling.
        //Override in child themes to alter this.
    }

    /**
     * Registers the menu options for the admin dashboard.
     *
     * To add additional menu sections, register another settings model.
     *
     * @return void
     * @final
     */
    final public function menu()
    {
        $toplevel = self::$core_models[$this::SETTINGS_INDEX_KEY];
        $toplevel_details = $toplevel->getMenuDetails();
        add_menu_page(
            esc_html__( $toplevel_details['title'], $toplevel_details['slug'] ),
            esc_html__( $toplevel_details['menu_title'],
                $toplevel_details['slug'] ), $toplevel_details['permission'],
            sanitize_key( $toplevel_details['slug'] ),
            array(
            $this,
            $this->getThemeSettingsPageMethod() ),
            'data:image/svg+xml;base64,' . file_get_contents( SANCTITY_BASEDIR . 'config/sanctity_icon_grayscale_encoded' ),
            $this->getThemeSettingsPageMenuOrder()
        );
        $pages = array();
        foreach ( self::$core_models as
            $type =>
            $model )
        {
            if ( $model === $toplevel )
            {
                continue;
            }
            $details = $model->getMenuDetails();
            //Prepare and sanitize model keys for menu addition.
            $opt = array(
                'menu_section' => sanitize_key( $details['menu_section'] ),
                'title' => esc_html__( $details['title'] ),
                'menu_title' => esc_html__( $details['menu_title'] ),
                'permission' => esc_attr( $details['permission'] ),
                'slug' => sanitize_key( $details['slug'] ),
                'callback' => array(
                    $this,
                    $model::MODEL_SETTINGS_KEY ),
            );
            $pages[$model::MODEL_SETTINGS_KEY] = array(
                'raw' => $details,
                'registered' => $opt
            );
            add_submenu_page(
                $opt['menu_section'], $opt['title'], $opt['menu_title'],
                $opt['permission'], $opt['slug'], $opt['callback']
            );
        }
        $this->debugStash( $pages, 'submenus_added' );
    }

    /**
     * Registers the theme customization options for Sanctity
     */
    public function customize()
    {
        //todo
    }

    /**
     * Returns the name of the function that handles
     * the settings page index for the theme.
     * @return string
     */
    protected function getThemeSettingsPageMethod()
    {
        return 'index';
    }

    /**
     * Returns the name of the function that handles
     * the settings page index for the theme.
     * @return string
     */
    protected function getThemeSettingsPageMenuOrder()
    {
        return 60;
    }

    /**
     * Returns the authorization permission for
     * accessing the settings page and seeing
     * the top level menu item.
     * @return string
     */
    protected function getThemeSettingsPageAuthLevel()
    {
        return 'manage_options';
    }

    protected function loadAdminTemplate( $template, $context )
    {
        $tpl_path = $template . '.twig';
        if ( $this->checkDebug() )
        {
            $debug = array(
                'view_class' => false,
                'view_scope' => false,
                'page_type' => 'admin_settings',
                'root_template' => $template,
                'render_context' => $context,
                'view_data' => false,
                'controller_data' => $this::debugGet(),
                'stacktrace' => debug_backtrace( 2 ),
                'origin' => __METHOD__
            );
            if ( !array_key_exists( 'debug', $context ) )
            {
                $context['debug'] = $this::containerizeInto( '\\mopsyd\\sanctity\\libs\\container\\ContextContainer',
                        $debug );
            }
        }
        $this->getView()->renderPart( $tpl_path, $context->toArray() );
    }

    /**
     * Prints an admin notification for the site administrator.
     *
     * @param string $level [error, warning, success, info]
     * @param string $message
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If $level is not a valid admin message level [error, warning, success, info],
     *     or $message is not a string.
     */
    protected function adminMessage( $level, $message )
    {
        if ( !in_array( $level, array_keys( self::$admin_messages ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured. Requested an invalid admin message '
                . 'level at [%2$s]. Valid levels are [%3$s].',
                get_class( $this ), __METHOD__, implode( ', ', $levels ) )
            );
        }
        if ( !is_string( $message ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured. Provided [$message] must be of type '
                . '[string], but received [%2$s] '
                . 'at [%3$s].', get_class( $this ), gettype( $message ),
                __METHOD__ )
            );
        }
        self::$admin_messages[$level][] = $message;
        $session = $this->load( 'library', 'session\\Session' );
        $session->set( 'admin-messages', self::$admin_messages );
    }

    /**
     * Admin Actions
     * Declares the actions accessible on the admin dashboard.
     * @return array
     */
    protected function declareActions()
    {
        return array(
            'admin_menu' => 'menu',
            'admin_enqueue_scripts' => 'adminScripts',
            'admin_notices' => 'renderAdminMessage',
            'admin_page_access_denied' => 'renderAdminErrorPage',
        );
    }

    /**
     * Renders an admin page
     *
     * @param int|string $page
     */
    protected function render( $page = null,
        \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view = null )
    {
        if ( !array_key_exists( $page, self::$core_models ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'No valid admin page known by the handle [%1$s]', $page
            ) );
        }
        $this->_renderSettingsPage( $page, $view );
    }

    /**
     * Handles post requests when settings forms are submitted.
     *
     * This
     *
     * @param type $page
     * @return void
     * @final
     */
    final protected function handlePost()
    {
        if ( empty( $_POST ) || !array_key_exists( 'page', $_GET ) )
        {
            return;
        }
        $prefix = $this->getThemePrefix();
        $request_page = $_GET['page'];
        if ( !strpos( $request_page, $prefix ) === 0 )
        {
            //This is not our form.
            //Let whatever else handle it.
            return;
        }
//        d($_POST); exit;
        $handler = esc_attr( substr( $request_page, strlen( $prefix . '-' ) ) );
        if ( !array_key_exists( $handler, self::$core_models ) )
        {
            //Not a valid postback. 403 it.
            $this->error( 403, 'Invalid form submission.',
                array(
                'mode' => 'post',
                'scope' => $this::CONTROLLER_SCOPE,
                'target' => $handler,
                'raw' => $_POST,
                'valid' => array_keys( self::$core_models ),
            ) );
            exit();
        }
        $model = self::$core_models[$handler];
        $values = $_POST;
        /**
         * The abstract model enforces sanitization before
         * allowing any child class model to even touch
         * post values at all.
         */
        try
        {
            $result = $model->postback( $values );
            $this::debugStash( $result, 'settings_updated' );
            if ( $this::AJAX_REQUEST )
            {
                $view = $this->load( 'view',
                    $this::CONTROLLER_SCOPE . '\\AjaxView' );
                $view->renderPart( null, $result );
            }
            if ( $result['success'] )
            {
                $this->adminMessage( 'success',
                    __( 'Settings successfully updated.' ) );
            } else
            {
                $this->adminMessage( 'warning',
                    __( 'Settings update was unsuccessful. Please try again.' ) );
            }
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            //The model has reported that this is not an acceptable request.
            d( $e->getMessage(), $e );
            $this->error(
                $e->getCode(), $e->getMessage(), $e->getTrace()
            );
            exit();
        }
    }

    // -------------------------------------------------------------------------
    // Client Side Declarations
    // -------------------------------------------------------------------------

    /**
     * Returns the data to pass to the client for javascript to consume.
     * @return void
     */
    protected function declareClientData( $page = null )
    {
        $data = parent::declareClientData( $page );
         if ( is_string( $page ) && array_key_exists( $page, self::$core_models ) )
        {
            $model = self::$core_models[$page];
            $data = array_merge( $data, $model->getClientData() );
        }
        return $data;
    }

    /**
     * Declares frontend javascript modules to make discoverable to the
     * frontend library for automatic scripting extension.
     * @return array
     */
    protected function declareModules( $page = null )
    {
        $modules = parent::declareModules( $page );
        if ( is_string( $page ) && array_key_exists( $page, self::$core_models ) )
        {
            $model = self::$core_models[$page];
            $modules = array_merge( $modules, $model->getModules() );
        }
        return $modules;
    }

    /**
     * Declares component data to pass to the frontend
     * for client side representation.
     * @return array
     */
    protected function declareComponents( $page = null )
    {
        return array();
    }

    /**
     * Declares the CSRF token to send to the frontend
     * for ajax cross-site request forgery protection.
     * @return boolean|string
     */
    protected function declareCsrf( $page = null )
    {
        return false;
    }

    /**
     * Declares any domains that should be allowed direct cross-origin resource
     * sharing rights, if any.
     * @return array
     */
    protected function declareCors( $page = null )
    {
        return array();
    }

    /**
     * Declares client side extensions, if any.
     * @return array
     */
    protected function declareClientExtensions( $page = null )
    {
        return array();
    }

    /**
     * Declares endpoints to pass to the frontend, if any.
     * These will be automatically accessible to frontend ajax requests.
     * @return array
     */
    protected function declareEndpoints( $page = null )
    {
        return array();
    }

    /**
     * Declares integrations with the native platform api
     * that the frontend should be made aware of.
     * @return array
     */
    protected function declareIntegrations( $page = null )
    {
        return array();
    }

    /**
     * Sets the baseline render directory for admin templates.
     *
     * @return void
     * @internal
     */
    private function _initializeAdmin()
    {
        if ( !defined( 'CONCATENATE_SCRIPTS' ) )
        {
            define( 'CONCATENATE_SCRIPTS', false );
        }
    }

    /**
     * This method renders an administrative settings page.
     *
     * This method is only ever called when the given settings page has
     * already passed validation as being correctly registered.
     *
     * The visibility of this method prevents child classes from
     * overriding it and breaking this functionality.
     *
     * @param string $page The internal identifying slug
     *     for the current settings page, which corresponds
     *     to a `MODEL_SETTINGS_KEY` of a registered settings model.
     * @return void
     * @internal
     */
    private function _renderSettingsPage( $page )
    {
        $model = self::$core_models[$page];
        $menu_details = $model->getMenuDetails();
        $context = $this->_getSettingsPanelContext( $page, $model );
//        d($context); exit;
        $this->addContext( 'page', $context );
        try
        {
//            s($this::AJAX_REQUEST, $this::HTTP_HOST, $this::REQUEST_METHOD, $this::REQUEST_URI, $_COOKIE, $_SESSION, getallheaders()); exit;
            $this->loadAdminTemplate( 'sanctity/sanctity-settings',
                $this->load( 'library', 'config\\ContextConfig' )->get() );
        } catch ( \Exception $e )
        {
            d( $e, $e->getMessage(), $e->getTrace() );
            exit;
        }
    }

    /**
     * Prepares a settings page for render, escapes and sanitizes everything,
     * and runs the translation filters on readable text.
     *
     * @param string $page The raw page slug, with all of the Wordpress prefix junk stripped off,
     *     so it only represents the internal identifier.
     * @param \mopsyd\sanctity\interfaces\models\SanctitySettingsModelInterface $model
     *     The model that represents the current settings page.
     * @return array
     * @internal
     * @todo This behemoth is very large and ugly, but it works.
     *     Chunk this down into several methods, or its own dedicated view class.
     *     The release package should not have such gargantuan methods in it.
     */
    private function _getSettingsPanelContext( $page, $model )
    {
        $page_details = $model->getPageDetails();
        $section_link_suffix = '/admin.php?page=';
        //Add the baseline theme identity context
        $result = array(
            'title' => __( esc_html( $this->getThemeName() . ' Theme Options' ) ),
            'page_logo' => $this->getSanctityLogo( true ),
            'page_logo_icon' => $this->getSanctityLogo( false ),
            'section_title' => __( esc_html( $page_details['title'] ) ),
            'description' => array_key_exists( 'description', $page_details )
            ? __( esc_html( $page_details['description'] ) )
            : false,
            'uri' => esc_url( admin_url( $section_link_suffix . $page_details['slug'] ) ),
            'slug' => sanitize_title( $page_details['page'] ),
            'icon' => $page_details['icon'],
            'template' => $page_details['template']
        );
        $links = array();
        //Add the links for the other settings pages,
        //if the user is authorized to see them.
        foreach ( self::$core_models as
            $key =>
            $settings_model )
        {
            $settings_model_settings = $settings_model->getMenuDetails();
            if ( !current_user_can( $settings_model_settings['permission'] ) )
            {
                continue;
            }
            $links[$settings_model::MODEL_SETTINGS_KEY] = array(
                'title' => __( esc_html( $settings_model::MODEL_SETTINGS_KEY ===
                        $this::SETTINGS_INDEX_KEY
                        ? 'Dashboard'
                        : $settings_model_settings['menu_title'] ) ),
                'icon' => $settings_model->getPageDetails()['icon'],
                'link' => esc_url( admin_url( $section_link_suffix . $settings_model_settings['slug'] ) ),
                'current' => $settings_model::MODEL_SETTINGS_KEY === $model::MODEL_SETTINGS_KEY
            );
        }
        $subsections = array();
        //Filter details for translations.
        if ( $page_details['subsections'] )
        {
            foreach ( $page_details['subsections'] as
                $key =>
                $subsection )
            {
                if ( $key === "options" )
                {
                    //Skip the model options
                    continue;
                }
                $subsection['title'] = __( esc_html( $subsection['title'] ) );
                if ( array_key_exists( 'description', $subsection ) )
                {
                    $subsection['description'] = __( esc_html( $subsection['description'] ) );
                }
                if ( array_key_exists( 'form', $subsection ) && array_key_exists( 'fields',
                        $subsection['form'] ) )
                {
                    foreach ( $subsection['form']['fields'] as
                        $field_key =>
                        $field )
                    {
                        $subsection['form']['fields'][$field_key]['label'] = __( esc_html( $subsection['form']['fields'][$field_key]['label'] ) );
                        if ( array_key_exists( 'placeholder',
                                $subsection['form']['fields'][$field_key] ) )
                        {
                            $subsection['form']['fields'][$field_key]['placeholder']
                                = __( esc_html( $subsection['form']['fields'][$field_key]['placeholder'] ) );
                        }
                        if ( array_key_exists( 'title',
                                $subsection['form']['fields'][$field_key] ) )
                        {
                            $subsection['form']['fields'][$field_key]['title'] = __( esc_html( $subsection['form']['fields'][$field_key]['title'] ) );
                        }
                        if ( array_key_exists( 'tooltip',
                                $subsection['form']['fields'][$field_key] ) )
                        {
                            $subsection['form']['fields'][$field_key]['tooltip']
                                = __( esc_html( $subsection['form']['fields'][$field_key]['tooltip'] ) );
                        }
                        if ( array_key_exists( 'popover',
                                $subsection['form']['fields'][$field_key] ) )
                        {
                            $subsection['form']['fields'][$field_key]['popover']
                                = __( esc_html( $subsection['form']['fields'][$field_key]['popover'] ) );
                        }
                    }
                }
                $subsections[$key] = $subsection;
            }
        }
        $result['links'] = $links;
        $result['subsections'] = $subsections;
        return $result;
    }

    /**
     * Loads the appropriate javascript and stylesheets for
     * the settings page currently being viewed.
     *
     * This insures that settings pages never pollute the rest of
     * the dashboard with their dependency scripts or stylesheets,
     * and that they only ever render on the appropriate page.
     *
     * @param string $page The internal slug of the current page,
     *     which corresponds to a registered settings model key.
     * @return void
     * @internal
     */
    private function _enqueueSettingsPanelScripts( $page )
    {
        parent::initializeScripts( $page );
        $model = self::$core_models[$page];
        foreach ( $model->getStyles( 'settings' ) as
            $style )
        {
            $this->load( 'library', 'media\\Stylesheet' )->load( $style )->compile()->register();
        }
        foreach ( $model->getScripts( 'settings' ) as
            $script )
        {
            $this->load( 'library', 'media\\Javascript' )->load( $script )->compile()->register();
        }
    }

    private function _enqueueDashboardScripts( $page )
    {
        //no-op
    }

    /**
     * Loads the baseline required models designated by the base level
     * theme as essential. Extension models are added separately.
     *
     * These models cannot be overridden by plugins or child themes,
     * because it will cause the settings panel to break if they are malformed.
     *
     * Each of these models presents an api for extension, in the event
     * their output needs to be modified or added to.
     *
     * @return void
     * @internal
     */
    private function _loadCoreModels()
    {
        if ( !self::$core_models_loaded )
        {
            $this::debugStash( self::$settings_models, 'expected_models',
                'core_models' );
            foreach ( self::$settings_models as
                $class )
            {
                $model = $this->load( 'model', $class );
                $model->setScope( $this::CONTROLLER_SCOPE );
                $model->register();
                self::$core_models[$model::MODEL_SETTINGS_KEY] = $model;
                $this::debugStash( get_class( $model ),
                    $model::MODEL_SETTINGS_KEY, 'core_models' );
            }
            $this::debugStash( __FUNCTION__, 'initialization_steps_completed' );
            self::$core_models_loaded = true;
        }
    }

    private function _initializeAdminMessages()
    {
        if ( empty( self::$admin_messages ) )
        {
            foreach ( array_keys( $this->getConfig( 'admin-message-levels' ) ) as
                $level )
            {
                self::$admin_messages[$level] = array();
            }
            $this::debugStash( __FUNCTION__, 'initialization_steps_completed' );
        }
    }

    /**
     * Enforces that the post handler MUST go through the proper channel.
     *
     * This method, the post handler itself, and the method referenced
     * in the settings model are all final methods, to enforce that logic
     * only proceed through the provided api for security purposes.
     *
     * This method will always be registered first for post handling,
     * and prevents child classes from overriding it.
     *
     * The only way to circumvent this is to unregister the entire
     * queue aggregator directly with Wordpress,
     * which prevents anything from being done at all by the theme.
     * There is a failsafe hardcoded into the base class that will
     * kill the page if this occurs, and notify the administrator
     * that a hack attempt or dangerously unsafe plugin is
     * threatening their site.
     *
     * @return void
     * @internal
     * @final
     */
    final private function _enqueueMandatoryMethods()
    {
        //The post handler is registered at the minimum possible integer value,
        //so that it is not possible to insert any action before it.
        $this->bindAction( 'init', 'handlePost', PHP_INT_MIN );
    }

    private function _getSettingsPageKey( $page )
    {
        $prefix = $this->getThemePrefix() . '_page_';
        $toplevel_prefix = 'toplevel_page_';
        if ( strpos( $page, $toplevel_prefix ) === 0 )
        {
            //The top level page
            $page = $this::SETTINGS_INDEX_KEY;
        } elseif ( strpos( $page, $prefix ) === 0 )
        {
            //A sub page
            $page = substr( $page,
                strlen( $prefix . $this->getThemePrefix() . '-' ) );
        }
        return $page;
    }

}
