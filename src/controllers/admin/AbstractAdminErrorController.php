<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers\admin;

/**
 * Abstract Admin Error Controller
 * Controls errors for the Admin Dashboard.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractAdminErrorController
    extends \mopsyd\sanctity\controllers\AbstractErrorController
    implements \mopsyd\sanctity\interfaces\controllers\admin\SanctityAdminControllerInterface,
    \mopsyd\sanctity\interfaces\controllers\SanctityErrorControllerInterface
{

    const CONTROLLER_SCOPE = 'admin';

    private static $admin_errors = array(
        401 => 'Sorry, you are not allowed to access this page.',
        403 => 'Sorry, you are not allowed to access this page.',
        404 => 'This page does not exist.',
        500 => 'The server has encountered an error.',
    );

    private $message;
    private $code;
    private $arguments;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handles the error. This fires immediately after the error info is handled,
     * so any modifications to that info should be available within this method
     * using `getErrorInfo`.
     * @param type $type
     * @param type $message
     */
    protected function handleError( $type, $message = null )
    {
        if ( !doing_action( 'admin_page_access_denied' ) )
        {
            do_action( 'admin_page_access_denied' );
        }
        if (is_null($message))
        {
            $message = self::$admin_errors[$type];
        }
        $this->message = $message;
//        $model = $this->load( 'model', 'display\\LayoutModel' );
//        $this->addContext('layout', $model->getSettings());
//        $model = $this->load( 'model', 'display\\BrandingModel' );
//        $this->addContext('branding', $model->getSettings());
//        $model = $this->load( 'model', 'display\\ErrorModel' );
//        $model->initialize( $type, $this->getErrorContext() );
//        $this->bindDataContext( $model );
        $this->render();
    }

    /**
     * Renders the page after all validation, data aggregation,
     * and other logic and opinion has been applied.
     *
     *
     * @param int|string $page The id or slug of the page to render.
     *     If no page is provided, it will be extracted from the registered
     *     contextual information. If "error" is provided, the correct error
     *     page will be extracted from the contextual information.
     * @param mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     *     If not supplied, the default view will be used. If a view object is supplied,
     *     the provided view object will be initialized and used in place of the default.
     * @return void
     * @note This method will terminate execution upon completion.
     */
    protected function render( $page = null,
        \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view = null )
    {
        if ( !is_null( $page ) )
        {
            d( 'Update here to render arbitrary pages.', $page );
            exit();
        }
        if (!(defined('WP_DEBUG') && WP_DEBUG))
        {
            //Flush everything not relevant if debug is off.
            ob_end_clean();
        }
        wp_die( __( (string) $this->message ), $this->code, $this->arguments );
//        $layout = $this->getLayout();
//        $this->addContext( 'layout', $layout );
//        $view = $this->getView( $view );
//        $test = $this->load( 'model', 'display\\PageModel' );
//        $view->render();
    }

}
