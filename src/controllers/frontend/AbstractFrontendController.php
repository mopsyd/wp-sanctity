<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\controllers\frontend;

/**
 * Abstract Frontend Controller
 * Controls default functionality of Sanctity for the site frontend.
 * This controller can be extended to provide additional frontend
 * controller functionality.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractFrontendController
    extends \mopsyd\sanctity\controllers\AbstractController
    implements \mopsyd\sanctity\interfaces\controllers\frontend\SanctityFrontendControllerInterface
{

    const CONTROLLER_SCOPE = 'frontend';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Performs any operations that must occur after Sanctity, Twig,
     * and any other underlying dependencies have properly instantiated.
     */
    public function initialize()
    {
        try
        {
            //Initialize the admin dashboard.
            $this->_initializeFrontend();
        } catch ( \Exception $e )
        {
            //If anyone sets up any broken overrides, this should catch it.
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException( sprintf(
                'Controller initialization of [%1$s] could not resolve,'
                . ' due to an exception of type [%2$s] encountered during '
                . 'initialization, with message [%3$s]. Please contact the '
                . 'developer of [%1$s] and request assistance.',
                get_class( $this ), get_class( $e ), $e->getMessage()
            ), $e->getCode(), $e );
        }
        parent::initialize();
    }

    /**
     * Frontend Actions
     * Declares the actions accessible on the frontend.
     * @return array
     */
    protected function declareActions()
    {
        return array();
    }

    /**
     * Enqueues the javascript for the frontend.
     * @return void
     * @final
     */
    protected function initializeScripts( $page = null )
    {
        parent::initializeScripts( $page );
        $this->_enqueueDefaultScripts();
    }

    /**
     * Enqueues the css and stylesheets for the frontend.
     * @return void
     * @final
     */
    protected function initializeCss()
    {
        parent::initializeCss();
        $this->_enqueueDefaultStylesheets();
    }

    /**
     * Enqueues the default frontend scripts.
     * @return void
     * @internal
     * @todo Abstract this out better.
     */
    private function _enqueueDefaultScripts()
    {
        $scripts = array(
            'bootstrap',
            'fontawesome',
            'common',
            'frontend'
        );
        foreach ( $scripts as
            $script )
        {
            $this->load( 'library', 'media\\Javascript' )
                ->load( $script )
                ->compile()
                ->register();
        }
    }

    private function _enqueueDefaultStylesheets()
    {
        $styles = array(
            get_option($this->getThemePrefix() . '-bootstrap-skin', 'bootstrap'),
            'frontend'
        );
        foreach ( $styles as
            $style )
        {
            $this->load( 'library', 'media\\Stylesheet' )->load( $style )->compile()->register();
        }
        // Debug test
        $this->load( 'library', 'media\\Stylesheet' )->load( 'style-colors' )->compile()->register();
    }

    private function _initializeFrontend()
    {
        if ( !defined( 'CONCATENATE_SCRIPTS' ) )
        {
            define( 'CONCATENATE_SCRIPTS', true );
        }
        $paths = array(
            SANCTITY_BASEDIR . 'templates/' );
        $layout = $this->load( 'model', 'display\\LayoutModel' );
        $layout->setScope( $this::CONTROLLER_SCOPE );
        $layout_paths = $layout->getPaths();
        $theme_dir = get_template_directory();
        $child_dir = get_stylesheet_directory();
    }

}
