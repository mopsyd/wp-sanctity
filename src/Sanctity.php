<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//namespace mopsyd\sanctity;

/**
 * Sanctity Front Controller
 *
 * This class handles the bootstrap initialization for Sanctity,
 * and proxies the public api to the rest of the system, which allows all typical
 * external usage to operate against a single class.
 *
 * DO NOT OVERRRIDE THE FRONT CONTROLLER **(this file)** IN YOUR CHILD THEME.
 *
 * All of the typical wordpress theme files only need to call the
 * appropriate method in this class to render their content.
 * By the time anything needs to actually render,
 * the underlying engine will have already parsed out the entire structure
 * of the page view intuitively enough to know what you want,
 * leaving very little need to fuss with extra parameters.
 *
 * You may of course feed in custom parameters for a more customized approach.
 * This is not excluded. However for a typical render that uses the normal
 * Sanctity logic to display, there is really not much need
 * to send anything extra to it.
 *
 * --------------------------------------------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
final class Sanctity
    implements \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface
{

    /**
     * The theme name will always be reflected by this class constant.
     */
    const THEME_NAME = 'Sanctity';

    /**
     * The theme version will always be reflected by this class constant.
     */
    const THEME_VERSION = '0.2.0';

    /**
     * The theme slug will always be reflected by this class constant.
     */
    const THEME_SLUG = 'sanctity';

    /**
     * Represents the adapter that does all of the
     * Wordpress under the hood stuff.
     *
     * @var \mopsyd\sanctity\libs\wordpress\WordpressAdapter
     */
    private static $wordpress_adapter;

    /**
     * Represents the adapter that does all of the
     * Wordpress under the hood stuff.
     *
     * @var \mopsyd\sanctity\libs\twig\WordpressAdapter
     */
    private static $template_adapter;

    /**
     * Represents the master configurations set of data for the current runtime.
     *
     * @var \mopsyd\sanctity\libs\config\CoreConfig
     */
    private static $core_config;

    /**
     * Represents the context data used to generate the output data for the view.
     *
     * @var \mopsyd\sanctity\libs\config\CoreConfig
     */
    private static $core_context;

    /**
     * Represents the model that handles extension integration.
     *
     * @var mopsyd\sanctity\models\integration\ExtensionModel
     */
    private static $extension_model;

    /**
     * Represents the factory factory, which loads all of the factories
     * through factory factorization. Well then, that was a mouthful.
     *
     * Super meta, but the end result is you don't have to guess
     * which factory to deal with. This one handles all of them automatically.
     *
     * @var mopsyd\sanctity\factory\FactoryFactory
     */
    private $factory;

    /**
     * Represents the router used to determine which controller to load.
     * @var mopsyd\sanctity\interfaces\RouterInterface
     */
    private $router;

    /**
     * Represents the active controller for the current runtime.
     * @var mopsyd\sanctity\interfaces\controllers\SanctityControllerInterface
     */
    private $controller;

    /**
     * Holds a persistent instance for late event-bound routing of error pages and such.
     *
     * This is not a singleton. You can make another instance if you want,
     * but there's really no reason to except for unit testing. There is also
     * no way to externally access this instance directly again if you didn't
     * save it in a variable the first time.
     *
     * @var \Sanctity
     */
    private static $instance;

    public function __construct()
    {
        $this->_loadFactory();
        $this->_loadAdapter();
        $this->_loadConfiguration();
        $this->_loadContext();
        $this->_loadController();
        $this->_initializeIntegrationSupport();
        $this->controller->initialize();
        if ( is_null( self::$instance ) )
        {
            self::$instance = $this;
        }
    }

    /**
     * Used for overloading the public accessor, and providing additional
     * public api methods that are directly callable against the accessor.
     *
     * @param string $name The name of the overloaded method called.
     * @param array $arguments Any arguments passed into the overloaded method.
     *
     * @return mixed
     *
     * @throws \mopsyd\sanctity\libs\exception\MethodNotFoundException
     *     This method throws this exception if the method was not found.
     *
     * @note Other exceptions may arise if the referenced
     *     methods api is not adhered to.
     */
    public static function __callStatic( $name, $arguments )
    {
        ;
    }

    /**
     * Lists the api methods currently available that have been
     * registered into the overloaded api.
     *
     * @return array
     *
     * @note This method returns the doc-block comment of each method
     *     registered into its api. If the docblock comment for a registered
     *     method is not populated, that may not be all that helpful.
     *     The methods provided with this system have pretty verbose documentation
     *     in their docblocks in anticipation of this, but there is no realistic way
     *     to enforce this behavior for any other method registered by a 3rd party.
     *
     * @note This method is primarily meant to be a developer tool for
     *     evaluating their options. This should not be used for any
     *     production code, or as part of any actual production functionality.
     *     It it simply gives you a quick reference for what can be done with
     *     Sanctity in the present scope.
     *     This method relies on Class Reflection, which is not very performant
     *     if numerous methods have been entered, and will kill page performance
     *     quickly if you try to base any actual runtime page logic on it.
     */
    public static function api()
    {
        ;
    }

    public function routeErrorPage( $error = 404 )
    {
        $this->controller->error( $error );
    }

    /**
     * Performs the active routing action for a post, page, etc.
     *
     * This is typically called from the static accessor dropped
     * into whatever Wordpress theme file needs to access it.
     *
     * This is just a passthrough to the controller.
     *
     * @param type $post
     */
    public function routePost( $post = null, $context = null )
    {
        try
        {
            $this->controller->handlePage( $post, $context );
        } catch ( \mopsyd\sanctity\interfaces\SanctityExceptionInterface $e )
        {
            $this->controller->error( 500 );
        }
    }

    /**
     * Handles inbound requests for partial content rendering.
     *
     * This is just a proxy to the controller. It can be called directly if
     * you saved the instantiated instance, or otherwise is proxied to by
     * the static method `renderPart`. Either way has the same effect.
     *
     * The actual logic is done internally from the controller already
     * associated with the current scope. This method just fires a call
     * through to that.
     *
     * @param type $section
     * @param type $context
     */
    public function routePartial( $section, $context = null, $element_id = null )
    {
        try
        {
            $this->controller->handlePart( $section, $context, $element_id );
        } catch ( \mopsyd\sanctity\interfaces\SanctityExceptionInterface $e )
        {
            $this->controller->error( 500 );
        }
    }

    /**
     * Runs the installer if not installed,
     * or the uninstaller if the theme is deleted.
     */
    public function checkActivation()
    {
        try
        {
            //@todo
        } catch ( \mopsyd\sanctity\interfaces\SanctityExceptionInterface $e )
        {
            $this->controller->error( 500 );
        }
    }

    /**
     * Registers a Sanctity extension.
     *
     * Extensions should be registered immediately when your plugin
     * or theme base file loads, so that none of the internal hooks
     * within wordpress are missed if they apply to your logic.
     *
     * If they are registered late, it will not make a difference to Sanctity,
     * but some of your logic may be omitted if bound hooks have already passed by.
     *
     * Sanctity is designed to perform its entire setup at the beginning
     * of the 'theme_loaded' hook, so it should be registered within that
     * hook if a hook is used.
     *
     * @param \mopsyd\sanctity\interfaces\libs\ExtensionInterface $extension
     *     May be expressed as either a string or an instantiated object.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided extension does not implement the correct interface.
     *     The provided class may be represented either as a string or an object,
     *     but must implement the Sanctity extension interface to be acceptable.
     * @see \mopsyd\sanctity\interfaces\libs\ExtensionInterface
     */
    public static function extend( $extension )
    {
        if ( is_object( $extension ) )
        {
            $class = get_class( $extension );
        } else
        {
            $class = $extension;
        }
        $expected = 'mopsyd\\sanctity\\interfaces\\libs\\ExtensionInterface';
        if ( !is_string( $class )
            && class_exists( $class )
            && in_array( $expected, class_implements( $class ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Provided [$extension] is not a valid instance of '
                . '[%1$s].', $expected )
            );
        }
        if ( !is_object( $extension ) )
        {
            $extension = $extension::init();
        }
        self::$extension_model->registerExtension( $extension );
    }

    /**
     * This method renders a page/post/etc.
     *
     * Static proxy method that calls either `routePost` or `routeErrorPage`
     * on the pre-instantiated instance. This method is just provided as a
     * convenience to manually call page loads if need be, without the overhead
     * of manually constructing an active instance.
     *
     * This should be the only method you ever need to call from any of
     * the expected Wordpress page/post files to add theme support.
     *
     * This method is smart enough to not really need any parameters,
     * because the system has already set up tracking when it bootstrapped
     * by the point you would ever call this.
     *
     * @note If for whatever reason you have a legitimate post with
     *     the same ID as an error code, pass it as a string and it will
     *     resolve to the actual page. If you pass an error code
     *     as an integer, you will get the error page.
     *
     * @param int|string $subject (optional) An error code (401, 403, 404, 500), post id,
     *     canonicalized name, or what have you. It's smart enough to figure
     *     out what you meant. If you give it nothing it handles the current
     *     page automatically.
     * @return void
     */
    public static function render( $subject = null, $data = null )
    {
        if ( in_array( $subject,
                array(
                401,
                403,
                404,
                500 ) ) )
        {
            //Error Houston
            self::$instance->routeErrorPage( $subject );
        } else
        {
            //Nah, regular page content.
            self::$instance->routePost( $subject, $data );
        }
    }

    /**
     * Renders a portion of content that is not a full page/post in and of itself.
     *
     * Static proxy method that calls `routePartial`
     * on the pre-instantiated instance. This method is just provided as a
     * convenience to manually call partial section loads, without the overhead
     * of manually constructing an active instance.
     *
     * This method handles widgets, sidebars, etc. The string should correspond
     * to the file that typically would sit in your theme root folder,
     * and the method will do the rest, provided that Sanctity bootstrapped
     * properly in your functions.php file.
     *
     * This method can also be called internally to render custom parts,
     * and it is registered directly into Twig so it can be used directly
     * inline within the template, although the internal logic
     * does not use it.
     *
     * @param string $section The section to render.
     * @param array $context (optional) encapsulated context for a section,
     *     if not supplied it will receive the full context of the current scope.
     *     This parameter typically should only be called via a Twig call, but it
     *     can be used independently without issue as well, provided you don't go
     *     crazy automating it and lose track of what is scoped where.
     * @param int $element_id (optional) The id of the specific element to render,
     *     which is useful if you are trying to isolate a specific widget, sidebar,
     *     etc. Corresponds to the internal Wordpress id for the element.
     * @return void
     * @example `\Sanctity::renderPart( 'footer', array( 'footerstuffs' => 'stuff to give the footer template to use locally in its template' ); //full content of footer.php
     * @example `\Sanctity::renderPart( 'header' ); //full content of header.php
     * @example `\Sanctity::renderPart( 'sidebar' ); //full content of sidebar.php
     */
    public static function renderPart( $section, $context = null,
        $element_id = null )
    {
        self::$instance->routePartial( $section, $context, $element_id );
    }

    /**
     * Loads the controller object, and injects an instance of self into it
     * through dependency injection for Visitor pattern operability.
     * @return void
     * @internal
     */
    private function _loadController()
    {
        if ( is_null( $this->router ) )
        {
            $this->_loadRouter();
        }
        $this->controller = $this->router->getController();
        $this->controller->setSiteObject( $this );
    }

    /**
     * Loads the Wordpress Router, which in turn decides
     * which controller should handle the page.
     * @return void
     * @internal
     */
    private function _loadRouter()
    {
        $router = $this->factory->load( 'LibraryFactory' )->load( 'router\\WordpressRouter' );
        $this->router = $router;
    }

    /**
     * Loads the FactoryFactory, which loads all of the things.
     *
     * Also dependency inject the thing which loads all of the things.
     *
     * @return void
     * @internal
     */
    private function _loadFactory()
    {
        $this->factory = new \mopsyd\sanctity\factory\FactoryFactory();
        \mopsyd\sanctity\models\AbstractModel::setFactory( $this->factory );
        \mopsyd\sanctity\controllers\AbstractController::setFactory( $this->factory );
        \mopsyd\sanctity\views\AbstractView::setFactory( $this->factory );
        \mopsyd\sanctity\libs\config\AbstractConfig::setFactory( $this->factory );
        \mopsyd\sanctity\libs\extension\AbstractExtension::setFactory( $this->factory );
        \mopsyd\sanctity\libs\router\AbstractRouter::setFactory( $this->factory );
        \mopsyd\sanctity\libs\rest\AbstractRest::setFactory( $this->factory );
        \mopsyd\sanctity\libs\session\Session::setFactory( $this->factory );
        \mopsyd\sanctity\libs\parser\HtmlParser::setFactory( $this->factory );
        \mopsyd\sanctity\libs\twig\TwigAdapter::setFactory( $this->factory );
        \mopsyd\sanctity\libs\wordpress\WordpressAdapter::setFactory( $this->factory );
    }

    /**
     * Loads the WordpressAdapter, which does all of the Wordpress things.
     *
     * Also dependency inject the thing which does all of the Wordpress things.
     *
     * @return void
     * @internal
     * @note This class is not overridable, which is why we are calling an
     *     explicit instance instead of using the factory to fetch it.
     */
    private function _loadAdapter()
    {
        self::$wordpress_adapter = new \mopsyd\sanctity\libs\wordpress\WordpressAdapter();
        \mopsyd\sanctity\AbstractBase::setAdapter( self::$wordpress_adapter );
    }

    /**
     * Loads the core configuration, which provides the uniform runtime data store
     * for the system to minimize database lookups, and keep settings and configurations
     * consistently serializable down to one flat set if required,
     * or alternately indexed (it supports both).
     *
     * Also dependency inject the thing which does all of the configuration things.
     *
     * @return void
     * @internal
     * @note This class is not overridable, which is why we are calling an
     *     explicit instance instead of using the factory to fetch it.
     */
    private function _loadConfiguration()
    {
        self::$core_config = $this->factory->load( 'LibraryFactory' )->load( 'config\\CoreConfig' );
    }

    /**
     * Loads the core configuration, which provides the uniform runtime data store
     * for the system to minimize database lookups, and keep settings and configurations
     * consistently serializable down to one flat set if required,
     * or alternately indexed (it supports both).
     *
     * Also dependency inject the thing which does all of the configuration things.
     *
     * @return void
     * @internal
     * @note This class is not overridable, which is why we are calling an
     *     explicit instance instead of using the factory to fetch it.
     */
    private function _loadContext()
    {
        self::$core_context = $this->factory->load( 'LibraryFactory' )->load( 'config\\ContextConfig' );
        \mopsyd\sanctity\AbstractBase::setContext( self::$core_context );
    }

    /**
     * The single binding actually needed to be handled here instead of in
     * the controller is the one that checks the installation.
     *
     * All of the rest of them get handled by controllers via the adapter.
     * @return void
     * @internal
     * @note After a bit of reading, this can actually get factored out
     *     of this class and go into the Admin Controller where it belongs.
     *     Awesome. Will put that on the todo list.
     */
    private function _registerBaselineBindings()
    {
        //Check if this theme was activated or deactivated, and take appropriate measures.
        add_action( 'after_switch_theme',
            array(
            $this,
            'checkActivation' ) );
        //Last call for extension registration.
        add_action( 'after_setup_theme', 'initializeExtensions',
            array(
            self::$extension_model,
            'closeRegistration' ), PHP_INT_MAX );
    }

    private function _initializeIntegrationSupport()
    {
        \mopsyd\sanctity\models\integration\ExtensionModel::setSanctity( $this );
        self::$extension_model = new \mopsyd\sanctity\models\integration\ExtensionModel();
        self::$extension_model->setAutoloader( \mopsyd\sanctity\libs\autoload\Autoload::init() );
    }

}
