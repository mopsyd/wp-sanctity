<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity;

/**
 * AbstractBase
 * This is the base level abstract class for all Sanctity
 * PHP classes other than the base accessor, which is the
 * dedicated Front Controller for WordPress.
 *
 * All Sanctity classes extend this class,
 * except for ones that MUST extend some other class (PDO, Exception, etc).
 *
 * The only exception to this rule is the Front Controller (`Sanctity`),
 * which does not extend this class because it packages this class,
 * and couldn't load if it extended it.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class AbstractBase
    implements interfaces\SanctityInterface
{
    //--------------------------------------------------------------------------
    //       Universal Stuff That Also Needs To Work On 3rd Party Classes
    //
    // The following traits are intentionally broken off for interoperability
    // with 3rd party class extensions like Exception, PDO, and other
    // such constructs that need to be compatible with the rest of this system,
    // but can't extend this class for one reason or another.
    //
    //--------------------------------------------------------------------------

/**
     * Debug all of the things. Safely for production though.
     *
     * This is broken off into a trait so it can also be attached to classes
     * that extend from 3rd party sources without any duplicate code.
     *
     * DRY and all that.
     */
    use traits\Debugger;

/**
     * Containerize all of the things.
     *
     * This is broken off into a trait so it can also be attached to classes
     * that extend from 3rd party sources without any duplicate code.
     *
     * DRY and all that.
     */
    use traits\ContainerPackagerUtility;

/**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use traits\AdapterCompatibility;

    //--------------------------------------------------------------------------
    //                              Variant Constants
    //
    // The following constants may be overridden in child classes
    // to produce alternate results as needed. Much of the logic handling these
    // is accomplished here in the base abstraction, but these constant values
    // allow configuration along expected lines in subclasses.
    //
    //--------------------------------------------------------------------------

    /**
     * This is the path that the class will look for configuration files.
     * It may be overridden in child classes to define a different
     * configuration directory if needed.
     *
     * This constant is not declared in the interface to preserve
     * the ability to override.
     */
    const RELATIVE_CONFIG_PATH = 'config';

    /**
     * This is the public folder path, which contains assets that
     * should be publicly viewable by the theme. This constant
     * can be overridden to declare a different public folder.
     *
     * This constant is not declared in the interface to preserve
     * the ability to override.
     */
    const RELATIVE_PUBLIC_PATH = 'dist';

    /**
     * This is the folder that templates are stored in. This directory
     * should not be publicly accessible, but should be readable by the
     * webserver, and writable by the webserver if you want to enable
     * some functionality for editing templates through the
     * Wordpress admin interface (not included with this theme).
     *
     * You may override this constant in a child class to declare
     * a different template directory, without disrupting the operations
     * of classes expecting to find the templates they need in this one.
     *
     * This constant is not declared in the interface to preserve
     * the ability to override.
     */
    const RELATIVE_TEMPLATE_PATH = 'templates';

    /**
     * If provided, it should map to a filename in the config directory.
     * If that file exists, it will be statically cached when the class
     * instantiates, and can be directly requested from any other class
     * that shares the same key through the `getConfig` method.
     *
     * The class will not be allowed to instantiate
     * if this constant is misconfigured.
     *
     * This constant is not declared in the interface to preserve
     * the ability to override.
     */
    const CONFIG_FILE = false;

    /**
     * If provided, it should map to a key in the specified config file.
     * This will scope all requests through `getConfig` to within that
     * specific key, which lets you create multipurpose config files that
     * only display relevant information within the correct class scope,
     * without the overhead of parsing the file repeatedly,
     * or parsing numerous files either.
     *
     * This constant is not declared in the interface to preserve
     * the ability to override.
     */
    const CONFIG_KEY = false;

    /**
     * This is the baseline container scoping definition, which allows all
     * classes to cast data into this container object by calling
     * `self::containerize($data)`.
     *
     * This constant may be overridden to scope return results appropriate
     * to the current class scope at any level.
     */
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\Container';

    //--------------------------------------------------------------------------
    //                              Universal Values
    //
    // The following variables are compiled statically in the base class, so that
    // All other classes can access them in one form or another through an accessible
    // getter method defined in the base class. This is used to insure that data
    // is only loaded a single time in all cases where it is applicable, and that
    // any subsequent class of the same type can recall its data without the
    // overhead of loading it again for any value that is static and unchanging.
    // None of these provide child classes with a means of mutating them;
    // they can get but not set. Additional updates to these class properties
    // can be appended by the base class as is appropriate at any time, but
    // child classes have no say in the matter, they can only request from it.
    //
    // @note Sanctity does not use any global or public variables. Literally none.
    //
    //   In fact, it pretty much doesn't even use protected variables either.
    //
    //      Everything has a getter method when needed for something.
    //
    //          This also means that it doesn't leave anything
    //
    //            cluttering up the global scope whatsoever.
    //
    //             It also means child classes cannot even
    //
    //              internally break stuff they shouldn't.
    //
    //               Which is why, in fact, that it runs
    //
    //                  so much more stable than your
    //
    //                     typical Wordpress theme
    //
    //                          Ever dreamt of
    //
    //                             running.
    //
    //--------------------------------------------------------------------------

    /**
     * This is the class that does literally every single thing involving
     * the Wordpress Api, so none of the other classes need to be concerned
     * with whether or not they are honoring the underlying platform's spec.
     *
     * Everything involving Wordpress gets done through this little guy.
     *
     * That way classes can remain entirely oblivious of wordpress methods
     * for the most part, and operate strictly in their own context.
     *
     * The only real edge case involved is some minor data formatting that has
     * to mostly conform to what wordpress wants,
     * but that can be pretty easily abstracted out for the most part too.
     *
     * @var \mopsyd\sanctity\libs\wordpress\WordpressAdapter
     */
    private static $wordpress_adapter;

    /**
     * All configs parsed at runtime are cached into this property privately
     * so they do not need to be retrieved from disk again. Subclasses may only
     * access what they are authorized to access.
     * @var array
     */
    private static $configs_parsed = array();

    /**
     * Represents a set of namespaces registered
     * to allow for 3rd party extension.
     * @var array
     */
    private static $namespace_prefixes = array(
        'mopsyd\\sanctity'
    );

    /**
     * Represents any extensions that have been registered with Sanctity.
     * @var array
     */
    private static $extensions = array(
        );

    /**
     * Represents the context, which allows for distribution of the
     * contextual data throughout the entire system.
     *
     * @var \mopsyd\sanctity\interfaces\libs\config\ContextConfigInterface
     */
    private static $context;

    /**
     * Designates whether debug mode is set or not for the current runtime.
     *
     * By default, this will correspond to the value of WP_DEBUG,
     * but it is also configurable through the theme settings to run
     * independently of this if need be.
     * @var bool
     */
    private static $debug_mode = false;

    /**
     * The default behavior of the base abstract is to only set debug mode one time,
     * and then skip it on all subsequent page class instantiations.
     *
     * @var bool
     */
    private static $debug_mode_set = false;

    /**
     * Designates whether or not data registration has closed for data meant
     * to be fed to the frontend. This typically happens when the frontend
     * scripts are enqueued. Data registration after that point will result
     * in an exception being raised to prevent unexpected lack of data keys
     * from propogating to the frontend.
     *
     * @var bool
     */
    private static $data_registration_closed = false;

    //--------------------------------------------------------------------------
    //                         Universal Public Api
    //
    // Below are the methods that are always available on every
    // single Sanctity class. These represent basic organization
    // of how objects intercommunicate, and informative methods
    // for figuring out what the context of any given object is.
    //
    //--------------------------------------------------------------------------

    /**
     * <Default Sanctity Constructor>
     *
     * The default Sanctity constructor allows for
     * two standard parameters: dependencies and args.
     *
     * The base class does nothing with these.
     *
     * This approach supports the Sanctity standard for factorization,
     * which indicates that constructor dependency injection ALWAYS is
     * passed into the constructor as the first parameter if it applies,
     * and arguments to give the instance upon instantiation ALWAYS get
     * passed into the second parameter if they apply.
     *
     * In most cases, neither apply. Most dependency injection is done
     * statically in whatever scope the object that needs it exists on
     * within the inheritance chain, and then is basically `prototyped`
     * into subsequent instantiations automatically (similar to the javascript
     * approach to object orientation). There are a few edge cases where
     * specific arguments MUST be passed into instantiation for one reason or
     * another, so this setup supports this universally, so a global standard
     * for instantiation can be declared in interfaces that works everywhere
     * (with the exception of classes that HAVE to extend a non-Sanctity class).
     *
     * --------
     *
     * The actions taken by this constructor only fire the first time any
     * Sanctity class extending it calls its parent constructor, and will
     * occur when any such class does so. It initializes the configuration,
     * preflights the environment to make sure globally required dependencies
     * have been appropriately set by the Front Controller, and that's about it.
     *
     * @param mixed $dependencies (optional) Does nothing in this scope.
     * @param array $args (optional) Does nothing in this scope.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initConfig();
        $this->_setDebugMode();
    }

    /**
     * <Auto-Initialization Method>
     *
     * Returns a fully configured
     * instance of self correctly.
     *
     * All Sanctity classes that do not
     * extend a 3rd-party class have this method.
     *
     * This method can be used to automatically instantiate
     * a class directly by name, or to produce a copy of
     * any Sanctity class from any other instance that is distinct,
     * and does not share state with the copy that produced it.
     *
     * Classes that do not require additional setup steps beyond
     * __construct can use this as is.
     *
     * Classes that do require additional steps should override this,
     * so that they can keep their constructor methods tidy and performant.
     *
     * This system is designed to inherently separate instantiation from
     * initialization (initialization is static, instantiation is not),
     *
     * so ALL classes extending from this class should
     * instantiate cleanly without parameters.
     *
     * This allows objects to be prototyped as needed from a base instance
     * that is not polluted with irrelevant state as needed.
     *
     * That also means no singletons. Never, ever, ever singletons.
     *
     * The unit testing pipeline will flat out fail any Sanctity class
     * that operates like a Singleton.
     *
     * Sanctity classes MUST ALWAYS be able to produce a distinct and unique copy.
     *
     * @return mopsyd\sanctity\interfaces\SanctityInterface
     */
    public static function init( $dependencies = null, $args = array() )
    {
        $class = get_called_class();
        $instance = new $class( $dependencies, $args );
        return $instance;
    }

    /**
     * <Context Setter Method>
     *
     * This method is for dependency injection of the Context, which contains
     * all of the data intended to be fed eventually to the View for final output.
     * The context object is globally available to all classes that are
     * extensions of this class (though not directly to extensions that
     * originate from other 3rd party classes).
     *
     * This fires automatically during the bootstrap,
     * so you don't really need to mess with it much.
     *
     * It only takes an instance of the context container,
     * to avoid collisions with other configuration containers,
     * which would produce erratic results throughout the system.
     *
     * @param \mopsyd\sanctity\interfaces\libs\config\ContextConfigInterface $context
     *
     * @see \mopsyd\sanctity\AbstractBase::getAdapter Every other class
     *     in the system has access to this method if it extends from
     *     the base abstract.
     *
     * @note This approach, having been taken from very early development,
     *     makes the possiblity of eventually integrating this platform into
     *     another system like Magento, SugarCRM, Drupal, Joomla, or any other
     *     platform entirely approachable with minimal tweaks to the core code.
     *     Since the core only knows it talks to an adapter to handle the
     *     underlying platform, there is really no particular reason why
     *     changing the adapter would make much difference, provided the new one
     *     has the same public api and returns the same required keys.
     *     ...Don't hold your breath on that though. It's possible, not probable.
     */
    public static function setContext( \mopsyd\sanctity\interfaces\libs\config\ContextConfigInterface $context )
    {
        self::$context = $context;
    }

    protected function getContextData( $key = null )
    {
        return self::$context->get( $key );
    }

    protected function setContextData( $key, $data )
    {
        return self::$context->set( $key, $data );
    }

    /**
     * <Extension Registration Method>
     *
     * Registers an extension.
     *
     * Oh yea, did I tell you that Sanctity has it's own unique extension Api?
     *
     * Anything can hook into it by extending the extension class
     * and registering it with `\Sanctity::extend` at some point
     * during the `after_setup_theme` hook.
     *
     * Extension acceptance is closed on `after_setup_theme`
     * with a priority of `PHP_INT_MAX`, so any lower priority
     * than the actual highest possible number on the current machine
     * will get your extension registered correctly.
     *
     * Extensions that attempt to register later than this will
     * raise an exception, because they need to be evaulated
     * during the `init` hook to see of they do anything that
     * applies to setup (`init` is when the actual internal setup happens).
     *
     * This also lets you directly hook into Sanctity without having to use
     * the wonky hook system of Wordpress, at which point it will expose some
     * of its internals to your class to play with, which are otherwise
     * inaccessible. Note that site owners can ALWAYS disable any extension,
     * and registered extensions are "opt-in", not "opt-out", meaning they
     * have to be turned on by the site owner before they do anything at all.
     *
     * Also note that extensions in release that generate a lot of complaints,
     * bug reports, or other headaches, or are designated by the Wordpress core team
     * as bad will be blacklisted completely in subsequent releases.
     * The blacklist is hard-coded in the source, so there's really no getting around it.
     *
     * This method only accepts a fully instantiated object
     * implementing the correct interface.
     *
     * For practical purposes, you should use `\Sanctity::extend`
     * in place of this method directly, as that will fully propogate through the
     * system correctly, and will also catch an error at runtime instead of
     * forcing a crash (the base accessor version of this method has ducktyped
     * type checking to prevent page errors on faulty extensions).
     *
     * @param \mopsyd\sanctity\interfaces\libs\ExtensionInterface $extension
     * @return void
     */
    public static function extend( \mopsyd\sanctity\interfaces\libs\ExtensionInterface $extension )
    {
        self::_getCoreConfig();
        $prefix = $extension::getPackageNamespace();
        if ( !array_key_exists( $prefix, self::$extensions ) )
        {
            self::$extensions[$prefix] = array();
        }
        $class = get_class( $extension );
        if ( !array_key_exists( $class, self::$extensions[$prefix] ) )
        {
            $current = get_called_class();
            $extension->register( $current::init() );
            self::$extensions[$prefix][$class] = $extension;
        }
        if ( !in_array( $extension::getPackageNamespace(),
                self::$namespace_prefixes ) )
        {
            self::$namespace_prefixes[] = $extension::getPackageNamespace();
        }
    }

    //--------------------------------------------------------------------------
    //                         Universal Internal Api
    //
    // Below are the methods that are internally applicable throughout the entire
    // Sanctity architecture. These methods provide a common ground for how all
    // Sanctity classes handle their internal operations in a streamlined manner.
    //
    //--------------------------------------------------------------------------

    /**
     * Binds data to the frontend javascript context,
     * to be fed into client side scripts.
     *
     * This data will automatically be packaged for
     * frontend consumption with no further effort.
     *
     * @param type $key
     * @param type $value
     * @note This process must be completed prior to the `enqueue`
     *     method for the current context, or it will not be added.
     *     Data registration is closed when the scripts are queued.
     */
    protected function bindClientData( $key, $value )
    {
        if ( self::$data_registration_closed )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. '
                . 'Attempted to bind frontend data after '
                . 'data registration has been closed. '
                . 'Please register your data prior to '
                . 'the frontend scripts being entered '
                . 'into the queue.', get_class( $this ) )
            );
        }
        $config = $this->load( 'library', 'config\\ClientDataConfig' );
        $config->set( $key, $value );
    }

    /**
     * Closes data registration, so no further frontend data
     * can be queued beyond this point.
     *
     * This should only be called by controllers.
     * It cannot be turned back on once it is closed
     * at any point within the same runtime.
     *
     * @return void
     */
    protected function closeDataRegistration()
    {
        self::$data_registration_closed = true;
    }

    /**
     * This is used internally to insure that loader classes
     * pick up on all registered locations for classes.
     *
     * All namespace prefixes registered as valid extension locations
     * for the Factory classes to look for overrides will be returned
     * from this method.
     *
     * @return type
     */
    protected function getPrefixes()
    {
        return self::$namespace_prefixes;
    }

    /**
     * Returns any extensions that have been registered
     * @return array
     */
    protected function getExtensions()
    {
        return self::$extensions;
    }

    /**
     * Returns the name of the current theme.
     * @return string
     */
    protected function getThemeName()
    {
        $name = (string) wp_get_theme( get_option( 'stylesheet' ) );
        return $name;
    }

    /**
     * Returns the Sanctity logo in svg format.
     *
     * The default icon is served via svg.
     * It can be obtained either as a web-accessible svg file,
     * or as an encoded string for embedding directly.
     *
     * @param bool $full_logo If true, returns the full size logo.
     *     If false, returns the icon.
     * @param bool $as_embed If true, returns the svg embed data.
     *     If false, returns the web include path for the unencoded file.
     * @return string
     */
    protected function getSanctityLogo( $full_logo = false, $as_embed = false )
    {
        if ( $as_embed )
        {
            $icon = 'data:image/svg+xml;base64,' . file_get_contents( $full_logo
                    ? SANCTITY_BASEDIR . 'config/sanctity_logo_encoded'
                    : SANCTITY_BASEDIR . 'config/sanctity_icon_encoded' );
        } else
        {
            $icon = $this::getAdapter()->get('uri', 'sanctity') . '/dist/media/img/' . ( $full_logo
                ? 'sanctity.svg'
                : 'sanctity-icon.svg' );
        }
        return $icon;
    }

    /**
     * Returns the prefix for the current theme,
     * which is the name cast to lower case with spaces replaced with underscores,
     * and extraneous characters stripped.
     * @return string
     */
    protected function getThemePrefix()
    {
        $name = (string) wp_get_theme( get_option( 'stylesheet' ) );
        $prefix = strtolower( str_replace( ' ', '_', $name ) );
        return esc_attr( preg_replace( "/[^A-Za-z0-9_ ]/", '', $prefix ) );
    }

    /**
     * Designates whether the current theme is a child theme or the parent theme.
     * @return bool
     */
    protected function isChildTheme()
    {
        return get_template_directory() !== get_stylesheet_directory();
    }

    /**
     * This method designates whether debug is enabled or not.
     * It may be overridden in any class to provide alternate behavior.
     * By default, its return value will correspond to the value of WP_DEBUG,
     * or false if that is not defined.
     * @return bool
     */
    protected function checkDebug()
    {
        return self::$debug_mode;
    }

    /**
     * Fetches a section from the core Sanctity Configuration.
     *
     * This configuration is read only. It does not exist in the
     * database and cannot be overridden.
     *
     * @param string $key The core configuration key to fetch
     * @return bool|array Returns false if the config key is not set,
     *     otherwise returns an array of values corresponding to the
     *     given key.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Key not found. Handle this appropriately if it occurs.
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the key is passed in an invalid format. Key must be a string.
     *     This exception should be allowed to bubble up.
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the parent config does not exist or is not readable,
     *     it indicates that either the theme is broken or there is
     *     a bug in the parent class logic.
     *     This exception should be allowed to bubble up.
     */
    protected function getConfig( $key = null )
    {
        if ( !$this::CONFIG_FILE )
        {
            //No config declared.
            return;
        }
        $this->_initConfig();
        if ( !$this::CONFIG_KEY )
        {
            $config = $this->_getConfig( $this::CONFIG_FILE );
        } else
        {
            $config = $this->_getConfig( $this::CONFIG_FILE, $this::CONFIG_KEY );
        }
        if ( is_null( $key ) )
        {
            //Return the whole config
            return $config;
        }
        if ( !is_string( $key ) )
        {
            //Invalid parameter. Requesting class has faulty logic.
            //his exception should not be handled.
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured. Invalid parameter passed at [%2$s].'
                . 'Expected [%3$s], but received [%4$s].', get_class( $this ),
                __METHOD__, 'string', gettype( $key ) )
            );
        }
        if ( !array_key_exists( $key, $config ) )
        {
            //Key not found. This exception should be handled.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid configuration key [%1$s] requested in [%2$s].',
                $key, get_class( $this ) ) );
        }
        return $config[$key];
    }

    //--------------------------------------------------------------------------
    //                         Under The Hood
    //
    // Below are the internals that drive baseline operations and keep the system
    // running correctly. You mostly don't need to worry about this stuff at all.
    //
    //--------------------------------------------------------------------------

    /**
     * Sets the baseline debug mode across all classes,
     * if it has not already been set.
     * @return void
     * @internal
     */
    private function _setDebugMode()
    {
        if ( !self::$debug_mode_set )
        {
            self::$debug_mode = false;
            if ( defined( 'WP_DEBUG' ) )
            {
                self::$debug_mode = WP_DEBUG
                    ? true
                    : false;
            }
            self::$debug_mode_set = true;
        }
    }

    /**
     * Initializes the loading sequence if it has not already occurred,
     * but only if the current class requires that a configuration be present.
     *
     * Otherwise it quietly returns without taking any action.
     *
     * @return void
     * @internal
     */
    private function _initConfig()
    {
        if ( !$this::CONFIG_FILE )
        {
            return;
        }
        $this->_loadConfig();
    }

    /**
     * Loads a json config file if it is not already,
     * and saves it statically to prevent additional
     * calls to the disk for the remainder of runtime.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the parent config does not exist or is not readable,
     *     it indicates that either the theme is broken or there is
     *     a bug in the parent class logic.
     * @internal
     */
    private function _loadConfig()
    {
        if ( array_key_exists( $this::CONFIG_FILE, self::$configs_parsed ) )
        {
            return;
        }
        $child = array();
        $paths = $this->_getConfigPaths();
        if ( !is_readable( $paths['parent'] ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Unable to locate or parse base configuration file [%1$s] in [%2$s]. '
                . 'If it exists in this location, it is not readable by the server.',
                $paths['parent'], get_class( $this ) )
            );
        }
        $base = json_decode( file_get_contents( $paths['parent'] ), 1 );
        if ( array_key_exists( 'child', $paths ) && is_readable( $paths['child'] ) )
        {
            $child = json_decode( file_get_contents( $paths['child'] ), 1 );
        }
        $full = array_replace_recursive( $base, $child );
        self::$configs_parsed[$this::CONFIG_FILE] = $full;
    }

    /**
     * Does a search to see if the child theme contains an
     * equivalent config file, and returns an array of only
     * the parent config or both.
     *
     * @return array
     * @internal
     */
    private function _getConfigPaths()
    {
        $paths = array();
        $stylesheet_dir = trailingslashit( get_stylesheet_directory() );
        $template_dir = trailingslashit( get_template_directory() );
        $parent_config = trailingslashit( $template_dir . $this::RELATIVE_CONFIG_PATH ) . $this::CONFIG_FILE;
        $child_config = trailingslashit( $stylesheet_dir . $this::RELATIVE_CONFIG_PATH ) . $this::CONFIG_FILE;
        $paths['parent'] = $parent_config;
        if ( $child_config !== $parent_config )
        {
            $paths['child'] = $child_config;
        }
        return $paths;
    }

    /**
     * Returns the configuration section from the statically loaded configuration file.
     *
     * Methods accessing this logic are expected to only reference valid keys.
     *
     * @param type $config
     * @param type $key
     * @return type
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the declared configuration key does not match any key
     *     in the declared configuration data.
     * @internal
     */
    private function _getConfig( $config, $key = null )
    {
        $config = self::$configs_parsed[$config];
        if ( is_null( $key ) )
        {
            return $config;
        }
        if ( !array_key_exists( $key, $config ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Invalid configuration key [%1$s] specified in [%2$s]. '
                . 'The specified key does not exist in configuration file [%3$s].',
                $key, get_class( $this ), $config )
            );
        }
        return $config[$key];
    }

}
