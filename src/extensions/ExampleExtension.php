<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\extensions;

/**
 * Example Extension
 *
 * This is an example extension that displays
 * the correct layout for a Sanctity extension.
 *
 * This class can be used as boilerplate for your own extensions.
 *
 * You do not need to declare most of the methods in this class,
 * as the underlying abstract already has null state declarations in place.
 * They exist here for reference only.
 *
 * All of the methods that return an empty array can be entirely omitted.
 *
 * The methods that return anything else must be provided.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @see mopsyd\sanctity\libs\extension\AbstractExtension
 *     This is the class you want to extend if you are writing an extension.
 * @see mopsyd\sanctity\interfaces\libs\ExtensionInterface
 *     This is the interface that is type checked for, which you should use
 *     if you have a class that cannot extend the provided abstract, or if
 *     you just like doing things the hard way.
 */
class ExampleExtension
    extends \mopsyd\sanctity\libs\extension\AbstractExtension
{

    /**
     * This constant represents the visible name for your extension
     * that displays in the settings panel.
     */
    const EXTENSION_NAME = 'Example Extension';

    /**
     * This constant represents the visible description for your extension
     * that displays in the settings panel. End users will use this to identify
     * the underlying purpose of your extension, and decide whether or not
     * to enable its features.
     */
    const DESCRIPTION = 'This is an example extension, provided with the Sanctity source code as an example of how to implement additional functionality with this system. This extension does not do anything other than to serve as an example of usage.';

    /**
     * This constant represents the package that your extension is part of.
     *
     * This should correspond to your theme or plugin slug, composer package,
     * or root vendor\package namespace. This should be unique to your own release.
     */
    const EXTENSION_PACKAGE = 'mopsyd\\sanctity';

    /**
     * This constant represents the version number of your extension.
     *
     * This can correspond to your plugin or theme version number,
     * or can be tracked independently.
     *
     * Sanctity uses this value to track extension integrity.
     * Problematic extensions are disabled until their
     * version number increments.
     *
     * If your extension causes known bugs, it will remain disabled even if
     * the rest of your package updates until this value changes.
     * If this occurs three times and it still incurs severe instability,
     * it may be registered into the permanent blacklist.
     *
     * The blacklist is based on user reports of stability and package integrity,
     * and is compiled directly into the Sanctity source code. If an extension
     * is blacklisted, no installation running that version of Sanctity will
     * enable it unless they have their site set to run in debug mode.
     *
     * For this reason, it is important that your respond to bug tickets
     * and error reports if you would like your extension to remain
     * in good standing. Any extensions found to be utilizing Sanctity
     * functionality to perform any subversive action, expoits,
     * or create the installation of any backdoors or other serious
     * intentional malfeasance will be immediately blacklisted with
     * no possiblity of getting restored to good standing.
     */
    const EXTENSION_VERSION = '1.0.0';

    /**
     * Return your Psr-4 compliant namespace root and profit.
     *
     * This is the only mandatory method if you are extending the provided abstract.
     * The remaining methods in this example just provide a template for
     * straightforward understanding of what you can do with your extension.
     *
     * @return string
     */
    public static function getPackageNamespace()
    {
        return 'mopsyd\\sanctity\\extensions';
    }

    /**
     * This method must be overridden, and must correspond to the
     * source directory that your Psr-4 namespace resolves to.
     *
     * This will be used in conjunction with `getPackageNamespace`
     * to register your package with the autoloader. If this is not valid,
     * your extension will be disabled.
     *
     * This method is provided as an example of appropriate setup.
     * You will note that the path is the current directory,
     * and the result of `getPackageNamespace` resolves to the
     * same namespace as this class declares.
     *
     * You are not restricted to placing your extension in your root
     * source directory, but that is the easiest way to do it.
     *
     * All paths your extension declares will be considered to be relative
     * to the absolute disk location returned by this method, and many path
     * resolvers actively prevent reverse filepath navigation
     * for security reasons. Although it is possible to place your extension
     * class elsewhere, the most straightforward implementation is to just
     * put it in your plugin or theme root directory, or alternately to put
     * all of your assets managed by Sanctity internals in a child directory
     * of its location.
     *
     * @return string
     */
    public static function getPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR;
    }

    /**
     * Override this method to declare any components your extension provides.
     *
     * This should return an associative array, where the key is the slug of the component,
     * and the value is the name of a class that implements the component interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\components\ComponentInterface
     *
     * @return array
     */
    protected function declareComponents()
    {
        return array();
    }

    /**
     * Override this method to declare any controllers your extension provides.
     *
     * This should return an associative array, where the key is the slug of the controller,
     * and the value is the name of a class that implements the controller interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\controllers\ControllerInterface
     *
     * @return array
     */
    protected function declareControllers()
    {
        return array();
    }

    /**
     * Override this method to declare any models your extension provides.
     *
     * This should return an associative array, where the key is the slug of the model,
     * and the value is the name of a class that implements the model interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\models\ModelInterface
     *
     * @return array
     */
    protected function declareModels()
    {
        return array();
    }

    /**
     * Override this method to declare any views your extension provides.
     *
     * This should return an associative array, where the key is the slug of the view,
     * and the value is the name of a class that implements the view interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\views\ViewInterface
     *
     * @return array
     */
    protected function declareViews()
    {
        return array();
    }

    /**
     * Override this method to declare any libraries your extension provides.
     *
     * This should return an associative array, where the key is the slug of the library,
     * and the value is the name of a class that implements the Sanctity base interface.
     *
     * @see \mopsyd\sanctity\interfaces\SanctityInterface
     *
     * @return array
     */
    protected function declareLibs()
    {
        return array();
    }

    /**
     * Override this method to declare any factories your extension provides.
     *
     * This should return an associative array, where the key is the slug of the factory,
     * and the value is the name of a class that implements the factory interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\factory\FactoryInterface
     *
     * @return array
     */
    protected function declareFactories()
    {
        return array();
    }

    /**
     * Override this method to declare any template paths your extension provides.
     *
     * This should return an array with values equal to any template paths
     * you want to register, relative to the declared extension root.
     * Template paths may not contain backwards filepath navigation.
     *
     * An associative key is not required for template registration.
     * Your extension name will be used for the key of the template group
     * resulting from generation of your template set.
     *
     * @return array
     */
    protected function declareTemplates()
    {
        return array();
    }

    /**
     * Override this method to declare any actions your extension needs to register.
     *
     * This should return an associative array, where the key is the hook you
     * want to run the action on, and the value is an array of all callbacks
     * that need to fire on that hook, in the order of preference.
     *
     * You may register multiple methods against a single hook in this manner,
     * and they will all fire in order on one single binding. They will fire
     * immediately after the core Sanctity actions have resolved for the
     * same action, if any are present.
     *
     * @return array
     */
    protected function declareActions()
    {
        return array();
    }

    /**
     * Override this method to declare any filters your extension needs to register.
     *
     * This should return an associative array, where the key is the filter you
     * want to provide callbacks for, and the value is an array of all callbacks
     * that need to fire on that filter, in order of preference.
     *
     * You may register multiple methods against a single filter in this manner,
     * and they will all fire in order in one single binding. They will fire
     * immediately after the core Sanctity filters have resolved for the same
     * filter, if any are present.
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareFilters()
    {
        return array();
    }

    /**
     * Override this method to declare any layouts your extension provides.
     *
     * Layouts must contain a manifest.json file in their root directory,
     * which provides a map of its assets and configurations. You may declare
     * any previously existing layout as a parent layout and inherit all
     * settings from that layout that you do not explicitly define.
     * Layouts that do not declare a parent use the default layout as
     * their parent (it is not possible to create a layout other than
     * the default that does not have a parent).
     *
     * This insures that no miscellaneous settings are not correctly resolved.
     * You may override all of the settings of the default if you care to,
     * but any that are missed will use the default (or whichever parent you
     * declare, which in turn may have a parent, and eventually has the
     * default as a parent).
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareLayouts()
    {
        return array();
    }

    /**
     * Override this method to declare any configurations your extension provides.
     *
     * This should return an associative array, where the key is the config id,
     * and the value either an array of config data, a relative filepath to a
     * json file that contains the config data, or a callback that will generate
     * the configuration data dynamically. This flexibility allows for
     * configurations to be generated a number of ways, including from a
     * flat file, from application logic, or by being pulled from a cache
     * or data layer.
     *
     * Configurations that have a previously existing key will be recursively
     * replaced into the existing config. Configs that do not have a previously
     * existing key will be containerized and retained independently in the
     * configuration manager. This process does not alter the origin data of
     * existing configs, so registration needs to occur on each runtime,
     * or the default will be used in place of the extension config.
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareConfigs()
    {
        return array();
    }

}
