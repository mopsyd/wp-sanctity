<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\adapters;

/**
 * Abstract Adapter
 *
 * This is the baseline for all adapters that defer work out to another
 * subsystem of some sort, whether it be the templating engine,
 * the platform that Sanctity is running on (Wordpress), etc.
 *
 * This class allows the front controller to prepare only a single class
 * via dependency injection with required details of the Sanctity system,
 * which are then immediately accessible in a uniform scope to all adapters.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractAdapter
extends \mopsyd\sanctity\AbstractBase
implements \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface
{
    const DEFAULT_SCOPE = 'global';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * This is defined by the controller upon initial boot after the routing step,
     * and remains locked in for the remainder of runtime.
     *
     * @var string
     */
    private static $scope;

    private static $valid_scopes = array(
        self::DEFAULT_SCOPE,
        \mopsyd\sanctity\controllers\admin\AbstractAdminController::CONTROLLER_SCOPE,
        \mopsyd\sanctity\controllers\frontend\AbstractFrontendController::CONTROLLER_SCOPE,
//        \mopsyd\sanctity\controllers\login\AbstractLoginController::CONTROLLER_SCOPE,
    );

    /**
     * This is defined by the root front controller, and presents a single
     * consistent factorization instance used to produce any system classes
     * required while conforming to autoload standards and allowing for
     * extensions to override instances in a uniform way.
     *
     * @var \mopsyd\sanctity\factory\FactoryFactory
     */
    private static $factory;

    /**
     * Sets the factory factory.
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $factory
     */
    public static function setFactory( \mopsyd\sanctity\factory\FactoryFactory $factory )
    {
        self::$factory = $factory;
    }

    /**
     * Internal getter for the factory object
     *
     * @return \mopsyd\sanctity\factory\FactoryFactory
     */
    protected function getFactory()
    {
        return self::$factory;
    }

    /**
     * Sets the scope, which is the relative directory within template
     * directories to search for templates.
     *
     * This insures that templates that are supposed to reflect admin or
     * login pages are not included on frontend requests accidentally.
     *
     * @param type $scope
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the scope is already defined, and the provided scope is not exactly identical.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the scope does not exist in the set of whitelisted scopes.
     * @note The scope is set automatically by the View, which defines the
     *     scope based on its own internal constant value. There is a View class
     *     dedicated to each major subsection of Wordpress, and the correct one
     *     is loaded based upon the controller that is loaded during the
     *     bootstrap process.
     *     This method should not be called as part of the public api,
     *     as it is meant to operate only in conjunction with a View object.
     */
    public static function setScope( $scope )
    {
        if ( !is_null( $scope ) && $scope === self::$scope )
        {
            //Duplicate scope setting should be non-blocking
            //for factorization purposes, such as when an error controller
            //attempts to re-register scope within the same bounds as
            //the parent controller that called it.
            self::debugStash( array( //For debug purposes, this gets logged, but only if debug mode is enabled.
                'scope' => $scope,
                ), 'scope_already_registered' );
            return;
        }
        if ( !is_null( self::$scope ) )
        {
            self::debugStash( array(
                'scope' => $scope,
                ), 'scope_registration_blocked' );
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Provided scope [%1$s] cannot be set because the '
                . 'scope has already been declared in [%2$s].', $scope,
                get_class( $this ),
                implode( ', ', self::$valid_scopes ) )
            );
        }
        if ( !in_array( $scope, self::$valid_scopes ) )
        {
            self::debugStash( array(
                'scope' => $scope,
                ), 'scope_not_valid' );
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Provided scope [%1$s] is not valid in [%2$s]. '
                . 'Valid scopes are [%3$s].', $scope, get_class( $this ),
                implode( ', ', self::$valid_scopes ) )
            );
        }
        self::$scope = $scope;
        self::debugStash( array(
            'scope' => $scope,
            ), 'scope_registered_successfully' );
    }

    /**
     * Returns the scope the Adapter is cast to,
     * or false if it has not been declared.
     *
     * @return bool|string
     */
    public static function getScope()
    {
        if (is_null(self::$scope))
        {
            return false;
        }
        return self::$scope;
    }

    /**
     * Returns a work chain object scoped to the current adapter.
     *
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function chain()
    {
        $chain = $this->load('library', 'chain\\WorkChain');
        $chain->scope($this);
        return $chain;
    }
}
