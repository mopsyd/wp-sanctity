<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\adapters\platform;

/**
 * Abstract Platform Adapter
 *
 * This is the baseline adapter abstraction for handling interoperability
 * with the underlying platform that Sanctity is running on.
 *
 * This class determines a standardized means for controllers, models,
 * and views to make calls to the platform that Sanctity is running on,
 * which decouples platform internals entirely from the underlying
 * Sanctity codebase, and opens the capacity for this system to be
 * interoperable across numerous opinionated platforms
 * (eg: Wordpress, Magento, Drupal, Joomla, PHPBB, etc).
 *
 * That way the order of operations of the underlying Sanctity system
 * is not disrupted by the quirks of the platform it is running on.
 * The Platform Adapter is expected to receive a standardized set
 * of requests, and resolve them against the internals of the
 * platform by whatever means is required, and return a consistent
 * result regardless of how the actual platform works.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @note This is a bit bare at the moment, but it will get
 *     fleshed out significantly more as time goes on.
 */
abstract class AbstractPlatformAdapter
    extends \mopsyd\sanctity\adapters\AbstractAdapter
    implements \mopsyd\sanctity\interfaces\adapters\platform\PlatformAdapterInterface
{

    private static $view;

    /**
     * Sets the view object so that the platform adapter can output through
     * the provided render layer in whatever normal context the platform demands.
     *
     * @param \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     */
    public static function setView( \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view )
    {
        self::$view = $view;
    }

    /**
     * Internal view getter for the platform adapter.
     * 
     * @return \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    protected static function getView()
    {
        return self::$view;
    }

}
