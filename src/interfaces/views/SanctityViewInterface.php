<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\views;

/**
 * Sanctity View Interface
 * All Sanctity Views have this interface.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface SanctityViewInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{
    /**
     * The default View constructor.
     *
     * Blocks instantiation if the class is not correctly configured,
     * which enforces integrity in child class logic, so abstraction
     * maintains expected integrity.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     Thrown if required view properties are not configured correctly.
     */
    public function __construct();

    /**
     * Sets the Sanctity Front Controller object, which is operated upon by the View.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site
     */
    public function setSite( \mopsyd\sanctity\interfaces\SanctityFrontControllerInterface $site );
//
//    /**
//     * Sets a contextual value, which is passed to the View,
//     * so that it can be reflected in the template.
//     *
//     * This method returns an instance of self for method chaining.
//     *
//     * @param string $key
//     * @param mixed $value
//     */
//    public function setContext( $key, $value );

    /**
     * Sets HTTP headers that should be declared prior to render.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param type $headers
     */
    public function setHeaders( array $headers );

    /**
     * Sets the root template that the view is expected to render.
     *
     * This method returns an instance of self for method chaining.
     *
     * @param type $template
     */
    public function setTemplate( $template );

    /**
     * Sets any headers registered with the View.
     *
     * This method returns an instance of self for method chaining.
     *
     * @return \mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    public function commitHeaders();

    /**
     * Renders the current template.
     *
     * If $return is true, the response body will be returned as a
     * string variable, or a stringable object that can be directly echoed.
     * If false, the View will echo the output directly, and return an instance
     * of itself for method chaining.
     *
     * @param bool $return (optional) If return is true,
     *     the render data will be returned as a value. If false,
     *     it will be echoed directly and the View will return
     *     an instance of self. Default false.
     * @return string|\mopsyd\sanctity\interfaces\views\SanctityViewInterface
     */
    public function render( $return = false );
}
