<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Settings Interface
 *
 * This interface represents an object tasked with getting and setting options
 * and parameters through the underlying platform.
 *
 * Sanctity expects that an object bearing this interface will appropriately
 * perform get and set operations for any settings required to be registered
 * through the platform being operated against, and will be capable of
 * translating them from the Sanctity format to the platform format,
 * and vice versa.
 *
 * This interface provides the required contract for streamlined getter
 * and setter operations of values obtained from adapter interaction,
 * and the appropriate handling the underlying api for leveraging this.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface SettingsInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

    /**
     * Gets a value by the provided key, and returns the default
     * if the key is not currently available.
     *
     * @param string $key The identifier for the get operation
     * @param string $default (optional) a default value to assign
     *     as the response if no key can be retreived.
     * @return mixed Returns the key if the key exists,
     *      and the default if it does not exist.
     */
    public function get( $key, $default = null );

    /**
     * Sets the value for the given key.
     *
     * @return void
     * @throws \mopsyd\sanctity\interfaces\libs\exception\ExceptionInterface
     *     If the represented setting is read only and a set
     *     operation is encountered.
     */
    public function set( $key, $value );
}
