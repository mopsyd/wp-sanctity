<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * <Work Chain Interface>
 *
 * This interface represents an external statement used to perform a series
 * of sequential operations against an adapter, and automatically pass the
 * prior response value as the parameter into the next task.
 *
 * A work chain must have an external api that reflects relatively
 * natural language,making the chaining of its methods read like a
 * standard sentence for the most part.
 *
 * It is tasked with encapsulating an Adapter, and providing a means to
 * chain its methods that the Adapter itself does not provide natively.
 *
 * Where an adapter performs an operation directly and returns the exact result,
 * a work chain internally stores the value provided by an adapter command,
 * and returns only itself until one of its break methods are called,
 * at which point it returns whatever it has from the last prior operation,
 * or executes the result of its last prior operation. A work chain may have
 * a `result` method that returns the result, `a render` method that prints
 * the result, and an `execute` method that runs the result.
 * All of these are considered to be break methods that will cause the chain
 * to reset when referenced, and return only the result, or a boolean
 * determination as to whether the underlying operation ran successfully.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface WorkChainInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

    // -------------------------------------------------------------------------
    //                      Scoping Methods
    //
    // These methods are chainable, and represent a change in the adapter that
    // the chain is operating against. The adapter may be swapped out at
    // various points within the chain to produce alternate results.
    // Subsequent methods in the chain must adhere to the api of the adapter
    // that is currently scoped.
    //
    // -------------------------------------------------------------------------

    /**
     * Scoping method.
     * This method is used to scope the work chain to an adapter,
     * which will then be used to operate against internally as
     * the subject that the chain references to enact further methodology.
     *
     * @param \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface $adapter
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function scope( \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface $adapter );

    // -------------------------------------------------------------------------
    //                      Control Methods
    //
    // These methods are chainable, and represent conditional logic within
    // the work chain. These are used to provide control blocks for how the
    // chain operates. These do not alter chain operation or prevent the chain
    // from running (except the `when` method, which can halt the chain entirely),
    // but rather scope the subject pointer when various events or conditions
    // are met.
    //
    // -------------------------------------------------------------------------

    /**
     * Control Method.
     * Means "Monitor the work chain results, and when the result of any
     * operation equals the condition, run this method and set the
     * subject pointer to the result."
     *
     * This is essentially an `if` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function on( $condition, $method, $command );

    /**
     * Control Method.
     * This must immediately follow an `on` call.
     *
     * Means "Monitor the work chain results, and if the previous `on` method
     * is not equal but this is equal to the result when the result of any
     * operation equals the condition, run this method and set the
     * subject pointer to the result."
     *
     * This is essentially an `elseif` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function elseOn( $condition, $method, $command );

    /**
     * Control Method.
     * This must immediately follow an `on` call or an `elseOn` call.
     *
     * Means "Monitor the work chain results, and if the previous `on` or `elseOn` method
     * is not equal to the result, run this method and set the subject pointer to the result."
     *
     * This is essentially an `elseif` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function otherwise( $method, $command );

    /**
     * Control Method.
     * This means "Run this Work Chain break method when this condition equals
     * the current subject pointer, and do not proceed with the rest of the chain".
     *
     * For example, you might say that when you get a string,
     * call `render` and stop the chain. You might also say when you get a callable,
     * call `execute` and stop the chain.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @param string $command A break method that exists in the work chain,
     *     not the scoped adapter. This method does not reference the adapter,
     *     it references `self`. Only break methods may be called via this logic
     *     to keep usage at least somewhat sane.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function when( $condition, $command );

    // -------------------------------------------------------------------------
    //                      Looping Methods
    //
    // These methods are chainable, and designate how iteration occurs for the
    // rest of the chain beyond the point they are declared. ALL remaining methods
    // in the chain will be called on each iteration, so this should be used
    // with care.
    //
    // -------------------------------------------------------------------------

    /**
     * Looping Method.
     * Means "Iterate over the subject pointer with this adapter command,
     * and run the rest of the chain up until a break method is encountered
     * on each result".
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function each( $method, $command );

    /**
     * Looping Method.
     * This means "Continue the next operation until the result
     * equals the provided condition, and run the rest of the chain
     * up until a break method is encountered on each result".
     *
     * Essentially this means run a `while` loop on the next thing done with
     * the provided argument as its break condition.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @param int $limit This is a failsafe breakpoint for iteration.
     *     If it is any value greater than zero, iteration will be halted
     *     if it proceeds beyond that number of iterations to prevent
     *     infinite recursive loop bugs.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function until( $condition, $limit = -1 );

    /**
     * Looping Method.
     * This may be called immediately after `until` to add additional
     * conditions that will break the `while` loop.
     *
     * This may be called any number of times immediately after an `until` call,
     * and will continue to add additional break conditions without starting the loop.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function orUntil( $condition );

    // -------------------------------------------------------------------------
    //                      Binding Methods
    //
    // These methods are chainable, and designate that an underlying
    // adapter method should be called at this point in the work chain.
    // These are the typical execution methods that fire underlying logic
    // that the work chain is wrapping.
    //
    // -------------------------------------------------------------------------

    /**
     * Binding Method.
     * Means "Use the result of the last method as the argument for this one".
     *
     * This is the standard chainable binding,
     * and represents the most common usage.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function to( $method, $command );

    /**
     * Binding Method.
     * Scopes the chain to the provided argument without obtaining
     * any result from the adapter, and without regard to the prior
     * subject pointer.
     *
     * @param mixed $args
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function from( $args );

    /**
     * Binding Method.
     * Chains a method with arbitrary arguments,
     * and sets the result as the subject pointer.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @paran mixed $args The subject to provide to the method being called.
     */
    public function with( $method, $command, $args );

    /**
     * Binding method.
     * Packages the results of the operation into the provided container
     * instead of scoping the subject pointer to it.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container
     *     A container object to package the result of the operation into.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function into( $method, $command, \mopsyd\sanctity\interfaces\libs\container\ContainerInterface &$container );

    /**
     * Binding method.
     * This method means "Use the last parameter for this also",
     * and will use the same subject pointer that the prior
     * method in the chain did.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function also( $method, $command );

    // -------------------------------------------------------------------------
    //                      Break Methods
    //
    // These methods are ALWAYS non chainable within the scope of the work chain.
    // They are used to finalize a chain statement, representing the end of
    // a logical expression. Most allow you to pick up where you left off
    // and continue, with the exception of the `reset` method, which clears all
    // internals of the chain back to a completely fresh state.
    //
    // -------------------------------------------------------------------------

    /**
     * Break method that discards results entirely,
     * and resets the chain back to a clean state.
     *
     * This method MUST be non-blocking.
     *
     * This method returns nothing, and calling it
     * MUST NOT be chainable to another method.
     *
     * @return void
     */
    public function reset();

    /**
     * Break method that prints the result from the prior operation.
     * Returns true if printing is possible and occurred successfully,
     * returns false otherwise. Attempts to print non printable parameters
     * result in an exception.
     *
     * This method MUST return a boolean value, and MUST NOT
     * be chainable to another method when called.
     *
     * This method MUST NOT reset the chain.
     *
     * @return bool
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function render();

    /**
     * Break method that executes the result from the prior operation.
     * Returns true if execution resolved, and false otherwise.
     * Attempts to execute a non-executable parameter results in an exception.
     *
     * This method MUST return the result of executing the current subject pointer.
     *
     * This method MUST NOT reset the chain.
     *
     * @return bool
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function execute();

    /**
     * Break method that returns the result obtained from the prior operation.
     *
     * This method may ONLY be chainable if the resulting parameter
     * is an object that provides its own chainable methods.
     * Any other value MUST NOT be chainable.
     *
     * This method MUST be non-blocking.
     *
     * This method MUST NOT reset the chain.
     *
     * @return mixed
     */
    public function result();
}
