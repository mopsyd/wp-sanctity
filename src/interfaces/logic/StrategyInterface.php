<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Strategy Interface
 *
 * This interface represents an object that determines the appropriate order
 * of execution, and returns a standardized subset of commands that Sanctity
 * internals understand.
 *
 * It is tasked with resolving the Sanctity logic
 * against the specifics of the actual platform. This allows the underlying
 * Sanctity logic to remain decoupled with the specific keys and/or methods
 * required by the platform to operate, by using an object bearing
 * this interface via the Platform Adapter dedicated to resolving
 * the distinction, or reverse-parsing it from a Platform-specific
 * keyword back to the generalized set used internally.
 *
 * Sanctity only expects operational order to be provided by an object bearing
 * this interface, and that it will provide answers for any given Sanctity
 * key that needs to resolve against the platform, or appropriately determine
 * a workaround if the underlying Platform does not provide a means of direct
 * implementation of the logic.
 *
 * This interface provides the required contract for allowing the internals
 * of Sanctity to operate in a more functional than procedural paradigm,
 * remaining more event driven than occurring in a strict order of operations
 * determined by the actual Sanctity system. There are some caveats to this,
 * which is why the Front Controller performs the baseline setup step prior
 * to referencing this subset, so that the low level execution is not disrupted
 * if there is a discrepancy.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface StrategyInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
