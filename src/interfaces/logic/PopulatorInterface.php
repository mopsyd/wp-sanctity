<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Populator Interface
 *
 * A populator is similar to a parser, except that it is provisioned with data
 * mutation to produce a final consistent outward result.
 *
 * Populators and sanitizers are the only objects that should be mutating raw values.
 * Parsers may alter key names, but are expected not to alter values, unless they
 * are wrapping them in containerization by deferring to an object that either
 * aggregates, populates, or sanitizes to perform the task.
 *
 * Sanctity expects a populator to return a fully usable result that
 * requires no additional consideration to use as output. A populator may
 * accomplish this in one of two ways. It may either standardize the subset
 * of data it is tasked with generating manually, or may provision it within a
 * container that applies filtering on get operations to return the correct result.
 * A mix of both of these may be appropriate depending on the scenario.
 * However its resulting response container should only produce fully
 * usable data that requires no additional consideration by the internals
 * of Sanctity, and is properly escaped, filtered, sanitized, standardized,
 * prefixed, etc prior to the being returned, or set into a container that
 * performs these operations on the fly when the data is requested
 * (this is actually pretty simple to accomplish with Sanctity containers,
 * so the approach is left to the implementing Populator to determine.
 * Generally official populators will perform operations manually prior
 * to packaging, but 3rd party populators may do it either way, provided
 * the response is consistently safe for use in all cases).
 *
 * This interface provides the required contract for decoupling standardization
 * of expected output to an object that is capable of handling the intricacies
 * of an underlying platform, and understands the individual interrelation
 * required to draw forth the full set of finalized data, either directly
 * or indirectly, so that the value returned from a populator does not need
 * to be evaulated by a model or controller prior to direct use.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface PopulatorInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
