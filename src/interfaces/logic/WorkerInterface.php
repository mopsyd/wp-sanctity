<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Worker Interface
 *
 * This interface represents an object that is "owned" by a "director",
 * and is expected to only know and interact with the director for all
 * operations. Workers do not directly interact with other workers,
 * they always defer work back to the director to delegate, and remain
 * focused directly on their given task completely decoupled from all
 * other considerations. Either they can figure it out immediately within
 * their own scope, or the director is asked to resolve it, which is
 * generally delegated to another worker who is also oblivious of any
 * considerations outside of its own scope.
 *
 * Sanctity expects adapters that have a particularly complex set of
 * considerations to implement this director/worker association,
 * with the primary adapter being the director and remaining
 * concerned with mediation of commands and delegation to workers,
 * and the workers being concerned only with execution of their
 * own specialized logic. If the adapter logic requires two or more
 * classes to accomplish, then this pattern is expected to be implemented.
 * If a single class can handle the task, the adapter itself may reflect
 * both the director and the worker.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface WorkerInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
