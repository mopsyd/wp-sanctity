<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Resolver Interface
 *
 * This interface represents an object that is tasked with
 * resolving discrepancies, such as information that requires
 * a set of complex conditional checks to verify,
 * or some multi-part process of interaction with the
 * underlying technology being represented by an adapter
 * to determine a non-ambiguous answer.
 *
 * Sanctity expects objects bearing this interface to be an authority on
 * the scope of information they reign over, and be capable of producing
 * an absolutely accurate answer, key that is entirely non-ambiguous
 * when called. It is also expected to present its answer in Sanctitys
 * own internal structure of understanding, rather than the underlying platform.
 * For example a platform may use its own unique page types that
 * do not map 1:1 to how Sanctity references a page type, and a
 * resolver is expected to provide the Sanctity identifier for
 * the page type when asked, not the platform one.
 *
 * This interface provides the required contract for decoupling the underlying
 * executional logic from the specifics of the platform, by unifying internal
 * data types with the specifics of the platform, and translating the platform
 * specifics back to the expected key set.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface ResolverInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
