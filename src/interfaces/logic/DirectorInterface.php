<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Director Interface
 *
 * This interface represents an object that delegates tasks to dedicated
 * objects who have only one narrow scope of focus.
 *
 * The director presents an external api for use, and delegates commands
 * to the appropriate worker, freeing the external calling logic of any
 * direct association with how the task is accomplished.
 *
 * Sanctity expects adapters to implement this if they require two or more
 * classes to perform their target objective. It is not mandatory for a simple
 * adapter that can fully encapsulate an underlying technology to use this,
 * but as most adapters wrap technology of some measure of complexity,
 * it is strongly suggested to leverage this to prevent writing god objects,
 * and keep full separation of concerns intact.
 *
 * A director must be capable of doing all of the following:
 *
 * - Know how to instantiate and dependency inject its workers,
 *     or know a means of factorization to accomplish this.
 * - Know how to resolve an external command to a call to the appropriate worker,
 *     and return the result from the worker unmodified.
 * - Mediate between workers that require cross-association internally in the
 *     course of resolving an external task, so they remain decoupled from each other.
 *     This means that workers require that the director dependency inject a copy
 *     of itself into them to defer to when encountering any work outside of
 *     their immediate scope.
 * - Provide a means of accepting additional workers for extension purposes.
 * - Provide an api report of what functionality it is possible to resolve
 *     given the current pool of available workers.
 * - Validate whether a task intended for delegation will correctly
 *     resolve against the worker PRIOR to calling the action method
 *     of the worker that handles the task. It may call validation methods
 *     on the worker to establish this, but MUST NOT call an execution
 *     method prior to verifying the arguments are correct, even if the
 *     Worker itself performs its own type checking within the action method.
 *     No assumption will be made that workers will type check,
 *     even if they do.
 * - Operate only as a director, and otherwise use a "hands off"
 *     policy in enacting the actual underlying task.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface DirectorInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
