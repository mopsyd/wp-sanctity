<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\logic;

/**
 * Aggregator Interface
 *
 * This interface represents an object used to iterate over a large subset
 * of data and defer individual entries to parsers in a large, complex,
 * and potentially multidimensional composite of data.
 *
 * Sanctity only expects that objects that require repetitive parse operations
 * will be driven by an aggregator, which is tasked with calling the correct
 * corresponding parser, and organizing the return set as needed into a final
 * result set that reflects the correct structure for its purpose.
 *
 * In cases where a simple list of records is required,
 * his is relatively straightforward. In instances where a nested composite
 * needs to be created from a flat set (such as a representation of
 * a navigation menu that may contain any number of sublinks,
 * or a discussion tree representing nested comments and replies,
 * then it is not straightforward). Either way, the Aggregator is tasked
 * with the correct iteration and organization of parsing operations,
 * and is expected to return a collection object that internally is
 * organized in the correct schema, nested or otherwise.
 *
 * Aggregators DO NOT parse data or mutate it. They ALWAYS defer to a parser
 * for individual parse operations. The parser may defer back to an aggregator
 * to perform a listed set operation required to reflect its internal expected
 * structure, which may recurse as needed between these two object types until
 * the original task is completed. They are both expected to understand how to
 * avoid recursion bugs in this process, even when the operation occurs across
 * multiple methods or classes.
 *
 * This interface provides the required contract for streamlined handling
 * of complex data structures in either a tree or table format,
 * with a consistent object type deferred to in order to accomplish
 * their sorting and parsing logic in the correct order.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface AggregatorInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{

}
