<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\extension;

/**
 * Sanctity Extension Interface
 * All Sanctity Extensions have this interface.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface ExtensionInterface extends \mopsyd\sanctity\interfaces\SanctityInterface {

    /**
     * An extension must instantiate cleanly without any required parameters.
     */
    public function __construct( $dependencies = null, $arguments = array() );

    /**
     * Override this method to declare your namespace prefix.
     *
     * It should correspond to the Psr-4 namespace prefix of your package.
     * If your directory structure matches that of Sanctity, your
     * extension classes will load without issue.
     *
     * This method is static so that Sanctity can distribute your
     * namespace prefix throughout its internals without the overhead
     * of loading your class, which allows its own internal logic to
     * integrate your extension seamlessly.
     *
     * @return string
     */
    public static function getPackageNamespace();

    /**
     * This method must be overridden, and must correspond to the
     * source directory that your Psr-4 namespace resolves to.
     *
     * This will be used in conjunction with `getPackageNamespace`
     * to register your package with the autoloader. If this is not valid,
     * your extension will be disabled.
     *
     * You are not restricted to placing your extension in your root
     * source directory, but that is the easiest way to do it.
     *
     * All paths your extension declares will be considered to be relative
     * to the absolute disk location returned by this method, and many path
     * resolvers actively prevent reverse filepath navigation
     * for security reasons. Although it is possible to place your extension
     * class elsewhere, the most straightforward implementation is to just
     * put it in your plugin or theme root directory, or alternately to put
     * all of your assets managed by Sanctity internals in a child directory
     * of its location.
     *
     * @return string The absolute filepath root of your extension,
     *     and it's assets that are otherwise declared for Sanctity integration.
     *     All such assets should be in a child directory relative to the path
     *     returned by this method. You may symlink if need be to reach
     *     backward directories, but you may not present a path string that
     *     contains reverse navigation. Symlinks are acceptable, as the
     *     server administrator should have strict control over their
     *     creation on a well maintained server.
     */
    public static function getPath();

    /**
     * Registers your extension with Sanctity internals.
     * This is called automatically
     * when you submit it to `\Sanctity::extend`
     *
     * This method is used to package the active Sanctity front controller instance
     * locally into your extension after registration has successfully resolved,
     * so it may be used during your initialization logic via `getSite()`
     * to perform any required operations on the Sanctity runtime.
     *
     * In contrast to the general public facing api, this gives your extension
     * direct access to the current active instance, which is otherwise protected
     * from direct manipulation by the public api. This will afford you access
     * to it's active state methods, which are not otherwise available for
     * active usage after initialization, if the original instantiated instance
     * was not retained externally (by default, it isn't).
     *
     * The Sanctity active instance is not a Singleton, and you *can* instantiate
     * another copy, however this is going to incur quite a lot of memory overhead
     * as it duplicates the bootload process in full, and this should NOT be done
     * outside of a unit testing environment for performance reasons.
     *
     * It should also be noted that quite a lot of internal logic is cached statically
     * after first load, and this is not overridden when duplicate front controller
     * instances are generated. It can be cleared, but that is done internally at
     * specific logical points in operation where previously accrued data needs
     * to be invalidated because the underlying subset has been updated,
     * and should not be done manually unless you are extremely familiar
     * with the api of Sanctity internals.
     *
     * Registration is only called one time per runtime for any given extension.
     * Attempting to re-register an existing extension will not have any effect.
     *
     * @return void
     */
    public function register( \mopsyd\sanctity\interfaces\SanctityInterface $site );

    /**
     * Override this method to provide any initialization logic required
     * by your extension. This method will fire in the `init` hook,
     * but only if registration of your extension is successful.
     *
     * This method will be called by the Extension Model after the extension
     * is correctly validated and registered, and after the extension has
     * been packaged via dependency injection with prerequisite Sanctity assets.
     *
     * Do not call this method manually, as required assets will not be present
     * until registration occurs normally.
     */
    public function initialize();

    /**
     * Used by the Component Model to retrieve any declared components.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getComponents();

    /**
     * Used by the Component Model to retrieve any declared controllers.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getControllers();

    /**
     * Used by the Component Model to retrieve any declared models.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getModels();

    /**
     * Used by the Component Model to retrieve any declared views.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getViews();

    /**
     * Used by the Component Model to retrieve any declared libraries.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getLibs();

    /**
     * Used by the Component Model to retrieve any declared factories.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getFactories();

    /**
     * Used by the Component Model to retrieve any declared templates.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getTemplates();

    /**
     * Used by the Component Model to retrieve any declared actions.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getActions();

    /**
     * Used by the Component Model to retrieve any declared filters.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getFilters();

    /**
     * Used by the Component Model to retrieve any declared layouts.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getLayouts();

    /**
     * Used by the Component Model to retrieve any declared configurations.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getConfigs();
}
