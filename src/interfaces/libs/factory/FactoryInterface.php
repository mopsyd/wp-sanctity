<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\factory;

/**
 * Sanctity Factory Interface
 * All Sanctity Factories have this interface.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface FactoryInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{
    /**
     * Factory Public Load Method
     * Resolves a requested object into an instantiated instance.
     *
     * This method takes two parameters. The first is the name of the resource
     * requested, which corresponds to the leaf level suffix of the class name.
     * All registered namespaces will be checked, so override classes can exist
     * in the appropriate load order.
     *
     * the second parameter is any arguments that need to be passed in to properly
     * initialize the object. This includes dependency injection parameters,
     * settings, or what have you. This is a single array, which should be
     * multidimensional, with keys corresponding to the parameter or parameter
     * group that needs to be injected. How this is implemented is up to the
     * method declaring itself as the load method.
     *
     * @param string $resource
     * @param array $args (optional) An array of any arguments that need to be passed
     * into the loader. Whether or not this is required is going to depend
     * on the declared method doing the actual loading.
     *
     * @return \mopsyd\sanctity\interfaces\SanctityInterface
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This exception is raised if any error occurs during the load order.
     * @see declareLoaderMethod
     */
    public function load( $resource, $args = array() );
}
