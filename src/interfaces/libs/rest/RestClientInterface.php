<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\rest;

/**
 * Sanctity Rest Client Interface
 * All Sanctity classes that act as rest clients use this interface.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface RestClientInterface
extends RestInterface
{
    /**
     * Resets the curl instance.
     */
    public function reset();

    /**
     * Provides the details required to send a valid request.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request
     * @param array $arguments
     */
    public function package( \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request, array $arguments = array() );

    /**
     * Sends the request
     */
    public function send();

    /**
     * Gets the packaged response
     */
    public function getResponse();

    /**
     * Gets an endpoint resource.
     *
     * May or may not require authentication, depending on the circumstances.
     */
    public function get();

    /**
     * Posts data to an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use,
     * but is often used to establish authentication also.
     *
     * It is usually a safe bet that authentication needs to either be
     * provided with the request or obtained prior to the request.
     */
    public function post();

    /**
     * Places an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use.
     *
     * It is usually a safe bet that authentication needs
     * to be obtained prior to the request.
     */
    public function put();

    /**
     * Deletes an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use.
     *
     * It is usually a safe bet that authentication needs
     * to be obtained prior to the request.
     */
    public function delete();

    /**
     * Requests the options available for further requests.
     *
     * It's excellent when an endpoint implements this because their api is
     * automatically discoverable, but most of them don't leverage it in any
     * coherent manner or at all in most cases.
     *
     * Usually does not require authentication.
     * The response may be expanded if authentication is provided.
     */
    public function options();

    /**
     * Requests that an endpoint server mirror the request back to the client.
     * Typically regarded as unsafe, but provided for completion as it is part
     * of the http spec still currently. Provided internals do not use this,
     * but you may use it for your own definitions even if it is discouraged
     * to do so.
     *
     * It is usually the safest bet not to use this and to assume
     * it is disabled in the majority of cases.
     */
    public function trace();

    /**
     * Maintains an open channel request.
     *
     * Typically used only for proxying and command line utilites that
     * hold an open socket channel, such as for a chat app or continuous
     * reporting module. This is here for completion, you will probably not
     * use it for anything Sanctity related unless you are doing something
     * highly custom.
     *
     * It is usually a safe bet to assume this is not implemented unless
     * explicitly stated in an api doc that it is.
     */
    public function connect();

    /**
     * Pulls only the headers for a request.
     *
     * This is a very fast method for preflighting a request, and is performant
     * on the client side for checking if a request can resolve before committing
     * to pulling a large subset data payload and incurring the expense of parsing
     * a large volume of data. Typically not required on small payloads, but a
     * pragmatic option on potentially large payloads. Response time may still
     * take a while if the endpoint does not implement separation between head
     * and get requests well (they may still parse the whole logic internally
     * instead of just sending the headers, in which case the time savings is
     * negigible, but the payload savings is still substantial if it doesn't resolve).
     */
    public function head();

    /**
     * Applies a partial modification to a resource.
     *
     * It is usually safest to assume this is not implemented,
     * as it is a reasonably new addition to the http spec.
     * It incurs less cost than a post or put call where it is implemented,
     * and should be used in place of those when the option presents itself
     * and it is pragmatic to do so.
     */
    public function patch();

    /**
     * This is used for header parsing automation.
     *
     * It is passed to the curl handler, and is generally not useful for public
     * consumption unless you are trying to manually parse a raw header that is
     * split by line break for some reason. It facilitates the internals
     * retaining the header and body in one request to perform more efficiently
     * without data loss, and is only declared public so it is reachable
     * by cURL as a callback.
     *
     * @param type $curl
     * @param type $header_line
     */
    public function headerLine( $curl, $header_line );
}
