<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * <Component Container Interface>
 *
 * The component container is used to store a set of component objects,
 * which are an object representation of an isolated chunk of the page.
 *
 * Major content blocks can be represented by a component,
 * as well as other significant encapsulated pieces of the page,
 * such as the header, footer, sidebar, etc. Components may also
 * contain subcomponents, so a component container should be
 * considered to potentially be a composite object that will
 * potentially require additional iteration inward.
 *
 * A component object is meant to be an isolated segment of the page,
 * and as such it may require frontend scripts, stylesheets,
 * or other media to display correctly. A component is expected
 * to be able to produce all of this in a uniform order of operations
 * on demand, without regard to other data floating around.
 *
 * As this is a dependency container, the keys MUST be string values
 * for full compliance with the Psr-11 specification.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface ComponentContainerInterface
extends DependencyContainerInterface
{

}
