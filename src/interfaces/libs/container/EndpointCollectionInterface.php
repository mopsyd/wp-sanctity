<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * <Endpoint Collection Interface>
 *
 * The endpoint collection represents an index of rest endpoints
 * represented as endpoint containers.
 *
 * This collection should be used to represent a list or set of
 * endpoint collections or containers that either represent a single
 * set of api endpoints within a single api, or a collection of related
 * endpoints required to fulfill a specific task that requires multiple
 * outbound calls to various services.
 *
 * This container enforces the string key parameter of Psr-11,
 * although it is a data container, not a dependency container.
 * The reason for this enforcement is to make the selection of
 * an api by keyword straightforward and prevent the need for
 * iteration to find the correct outbound protocol.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface EndpointCollectionInterface
extends EndpointContainerInterface,
 CollectionInterface, ConfigurationCollectionInterface
{

}
