<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * <Option Collection Interface>
 *
 * The option collection interface designates a master set of
 * numerous option subsets. This collection object allows a number
 * of individual datasets to be stored in a single index, which
 * can be performantly flattened into a minified serializable set.
 * The child objects of it also collapse recursively when this occurs,
 * allowing for the container to present a minimal data footprint,
 * and expand back to its original state automatically when recalled,
 * and also trigger each subsequent child object to do so also.
 *
 * This presents the ability to store numerous sets of data in a single row
 * in the data layer, and then uncompress and expand them at runtime by only
 * retrieving the value of a single record from the data layer.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface OptionCollectionInterface
extends OptionContainerInterface,
 CollectionInterface
{

}
