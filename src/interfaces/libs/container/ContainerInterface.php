<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * Container Interface
 *
 * This is the base level interface for Sanctity containers.
 *
 * Usage of a container is checked against this interface,
 *
 * but various classes have their own implementation for various purposes.
 *
 * This container is a general purpose, flexible container that is usable
 * from both an array and an object standpoint.
 *
 * It is iterable, countable, array accessible, serializable, and json serializable,
 * to maximize the interoperability of its payload with the standard Wordpress api.
 *
 * This flexibility allows for usability with the maximum possible range of
 * programming styles with minimal bugs.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface ContainerInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface,
 \Iterator,
 \Serializable,
 \JsonSerializable,
 \Countable,
 \ArrayAccess,
 \Psr\Container\ContainerInterface
{

    /**
     * Makes containers compatible with standard factorization.
     *
     * @param int $size If not provided, should use the default value of -1
     * @param array $contents
     * @return type
     */
    public static function init( $size = null, array $contents = array() );

    /**
     * Used to cast a container to a different type.
     *
     * This method creates an instance of itself from the contents
     * of a provided container, so that container contents can be
     * easily transferred between container types without
     * additional effort by external logic.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container
     * @return type
     */
    public static function from( \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container );

    /**
     * Returns all container debug information if debug mode is enabled,
     * or false if it is not.
     *
     * @return array|false
     */
    public static function getDebug();
}
