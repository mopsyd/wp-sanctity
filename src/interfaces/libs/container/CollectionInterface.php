<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * Collection Interface
 *
 * This is the base level interface for Sanctity collections,
 * which represent a container of containers.
 *
 * Where individual containers are concerned with being scoped to a
 * specific realm of content, the collection is in turn concerned
 * with being scoped to a specific container type.
 *
 * This is used to insure that a given collection will return accurate results
 * when its child containers are iterated over for any given purpose.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface CollectionInterface
extends ContainerInterface
{

    /**
     * Returns a string representing an interface that is used
     * to validate values set in the collection,
     * which must extend \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     *
     * Values that correctly implement the given interface MUST
     * be accepted as valid values for the collection.
     *
     * Values that do not correctly implement the given interface MUST NOT
     * be accepted as valid values for the collection, and should raise
     * a valid instance of \Psr\Container\ContainerExceptionInterface
     * to block the setter attempt.
     *
     * Cases where the provided return value of this method do not resolve
     * to the correct interface or an interface extending it MUST raise a
     * `\mopsyd\sanctity\libs\exception\SanctityBrokenClassException`
     * in their constructor and block instantiation of the collection entirely.
     *
     * In the event that a `SanctityBrokenClassException` is raised,
     * it MUST NOT be caught and handled by implementing logic,
     * because this specific exception is intended to immediately impart
     * that a class is broken and needs to be fixed prior to release.
     * These exceptions should never occur in any stable production code
     * that is in release.
     *
     * @return string
     * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    public function getExpectedContainerType();
}
