<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * Twig Extension Container Interface
 *
 * This is for Twig Extension Containers.
 *
 * Valid implementations of this interface are expected to follow the Psr-11
 * specification explicitly.
 *
 * Valid implementations of this interface only accept objects bearing the
 * Twig_ExtensionInterface or a string representation of the class name of
 * a class that does, and only return correctly instantiated usable instances
 * of the extension.
 *
 * Individual implementations of this container should be able to resolve
 * a string representation into a correctly instantiated object.
 *
 * If an implementation is designed to handle custom Twig extensions that
 * require parameters in their constructor, it MUST either be capable of
 * resolving this internally, or utilize a factory that does so correctly,
 * so that it always returns a stored parameter as a valid usable object
 * in a non-blocking manner.
 *
 * Implementations MUST reject invalid use cases on `get` operations to avoid confusion.
 *
 * This is for a very narrow scope purpose,
 * used for automation of the process of adding extensions.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface TwigExtensionContainerInterface
extends DependencyContainerInterface
{

    /**
     * The TwigExtensionContainerInterface instance MUST only accept a parameter
     * that is either an object bearing this interface,
     * or a string representation of a class bearing this interface.
     *
     * Get functions should only accept these two use cases.
     *
     * Set functions MUST return a valid instantiated object
     * representation of this interface.
     *
     * This class constant should be referenced in the setter operations
     * to validate that the given value is a correct parameter,
     * using either `instanceof`, `class_implements`, or both.
     *
     * Invalid values should ALWAYS be rejected
     * with a container exception.
     */
    const EXPECTED_INTERFACE = 'Twig_ExtensionInterface';

    /**
     * Valid implementations MUST enforce a string key identifier,
     * for Psr-11 compliance, as this is a dependency container.
     *
     * This can be done in setter operations using either
     * `gettype($key) === $this::EXPECTED_KEY_TYPE`,
     * or alternately the simplified `is_string($key)`
     *
     * Non-string key identifiers should ALWAYS be rejected
     * with a container exception.
     */
    const EXPECTED_KEY_TYPE = 'string';

}
