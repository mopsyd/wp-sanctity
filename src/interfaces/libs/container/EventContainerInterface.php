<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * <Event Container Interface>
 *
 * The event container is used to store a set of events that need to fire
 * at ambiguous times that cannot be readily determined by means of a strict
 * linear order of execution. The container is meant as a queue of callables
 * or executable objects that can be referenced directly by a key registered
 * with a task manager class, so that it can fire the set of actions required
 * when the trigger for the event occurs.
 *
 * An event container represents all of the executables bound to a single event.
 *
 * Multiple events should be represented by an instance of the EventCollectionInterface,
 * which has a key equal to the event slug, and a value equal to an instance of this
 * interface which designates the queue for the individual event.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface EventContainerInterface
extends ContainerInterface
{

}
