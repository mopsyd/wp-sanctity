<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\container;

/**
 * <Endpoint Container Interface>
 *
 * The endpoint container houses a standardized set of data describing
 * a rest endpoint.
 *
 * It is designed to work in conjunction with a Psr-7 message interface system,
 * and be utilized as a payload container that can insure that expected keys
 * for an HTTP message are provided.
 *
 * These containers are used internally to enforce a standardized message body
 * data set. Eventually the external api will leverage these to present their
 * data through a standard Psr-7 message implementation, although this
 * functionality is not currently in place. You may also utilize these
 * containers to work with rest endpoints in the event that you do not
 * want to use Psr-7 for one reason or another, and will find that they
 * provide a 1:1 conversion between a simple iterable data set and a full
 * on message body implementation.
 *
 * This container enforces the string key parameter of Psr-11,
 * although it is a data container, not a dependency container.
 * The reason for this enforcement is to make the selection of
 * an api by keyword straightforward and prevent the need for
 * iteration to find the correct outbound protocol.
 *
 * @note Psr-7 is not yet implemented into Sanctity as of this writing,
 *     but it is a planned feature for future release. For this reason,
 *     the containerization of rest endpoints is done in such a way where
 *     it will be fully compatible with a Psr-7 implementation without
 *     much additional work.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface EndpointContainerInterface
extends ContainerInterface, ConfigurationContainerInterface
{

}
