<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Sanitizer Interface
 *
 * Designates that a class is intended to filter, escape, translate,
 * and otherwise modify provided content for final display for the Wordpress Adapter.
 *
 * Sanitizers sole purpose is to receive a set of raw unfiltered data of their
 * specified type, and convert it fully into a set of data that is safe, readable,
 * and properly formatted for final display. All raw data aggregated by a
 * populator or parser is expected to be filtered through a parallel sanitizer
 * object dedicated to formatting that same data for output.
 *
 * Sanitizers do not collect data or make decisions about what data is related.
 * Their only purpose is to filter existing data exactly as presented into a
 * final result, and strip any data from the presented set that is not relevant
 * to the current user. They have the capacity to check existing user permissions
 * for the sole sake of removing content that is outside their
 * visibility authorization, but otherwise may only format existing data into a
 * consumable final form. They are expected to know all of the escaping,
 * filtering, sanitizing, and validating methods required of them to fulfill
 * this role, alleviating any other part of the package from the need to
 * conditionally filter output.
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface RegisterInterface
extends WordpressWorkerInterface,
 \mopsyd\sanctity\interfaces\logic\RegistrationInterface
{

    /**
     * Applies a registration action to the supplied subject appropriate
     * to the scope of the worker object.
     *
     * This method is the action method for register type workers.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $subject
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\WordpressContainerInterface
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject );

    /**
     * Clears registration of the subject matter, if it is possible to do so.
     *
     * @return bool Returns true if unregistration was successful,
     *     and false if not successful. If it is not possible to unregister
     *     a specific target, this should just return false and otherwise be
     *     non-blocking.
     */
    public function unregister( $subject );

    /**
     * This method clears any internally set data, and sets the register object
     * back to a clean internal state, if any data was retained internally.
     *
     * If no data is retained internally, it is expected to just return itself
     * and perform no other operation.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset();
}
