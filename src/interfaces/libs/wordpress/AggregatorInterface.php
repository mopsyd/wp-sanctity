<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Aggregator Interface
 *
 * Designates that a class is intended to be aggregate multiple parse operations
 * into a single output collection for the Wordpress Adapter.
 *
 * Aggregators Receive input from the adapter from a parser output,
 * and package it into a collection. They present an orderly structured set
 * of multiple related constructs into a dedicated container for those
 * constructs that can be easily iterated over to dynamically index
 * individual container instances within it.
 *
 * Aggregators do not parse, modify, evaluate, or conditionally check data.
 * They are simply a packaging utility for bulk operations against a related
 * parser.
 *
 * An aggregator is expected to be ignorant of any object relations other than
 * its association with the Adapter. It is aware only of the adapter,
 * and that the adapter has a `parse` method that it can gain accurate results
 * from by calling its same identifying key as the key, and passing the
 * provided valid subject as the parameter. The adapter works as a mediator
 * between the parser and the aggregator so neither of them is directly
 * aware of the other.
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface AggregatorInterface
extends WordpressWorkerInterface,
\mopsyd\sanctity\interfaces\logic\AggregatorInterface
{

    /**
     * Receives a subject and parses it into a collection of usable raw results.
     *
     * This method is the action method for aggregator type workers.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $subject
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\WordpressContainerInterface
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     */
    public function aggregate( $subject );

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset();
}
