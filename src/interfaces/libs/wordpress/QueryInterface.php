<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Query Interface
 *
 * Designates that a class is intended to wrap Wordpress query objects
 * for the Wordpress Adapter.
 *
 * Query objects provide a layer of abstraction for typical
 * WP_Query/WP_*_Query type objects,allowing them to be utilized
 * in a streamlined manner through the Wordpress Adapter.
 * They will produce a query from an array, id, or other
 * standard PHP variable type or primitive value, which can then
 * be used to fire additional workers that require explicit
 * object types as their subject.
 *
 * The api for this is also presented publicly through the adapter,
 * where all of the query types can be simply accessed with the
 * public method `$adapter->query( $type, $args )`.
 *
 * Query objects distributed as part of this package are straightforward
 * abstraction of the underlying Wordpress logic.
 * They will accept any typical Wordpress query arguments that are valid
 * for their type, and also provide a degree of standardization that is
 * not present in the vanilla Wordpress distribution.
 *
 * Unlike other worker objects for the Wordpress Adapter,
 * Query workers never call other workers.
 * They are only present to provide the correct parameter
 * set for firing other workers, so that other workers can
 * leverage them to interoperate more seamlessly.
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface QueryInterface
extends WordpressWorkerInterface,
 \mopsyd\sanctity\interfaces\logic\QueryInterface
{

    /**
     * Receives a subject and parses it into a Query object corresponding to its type.
     *
     * This method is the action method for query type workers.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * Query workers should accept any valid parameter that would resolve
     * to a valid query for the query type they represent,
     * and may optionally designate their own parameters that are non-standard,
     * but resolve to a valid query result, provided there is no ambiguity
     * about what is being queried.
     *
     * @param mixed $subject
     *
     * @return \WP_Query|\WP_Comment_Query|\WP_Term_Query|\WP_User_Query
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     */
    public function query( $subject );

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset();
}
