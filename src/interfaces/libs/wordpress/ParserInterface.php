<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Parser Interface
 *
 * Designates that a class is intended to be generator of a completed
 * set of raw data representing a single element instance for the Wordpress Adapter.
 *
 * Parsers parse single data sets from a native Wordpress object into an isolated
 * container of data that is decoupled from its original source.
 * Their purpose is to extract interesting bits of data from an object that
 * contains methods that can alter data persistence,
 * and present them within a construct that has no further capacity to modify
 * its origin, leaving the data free to be used or mutated throughout any other
 * logic without any lasting consequences.
 *
 * Parsers do not parse complex sets of data across multiple sources,
 * nor do they parse multiple sources of the same type.
 * The output from a parser should reflect a full set of 1:1 data for
 * a single given instance, and yeild a return result packaged in
 * a dedicated container instance that reflects the source data of
 * the original object without any modification.
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface ParserInterface
extends WordpressWorkerInterface,
 \mopsyd\sanctity\interfaces\logic\ParserInterface
{

    /**
     * Receives a subject and parses it into a container of usable raw results.
     *
     * This method is the action method for parser type workers.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $raw
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\WordpressContainerInterface
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     */
    public function parse( $subject );

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset();
}
