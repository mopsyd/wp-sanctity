<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Evaluator Interface
 *
 * Designates that a class is intended to make concrete determinations
 * about scope for the Wordpress Adapter.
 *
 * Evaluators parse a query object and return explicit single scoped responses
 * used in control flow logic. Their purpose is to assist in routing,
 * conditional statements, and other control structures in determining
 * an accurate execution plan for the page, and how other worker elements
 * within the Wordpress Adapter's dependent objects should interoperate.
 *
 * Evaluators do not provide data for the render step.
 * Information gleaned from evaluators may be used as part of the id structure
 * of html in the output document, or as an identifying key of a data subset,
 * but it does not render for visible display in the final output.
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface EvaluatorInterface
extends WordpressWorkerInterface,
\mopsyd\sanctity\interfaces\logic\ResolverInterface
{
    /**
     * Receives a subject and produces a determination about its exact meaning.
     *
     * This method is the action method for evaluator type workers.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $subject
     * @return mixed
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     */
    public function evaluate( $subject );

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset();
}
