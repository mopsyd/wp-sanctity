<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Sanctity Wordpress Adapter Interface
 *
 * Provides an object oriented wrapper for the Wordpress API,
 * and handles all operations against the Wordpress API using
 * its standard functionality. This process is decoupled from
 * the rest of the Sanctity logic, providing the rest of the
 * system a means to operate explicitly in its own scope without
 * regard to how the underlying platform api relates to it.
 *
 * This Adapter is referenced by most other classes that require interaction
 * with Wordpress functionality for any purpose, and mediates that interaction
 * without exposing the internals to classes depending on it.
 *
 * This allows the system to maintain a consistent api everywhere else
 * regardless of what happens in terms of the Wordpress Api.
 *
 * --------
 *
 * The Wordpress adapter is a multipurpose bridge to the Wordpress api.
 *
 * Outwardly, it presents a uniform means of accomplishing calls to the
 * Wordpress api without the need for an object operating against it to
 * have any knowledge of how Wordpress works.
 *
 * Inwardly, it acts as a Director and Mediator for dedicated worker objects
 * that are tasked with a specific scope of operation, also providing
 * separation between them so they can interoperate without direct
 * knowledge of one another in an organized manner.
 *
 * The worker objects all understand a specific scope of Wordpress internals,
 * and operate against those in a uniform way to insure that data and commands
 * within their realm of responsibility is accomplished using Wordpress
 * best practices and available methods.
 *
 * The worker objects will in turn decouple their responses from any
 * Wordpress internals, so that Wordpress objects are not passed back
 * to any other part of the system. All response values from workers
 * are either a container or a standard php variable type.
 *
 * The objective of this approach is to provide data security,
 * by preventing any other part of the system from direct access
 * to an object that can mutate the global scope or alter the
 * data layer in unexpected ways. The secondary objective of
 * this approach is to insure that any changes to the Wordpress API
 * over time are segregated from the underlying logic of Sanctity,
 * making it possible to rapidly produce updates, factor out deprecation,
 * and add additional Wordpress functionality without any refactoring
 * within the Sanctity core engine. Lastly, the Adapter can prevent Sanctity
 * from operating out of the scope of a theme by not presenting any api
 * for alterations that are not appropriate to a theme, thereby preventing
 * the rest of Sanctity from operating out of scope.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface WordpressAdapterInterface
extends \mopsyd\sanctity\interfaces\adapters\platform\PlatformAdapterInterface,
 \mopsyd\sanctity\interfaces\logic\DirectorInterface
{

    /**
     * Performs parse operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a parsed set of raw unfiltered data by leveraging
     * a parser worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to a parser identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known parsers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested parse operation.
     */
    public function parse( $command, $subject );

    /**
     * Performs population operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a populated set of raw unfiltered data by leveraging
     * a populator worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to an aggregator identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known aggregators
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested aggregate operation.
     */
    public function aggregate( $command, $subject );

    /**
     * Performs population operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a populated set of raw unfiltered data by leveraging
     * a populator worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to a populator identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known populators
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested poulate operation.
     */
    public function populate( $command, $subject );

    /**
     * Performs an evaluation operation upon the subject by leveraging
     * a dedicated evaluation worker.
     *
     * @param string $command A command corresponding to a evaluate identifying key known to the Adapter
     * @param \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface $subject
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known sanitizers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested sanitization operation.
     */
    public function evaluate( $command, $subject );

    /**
     * Performs sanitization upon a previously populated set of raw data,
     * and produces a render-ready result collection or container.
     *
     * @param string $command A command corresponding to a sanitizer identifying key known to the Adapter
     * @param \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface $subject
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known sanitizers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested sanitization operation.
     */
    public function sanitize( $command, $subject );

    /**
     * Performs sanitization upon a previously populated set of raw data,
     * and produces a render-ready result collection or container.
     *
     * @param string $command A command corresponding to a query identifying key known to the Adapter
     *     [query, comment, term, user]
     * @param mixed $subject Query args relevant to the specified query type
     *
     * @return \WP_Query|\WP_Comment_Query|\WP_Term_Query|\WP_User_Query etc.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known query handlers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested query operation.
     */
    public function query( $command, $subject );
}
