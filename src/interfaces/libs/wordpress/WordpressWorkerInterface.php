<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\libs\wordpress;

/**
 * Wordpress Worker Interface
 *
 * Designates that a class is intended to be a
 * component used by the WordpressAdapter
 *
 * This represents part of an overarching Builder pattern. Kinda.
 *
 * Not everything that uses this builds stuff though.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface WordpressWorkerInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface,
 \mopsyd\sanctity\interfaces\logic\WorkerInterface
{

    /**
     * Returns the class or interface name of the subject
     * that the object is meant to handle.
     *
     * In some cases, such as containers, the type of a php variable
     * is an acceptable value for this:
     *
     * [string, array, boolean, resource, null, object, integer,
     * double, or "mixed" if the content accepted is ambiguous,
     * as is generally the case with containers].
     *
     * The Adapter is tasked with using this method to preflight
     * whether or not the provided arguments supply an instance of
     * this class or interface designation when directed to execute
     * logic against the worker object, and rejecting invalid requests
     * without passing them to the worker.
     *
     * @return string A class or interface name corresponding to
     *     the expected parameters of the action method(s) of the object.
     */
    public function getSubjectType();

    /**
     * This method returns a slug identifier which designates
     * to the Adapter what realm of responsibility the
     * worker object exists within. Related worker objects
     * tasked with handling the same type of work should all
     * return the same key from this method.
     *
     * These keys should typically correspond to a related
     * Wordpress construct, but this method does not exclude
     * identifying keys that are not Wordpress constructs if
     * they are useful for the underlying purpose of creating
     * a final result, or resolving ambiguity that the
     * Wordpress Api does not present a clear answer for.
     *
     * @return string A short slug in lower case, preferably a single word,
     *     but canonicalized with hyphens if multiple words are required.
     */
    public function getSubjectKey();
}
