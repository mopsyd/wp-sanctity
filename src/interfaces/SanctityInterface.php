<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces;

/**
 * <Sanctity Interface>
 *
 * Defines the default universal identity constants for the Sanctity framework.
 *
 * All Sanctity classes have this interface.
 *
 * All other Sanctity interfaces extend this interface.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface SanctityInterface {

    //--------------------------------------------------------------------------
    //                              Fixed Constants
    //
    // The following constants are fixed, and cannot be overridden by any class
    // within Sanctity or extending any Sanctity class. They are either hard-coded,
    // or are determined during the bootstrap process and then compiled into the
    // base interface to insure that all classes use a consistent value throughout
    // the entire runtime execution.
    //
    // This is a two-fold benefit. First, every class implementing this interface
    // or any extension of it has access to identical values in their own scope,
    // and has certainty that those values have not been mutated
    // by any source whatsoever, and do not need to be checked, sanitized, escaped,
    // or further treated as unreliable.
    //
    // Second, the data defined here FORCES the program to be run in the correct
    // order, or the class implementing it will kill the page.
    //
    // This is similar to the often encountered `if (!defined("ABSPATH") die;`
    // statement, except it only needs to be done in one spot, and everything
    // inheriting it automatically applies it.
    //
    // There is no need to check `ABSPATH` in any file that declares a class
    // that implements this interface, because it will automatically kill the page
    // immediately when the file is loaded if it is not defined due to a compile error.
    // This makes every single class file in this theme secure without any
    // functional checks at the beginning of the file, which also allows the theme
    // to be fully compliant with the object-oriented best practice of
    // never including declarations and execution in the same file.
    //
    //--------------------------------------------------------------------------

    /**
     * This should always correspond to the same value of the theme name in "styles.css"
     *
     * This can be obtained from any sanctity class using `$class::PACKAGE_NAME`
     */
    const PACKAGE_NAME = 'Sanctity';

    /**
     * This is the default prefix, if the theme is not currently child themed.
     * This is prefixed to everything that gets declared by this system prior
     * to registration with the Wordpress system.
     *
     * If a child theme is active, it may internally declare a prefix,
     * and if it does not a canonicalized version of its theme name
     * with all non-alphanumeric characters stripped out will be used
     * by default for all options that are extensible by child themes
     * (which is almost all of them).
     *
     * This value will be used for any purpose in which the logic MUST
     * be decided based on the parent theme, such as default fallback configurations,
     * the layout for the settings page, etc, and any properties to supply if
     * they are omitted by a child theme to make the experience more lenient
     * to program around as a 3rd party. For example, Sanctity provides a means
     * of a child theme loading the configurations from the parent theme,
     * which is powered by this constant, which never changes, and corresponds
     * to the parent theme prefix, so a database lookup is a simple endeavor
     * without having to loop through a long chain of theme inheritance
     * if it's a child-of-a-child-of-a-child.
     *
     * This is handled internally by controllers and/or models as needed.
     *
     * This can be obtained from any sanctity class using `$class::PACKAGE_SLUG`
     */
    const PACKAGE_SLUG = 'sanctity';

    /**
     * This is the release version of Sanctity.
     *
     * This should always correspond to the value in "styles.css".
     *
     * This can be obtained from any sanctity class using `$class::PACKAGE_VERSION`
     */
    const PACKAGE_VERSION = '0.2.0';

    /**
     * This constant compiles into the root interface of Sanctity to insure
     * that it is properly bootstrapped from its intended source in `functions.php`,
     * and attemting to skirt this or sidestep it causes the page to crash.
     *
     * This theme does not tolerate abusive programming practices or hack nonsense.
     *
     * Use it right or don't use it.
     *
     * The right way is to let Wordpress load it normally,
     * exactly the way Wordpress intends for it to be loaded.
     *
     * This can be obtained from any Sanctity class using `$class::PACKAGE_ROOT`
     *
     * This will be useful to theme and/or plugin authors who want to utilize
     * the extension api provided with this theme, to quickly identify the origin
     * of a potentially nested chain of child themes.
     */
    const PACKAGE_ROOT = SANCTITY_BASEDIR;

    /**
     * This constant compiles into the root interface of Sanctity to insure
     * that it is properly bootstrapped a valid Wordpress boot,
     * and attemting to skirt this or sidestep it causes the page to crash.
     *
     * Instead of prefixing every single file with
     * `if (!defined('ABSPATH') {die();}`
     * like almost everyone does,
     *
     * This approach basically states that the root interface that all
     * of the theme files for this theme require because they all extend
     * from it has to have ABSPATH defined or it won't compile,
     * and has to have SANCTITY_BASEDIR defined as well
     *
     * (which occurs when Wordpress bootstraps the theme in the expected manner)
     * or it will still not compile.
     *
     * And if this interface won't compile,
     *
     * neither will
     *
     * Any. Other. Class. In. This. Theme.
     *
     * This theme does not tolerate abusive programming practices or hack nonsense.
     *
     * Use it right or don't use it.
     *
     * This can be obtained from any Sanctity class using `$class::SITE_ROOT`
     */
    const SITE_ROOT = ABSPATH;

    /**
     * The current HTTP hostname.
     *
     * This compiles into every single Sanctity class via this interface.
     */
    const HTTP_HOST = SANCTITY_HTTP_HOST;

    /**
     * The exact, unmodified full page uri for the current request,
     * without any filtering or other modification applied.
     *
     * This compiles into every single Sanctity class via this interface.
     */
    const REQUEST_URI = SANCTITY_URI_LITERAL;

    /**
     * Whether or not the current request is a standard Ajax call.
     * This can be checked from any object context without
     * additional function calls.
     *
     * The purpose of this is to allow extending classes to easily determine if
     * they should handle an inbound request as AJAX or as a standard page request,
     * and also assists the router in determining the correct controller to load.
     * This may be checked in any Sanctity class scope by just checking
     * `self::AJAX_REQUEST`, which will be `true` if the request is ajax,
     * and `false` if it is not.
     *
     * This value is snapshotted immediately upon instantiation and cast into
     * a fixed interface constant so it remains immutable, unlike the $_SERVER
     * superglobal.
     */
    const AJAX_REQUEST = SANCTITY_AJAX;

    /**
     * The HTTP request method reported by the PHP server.
     * This can be used for routing easily based on request method,
     * and quickly determining if the page is
     *
     * get, post, put, delete, options, head, trace, etc
     *
     * without additional function calls.
     *
     * This is cast to lower case when declared, and compiles into
     * every single Sanctity class via this interface.
     *
     * The purpose of this is to allow extending classes to easily determine if
     * they should handle a post request by just checking `self::REQUEST_METHOD`.
     *
     * Unlike the PHP global `$_SERVER`, this cannot be changed once
     * it has been set, so it is consistently reliable unless someone
     * broke `$_SERVER` before the theme loaded at all.
     */
    const REQUEST_METHOD = SANCTITY_REQUEST_METHOD;

    /**
     * This is a unique sha1 hash that occurs on every page load generated
     * by the current microtime.
     *
     * This can be used to group page errors across unrelated parts of the stack,
     * so errors that occurred in the same page can be quickly grouped into
     * an isolated list in the error log if logging is enabled.
     *
     * The value of this constant will be a sha1 hash of the microtime
     * that the theme instantiated. This value remains fixed for the entire page
     * execution, and can be used in debugging and error logging to insure that
     * it is easy to isolate the error(s) or entries from a single page load even
     * if many people are viewing the page at the same time, because all of the
     * ones from the same page load will bear the exact same fingerprint,
     * and no other instance will, unless it loaded the `setup.php` file at the
     * exact same nanosecond.
     *
     * Logging a combination of `REQUEST_URI`, REQUEST_METHOD`,
     * and `RUNTIME_FINGERPRINT` in your log gives you a likelihood of
     * collision out to about a fraction of one in whatever a million
     * with a zillion more zeros is. In this case, even if the fingerprint
     * were identical (it won't be) they would have to also have loaded
     * the exact same page with the exact same request method also.
     *
     * This compiles into every single Sanctity class via this interface.
     */
    const RUNTIME_FINGERPRINT = SANCTITY_RUNTIME_INSTANCE_HASH;
}
