<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\models;

/**
 * Sanctity Settings Model Interface
 * All Sanctity Settings Models have this interface.
 *
 * The following expectations are required to be satisfied
 * for this interface implementation to be valid.
 *
 * A settings model MUST:
 * - Be able to correctly identify what is and is not a valid request in its scope.
 * - Be able to correctly enter and retrieve data without error in its scope.
 * - Be able to return a detailed report about what is incorrect about data presented in its scope.
 * - Be able to mitigate any data failures in its scope that result from bad data being supplied.
 *
 * A settings model MUST NOT:
 * - Operate directly outside of its scope (it MAY load other models outside of its scope to perform these tasks, if they are required for some purpose that is within its scope)
 * - Be tightly coupled with unrelated classes (All objects must be checked either by interface or by abstraction, like through a factory or similar construct)
 * - Directly interact with any view or template (only controllers and views may operate on templates)
 * - Apply any opinion about why it was ordered to perform a task (only controllers may apply opinion)
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface SanctitySettingsModelInterface extends SanctityModelInterface
{

    /**
     * Validates whether a submitted form is properly formatted,
     * and contains acceptable values.
     *
     * This method MUST NOT enact any changes to data structure.
     * Its only purpose is to throw an exception if the request
     * is not valid, prompting the controller to enact a mitigation
     * policy.
     *
     * @param array $request
     * @return void
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This MUST be an instance of \InvalidArgumentException which
     *     correctly implements the Sanctity exception interface.
     *     This exception MUST NOT be thrown for any reason if the
     *     request is valid.
     */
    public function validateRequest( $request );

    /**
     * Provides contextual details about what is incorrect about a request.
     *
     * This method MUST be non-blocking. No exceptions should be thrown
     * from this method for any reason.
     *
     * If the method does not detect any errors in the request,
     * it should return false.
     *
     * This method MUST return an associative array of messages designating
     * what is incorrect about the request. The array should have keys
     * corresponding to the request keys, and values that are arrays,
     * designating the following details about the request:
     *
     * - string 'message' (mandatory) A contextual message about what is incorrect.
     *
     * - array 'errors' (mandatory if known errors would occur) An array of any
     *     errors that would occur during processing
     *
     * - string 'level' (optional) A Psr3 compliant log level, if an error
     *     would occur, indicating how the data provided should be regardedd
     *     as a risk to the system.
     *
     * - string 'log-message' (mandatory if level is provided) A message that
     *     should be entered into the log. This can be the same as the
     *     public message, or may provide additional context, depending
     *     on the nature of the error
     *
     * The method MAY return an object representing the error details.
     *     If it returns an object, the object MUST implement \Traversable
     *     and \ArrayAccess, so it may be treated natively as an array,
     *     because Wordpress expects everyone to code with primitives,
     *     but they also tend not to typecheck them before throwing them
     *     at their internals, so iterable objects that work like arrays
     *     tend not to break things.
     *
     * @param array $request
     * @return bool|array|\Traversable
     */
    public function getInvalidFields( $request );

    /**
     * Handles a provided request, and enacts any business logic required by it.
     *
     * This method MUST NOT call validate request. It is the responsibility
     * of the controller to validate the request prior to handling it.
     * Models do not apply opinion, they only do what they are told.
     *
     * @param array $request
     * @return bool|array|\Traversable
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \InvalidArgumentException
     *     implementing the aforementioned interface if provided request
     *     data is not correct.
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \RuntimeException
     *     implementing the aforementioned interface if class logic
     *     is not correct.
     * @throws \mopsyd\sanctity\interfaces\SanctityExceptionInterface
     *     This method MUST throw an instance of \LogicException
     *     implementing the aforementioned interface if either
     *     Wordpress or the database derp out, but the handling
     *     logic of the class operating on them is sound.
     */
    public function handleRequest( $request );

    /**
     * This method should correctly handle any failures that occur on any
     * instance that either validateRequest or handleRequest throw an exception,
     * including but not limited to:
     *
     * - Restoring any partially saved data to its original state.
     * - Disabling the theme entirely if recovery is not possible.
     * - Kicking out any malicious users attempting to abuse a form
     *     by invalidating their session.
     * - Resetting any malformed settings back to their last known stable state.
     * - Notifying the admin that an error occurred.
     * - Logging the activity if a logger is available,
     *     including whether or not corrective measures
     *     were successful.
     *
     * Barring any unforseen complications, calling this method when a request
     * fails should FULLY correct any issues that occurred with the request.
     * Tasks required to perform this logic can be deferred to other objects
     * if they are known to be available and are better suited to specific steps.
     *
     * @param array $request
     * @return void
     */
    public function mitigateFailure( $request );
}
