<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\adapters;

/**
 * Sanctity Adapter Interface
 * All Sanctity Adapters have this interface.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
interface SanctityAdapterInterface
extends \mopsyd\sanctity\interfaces\SanctityInterface
{
    /**
     * Sanctity Adapter Constructor
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $dependencies (optional)
     *     The WordpressAdapter may receive an instance of the primary
     *     factory in its constructor if one has not been set statically.
     *
     * @param array $args Any arguments to supply to the adapter upon instantiation.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If no factory is available by the end of instantiation,
     *     indicating that one was not properly injected either
     *     statically or through the constructor.
     */
    public function __construct( $dependencies = null, $args = array() );

    /**
     * Sets the scope, which is determined by the controller, model, or view
     * interacting with the adapter.
     *
     * Adapters may not declare their own scope.
     *
     * This insures that adapters that are supposed to reflect admin or
     * login pages are not operating outside of their declared current area of effect.
     * Adapters are expected to explicitly adhere to scope at all times.
     *
     * Once scope has been set, the adapter is considered "scope locked",
     * which means it may not operate outside of that scope for any other call
     * during the current runtime. This prevents frontend and backend logic from
     * being mixed in inappropriate ways and presenting security risks.
     *
     * If they must be mixed, it has to be proxied through a dependent worker class
     * that is explicitly declared to handle the ambiguity,
     * and does not implement this interface.
     *
     * @param type $scope
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the scope is already defined, and the provided scope
     *     is not exactly identical.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the scope does not exist in the set of whitelisted scopes.
     *
     * @note The scope is set automatically by the controller, model, or view
     *     (depending on the specific adapter subtype), which defines the
     *     scope based on its own internal constant value.
     *     This method should not be called as part of the public api,
     *     as it is meant to operate during the initialization step of classes
     *     working with the Adapter, and calling it locks in a state for the
     *     rest of runtime that may prevent correct operation of the program
     *     if set inappropriately.
     *
     * @note Unit testing this paradigm through PHPUnit works fine,
     *     as PHPUnit caches static properties internally and opens each
     *     subsequent test as a completely fresh object. I can't really speak
     *     for other testing frameworks, but at least with PHPUnit this does not
     *     present a unit test issue.
     */
    public static function setScope( $scope );

    /**
     * Returns the scope the Adapter is cast to,
     * or false if it has not been declared.
     *
     * @return bool|string
     */
    public static function getScope();

    /**
     * Sets the factory factory, and passes it to child adapter abstract classes.
     *
     * The adapter is expected to pass on distribution of this object to any of
     * its own dependent worker classes that require factorization when this occurs.
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $factory
     */
    public static function setFactory( \mopsyd\sanctity\factory\FactoryFactory $factory );

    /**
     * Returns a work chain object scoped to the current adapter.
     *
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function chain();
}
