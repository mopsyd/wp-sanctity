<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\interfaces\adapters\platform;

/**
 * Platform Adapter Interface
 *
 * Standardizes how the rest of Sanctity handles operation with
 * the underlying platform it is running on.
 *
 * It is the design of this architecture to fully separate platform concerns
 * from internal logic concerns, so that either can be updated without
 * regard to the other.
 *
 * This also maximizes the likelihood of cross-platform integration.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface PlatformAdapterInterface
extends \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface
{
    /**
     * Sets the view object so that the platform adapter can output through
     * the provided render layer in whatever normal context the platform demands.
     *
     * This step occurs when the view is first instantiated. It will set a copy
     * of itself into the platform immediately after completing its initialization.
     *
     * @param \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view
     */
    public static function setView( \mopsyd\sanctity\interfaces\views\SanctityViewInterface $view );

}
