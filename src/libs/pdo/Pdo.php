<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\pdo;

/**
 * Database Connector
 * This class enforces that clean SQL it is only instantiated from a set
 * of whitelisted interfaces or else it says nope, not happening.
 *
 * This is a super lightweight, barebones PDO wrapper preconfigured to autoconnect
 * to a valid Wordpress install, and provides a couple of low level methods for
 * abstracting prepared statements, but that's about it.
 *
 * This class is loaded automatically by the abstract model,
 * and otherwise does not allow itself to be instantiated elsewhere.
 * This is one of the only points where Sanctity deviates from the
 * standard Factory loading pattern, mostly for security and performance's sake,
 * as having a million open database connections is absolutely terrible
 * for performance. Classes should be configured to receive a copy of the
 * single instance from the model through standard dependency injection
 * if they need it for something. The model class(es) have the final say
 * on who gets a copy of this.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class Pdo
    extends \PDO
    implements \mopsyd\sanctity\interfaces\libs\pdo\PdoWrapperInterface
{

    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

    /**
     * The standard wordpress database object
     * @var \wpdb
     */
    private static $wpdb;

    public function __construct( array $options = array() )
    {
        parent::__construct( $this->_constructDsn(),
            $this->_constructUsername(), $this->_constructPassword(),
            $this->_constructOptions( $options ) );
        $this->_bindWordpressDatabase();
    }

    public static function init( $dependencies = null, $args = array() )
    {
        $class = get_called_class();
        return new $class( $args );
    }

    /**
     * Abstracts prepared statements into one method.
     *
     * @param string $sql the query
     * @param array $args the bindings for the query
     * @return \PDOStatement the query response
     */
    public function run( $sql, array $args = null )
    {
        if ( !$args )
        {
            return $this->query( $sql );
        }
        $stmt = $this->prepare( $sql );
        $stmt->execute( $args );
        return $stmt;
    }

    /**
     * Returns the wordpress database object
     * @return \wpdb
     */
    public function wpdb()
    {
        return self::$wpdb;
    }

    /**
     * Gets the name of the current database being used
     * @return string
     */
    public function database()
    {
        return $this->_getCurrentDatabaseName();
    }

    /**
     * Gets the name of the wordpress prefix if not supplied a table, or the
     * prefixed table name if one is supplied.
     *
     * If a table is supplied, it returns the correct table name with
     * the prefix applied. The prefix will not be duplicated if it
     * already exists on the supplied table name.
     *
     * @param string $table (optional)
     * @return string
     */
    public function prefix( $table = null )
    {
        $prefix = $this->_getCurrentTablePrefix();
        if ( !is_null( $table ) )
        {
            if ( strpos( $table, $prefix ) === 0 )
            {
                return $table;
            }
            return $prefix . $table;
        }
        return $prefix;
    }

    /**
     * Correctly formats a fully qualified table name in the current database,
     * with escaped database and table name, and the Wordpress prefix
     * correctly applied to the table.
     * @param type $table
     * @return type
     */
    public function table( $table )
    {
        return '`' . $this->database() . '`.`' . $this->prefix( $table ) . '`';
    }

    /**
     * Returns an indexed array of all of the tables in the current database.
     *
     * @return array
     */
    public function tables()
    {
        $tables = array();
        $query = "SHOW TABLES FROM `" . $this->_getCurrentDatabaseName() . "`;";
        $res = $this->run( $query )->fetchAll( \PDO::FETCH_ASSOC );
        return array_column( $res, array_keys( $res[0] )[0] );
    }

    /**
     * Returns a list of all of the columns in the given table.
     *
     * @param string $table
     * @return array
     */
    public function columns( $table )
    {
        $query = "SHOW COLUMNS FROM " . $this->table( $table ) . ";";
        $res = $this->run( $query )->fetchAll( \PDO::FETCH_ASSOC );
        return array_column( $res, array_keys( $res[0] )[0] );
    }

    /**
     * Returns a list of all of the rows in a supplied table.
     *
     * @todo Add support for where clauses
     *
     * @param string $table
     * @param array $where (optional) incomplete
     * @return type
     */
    public function rows( $table, array $where = null )
    {
        if ( is_null( $where ) )
        {
            $query = 'SELECT * FROM ' . $this->table( $table ) . ';';
            $res = $this->run( $query )->fetchAll( \PDO::FETCH_ASSOC );
            return $res;
        }
    }

    /**
     * Returns a list of all of the indexes that exist on a supplied table.
     *
     * @param string $table
     * @return array
     */
    public function indexes( $table )
    {
        $query = 'SHOW INDEX FROM ' . $this->table( $table ) . ';';
        $res = $this->run( $query )->fetchAll( \PDO::FETCH_ASSOC );
        return $res;
    }

    /**
     * Returns a count of all of the rows that exist in a given table,
     * or false if the table is empty.
     *
     * @todo Add support for where clauses
     *
     * @param string $table
     * @param array $where (optional)
     * @return bool|array
     */
    public function rowCount( $table, array $where = null )
    {
        if ( is_null( $where ) )
        {
            $query = 'SELECT count(*) FROM ' . $this->table( $table ) . ';';
            $res = $this->run( $query )->fetchAll( \PDO::FETCH_ASSOC );
        }
        if ( empty( $res ) )
        {
            return false;
        }
        return array_shift( $res[0] );
    }

    /**
     * Returns whatever the default wordpress database endpoint currently is.
     *
     * @return string
     * @internal
     */
    private function _constructDsn()
    {
        $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET;
        return $dsn;
    }

    /**
     * Returns whatever the default wordpress database username currently is.
     *
     * @return string
     * @internal
     */
    private function _constructUsername()
    {
        return DB_USER;
    }

    /**
     * Returns whatever the default wordpress database password currently is.
     *
     * @return string
     * @internal
     */
    private function _constructPassword()
    {
        return DB_PASSWORD;
    }

    /**
     * Sets the default safe options, and allows for overrides
     * @param array $options (optional) valid PDO option attributes
     * @return array
     * @internal
     */
    private function _constructOptions( array $options = array() )
    {
        return array_replace( array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
            ), $options );
    }

    private function _bindWordpressDatabase()
    {
        if ( is_null( self::$wpdb ) )
        {
            global $wpdb;
            self::$wpdb = &$wpdb;
        }
    }

    private function _getCurrentDatabaseName()
    {
        return self::$wpdb->dbname;
    }

    private function _getCurrentTablePrefix()
    {
        return self::$wpdb->prefix;
    }

}
