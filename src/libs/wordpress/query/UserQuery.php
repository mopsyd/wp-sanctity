<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\query;

/**
 * User Query
 *
 * Provides base abstraction for WP_User_Query.
 *
 * (WP_Query, WP_User_Query, WP_Comment_Query, WP_Term_Query, etc)
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class UserQuery
extends AbstractQuery
{
    /**
     * Returns the identifying key of the worker.
     *
     * @return string "user"
     */
    public function getSubjectKey()
    {
        return 'user';
    }

    /**
     * Receives a subject and parses it into a WP_User_Query instance.
     *
     * @param mixed $subject
     *
     * @return \WP_User_Query
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameter is not an instance
     *     of the object or interface declared in `getSubjectType`
     */
    public function query( $subject )
    {
        return new \WP_User_Query( $subject );
    }

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset()
    {

    }
}
