<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Sidebar
 *
 * Fetches dynamic sidebar markup.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class Sidebar
    extends AbstractInfo
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "sidebar"
     */
    public function getSubjectKey()
    {
        return 'sidebar';
    }

    /**
     * Fetches a Dynamic Sidebar from WordPress, captures it with
     * the output buffer, and returns the string represenation of it.
     *
     * @param string $key The sidebar id
     * @param string $default (not used)
     * @return string Returns the sidebar markup
     */
    public function get( $key, $default = null )
    {
        $this->typeCheckRaw( $key );
        ob_start();
        dynamic_sidebar( $key );
        return ob_get_clean();
    }

    /**
     * This method just defers to the registration method.
     *
     * If this is called out of sync with the correct registration action,
     * it will result in an exception.
     *
     * Don't use this, use the registration worker.
     *
     * @param type $key The id of the sidebar, if you insist on doing it the
     *      hard way and wasting cycles with redundant internal proxying.
     * @param array $value It will try to pass this to the
     *      registration method, but it's probably going to bounce you
     *      because you're doing it wrong.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *      Honestly, you're probably going to get this about 90% of the time
     *      if you call this. This is why this method is not exposed through
     *      the adapter. The proper way to enable a sidebar is to use the
     *      registration method `$adapter->register('sidebar', $details);`
     */
    public function set( $key, $value )
    {
        $this->typeCheckRaw( $key );
    }

}
