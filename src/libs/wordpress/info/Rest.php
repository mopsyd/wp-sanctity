<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Rest
 *
 * Fetches REST endpoints.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class Rest
    extends AbstractInfo
{

    private static $endpoints = array(
        'posts' => '/wp/v2/posts',
        'revisions' => '/wp/v2/revisions',
        'categories' => '/wp/v2/categories',
        'tags' => '/wp/v2/tags',
        'pages' => '/wp/v2/pages',
        'comments' => '/wp/v2/comments',
        'taxonomies' => '/wp/v2/taxonomies',
        'media' => '/wp/v2/media',
        'users' => '/wp/v2/users',
        'post-types' => '/wp/v2/types',
        'post-status' => '/wp/v2/statuses',
        'settings' => '/wp/v2/settings',
    );

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "rest"
     */
    public function getSubjectKey()
    {
        return 'rest';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * The image parser generally receives null, but allows for strings
     * so it can remain compatible to rebranding modules.
     *
     * @return string "mixed".
     */
    public function getSubjectType()
    {
        return 'mixed';
    }

    /**
     * Fetches the specified REST endpoints,
     * or all endpoints if no parameter is passed.
     *
     * @param string $key The sidebar id
     * @param string $default (not used)
     * @return string Returns the sidebar markup
     */
    public function get( $key = null, $default = null )
    {
        if (is_string($key))
        {
            if (array_key_exists($key, self::$endpoints))
            {
                return self::$endpoints[$key];
            }
            return false;
        }
        return self::$endpoints;
    }

    /**
     * This method just defers to the registration method.
     *
     * If this is called out of sync with the correct registration action,
     * it will result in an exception.
     *
     * Don't use this, use the registration worker.
     *
     * @param type $key The id of the sidebar, if you insist on doing it the
     *      hard way and wasting cycles with redundant internal proxying.
     * @param array $value It will try to pass this to the
     *      registration method, but it's probably going to bounce you
     *      because you're doing it wrong.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *      Honestly, you're probably going to get this about 90% of the time
     *      if you call this. This is why this method is not exposed through
     *      the adapter. The proper way to enable a sidebar is to use the
     *      registration method `$adapter->register('sidebar', $details);`
     */
    public function set( $key, $value )
    {
        $this->typeCheckRaw( $key );
    }

}
