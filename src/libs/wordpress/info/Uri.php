<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Uri
 *
 * Fetches correctly formatted full uri to the local relative script path,
 * based on the platforms routing schema.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class Uri
    extends AbstractInfo
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "uri"
     */
    public function getSubjectKey()
    {
        return 'uri';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "string".
     */
    public function getSubjectType()
    {
        return 'string';
    }

    /**
     * Fetches the root or child theme uri path.
     *
     * @param string $key root|sanctity|override
     * @param string $default (not used)
     * @return string Returns the fully qualified uri to the Sanctity directory
     *     or child theme directory.
     */
    public function get( $key, $default = null )
    {
        $this->typeCheckRaw( $key );
        switch ( $key )
        {
            case 'root':
                return trailingslashit( site_url() );
                break;
            case 'sanctity':
                return trailingslashit( get_template_directory_uri() );
                break;
            case 'override':
                return trailingslashit( get_stylesheet_directory_uri() );
                break;
            default:
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Error encountered in [%1$s]. Unknown key [%2$s]. '
                    . 'Acceptable keys are [%3$s].', get_class( $this ), $key,
                    'root|sanctity|override' )
                );
        }
    }

    /**
     * This method just defers to the registration method.
     *
     * If this is called out of sync with the correct registration action,
     * it will result in an exception.
     *
     * Don't use this, use the registration worker.
     *
     * @param type $key The id of the sidebar, if you insist on doing it the
     *      hard way and wasting cycles with redundant internal proxying.
     * @param array $value It will try to pass this to the
     *      registration method, but it's probably going to bounce you
     *      because you're doing it wrong.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *      Honestly, you're probably going to get this about 90% of the time
     *      if you call this. This is why this method is not exposed through
     *      the adapter. The proper way to enable a sidebar is to use the
     *      registration method `$adapter->register('sidebar', $details);`
     */
    public function set( $key, $value )
    {
        $this->typeCheckRaw( $key );
    }

}
