<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Site
 *
 * Fetches site information from bloginfo.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class Site
    extends AbstractInfo
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "site"
     */
    public function getSubjectKey()
    {
        return 'site';
    }

    /**
     * Fetches the logo declared by Wordpress.
     *
     * @param string $key The info key id
     * @param string $default (not used)
     * @return string Returns the sidebar markup
     */
    public function get( $key, $default = null )
    {
        return $this->_parseKey( $key );
    }

    /**
     * This method just defers to the registration method.
     *
     * If this is called out of sync with the correct registration action,
     * it will result in an exception.
     *
     * Don't use this, use the registration worker.
     *
     * @param type $key The id of the sidebar, if you insist on doing it the
     *      hard way and wasting cycles with redundant internal proxying.
     * @param array $value It will try to pass this to the
     *      registration method, but it's probably going to bounce you
     *      because you're doing it wrong.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *      Honestly, you're probably going to get this about 90% of the time
     *      if you call this. This is why this method is not exposed through
     *      the adapter. The proper way to enable a sidebar is to use the
     *      registration method `$adapter->register('sidebar', $details);`
     */
    public function set( $key, $value )
    {
        $this->typeCheckRaw( $key );
    }

    private function _parseKey( $key )
    {
        $value = false;
        switch ( $key )
        {
            case 'name':
            case 'description':
                $value = get_bloginfo( $key );
                break;
            case 'url':
                $value = esc_url( trailingslashit( get_bloginfo( $key ) ) );
                break;
            case 'banner':
                if ( get_theme_mod( 'header_image_data' ) && is_object( get_theme_mod( 'header_image_data' ) ) )
                {
                    $data = get_object_vars( get_theme_mod( 'header_image_data' ) );
                    $attachment_id = is_array( $data ) && isset( $data['attachment_id'] )
                        ? $data['attachment_id']
                        : false;
                    if ( $attachment_id !== false )
                    {
                        $value = $this::getAdapter()->parse( 'image',
                            get_post( $attachment_id ) );
                    }
                }
                break;
            case 'logo':
                $value = get_theme_mod( 'custom_logo' );
                if ( $value !== '' )
                {
                    $value = $this::getAdapter()->parse( 'image',
                        get_post( $value ) );
                }
                break;
            case 'body-class':
                $value = get_body_class();
                break;
            case 'background':
                if ( strpos( get_theme_mod( 'background_image' ),
                        $this->_parseKey( 'url' ) ) !== false )
                {
                    // PDO is a bzillion times faster than the WordPress database object,
                    // and has much more secure prepared statements.
                    // It also doesn't pollute queries with random stripslash and
                    // addslash garbage that is terrible practice. :D
                    $db = $this->load( 'library', 'pdo\\Pdo' );
                    $res = $db->run( "SELECT ID FROM {$db->wpdb()->prefix}posts WHERE guid RLIKE :guid;",
                            array(
                            'guid' => substr( get_theme_mod( 'background_image' ),
                                strpos( get_theme_mod( 'background_image' ),
                                    $this->_parseKey( 'url' ) ) + strlen( $this->_parseKey( 'url' ) )
                            )
                            )
                        )->fetch();
                    if ( array_key_exists( 'ID', $res ) )
                    {
                        $value = $this::getAdapter()->parse( 'image',
                            get_post( $res['ID'] ) );
                    }
                }
                break;
            case 'background-color':
                $value = esc_attr( get_background_color() );
                if ( $value !== '' )
                {
                    $value = '#' . $value;
                }
                break;
            case 'email':
                $value = sanitize_email( get_bloginfo( 'admin_email' ) );
                break;
            default:
                // Unknown key
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Error encountered in [%1$s]. Specified key [%2$s] is not known.',
                    get_class( $this ), $key )
                );
                break;
        }
        // WordPress can't seem to null results appropriately,
        // and sometimes returns zero, -1, an empty string, or false.
        // Empty strings are always false (no result) in this case.
        // Numerical nulls are dealt with in the individual parser
        // on a case by case. For consistency's sake, this method always
        // returns false if there is no result but the key request is valid.
        if ( $value === '' )
        {
            $value = false;
        }
        return $value;
    }

}
