<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Stylesheet Info
 *
 * Handles getting and setting of stylesheets that should queue automatically
 * for various WordPress pages.
 *
 * Setting on this object does not retain the data in the data layer.
 * It instead statically caches it at runtime, so get requests for the
 * script handle will return it for the given page type.
 *
 * Setting on this object also does not perform actual stylesheet registration,
 * it just sets it as a valid stylesheet handle for any given page request type.
 *
 * It should also be noted that this object uses the Sanctity page type notation,
 * not the WordPress notation.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class Stylesheets
    extends AbstractInfo
{

    private static $stylesheet_registry = array(
        'login' => array(),
        'ajax' => array(),
        'admin-theme-index' => array(),
        'admin-media-upload' => array(),
        'admin-media-editor' => array(),
        'admin-tag-editor' => array(),
        'admin-category-editor' => array(),
        'admin-customize' => array(),
        'admin-comment-editor' => array(),
        'admin-file-editor' => array(),
        'admin-menu-editor' => array(),
        'admin-widget-editor' => array(),
        'admin-editor' => array(),
        'admin-feedback-index' => array(),
        'admin-update' => array(),
        'admin-comment-index' => array(),
        'admin-page-index' => array(),
        'admin-post-index' => array(),
        'admin-plugin-index' => array(),
        'admin-plugin-selector' => array(),
        'admin-plugin-editor' => array(),
        'admin-plugin-settings' => array(),
        'admin-user-index' => array(),
        'admin-user-editor' => array(),
        'admin-about' => array(),
        'admin-about-credits' => array(),
        'admin-about-license' => array(),
        'admin-about-privacy' => array(),
        'admin-tools' => array(),
        'admin-import' => array(),
        'admin-export' => array(),
        'admin-settings-custom' => array(),
        'admin-settings' => array(),
        'admin-settings-reading' => array(),
        'admin-settings-writing' => array(),
        'admin-settings-discussion' => array(),
        'admin-settings-media' => array(),
        'admin-settings-permalinks' => array(),
        'admin-dashboard' => array(),
        'admin' => array(),
        401 => array(),
        403 => array(),
        404 => array(),
        500 => array(),
        'attachment' => array(),
        'index' => array(),
        'blog' => array(),
        'search' => array(),
        'author' => array(),
        'tag' => array(),
        'category' => array(),
        'archive' => array(),
        'robots' => array(),
        'custom' => array(),
        'feed' => array(),
        'page' => array(),
        'post' => array(),
    );

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "styles"
     */
    public function getSubjectKey()
    {
        return 'styles';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * The image parser generally receives null, but allows for strings
     * so it can remain compatible to rebranding modules.
     *
     * @return string "string".
     */
    public function getSubjectType()
    {
        return 'string';
    }

    public function get( $key, $default = null )
    {
        $this->typeCheckRaw( $key );
        if (!array_lkey_exists($key, self::$stylesheet_registry))
        {
            return array();
        }
        return self::$stylesheet_registry[$key];
    }

    public function set( $key, $value )
    {
        $this->typeCheckRaw( $key );
        if (!array_key_exists($key, self::$stylesheet_registry))
        {
            return false;
        }
        if (in_array($value, self::$stylesheet_registry))
        {
            return false;
        }
        self::$stylesheet_registry[$key][] = $value;
        return true;
    }

}
