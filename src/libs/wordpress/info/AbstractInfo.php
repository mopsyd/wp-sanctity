<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\info;

/**
 * Abstract Info
 *
 * Represents the base abstraction for Wordpress information providers.
 *
 * This object operates against the WordPress
 * Api directly to obtain and set data.
 *
 * This lets the underlying Sanctity logic remain decoupled entirely from the
 * Wordpress filtering logic, but still leverage it via the adapter.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractInfo
    extends \mopsyd\sanctity\libs\wordpress\AbstractWordpressWorker
    implements \mopsyd\sanctity\interfaces\libs\wordpress\InfoInterface
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "title"
     */
    public function getSubjectKey()
    {
        return 'title';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "string".
     */
    public function getSubjectType()
    {
        return 'string';
    }

    public function get( $key, $default = null )
    {
        ;
    }

    public function set( $key, $value )
    {
        ;
    }

}
