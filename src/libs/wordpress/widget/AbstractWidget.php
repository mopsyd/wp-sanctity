<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\widget;

/**
 * Abstract Widget
 *
 * Wordpress Widgets done Sanctity style.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractWidget
    extends \WP_Widget
    implements \mopsyd\sanctity\interfaces\libs\wordpress\WidgetInterface
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

/**
     * Debug all of the things. Safely for production though.
     *
     * This is broken off into a trait so it can also be attached to classes
     * that extend from 3rd party sources without any duplicate code.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\Debugger;

/**
     * Containerize all of the things.
     *
     * This is broken off into a trait so it can also be attached to classes
     * that extend from 3rd party sources without any duplicate code.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\ContainerPackagerUtility;

/**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

    const WIDGET_IDENTIFIER = self::PACKAGE_SLUG;
    const WIDGET_NAME = false;
    const WIDGET_DESCRIPTION = false;

    private static $factory;
    private static $view;
    private $instance_id;
    private $instance_name;
    private $instance_options = array();
    private $instance_control_options = array();
    private static $widget_details = array();
    private static $model;

    /**
     * PHP5 constructor.
     *
     * @since 2.8.0
     *
     * @param string $id_base         Optional Base ID for the widget, lowercase and unique. If left empty,
     *                                a portion of the widget's class name will be used Has to be unique.
     * @param string $name            Name for the widget displayed on the configuration page.
     * @param array  $widget_options  Optional. Widget options. See wp_register_sidebar_widget() for information
     *                                on accepted arguments. Default empty array.
     * @param array  $control_options Optional. Widget control options. See wp_register_widget_control() for
     *                                information on accepted arguments. Default empty array.
     */
    public function __construct( $id_base = null, $name = null,
        $widget_options = array(), $control_options = array() )
    {
        parent::__construct(
            $this->declareId( $id_base ), $this->declareName( $name ),
            $this->declareOptions( $widget_options ),
            $this->declareControlOptions( $control_options )
        );
    }

    public static function init( $dependencies = null, $args = array() )
    {
        $id = false;
        $name = null;
        $widget_options = array();
        $control_options = array();
        if ( array_key_exists( 'id', $args ) )
        {
            $id = $args['id'];
        }
        if ( array_key_exists( 'name', $args ) )
        {
            $name = $args['name'];
        }
        if ( array_key_exists( 'options', $args ) )
        {
            $widget_options = $args['options'];
        }
        if ( array_key_exists( 'control-options', $args ) )
        {
            $control_options = $args['control-options'];
        }
        $class = get_called_class();
        return new $class( $id, $name, $widget_options, $control_options );
    }

    /**
     * Sets the factory factory, and passes it to child adapter abstract classes.
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $factory
     */
    public static function setFactory( \mopsyd\sanctity\factory\FactoryFactory $factory )
    {
        self::$factory = $factory;
    }

    /**
     * Sets the widget model, which is referenced to interact
     * with the Sanctity system indirectly by Widgets.
     *
     * @param \mopsyd\sanctity\models\integration\WidgetModel $model
     */
    public static function setModel( \mopsyd\sanctity\models\integration\WidgetModel $model )
    {
        self::$model = $model;
    }

    public static function setDetails( array $details )
    {
        $class = get_called_class();
        self::$widget_details[$class] = $class::containerize( $details );
    }

    /**
     * PHP4 constructor. Also known as "You Shall Not Pass"
     *
     * That's a big fat nope. Don't use this, it's only there because Wordpress
     *     insists on supporting PHP versions older than the internet.
     *
     * @note Shennanigans.
     */
    public function WP_Widget( $id_base, $name, $widget_options = array(),
        $control_options = array() )
    {
        //I don't know how you managed to run this on a PHP 4 install
        //because this package requires PHP 5.6+ bare minimum.
        //
        //UPDATE YOUR CODE. Bloody hell.
        return $this->__construct( $id_base, $name, $widget_options,
                $control_options );
    }

    /**
     * Echoes the widget content.
     *
     * Sub-classes should over-ride this function to generate their widget code.
     *
     * @since 2.8.0
     *
     * @param array $args     Display arguments including 'before_title', 'after_title',
     *                        'before_widget', and 'after_widget'.
     * @param array $instance The settings for the particular instance of the widget.
     */
    public function widget( $args, $instance )
    {
        $context = array();
        $context['args'] = $args;
        $context['instance'] = $instance;
        $context = array_merge($context, $this->getContext() );
        $template = $this->getDetails()->get( 'template' );
        foreach ( $this->parseArguments( $args, $instance ) as
            $key =>
            $value )
        {
            $context[$key] = $value;
        }
        $this::getAdapter()->getView()->renderPart( $template, $context );
    }

    /**
     * Updates a particular instance of a widget.
     *
     * This function should check that `$new_instance` is set correctly. The newly-calculated
     * value of `$instance` should be returned. If false is returned, the instance won't be
     * saved/updated.
     *
     * @since 2.8.0
     *
     * @param array $new_instance New settings for this instance as input by the user via
     *                            WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     * @return array Settings to save or bool false to cancel saving.
     */
    public function update( $new_instance, $old_instance )
    {
        s( $new_instance, $old_instance );
        return array(
            'test' => 'foo' );
    }

    /**
     * Outputs the settings update form.
     *
     * @since 2.8.0
     *
     * @param array $instance Current settings.
     * @return string Default return is 'noform'.
     */
    public function form( $instance )
    {
        return parent::form( $instance );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post" is the default key,
     *     as it is the most comon context.
     */
    public function getSubjectKey()
    {
        return 'widget';
    }

    /**
     * Wordpress widgets have a screwey Api, so this can't really appy and
     * be compatible with the underlying Wordpress logic.
     *
     * We'll have to typecheck within the Widget itself.
     *
     * @return string "mixed"
     */
    public function getSubjectType()
    {
        return 'mixed';
    }

    /**
     * Override this method to provide parsing logic for the widget
     * immediately prior to render. It will be passed the same arguments
     * that are provided to the `widget` method, and should return an array
     * of additional details to package into the template context.
     * The args and instance will already be packaged, so there is no need
     * to return those, this is only for additional logic that is out of
     * scope of the abstraction.
     *
     * @param type $args
     * @param type $instance
     * @return array
     */
    protected function parseArguments( $args, $instance )
    {
        return array();
    }

    /**
     * Gets the details
     * @param type $key
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    protected function getDetails()
    {
        $class = get_called_class();
        if ( !array_key_exists( $class, self::$widget_details ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Credential details not detected. '
                . 'This class must be instantiated by the Widget Model to be used.',
                get_class( $this ) )
            );
        }
        return self::$widget_details[$class];
    }

    protected function getContext()
    {
        $context = self::$model->getWidgetContext( $this->getDetails()->get( 'slug' ) );
        return $context;
    }

    protected function declareId( $id )
    {
        if ( is_null( $id ) )
        {
            $id = $this->getDetails()->get( 'slug' );
        }
        return $id;
    }

    protected function declareName( $name )
    {
        if ( is_null( $name ) )
        {
            $name = $this->getDetails()->get( 'name' );
        }
        return $name;
    }

    protected function declareOptions( $options )
    {
        if ( empty( $options ) )
        {
            return $this->getDetails()->get( 'options' );
        }
        return $options;
    }

    protected function declareControlOptions( $options )
    {
        if ( empty( $options ) )
        {
            return $this->getDetails()->get( 'control' );
        }
        return $options;
    }

}
