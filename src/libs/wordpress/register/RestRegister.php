<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Rest Register
 *
 * Registers and unregisters the WordPress REST Api.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class RestRegister
    extends AbstractRegister
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "rest"
     */
    public function getSubjectKey()
    {
        return 'rest';
    }

    /**
     * Register type workers generally accept array arguments.
     *
     * This worker only has one job, so the parameter is irrelevant.
     *
     * @return string "mixed".
     */
    public function getSubjectType()
    {
        return 'mixed';
    }

    /**
     * Registers the WordPress REST Api. This gets its own worker because
     * it's inherently incompatible with the rest of the script registration logic.
     *
     * @param mixed $subject (not used)
     * @return bool Returns true on successful registration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject = null )
    {
        wp_enqueue_script( 'wp-api' );
        return true;
    }

    /**
     * Unregisters the WordPress REST Api.
     *
     * @param mixed $subject (not used)
     * @return bool Returns true on successful unregistration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function unregister( $subject = null )
    {
        wp_dequeue_script( 'wp-api' );
        return true;
    }

    /**
     * Added for worker compliance, but registration data is generally not
     * retained internally in the worker object, so this method doesn't
     * really need to do much in this case.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset()
    {
        // no-op. Settings are not retained in register objects by default.
        return $this;
    }
}
