<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Abstract Register
 *
 * Provides base abstraction for registering various things with Wordpress.
 *
 * (settings, options, menus, sidebars, widgets, etc)
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractRegister
extends \mopsyd\sanctity\libs\wordpress\AbstractWordpressWorker
implements \mopsyd\sanctity\interfaces\libs\wordpress\RegisterInterface
{
    /**
     * Register type workers generally accept array arguments.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Applies a registration action to the supplied subject appropriate
     * to the scope of the worker object.
     *
     * This method is the action method for register type workers.
     *
     * By default, this method takes no action unless overridden,
     * which allows a child class to use it as a null pattern if not overridden.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $subject
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type, class,
     *     or interface declared by `getSubjectType`. This behavior is
     *     consistent with all other instances or the WordpressWorkerInterface,
     *     and should be implemented to preemptively block errors even on a
     *     null-state pattern. Zero tolerance for data ambiguity at this level,
     *     particularly as this method enacts destructive changes on the data layer.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject )
    {
        $this->typeCheckRaw( $subject );
        return false;
    }

    /**
     * Clears registration of the subject matter, if it is possible to do so.
     *
     * By default, this method takes no action unless overridden,
     * which allows a child class to use it as a null pattern if not overridden.
     *
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type, class,
     *     or interface declared by `getSubjectType`. This behavior is
     *     consistent with all other instances or the WordpressWorkerInterface,
     *     and should be implemented to preemptively block errors even on a
     *     null-state pattern. Zero tolerance for data ambiguity at this level,
     *     particularly as this method enacts destructive changes on the data layer.
     */
    public function unregister( $subject )
    {
        $this->typeCheckRaw( $subject );
        return false;
    }

    /**
     * Added for worker compliance, but registration data is generally not
     * retained internally in the worker object, so this method doesn't
     * really need to do much in this case.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset()
    {
        // no-op. Settings are not retained in register objects by default.
        return $this;
    }
}
