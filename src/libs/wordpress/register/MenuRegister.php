<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Menu Register
 *
 * Registers and unregisters nav menus.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class MenuRegister
    extends AbstractRegister
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "menu"
     */
    public function getSubjectKey()
    {
        return 'menu';
    }

    /**
     * Register type workers generally accept array arguments.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Applies a registration action to the supplied subject appropriate
     * to the scope of the worker object.
     *
     * This method is the action method for register type workers.
     *
     * By default, this method takes no action unless overridden,
     * which allows a child class to use it as a null pattern if not overridden.
     *
     * If the provided parameter is invalid, this method must immediately
     * raise a \mopsyd\sanctity\libs\exception\SanctityException
     * without doing any processing on the object.
     *
     * @param object $subject
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type, class,
     *     or interface declared by `getSubjectType`. This behavior is
     *     consistent with all other instances or the WordpressWorkerInterface,
     *     and should be implemented to preemptively block errors.
     *     Zero tolerance for data ambiguity at this level,
     *     particularly as this method enacts destructive changes
     *     on the data layer.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject )
    {
        $this->typeCheckRaw( $subject );
        $subject = array_merge( $this->_getDefaults(), $subject );
        $this->_checkSuppliedKeys( $subject );
        register_nav_menu( $subject['handle'],
            __( $subject['name'], $subject['textdomain'] ) );
        return true;
    }

    /**
     * Clears registration of the subject matter, if it is possible to do so.
     *
     * By default, this method takes no action unless overridden,
     * which allows a child class to use it as a null pattern if not overridden.
     *
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type, class,
     *     or interface declared by `getSubjectType`. This behavior is
     *     consistent with all other instances or the WordpressWorkerInterface,
     *     and should be implemented to preemptively block errors.
     *     Zero tolerance for data ambiguity at this level,
     *     particularly as this method enacts destructive changes
     *     on the data layer.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function unregister( $subject )
    {
        $this->typeCheckRaw( $subject );
        $subject = array_merge( $this->_getDefaults(), $subject );
        $this->_checkSuppliedKeys( $subject );
        unregister_nav_menu( $subject['handle'] );
        return true;
    }

    /**
     * Added for worker compliance, but registration data is generally not
     * retained internally in the worker object, so this method doesn't
     * really need to do much in this case.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset()
    {
        // no-op. Settings are not retained in register objects by default.
        return $this;
    }

    /**
     * Enforces that the provided keys are present prior to
     * register or unregister operations.
     *
     * Although the underlying Wordpress api allows menus to be unregistered
     * by only supplying the key, this logic does not.
     *
     * It requires that an identical set is provided to the original
     * menu declaration to perform unregistration, so there is zero ambiguity
     * about intent.
     *
     * Higher level logic is expected to obtain or retain a reference
     * to the original data provided to perform unregistrations in
     * order to strictly enforce data integrity.
     *
     * @param array $subject
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any expected keys are not present, OR if their values
     *     are incorrectly typed.
     * @internal
     */
    private function _checkSuppliedKeys( $subject )
    {
        $expected_keys = array(
            'handle' => 'string',
            'name' => 'string',
            'textdomain' => 'string'
        );
        $invalid = array();
        foreach ( $expected_keys as
            $key =>
            $type )
        {
            if ( !array_key_exists( $key, $subject ) || gettype( $subject[$key] )
                !== $type )
            {
                $invalid[] = $key . ': ' . $type;
            }
        }
        if ( !empty( $invalid ) )
        {
            //The supplied menu does not follow the expected key structure. Bounce it.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid parameter set passed in [%1$s]. '
                . 'The following supplied keys were missing or '
                . 'invalid in the provided subject [%2$s].', get_class( $this ),
                implode( ', ', $invalid ) )
            );
        }
    }

    /**
     * It is allowable for a textdomain not to be present,
     * in which case it will use the default.
     * All other keys are required.
     * @return array
     * @internal
     */
    private function _getDefaults()
    {
        return array(
            'textdomain' => 'default'
        );
    }

}
