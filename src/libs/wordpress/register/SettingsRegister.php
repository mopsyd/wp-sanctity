<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Settings Register
 *
 * Registers and unregisters settings.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class SettingsRegister
    extends AbstractRegister
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "option"
     */
    public function getSubjectKey()
    {
        return 'option';
    }

    /**
     * Register type workers generally accept array arguments.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Registers Wordpress options. Will perform an internal preflight to insure
     * that all expected keys are present, and that the given options are congruent
     * with what WordPress expects for options registration.
     *
     * @param array $subject
     * @return bool Returns true on successful registration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( empty( $subject ) )
        {
            return false;
        }
        $opts = $this->_collectOptions( $subject );
        $this->_registerSettings( $opts );
        return true;
    }

    /**
     * Unregisters WordPress options. Will perform the same internal preflight
     * as the registration process, and extract the details needed to cleanly
     * de-register. Logic calling against this method should provide the exact
     * same set of credentials used to register the setting in order to
     * unregister it.
     *
     * @param array $subject
     * @return bool Returns true on successful unregistration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function unregister( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( empty( $subject ) )
        {
            return false;
        }
        $opts = $this->_collectOptions( $subject );
        $this->_unregisterSettings( $opts );
        return true;
    }

    /**
     * Added for worker compliance, but registration data is generally not
     * retained internally in the worker object, so this method doesn't
     * really need to do much in this case.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset()
    {
        // no-op. Settings are not retained in register objects by default.
        return $this;
    }

    private function _collectOptions( $subject )
    {
        if ( !( array_key_exists( 'options', $subject ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Key [%2$s] is not valid in provided settings group.',
                get_class( $this ), 'options' )
            );
        }
        $opts = array();
        $defaults = $this->_getOptionDefaultKeys();
        foreach ( $subject['options'] as
            $key =>
            $option )
        {
            $opt = array_replace_recursive( $defaults,
                $this->_standardizeOptionKeyset( $option ) );
            $this->_preflightOption( $opt );
            $opts[$key] = $opt;
        }
        return $opts;
    }

    /**
     * Registers the settings with Wordpress
     * @param array $settings_groups
     * @return void
     * @internal
     */
    private function _registerSettings( $opts )
    {
        foreach ( $opts as
            $key =>
            $value )
        {
            if ( array_key_exists( 'option_group', $value['option_args'] ) )
            {
                // This is only needed for validation,
                // not for registration.
                unset( $value['option_args']['option_group'] );
            }
            register_setting( $value['option_group'], $value['option_name'],
                $value['option_args'] );
            $this::debugStash( array(
                'group' => $value['option_group'],
                'name' => $value['option_name'],
                'args' => $value['option_args'],
                ), 'settings_registered' );
        }
    }

    private function _unregisterSettings( $opts )
    {
        foreach ( $opts as
            $key =>
            $value )
        {
            if ( array_key_exists( 'option_group', $value['option_args'] ) )
            {
                // This is only needed for validation,
                // not for registration.
                unset( $value['option_args']['option_group'] );
            }
            unregister_setting( $value['option_group'], $value['option_name'],
                $value['option_args']['sanitize_callback'] );
            $this::debugStash( array(
                'group' => $value['option_group'],
                'name' => $value['option_name'],
                'args' => $value['option_args'],
                ), 'settings_unregistered' );
        }
    }

    /**
     * Checks that the provided options parsing is correctly
     * structured to be passed to Wordpress without error.
     *
     * @param array $settings_groups
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This occurs if the provided options are lacking a "group"
     *     or "options" key, or if the options set is lacking any of the
     *     following keys: "name", "type", "description", "sanitize_callback", "rest"
     * @internal
     */
    private function _preflightOption( $option )
    {
        $required = $this->_getOptionRequiredKeys();
        foreach ( $required as
            $require )
        {
            if ( !array_key_exists( $require, $option ) )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Error encountered in [%1$s]. Key [%2$s] is missing from provided option arguments.',
                    get_class( $this ), $require )
                );
            }
        }
        $this->_preflightOptionsArgs( $option['option_group'],
            $option['option_args'] );
    }

    /**
     * Preflights a set of options bound to a group, to insure that
     * they will not cause Wordpress to choke.
     *
     * @param string $group
     * @param array $settings
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     This occurs if the settings set is lacking any of the
     *     following keys: "name", "type", "description", "sanitize_callback", "rest"
     * @internal
     */
    private function _preflightOptionsArgs( $group, $settings )
    {
        $required = $this->_getArgsRequiredKeys();
        $missing = array();
        foreach ( $required as
            $require )
        {
            if ( !array_key_exists( $require, $settings ) )
            {
                $missing[] = $require;
            }
        }
        if ( !empty( $missing ) )
        {
            d( $settings, $group, $required, $missing );
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Required settings keys '
                . '[%2$s] were not provided for options args [%3$s].',
                get_class( $this ), implode( ', ', $missing ), $group )
            );
        }
    }

    private function _standardizeArgsKeyset( $args )
    {
        $result = array();
        $translator = $this->_getArgsRequiredKeys();
        foreach ( $args as
            $key =>
            $detail )
        {
            if ( array_key_exists( $key, $translator ) )
            {
                $result[$translator[$key]] = $detail;
            } else
            {
                $result[$key] = $detail;
            }
        }
        return $result;
    }

    private function _stripUnknownArgKeys( $args )
    {
        $translator = $this->_getArgsRequiredKeys();
        foreach ( $args as
            $key =>
            $value )
        {
            if ( !in_array( $key, $translator ) )
            {
                unset( $args[$key] );
            }
        }
        return $args;
    }

    private function _getArgsRequiredKeys()
    {
        return array(
            'group' => 'option_group',
            'type' => 'type',
            'description' => 'description',
            'sanitizer' => 'sanitize_callback',
            'rest' => 'show_in_rest',
            'default' => 'default'
        );
    }

    private function _getArgsDefaultKeys()
    {
        return array(
            'type' => 'string',
            'option_group' => $this->_prefixOptionKey( 'default' ),
            'description' => '',
            'sanitize_callback' => null,
            'show_in_rest' => false,
            'default' => false,
        );
    }

    private function _standardizeOptionKeyset( $option )
    {
        $result = array();
        $translator = $this->_getOptionRequiredKeys();
        foreach ( $option as
            $key =>
            $detail )
        {
            if ( array_key_exists( $key, $translator ) )
            {
                $result[$translator[$key]] = $detail;
            } else
            {
                $result[$key] = $detail;
            }
        }
        if ( array_key_exists( 'option_args', $result ) )
        {
            $result['option_args'] = $this->_standardizeArgsKeyset( $result['option_args'] );
            $result['option_args']['option_group'] = $result['option_group'];
        }
        return $this->_stripUnknownOptionKeys( $result );
    }

    private function _stripUnknownOptionKeys( $option )
    {
        $translator = $this->_getOptionRequiredKeys();
        foreach ( $option as
            $key =>
            $value )
        {
            if ( !in_array( $key, $translator ) )
            {
                unset( $option[$key] );
            }
        }
        if ( array_key_exists( 'option_args', $option ) )
        {
            $option['option_args'] = $this->_stripUnknownArgKeys( $option['option_args'] );
        }
        return $option;
    }

    private function _getOptionRequiredKeys()
    {
        return array(
            'group' => 'option_group',
            'id' => 'option_name',
            'options' => 'option_args'
        );
    }

    private function _getOptionDefaultKeys()
    {
        return array(
            'option_group' => $this->_prefixOptionKey( 'default' ),
            'option_args' => $this->_getArgsDefaultKeys(),
        );
    }

    private function _prefixOptionKey( $key )
    {
        return $this->getThemePrefix() . '-' . $key;
    }

}
