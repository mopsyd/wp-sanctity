<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Stylesheet Register
 *
 * Registers and unregisters stylesheets.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class StylesheetRegister
    extends AbstractRegister
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "style"
     */
    public function getSubjectKey()
    {
        return 'style';
    }

    /**
     * Register type workers generally accept array arguments.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Registers Wordpress stylesheets. Will perform an internal preflight to insure
     * that all expected keys are present, and that the given options are congruent
     * with what WordPress expects for stylesheet registration.
     *
     * @param array $subject
     * @return bool Returns true on successful registration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( empty( $subject ) )
        {
            return false;
        }
        return true;
    }

    /**
     * Unregisters WordPress stylesheets. Will perform the same internal preflight
     * as the registration process, and extract the details needed to cleanly
     * de-register. Logic calling against this method should provide the exact
     * same set of credentials used to register the stylesheet in order to
     * unregister it.
     *
     * @param array $subject
     * @return bool Returns true on successful unregistration,
     *     or false otherwise (false occurs if provided an empty set).
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not resolve to a valid options
     *     declaration for WordPress, even with default values applied
     *     to the provided data set.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function unregister( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( empty( $subject ) )
        {
            return false;
        }
        return true;
    }

    /**
     * Added for worker compliance, but registration data is generally not
     * retained internally in the worker object, so this method doesn't
     * really need to do much in this case.
     *
     * @return $this This method returns an instance of self for method chaining.
     */
    public function reset()
    {
        // no-op. Settings are not retained in register objects by default.
        return $this;
    }
}
