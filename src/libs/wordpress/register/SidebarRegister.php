<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\register;

/**
 * Sidebar Register
 *
 * Registers and unregisters sidebars.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class SidebarRegister
    extends AbstractRegister
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "sidebar"
     */
    public function getSubjectKey()
    {
        return 'sidebar';
    }

    /**
     * Registers a Wordpress Sidebar.
     *
     * @param object $subject
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function register( $subject )
    {
        $this->typeCheckRaw( $subject );
        $subject = array_replace( $this->_getDefaults(),
            $this->_stripUnknownParameters( $subject ) );
        $this->_checkSuppliedKeys( $subject );
        $sidebar = $this->_standardizeParameters( $subject );
        register_sidebar( $sidebar );
        return true;
    }

    /**
     * Unregisters a Wordpress Sidebar.
     *
     * @return bool Returns true on successful registration,
     *     or false otherwise.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject does not meet the declared type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if the provided parameters are improperly
     *     formatted for the requested registration operation.
     */
    public function unregister( $subject )
    {
        $this->typeCheckRaw( $subject );
        $subject = array_replace( $this->_getDefaults(),
            $this->_stripUnknownParameters( $subject ) );
        $this->_checkSuppliedKeys( $subject );
        $sidebar = $this->_standardizeParameters( $subject );
        unregister_sidebar( $sidebar['id'] );
        return true;
    }

    /**
     * Checks Sidebar expected keys in the register or unregister operation,
     * and bounces the attempt if they are not valid. Defaults are applied
     * prior to this operation, so only values not provided by the defaults
     * need to be presented.
     *
     * @param array $subject
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any expected keys are not present, OR if their values
     *     are incorrectly typed.
     * @internal
     */
    private function _checkSuppliedKeys( $subject )
    {
        $expected_keys = $this->_getExpectedParameters();
        $invalid = array();
        foreach ( $expected_keys as
            $key =>
            $type )
        {
            if ( !array_key_exists( $key, $subject ) || gettype( $subject[$key] )
                !== $type )
            {
                $invalid[] = $key . ': ' . $type;
            }
        }
        if ( !empty( $invalid ) )
        {
            //The supplied menu does not follow the expected key structure. Bounce it.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid parameter set passed in [%1$s]. '
                . 'The following supplied keys were missing or '
                . 'invalid in the provided subject [%2$s].', get_class( $this ),
                implode( ', ', $invalid ) )
            );
        }
    }

    /**
     * Provides the defaults for a new sidebar.
     * The handle is required, but the rest can use the default.
     * It is strongly suggested to at least provide a name and description.
     *
     * @return array
     * @internal
     */
    private function _getDefaults()
    {
        return array(
            'name' => 'Untitled Sidebar',
            'description' => 'No description provided.',
            'class' => '',
            'before_widget' => '<li id=\"%1$s\" class=\"%2$s\">',
            'after_widget' => '</li>',
            'before_title' => '<h2>',
            'after_title' => '</h2>'
        );
    }

    private function _standardizeParameters( $sidebar )
    {
        $result = array(
            'id' => $sidebar['id'], //The id that gets registered with the Wordpress api
            'name' => __( $sidebar['name'] ), //The human readable name
            'description' => __( $sidebar['description'] ), //The human readable description
            'class' => $sidebar['class'], //Any additioal classes passed by the layout
            'before_widget' => $sidebar['before_widget'], //pre-widget wrap markup
            'after_widget' => $sidebar['after_widget'], //post-widget wrap markup
            'before_title' => $sidebar['before_title'], //pre-title wrap markup
            'after_title' => $sidebar['after_title'] //post-title wrap markup
        );
        return $result;
    }

    private function _stripUnknownParameters( $params )
    {
        $valid = array_keys( $this->_getExpectedParameters() );
        foreach ( $params as
            $key =>
            $param )
        {
            if ( !in_array( $key, $valid ) )
            {
                unset( $params[$key] );
            }
        }
        return $params;
    }

    private function _getExpectedParameters()
    {
        $expected_keys = array(
            'id' => 'string',
            'name' => 'string',
            'description' => 'string',
            'class' => 'string',
            'before_widget' => 'string',
            'after_widget' => 'string',
            'before_title' => 'string',
            'after_title' => 'string'
        );
        return $expected_keys;
    }

}
