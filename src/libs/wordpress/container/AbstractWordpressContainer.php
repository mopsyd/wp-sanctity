<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\container;

/**
 * Abstract Wordpress Container
 *
 * This is an abstraction layer for containers that allows
 * them to work seamlessly with the Wordpress Adapter.
 *
 * This is used by the Wordpress Adapter to standardize the way that
 * data extracted from any of its various containers is handled,
 * and allows them to use the adapter itself as a mediator
 * for normalizing data values.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractWordpressContainer
    extends \mopsyd\sanctity\libs\container\AbstractContainer
    implements \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post" is the default key,
     *     as it is the most comon context.
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "mixed" is the most common value type for containers.
     */
    public function getSubjectType()
    {
        return 'mixed';
    }

    /**
     * <Abstract Wordpress Container Object Instantiation Trigger Method>
     * Blocks instantiation if the Adapter has not yet been properly set up
     * through dependency injection.
     *
     * This method provides an opportunity to do so through this method,
     * which covers the case of standard factorization using the Sanctity factory logic,
     * as well as manually passing an adapter instance into the constructor
     * under a variable named "adapter", so that extending classes can implement
     * direct adapter injection on instantiation if they wish.
     * The underlying interace check is still applied, and the program will
     * encounter a compile error if the provided adapter does not conform
     * to the requirements of the `setAdapter` method.
     *
     * @param array $args The arguments to be passed into the constructor.
     * @param string $method The internal method that the operation arose from.
     *     This function will not fire if it does not receive this argument
     *     as itself or the constructor.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the adapter has not properly initialized the class prior to use.
     *     Many of the internals require the adapter to have been properly passed
     *     through static dependency injection prior to usage.
     *     If this has not occurred, internals will break, generate errors,
     *     or cause erratic behavior.
     */
    protected function _onContainerInstantiation( $args, $method = null )
    {
        if ( !in_array( $method,
                array(
                "__construct",
                __FUNCTION__ ) ) )
        {
            //This method is only appropriate to fire from the constructor,
            //or another instance of this method calling parent.
            return;
        }
        if ( is_null( $this::getAdapter() ) && array_key_exists( 'adapter',
                $args ) )
        {
            //Covers direct dependency injection on instantiation by child classes
            //that have added an `$adapter` parameter to their constructor.
            self::setAdapter( $args['adapter'] );
        } elseif ( is_null( $this::getAdapter() ) && array_key_exists( 'dependencies',
                $args ) && array_key_exists( 'adapter', $args['dependencies'] ) )
        {
            //Covers injection into the constructor via factorization or unit testing.
            self::setAdapter( $args['dependencies']['adapter'] );
        }
        if ( is_null( $this::getAdapter() ) )
        {
            $trace = debug_backtrace(1);
            $trace = $trace[2];
            $this->debugStash( array(
                        'class' => get_class( $this ),
                        'context' => 'WordpressAdapter not yet set',
                        'culprit' => $trace['class'] . '::' . $trace['method']
                        ), 'initialization', 'invalid_load_order' );
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Inappropriate load order in [%1$s]. '
                . 'The adapter must initialize the container abstract '
                . '[%2$s] prior to instantiation.', get_class( $this ),
                __CLASS__ )
            );
        }
    }
}
