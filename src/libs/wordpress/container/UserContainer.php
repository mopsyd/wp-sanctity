<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\container;

/**
 * User Container
 *
 * Contains a set of details about a registered user.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class UserContainer
    extends AbstractWordpressContainer
{

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post" is the default key,
     *     as it is the most comon context.
     */
    public function getSubjectKey()
    {
        return 'user';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "mixed" is the most common value type for containers.
     */
    public function getSubjectType()
    {
        return 'string';
    }

    /**
     * Enforces that the provided key and value are identical on a set operation,
     * so that no mismatching of shortcode input occurs.
     *
     * @param string $key A shortcode
     * @param string $value The same shortcode
     * @param string $method The method internally that
     *     referenced the setter option.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Enforces that the value is the same shortcode value as the key,
     *     so mismatches do not occur.
     */
    protected function _onSet( $key, $value, $method = null )
    {
        parent::_onSet( $key, $value, $method );
    }

    /**
     * Automatically applies the shortcode on a set operation.
     *
     * @param string $key A shortcode
     * @param string $value The same shortcode
     * @param string $method The method internally that
     *     referenced the setter option.
     * @return string
     */
    protected function _filterSetValue( $key, $value, $method = null )
    {
        $value = parent::_filterSetValue( $key, $value, $method );
        return $value;
    }

}
