<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\container;

/**
 * Tag Collection
 *
 * Represents a collection of Wordpress tag containers.
 *
 * This is used by the Wordpress Adapter to standardize the way that
 * taxonomy data is returned to the rest of the system, so they do not have
 * to work directly against Wordpress object, and can defer
 * Wordpress api calls back to the adapter to complete in a standardized way.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class TagCollection
    extends AbstractTermCollection
    implements \mopsyd\sanctity\interfaces\libs\wordpress\container\TagContainerInterface
{
    /**
     * Child classes of this abstract should always return
     * an interface that extends the TermContainerInterface.
     *
     * @return string
     */
    public function getExpectedContainerType()
    {
        return 'mopsyd\\sanctity\\interfaces\\libs\\wordpress\\container\\TagContainerInterface';
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "tag"
     */
    public function getSubjectKey()
    {
        return 'tag';
    }
}
