<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\container;

/**
 * Abstract Term Container
 *
 * This is a container for data from a Wordpress Term.
 * This container constitutes the set of data extracted from the term,
 * representing its role and origin.
 *
 * As numerous aspects of Wordpress are represented by terms, this abstraction
 * serves as a base level contract that allows all extensions to be
 * interchangeably handled as term containers. More specialized term
 * containers (eg menus, categories, tags, etc) should extend from this
 * abstraction to automatically inherit compliance with the term interface
 * without further effort.
 *
 * This is used by the Wordpress Adapter to standardize the way that
 * data extracted from a term object is handled, so it can be presented
 * in a uniform way, and separates the data into a safe read only set
 * that does not expose any underlying methods that can perform write operations.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractTermContainer
    extends AbstractWordpressContainer
    implements \mopsyd\sanctity\interfaces\libs\wordpress\container\TermContainerInterface
{
    /**
     * Returns the identifying key of the worker.
     *
     * @return string "term"
     */
    public function getSubjectKey()
    {
        return 'term';
    }
}
