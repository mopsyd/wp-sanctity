<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\container;

/**
 * Abstract Post Container
 *
 * This is an abstraction layer for dealing with Wordpress posts.
 * As almost everything in Wordpress is a post,
 * this is a fundamental underlying construct for streamlining
 * the output data appropriately, and presents the basis of
 * containerization of 95% of the data received from Wordpress.
 *
 * --------
 *
 * This container represents the values of a post decoupled from the actual
 * `WP_Post` object, so that mutations and manipulation do not actually affect
 * any concrete properties of Wordpress data integrity. These objects are
 * packaged and distributed throughout Sanctity, which can be used as a
 * reference point to create a new instance of `WP_Post` to work with if
 * you need to actually alter a post directly. For simple read operations
 * and applying stateless filtering, this is an ideal solution,
 * as it removes any chance of accidentally causing a data persistence bug
 * in the data layer. Alterations to any data within an instance of this
 * container are entirely encapsulated within the container,
 * and do not propogate elsewhere.
 *
 * --------
 *
 * This is used by the Wordpress Adapter to standardize the way that
 * data extracted from a post object is handled, so it can be presented
 * in a uniform way. Individual dedicated containers for various types extend
 * from this container, and present additional criteria for customizing a
 * specific post types output dataset. This abstraction covers each
 * individual property received from an instance of WP_Post,
 * and applies them through default internal filters that can be extended
 * by child class logic to achieve specialized results.
 *
 * --------
 *
 * As this class extends from the base Container class,
 * it is directly iterable and usable within a template
 * without the templating layer being required to know what
 * is going on under the hood, which in turn creates a
 * layer of separation between handling content output appropriately,
 * which is a backend concern, and displaying presentation layer level output,
 * which is a templating concern.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractPostContainer
    extends \mopsyd\sanctity\libs\wordpress\container\AbstractWordpressContainer
    implements \mopsyd\sanctity\interfaces\libs\wordpress\container\PostContainerInterface
{

    /**
     * Allows for keys to be aliased for more human readable representation,
     * without disrupting the expected keys from the actual post object.
     *
     * @var array
     */
    private static $key_alias_map = array(
        'comment_count' => array(
            'comment_count' ),
        'comment_status' => array(
            'comment_status' ),
        'filter' => array(
            'filter' ),
        'guid' => array(
            'guid' ),
        'ID' => array(
            'ID',
            'id' ),
        'menu_order' => array(
            'menu_order' ),
        'pinged' => array(
            'pinged' ),
        'ping_status' => array(
            'ping_status' ),
        'post_author' => array(
            'post_author',
            'author' ),
        'post_content' => array(
            'post_content' ),
        'post_content_filtered' => array(
            'post_content_filtered',
            'filtered_content' ),
        'post_date' => array(
            'post_date',
            'date_published' ),
        'post_date_gmt' => array(
            'post_date_gmt',
            'date_published_gmt' ),
        'post_exerpt' => array(
            'post_exerpt',
            'exerpt' ),
        'post_name' => array(
            'post_name',
            'name',
            'slug' ),
        'post_parent' => array(
            'post_parent',
            'parent' ),
        'post_password' => array(
            'post_password',
            'password' ),
        'post_status' => array(
            'post_status',
            'status' ),
        'post_title' => array(
            'post_title',
            'title' ),
        'post_type' => array(
            'post_type',
            'type' ),
        'to_ping' => array(
            'to_ping' ),
    );

    /**
     * Represents a list of container classes that have already undergone
     * their setup process, so this will be skipped on subsequent instantiations.
     * @var array
     */
    private static $containers_instantiated = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post"
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * <Post Container Object Instantiation Trigger Method>
     * This method triggers from the constructor of the container. It allows any
     * instantiation parameters to be handled prior to following through with
     * instantiation. The return value is ignored.
     *
     * This method is used to configure aliasing and other Wordpress
     * specific supporting logic. This process is used to avoid any need
     * to override the actual constructor.
     *
     * All arguments given to the constructor are passed into the
     * first parameter as an array, which allows a container to receive
     * any number of arguments without redeclaring its constructor.
     *
     * @param array $args The arguments to be passed into the constructor.
     * @param string $method The internal method that the operation arose from.
     *     This function will not fire if it does not receive this argument
     *     as itself or the constructor.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the adapter has not properly initialized the class prior to use.
     *     Many of the internals require the adapter to have been properly passed
     *     through static dependency injection prior to usage.
     *     If this has not occurred, internals will break, generate errors,
     *     or cause erratic behavior.
     */
    protected function _onContainerInstantiation( $args, $method = null )
    {
        parent::_onContainerInstantiation( $args, $method );
        $this->_registerAliases();
    }

    /**
     * <Post Container Size Setter Trigger Method>
     * Prevents reducing the size to a depth lower than the required number of fields
     * to house a valid set of WP_Post values. The size may be set to any number of
     * fields greater than this, or -1 (unlimited).
     *
     * @param int $size The size parameter passed, indicating how large the
     *     maximum size of the resulting container should be.
     * @param string $method The internal method that the operation arose from.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\ContainerException
     *     If an attempt is made to set the size to a lower number of values
     *     than a valid post requires, which would break internals
     *     if it were allowed to proceed.
     * @note You may limit the size of a post container to any value greater than
     *     or equal to the number of typical `WP_Post` properties, and may use
     *     this to truncate out custom data if you wish. Truncation occurs in
     *     the order of setting, and implementing Sanctity logic always sets
     *     the core fields first prior to setting any custom data to facilitate
     *     this without additional considerations within this class logic.
     *     If you are not using the provided Sanctity api, you may encounter
     *     issues with this if your logic is malformed.
     */
    protected function _onSetSize( $size, $method = null )
    {
        if ( !($size === -1 || $size >= count( self::$key_alias_map ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Cannot reduce a post container\'s size to less than '
                . 'the size of the required post result values in [%1$s]',
                get_class( $this ) ) );
        }
    }

    /**
     * <Post Container Key Filter>
     * Enables key aliasing for get operations,
     * while maintaining the underlying data integrity.
     *
     * The underlying container logic references this method to obtain the real
     * key of a property in it's iterative set. This logic allows for aliasing
     * of those properties for a more readable display.
     *
     * @param string $key
     * @param string $method
     * @return string
     */
    protected function _filterKey( $key, $method = null )
    {
        return $this->_getRealKey( $key );
    }

    /**
     * This method can be overridden to provide additional aliases for
     * various fields within a WP_Post value set, which can then be
     * used as identifying keys for get operations against the container.
     *
     * It should be noted that iteration will always return the standard key,
     * so these are better used for one-off checks. There is no mechanism to
     * set an aliasing priority within this class logic, or map a real key
     * back to a provided alias, so implementing logic should be prepared
     * to handle both, and know what their aliases mean in terms of a
     * real WP_Post field.
     *
     * This method must return an associative array, where the key is a
     * real WP_Post property, and the value is an array of aliases to provide.
     * Aliases may not be unset once they are set, and they will be rejected
     * if they collide with aliases that are already set to prevent ambiguity
     * in get operations.
     *
     * @example: To be able to use "hard_link" as an alias for "guid", this should return:
     *     `return array( 'guid' => array( 'hard_link' ) );`
     *
     * Then you can do:
     *
     * `$object->has('hard_link') //true`
     * `$object->get('hard_link') //returns guid`
     * `$object->get('hard_link') === $object->get('guid') //true`
     *
     * The same is true for any other key.
     *
     * Note that when you use:
     *
     * `foreach ($object as $key => $value ) ...`
     *
     * The value of `$key` will still return `'guid'` instead of `'hard_link'`
     *
     * If this is problematic, then maintain a seperate list of aliases
     * to iterate over and use `$object->get` to retrieve them
     * instead of iterating over the container directly.
     *
     * @note Some aliases are already built in for standardization purposes,
     *     which cannot be overridden in child class logic.
     *     The following aliases are considered reserved:
     *     **['id', 'slug', 'name', 'author', 'raw', 'content',
     *     'filtered_content', date_published', 'date_published_gmt',
     *     'exerpt', 'parent', 'password', 'status', 'title', and 'type']**
     *     Attempting to register any of these will raise an exception
     *     that your class is misconfigured.
     *
     * @note If you are extending multiple levels of depth of a container,
     *     you do not want to return `parent::declareAliases` in your result set,
     *     or you will likely get a duplicate alias exception.
     *
     * @return array
     */
    protected function declareAliases()
    {
        return array();
    }

    /**
     * Filters the "comment_count" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterCommentCount( $count )
    {
        return $count;
    }

    /**
     * Filters the "comment_status" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterCommentStatus( $status )
    {
        return $status;
    }

    /**
     * Filters the "filter" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterFilterType( $filter )
    {
        return $filter;
    }

    /**
     * Filters the "guid" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterGuid( $guid )
    {
        return $guid;
    }

    /**
     * Filters the "ID" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterId( $id )
    {
        return $id;
    }

    /**
     * Filters the "menu_order" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterMenuOrder( $order )
    {
        return $order;
    }

    /**
     * Filters the "pinged" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterPinged( $pinged )
    {
        return $pinged;
    }

    /**
     * Filters the "ping_status" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterPingStatus( $status )
    {
        return $status;
    }

    /**
     * Filters the "post_author" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterAuthor( $author )
    {
        return $author;
    }

    /**
     * Filters the "post_content" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterContentRaw( $content )
    {
        return $content;
    }

    /**
     * Filters the "post_content_filtered" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterContentFiltered( $content )
    {
        return $content;
    }

    /**
     * Filters the "post_date" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterPostDate( $date )
    {
        return $date;
    }

    /**
     * Filters the "post_date_gmt" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterPostDateGmt( $date )
    {
        return $date;
    }

    /**
     * Filters the "post_exerpt" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterExerpt( $exerpt )
    {
        return $exerpt;
    }

    /**
     * Filters the "post_mime_type" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterMime( $mime )
    {
        return $mime;
    }

    /**
     * Filters the "post_modified" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterModifiedDate( $date )
    {
        return $date;
    }

    /**
     * Filters the "post_modified_gmt" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterModifiedDateGmt( $date )
    {
        return $date;
    }

    /**
     * Filters the "post_name" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterName( $name )
    {
        return $name;
    }

    /**
     * Filters the "post_parent" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterParentId( $parent_id )
    {
        return $parent_id;
    }

    /**
     * Filters the "post_password" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterPassword( $password )
    {
        return $password;
    }

    /**
     * Filters the "post_status" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterStatus( $status )
    {
        return $status;
    }

    /**
     * Filters the "post_title" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterTitle( $title )
    {
        return $title;
    }

    /**
     * Filters the "post_type" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterType( $type )
    {
        return $type;
    }

    /**
     * Filters the "to_ping" property of a WP_Post response.
     *
     * @param type $to_ping
     * @return type
     */
    protected function filterToPing( $to_ping )
    {
        return $to_ping;
    }

    /**
     * A filter for any non-standard custom data bound to the post type.
     *
     * Anything within child class logic that does not use a standard key
     * from WP_Post should bind their filter to this method and aggregate
     * further for their specific use case.
     *
     * @param mixed $data
     * @return mixed
     */
    protected function filterCustomDataType( $data )
    {
        return $data;
    }

    /**
     * Filters the wordpress content field value by type.
     *
     * @param string $key The key from WP_Post
     * @param mixed $value The value from WP_Post
     * @return mixed
     * @internal
     */
    private function _filterWordpressPostKeyData( $key, $value )
    {
        $result = $value;
        d( $key );
        switch ( $key )
        {
            case 'comment_count':
                $result = $this->filterCommentCount( $value );
                break;
            case 'comment_status':
                $result = $this->filterCommentStatus( $value );
                break;
            case 'filter':
                $result = $this->filterFilterType( $value );
                break;
            case 'guid':
                $result = $this->filterGuid( $value );
                break;
            case 'ID':
            case 'id':
                $result = $this->filterId( $value );
                break;
            case 'menu_order':
                $result = $this->filterMenuOrder( $value );
                break;
            case 'pinged':
                $result = $this->filterPinged( $value );
                break;
            case 'ping_status':
                $result = $this->filterPingStatus( $value );
                break;
            case 'post_author':
            case 'author':
                $result = $this->filterAuthor( $value );
                break;
            case 'post_content':
            case 'raw':
                $result = $this->filterContentRaw( $value );
                break;
            case 'post_content_filtered':
            case 'filtered_content':
            case 'content':
                $result = $this->filterContentFiltered( $value );
                break;
            case 'post_date':
            case 'date_published':
                $result = $this->filterPostDate( $value );
                break;
            case 'post_date_gmt':
            case 'date_published_gmt':
                $result = $this->filterPostDateGmt( $value );
                break;
            case 'post_exerpt':
            case 'exerpt':
                $result = $this->filterExerpt( $value );
                break;
            case 'post_name':
            case 'name':
            case 'slug':
                $result = $this->filterName( $value );
                break;
            case 'post_parent':
            case 'parent':
                $result = $this->filterParentId( $value );
                break;
            case 'post_password':
            case 'password':
                $result = $this->filterPassword( $value );
                break;
            case 'post_status':
            case 'status':
                $result = $this->filterStatus( $value );
                break;
            case 'post_title':
            case 'title':
                $result = $this->filterTitle( $value );
                break;
            case 'post_type':
            case 'type':
                $result = $this->filterType( $value );
                break;
            case 'to_ping':
                $result = $this->filterToPing( $value );
                break;
            default:
                $result = $this->filterCustomData( $value );
                break;
        }
        return $result;
    }

    private function _getRealKey( $key )
    {
        if ( !in_array( $key, array_keys( self::$key_alias_map ) ) )
        {
            return $this->_lookupKeyByAlias( $key );
        }
        return $key;
    }

    private function _lookupKeyByAlias( $key )
    {
        foreach ( self::$key_alias_map as
            $real_key =>
            $aliases )
        {
            if ( in_array( $key, $aliases ) )
            {
                return $real_key;
            }
        }
        return $key;
    }

    private function _registerAliases()
    {
        if ( in_array( get_class( $this ), self::$containers_instantiated ) )
        {
            //Do not double-register aliases.
            //Class alias registration must be unique.
            return;
        }
        foreach ( $this->declareAliases() as
            $real_key =>
            $alias_set )
        {
            if ( !is_array( $alias_set ) )
            {
                //Only arrays may be used for declaration of aliases.
                throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
                sprintf( 'Instance of [%1$s] is misconfigured. '
                    . 'Declared alias set for post field [%2$s] '
                    . 'must be an array. Recieved [%3$s]', get_class( $this ),
                    $real_key, gettype( $alias_set ) )
                );
            }
            foreach ( $alias_set as
                $alias )
            {
                if ( in_array( $alias, $this->_getCurrentAliases() ) )
                {
                    //Aliases may not be duplicates of existing aliases.
                    $this->debugStash( array(
                        'class' => get_class( $this ),
                        'current_aliases' => self::$key_alias_map,
                        'misconfigured_alias' => $alias,
                        'target_key' => $real_key,
                        ), 'initialization', 'duplicate_alias_detected' );
                    throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
                    sprintf( 'Instance of [%1$s] is misconfigured. '
                        . 'Declared alias [%2$s] for post field [%3$s] '
                        . 'has already been declared.', get_class( $this ),
                        $alias, $real_key )
                    );
                }
            }
            $this->_appendAliases( $real_key, $alias_set );
        }
        $this->debugStash( array(
            'class' => get_class( $this ),
            'current_aliases' => self::$key_alias_map
            ), 'initialization', 'key_aliases_registered' );
        self::$containers_instantiated[] = get_class( $this );
    }

    private function _appendAliases( $real_key, array $alias_set )
    {
        $existing = array();
        if ( array_key_exists( $real_key, self::$key_alias_map ) )
        {
            $existing = self::$key_alias_map[$real_key];
        }
        self::$key_alias_map[$real_key] = array_merge( self::$key_alias_map[$real_key],
            $alias_set );
    }

    private function _getCurrentAliases()
    {
        $aliases = array();
        foreach ( self::$key_alias_map as
            $real_key =>
            $alias_set )
        {
            foreach ( $alias_set as
                $alias )
            {
                if ( in_array( $alias, $aliases ) )
                {
                    //This exception only happens if our baseline aliases are borked
                    //or we did not properly preflight additions,
                    //which means that the core logic is faulty and we need to own it.
                    $this->debugStash( array(
                        'class' => __CLASS__,
                        'aliases' => self::$key_alias_map
                        ), 'initialization', 'core_alias_preflight_failed' );
                    throw new \mopsyd\sanctity\libs\exception\SanctityFailureException(
                    sprintf( 'Core class [%1$s] is misconfigured. '
                        . 'Supplied alias [%2$s] has duplicate entries '
                        . 'and is an ambiguous reference. '
                        . 'Please report this issue to the Sanctity team '
                        . 'for a patch.', get_class( $this ), $alias )
                    );
                }
                $aliases[] = $alias;
            }
        }
        return $aliases;
    }

}
