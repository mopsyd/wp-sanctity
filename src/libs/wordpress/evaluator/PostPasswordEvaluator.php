<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\evaluator;

/**
 * Post Password Evaluator
 *
 * Evaluates an instance of WP_Query,
 * and determines whether the subject currently requires a password.
 *
 * This will return true only if a password is currently required,
 * and will return false if it has already been entered,
 * or if no password support is enabled for the post.
 *
 * @note This does not check other visibility settings. That is checked separately.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class PostPasswordEvaluator
    extends AbstractEvaluator
{

    private $query;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "visibility"
     */
    public function getSubjectKey()
    {
        return 'post-password';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query".
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    public function evaluate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->query = $subject;
        return $this->_getPasswordStatus();
    }

    public function reset()
    {
        $this->query = null;
    }

    /**
     * Visibility Check Method
     *
     * Returns a determination as to whether the current user can view
     * the subject of the provided query.
     *
     * @return bool.
     * @internal
     */
    private function _getPasswordStatus()
    {
        return $this->_getSubjectPasswordStatus();
    }

    /**
     * Fetches the current password status of the subject.
     * @return string
     * @internal
     */
    private function _getSubjectPasswordStatus()
    {
        return post_password_required( $this->query->queried_object_id );
    }
}
