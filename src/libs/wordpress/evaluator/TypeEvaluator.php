<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\evaluator;

/**
 * Type Evaluator
 *
 * Evaluates an instance of WP_Query,
 * and determines what page type it is in regards to Sanctity page types.
 *
 * Sanctity honors the standard Wordpress Post types,
 * and has a couple of internal classifications as well
 * to clarify typical ambiguity,
 * such as whether or not the home page is also the blogroll,
 * and other points of typical confusion.
 *
 * These internal classifications are not actual post types,
 * but are used by the theme to make a straightforward,
 * user-friendly determination about the load strategy
 * that should be used to generate page content.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class TypeEvaluator
    extends AbstractEvaluator
{

    const DEFAULT_PAGE_TYPE = 'custom';

    /**
     * Represents internal class methods used to make an absolute determination
     * about the request type. These fire from specific to general,
     * and have to be accomplished in a specific order to maintain accuracy,
     * because the wordpress internals for determining the page type
     * are nebulous at best.
     *
     * As a workaround for this, the very explicit cases are checked first,
     * and then constraints are loosened gradually toward more generalized
     * page view types.
     *
     * This approach can make an accurate designation about page types even if
     * they are not officially posts (such as AJAX endpoints),
     * but only in the case where the evaluator is evaluating
     * the current page view.
     *
     * These MUST fire in this exact order to work correctly.
     *
     * All indeterminate types are 404s.
     *
     * @var array
     */
    private static $type_evaluators = array(
        'login' => '_loginPageCheck',
        'ajax' => '_ajaxCheck',
        'admin-theme-index' => '_themeIndexCheck',
        'admin-media-upload' => '_mediaUploadCheck',
        'admin-media-editor' => '_mediaEditorCheck',
        'admin-tag-editor' => '_tagEditorCheck',
        'admin-category-editor' => '_categoryEditorCheck',
        'admin-customize' => '_customizerCheck',
        'admin-comment-editor' => '_commentEditorCheck',
        'admin-file-editor' => '_fileEditorCheck',
        'admin-menu-editor' => '_menuEditorCheck',
        'admin-widget-editor' => '_widgetEditorCheck',
        'admin-editor' => '_editorCheck',
        'admin-feedback-index' => '_feedbackCheck',
        'admin-update' => '_updateCheck',
        'admin-comment-index' => '_commentIndexCheck',
        'admin-page-index' => '_pageIndexCheck',
        'admin-post-index' => '_postIndexCheck',
        'admin-plugin-index' => '_pluginIndexCheck',
        'admin-plugin-selector' => '_pluginSelectorCheck',
        'admin-plugin-editor' => '_pluginEditorCheck',
        'admin-plugin-settings' => '_pluginSettingsPageCheck',
        'admin-user-index' => '_userIndexCheck',
        'admin-user-editor' => '_userEditorCheck',
        'admin-about' => '_aboutCheck',
        'admin-about-credits' => '_aboutCreditsCheck',
        'admin-about-license' => '_aboutLicenseCheck',
        'admin-about-privacy' => '_aboutPrivacyCheck',
        'admin-tools' => '_toolsCheck',
        'admin-import' => '_importCheck',
        'admin-export' => '_exportCheck',
        'admin-settings-custom' => '_settingsCustomCheck',
        'admin-settings' => '_settingsCheck',
        'admin-settings-reading' => '_settingsReadingCheck',
        'admin-settings-writing' => '_settingsWritingCheck',
        'admin-settings-discussion' => '_settingsDiscussionCheck',
        'admin-settings-media' => '_settingsMediaCheck',
        'admin-settings-permalinks' => '_settingsPermalinksCheck',
        'admin-page' => '_adminPageCheck',
        'admin-dashboard' => '_adminDashboardCheck',
        'admin' => '_adminCheck',
        404 => '_errorCheck',
        'attachment' => '_attachmentCheck',
        'index' => '_indexCheck',
        'blog' => '_blogCheck',
        'search' => '_searchCheck',
        'author' => '_authorCheck',
        'tag' => '_tagCheck',
        'category' => '_categoryCheck',
        'archive' => '_archiveCheck',
        'robots' => '_robotsCheck',
        'custom' => '_customCheck',
        'feed' => '_feedCheck',
        'page' => '_pageCheck',
        'post' => '_postCheck',
    );
    private $query;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "type"
     */
    public function getSubjectKey()
    {
        return 'type';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query".
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    public function evaluate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->query = $subject;
        $type = $this->_getType();
        if ( !in_array( $type,
                array(
                'admin-about',
                'admin-about-credits',
                'admin-about-license',
                'admin-about-privacy',
                'admin-media-upload',
                'admin-media-editor',
                'admin-tag-editor',
                'admin-category-editor',
                'admin-comment-editor',
                'admin-file-editor',
                'admin-menu-editor',
                'admin-widget-editor',
                'admin-customize',
                'admin-editor',
                'admin-update',
                'admin-theme-index',
                'admin-comment-index',
                'admin-page-index',
                'admin-post-index',
                'admin-feedback-index',
                'admin-plugin-index',
                'admin-plugin-selector',
                'admin-plugin-editor',
                'admin-plugin-settings',
                'admin-user-index',
                'admin-user-editor',
                'admin-tools',
                'admin-import',
                'admin-export',
                'admin-settings',
                'admin-settings-custom',
                'admin-settings-discussion',
                'admin-settings-media',
                'admin-settings-permalinks',
                'admin-settings-reading',
                'admin-settings-writing',
                'admin-page',
                'admin-dashboard',
                'admin',
                'post',
                'page',
                'search',
                404,
                'index',
                'blog',
                'category',
                'attachment',
                'archive',
                'tag',
                'author',
                'login' ) ) )
        {
            d( $subject, $this->_getType() );
            exit;
        }
        return $type;
    }

    public function reset()
    {
        $this->query = null;
    }

    /**
     * The unneccessarily cyclomatically complex method for finding the page type,
     *
     * Wordpress Ought To Standardize A Router Edition.
     *
     * @return int|string Returns 404 on any not found type,
     *     otherwise a string representation of the page type slug.
     * @internal
     */
    private function _getType()
    {
        $type = $this::DEFAULT_PAGE_TYPE;
        foreach ( self::$type_evaluators as
            $evaluate =>
            $method )
        {
            if ( $this->$method() )
            {
                $type = $evaluate;
                break;
            }
        }
        return $type;
    }

    /**
     * Securely and absolutely checks if the current page is the login page,
     * even if the URL has changed or some plugin is masking the typical address
     * with a rewrite. This is used to prevent the possibility of frontend render
     * code from being displayed on the login page, even if plugins have altered
     * the state of the uri, rewritten it, or otherwise attempted to obscure it,
     * which is common with poorly written security plugins that think that
     * you can make a site secure by hiding stuff from humans without considering
     * that machines will just scrape the site and find it anyways.
     *
     * Slightly formatted for clarity, but not otherwise modified.
     * Original solution author attributed below.
     *
     * @return bool
     * @internal
     *
     * @note This method is only used to prevent any chance of theme frontend
     *     code from displaying on the login page.
     *
     * @note Thanks to T.Todua on Wordpress Stack Exchange site for this
     *     absolutely excellent answer to this common problem.
     * @link https://wordpress.stackexchange.com/a/237285/60036
     */
    private function _loginPageCheck()
    {
        $ABSPATH_MY = str_replace( array(
            '\\',
            '/' ), DIRECTORY_SEPARATOR, ABSPATH );
        return (                                                                //From
            ( in_array( $ABSPATH_MY . 'wp-login.php', get_included_files() )    //specific
            || in_array( $ABSPATH_MY . 'wp-register.php', get_included_files() )//to
            )                                                                   //the
            || $GLOBALS['pagenow'] === 'wp-login.php'                           //general
            || $_SERVER['PHP_SELF'] == '/wp-login.php'                          //answer
            );
    }

    /**
     * Checks for ajax. This only works if we are investigating the current page.
     * Otherwise you're going to get an admin result if it is not the current page.
     *
     * @todo Figure out how to figure out if a different page is an ajax endpoint.
     *     Not that pressing, but still. Really don't like leaving any holes in the logic.
     *
     * @return bool
     * @internal
     */
    private function _ajaxCheck()
    {
        return $this->query->is_admin() && $this->query === self::$current_query &&
            wp_doing_ajax();
    }

    /**
     * Checks if this is the media upload view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _mediaUploadCheck()
    {
        return strpos( $this::REQUEST_URI, '/media-new.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the theme selector view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _themeIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/themes.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the file editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _fileEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/theme-editor.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the menu editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _menuEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/nav-menus.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the widget editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _widgetEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/widgets.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the media editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _mediaEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/upload.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the category editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _tagEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/edit-tags.php?taxonomy=post_tag' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the category editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _categoryEditorCheck()
    {
        return strpos( $this::REQUEST_URI,
                '/wp-admin/edit-tags.php?taxonomy=category' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the customizer view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _customizerCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/customize.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a page editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _editorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/post.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a feedback view, for responding to contact form submissions.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _feedbackCheck()
    {
        return strpos( $this::REQUEST_URI,
                '/wp-admin/edit.php?post_type=feedback' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a updater view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _updateCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/update-core.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a comment editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _commentEditorCheck()
    {
        return strpos( $this::REQUEST_URI,
                '/wp-admin/comment.php?action=editcomment' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a comment index view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _commentIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/edit-comments.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a page index view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _pageIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/edit.php?post_type=page' )
            !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a post index view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _postIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/edit.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a plugin index view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _pluginIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/plugins.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a plugin custom settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _pluginSettingsPageCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/admin.php?page=' ) !== false
            && ( $this::REQUEST_METHOD === 'get' || $this::REQUEST_METHOD === 'post' )
            // Wordpress apparently doesn't recognize a post request as still being admin
            // when it's a post request to admin. Because reasons.
            && ($this->query->is_admin || ( $this::REQUEST_METHOD === 'post' && !$this->query->is_admin ) )
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a plugin selection view, for adding additional plugins.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _pluginSelectorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/plugin-install.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a plugin editor view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _pluginEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/plugin-editor.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a user index view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _userIndexCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/users.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a user edit/profile view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _userEditorCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/profile.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a "about WordPress" view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _aboutCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/about.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a credits page for the WordPress dev team.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _aboutCreditsCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/credits.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a privacy policy disclosure page for the WordPress system.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _aboutPrivacyCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/privacy.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a license page for the WordPress GPL disclosure
     * (commonly called the "freedoms" page under the about section,
     * but it's just a license disclosure).
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _aboutLicenseCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/freedoms.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is an admin tools view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _toolsCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/tools.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is an admin importer tool view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _importCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/import.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is an admin exporter tool view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _exportCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/export.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a custom site settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsCustomCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-general.php?page=' )
            !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a general site settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-general.php' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a writing settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsWritingCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-writing.php' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a discussion settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsDiscussionCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-discussion.php' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a discussion settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsMediaCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-media.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a permalink settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsPermalinksCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-permalink.php' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a reading settings view.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _settingsReadingCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/options-reading.php' ) !==
            false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is a custom admin page added by a theme or plugin.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _adminPageCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/admin.php?page=' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && !$this->query->is_404;
    }

    /**
     * Checks if this is the main admin dashboard.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _adminDashboardCheck()
    {
        return strpos( $this::REQUEST_URI, '/wp-admin/index.php' ) !== false
            && $this::REQUEST_METHOD === 'get'
            && $this->query->is_admin
            && !$this->query->is_404;
    }

    /**
     * Checks if this is an admin page.
     *
     * This works as long as it fires in the correct order,
     * because there are other types that return true on "is_admin",
     * but have more specific conditions that should also be evaulated.
     *
     * @return bool
     * @internal
     */
    private function _adminCheck()
    {
        return $this->query->is_admin && !$this->query->is_404;
    }

    /**
     * Checks if the page is not found.
     *
     * @return bool
     * @internal
     */
    private function _errorCheck()
    {
        return $this->query->is_404;
    }

    /**
     * Checks if the page is a preview.
     * This should trigger additional edge case logic,
     * because a preview can be any "type" that is a valid frontend page.
     * In that case, a lookup by the actual query is in order,
     * or some manner of flag that can tell this class to skip this one.
     *
     * @return bool
     * @internal
     */
    private function _previewCheck()
    {
        return $this->query->is_preview() && !$this->query->is_404;
    }

    /**
     * Someone on the Wordpress team thought it would be a great idea
     * to make the index page check method return true even if its not
     * a homepage, and the homepage method return true if it's not the index.
     *
     * Because reasons.
     *
     * So because of whatever these reasons are, Sanctity assigns
     * a page type of "index" to a front page that is also a homepage,
     * so the api can use one simple check to determine this,
     * and you don't need to spend four hours on google figuring out
     * the right order of the four different if/elseif's you need
     * to get it right. Otherwise "home" is a non-frontpage homepage,
     * and "blog" is still a normal blog, front page or otherwise.
     *
     * @return bool
     * @internal
     */
    private function _indexCheck()
    {
        return is_front_page() && !is_home();
    }

    /**
     * Aaaaand the blog page check is also the same method
     * you use to check for a homepage.
     *
     * Of course it is.
     *
     * Because reasons.
     *
     * @return bool
     * @internal
     */
    private function _blogCheck()
    {
        return is_home();
    }

    /**
     * A normal search is a search. A category or a tag search are not a search.
     *
     * Because reasons.
     *
     * @return bool
     * @internal
     */
    private function _searchCheck()
    {
        return $this->query->is_search() && !$this->query->is_404;
    }

    /**
     * The default behavior of the author archive is to just show the posts
     * for a specific author, but since they still let you style it by putting
     * an "author.php" page in your theme, we can leverage that to make a
     * pretty sweet user bio page out if it if the site owner
     * configures it to do that.
     *
     * Otherwise they just get a slightly more personalized archive page result.
     *
     * @return bool
     * @internal
     */
    private function _authorCheck()
    {
        return $this->query->is_archive()
            && $this->query->is_author()
            && !$this->query->is_404;
    }

    /**
     * This is the part where we can check against all those
     * sweet custom post types everyone loves.
     *
     * @return bool
     * @internal
     * @todo Register some of the more common custom post types,
     *     like portfolio, product, etc, and eventually
     *     add an api for binding any of them.
     */
    private function _customCheck()
    {
        return $this->query->is_tax() && !$this->query->is_404;
    }

    /**
     * What is a man.
     * If his chief good and market of his time.
     * Be but to sleep and feed?
     *
     * @return bool
     * @internal
     */
    private function _feedCheck()
    {
        return $this->query->is_comment_feed() && !$this->query->is_404;
    }

    /**
     * I have a dream that my little codebase will one day live in a repo
     * where it will not be judged by the catchiness of its name,
     * but by the content of its api.
     *
     * @return bool
     * @internal
     */
    private function _categoryCheck()
    {
        return $this->query->is_category() && !$this->query->is_404;
    }

    /**
     * You're it.
     *
     * @return bool
     * @internal
     */
    private function _tagCheck()
    {
        return $this->query->is_tag() && !$this->query->is_404;
    }

    /**
     * Butterfly in the sky, I can fly twice as high.
     *
     * @return bool
     * @internal
     */
    private function _archiveCheck()
    {
        return $this->query->is_archive() && !$this->query->is_404;
    }

    /**
     * I'm looking for John Connor.
     *
     * @return bool
     * @internal
     */
    private function _robotsCheck()
    {
        return $this->query->is_attachment() && !$this->query->is_404;
    }

    /**
     * A lawyer with his briefcase can steal more than a hundred men with guns.
     *
     * @return bool
     * @internal
     */
    private function _attachmentCheck()
    {
        return (
            ( $this->query->is_attachment() && !$this->query->is_404 )
            || ( $this->query->is_singular
            && property_exists( $this->query, 'post_type' )
            && $this->query->post->post_type === "attachment" )
            );
    }

    /**
     * Tabula Rasa.
     *
     * @return bool
     * @internal
     */
    private function _pageCheck()
    {
        return $this->query->is_page() && !$this->query->is_404;
    }

    /**
     * Write drunk, edit sober.
     *
     * @return bool
     * @internal
     */
    private function _postCheck()
    {
        return $this->query->is_single() && !$this->query->is_404;
    }

}
