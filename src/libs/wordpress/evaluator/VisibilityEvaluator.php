<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\evaluator;

/**
 * Visibility Evaluator
 *
 * Evaluates an instance of WP_Query,
 * and determines whether the subject has current visibility.
 *
 * Resolves common edge cases so that the determination
 * is accurate for the current viewer regardless.
 *
 * @note This does not restrict passworded pages. That is checked separately.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class VisibilityEvaluator
    extends AbstractEvaluator
{

    private $query;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "visibility"
     */
    public function getSubjectKey()
    {
        return 'visibility';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query".
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    public function evaluate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->query = $subject;
        return $this->_getVisibility();
    }

    public function reset()
    {
        $this->query = null;
    }

    /**
     * Visibility Check Method
     *
     * Returns a determination as to whether the current user can view
     * the subject of the provided query.
     *
     * @return bool.
     * @internal
     */
    private function _getVisibility()
    {
        $status = $this->_getSubjectStatus();
        //Checks if the page is published publicly
        if ( $status === 'publish' )
        {
            return true;
        }
        //Checks if the current viewer can view hidden pages if it's a page,
        //or view hidden posts otherwise.
        if ( $this->_getUserAccessPermissions() )
        {
            return true;
        }
        //Checks if the current viewer owns the page
        if ( $this->_resolvePostOwnershipAccess() )
        {
            return true;
        }
        return false;
    }

    /**
     * Fetches the current status of the subject.
     * @return string
     * @internal
     */
    private function _getSubjectStatus()
    {
        return get_post_status( $this->query->queried_object_id );
    }

    /**
     * Returns whether or not the current viewer has permission
     * to view hidden or passworded posts/pages (whichever is applicable)
     * without restriction.
     *
     * @return bool
     * @internal
     */
    private function _getUserAccessPermissions()
    {
        return (
            ( current_user_can( 'read_private_pages' ) //covers pages
            && in_array( $this::getAdapter()->evaluate( 'type', $this->query ),
                array(
                'index',
                'page' ) ) )
            || ( current_user_can( 'read_private_posts' ) //covers everything else
            && !in_array( $this::getAdapter()->evaluate( 'type', $this->query ),
                array(
                'index',
                'page' ) ) )
            );
    }

    /**
     * Returns whether the current subject belongs to the current user,
     * and the current user has access rights to edit their own posts.
     *
     * @return bool
     * @internal
     */
    private function _resolvePostOwnershipAccess()
    {
        return wp_get_current_user()->ID === (int) get_post_field( 'post_author',
                $this->query->queried_object_id );
    }

}
