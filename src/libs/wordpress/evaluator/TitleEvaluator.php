<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\evaluator;

/**
 * Title Evaluator
 *
 * Evaluates an instance of WP_Query,
 * and determines what the page title should be...
 *
 * ...Even if the page doesn't have a title, or isn't even a post.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class TitleEvaluator
    extends AbstractEvaluator
{

    private $query;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "title"
     */
    public function getSubjectKey()
    {
        return 'title';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query".
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    public function evaluate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->query = $subject;
        $title = $this->_buildRawTitle();
        return __( $this->_buildRawTitle() );
    }

    public function reset()
    {
        $this->query = null;
    }

    /**
     * Visibility Check Method
     *
     * Returns a determination as to whether the current user can view
     * the subject of the provided query.
     *
     * Awful nice that we got that page type evaluator thing done eh?
     * That makes this much easier.
     *
     * @return bool.
     * @internal
     */
    private function _buildRawTitle()
    {
        $title = null;
        //This will get replaced with a dynamic preference driven setup in the near future.
        switch ( $this::getAdapter()->evaluate( 'type', $this->query ) )
        {
            case 'index':                                       //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is the front page with a static page set that is not a blog page.
                //Use the site title.
                $title = $this->_buildHomePageTitle();
                break;
            case 'page':                                        //Switch cases are ugly noob shit. I need my own method Mopsyd.
            //This is a page. Use the page title.
            case 'post':                                        //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is a post. Use the post title.
                $title = $this->_buildPageTitle();
                break;
            case 401:
            case 403:
            case 404:
            case 500:
                //This is an error page. Use the default error page message.
                $title = $this->_buildErrorPageTitle();
                break;
            case 'search':                                      //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is a search page. Use the search criteria.
                $title = $this->_buildSearchTitle();
                break;
            case 'archive':                                     //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is a archive page. Use the archive date.
                $title = $this->_buildArchiveTitle();
                break;
            case 'category':                                    //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is a category search page. Use the category name.
                $title = $this->_buildCategoryTitle();
                break;
            case 'tag':                                         //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is a tag search page. Use the tag name.
                $title = $this->_buildTagTitle();
                break;
            case 'author':                                      //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is an author page. Use the author name.
                $title = $this->_buildAuthorTitle();
                break;
            case 'blog':                                        //Switch cases are ugly noob shit. I need my own method Mopsyd.
                //This is the blogroll page. Use "Blog".
                return "Blog";
                $title = $this->_buildBlogPageTitle();
                break;
            case 'login':                                       //Switch cases are ugly noob shit. I need my own method Mopsyd.
            case 'admin':                                       //Switch cases are ugly noob shit. I need my own method Mopsyd.
            case 'ajax':                                        //Switch cases are ugly noob shit. I need my own method Mopsyd.
            case 'rest':                                        //Switch cases are ugly noob shit. I need my own method Mopsyd.
            case 'robots':                                      //Switch cases are ugly noob shit. I need my own method Mopsyd.
            case 'attachment':                                  //Switch cases are ugly noob shit. I need my own method Mopsyd.
            default:
                //Let Wordpress handle it.
                return false;
                break;
        }
        //Lets give back a completely clean title.
        //We'll run the encode filter on it once like
        //ought to normally happen when it goes to the view.
        return $this->_castToRaw( $title );
    }

    /**
     * Fetches the current status of the subject.
     *
     * @return string
     * @internal
     */
    private function _getSubjectStatus()
    {
        return get_post_status( $this->query->queried_object_id );
    }

    /**
     * Completely remove any and all html encoding, even if it's mixed up or
     * otherwise horribly mangled beyond normal repair.
     *
     * Wordpress loves to stash encoded stuff in the database,
     * and then chokes up on decoding it. It then often gets
     * mis-encoded or partial-double-encoded because plugins
     * also fuck with it and don't do it right
     * (some people are convinced that regex is the answer to everything,
     * when in reality it's barely the answer to anything).
     *
     * This fixes all of that nonsense.
     *
     * @param string $title
     * @return string
     */
    private function _castToRaw( $title )
    {
        while ( $title && $title !== html_entity_decode( $title, ENT_QUOTES ) )
        {
            //Strip it down even if it's layered 20 times over (or hopefully not more).
            //Clean Page titles only. It will get properly encoded
            //one time the right way before output.
            //Skips completely if the previous method returned false.
            //That doesn't need decoding.
            $title = html_entity_decode( $title, ENT_QUOTES );
        }
        return $title;
    }

    /**
     * Bring on the singularity!
     *
     * @return string
     * @internal
     */
    private function _getSingluarity()
    {

        return $this->query->is_singular;
    }

    private function _getErrorStatus()
    {
        return $this->query->is_404;
    }

    /**
     * This will get replaced with a dynamic preference driven setup in the near future.
     *
     * @param \WP_Query $query
     * @param string $status
     * @return string
     */
    private function _getVisibilityPrefix( $query, $status )
    {
        $prefix = null;
        switch ( $query->post_status )
        {
            case 'future':                  //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Scheduled: ';
                break;
            case 'draft':                   //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Draft: ';
                break;
            case 'pending':                 //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Pending: ';
                break;
            case 'private':                 //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Private: ';
                break;
            case 'trash':                   //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Trashed: ';
                break;
            case 'auto-draft':              //Switch cases are ugly noob shit. I need my own method Mopsyd.
                $prefix = 'Autosave: ';
                break;
            case 'inherit':                 //Switch cases are ugly noob shit. I need my own method Mopsyd.
                if ( $query->post_parent === 0
                    || $query->post_parent === -1
                    || !property_exists( $query, 'post_parent' )
                )
                {
                    //no parent
                    break;
                }
                $q = $this::getAdapter()->query( 'query',
                    array(
                    'p' => $query->post_parent,
                    'post_type' => 'any'
                    ) );
                if ( !property_exists( $q, 'post_status' ) )
                {
                    //Nothing to see here.
                    return;
                }
                return $this->_getVisibilityPrefix( $q, $q->post_status );
                break;
        }
        return $prefix;
    }

    //All of these will get updated with integrated options eventually.
    //Just need placeholders for the time being.

    /**
     * This will get replaced with a dynamic preference driven setup in the near future.
     * @return string
     */
    private function _buildAuthorTitle()
    {
        return $this::getAdapter()->parse( 'user',
                get_user_by( 'login', $this->query->query['author_name'] ) )->name;
    }

    /**
     * Builds a human-readable date title based on the values from the search query.
     * Missing elements are emulated to leverage PHP's date casting without
     * writing 2000 lines of code to utilize it.
     * @return string
     */
    private function _buildArchiveTitle()
    {
        $day = null;
        $month = null;
        $year = null;
        $prefix = 'Archive: ';
        if ( array_key_exists( 'day', $this->query->query )
            && array_key_exists( 'monthnum', $this->query->query )
            && array_key_exists( 'year', $this->query->query ) )
        {
            $raw_date = $this->query->query['year']
                . '-' . $this->query->query['monthnum']
                . '-' . $this->query->query['day'];
            $date = date( 'l, F j, Y', strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'day', $this->query->query )
            && array_key_exists( 'monthnum', $this->query->query ) )
        {
            $raw_date = '2018' // This forces the date to work the right way. Don't remove.
                . '-' . $this->query->query['monthnum']
                . '-' . $this->query->query['day'];
            $date = date( '\t\h\e jS \o\f F', strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'monthnum', $this->query->query )
            && array_key_exists( 'year', $this->query->query ) )
        {
            $raw_date = $this->query->query['year']
                . '-' . $this->query->query['monthnum']
                . '-' . '01'; // This forces the date to work the right way. Don't remove.
            $date = date( 'F, Y', strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'day', $this->query->query )
            && array_key_exists( 'year', $this->query->query ) )
        {
            $raw_date = $this->query->query['year']
                . '-' . '01' // This forces the date to work the right way. Don't remove.
                . '-' . $this->query->query['day'];
            $date = date( '\t\h\e jS \d\a\y \o\f \a\l\l \m\o\n\t\h\s \i\n Y',
                strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'day', $this->query->query ) )
        {
            $raw_date = '2018' // This forces the date to work the right way. Don't remove.
                . '-' . '01' // This forces the date to work the right way. Don't remove.
                . '-' . $this->query->query['day'];
            $date = date( '\t\h\e jS', strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'monthnum', $this->query->query ) )
        {
            $raw_date = '2018' // This forces the date to work the right way. Don't remove.
                . '-' . $this->query->query['monthnum']
                . '-' . '01'; // This forces the date to work the right way. Don't remove.
            $date = date( 'F', strtotime( $raw_date ) );
        } elseif ( array_key_exists( 'year', $this->query->query ) )
        {
            $date = $this->query->query['year'];
        }
        return $prefix . $date;
    }

    /**
     * Sets the title to the search query value.
     * @return string
     */
    private function _buildSearchTitle()
    {
        return 'Search results for ' . get_search_query();
    }

    /**
     * Sets the title to the tag name.
     * @return string
     */
    private function _buildTagTitle()
    {
        return 'Tag: ' . $this->query->queried_object->name;
    }

    /**
     * Sets the title to the category name.
     * @return string
     */
    private function _buildCategoryTitle()
    {
        return 'Category: ' . $this->query->queried_object->name;
    }

    /**
     * This will get replaced with a dynamic preference driven setup in the near future.
     * @return string
     */
    private function _buildErrorPageTitle()
    {
        return 'Page not found';
    }

    /**
     * This will get replaced with a dynamic preference driven setup in the near future.
     * @return string
     */
    private function _buildBlogPageTitle()
    {
        return 'Blog';
    }

    /**
     * Returns the post/page name, with a prefix if it's not public
     * that indicates its status at first glance.
     *
     * @return string
     * @internal
     */
    private function _buildPageTitle()
    {
        $visibility_prefix = $this->_getVisibilityPrefix( $this->query,
            $this->query->post_status );
        return $visibility_prefix . $this->query->post->post_title;
    }

    /**
     * Returns the site title for now.
     * The branding settings will give you options for this later.
     * @return string
     * @internal
     */
    private function _buildHomePageTitle()
    {
        return get_bloginfo( 'name' );
    }

}
