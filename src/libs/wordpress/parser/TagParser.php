<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Tag Parser
 *
 * Parses tags.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class TagParser
    extends AbstractTermParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\TagContainer';
    const DEFAULT_TERM_TYPE = 'term';

    /**
     * A static reference to the work queue used to package additional category
     * details into a category container object.
     *
     * This is kept statically to facilitate extension logic in the future
     * that resolves across instances.
     *
     * @var array
     */
    private static $work_queue = array(
        'uri' => 'tagUri',
    );

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "tag"
     */
    public function getSubjectKey()
    {
        return 'tag';
    }

    /**
     * Declares the work queue for details required by categories.
     *
     * @return array
     */
    protected function declareWorkQueue()
    {
        return self::$work_queue;
    }

    /**
     * Gets the Tag link for generating direct links to the category page.
     *
     * @param type $term
     * @param type $output
     * @return string
     */
    protected function tagUri( $term, $output )
    {
        return get_tag_link( $term->term_id );
    }

}
