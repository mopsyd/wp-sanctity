<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Role Parser
 *
 * Parses a user role, and returns a container with its details and capabilities.
 *
 * For individual shortcode parsing, use ShortcodeParser ( without the "s" )
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class RoleParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\RoleContainer';

    /**
     * Keeps repeated role requests nice and zippy fast by not reparsing them.
     *
     * Also prevents recursion bugs.
     *
     * Double win.
     *
     * @var array
     */
    private static $role_cache = array();
    private static $parse_actions = array(
        'name' => 'getRoleName',
        'display' => 'getRoleDisplayName',
        'permissions' => 'getRolePermissions',
    );

    /**
     * The current role object being parsed.
     *
     * @var \WP_Role
     */
    private $role;

    /**
     * Represents the working state of the parser prior to returning its results.
     * @var array
     */
    private $details = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "permission"
     */
    public function getSubjectKey()
    {
        return 'role';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Role".
     */
    public function getSubjectType()
    {
        return 'WP_Role';
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( array_key_exists( $subject->name, self::$role_cache ) )
        {
            return self::$role_cache[$subject->name];
        }
        $this->role = $subject;
        foreach ( self::$parse_actions as
            $key =>
            $method )
        {
            $this->details[$key] = $this->$method();
        }
        $result = $this::containerize( $this->details );
        self::$role_cache[$subject->name] = $result;
        return $result;
    }

    protected function getRoleName()
    {
        return $this->role->name;
    }

    protected function getRoleDisplayName()
    {
        return ucwords( $this->role->name );
    }

    protected function getRolePermissions()
    {
        return $this::getAdapter()->parse( 'permission', $this->role );
    }

    public function reset()
    {
        $this->role = null;
        $this->details = array();
        return $this;
    }

}
