<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Permission Parser
 *
 * Parses a set of permissions into a checkable container,
 * which designates whether they are enabled or disabled
 * in the supplied user scope.
 *
 * There is no capability object, so this is a pretty cut and dry process,
 * and it doesn't even really need any subtasks. Win.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class PermissionParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\PermissionContainer';

    /**
     * The current role object being parsed.
     *
     * @var \WP_Role
     */
    private $role;

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "permission"
     */
    public function getSubjectKey()
    {
        return 'permission';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Role".
     */
    public function getSubjectType()
    {
        return 'WP_Role';
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->role = $subject;
        $permissions = $this->role->capabilities;
        if ( !is_array( $permissions ) )
        {
            //In case this ever returns false or something in the future.
            $permissions = array();
        }
        $result = $this::containerize( $permissions );
        return $result;
    }

    public function reset()
    {
        $this->role = null;
        return $this;
    }

}
