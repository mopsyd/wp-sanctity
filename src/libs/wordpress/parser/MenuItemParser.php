<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Menu Item Parser
 *
 * Parses menu items, and returns a menu item container,
 * which is a specialized post container specifically
 * for menu items. This collects all of the details related to
 * the link as an independent menu item, and also collects an
 * equivalent container of info about its target if one can be found.
 * That can be iterated over just like the post containers from this
 * system normally are, so if you want window dressing for your
 * link menus like author, category, post title, exerpt,
 * or whatever else on hover, it's already packed in there
 * to use however you want. Pretty swell eh?
 *
 * Please note that this will aggressively reject menu items that are not,
 * in fact, menu items. If you have hacked in some kind of wonky thing
 * that tries to make menu items out of some other term, post,
 * or some other nonsense, this is going to bounce it
 * without a second thought immediately.
 * This system is designed to follow the Wordpress standards to the letter,
 * and if you are cutting corners it will give you a flat out NOPE.
 * It DOES NOT allow those things access to Wordpress,
 * it preemptively rejects EVERYTHING that would not appropriately gel
 * with Wordpress internals exactly as they are defined in the Wordpress dev docs,
 * and throws an exception BEFORE errors happen when it encounters anything like that.
 *
 * TLDR; You don't get to do the hack thing with this.
 * You either do it right or use something else.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class MenuItemParser
    extends AbstractPostParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\MenuItemContainer';

    /**
     * Used to avoid duplicate parsing.
     * If the id is identical to an entry in this set,
     * it just gets returned as is, and is not re-parsed.
     * This prevents recusive loop errors.
     *
     * @var array
     */
    private static $menu_item_cache = array();

    /**
     * Represents the current working menu item.
     *
     * Must have post type "nav_menu_item"
     *
     * @var \WP_Post
     */
    protected $post;

    /**
     * Represents the current working details set.
     *
     * @var \mopsyd\sanctity\libs\wordpress\container\MenuItemContainer
     */
    private $details;

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post"
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Post".
     */
    public function getSubjectType()
    {
        return 'WP_Post';
    }

    /**
     * Parses a nav menu item into a container.
     *
     * @param \WP_Post $subject Must have the "nav_menu_item" post type.
     * @return \mopsyd\sanctity\libs\wordpress\container\MenuItemContainer
     *
     * @note If you are using some screwball custom post type as a nav menu item,
     *     this will need to be overridden, or your menus will cause exceptions to arise.
     *     Use the Extension Api if that is the case, and reproduce only this class,
     *     and it will work fine.
     *
     * @see _typeCheckPostType If you are doing it wrong,
     *     this little guy is going to be a real pain in your ass.
     */
    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->_typeCheckPostType( $subject );
        if ( array_key_exists( $subject->ID, self::$menu_item_cache ) )
        {
            return self::$menu_item_cache[$subject->ID];
        }
        $this->details = parent::parse( $subject );
        $this->_parseMenuInfo();
        self::$menu_item_cache[$this->details->get('id')] = $this->details;
        return $this->details;
    }

    public function reset()
    {
        parent::reset();
        $this->post = null;
        $this->details = null;
        return $this;
    }

    /**
     * Gets the post menu order for the current subject
     *
     * @return bool|string
     * @internal
     */
    protected function getPostMenuOrder()
    {
        return $this->post->menu_order;
    }

    /**
     * Gets the title for the post.
     *
     * @return string Returns the title.
     * @internal
     */
    protected function getPostTitle()
    {
        return $this->post->title;
    }

    /**
     * Gets the current Sanctity post type for the post.
     *
     * @return string
     * @internal
     */
    protected function getPostType()
    {
        return 'menu-item';
    }

    /**
     * Links do not have content.
     * @return null
     */
    protected function getPostExerpt()
    {
        return null;
    }

    /**
     * Links do not have content.
     * @return null
     */
    protected function getPostContent()
    {
        return null;
    }

    /**
     * Links do not have content.
     * @return null
     */
    protected function getPostDebugContent()
    {
        return null;
    }

    /**
     * Links do not have content.
     * @return null
     */
    protected function getPostRawContent()
    {
        return null;
    }

    /**
     * Links do not have content.
     * @return null
     */
    protected function getPostFilteredContent()
    {
        return null;
    }

    /**
     * Parses the menu item.
     * @return void
     * @internal
     * @todo Use a work queue like the parent object
     *     instead of this monolithic nonsense,
     *     but stabilize the actual menu first.
     */
    private function _parseMenuInfo()
    {
        $shortcodes = $this::getAdapter()->parse( 'shortcodes', $this->post );
        $classes = apply_filters( 'nav_menu_css_class',
            array_filter( $this->post->classes ), $this->post );
        foreach ( $classes as
            $key =>
            $value )
        {
            $classes[$key] = esc_attr( $value );
        }
        $this->details['id'] = $this->getPostId();
        $this->details['name'] = $this->post->post_name;
        $this->details['uri'] = $this->post->url;
        $this->details['target'] = $this->post->target;
        $this->details['subject'] = $this->_getLinkSubject();
        $this->details['description'] = $this->post->description;
        $this->details['classes'] = $classes;
        $this->details['class'] = implode( ' ', $classes );
        $this->details['menu-item-parent'] = (int) $this->post->menu_item_parent
            !== 0
            ? (int) $this->post->menu_item_parent
            : false;

        $this->details['object'] = $this->post->object;
        $this->details['object-id'] = (int) $this->post->object_id;
        $this->details['type'] = $this->post->type;
        $this->details['type-label'] = $this->post->type_label;
        $this->details['db-id'] = $this->post->db_id;
        $this->details['xfn'] = $this->post->xfn;
        $this->details['type'] = 'menu-item';
        $this->details['status'] = $this->post->post_status;
        $this->details['post_type'] = $this->post->post_type;
    }

    /**
     * This pulls the container object for the target of the link directly,
     * if it can be found. It does not currently support category or term links,
     * but it will very soon(ish).
     *
     * If it's an offsite link, you're out of luck and it will return false.
     * However, this is useful for any effects that want to display the author,
     * page title, icon, or what have you in a link menu that corresponds to
     * the link being referenced, which is quite a lot more robust than most
     * any nav walker is ever going to do. This is approachable because
     * this system statically keeps a runtime cache of all post objects
     * it parses so they never get parsed twice unless they change,
     * so it doesn't kill performance to do it this way.
     *
     * If you var dump it though, it's definitely going to run out of memory,
     * so don't do that. This system is written to leverage the underlying power
     * of the C pointers used by PHP internals effectively, so that elements
     * that exist are not handled twice. When you var dump,
     * things are detached from their pointers and printed as distinct
     * independent elements, which will very quickly make your memory usage
     * spike through the roof.
     * Particularly if you use Kint or similar which tries to walk backwards
     * and parse all of the things related to what you are dumping too.
     *
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\PostContainer
     */
    private function _getLinkSubject()
    {
        $post = get_post( $this->post->object_id );
        if ( is_null( $post ) )
        {
            return false;
        }
        $post = $this::getAdapter()->parse( 'post',
            get_post( $this->post->object_id ) );
        return $post;
    }

    /**
     * Insures that a menu item is actually a "nav_menu_item". Duh.
     *
     * If you are doing something stupid and hacky,
     * this is going to suck really bad for you.
     * Fix your code so it isn't hacky and we can stay friends.
     *
     * I'm not sorry, for the record.
     *
     * @param \WP_Term $subject an instance of WP_Term with the taxonomy "nav_menu_item".
     *     If it does not have the correct taxonomy it will be flat out rejected.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     You won't ever get an exception from this unless you are doing it wrong.
     *     Don't do it wrong.
     * @internal
     */
    private function _typeCheckPostType( $subject )
    {
        if ( !$subject->post_type === 'nav_menu_item' )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Type error encountered in [%1$s]. The provided post type is not a menu item. '
                . 'Expected [%2$s], but received [%3$s]', get_class( $this ),
                'nav_menu_item', $subject->post_type
            ) );
        }
    }

}
