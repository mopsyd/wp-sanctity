<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * User Parser
 *
 * Parses an instance of WP_User, and returns a container of useful details.
 *
 * These details are packaged in a container that only provides the details,
 * without any direct access to the WP_User instance, so there is no risk of
 * accidentally borking a user account with a messed up script.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class UserParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\UserContainer';

    /**
     * Keeps repeated requests for the same user object nice and zippy
     * by not bothering to reparse them.
     *
     * Also prevents recursion bugs.
     *
     * Double win.
     *
     * @var array
     */
    private static $user_cache = array();
    private static $parse_actions = array(
        'id' => 'parseUserId',
        'exists' => 'parseUserExists',
        'name' => 'parseUserDisplayName',
        'role' => 'parseUserRoles',
        'first_name' => 'parseUserFirstName',
        'last_name' => 'parseUserLastName',
        'username' => 'parseUserUsername',
        'avatar' => 'parseUserAvatar',
        'email' => 'parseUserEmail',
        'site' => 'parseUserWebsite',
        'page' => 'parseUserPage',
        'login' => 'parseUserLogin',
        'password' => 'parseUserPassword',
    );
    private static $meta_fields = array(
        'registered' => 'user_registered',
        'bio' => 'description',
        'aim' => 'aim',
        'yahoo' => 'yim',
        'jabber' => 'jabber',
        'comment_shortcuts' => 'comment_shortcuts',
        'admin_color' => 'admin_color',
        'rich_editing' => 'rich_editing',
        'syntax_highlighting' => 'syntax_highlighting',
    );

    /**
     * Represents the user object for the given user.
     *
     * @var \WP_User
     */
    private $user;

    /**
     * Represents the working state details of the user
     * prior to completion of parsing.
     *
     * @var array
     */
    private $details = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "user"
     */
    public function getSubjectKey()
    {
        return 'user';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_User".
     */
    public function getSubjectType()
    {
        return 'WP_User';
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( array_key_exists( $subject->ID, self::$user_cache ) )
        {
            return self::$user_cache[$subject->ID];
        }
        $this->user = $subject;
        foreach ( self::$parse_actions as
            $key =>
            $method )
        {
            $this->details[$key] = $this->$method();
        }
        foreach ( self::$meta_fields as
            $key =>
            $field )
        {
            $meta = $this->_parseUserMeta( $field );
            if ( !$meta || $meta === '' || $meta === 'false' )
            {
                $this->details[$key] = false;
                continue;
            }
            $this->details[$key] = $meta;
        }
        $result = $this::containerize( $this->details );
        self::$user_cache[$subject->ID] = $result;
        return $result;
    }

    public function reset()
    {
        $this->user = null;
        $this->details = array();
        return $this;
    }

    protected function parseUserId()
    {
        $details = $this->user->to_array();
        if ( !$this->parseUserExists() || !array_key_exists( 'ID', $details ) )
        {
            return false;
        }
        return (int) $details['ID'];
    }

    protected function parseUserExists()
    {
        return $this->user->exists();
    }

    protected function parseUserDisplayName()
    {
        $details = $this->user->to_array();
        if ( !$this->parseUserExists() || !array_key_exists( 'display_name',
                $details ) )
        {
            return false;
        }
        return $details['display_name'];
    }

    protected function parseUserFirstName()
    {
        if ( !$this->parseUserExists() || !$this->user->first_name )
        {
            return false;
        }
        return $this->user->first_name;
    }

    protected function parseUserLastName()
    {
        if ( !$this->parseUserExists() || !$this->user->last_name )
        {
            return false;
        }
        return $this->user->last_name;
    }

    protected function parseUserUsername()
    {
        $details = $this->user->to_array();
        if ( !$this->parseUserExists() || !array_key_exists( 'user_nicename',
                $details ) )
        {
            return false;
        }
        return $details['user_nicename'];
    }

    protected function parseUserRoles()
    {
        if ( !$this->parseUserExists() )
        {
            return false;
        }
        return $this::getAdapter()->aggregate( 'role', $this->user );
    }

    protected function parseUserLogin()
    {
        $details = $this->user->to_array();
        if ( !$this->parseUserExists() || !array_key_exists( 'user_login',
                $details ) )
        {
            return false;
        }
        return $details['user_login'];
    }

    protected function parseUserPassword()
    {
        $details = $this->user->to_array();
        if ( !$this->parseUserExists() || !array_key_exists( 'user_pass',
                $details ) )
        {
            return false;
        }
        return $details['user_pass'];
    }

    protected function parseUserEmail()
    {
        if ( !$this->parseUserExists() || !( property_exists( $this->user,
                'data' )
            && property_exists( $this->user->data, 'user_email' ) ) )
        {
            return false;
        }
        return $this->user->data->user_email;
    }

    protected function parseUserWebsite()
    {
        if ( !$this->parseUserExists() || !( property_exists( $this->user,
                'data' )
            && property_exists( $this->user->data, 'user_url' ) ) )
        {
            return false;
        }
        return $this->user->data->user_url;
    }

    protected function parseUserPage()
    {
        if ( !$this->parseUserExists() )
        {
            return false;
        }
        return get_author_posts_url( $this->parseUserId() );
    }

    protected function parseUserAvatar()
    {
        return get_avatar_url( $this->user );
    }

    private function _parseUserMeta( $field )
    {
        if ( !$this->parseUserExists() )
        {
            return false;
        }
        $result = get_the_author_meta( $field, $this->parseUserId() );
        return $result;
    }

}
