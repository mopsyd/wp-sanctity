<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Footer Parser
 *
 * Parses the WordPress document footer.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class FooterParser
extends AbstractParser
{

    private static $footer_cache;

    private $raw;

    /**
     * The HTML parser used to parse out the WordPress head data.
     *
     * @var \mopsyd\sanctity\libs\parser\HtmlParser
     */
    private $parser;

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "footer"
     */
    public function getSubjectKey()
    {
        return 'footer';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "null".
     */
    public function getSubjectType()
    {
        return 'null';
    }

    public function parse( $subject = null )
    {
        $this->typeCheckRaw( $subject );
        if ( !is_null(self::$footer_cache) )
        {
            return self::$footer_cache;
        }
        $this->parser = $this->load('library', 'parser\\HtmlParser');
        $this->raw = $this->_captureFooter();
        $result = $this->_parseDocumentFooter();
        self::$footer_cache = $result;
        return $result;
    }

    public function reset()
    {
        return $this;
    }

    private function _captureFooter()
    {
        ob_start();
        wp_footer();
        return ob_get_clean();
    }

    private function _parseDocumentFooter()
    {
        $result = array();
        $this->parser->parse($this->raw);
        $result['meta'] = $this->parser->getByTag('meta');
        $result['links'] = $this->parser->getByTag('link');
        $result['styles'] = $this->parser->getByTag('style');
        $result['scripts'] = $this->parser->getByTag('script');
        $result['raw'] = $this->raw;
        $result['formatted'] = $this->parser->get();
        $result['content'] = $this->parser->removeByTag('meta')->removeByTag('link')->removeByTag('style')->removeByTag('script')->get();
        return $result;
    }
}
