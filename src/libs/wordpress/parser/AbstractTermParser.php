<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Abstract Term Parser
 *
 * Provides the abstraction to effectively parse Term objects
 * of all types in a uniform way.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractTermParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\TermContainer';
    const DEFAULT_TERM_TYPE = 'term';

    /**
     * Designates proxying for specific taxonomies.
     * Anything not in this array gets parsed as a generic term.
     *
     * The key is the taxonomy, and the value is the
     * keyword for the adapter parse method.
     *
     * @example `$this::getAdapter()->parse('tag', $wp_term_instance ); // fetches tags, which have the "post_tag" taxonomy.`
     * @var array
     */
    private static $term_types = array(
        'term' => 'term',
        'category' => 'category',
//        'nav_menu' => 'menu', // fix this later.
        'post_tag' => 'tag'
    );

    /**
     * Caches menus that have already been parsed statically at runtime,
     * so repeated requests for them do not result in additional overhead.
     *
     * This represents an array of pointers to already parsed menus.
     * If the requested menu already exists here, it will be returned
     * as is instead of being parsed again. The individual menu items
     * also do this, so if a menu item exists in numerous menus,
     * it will not be processed multiple times, and each element
     * only gets packaged one time per runtime, regardless of
     * how many times it is requested. Posts, users, etc
     * pretty much do the same thing, so firing numerous different requests
     * that all contain data that crosses over get increasingly
     * more performant as the request process proceeds.
     *
     * @var array
     */
    private static $term_cache = array();

    /**
     * The term object representing the menu being worked with.
     *
     * @var \WP_Term
     */
    protected $term;

    /**
     * The working data set during parsing. This will contain the term data,
     * and also an additional menu item collection upon completion.
     *
     * @var array
     */
    protected $output = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "menu"
     */
    public function getSubjectKey()
    {
        return 'term';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * This should correspond to a menu entry from a layout manifest.json file,
     * or an emulated approximation of that.
     *
     * As all classes in this chain of logic parse terms,
     * this method is declared final at this point in the abstraction.
     *
     * @return string "WP_Term".
     * @final
     */
    final public function getSubjectType()
    {
        return 'WP_Term';
    }

    /**
     * Resets the menu parser back to a clean status,
     * as if it had been freshly instantiated.
     *
     * @return $this
     */
    public function reset()
    {
        $this->term = null;
        $this->output = array();
        return $this;
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( !$this->_checkTermType( $subject->taxonomy ) )
        {
            //Proxy specialized terms to their designated handlers.
            return $this->_proxyTermParseResponse( $subject );
        }
        if ( array_key_exists( $subject->term_id, self::$term_cache ) )
        {
            return self::$term_cache[$subject->term_id];
        }
        $this->term = $subject;
        $this->_parseTerm();
        $result = $this::containerize( $this->output );
        self::$term_cache[$subject->term_id] = $result;
        return $result;
    }

    /**
     * This method should be overridden to declare a work queue of methods that
     * will package additional details into the term container relevant to
     * the specific term type.
     *
     * These methods will each be called in the order they are declared,
     * and will receive the raw term as their first parameter,
     * and the current working set of details as their second parameter.
     * They are expected to return the value that should populate the output
     * in accordance with the they key the method was registered under in the
     * response array of this method.
     *
     * These methods may override default values as needed.
     *
     * When this process is completed, the final packaged result will be
     * returned from the `parse` method in this abstract.
     *
     * If no additional data is required, you can leave this method as is,
     * in which case the only details returned will be the raw default
     * term values, and additional details will be ignored.
     *
     * @return array
     */
    protected function declareWorkQueue()
    {
        return array();
    }

    /**
     * Parses the default term keys into the output set,
     * and any declared overrides after the fact.
     */
    private function _parseTerm()
    {
        $this->output['id'] = $this->term->term_id;
        $this->output['parser'] = $this->getSubjectKey();
        $this->output['slug'] = $this->term->slug;
        $this->output['name'] = $this->term->name;
        $this->output['description'] = $this->term->description;
        $this->output['count'] = $this->term->count;
        $this->output['filter'] = $this->term->filter;
        $this->output['parent'] = $this->_getParentTerm();
        $this->output['taxonomy'] = $this->term->taxonomy;
        $this->output['meta'] = $this->_getTermMeta();
        $this->output['group'] = $this->_parseTermGroup();
        $this->output['taxonomy_id'] = $this->term->term_taxonomy_id;
        foreach ( $this->declareWorkQueue() as
            $key =>
            $method )
        {
            $this->output[$key] = $this->$method( $this->term, $this->output );
        }
    }

    /**
     * If there is a parent term, this will return a term container
     * representing the parent.
     *
     * This will continue recursively until a root term is encountered,
     * at which point this will return false.
     *
     * This operation will honor the dedicated term handling classes,
     * which means associations can be created across terms and still
     * come back correctly, which is generally not an option
     * with vanilla Wordpress (eg you could affix a tag with
     * a term parent that is a category in the database somehow,
     * and this logic will reflect that,
     * whereas the Wordpress internals will not).
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\wordpress\container\TermContainerInterface
     *
     * @note This opens the possiblity of creating pseudo-relations that are not
     *     otherwise possible in Wordpress, emulating the relational structure
     *     of a well indexed data layer (the WordPress data layer is NOT
     *     well indexed). This also does not require any database work to enact,
     *     though it does not provide an interface to accomplish this either.
     *     It just honors it if it is present. It's up to you to write some
     *     custom logic that interacts with terms if you want to leverage this,
     *     although you should be aware that it may break a lot of plugins
     *     that make assumptions about the term type always being the same
     *     from parent to child.
     *
     * @note Be sure you understand the implications of messing with the
     *     data layer in non-standard ways before you attempt anything
     *     of the sort that this functionality supports.
     *     Consider this to be an easter egg for advanced developers.
     *     This is not junior level stuff, there are no docs,
     *     and there is no forum for assistance. If you break your site
     *     and submit a bug ticket it will be closed with
     *     a "told you not to mess with it". Fair warning. -Mopsyd
     * @internal
     */
    private function _getParentTerm()
    {
        $id = $this->term->parent;
        if ( is_null($id) || !$id )
        {
            return false;
        }
        $type = $this->_getTermType( $this->term->taxonomy );
        return $this::getAdapter()->parse( $type, get_term( $id ) );
    }

    /**
     * So meta.
     *
     * Returns all of the metadata associated with the term.
     *
     * @return bool|array
     * @internal
     */
    private function _getTermMeta()
    {
        $meta = get_term_meta( $this->term->term_id );
        if ( empty( $meta ) )
        {
            return false;
        }
        d( $meta );
        return $meta;
    }

    /**
     * Resolves a taxonomy into the term parser slug dedicated to handling it,
     * or the default if one does not exist for the given term.
     *
     * This corresponds to the first parameter you would call when
     * you do `$adapter->parse()` on the term, which allows terms
     * to be dynamically proxied to the correct class to handle any
     * given term type from the base abstraction if the current object
     * is not appropriate to the given term.
     *
     * This abstraction will allow this to pass if it is the default,
     * a custom type that has no handler, or if the current object
     * reflects the handler for the term, so it does not result
     * in recursion bugs, as recursion across nine classes is a
     * huge migraine to debug, so lets just not let that
     * become an issue at all.
     *
     * @param string $taxonomy
     * @return string
     * @internal
     */
    private function _getTermType( $taxonomy )
    {
        if ( array_key_exists( $taxonomy, self::$term_types ) )
        {
            return self::$term_types[$taxonomy];
        }
        return $this::DEFAULT_TERM_TYPE;
    }

    /**
     * Checks if the term request should be rerouted based on the term type
     * to some other term designator, or parsed by the underlying abstract
     * (which is the default behavior).
     *
     * This allows new term types to be parsed by just registering their
     * dedicated class with the Adapter without additional work.
     * All unknown terms will be handled by the default object.
     * Extension terms can also declare their own default,
     * but the one this method checks against is fixed to this
     * abstraction to prevent unbreaking recursive loops in the core logic.
     *
     * This covers both default terms, and all custom terms that do not have a
     * Sanctity term handler written for them.
     *
     * @param string $taxonomy
     * @return bool
     * @internal
     */
    private function _checkTermType( $taxonomy )
    {
        return
            // This is the absolute default term. It should be handled exactly by this logic.
            $taxonomy === self::DEFAULT_TERM_TYPE
            // This is a custom term with no registered handler, which gets parsed like a default term.
            || $this->_getTermType( $taxonomy ) === self::DEFAULT_TERM_TYPE
            // This is a child class of this abstract phoning back to parent, which must be allowed to proceed.
            || $this->_getTermType( $taxonomy ) === $this->getSubjectKey();
    }

    /**
     * Proxies a term parse operation to a dedicated subclass of this abstraction,
     * which will handle the additional details for the specialized term that
     * this class doesn't handle by default.
     *
     * If the current class scope does not reflect the given term and there is
     * a known class that does, this hands the request off to that class.
     * This is done in a way that the underlying abstraction is ignorant
     * of the classes that handle it, it only knows that specific terms
     * can be proxied by keyword through the adapter, which will mediate
     * and figure out the rest.
     *
     * @param \WP_Term $subject
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\TermContainerInterface
     * @internal
     */
    private function _proxyTermParseResponse( $subject )
    {
        $method = $this->_getTermType( $subject->taxonomy );
        return $this::getAdapter()->parse( $method, $subject );
    }

    /**
     * This is a placeholder method for if Wordpress ever decides to expand on
     * the notion of term groups, which they currently don't do much with.
     * Some plugins do, so it's included to support those, but it's not worth
     * the work to create a designated container for them if they aren't
     * actually doing anything related to the core.
     *
     * For now.
     *
     * If they add support for this in some semblance of sanity,
     * it will be immediately supported on the next release,
     * which is why this placeholder method is here.
     *
     * Future proofing yo.
     *
     * @return bool|int
     * @internal
     */
    private function _parseTermGroup()
    {
        $group = $this->term->term_group;
        if ( $group === 0 )
        {
            return false;
        }
        //This will be a term group container later. Maybe.
        //Almost nobody uses it though, so it's iffy.
        return $group;
    }

}
