<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Menu Parser
 *
 * Parses menus.
 *
 * Nevermind that Nav_Walker nonsense.
 * This is how it should have been done to begin with
 * ( eg: not mixing logic with presentation ).
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class MenuParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\MenuContainer';

    /**
     * A static reference to the work queue used to package additional menu
     * details into a menu container object.
     *
     * This is kept statically to facilitate extension logic in the future
     * that resolves across instances.
     *
     * @var array
     */
    private static $work_queue = array();

    /**
     * Caches menus that have already been parsed statically at runtime,
     * so repeated requests for them do not result in additional overhead.
     *
     * This represents an array of pointers to already parsed menus.
     * If the requested menu already exists here, it will be returned
     * as is instead of being parsed again. The individual menu items
     * also do this, so if a menu item exists in numerous menus,
     * it will not be processed multiple times, and each element
     * only gets packaged one time per runtime, regardless of
     * how many times it is requested. Posts, users, etc
     * pretty much do the same thing, so firing numerous different requests
     * that all contain data that crosses over get increasingly
     * more performant as the request process proceeds.
     *
     * @var array
     */
    private static $menu_cache = array();

    /**
     * The term object representing the menu being worked with.
     *
     * @var \WP_Term
     */
    private $menu;

    /**
     * The working data set during parsing. This will contain the term data,
     * and also an additional menu item collection upon completion.
     *
     * @var array
     */
    private $output = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "menu"
     */
    public function getSubjectKey()
    {
        return 'menu';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * This should correspond to a menu entry from a layout manifest.json file,
     * or an emulated approximation of that.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Declares the work queue for details required by categories.
     *
     * @return array
     */
    protected function declareWorkQueue()
    {
        return self::$work_queue;
    }

    /**
     * Resets the menu parser back to a clean status,
     * as if it had been freshly instantiated.
     *
     * @return $this
     */
    public function reset()
    {
        $this->menu = null;
        $this->output = array();
        return $this;
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( array_key_exists( $subject['handle'], self::$menu_cache ) )
        {
            return self::$menu_cache[$subject['handle']];
        }
        $subject = array_merge( $this->_getDefaults(), $subject );
        $this->_checkSuppliedKeys( $subject );
        $id = $this->_identifyMenuId( $subject['handle'] );
        if ( $id === false ) // It might be zero, which would be stupid, but it could happen.
        {
            // The supplied menu was never registered. Bounce it.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Menu supplied in [%1$s] with handle [%2$s] does not exist.',
                get_class( $this ), $subject['handle'] )
            );
        }
        $this->menu = $this->_getMenuObject( $id );
        if (!$this->menu)
        {
            return false;
        }
        $this->_parseMenuTerm();
        $result = $this::containerize( $this->output );
        self::$menu_cache[$subject['handle']] = $result;
        return $result;
    }

    /**
     * Obtains the menu id by the provided location slug.
     *
     * @param string $slug
     * @return bool|int
     */
    private function _identifyMenuId( $slug )
    {
        $menus = get_nav_menu_locations();
        if ( !array_key_exists( $slug, $menus ) )
        {
            return false;
        }
        return $menus[$slug];
    }

    /**
     * Obtains the term object associated with the menu.
     *
     * @param int $id
     * @return \WP_Term
     */
    private function _getMenuObject( $id )
    {
        $object = wp_get_nav_menu_object( $id );
        return $object;
    }

    private function _parseMenuTerm()
    {
        $this->output['id'] = $this->menu->term_id;
        $this->output['slug'] = $this->menu->slug;
        $this->output['name'] = $this->menu->name;
        $this->output['description'] = $this->menu->description;
        $this->output['count'] = $this->menu->count;
        $this->output['filter'] = $this->menu->filter;
        $this->output['parent'] = $this->_getMenuParent( $this->menu->parent );
        $this->output['taxonomy'] = $this->menu->taxonomy; // @todo This should be replaced with a taxonomy container via parse taxonomy at some point. Whenever that gets done.
        $this->output['term_group'] = $this->menu->term_group;
        $this->output['term_id'] = $this->menu->term_id;
        $this->output['term_taxonomy_id'] = $this->menu->term_taxonomy_id;
        $this->output['items'] = $this::getAdapter()->aggregate( 'menu',
            $this->menu );
    }

    /**
     * Prototypes an instance of this class to handle the menu parent
     * if a parent exists, otherwise returns false.
     *
     * @param type $id
     * @return A container of some ilk that is yet to be determined lol.
     */
    private function _getMenuParent( $id )
    {
        $menu = false;
        $menus = get_nav_menu_locations();
        if ( in_array( $id, $menus ) )
        {
            //Prototype a copy of this class to handle the parent.
            $instance = $this::init();
            $menu = $instance->parse( array_search( $id, $menus ) );
        }
        return $menu;
    }

    /**
     * Enforces that the provided keys are present prior to
     * register or unregister operations.
     *
     * Although the underlying Wordpress api allows menus to be unregistered
     * by only supplying the key, this logic does not.
     *
     * It requires that an identical set is provided to the original
     * menu declaration to perform unregistration, so there is zero ambiguity
     * about intent.
     *
     * Higher level logic is expected to obtain or retain a reference
     * to the original data provided to perform unregistrations in
     * order to strictly enforce data integrity.
     *
     * @param array $subject
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any expected keys are not present, OR if their values
     *     are incorrectly typed.
     * @internal
     */
    private function _checkSuppliedKeys( $subject )
    {
        $expected_keys = array(
            'handle' => 'string',
            'name' => 'string',
            'textdomain' => 'string'
        );
        $invalid = array();
        foreach ( $expected_keys as
            $key =>
            $type )
        {
            if ( !array_key_exists( $key, $subject ) || gettype( $subject[$key] )
                !== $type )
            {
                $invalid[] = $key . ': ' . $type;
            }
        }
        if ( !empty( $invalid ) )
        {
            //The supplied menu does not follow the expected key structure. Bounce it.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid parameter set passed in [%1$s]. '
                . 'The following supplied keys were missing or '
                . 'invalid in the provided subject [%2$s].', get_class( $this ),
                implode( ', ', $invalid ) )
            );
        }
    }

    /**
     * It is allowable for a textdomain not to be present,
     * in which case it will use the default.
     * All other keys are required.
     * @return array
     * @internal
     */
    private function _getDefaults()
    {
        return array(
            'textdomain' => 'default'
        );
    }

}
