<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Image Parser
 *
 * Parses images into its various sizes, and packages them in an image container.
 *
 * Images will return all of the currently registered images sizes in its container.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class ImageParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\ImageContainer';

    /**
     * Keeps repeated requests for the same image nice and zippy
     * by not bothering to reparse them.
     *
     * Also stops recursion bugs.
     *
     * Double win.
     *
     * @var array
     */
    private static $image_cache = array();

    private static $early_filters = array(
        'widgets_init',
        'init',
        'template_redirect',
    );

    /**
     * The current role object being parsed.
     *
     * @var \WP_Post
     */
    private $post;

    /**
     * Represents the working state details of the image parsing process prior to completion.
     *
     * @var array
     */
    private $details = array();

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "permission"
     */
    public function getSubjectKey()
    {
        return 'image';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Post".
     */
    public function getSubjectType()
    {
        return 'WP_Post';
    }

    /**
     *
     * @param \WP_Post $subject
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\ImageContainer
     */
    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( array_key_exists( $subject->ID, self::$image_cache )
            //If we cache this too early, some plugins might not yet be set up
            //and properly have declared their filters.
            //This was noticed due to a conflict that occurred when using WP_Offload_S3,
            //which fires its registration a bit late.
            && !in_array( current_filter(), self::$early_filters )  )
        {
            return self::$image_cache[$subject->ID];
        }
        if ( !$this::getAdapter()->evaluate( 'type',
                $this::getAdapter()->query( 'query',
                    array(
                    'p' => $subject->ID,
                    'post_type' => 'any'
            ) ) ) === 'attachment' )
        {
            return false;
        }
        $this->post = $subject;
        $this->_getMetaData();
        $this->_parseImage();
        $result = $this::containerize( $this->details );
        self::$image_cache[$subject->ID] = $result;
        return $result;
    }

    public function reset()
    {
        $this->post = null;
        $this->details = array();
        return $this;
    }

    private function _parseImage()
    {
        $logo = array(
            'full' => $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                    'full' ) ),
            'large' => $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                    'large' ) ),
            'medium' => $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                    'medium' ) ),
            'post' => $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                    'post-thumbnail' ) ),
            'thumbnail' => $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                    'thumbnail' ) ),
        );
        foreach ( get_intermediate_image_sizes() as
            $size )
        {
            if ( !in_array( $size,
                    array(
                    'full',
                    'large',
                    'medium',
                    'post-thumbnail',
                    'thumbnail' ) ) )
            {
                $logo[$size] = $this->_filterImageResponse( wp_get_attachment_image_src( $this->post->ID,
                        $size ) );
            }
        }
        foreach ( $logo as
            $size =>
            $link )
        {
            if ( !array_key_exists( $size, $this->details ) )
            {
                $this->details[$size] = $link;
            }
        }
    }

    private function _filterImageResponse( $image_data )
    {
        $details = array();
        $details['src'] = esc_url( $image_data[0] );
        $details['width'] = $image_data[1];
        $details['height'] = $image_data[2];
        $details['intermediate'] = $image_data[3];
        if ( is_null( $details['src'] ) )
        {
            return false;
        }
        return $details;
    }

    private function _getMetaData()
    {
        $this->details['id'] = $this->post->ID;
        $this->details['name'] = $this->post->post_name;
        $this->details['metadata'] = array(
            'owner' => (int) $this->post->post_author,
            'mime-type' => $this->post->post_mime_type,
            'created' => ( property_exists( $this->post, 'post_date' ) && is_string( $this->post->post_date ) )
            ? $this->_filterDateString( $this->post->post_date )
            : false,
            'created-gmt' => ( property_exists( $this->post, 'post_date_gmt' ) &&
            is_string( $this->post->post_date_gmt ) )
            ? $this->_filterDateString( $this->post->post_date_gmt )
            : false,
            'modified' => ( property_exists( $this->post, 'post_modified' ) && is_string( $this->post->post_modified ) )
            ? $this->_filterDateString( $this->post->post_modified )
            : false,
            'modified-gmt' => ( property_exists( $this->post,
                'post_modified_gmt' ) && is_string( $this->post->post_modified_gmt ) )
            ? $this->_filterDateString( $this->post->post_modified_gmt )
            : false,
            'guid' => $this->post->guid,
        );
    }

    private function _filterDateString( $date )
    {
        return date( get_option( 'date_format' ), strtotime( $date ) );
    }

}
