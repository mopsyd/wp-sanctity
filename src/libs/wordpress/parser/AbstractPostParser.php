<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Post Parser
 *
 * Parses individual posts.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractPostParser
    extends AbstractParser
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\PostContainer';

    /**
     * Keeps repeated post parse requests nice and zippy by not
     * reparsing previously parsed posts.
     *
     * Also stops recursion bugs.
     *
     * Double win.
     *
     * @var array
     */
    private static $post_cache = array();
    private $query;
    private $term_query;
    private $post_formatted;
    private $post_vars;
    private $post_shortcodes;

    /**
     * Represents the mapping of all details,
     * and the corresponding methods that generate
     * each field correctly.
     *
     * @var array
     */
    private static $post_property_mapping = array(
        'id' => 'getPostId',
        'parent' => 'getPostParentId',
        'uri' => 'getPostUri',
        'guid' => 'getPostGuid',
        'slug' => 'getPostSlug',
        'title' => 'getPostTitle',
        'type' => 'getPostType',
        'post_type' => 'getPostPostType',
        'preview' => 'getPostPreviewStatus',
        'format' => 'getPostFormat',
        'menu-order' => 'getPostMenuOrder',
        'visible' => 'getPostVisibility',
        'password' => 'getPostPasswordProtection',
        'image' => 'getPostFeatureImage',
        'author' => 'getPostAuthor',
        'excerpt' => 'getPostExerpt',
        'terms' => 'getPostTerms',
        'tags' => 'getPostTags',
        'categories' => 'getPostCategories',
        'category-count' => 'getPostCategoryCount',
        'created' => 'getPostDateCreated',
        'created-gmt' => 'getPostDateCreatedGmt',
        'modified' => 'getPostDateModified',
        'modified-gmt' => 'getPostDateModifiedGmt',
        'comments' => 'getPostComments',
        'comment-count' => 'getPostCommentCount',
        'comments-enabled' => 'getPostCommentsEnabled',
        'content' => 'getPostContent',
        'debug_content' => 'getPostDebugContent',
        'filtered_content' => 'getPostFilteredContent',
        'raw_content' => 'getPostRawContent',
    );

    public function __construct( $dependencies = null, $args = array() )
    {
        //This stupid thing CONSTANTLY corrupts content. Buh Bye.
        //This will be removed on every single instance of this object,
        //in the event it tries to slink its way back in.
        remove_filter( 'the_content', 'wpautop' );
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post"
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Post".
     */
    public function getSubjectType()
    {
        return 'WP_Post';
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        if ( array_key_exists( $subject->ID, self::$post_cache ) )
        {
            return self::$post_cache[$subject->ID];
        }
        $this->reset();
        $this->post = $subject;
        $this->_setupPost();
        $this->query = $this::getAdapter()->query( 'query',
            array(
            'p' => $this->post->ID,
            'post_type' => 'any'
            ) );
        $this->term_query = $this::getAdapter()->query( 'term',
            array(
            'object_ids' => $this->post->ID
            ) );
        $this->post_shortcodes = $this::getAdapter()->parse( 'shortcodes',
            $this->post );
        $this->post_vars = $this->handleParse( $subject );
        $this->post_formatted = $this->handleFormatting( $this->post_vars );
        $result = $this::containerize( $this->post_formatted );
        self::$post_cache[$subject->ID] = $result;
        return $result;
    }

    /**
     *
     * @param type $key
     * @return type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Throws an exception if no query has run for this instance as of yet.
     */
    public function has( $key )
    {
        if ( is_null( $this->post ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'No query has run yet in [%1$s].', get_class( $this ) )
            );
        }

        return array_key_exists( $key, $this->post_formatted );
    }

    public function get( $key )
    {
        if ( is_null( $this->post ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'No query has run yet in [%1$s].', get_class( $this ) )
            );
        }
        if ( !$this->has( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid key [%1$s] in [%2$s].', $key, get_class( $this ) )
            );
        }
        return $this->post_formatted[$key];
    }

    public function reset()
    {
        $this->post = null;
        $this->query = null;
        $this->term_query = null;
        $this->post_shortcodes = null;
        $this->post_formatted = array();
        $this->post_vars = array();
        return $this;
    }

    protected function handleParse( $subject )
    {
        return get_object_vars( $subject );
    }

    protected function handleFormatting( $vars )
    {
        $result = array();
        foreach ( self::$post_property_mapping as
            $key =>
            $method )
        {
            $result[$key] = $this->$method();
        }
        return $result;
    }

    protected function getRaw()
    {
        return $this->post;
    }

    protected function getVars()
    {
        return $this->post_vars;
    }

    protected function getFormatted()
    {
        return $this->post_formatted;
    }

    protected function buildShortcodeParsedContent( $content, $shortcodes,
        $debug = false )
    {
        foreach ( $shortcodes as
            $shortcode =>
            $payload )
        {
            if ( $debug )
            {
                $content = str_replace( $shortcode,
                    '<!-- Begin Shortcode ::' . rtrim( ltrim( substr( $shortcode,
                                0, strpos( $shortcode, ']' ) ), '[' ), ']' ) . ':: -->'
                    . $shortcode
                    . '<!-- End Shortcode ::' . rtrim( ltrim( substr( $shortcode,
                                0, strpos( $shortcode, ']' ) ), '[' ), ']' ) . ':: -->',
                    $content );
            }
        }
        $parser = $this->load( 'library', 'parser\\HtmlParser' );
        // This replaces content that would typically get automatically
        // stripped by WordPress with markers, replaces them with the
        // intended content after the filtering process is complete, and
        // then applies a DOMDocument resolver to clean up the leftover mess from
        // regex used by WordPress core that butchers the html. The end result is
        // clean html markup in the post body that is to W3C spec.
        // This unfortunately does not cover the rest of the crap injected
        // into the page, but at least the post body is clean.
        // There are exactly zero regex instances used in this process.
        $result = $parser->parse( $this->_restoreExpectedContentMarkers(
                    apply_filters( 'the_content',
                        $this->_substituteExpectedContentMarkers( $content )
                    )
                )
            )->get();
        return $result;
    }

    /**
     * Gets the id for the post.
     *
     * @return int
     * @internal
     */
    protected function getPostId()
    {
        if ( !$this->post->ID || $this->post->ID === -1 )
        {
            return false;
        }
        return $this->post->ID;
    }

    /**
     * Gets the id for the post parent.
     *
     * @return int|bool Returns false on 404 and other error posts,
     *     and any post that does not have a parent
     * @internal
     */
    protected function getPostParentId()
    {
        $parent = wp_get_post_parent_id( $this->post->ID );
        return ( $parent !== 0 && $parent !== -1 )
            ? $parent
            : false;
    }

    /**
     * Gets the id for the post.
     *
     * @return int|null Returns null on 404 posts
     * @internal
     */
    protected function getPostUri()
    {
        return get_permalink( $this->post->ID );
    }

    /**
     * Gets the id for the post.
     *
     * @return string|bool Returns the guid, or false on posts that do not have one
     * @internal
     */
    protected function getPostGuid()
    {
        $guid = $this->post->guid;
        if ( $guid === '' )
        {
            return false;
        }
        return $guid;
    }

    /**
     * Gets the slug for the post.
     *
     * @return int|null Returns null on 404 posts
     * @internal
     */
    protected function getPostSlug()
    {
        return $this->post->post_name;
    }

    /**
     * Gets the title for the post.
     *
     * @return string Returns the title, or makes one if one doesn't exist.
     * @internal
     */
    protected function getPostTitle()
    {
        if ( !post_type_supports( $this->post->post_type, 'title' ) )
        {
            return false;
        }
        return $this->post->post_title;
    }

    /**
     * Gets the current native Wordpress post type for the post.
     *
     * @return string|bool Returns the post type, or false if it doesn't have one.
     * @internal
     */
    protected function getPostPostType()
    {
        return $this->post->post_type;
    }

    /**
     * Gets the current Sanctity post type for the post.
     *
     * @return string|int Returns a post type string,
     *     or a numeric error status code
     *     (pretty much 404 in all cases).
     * @internal
     */
    protected function getPostType()
    {
        return $this::getAdapter()->evaluate( 'type', $this->query );
    }

    /**
     * Gets the current preview status, so it can be determined whether
     * any applicable helper functionality for preview views should be
     * displayed.
     *
     * @return bool
     * @internal
     */
    protected function getPostPreviewStatus()
    {
        return is_customize_preview();
    }

    /**
     * Gets the post format for the current subject
     *
     * @return bool|string
     * @internal
     */
    protected function getPostFormat()
    {
        if ( !post_type_supports( $this->post->post_type, 'post-formats' ) )
        {
            return false;
        }
        return get_post_format( $this->post->ID );
    }

    /**
     * Gets the post menu order for the current subject
     *
     * @return bool|string
     * @internal
     */
    protected function getPostMenuOrder()
    {
        if ( !post_type_supports( $this->post->post_type, 'page-attributes' ) )
        {
            return false;
        }
        $order = $this->post->menu_order;
        return is_int( $order )
            ? $order
            : false;
    }

    /**
     * Gets the featured image of the current queried subject, if there is one.
     *
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\ImageContainer
     * @todo This should return a media container object eventually. There's logic floating around to do this right in one of the earlier classes already, port that to the media object.
     */
    protected function getPostFeatureImage()
    {
        if ( !post_type_supports( $this->post->post_type, 'thumbnail' ) )
        {
            return false;
        }
        if ( !has_post_thumbnail( $this->post->ID ) )
        {
            return false;
        }
        $img = get_post( get_post_thumbnail_id( $this->post->ID ) );
        return $this::getAdapter()->parse( 'image', $img );
    }

    protected function getPostDateCreated()
    {
        return get_the_date( get_option( 'date_format' ), $this->post->ID );
    }

    protected function getPostDateCreatedGmt()
    {
        $date = $this->getPostDateCreated();
        if ( $date === false )
        {
            return false;
        }
        return get_date_from_gmt( $date, get_option( 'date_format' ) );
    }

    protected function getPostDateModified()
    {
        return get_the_modified_date( get_option( 'date_format' ),
            $this->post->ID );
    }

    protected function getPostDateModifiedGmt()
    {
        $date = $this->getPostDateModified();
        if ( $date === false )
        {
            return false;
        }
        return get_date_from_gmt( $date, get_option( 'date_format' ) );
    }

    protected function getPostAuthor()
    {
        if ( !post_type_supports( $this->post->post_type, 'author' ) )
        {
            return false;
        }
        $author_id = get_post_field( 'post_author', $this->post->ID );
        if ( $author_id === '' )
        {
            return false;
        }
        $user_object = new \WP_User( (int) $author_id );
        return $this::getAdapter()->parse( 'user', $user_object );
    }

    protected function getPostExerpt()
    {
        if ( !property_exists( $this->post, 'post_excerpt' ) )
        {
            return false;
        }
        if ( !post_type_supports( $this->post->post_type, 'excerpt' ) )
        {
            return false;
        }
        $this->_setupPost();
        return get_the_excerpt( $this->post->ID );
    }

    protected function getPostContent()
    {
        if ( !post_type_supports( $this->post->post_type, 'editor' ) )
        {
            return false;
        }
        return $this->buildShortcodeParsedContent(
                $this->post_vars['post_content'], $this->post_shortcodes
        );
    }

    protected function getPostDebugContent()
    {
        if ( !post_type_supports( $this->post->post_type, 'editor' ) )
        {
            return false;
        }
        return $this->buildShortcodeParsedContent(
                $this->post_vars['post_content'], $this->post_shortcodes, true
        );
    }

    protected function getPostRawContent()
    {
        if ( !post_type_supports( $this->post->post_type, 'editor' ) )
        {
            return false;
        }
        return $this->post_vars['post_content'];
    }

    protected function getPostFilteredContent()
    {
        if ( !post_type_supports( $this->post->post_type, 'editor' ) )
        {
            return false;
        }
        return $this->post_vars['post_content_filtered'];
    }

    protected function getPostCategories()
    {
        if ( !$this->getPostId() )
        {
            // No categories on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'category', $terms );
    }

    protected function getPostTags()
    {
        if ( !$this->getPostId() )
        {
            // No tags on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'tag', $terms );
    }

    protected function getPostTerms()
    {
        if ( !$this->getPostId() )
        {
            // No terms on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'term', $terms );
    }

    protected function getPostCategoryCount()
    {
        $count = 0;
        $categories = wp_get_post_categories( $this->post->ID );
        if ( is_array( $categories ) )
        {
            $count = count( $categories );
        }
        return $count;
    }

    protected function getPostVisibility()
    {
        if ( $this->post->post_status === 'publish' )
        {
            return true;
        }
        // If the current user has access to non-public posts.
        if ( (
            ( current_user_can( 'read_private_pages' ) //covers pages
            && in_array( $this->getPostType(),
                array(
                'index',
                'page' ) ) )
            || ( current_user_can( 'read_private_posts' ) //covers everything else
            && !in_array( $this->getPostType(),
                array(
                'index',
                'page' ) ) )
            ) )
        {
            return true;
        }
        // If the post is not public, but is being viewed by its owner
        if ( wp_get_current_user()->ID === (int) $this->post->post_author )
        {
            return true;
        }
        return false;
    }

    protected function getPostPasswordProtection()
    {
        return post_password_required( $this->post->ID );
    }

    protected function getPostComments()
    {
        if ( !post_type_supports( $this->post->post_type, 'comments' ) )
        {
            return false;
        }
        if ( !$this->getPostCommentsEnabled() || $this->getPostCommentCount() ===
            0 )
        {
            return false;
        }
        if ( defined( 'WP_DEBUG' ) && WP_DEBUG && current_user_can( 'administrator' ) )
        {
//            d( 'Awww shit. Someone forgot to write the comment aggregator. Get on that guy.' );
        }
        return false;
    }

    protected function getPostCommentCount()
    {
        if ( !post_type_supports( $this->post->post_type, 'comments' ) )
        {
            return false;
        }
        return (int) get_comments_number( $this->post->ID );
    }

    protected function getPostCommentsEnabled()
    {
        if ( !post_type_supports( $this->post->post_type, 'comments' ) )
        {
            return false;
        }
        return comments_open( $this->post->ID );
    }

    /**
     * Clean Error Suppression: This Is Why You Don't Use F***'ing Globals And Also Not Typecheck Edition.
     *
     * Pre-emptively corrects the constant onslaught of
     * "trying to get property of non-object" errors that
     * constantly occur and nobody can explain
     * because the WordPress core DOESN'T F***'ING TYPECHECK.
     *
     * But of course we use globals! Because reasons!
     *
     * We also don't typecheck! Also because reasons!
     *
     * @return void
     * @internal
     *
     * @note For funsies, Google "trying to get property of non-object wordpress":
     *     Results: 79,000.
     *     Typical solution: "You don't actually need the thing you want"
     *     Honorable Mention: "Use an @ to pretend it doesn't exist"
     */
    private function _setupPost()
    {
        $GLOBALS['post'] = &$this->post;
        setup_postdata( $GLOBALS['post'] );
    }

    private function _substituteExpectedContentMarkers( $content )
    {
        $result = str_replace( '\\\\',
            '<br class="sanctity-backslash-placeholder">', $content );
        $result = str_replace( '\\',
            '<br class="sanctity-backslash-placeholder">', $content );
        $result = str_replace( '<br>', '<br class="sanctity-br-placeholder">',
            $result );
        $result = str_replace( '<br />', '<br class="sanctity-br-placeholder">',
            $result );
        $result = str_replace( '<p>',
            '<br class="sanctity-p-open-placeholder">', $result );
        $result = str_replace( '</p>',
            '<br class="sanctity-p-close-placeholder">', $result );
        return $result;
    }

    private function _restoreExpectedContentMarkers( $content )
    {
        $result = str_replace( '&nbsp;', '<br class="pb-2">', $content );
        $result = str_replace( PHP_EOL . PHP_EOL, PHP_EOL, $result );
        $result = str_replace( '<br class="sanctity-backslash-placeholder">',
            '\\', $result );
        $result = str_replace( '<br class="sanctity-br-placeholder">', '<br class="pb-2">', $result );
        $result = str_replace( '<br class="sanctity-p-open-placeholder">',
            '<p>', $result );
        $result = str_replace( '<br class="sanctity-p-close-placeholder">',
            '</p>', $result );
        return trim($result, PHP_EOL);
    }

}
