<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\parser;

/**
 * Shortcodes Parser
 *
 * "Regex is only the answer when there isn't any other answer"
 *
 * - Words to live by.
 *
 * --------
 *
 * Parses all of the shortcodes in the raw content of a WP_Post object,
 * and returns a collection of resulting values.
 *
 * For individual shortcode parsing, use ShortcodeParser ( without the "s" )
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class ShortcodesParser
extends AbstractParser
{
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\ShortcodeContainer';

    /**
     * Represents the official Wordpress shortcode regex expression.
     * This is only called one time to obtain it, and then used locally
     * thereafter on all subsequent operations to reduce redundant
     * function calls.
     *
     * @var string
     */
    private static $shortcode_regex;

    private $post;

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "term"
     */
    public function getSubjectKey()
    {
        return 'shortcodes';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Post".
     */
    public function getSubjectType()
    {
        return 'WP_Post';
    }

    public function parse( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->_initializeShortcodeRegex();
        $this->post = $subject;
        return $this->_extractShortcodes();
    }

    public function reset()
    {
        $this->post = null;
        return $this;
    }

    /**
     * Extracts only the valid unique shortcodes,
     * and returns a container of them.
     *
     * @return \mopsyd\sanctity\libs\wordpress\container\ShortcodeContainer
     */
    private function _extractShortcodes()
    {
        $content = $this->post->post_content_filtered;
        $pattern = self::$shortcode_regex;
        $matches = array();
        $valid = array();
        $has_shortcodes = preg_match( '/' . $pattern . '/', $content, $matches );
        if ( $has_shortcodes !== false )
        {
            foreach ( $matches as
                $shortcode )
            {
                if ( !$this->_validateShortcodeRaw( $shortcode ) )
                {
                    continue;
                }
                $valid[$shortcode] = $shortcode;
            }
        }
        return $this::containerize( $valid );
    }

    /**
     * Validates shortcodes from the raw array provided by Wordpress's
     * `get_shortcode_regex` function, which occasionally provides
     * duplicate matches, empty strings, or other misfires.
     *
     * This method assumes the regex has already been run, and that it is
     * receiving a raw entry from the result set. It will verify that the
     * given string matches the expected shortcode output and
     * return true if it is well formed, and false if it is
     * not for the `do_shortcode` function.
     *
     * @param string $shortcode
     * @return bool
     * @internal
     */
    private function _validateShortcodeRaw( $shortcode )
    {
        if ( is_string( $shortcode ) //non-strings are not valid
            && $shortcode !== '' //empty strings returned occasionally by wordpress's regex logic derping are not valid
            && substr( $shortcode, 0, 1 ) === '[' //valid shortcodes start with a left bracket
            && substr( $shortcode, strlen( $shortcode ) - 1 ) === ']' //valid shortcodes end with a right bracket
        )
        {
            //remaining preg_match considerations have already been applied
            //in the calling function. There is no need to regex again
            //because regex is slooooooooow, and we don't want to affect
            //performance if this gets called a bjillion times.
            return true;
        }
        return false;
    }

    /**
     * Fetches the shortcode regex from the standard Wordpress method
     * dedicated to returning it, and then stores it locally so we never
     * need to ask again in any given runtime.
     *
     * @return void
     * @internal
     */
    private function _initializeShortcodeRegex()
    {
        if (is_null(self::$shortcode_regex))
        {
            self::$shortcode_regex = get_shortcode_regex();
        }
    }
}
