<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\strategy;

/**
 * Abstract Strategy
 *
 * Represents the base abstraction for determining the order of operations of
 * the underlying platform.
 *
 * This is requested by the Front Controller
 * almost immediately, and used to organize how the internals of the
 * Sanctity MVC logic interoperates.
 *
 * This subset is then provided to the Runtime Controller,
 * which will organize its path of execution in accordance
 * with the provided logic.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractStrategy
    extends \mopsyd\sanctity\libs\wordpress\AbstractWordpressWorker
    implements \mopsyd\sanctity\interfaces\libs\wordpress\StrategyInterface
{

}
