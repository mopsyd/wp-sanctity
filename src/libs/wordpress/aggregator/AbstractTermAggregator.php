<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\aggregator;

/**
 * Abstract Term Aggregator
 *
 * Provides abstraction to aggregate terms into a collection,
 * or defer to a dedicated aggregator if one is available.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
abstract class AbstractTermAggregator
    extends AbstractAggregator
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\TermCollection';

    /**
     * Override this constant with a specific taxonomy to filter for.
     *
     * If this is false, no taxonomy filter will be applied.
     */
    const TAXONOMY_FILTER = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "term"
     */
    public function getSubjectKey()
    {
        return 'term';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "array".
     */
    public function getSubjectType()
    {
        return 'array';
    }

    /**
     * Takes an instance of \WP_Term, and spits out a menu item collection.
     *
     * @param \WP_Term $subject Must have the taxonomy "nav_menu"
     * @return \mopsyd\sanctity\libs\wordpress\container\MenuItemCollection
     *     The response body will be a menu item collection,
     *     with keys equal to the guid of the post,
     *     and the value being a PostContainer
     *
     * @note DO NOT use the key as the post link when iterating over the collection.
     *     It is simply the only unchanging string property of the post,
     *     which is why it is used as the key. The Psr-11 spec requires string keys.
     */
    public function aggregate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->_verifySubject( $subject );
        $term_items = array();
        foreach ( $subject as
            $key =>
            $term )
        {
            if ( !$this->filterTaxonomy( $term->taxonomy ) )
            {
                continue;
            }
            $item = $this::getAdapter()->parse( $this->getSubjectKey(), $term );
            $term_items[$term->term_id] = $item;
        }
        if ( empty( $term_items ) )
        {
            return false;
        }
        return $this::containerize( $term_items );
    }

    /**
     * Filters for a specific taxonomy. By default, this filters for the taxonomy
     * defined by the `TAXONOMY_FILTER` class constant, and skips filtering if
     * that constant is set to false. You may filter for single taxonomies per
     * aggregator (suggested) by simply overriding that class constant with the
     * taxonomy to filter for. If you want to filter for multiple similar
     * taxonomies instead, then override this method and match against
     * the desired subset instead.
     *
     * Eg, if you wanted to filter for portfolio items from a number
     * of plugins that all declare their custom type differently,
     * and wanted them all to display as portfolio items.
     *
     * @param string $taxonomy
     * @return bool Returns true if a direct match or
     *     if filtering is disabled (default behavior),
     *     returns false if filtering is enabled and no match.
     */
    protected function filterTaxonomy( $taxonomy )
    {
        if ( !$this::TAXONOMY_FILTER )
        {
            return true;
        }
        return $taxonomy === $this::TAXONOMY_FILTER;
    }

    /**
     * Verifies that only valid term objects were supplied.
     *
     * @param array $subject
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any of the provided values are not instances of WP_Term
     * @internal
     */
    private function _verifySubject( $subject )
    {
        $expected = 'WP_Term';
        foreach ( $subject as
            $term )
        {
            if ( !is_object( $term ) && ( $term instanceof $expected ) )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Invalid arguments passed in [%1$s]. '
                    . 'Expected only instances of [%2$s].', get_class( $this ),
                    $expected )
                );
            }
        }
    }

}
