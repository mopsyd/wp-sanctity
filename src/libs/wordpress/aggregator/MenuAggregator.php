<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\aggregator;

/**
 * Menu Aggregator
 *
 * Aggregates the creation of a menu from a WP_Term object
 * from a query into a MenuItemCollection.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class MenuAggregator
    extends AbstractAggregator
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\MenuItemCollection';

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "menu"
     */
    public function getSubjectKey()
    {
        return 'menu';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Term".
     */
    public function getSubjectType()
    {
        return 'WP_Term';
    }

    /**
     * Takes an instance of \WP_Term, and spits out a menu item collection.
     *
     * @param \WP_Term $subject Must have the taxonomy "nav_menu"
     * @return \mopsyd\sanctity\libs\wordpress\container\MenuItemCollection
     *     The response body will be a menu item collection,
     *     with keys equal to the guid of the post,
     *     and the value being a PostContainer
     *
     * @note DO NOT use the key as the post link when iterating over the collection.
     *     It is simply the only unchanging string property of the post,
     *     which is why it is used as the key. The Psr-11 spec requires string keys.
     */
    public function aggregate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->_validateMenuTerm( $subject );
        $menu_items = array();
        $raw_menu_items = wp_get_nav_menu_items( $subject->name, array() );
        try {
            foreach ( $raw_menu_items as
                $key =>
                $post )
            {
                $item = $this::getAdapter()->parse( 'menu-item', $post );
                $menu_items[$item->ID] = $item;
            }
        } catch (\Exception $e) {
//            d($e->getMessage(), $e); // Placeholder for refactoring architecture. Do not remove until further notice.
            throw $e;
        }
        $menu = $this->_orderMenuItems( $menu_items );
        return $this::containerize( $menu );
    }

    /**
     * Order! Order! I demand order in the court!
     *
     * Makes menu items fall in line like good little peons.
     *
     * @param array $items
     * @return array
     * @internal
     */
    private function _orderMenuItems( $items )
    {
        $toplevel = array();
        foreach ( $items as
            $id =>
            $item )
        {
            if ( $item['menu-item-parent'] === false )
            {
                $toplevel[$item['menu-order']] = $this->_packageSublinks( $item,
                    $items );
            }
        }
        $toplevel = $this->_sortLinkset( $toplevel );
        return $toplevel;
    }

    /**
     * Recursively orders and packages sublinks
     * all the way down the rabbit hole.
     *
     * @param array $item The current item
     * @param array $links The subset of all unorganized items
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\MenuItemCollection
     * @internal
     */
    private function _packageSublinks( $item, $links )
    {
        $id = $item['id'];
        $children = array();
        foreach ( $links as
            $link_id =>
            $link )
        {
            if ( $link['menu-item-parent'] !== false && $link['menu-item-parent']
                === $id )
            {
                $children[$link_id] = $this->_packageSublinks( $link, $links );
            }
        }
        if ( empty( $children ) )
        {
            $item['children'] = false;
            return $item;
        }
        $item['children'] = $this::containerize( $this->_sortLinkset( $children ) );
        return $item;
    }

    /**
     * Sorts a set of provided links by their menu order,
     * and returns them still keyed by id.
     *
     * @param array $links
     * @return array
     */
    private function _sortLinkset( $links )
    {
        $sort_set = array();
        $result = array();
        foreach ( $links as
            $id =>
            $link )
        {
            $sort_set[$link['menu-order']] = $link;
        }
        ksort( $sort_set );
        foreach ( $sort_set as
            $order =>
            $link )
        {
            $result[$link['id']] = $link;
        }
        return $result;
    }

    /**
     * Insures that a nav menu is actually a "nav_menu". Duh.
     *
     * If you are doing something stupid and hacky,
     * this is going to suck really bad for you.
     * Fix your code so it isn't hacky and we can stay friends.
     *
     * I'm not sorry, for the record.
     *
     * @param \WP_Term $subject an instance of WP_Term with the taxonomy "nav_menu"
     *     If it does not have the correct taxonomy it will be flat out rejected.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     You won't ever get an exception from this unless you are doing it wrong.
     *     Don't do it wrong.
     * @internal
     */
    private function _validateMenuTerm( $subject )
    {
        if ( !$subject->taxonomy === 'nav_menu' )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Type error encounted in [%1$s]. The supplied term '
                . 'does not have the correct taxonomy. '
                . 'Expected [%2$s] but received [%3$s].', get_class( $this ),
                'nav_menu', $subject->taxonomy )
            );
        }
    }

}
