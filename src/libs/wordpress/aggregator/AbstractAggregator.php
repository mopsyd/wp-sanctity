<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\aggregator;

/**
 * Abstract Aggregator
 *
 * Base abstraction for Wordpress aggregation workers.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
abstract class AbstractAggregator
    extends \mopsyd\sanctity\libs\wordpress\AbstractWordpressWorker
    implements \mopsyd\sanctity\interfaces\libs\wordpress\AggregatorInterface
{

    /**
     * Aggregators parse elements iteratively, handing off each subsequent
     * object in the provided subject to a parser to complete formatting
     * of each element, and then return a collection of the full result set.
     *
     * The `aggregate` method is the action method for aggregator type workers.
     *
     * They differ from parsers in that they direct multiple similar operations
     * to a single appropriate parser, and differ from populators in that they
     * do not apply baseline formatting to the result set.
     *
     * @param mixed $subject This will either be an array, container,
     *     or an object that the target aggregator knows some customized
     *     means of iterating if it is non-standard (most WP_* objects are
     *     non-standard iterables that have their own "Wordpress Way" of
     *     being iterated over).
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\CollectionInterface
     *     The response body will be a collection of one sort or another, scoped to the
     *     wordpress adapters appropriate collection interface. All of those extend
     *     from this interface though.
     */
    abstract public function aggregate( $subject );

    /**
     * Releases the current subject and clears all internal data representing it,
     * so the object can be used fresh without any remnant data of a prior run.
     *
     * The default implementation makes no assumption that internal properties
     * are retained. This method must be overrided for specific aggregators
     * that store internal properties during the aggregation process,
     * which is generally only required if the aggregation in question
     * is particularly complex.
     *
     * @return $this This object returns itself for method chaining when
     *     this method is called.
     */
    public function reset()
    {
        return $this;
    }

}
