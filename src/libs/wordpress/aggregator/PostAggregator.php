<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\aggregator;

/**
 * Post Aggregator
 *
 * Aggregates posts from a query into a PostCollection,
 * and properly restores the state of the query afterwards.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class PostAggregator
    extends AbstractAggregator
{
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\PostCollection';

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post"
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query".
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    /**
     * Takes an instance of \WP_Query, and spits out a post collection.
     *
     * Resets the Query back to its original state,
     * so it can be used elsewhere without issue.
     *
     * @param \WP_Query $subject
     * @return \mopsyd\sanctity\libs\wordpress\container\PostCollection
     *     The response body will be a post collection,
     *     with keys equal to the guid of the post,
     *     and the value being a PostContainer
     *
     * @note DO NOT use the key as the post link when iterating over the collection.
     *     It is simply the only unchanging string property of the post,
     *     which is why it is used as the key. The Psr-11 spec requires string keys.
     */
    public function aggregate( $subject )
    {
        $posts = array();
        if ( $subject->have_posts() )
        {
            while ( $subject->have_posts() )
            {
                $subject->the_post();
                $post = $this::getAdapter()->parse( 'post', $subject->post );
                $posts[$post['guid']] = $post;
            }
            $subject->rewind_posts();
            wp_reset_postdata();
        }
        return $this::containerize( $posts );
    }

}
