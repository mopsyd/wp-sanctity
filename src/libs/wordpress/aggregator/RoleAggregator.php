<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\aggregator;

/**
 * Role Aggregator
 *
 * Aggregates roles bound to a user, and returns a collection of them
 * populated with individual role containers,
 * that have the full set of details for each role.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class RoleAggregator
    extends AbstractAggregator
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\RoleCollection';

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "role"
     */
    public function getSubjectKey()
    {
        return 'role';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_User".
     */
    public function getSubjectType()
    {
        return 'WP_User';
    }

    /**
     * Takes an instance of \WP_User, and spits out a role collection.
     *
     * @param \WP_User $subject
     * @return \mopsyd\sanctity\libs\wordpress\container\RoleCollection
     */
    public function aggregate( $subject )
    {
        $roles = array();
        if ( $subject->roles && !empty( $subject->roles ) )
        {
            foreach ( $subject->roles as
                $role )
            {
                $roles[$role] = $this::getAdapter()->parse( 'role', get_role( $role ) );
            }
        }
        return $this::containerize( $roles );
    }

}
