<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress;

/**
 * Abstract Wordpress Worker
 *
 * Represents an object designated as a handler
 * for various tasks of the Wordpress Adapter.
 *
 * This abstraction layer allows baseline dependencies of the Wordpress Adapter
 * to receive dependency injection statically, so that it is not required to
 * individually inject instances of the adapter into a billion tiny little
 * subclasses.
 *
 * All of the individual dependencies of the Wordpress Adapter that do not
 * have to extend another class extend this one.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractWordpressWorker
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\wordpress\WordpressWorkerInterface
{

    /**
     * Wordpress Worker classes should override this constant if they need a
     * scoped container to distribute their finalized payload. This is just
     * the default container scoped to the Wordpress Adapter module.
     */
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\WordpressContainer';

    /**
     * One Factory To Rule Them All!
     *
     * Factorization is required for the workers to load container
     * and collection instances to package with their result sets.
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Wordpress Worker Constructor
     *
     * @param type $dependencies
     * @param type $args
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     The worker constructor does not allow instantiation if an instance of
     *     the Adapter has not been statically injected, or provided in the
     *     dependency parameter of the constructor.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        if ( is_null( $this::getAdapter() ) )
        {
            $trace = debug_backtrace( 1 );
            $trace = $trace[1];
            $this->debugStash( array(
                'class' => get_class( $this ),
                'context' => 'WordpressAdapter not yet set',
                'culprit' => $trace['class'] . '::' . $trace['method']
                ), 'initialization', 'invalid_load_order' );
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Inappropriate load order in [%1$s]. '
                . 'The adapter must initialize the worker abstract '
                . '[%2$s] prior to instantiation.', get_class( $this ),
                __CLASS__ )
            );
        }
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "post" is the default key,
     *     as it is the most comon context.
     */
    public function getSubjectKey()
    {
        return 'post';
    }

    /**
     * Returns the expected subject type of the worker object,
     * which corresponds to the object instance it expects to receive
     * as a parameter in its action method.
     *
     * @return string "WP_Query" is the most common object type,
     *     so this is the default. Child classes that use a different
     *     subject type can override this method to declare an alterate subject.
     */
    public function getSubjectType()
    {
        return 'WP_Query';
    }

    /**
     * Typechecks the declared parameter against the supplied one,
     * and throws an exception if it's a mismatch.
     *
     * This method should be called on the first line of each workers action method.
     *
     * @param type $subject
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the supplied parameter is not congruent with the current worker.
     */
    protected function typeCheckRaw( $subject )
    {
        $expected = $this->getSubjectType();
        if ( $expected
            && strtolower( gettype( $subject ) ) !== strtolower( $expected )
            && !( is_object( $subject ) && ( $subject instanceof $expected ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Invalid parse format passed in [%1$s]. Expected [%2$].',
                get_class( $this ), $expected
            ) );
        }
    }

}
