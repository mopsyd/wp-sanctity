<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\populator;

/**
 * Abstract Page Populator
 *
 * Provides an abstraction layer for populating a set of page details.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 *
 * @note Many of the methods in this class are nearly identical to the methods
 *     in the PostParser, except the PostParser operates against a
 *     specific post object, and this class operates against a
 *     query object, so there are some subtle distinctions that
 *     warrant both approaches, because it is sometimes appropriate
 *     to evaluate the page itself (such as if you are evaluating a query
 *     that doesn't neccessarily represent a post, like a 404 page),
 *     and other times it is more appropriate to evaluate the post
 *     directly (which is usually the case, but not always). I really
 *     do not like code duplication, but due to the quirks of the
 *     underlying platform it is pretty neccessary in this case to
 *     keep things performance oriented, because the underlying
 *     platform doesn't have a consistent api, so in order to
 *     present one externally, a couple of different ways to
 *     accomplish nearly identical results have to be done to
 *     preserve public api flexibility. Just be aware that I know
 *     it isn't strictly DRY, and I don't like that it's not
 *     strictly DRY either. Can't be helped in this case.
 * @note The primary role of this class is to avoid the overhead of
 *     numerous queries, which kill RAM quickly, particularly on low memory
 *     shared hosting instances. As the Wordpress database model
 *     is primarily EAV (the WORST way to layout a relational database),
 *     it is increasingly essential to grab everything in only one query,
 *     which means not firing a ton of instances of WP_Query on a single page,
 *     which is well known to cause severe performance issues.
 *     This class allows a single query to be parted out and/or passed around as
 *     needed, so additional hits on the database do not occur after
 *     the first query is supplied, which means that additional
 *     redundant data is not cached in the Wordpress internals,
 *     and RAM is preserved pretty optimally.
 *
 * @link https://softwareengineering.stackexchange.com/questions/93124/eav-is-it-really-bad-in-all-scenarios
 *     The link is provided as some context as to why this particular
 *     database layout decision basically snowballed into all of the
 *     other nonsensical quirky bugginess that the Wordpress platform
 *     suffers from. This is also something that can't be fixed once
 *     you commit to it.
 */
abstract class AbstractPagePopulator
    extends AbstractPopulator
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\wordpress\\container\\PageContainer';

    /**
     * Represents the mapping of all details,
     * and the corresponding methods that generate
     * each field correctly.
     *
     * @var array
     */
    private static $page_property_mapping = array(
        'id' => 'getQueriedId',
        'parent' => 'getQueriedParentId',
        'uri' => 'getQueriedUri',
        'guid' => 'getQueriedGuid',
        'slug' => 'getQueriedSlug',
        'title' => 'getQueriedTitle',
        'type' => 'getQueriedType',
        'preview' => 'getQueriedPreviewStatus',
        'post_type' => 'getQueriedPostType',
        'format' => 'getQueriedFormat',
        'menu-order' => 'getQueriedMenuOrder',
        'visible' => 'getQueriedVisibility',
        'password' => 'getQueriedPasswordProtection',
        'content' => 'getQueriedContent',
        'raw-content' => 'getQueriedRawContent',
        'debug-content' => 'getQueriedDebugContent',
        'posts' => 'getQueriedPosts',
        'image' => 'getQueriedFeatureImage',
        'author' => 'getQueriedAuthor',
        'terms' => 'getQueriedTerms',
        'tags' => 'getQueriedTags',
        'categories' => 'getQueriedCategories',
        'category-count' => 'getQueriedCategoryCount',
        'created' => 'getQueriedDateCreated',
        'created-gmt' => 'getQueriedDateCreatedGmt',
        'modified' => 'getQueriedDateModified',
        'modified-gmt' => 'getQueriedDateModifiedGmt',
        'comments' => 'getQueriedComments',
        'comment-count' => 'getQueriedCommentCount',
        'comments-enabled' => 'getQueriedCommentsEnabled',
    );

    /**
     * Represents the default values used if a determination
     * cannot be made about a given field.
     *
     * @var array
     */
    private static $details_defaults = array();

    /**
     * Represents the instance of WP_Query currently being evaluated.
     * @var \WP_Query
     */
    private $query;

    /**
     * Represents the term query for all taxonomies associated
     * with the currently evaluated query.
     * @var \WP_Term_Query
     */
    private $term_query;
    private $target_post = false;

    /**
     * Represents the working details generated
     * during the population process.
     * @var array
     */
    private $details = array();

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns the identifying key of the worker.
     *
     * @return string "page"
     */
    public function getSubjectKey()
    {
        return 'page';
    }

    public function populate( $subject )
    {
        $this->typeCheckRaw( $subject );
        $this->query = $subject;
        $this->term_query = $this::getAdapter()->query( 'term',
            array(
            'object_ids' => $this->query->queried_object_id
            ) );
        $this->_indexQuery();
        $result = $this::containerize( $this->details );
        $categories = get_categories();
        $test = $this::getAdapter()->aggregate( 'term', get_categories() );
        return $result;
    }

    public function reset()
    {
        $this->query = null;
        $this->term_query = null;
        $this->details = array();
        $this->target_post = false;
    }

    /**
     * Creates a set of values to return for standard pages and posts.
     * @return array
     * @internal
     * @deprecated This method is only for reference.
     *     It doesn't fire anywhere in the code.
     *     It is a placeholder for development to remind me to finish these sections.
     *     It is intentionally left as an unreachable method so the CI pipeline
     *     will not clear the class as being 100% covered until these are accounted for,
     *     so CI will nag me to get this done, and it doesn't wind up being a lost detail
     *     that never gets revisited that way.
     */
    private function _populatePage()
    {
        $result = array(
            'comments' => $this->query->tax_query, //Needs a comment parser to complete
        );

        return $result;
    }

    /**
     * Gets the id for the queried object.
     *
     * @return int|bool Returns false on 404 and other error pages
     * @internal
     */
    protected function getQueriedId()
    {
        if ( in_array( $this::getAdapter()->evaluate( 'type', $this->query ),
                array(
                'error',
                401,
                403,
                404,
                500
            ) ) )
        {
            return false;
        }
        return $this->query->queried_object_id;
    }

    /**
     * Gets the id for the queried object parent.
     *
     * @return int|bool Returns false on 404 and other error pages,
     *     and any page that does not have a parent
     * @internal
     */
    protected function getQueriedParentId()
    {
        $parent = wp_get_post_parent_id( $this->getQueriedId() );
        return $parent !== 0
            ? $parent
            : false;
    }

    /**
     * Gets the id for the queried object.
     *
     * @return int|null Returns null on 404 pages
     * @internal
     */
    protected function getQueriedUri()
    {
        return get_permalink( $this->getQueriedId() );
    }

    /**
     * Gets the id for the queried object.
     *
     * @return string|bool Returns the guid, or false on pages that do not have one
     * @internal
     */
    protected function getQueriedGuid()
    {
        $guid = get_the_guid( $this->getQueriedId() );
        if ( $guid === '' )
        {
            return false;
        }
        return $guid;
    }

    /**
     * Gets the id for the queried object.
     *
     * @return int|null Returns null on 404 pages
     * @internal
     */
    protected function getQueriedSlug()
    {
        return get_page_uri( $this->getQueriedId() );
    }

    /**
     * Gets the title for the queried object.
     *
     * @return string Returns the title, or makes one if one doesn't exist.
     * @internal
     */
    protected function getQueriedTitle()
    {
        return $this::getAdapter()->evaluate( 'title', $this->query );
    }

    /**
     * Gets the current native Wordpress post type for the queried object.
     *
     * @return string|bool Returns the post type, or false if it doesn't have one.
     * @internal
     */
    protected function getQueriedPostType()
    {
        return get_post_type( $this->getQueriedId() );
    }

    /**
     * Gets the current page type for the queried object.
     *
     * @return string|int Returns a page type string,
     *     or a numeric error status code
     *     (pretty much 404 in all cases).
     * @internal
     */
    protected function getQueriedType()
    {
        return $this::getAdapter()->evaluate( 'type', $this->query );
    }

    /**
     * Gets the current preview status, so it can be determined whether
     * any applicable helper functionality for preview views should be
     * displayed.
     *
     * @return bool
     * @internal
     */
    protected function getQueriedPreviewStatus()
    {
        return is_customize_preview();
    }

    /**
     * Gets the post format for the current subject
     *
     * @return bool|string
     * @internal
     */
    protected function getQueriedFormat()
    {
        return get_post_format( $this->getQueriedId() );
    }

    /**
     * Gets the post menu order for the current subject
     *
     * @return bool|string
     * @internal
     */
    protected function getQueriedMenuOrder()
    {
        $order = get_post_field( 'menu_order', $this->getQueriedId() );
        return is_int( $order )
            ? $order
            : false;
    }

    /**
     * Gets the posts, or false if there are none.
     *
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\PostCollection
     */
    protected function getQueriedPosts()
    {
        if ( $this->query->post_count === 0 || in_array( $this->getQueriedType(),
                array(
                401,
                403,
                404,
                500,
                'login',
                'robots',
                'admin' ) ) || $this->query->is_singular )
        {
            return false;
        }
        return $this::getAdapter()->aggregate( 'post', $this->query );
    }

    /**
     * Gets the featured image of the current queried subject, if there is one.
     *
     * @return bool|\mopsyd\sanctity\libs\wordpress\container\ImageContainer
     * @todo This should return a media container object eventually. There's logic floating around to do this right in one of the earlier classes already, port that to the media object.
     */
    protected function getQueriedFeatureImage()
    {
        if ( !has_post_thumbnail( $this->getQueriedId() ) )
        {
            return false;
        }
        $img = get_post( get_post_thumbnail_id( $this->getQueriedId() ) );
        return $this::getAdapter()->parse( 'image', $img );
    }

    protected function getQueriedDateCreated()
    {
        return get_the_date( get_option( 'date_format' ), $this->getQueriedId() );
    }

    protected function getQueriedDateCreatedGmt()
    {
        $date = $this->getQueriedDateCreated();
        if ( $date === false )
        {
            return false;
        }
        return get_date_from_gmt( $date, get_option( 'date_format' ) );
    }

    protected function getQueriedDateModified()
    {
        return get_the_modified_date( get_option( 'date_format' ),
            $this->getQueriedId() );
    }

    protected function getQueriedDateModifiedGmt()
    {
        $date = $this->getQueriedDateModified();
        if ( $date === false )
        {
            return false;
        }
        return get_date_from_gmt( $date, get_option( 'date_format' ) );
    }

    protected function getQueriedAuthor()
    {
        $author_id = get_post_field( 'post_author', $this->getQueriedId() );
        if ( $author_id === '' )
        {
            return false;
        }
        $user_object = new \WP_User( (int) $author_id );
        return $this::getAdapter()->parse( 'user', $user_object );
    }

    protected function getQueriedCategoryCount()
    {
        $count = 0;
        if ( !$this->getQueriedId() )
        {
            // No categories on 404. WordPress lies.
            return false;
        }
        $categories = wp_get_post_categories( $this->getQueriedId() );
        if ( is_array( $categories ) )
        {
            $count = count( $categories );
        }
        return $count;
    }

    protected function getQueriedCategories()
    {
        if ( !$this->getQueriedId() )
        {
            // No categories on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'category', $terms );
    }

    protected function getQueriedTags()
    {
        if ( !$this->getQueriedId() )
        {
            // No tags on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'tag', $terms );
    }

    protected function getQueriedTerms()
    {
        if ( !$this->getQueriedId() )
        {
            // No terms on 404. WordPress lies.
            return false;
        }
        $terms = $this->term_query->get_terms();
        return $this::getAdapter()->aggregate( 'term', $terms );
    }

    protected function getQueriedVisibility()
    {
        return $this::getAdapter()->evaluate( 'visibility', $this->query );
    }

    protected function getQueriedPasswordProtection()
    {
        return $this::getAdapter()->evaluate( 'post-password', $this->query );
    }

    protected function getQueriedContent()
    {
        if ( !$this->target_post )
        {
            return false;
        }
        return $this->target_post->get( 'content' );
    }

    protected function getQueriedRawContent()
    {
        if ( !$this->target_post )
        {
            return false;
        }
        return $this->target_post->get( 'raw_content' );
    }

    protected function getQueriedDebugContent()
    {
        if ( !$this->target_post )
        {
            return false;
        }
        return $this->target_post->get( 'debug_content' );
    }

    protected function getQueriedComments()
    {
        if ( !$this->getQueriedCommentsEnabled() || $this->getQueriedCommentCount()
            === 0 )
        {
            return false;
        }
        if ( defined( 'WP_DEBUG' ) && WP_DEBUG && current_user_can( 'administrator' ) )
        {
//            d( 'Awww shit. Someone forgot to write the comment aggregator. Get on that guy.' );
        }
        return false;
    }

    protected function getQueriedCommentCount()
    {
        return (int) get_comments_number( $this->getQueriedId() );
    }

    protected function getQueriedCommentsEnabled()
    {
        if ( !$this->getQueriedPostType() )
        {
            return false;
        }
        return comments_open( $this->getQueriedId() );
    }

    /**
     * Runs the indexing operations for the page populator
     *
     * @return void
     * @internal
     */
    private function _indexQuery()
    {
        if ( $this->query->is_singular )
        {
            $this->target_post = $this::getAdapter()->parse( 'post',
                $this->query->post );
        }
        foreach ( self::$page_property_mapping as
            $key =>
            $method )
        {
            $this->details[$key] = $this->$method();
        }
    }

}
