<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\populator;

/**
 * Login Populator
 *
 * Populates a set of page details for the login page.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @note Any alterations to the display of the login page by this theme are
 *     strictly opt-in for end users.
 *     The theme presents the capability of theming the login page
 *     to reflect the style, structure and branding of the frontend,
 *     but does not do this by default. The option to do so has to be
 *     explicitly enabled in the settings panel.
 * @note If you are writing a plugin or child theme and want to alter
 *     the display of the login page, you can hook into this with the
 *     Extension Api provided with this package. This presents a safe way to
 *     alter the visual presentation of the login without borking any of
 *     the internal Wordpress logic and locking yourself out of the login.
 * @final
 */
final class LoginPopulator
extends AbstractPagePopulator
{
    /**
     * Returns the identifying key of the worker.
     *
     * @return string "login"
     */
    public function getSubjectKey()
    {
        return 'login';
    }
}
