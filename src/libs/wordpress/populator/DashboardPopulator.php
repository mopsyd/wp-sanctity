<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\populator;

/**
 * Dashboard Populator
 *
 * Populates a set of page details for an admin dashboard page view.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @note By default, Sanctity does not use this information by default,
 *     and this class is a placeholder for child class and plugin extension.
 *     It still collects the details for admin page views as a convenience
 *     to extending logic, but nothing in this package leverages this information
 *     to alter the standard dashboard experience.
 * @note If you are writing a plugin or child theme and want to alter the
 *     display of the Wordpress dashboard, you can hook into this with the
 *     Extension Api provided with this package. This presents a safe way to
 *     alter the visual presentation of the dashboard without borking any of
 *     the internal Wordpress logic that is required to make the site run smoothly.
 * @final
 */
final class DashboardPopulator
extends AbstractPagePopulator
{
    /**
     * Returns the identifying key of the worker.
     *
     * @return string "admin"
     */
    public function getSubjectKey()
    {
        return 'admin';
    }
}
