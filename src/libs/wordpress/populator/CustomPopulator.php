<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress\populator;

/**
 * Default Custom Post Populator
 *
 * This is the default behavior of custom post types that
 * do not have their own page population logic.
 *
 * This is essentially identical to the post populator,
 * with a couple of additional internal checks to see
 * if any information about the context can be gleaned
 * that allows the system to make a concrete determination
 * as to the purpose of the custom post type.
 * If no absolute determination can be made,
 * it will render identically to a standard post.
 *
 * This system aims not to make any assumptions.
 * If it can't determine exactly what you want it
 * does whatever the default expected behavior
 * would be.
 *
 * Custom post support is slated to be added for any/all
 * custom post types added by official Wordpress plugins
 * like Askimet or Jetpack, and several other common custom
 * types that have wide usage, such as woocommerce products.
 * An api will be provided for plugin authors to hook into
 * programmatically to provide additional integrations,
 * but that is on the plugin authors to contribute.
 *
 * Officially supported custom post types will have their
 * own page populator class dedicated to their post type,
 * and will not use this one if one is present to handle it.
 * This is just the last line default for unknowns for the most part.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class CustomPopulator
extends AbstractPagePopulator
{
    /**
     * Returns the identifying key of the worker.
     *
     * @return string "custom"
     */
    public function getSubjectKey()
    {
        return 'custom';
    }
}
