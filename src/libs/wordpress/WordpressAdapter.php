<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\wordpress;

/**
 * Wordpress Adapter
 *
 * Provides an object oriented wrapper for the Wordpress API.
 *
 * It creates a layer of separation between Wordpress
 * mechanics and their underlying need within the rest
 * of the system, and insures that requested tasks are
 * always done in a standardized way.
 *
 * All of the internals of Sanctity use this object as a gateway to all
 * Wordpress functionality, which leaves the core logic decoupled from
 * the nuances of Wordpress, so it can operate independently.
 *
 * This class is referenced by most other classes that require interaction
 * with Wordpress functionality for any purpose, and mediates that interaction
 * without exposing the internals to classes depending on it.
 *
 * This allows the system to maintain a consistent api everywhere else
 * regardless of what happens in terms of the Wordpress Api.
 *
 * This is probably the second most important class in this package,
 * immediately after the front facing Sanctity class that everything
 * initially starts from. If you are developing anything on top of this package,
 * become very familiar with the api for this class,
 * and use it for basically everything if you are adding any logic
 * that interacts directly with Sanctity internals.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class WordpressAdapter
    extends \mopsyd\sanctity\adapters\platform\AbstractPlatformAdapter
    implements \mopsyd\sanctity\interfaces\libs\wordpress\WordpressAdapterInterface
{

    /**
     * The Wordpress Adapter uses the adapter config (duh).
     */
    const CONFIG_FILE = 'adapter.json';

    /**
     * The classes used to get info about wordpress stuff.
     *
     * The logic within these classes is used to decouple the data provided
     * by native Wordpress functions and objects from the underlying MVC logic,
     * so it can leverage the adapter to obtain details without having to
     * directly know anything about WordPress. This makes the notion of running
     * this beast on another platform approachable, because none of the WordPress
     * internals have anything to do with the driving logic, so porting it should
     * only incur writing another adapter, and not rewriting the entire MVC layer.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $getter_classes;

    /**
     * The classes used to set options and other info into WordPress.
     * These are usually the same classes as the getters, but they are
     * tracked separately (even when they are the same), so read-only
     * operations are not presented with setter logic.
     *
     * The logic within these classes is used to decouple the data provided
     * by native Wordpress functions and objects from the underlying MVC logic,
     * so it can leverage the adapter to set details without having to
     * directly know anything about WordPress. This makes the notion of running
     * this beast on another platform approachable, because none of the WordPress
     * internals have anything to do with the driving logic, so porting it should
     * only incur writing another adapter, and not rewriting the entire MVC layer.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $setter_classes;

    /**
     * The classes used to parse wordpress stuff.
     *
     * The logic within these classes is used to decouple the data provided
     * by native Wordpress functions and objects from the global scope,
     * and create an encapsulated representation of it that has no liability
     * of affecting the actual underlying object or functionality
     * it was derived from.
     *
     * This system does not use globals, does not pass globals around
     * or operate on them in any context that is not strictly neccessary,
     * and all of the rest of this system depends on this adapter class
     * to perform all underlying Wordpress operations needed to operate correctly.
     *
     * All of the rest of the logic in this package aside from this adapter
     * and its dependencies is entirely decoupled from direct interaction
     * with Wordpress, so that correct usage of the Wordpress API is mandatory,
     * and any updates to the Wordpress API that occur that would break
     * this package only require refactoring one class instead of picking
     * through the entire build to fix.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $parser_classes;

    /**
     * These are classes used to aggregate parsing of groups of stuff.
     *
     * These classes perform bulk aggregation operations using parsers.
     * Where a parser would extract the data for a single post or comment,
     * an aggregator will batch groups of these, and individually leverage
     * the parsers needed to generate a correct representation of each
     * individual set.
     *
     * Aggregators are tasked with working directly with Wordpress objects,
     * and insuring that they are properly reset prior to returning any output.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $aggregator_classes;

    /**
     * These are classes used to make concrete determinations
     * about what a thing is, and ought to look like in the
     * presentation layer.
     *
     * These classes provide logical determinations about output
     * and control flow, which the rest of the system leverages
     * to insure that display looks exactly as intended with no ambiguity.
     *
     * Values returned by evaluators are used for control flow checks
     * and determining the correct execution strategy for the underlying system.
     * In general, typical Wordpress terminology is used for all of this,
     * however there are a few custom parameters added to remove ambiguity
     * that would typically exist using strictly vanilla Wordpress logic.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The classes in this array represent singular properties of
     *     the output object that can have multiple conditions required
     *     to make an absolute determination about them.
     */
    private static $evaluator_classes;

    /**
     * These are classes that create sets of raw render data
     * to be used for display purposes. All of the data returned
     * from these is in a raw, unfiltered state. It is passed through
     * filters and sanitization appropriately prior to render based on:
     *
     * - Data security and integrity,
     * - Wordpress best practices,
     * - System performance,
     * - end user preference,
     * - and plugin or child theme custom extension
     *
     * in that order of priority.
     *
     * These get factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $populator_classes;

    /**
     * These are classes that register settings, options, menus, sidebars,
     * and pretty much anything else that needs to be registered, enqueued,
     * or otherwise entered into the Wordpress api as a persistent declaration
     * either to go to the database or the output through the Wordpress api.
     *
     * Sanctity is configured in such a way that ALL registration keys provided
     * to these methods must be provided EXACTLY the same way to unregister.
     * Whereas Wordpress itself lets you unregister by key, this logic
     * ABSOLUTELY requires that you provide an identical data set
     * to unregister anything, to insure that no content ambiguity exists
     * and intent is explicit. Logic at the Model level will typically
     * resolve this internally, so the public facing api still only
     * needs to present the key. However, if the key cannot resolve
     * to a valid registration, then these classes WILL bounce the
     * attempt prior to calling any Wordpress internals, which will
     * then cascade back to the public api for the implementing
     * programmer to handle.
     * There is a zero tolerance policy for content ambiguity.
     * It's right or else it doesn't get registered OR unregistered.
     * If a provisional implementer is not capable of providing ALL of
     * the original details for the registration process, it is assumed
     * that segment of logic is not authorized to make the unregistration
     * request.
     *
     * These classes are factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $register_classes;

    /**
     * These are classes that filter, format, sanitize, and escape output
     * for final render. All data goes through one or more of these prior
     * to display. In most cases, these will fire the typical methods that
     * the Wordpress api designates as appropriate sanitization and filter
     * methods for the given content. In cases where a native method does not exist,
     * customized logic is included to insure data integrity and data safety.
     *
     * Sanctity is configured in such a way that literally ALL data that is
     * handled by the theme MUST be passed through sanitization prior to
     * being made available to any other part of the system.
     * There is a zero tolerance policy for content ambiguity.
     * It's right or else it doesn't display and can't be accessed.
     *
     * These classes are factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $sanitizer_classes;

    /**
     * These are classes that provide various Wordpress Query type objects from
     * primitive values to facilitate interoperability and cohesion between workers,
     * which typically all require an explicit object as their
     * actionable parameter otherwise.
     *
     * Each of these supports the native query api fully,
     * and additionally provides an extra compatibility layer.
     *
     * These classes are factorized on the fly as needed.
     *
     * @var array
     *
     * @note The values in this array are not used directly at runtime.
     *     These are the core level classes that cannot be overridden and
     *     are always present. These are packaged into a corresponding
     *     container of available options upon initialiation, which can
     *     be extended using the provided Extension Api,
     *     though it cannot override core classes or their
     *     internal operations.
     */
    private static $query_classes;

    /**
     * What would Wordpress be without widgets?
     *
     * These classes are factorized on the fly as needed.
     *
     * @var array
     */
    private static $widget_classes;

    /**
     * A containerized set of getter class names.
     * This is what is referenced when a get operation occurs.
     *
     * This exists so that extensions can add custom getters.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $getters;

    /**
     * A containerized set of setter class names.
     * This is what is referenced when a set operation occurs.
     *
     * This exists so that extensions can add custom setters.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $setters;

    /**
     * A containerized set of parser class names.
     * This is what is referenced when a parse operation occurs.
     *
     * This exists so that extensions can add custom parsers.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $parsers;

    /**
     * A containerized set of aggregator class names.
     * This is what is referenced when an aggregate operation occurs.
     *
     * This exists so that extensions can add custom aggregators.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $aggregators;

    /**
     * A containerized set of evaluator class names.
     * This is what is referenced when an evaluate operation occurs.
     *
     * This exists so that extensions can add custom evaluators.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $evaluators;

    /**
     * A containerized set of populator class names.
     * This is what is referenced when an populate operation occurs.
     *
     * This exists so that extensions can add custom populators.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $populators;

    /**
     * A containerized set of register class names.
     * This is what is referenced when a register or unregister operation occurs.
     *
     * This exists so that extensions can add custom registers.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $registers;

    /**
     * A containerized set of sanitizer class names.
     * This is what is referenced when a sanitize operation occurs.
     *
     * This exists so that extensions can add custom sanitizers.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $sanitizers;

    /**
     * A containerized set of sanitizer class names.
     * This is what is referenced when a sanitize operation occurs.
     *
     * This exists so that extensions can add custom sanitizers.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $queries;

    /**
     * A containerized set of widget class names.
     * These are created dynamically though external configuration.
     * The adapter presents a much more user-friendly means of initializing
     * custom widgets with complex options.
     *
     * This exists so that extensions can add custom widgets.
     * WooHoo! Widgets! Widgets for everyone!
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private static $widgets;

    /**
     * Determines if the Wordpress Adapter is initialized.
     *
     * @var bool
     */
    private static $is_initialized = false;

    /**
     * <Wordpress Adapter Constructor>
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $dependencies (optional)
     *     The WordpressAdapter may receive an instance of the primary
     *     factory in its constructor if one has not been set statically.
     *
     * @param array $args Any arguments to supply to the adapter upon instantiation.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If no factory is available by the end of instantiation,
     *     indicating that one was not properly injected either
     *     statically or through the constructor.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        //  __---``````````````---__            This is a tank.
        if ( is_null( $dependencies ) //          There are many like it,
            && is_object( $dependencies ) //                                           //_-````--__
            && ( $dependencies instanceof \mopsyd\sanctity\factory\FactoryFactory ) ) // PEW PEW PEW -====---
        {                                 //                                          \\`-_____--``
            $this::setFactory( $dependencies ); //       but this one is mine.
        }                                 //
        $this->_initializeWordpressEnvironment(); //        War is hell.
        $this->_modPlatform();                     //
        parent::__construct( $dependencies, $args ); //
    }

// ooooooooooooooooooooooooooooooooooooooooooooo~~~~~    I wanted to be a beat poet dammitall.   ~~~~~~~~~

    /**
     * Gets stuff. Options, settings, lists of plugins, sidebars, etc.
     * I shouldn't have to explain what a getter is.
     *
     * @param string $command A command corresponding to a getter
     *     identifying key known to the Adapter
     * @param string $subject The identifying key to ask the getter for.
     * @param string $subject (optional) An optional default value to return
     *     if no value is found. If not supplied you get nothing (null).
     *
     * @return mixed Returns the result as is.
     *      Does not package it in any particular way.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known parsers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested parse operation
     *     (hint, it's probably a string in every possible case).
     */
    public function get( $command, $subject = null, $default = null )
    {
        if ( !self::$getters->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown getter format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $getter = $this->load( 'library', self::$getters->get( $command ) );
        $this->_typecheckParameter( $subject, $getter );
        $result = $getter->get( $subject, $default );
        return $result;
    }

    /**
     * Sets stuff. Options, settings, etc.
     * I shouldn't have to explain what a setter is either.
     *
     * @param string $command A command corresponding to a setter
     *     identifying key known to the Adapter
     * @param string $subject The identifying key to set.
     * @param mixed $value The value to set on the provided key.
     *
     * @return mixed Usually doesn't return anything.
     *     Might occasionally return bool, depending on the setter.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known parsers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested parse operation
     *     (hint, it's probably a string in every possible case).
     */
    public function set( $command, $subject, $value )
    {
        if ( !self::$setters->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown setter format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $setter = $this->load( 'library', self::$setters->get( $command ) );
        $this->_typecheckParameter( $subject, $setter );
        $result = $setter->set( $subject, $value );
        return $result;
    }

    /**
     * Performs parse operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a parsed set of raw unfiltered data by leveraging
     * a parser worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to a parser
     *     identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known parsers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested parse operation.
     */
    public function parse( $command, $subject = null )
    {
        if ( !self::$parsers->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown parser format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $parser = $this->load( 'library', self::$parsers->get( $command ) );
        $this->_typecheckParameter( $subject, $parser );
        $parsed = $parser->parse( $subject );
        return $parsed;
    }

    /**
     * Performs population operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a populated set of raw unfiltered data by leveraging
     * a populator worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to an aggregator
     *     identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known aggregators
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested aggregate operation.
     */
    public function aggregate( $command, $subject )
    {
        if ( !self::$aggregators->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown aggregator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $aggregator = $this->load( 'library',
            self::$aggregators->get( $command ) );
        $this->_typecheckParameter( $subject, $aggregator );
        $aggregated = $aggregator->aggregate( $subject );
        return $aggregated;
    }

    /**
     * Performs population operation upon a supplied raw \WP_Query,
     * \WP_Term, \WP_User, or other related Wordpress object,
     * and produces a populated set of raw unfiltered data by leveraging
     * a populator worker dedicated to the specific command in question.
     *
     * @param string $command A command corresponding to a populator
     *     identifying key known to the Adapter
     * @param object $subject The subject matter that the command is expected to operate against.
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known populators
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided subject is the wrong type
     *     for the requested poulate operation.
     */
    public function populate( $command, $subject )
    {
        if ( !self::$populators->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown populator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $populator = $this->load( 'library', self::$populators->get( $command ) );
        $this->_typecheckParameter( $subject, $populator );
        $populated = $populator->populate( $subject );
        return $populated;
    }

    /**
     * Performs an evaluation operation upon the subject by leveraging
     * a dedicated evaluation worker.
     *
     * @param string $command A command corresponding to a evaluate
     *     identifying key known to the Adapter
     * @param \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface $subject
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known sanitizers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested sanitization operation.
     */
    public function evaluate( $command, $subject )
    {
        if ( !self::$evaluators->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown evaluator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $evaluator = $this->load( 'library', self::$evaluators->get( $command ) );
        $this->_typecheckParameter( $subject, $evaluator );
        $evaluated = $evaluator->evaluate( $subject );
        return $evaluated;
    }

    /**
     * Performs registration of a supplied entity, so it is entered into Wordpress
     * for persistent handling of some sort.
     *
     * @param string $command A command corresponding to a register
     *     identifying key known to the Adapter
     * @param mixed $subject Almost always an array.
     *
     * @return bool Returns true on successful operation
     *     and false on unsuccessful operation. A value of false indicates that
     *     the parameters were correctly formatted, but registration could not
     *     occur for some reason.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known registers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided paraneter is the wrong instance type
     *     for the requested registration operation.
     */
    public function register( $command, $subject )
    {
        if ( !self::$registers->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown evaluator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $register = $this->load( 'library', self::$registers->get( $command ) );
        $this->_typecheckParameter( $subject, $register );
        $registered = $register->register( $subject );
        return $registered;
    }

    /**
     * Performs unregistration of a supplied set, a previously registered
     * entity of some sort is removed from Wordpress.
     *
     * @param string $command A command corresponding to a register
     *     identifying key known to the Adapter
     * @param mixed $subject Almost always an array. This should be identical
     *     to the same value provided for the registration step.
     *     Single keys are not acceptable, calling logic is expected
     *     to provide exactly identical parameters for unregistration
     *     to be processed.
     *
     * @return bool Returns true on successful operation
     *     and false on unsuccessful operation. A value of false indicates that
     *     the parameters were correctly formatted, but unregistration could not
     *     occur for some reason.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known registers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided paraneter is the wrong instance type
     *     for the requested registration operation.
     */
    public function unregister( $command, $subject )
    {
        if ( !self::$registers->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown evaluator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $register = $this->load( 'library', self::$registers->get( $command ) );
        $this->_typecheckParameter( $subject, $register );
        $unregistered = $register->unregister( $subject );
        return $runegistered;
    }

    /**
     * Performs sanitization upon a previously populated set of raw data,
     * and produces a render-ready result collection or container.
     *
     * @param string $command A command corresponding to a sanitizer
     *     identifying key known to the Adapter
     * @param \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface $subject
     *
     * @return \mopsyd\sanctity\interfaces\libs\wordpress\container\WordpressContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known sanitizers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested sanitization operation.
     */
    public function sanitize( $command, $subject )
    {
        if ( !self::$sanitizers->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown evaluator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $sanitizer = $this->load( 'library', self::$sanitizers->get( $command ) );
        $this->_typecheckParameter( $subject, $sanitizer );
        $sanitized = $sanitizer->sanitize( $subject );
        return $sanitized;
    }

    /**
     * Performs sanitization upon a previously populated set of raw data,
     * and produces a render-ready result collection or container.
     *
     * @param string $command A command corresponding to a query
     *     identifying key known to the Adapter
     *     [query, comment, term, user]
     * @param mixed $subject Query args relevant to the specified query type
     *
     * @return \WP_Query|\WP_Comment_Query|\WP_Term_Query|\WP_User_Query etc.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key is not amongst any known query handlers
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided container is the wrong instance type
     *     for the requested query operation.
     */
    public function query( $command, $subject )
    {
        if ( !self::$queries->has( (string) $command ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Unknown evaluator format [%1$s] in [%2$s]', $command,
                get_class( $this )
            ) );
        }
        $query = $this->load( 'library', self::$queries->get( $command ) );
        $this->_typecheckParameter( $subject, $query );
        $results = $query->query( $subject );
        return $results;
    }

    /**
     * Returns the View object, so certain edge case workers
     * (ahem, widgets) can leverage the RenderAdapter, without
     * requiring their own factorization logic.
     *
     * @return \mopsyd\sanctity\interfaces\views\ViewInterface
     */
    public static function getView()
    {
        return parent::getView();
    }

    // -------------------------------------------------------------------------
    //          In the unlikely event I ever make this class extendable
    // -------------------------------------------------------------------------

    /**
     * Designates the container class used by the Wordpress Adapter
     * for packaging dependency worker options to feed to the factory.
     *
     * @return string
     *
     * @see \mopsyd\sanctity\libs\container\AbstractContainer
     * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @see \mopsyd\sanctity\traits\ContainerUtility
     */
    protected static function declareContainerClass()
    {
        $class = get_called_class();
        if ( defined( $class . '::CONTAINER_CLASS' ) )
        {
            return $class::CONTAINER_CLASS;
        }
        return 'mopsyd\\sanctity\\libs\\wordpress\\container\\WordpressContainer';
    }

    // -------------------------------------------------------------------------
    //                              Internals
    // -------------------------------------------------------------------------

    /**
     * His is the hand that makes.
     * His is the hand that hurts.
     * His is the hand that heals.
     *
     * His is the House of Pain.
     *
     * @return void
     * @internal
     */
    private function _initializeWordpressEnvironment()
    {
        if ( !self::$is_initialized )
        {
            self::$aggregator_classes = $this->getConfig( 'aggregate' );
            self::$evaluator_classes = $this->getConfig( 'evaluate' );
            self::$getter_classes = $this->getConfig( 'getter' );
            self::$setter_classes = $this->getConfig( 'setter' );
            self::$parser_classes = $this->getConfig( 'parse' );
            self::$populator_classes = $this->getConfig( 'populate' );
            self::$register_classes = $this->getConfig( 'register' );
            self::$sanitizer_classes = $this->getConfig( 'sanitize' );
            self::$query_classes = $this->getConfig( 'query' );
            self::$widget_classes = $this->getConfig( 'widget' );
            $this->_initializeWorkerFactories();
            $this->_initializeWorkers();
            $this->_initializeContainers();
            $this->_initializeGetters();
            $this->_initializeSetters();
            $this->_initializeParsers();
            $this->_initializeAggregators();
            $this->_initializeEvaluators();
            $this->_initializePopulators();
            $this->_initializeRegisters();
            $this->_initializeQueries();
            $this->_initializeWidgets();
            $this->_modPlatform();
            self::$is_initialized = true;
        }
    }

    /**
     * Move sixteen tons, and what do you get?
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $factory
     * @return void
     * @internal
     */
    private function _initializeWorkers()
    {
        AbstractWordpressWorker::setAdapter( $this );
        widget\AbstractWidget::setAdapter( $this );
    }

    /**
     * We've got to move these refrigerators.
     * We've got to move these color tv's.
     *
     * @return void
     * @internal
     */
    private function _initializeContainers()
    {
        container\AbstractWordpressContainer::setAdapter( $this );
    }

    /**
     * Gotta get me some of that
     *
     * @return void
     * @internal
     */
    private function _initializeGetters()
    {
        self::$getters = $this::containerize( self::$getter_classes );
    }

    /**
     * Almost as cool as Osiris
     *
     * @return void
     * @internal
     */
    private function _initializeSetters()
    {
        self::$setters = $this::containerize( self::$setter_classes );
    }

    /**
     * Come together, right now
     * Over me
     *
     * @return void
     * @internal
     */
    private function _initializeAggregators()
    {
        self::$aggregators = $this::containerize( self::$aggregator_classes );
    }

    /**
     * If all printers were determined not to print anything
     * till they were sure it would offend nobody,
     * there would be very little printed.
     *
     * @return void
     * @internal
     */
    private function _initializeParsers()
    {
        self::$parsers = $this::containerize( self::$parser_classes );
    }

    /**
     * The tree of research must be fed from time to time
     * with the blood of bean-counters, for it is its natural manure.
     *
     * @return void
     * @internal
     */
    private function _initializeEvaluators()
    {
        self::$evaluators = $this::containerize( self::$evaluator_classes );
    }

    /**
     * Iam pridem, ex quo suffragia nulli uendimus, effudit curas;
     * nam qui dabat olim imperium, fasces, legiones, omnia, nunc se
     * continet atque duas tantum res anxius optat, panem et circenses.
     *
     * @return void
     * @internal
     */
    private function _initializePopulators()
    {
        self::$populators = $this::containerize( self::$populator_classes );
    }

    /**
     * If you ladies leave my island, if you survive recruit training,
     * you will be a weapon. You will be a minister of death praying for war.
     * But until that day you are pukes.
     *
     * @return void
     * @internal
     */
    private function _initializeRegisters()
    {
        self::$registers = $this::containerize( self::$register_classes );
    }

    /**
     * It is a capital mistake to theorize before one has data.
     *
     * @return void
     * @internal
     */
    private function _initializeQueries()
    {
        self::$queries = $this::containerize( self::$query_classes );
    }

    /**
     * The whole is more than the sum of its parts.
     *
     * That's what we're shooting for anyhow.
     *
     * @return void
     * @internal
     */
    private function _initializeWidgets()
    {
        self::$widgets = $this::containerize( self::$widget_classes );
    }

    /**
     * Welcome my friends, welcome to my chocolate factory.
     *
     * @return void
     * @internal
     */
    private function _initializeWorkerFactories()
    {
        AbstractWordpressWorker::setFactory( $this::getFactory() );
        widget\AbstractWidget::setFactory( $this::getFactory() );
    }

    /**
     * He's making a list, and checking it twice.
     *
     * @param mixed $param
     * @param \mopsyd\sanctity\interfaces\libs\wordpress\WordpressWorkerInterface $worker
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given parameter does not match the declared actionable
     *     argument from the worker.
     * @internal
     */
    private function _typecheckParameter( $param,
        \mopsyd\sanctity\interfaces\libs\wordpress\WordpressWorkerInterface $worker )
    {
        // This insures this f***ing persistent plague is ALWAYS removed,
        // no matter how many times it gets re-registered.
        // Since every single adapter call calls this method,
        // and every single thing WordPress does gets deferred to this adapter,
        // this will basically run on pretty much every single thing it
        // could possibly run on.
        $this->_killWpAutoPWithFire();
        $expected = $worker->getSubjectType();              //It's kinda funny
        //that this method looks
        if ( $expected === 'mixed' )                        //sorta like a toilet.
        {                                                   //because it's only job
            return;                                         //is to flush sloppy code.
        }
        //      Irony.
        if ( !( strtolower( gettype( $param ) ) === strtolower( $expected )
            || ( is_object( $param ) && ( $param instanceof $expected ) ) ) )
        {
            //``````````````````````````````````````````//
            //   Bzzzt! Wrong! Try again dumbass!       //
            //                                          //
            //   - That teacher guy from South Park.    //
            //                                          //
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                'Invalid parameter supplied in [%1$s] for [%4$s]. '
                . 'Expected [%2$s] but received [%3$s].', get_class( $this ),
                $expected,
                ( is_object( $param )
                    ? get_class( $param )
                    : gettype( $param ) ), get_class( $worker )
            )
            );
        }
    }

    private function _modPlatform()
    {
        $this->_killWpAutoPWithFire();
    }

    private function _killWpAutoPWithFire()
    {
        // KILL. IT. WITH. FIRE.
        // This damn thing keeps getting arbitrarily turned back on. No.
        // Everyone hates this thing. Do not f*** up peoples content.
        // I have no idea why WordPress thinks it's a good idea to
        // butcher markup, and is so persistent in turning it back
        // on when it is disabled, but this is going to
        // fire on EVERY SINGLE ADAPTER CALL
        // to insure that this goes the f*** away properly.
        remove_filter( 'the_content', 'wpautop' );
        remove_filter( 'the_excerpt', 'wpautop' );
    }

}
