<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig\environment;

/**
 * Abstract Twig Environment
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractTwigEnvironment
    extends \Twig_Environment
    implements \mopsyd\sanctity\interfaces\SanctityInterface
{
    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

    /**
     * The render adapter, which determines the correct paths to load.
     *
     * @var \mopsyd\sanctity\libs\twig\TwigAdapter
     */
    private static $render_adapter;

    /**
     * If this is false, it will trigger the
     * internal initialization upon construction.
     *
     * @var bool
     */
    private static $is_initialized = false;

    public function __construct( \Twig_LoaderInterface $loader, array $options = array() )
    {
        $this->_initializeTwigThemeEnvironment();
        parent::__construct( $loader, $options );
    }

    public static function setThemeAdapter( \mopsyd\sanctity\libs\twig\TwigAdapter $adapter )
    {
        self::$render_adapter = $adapter;
    }

    private function _initializeTwigThemeEnvironment()
    {
        if (!self::$is_initialized)
        {
            //no-op
            self::$is_initialized = true;
        }
    }
}
