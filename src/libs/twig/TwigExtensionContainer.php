<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig;

/**
 * Twig Extension Container
 *
 * This is a container for Twig extensions.
 *
 * This is used by the Twig Adapter to create a separation layer between
 * itself and the extensions that it manages for Twig.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
final class TwigExtensionContainer
    extends \mopsyd\sanctity\libs\container\AbstractContainer
    implements \mopsyd\sanctity\interfaces\libs\container\TwigExtensionContainerInterface
{

    /**
     * <Twig Extension Container Setter Trigger Method>
     * Prevents the container from accepting parameters that are not valid Twig Extensions,
     * or keys that are not strings, so it is fully Psr-11 compliant.
     *
     * @param string $key The passed key that the setter is supposed to have
     *
     * @param string|\Twig_ExtensionInterface $value An instantiated Twig extension,
     *     or the class name of one that resolves to an instantiated Twig extension
     * @param string $method The internal method that the operation arose from
     *
     * @return void
     *
     * @throws \mopsyd\sanctity\libs\exception\ContainerException
     *     A container exception is raised if the key is not a string,
     *     or the provided value is not a class implementing the \Twig_ExtensionInterface,
     *     or the string representation of one that can be instantiated
     *     without dependencies
     */
    protected function _onSet( $key, $value, $method = null )
    {
        if ( is_object( $value ) )
        {
            $value = get_class( $value );
        }
        if ( !is_string( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Invalid dependency supplied in [%1$s]. '
                . 'Provided key must be a string.', get_class( $this ) )
            );
        }
        if ( !class_exists( $value ) && in_array( $this::EXPECTED_INTERFACE,
                class_implements( $value ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Invalid dependency supplied in [%1$s]. '
                . 'Provided class [%2$s] identified by key '
                . '[%3$s] must be an instance of [%4$s].', get_class( $this ),
                $value, $key, $this::EXPECTED_INTERFACE )
            );
        }
    }

    /**
     * <Twig Extension Container Getter Value Filter Method>
     * Returns a fully instantiated Twig_Extension if the provided value
     * was not already an instance.
     *
     * This filter forces the container to always handle correct instantiation
     * of the dependencies it receives, and always return an instantiated instance.
     *
     * @param string $key The identifying key of the twig extension,
     *     typically its fully qualified class name
     *
     * @param string|\Twig_ExtensionInterface $value An instantiated Twig extension,
     *     or a string representing one that needs to be instantiated.
     *
     * @param string $method The internal method that the operation arose from.
     *
     * @return \Twig_ExtensionInterface Always returns an instantiated instance.
     *     Does not reinstantiate instances that are already instantiated.
     */
    protected function _filterGet( $key, $value, $method = null )
    {
        if ( !is_string( $value ) )
        {
            $value = new $value();
        }
        return $value;
    }

}
