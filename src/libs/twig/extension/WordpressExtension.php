<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig\extension;

/**
 * Wordpress Twig Extension
 *
 * Provides support for a number of native Wordpress methods
 * to be used directly in Twig templates.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 * @note You are in for a world of pain if you try to
 *     use the token parser override.
 *     Don't do it. Fair warning.
 */
final class WordpressExtension
    extends AbstractTwigExtension
{
    /**
     * Returns the token parser instances to add to the existing list.
     *
     * @return Twig_TokenParserInterface[]
     */
//    public function getTokenParsers();

    /**
     * Returns the node visitor instances to add to the existing list.
     *
     * @return Twig_NodeVisitorInterface[]
     */
//    public function getNodeVisitors();

    /**
     * Returns a list of filters to add to the existing list.
     *
     * @return Twig_Filter[]
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter( 'absint', 'absint' ),
            new \Twig_SimpleFilter( 'antispambot', 'antispambot' ),
            new \Twig_SimpleFilter( 'body_class', 'body_class' ),
            new \Twig_SimpleFilter( 'get_bloginfo', 'get_bloginfo' ),
            new \Twig_SimpleFilter( 'get_blogs_of_user', 'get_blogs_of_user' ),
            new \Twig_SimpleFilter( 'convert_smilies', 'convert_smilies' ),
            new \Twig_SimpleFilter( 'deslash', 'deslash' ),
            new \Twig_SimpleFilter( 'dynamic_sidebar', 'dynamic_sidebar' ),
            new \Twig_SimpleFilter( 'shortcode', 'do_shortcode' ),
            new \Twig_SimpleFilter( 'translate', '__'),
            new \Twig_SimpleFilter( 'esc_url', 'esc_url' ),
            new \Twig_SimpleFilter( 'esc_html', 'esc_html' ),
            new \Twig_SimpleFilter( 'filter_ssl', 'filter_ssl' ),
            new \Twig_SimpleFilter( 'get_author_posts_url', 'get_author_posts_url' ),
            new \Twig_SimpleFilter( 'get_avatar_url', 'get_avatar_url' ),
            new \Twig_SimpleFilter( 'urlencode', 'urlencode' ),
            new \Twig_SimpleFilter( 'sanitize_title', 'sanitize_title' ),
            new \Twig_SimpleFilter( 'sanitize_key', 'sanitize_key' ),
            new \Twig_SimpleFilter( 'sanitize_key', 'sanitize_key' ),
            new \Twig_SimpleFilter( 'wp_head', 'wp_head' ),
            new \Twig_SimpleFilter( 'wp_footer', 'wp_footer' ),
        );
    }

    /**
     * Returns a list of tests to add to the existing list.
     *
     * @return Twig_Test[]
     */
    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('author_can', 'author_can'),
            new \Twig_SimpleTest('comments_open', 'comments_open'),
            new \Twig_SimpleTest('current_user_can', 'current_user_can'),
            new \Twig_SimpleTest('email_exists', 'email_exists'),
        );
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction( 'absint', 'absint' ),
            new \Twig_SimpleFunction( 'admin_url', 'admin_url' ),
            new \Twig_SimpleFunction( 'apply_filters', 'apply_filters' ),
            new \Twig_SimpleFunction( 'antispambot', 'antispambot' ),
            new \Twig_SimpleFunction( 'comment_form', 'comment_form' ),
            new \Twig_SimpleFunction( 'convert_smilies', 'convert_smilies' ),
            new \Twig_SimpleFunction( 'count_user_posts', 'count_user_posts' ),
            new \Twig_SimpleFunction( 'deslash', 'deslash' ),
            new \Twig_SimpleFunction( 'dynamic_sidebar', 'dynamic_sidebar' ),
            new \Twig_SimpleFunction( 'esc_html', 'esc_html' ),
            new \Twig_SimpleFunction( 'feed_links', 'feed_links' ),
            new \Twig_SimpleFunction( 'feed_links_extra', 'feed_links_extra' ),
            new \Twig_SimpleFunction( 'get_author_posts_url', 'get_author_posts_url' ),
            new \Twig_SimpleFunction( 'get_avatar_url', 'get_avatar_url' ),
            new \Twig_SimpleFunction( 'get_bloginfo', 'get_bloginfo' ),
            new \Twig_SimpleFunction( 'get_blogs_of_user', 'get_blogs_of_user' ),
            new \Twig_SimpleFunction( 'get_background_color', 'get_background_color' ),
            new \Twig_SimpleFunction( 'get_background_image', 'get_background_image' ),
            new \Twig_SimpleFunction( 'shortcode', 'do_shortcode' ),
            new \Twig_SimpleFunction( '__', '__' ),
            new \Twig_SimpleFunction( 'get_search_form', 'get_search_form' )
        );
    }

    /**
     * Returns a list of operators to add to the existing list.
     *
     * @return array<array> First array of unary operators, second array of binary operators
     */
//    public function getOperators();
}
