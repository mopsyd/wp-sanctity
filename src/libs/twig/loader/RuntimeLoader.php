<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig\loader;

/**
 * Twig Extension RuntimeLoader
 *
 * This class allows overloading Twig extensions at runtime
 * via dependency injection.
 *
 * This class handles all of the dynamic extension overloading for the theme.
 * There is really not any room for extension here, and it would be particularly
 * disruptive if it were done wrong, so in the interest of enforcing the api,
 * this class is final, and does not provide room for extension.
 *
 * Extensions can be registered through the normal Sanctity extension api,
 * and they will wind up here if they authenticate correctly on their own.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class RuntimeLoader
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\SanctityInterface,
    \Twig_RuntimeLoaderInterface
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

    /**
     * The theme adapter, which determines the correct paths to load.
     *
     * @var \mopsyd\sanctity\libs\twig\TwigAdapter
     */
    private static $render_adapter;

    public function __construct( $dependencies = null, $args = array() )
    {

    }

    public static function setThemeAdapter( \mopsyd\sanctity\libs\twig\TwigAdapter $adapter )
    {
        self::$render_adapter = $adapter;
    }

    public function load( $class )
    {
        // implement the logic to create an instance of $class
        // and inject its dependencies
        // most of the time, it means using your dependency injection container
        if ( 'Project_Twig_RuntimeExtension' === $class )
        {
            return new $class( new Rot13Provider() );
        } else
        {
            // ...
        }
    }

    /**
     * Via the loader trait, for reference mostly.
     *
     * This method can be called directly on this class via the trait.
     *
     * @note The commented out copy is for reference, so you don't have to go
     *     digging through the trait folder to find it.
     *
     * @param type $name
     * @param type $type
     * @param type $dependencies
     * @param type $args
     * @return type
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
//    private function _loadClass( $name, $type, $dependencies = null, $args = array() )
//    {
//        $this->_typecheckLoaderClassName( $name );
//        $this->_typecheckLoaderClassType( $type );
//        foreach ( self::$_namespace_prefixes as
//            $prefix )
//        {
//            $class = $this->_constructClassName( $prefix, $name, $type );
//            if ( !$this->_validateType( $class, $type ) )
//            {
//                continue;
//            }
//            $instance = $class::init( $dependencies, $args );
//            return $instance;
//        }
//        //If all options have been exhausted, report an error.
//        throw new \mopsyd\sanctity\libs\exception\SanctityException(
//        sprintf( 'Class [%1$s] of type [%2$s] not found in any known namespace. Requested in [%3$s].',
//            $name, $type, get_class( $this ) )
//        );
//    }
}
