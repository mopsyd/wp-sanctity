<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig\filesystem;

/**
 * Description of Twig Filesystem Loader
 *
 * Integrates the Twig Filesystem Loader locally to the Wordpress
 * theme installation, and any child themes that may be active.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractTwigFilesystemLoader
    extends \Twig_Loader_Filesystem
    implements \mopsyd\sanctity\interfaces\SanctityInterface
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * The theme adapter, which determines the correct paths to load.
     *
     * @var \mopsyd\sanctity\libs\twig\TwigAdapter
     */
    private static $adapter;

    /**
     * An array of core paths obtained from the adapter.
     * These paths cannot be overridden.
     *
     * @var array
     */
    private static $core_paths;

    /**
     * The paths that get packaged for loading, which always include the core paths,
     * and any additional paths added after the fact upon instantiation.
     * @var array
     */
    private $runtime_paths;

    /**
     * Constructor.
     *
     * Takes a preliminary step to route paths internally
     * prior to calling the normal constructor.
     *
     * @param string|array $paths A path or an array of paths where to look for templates
     */
    public function __construct( $paths = array() )
    {
        $this->_initializePaths($paths);
        if ( $paths && !empty( $paths ) )
        {
            parent::setPaths();
            $this->setPaths( $paths );
        }
    }

    public static function setThemeAdapter( \mopsyd\sanctity\libs\twig\TwigAdapter $adapter )
    {
        self::$adapter = $adapter;
    }

    /**
     * Initializes the initial pathset if it is not already initialized,
     * and creates or updates the runtime path set based on the supplied paths.
     * @param type $paths
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If a provided path is not valid, or attempts to override a core path.
     * @internal
     */
    private function _initializePaths( $paths = array() )
    {
        if ( is_null( self::$core_paths ) )
        {
            self::$core_paths = $this->_getCorePaths();
            $this->runtime_paths = self::$core_paths;
        }
        $paths_to_add = array();
        foreach ( $paths as
            $key =>
            $path )
        {
            //Do not overwrite core paths.
            if ( !$this->_checkPathValid( $key, $path ) )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Cannot add Twig path [%1$s] identified by key [%2$s] in [%3$s] '
                    . 'because it is not readable or attempts to override a core path.',
                    $path, $key, get_class( $this ) )
                );
            }
            if ( array_key_exists( $key, $this->runtime_paths ) )
            {
                $this->runtime_paths[$key] = realpath( $path );
            } else
            {
                $this->runtime_paths = $this->runtime_paths + array(
                    $key => realpath( $path ) );
            }
        }
    }

    /**
     * Initializes the core paths required by the base level theme,
     * which cannot be overwritten.
     *
     * @return array
     * @internal
     */
    private function _getCorePaths()
    {
        return array();
    }

    /**
     * Preflights a given path to insure that it exists
     * and does not override a core path.
     *
     * @param string $key
     * @param string $path A directory path.
     * @return bool
     * @internal
     */
    private function _checkPathValid( $key, $path )
    {
        if ( array_key_exists( $key, self::$core_paths )
            && $path !== self::$core_paths[$key] )
        {
            //Core paths cannot be altered.
            return false;
        }
        if ( !is_readable( $path ) && is_dir( $path ) )
        {
            //Unreadable directory paths cannot be added.
            return false;
        }
        return true;
    }

}
