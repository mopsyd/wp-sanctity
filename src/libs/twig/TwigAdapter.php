<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\twig;

/**
 * Twig Adapter
 *
 * Provides an integrated object used to operate directly with Twig.
 *
 * This adapter is used to manage all Twig functionality for the theme,
 * and provides an integrated bridge between the theme logic and twig internals.
 * This abstraction allows for alternate adapters to be implemented
 * in the future to support additional template engines.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class TwigAdapter
    extends \mopsyd\sanctity\adapters\render\AbstractRenderAdapter
{

    /**
     * Baseline template directory relative to the theme root.
     */
    const TEMPLATE_DIRECTORY = 'templates';

    /**
     * The bottom level fallback subdirectory for templates.
     *
     * If no other template subdirectories are provided, this will be used.
     */
    const DEFAULT_SCOPE = 'global';

    /**
     * An array of core Twig extensions required for the theme to work.
     *
     * Additional extensions can be added, but these will always be available.
     *
     * @var array
     */
    private static $core_extensions = array(
        'Twig_Extension_StringLoader',
        'mopsyd\\sanctity\libs\\twig\\extension\\WordpressExtension',
        'mopsyd\\sanctity\libs\\twig\\extension\\SanctityExtension'
    );

    /**
     * Currently registered Twig extensions, which include the core extensions,
     * and any additional extensions loaded or provided through dependency injection.
     *
     * @var array
     */
    private static $extensions;

    /**
     * Currently registered contextual keys, which are available
     * in the global scope within the templates.
     * @var array
     */
    private static $context;

    /**
     * Currently registered filters, which transform or
     * decorate data fed to them within the templates.
     * @var array
     */
    private static $filters;

    /**
     * Currently registered functions or callables,
     * which execute operations directly when called
     * within the templates.
     * @var array
     */
    private static $functions;

    /**
     * Currently registered tests, which return a boolean determination
     * when called within the templates.
     * @var array
     */
    private static $tests;

    /**
     * The object that loads the Twig template paths.
     *
     * @var \mopsyd\sanctity\libs\twig\filesystem\TwigFilesystemLoader
     */
    private $filesystem_loader;

    /**
     * The Twig environment object.
     *
     * @var \mopsyd\sanctity\libs\twig\environment\TwigEnvironment
     */
    private $environment;

    /**
     * The Twig extension loader class used to load extensions.
     *
     * @var \mopsyd\sanctity\libs\twig\extension\TwigExtensionLoader
     */
    private $extension_loader;

    /**
     * A list of base directories to use as search paths for added relative paths.
     *
     * Child theme paths are automatically registered if they use the
     * standard template location.
     *
     * Plugins and child themes that use custom paths have to
     * explicitly declare a root path prior to being able to
     * register a new template location.
     *
     * @var array
     */
    private static $base_directories;

    /**
     * Registered paths for loading templates.
     *
     * @var type
     */
    private static $paths = array();

    /**
     * If not fully initialized, this will be false,
     * and trigger initialization in the constructor.
     *
     * @var bool
     */
    private static $is_initialized = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct();
        $this->_initializeAdapter();
        $this->_loadFilesystem();
        $this->_loadEnvironment();
        $this->_loadDefaultExtensions();
    }

    /**
     *
     * @param type $template
     * @param array $context (optional) An array of data to use in the template
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function render( $template, array $context = null )
    {
        if (is_null($context))
        {
            $context = array();
        }
        $template = $this->environment->load( $template );
        print $template->render( $context );
    }

    /**
     *
     * @param string $key
     * @param string|array|\Traversable
     * @return void
     */
    public function addContext( $key, $value )
    {

    }

    /**
     *
     * @param string $key
     */
    public function getContext( $key )
    {

    }

    /**
     *
     * @param string $key
     * @return bool
     */
    public function hasContext( $key )
    {

    }

    /**
     *
     * @return array
     */
    public function listContext()
    {

    }

    /**
     * @param string $key
     * @param string|array|\Traversable
     * @return void
     */
    public function editContext( $key, $values )
    {

    }

    /**
     *
     * @param string $key
     * @param \mopsyd\sanctity\libs\twig\extension\AbstractTwigExtension $value
     * @return void
     */
    public function addExtension( $key, $value )
    {

    }

    /**
     *
     * @param string $key
     * @return \mopsyd\sanctity\libs\twig\extension\AbstractTwigExtension
     */
    public function getExtension( $key )
    {

    }

    /**
     *
     * @param string $key
     * @return bool
     */
    public function hasExtension( $key )
    {

    }

    /**
     *
     * @return array
     */
    public function listExtensions()
    {

    }

    /**
     * @param string $key
     * @param callable
     * @return void
     */
    public function addFilter( $key, $value )
    {

    }

    /**
     *
     * @param string $key
     * @return callable
     */
    public function getFilter( $key )
    {

    }

    /**
     *
     * @param string $key
     * @return bool
     */
    public function hasFilter( $key )
    {

    }

    /**
     *
     * @return array
     */
    public function listFilters()
    {

    }

    /**
     * @param string $key
     * @param callable
     * @return void
     */
    public function addFunction( $key, $value )
    {

    }

    /**
     *
     * @param string $key
     * @return callable
     */
    public function getFunction( $key )
    {

    }

    /**
     *
     * @param string $key
     * @return bool
     */
    public function hasFunction( $key )
    {

    }

    /**
     *
     * @return array
     */
    public function listFunctions()
    {

    }

    /**
     * @param string $key
     * @param callable
     * @return void
     */
    public function addTest( $key, $value )
    {

    }

    /**
     *
     * @param string $key
     * @return callable
     */
    public function getTest( $key )
    {

    }

    /**
     *
     * @param string $key
     * @return bool
     */
    public function hasTest( $key )
    {

    }

    /**
     *
     * @return array
     */
    public function listTests()
    {

    }

    /**
     * Returns an array of all registered absolute paths.
     *
     * @return array
     */
    public function getPaths()
    {
        $subject_paths = self::$paths;
        $paths = array();
        foreach ( $subject_paths as
            $key =>
            $pathset )
        {
            foreach ( $pathset as
                $path )
            {
                $paths[] = ( $path );
            }
        }
        return $paths;
    }

    /**
     * Adds a relative path. All valid locations for currently registered
     * base paths that resolve to the relative path will be registered.
     *
     * If the path exists in multiple registered locations, then all of the locations
     * will be registered, with the later registered paths taking priority over
     * the earlier registered ones.
     *
     * Supplied paths SHOULD NOT be absolute paths. If an absolute path is required,
     * the base path needs to be registered prior to registering the relative path.
     * This insures that child themes and extensions can properly override appropriate
     * templates using a standardized file structure.
     *
     * @param type $key
     * @param type $path
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function addPath( $key, $path )
    {
        if ( !$this->hasPath( $key ) )
        {
            $valid = array();
            $path_base_directories = $this->_getBaseDirectories();
            foreach ( $path_base_directories as
                $base )
            {
                $full = $base . $path;
                if ( !$this->_checkPath( $full ) )
                {
                    continue;
                }
                $valid[] = rtrim( realpath( $full ), '\\/' ) . DIRECTORY_SEPARATOR;
            }
            if ( empty( $valid ) )
            {
                d( self::$base_directories );
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Specified path [%1$s] identified by key [%2$s] '
                    . 'does not exist in any known path root registered '
                    . 'in [%3$s]. Searching in [%4$s]', $path, $key,
                    get_class( $this ),
                    implode( ', ', self::$base_directories->toArray() ) )
                );
            }
            self::$paths = array(
                $key => $valid ) + self::$paths;
            if ( !is_null( $this->filesystem_loader ) )
            {
                $this->filesystem_loader->setPaths( $this->getPaths() );
            }
            $this->debugStash( array(
                'name' => $key,
                'paths' => self::$paths[$key]
                ), 'path_added' );
        }
    }

    /**
     * Gets a path, or set of paths by identifying key.
     *
     * This method will always return an array, not a string literal.
     * It may only contain one entry. The array will contain all of the
     * locations that the supplied key found valid source directories
     * when it was registered, listed in load order of preference
     * for the given key.
     *
     * @param type $key
     * @return array
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     For safety, this method raises an exception if an invalid path key is supplied.
     *     This prevents errors in the Twig render engine from occurring.
     * @note You should always call `hasPath` first, which does not produce an exception.
     *     If that returns true, this method will not raise an exception.
     */
    public function getPath( $key )
    {
        if ( !$this->hasPath( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Path [%1$s] is not registered in [%2$s]', $key,
                get_class( $this ) )
            );
        }
        return self::$paths[$key];
    }

    /**
     * Returns true if the given key has been registered to a
     * valid path or pathset, and false otherwise.
     *
     * @param type $key
     * @return bool
     */
    public function hasPath( $key )
    {
        return array_key_exists( $key, self::$paths );
    }

    /**
     * Deletes a path or pathset by key, if it was registered.
     * If it was not registered, this does nothing.
     *
     * @param string $key
     */
    public function deletePath( $key )
    {
        if ( $this->hasPath( $key ) )
        {
            $this->debugStash( array(
                'name' => $key,
                'path' => self::$paths[$key]
                ), 'path_removed' );
            unset( self::$paths[$key] );
        }
    }

    private function _initializeAdapter()
    {
        if ( is_null( self::getScope() ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Scope must be declared prior to instantiation in [%1$s]',
                get_class( $this ) )
            );
        }
        if ( !self::$is_initialized )
        {
            self::$base_directories = $this->_getBaseDirectories();
            self::$paths = array(
                'default' => self::$base_directories );
            filesystem\AbstractTwigFileSystemLoader::setThemeAdapter( $this );
            environment\AbstractTwigEnvironment::setThemeAdapter( $this );
            extension\AbstractTwigExtension::setThemeAdapter( $this );
            loader\RuntimeLoader::setThemeAdapter( $this );
            self::$extensions = new \mopsyd\sanctity\libs\twig\TwigExtensionContainer();
            foreach ( self::$core_extensions as
                $extension )
            {
                self::$extensions[$extension] = $extension;
            }
            $this->debugStash( 'Core initialization resolved successfully',
                'twig_adapter' );
            self::$is_initialized = true;
        }
    }

    private function _loadFilesystem()
    {
        $this->filesystem_loader = new filesystem\TwigFileSystemLoader( array() );
        $this->filesystem_loader->setPaths( $this->getPaths() );
    }

    private function _loadEnvironment()
    {
        $this->environment = new environment\TwigEnvironment( $this->filesystem_loader,
            $this->_defineEnvironmentArguments() );
        $loader = new loader\RuntimeLoader();
        $this->environment->addRuntimeLoader( $loader );
        foreach ( self::$core_extensions as
            $extension )
        {
            $this->environment->addExtension( new $extension() );
        }
//        $extensions = array();
//        foreach(self::$extensions as $class => $object)
//        {
//            $extensions[$class] = $object;
//        }
//        d($this->environment, $extensions, self::$extensions->toArray());
//        exit();
    }

    private function _defineEnvironmentArguments()
    {
        $arguments = array(
            'debug' => defined( 'WP_DEBUG' ) && WP_DEBUG,
            'charset' => 'UTF-8',
            'base_template_class' => 'Twig_Template',
            'cache' => $this->_determineCachingPath(),
            'strict_variables' => false,
            'autoescape' => false,
            'optimizations' => -1
        );
        return $arguments;
    }

    private function _loadDefaultExtensions()
    {

    }

    private function _getBaseDirectories()
    {
        if ( !is_null( self::$base_directories ) )
        {
            return self::$base_directories;
        }
        $directories = array();
        $child_directory = trailingslashit( trailingslashit( get_stylesheet_directory() )
            . $this::TEMPLATE_DIRECTORY );
        $parent_directory = trailingslashit( trailingslashit( get_template_directory() )
            . $this::TEMPLATE_DIRECTORY );
        $directories['parent_theme'] = $parent_directory;
        if ( !$child_directory === $parent_directory )
        {
            $directories = array(
                'child_theme' => $child_directory ) + $directories;
        }
        return $this->_scopeBaseDirectories( $directories );
    }

    private function _scopeBaseDirectories( $directories )
    {
        $default = $this::DEFAULT_SCOPE;
        $declared = self::getScope();
        $scoped_directories = array();
        foreach ( $directories as
            $prefix =>
            $directory )
        {
            if ( $this->_checkPath( $directory . $default ) )
            {
                $scoped_directories = array(
                    $prefix . '_' . $default => trailingslashit( realpath( $directory . $default ) ) ) +
                    $scoped_directories;
            }
            if ( $this->_checkPath( $directory . $declared ) )
            {
                $scoped_directories = array(
                    $prefix . '_' . $declared => trailingslashit( realpath( $directory . $declared ) ) ) +
                    $scoped_directories;
            }
        }
        return $this::containerize( $scoped_directories );
    }

    private function _getScopedBasePaths()
    {
        $paths = array();
        foreach ( self::$base_directories as
            $key =>
            $path )
        {
            $base = $path . self::getScope();
            if ( $this->_checkPath( $base ) )
            {
                $paths[] = trailingslashit( realpath( $base ) );
            }
            $base = $path . $this::DEFAULT_SCOPE;
            if ( $this->_checkPath( $base ) )
            {
                $paths[] = trailingslashit( realpath( $base ) );
            }
        }
        return $paths;
    }

    private function _checkPath( $path )
    {
        return ( is_string( $path ) && is_readable( $path ) && is_dir( $path ) );
    }

    private function _determineCachingPath()
    {
        if ( is_writable( dirname( get_theme_root() ) ) )
        {
            $dir = trailingslashit( dirname( get_theme_root() ) )
                . $this->getThemePrefix() . '_' . 'template_cache'
                . DIRECTORY_SEPARATOR;
            if ( !is_dir( $dir ) )
            {
                //0770 insures that FTP and command line users have access
                //to the directory in addition to the webserver,
                //but nobody else does.
                mkdir( $dir, 0770 );
                //In case the install has a wonky umask setting.
                chmod( $dir, 0770 );
                //Insure that the cache directory is not web-view accessible,
                //even if the file permissions are changed.
                $fh = fopen( $dir . '.htaccess', 'w+' );
                fwrite( $fh, 'deny from all' . DIRECTORY_SEPARATOR );
                fclose( $fh );
                chmod( $dir . '.htaccess', 0664 );
            }
            return $dir;
        }
        //If the content directory below uploads is not writable,
        //then no caching until a better implementation of the
        //cache interface is created.
        //Placing the template cache in the uploads directory
        //is not a secure option.
        return false;
    }

}
