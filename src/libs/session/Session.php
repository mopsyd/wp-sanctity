<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\session;

/**
 * Session
 * Handles session storage and retrieval for Sanctity
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class Session
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\session\SessionInterface
{
    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    private static $session;

    const SESSION_KEY = 'sanctity';

    public function __construct()
    {
        $this->_initializeSession();
        parent::__construct();
    }

    public function get( $key )
    {
        $this->_initializeSessionPrefix();
        if ( array_key_exists( $key, self::$session[$this::SESSION_KEY] ) )
        {
            return self::$session[$this::SESSION_KEY][$key];
        }
        return false;
    }

    public function set( $key, $value )
    {
        $this->_initializeSessionPrefix();
        self::$session[$this::SESSION_KEY][$key] = $value;
    }

    public function drop( $key )
    {
        $this->_initializeSessionPrefix();
        if ( array_key_exists( $key, self::$session[$this::SESSION_KEY] ) )
        {
            unset( self::$session[$this::SESSION_KEY][$key] );
        }
    }

    private function _initializeSession()
    {
        if ( is_null( self::$session ) )
        {
            $this->_initializeSessionPrefix();
            self::$session = &$_SESSION;
        }
    }

    private function _initializeSessionPrefix()
    {
        if ( session_status() == PHP_SESSION_NONE )
        {
            session_start();
        }
        if ( !array_key_exists( $this::SESSION_KEY, $_SESSION ) )
        {
            $_SESSION[$this::SESSION_KEY] = array();
        }
    }

}
