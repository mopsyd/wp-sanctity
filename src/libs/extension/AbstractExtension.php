<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\extension;

/**
 * Abstract Extension
 * Provides an abstract basis for extension on the Sanctity MVC system.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractExtension
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\extension\ExtensionInterface
{

    /**
     * This is the human-readable name of your extension,
     * which will display to the end user in the
     * integrations pane of their theme.
     *
     * Maximum acceptable length is 128 characters.
     */
    const EXTENSION_NAME = 'Untitled Extension';

    /**
     * This is a short description of what your extension is for.
     *
     * Maximum acceptable length is 256 characters.
     *
     * Keep it to one brief sentence (it should display cleanly as a field label
     * in the integrations panel, and will be truncated if it is too long).
     */
    const DESCRIPTION = 'No description provided.';

    /**
     * The slug identifier of your package.
     *
     * Maximum acceptable length is 64 characters.
     *
     * This should be one of the following:
     *
     * - Your `vendor\namespace` declaration for your Psr-4 root class base,
     * - Your identifying package name in your `composer.json`,
     * - Your id in the official Wordpress repository,
     * - Your the name of the folder your package resides in if none of the prior are applicable.
     */
    const EXTENSION_PACKAGE = false;

    /**
     * The extension version. This should typically correspond
     * to your theme or plugin version, although it may differ
     * if you version your extension support differently.
     *
     * Maximum acceptable length is 32 characters,
     * and it should be a standardized version number
     * (major-version dot minor-version dot patch-version).
     *
     * Standardized suffixes are acceptable (eg: 1.0.3a, 1.0.3-alpha, etc).
     *
     * Any version suffixed with "a", "-alpha", "~alpha", "dev", "-dev", or "~dev"
     * will be disabled in production by default unless debug mode is active.
     * Any build version after any of these suffixes will not alter this behavior.
     *
     * Any version suffixed with "b", "-beta", or "~beta"
     * will display a beta notification in production,
     * but will not be disabled by default.
     * Any build version after any of these suffixes will not alter this behavior.
     *
     * Any version suffixed with "r", "-release", "~release", "v", "-version",
     * "~version", "s", "-stable", or "~stable"
     * will be considered fully production ready, and will not produce a warning.
     * Any build version after any of these suffixes will not alter this behavior.
     *
     * @note Unstable extensions tagged as stable or release ready may be blacklisted
     *     if they are in fact, not stable. Extensions without a suffix will be treated
     *     as having a category of "stable" by default. For this reason,
     *     it is strongly suggested that you use a suffix for dev, alpha, and beta builds.
     */
    const EXTENSION_VERSION = '1.0.0';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

/**
     * Extend all of the things.
     */
    use \mopsyd\sanctity\traits\ExtensionLoader;

    /**
     * Represents the Sanctity primary instance.
     *
     * @var \Sanctity
     */
    private $site;
    private $controllers;
    private $components;
    private $models;
    private $views;
    private $libs;
    private $factories;
    private $actions;
    private $filters;
    private $templates;
    private $layouts;
    private $configs;

    /**
     * An extension must instantiate cleanly without any required parameters.
     */
    public function __construct( $dependencies = null, $arguments = array() )
    {
        $this->_addNamespacePrefix();
        $this->_initializeExtension();
        parent::__construct();
    }

    /**
     * Override this method to declare your namespace prefix.
     *
     * It should correspond to the Psr-4 namespace prefix of your package.
     * If your directory structure matches that of Sanctity, your
     * extension classes will load without issue.
     *
     * This method is static so that Sanctity can distribute your
     * namespace prefix throughout its internals without the overhead
     * of loading your class, which allows its own internal logic to
     * integrate your extension seamlessly.
     *
     * @return string
     */
    abstract public static function getPackageNamespace();

    /**
     * This method must be overridden, and must correspond to the
     * source directory that your Psr-4 namespace resolves to.
     *
     * This will be used in conjunction with `getPackageNamespace`
     * to register your package with the autoloader. If this is not valid,
     * your extension will be disabled.
     *
     * You are not restricted to placing your extension in your root
     * source directory, but that is the easiest way to do it.
     *
     * All paths your extension declares will be considered to be relative
     * to the absolute disk location returned by this method, and many path
     * resolvers actively prevent reverse filepath navigation
     * for security reasons. Although it is possible to place your extension
     * class elsewhere, the most straightforward implementation is to just
     * put it in your plugin or theme root directory, or alternately to put
     * all of your assets managed by Sanctity internals in a child directory
     * of its location.
     *
     * @return string The absolute filepath root of your extension,
     *     and it's assets that are otherwise declared for Sanctity integration.
     *     All such assets should be in a child directory relative to the path
     *     returned by this method. You may symlink if need be to reach
     *     backward directories, but you may not present a path string that
     *     contains reverse navigation. Symlinks are acceptable, as the
     *     server administrator should have strict control over their
     *     creation on a well maintained server.
     */
    abstract public static function getPath();

    /**
     * Registers your extension with Sanctity internals.
     * This is called automatically
     * when you submit it to `\Sanctity::extend`
     *
     * This method is used to package the active Sanctity front controller instance
     * locally into your extension after registration has successfully resolved,
     * so it may be used during your initialization logic via `getSite()`
     * to perform any required operations on the Sanctity runtime.
     *
     * In contrast to the general public facing api, this gives your extension
     * direct access to the current active instance, which is otherwise protected
     * from direct manipulation by the public api. This will afford you access
     * to it's active state methods, which are not otherwise available for
     * active usage after initialization, if the original instantiated instance
     * was not retained externally (by default, it isn't).
     *
     * The Sanctity active instance is not a Singleton, and you *can* instantiate
     * another copy, however this is going to incur quite a lot of memory overhead
     * as it duplicates the bootload process in full, and this should NOT be done
     * outside of a unit testing environment for performance reasons.
     *
     * It should also be noted that quite a lot of internal logic is cached statically
     * after first load, and this is not overridden when duplicate front controller
     * instances are generated. It can be cleared, but that is done internally at
     * specific logical points in operation where previously accrued data needs
     * to be invalidated because the underlying subset has been updated,
     * and should not be done manually unless you are extremely familiar
     * with the api of Sanctity internals.
     *
     * Registration is only called one time per runtime for any given extension.
     * Attempting to re-register an existing extension will not have any effect.
     *
     * @return void
     * @final
     */
    final public function register( \mopsyd\sanctity\interfaces\SanctityInterface $site )
    {
        $this->site = $site;
    }

    /**
     * Override this method to provide any initialization logic required
     * by your extension. This method will fire in the `init` hook,
     * but only if registration of your extension is successful.
     *
     * This method will be called by the Extension Model after the extension
     * is correctly validated and registered, and after the extension has
     * been packaged via dependency injection with prerequisite Sanctity assets.
     *
     * Do not call this method manually, as required assets will not be present
     * until registration occurs normally.
     */
    public function initialize()
    {
        //no-op
    }

    /**
     * Used by the Component Model to retrieve any declared components.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getComponents()
    {
        return $this->components;
    }

    /**
     * Used by the Component Model to retrieve any declared controllers.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getControllers()
    {
        return $this->controllers;
    }

    /**
     * Used by the Component Model to retrieve any declared models.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getModels()
    {
        return $this->models;
    }

    /**
     * Used by the Component Model to retrieve any declared views.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getViews()
    {
        return $this->views;
    }

    /**
     * Used by the Component Model to retrieve any declared libraries.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getLibs()
    {
        return $this->libs;
    }

    /**
     * Used by the Component Model to retrieve any declared factories.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getFactories()
    {
        return $this->factories;
    }

    /**
     * Used by the Component Model to retrieve any declared templates.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getTemplates()
    {
        return $this->templates;
    }

    /**
     * Used by the Component Model to retrieve any declared actions.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getActions()
    {
        return $this->actions;
    }

    /**
     * Used by the Component Model to retrieve any declared filters.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Used by the Component Model to retrieve any declared layouts.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getLayouts()
    {
        return $this->layouts;
    }

    /**
     * Used by the Component Model to retrieve any declared configurations.
     *
     * @return bool|\mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @final
     */
    final public function getConfigs()
    {
        return $this->configs;
    }

    /**
     * Override this method to declare any components your extension provides.
     *
     * This should return an associative array, where the key is the slug of the component,
     * and the value is the name of a class that implements the component interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\components\ComponentInterface
     *
     * @return array
     */
    protected function declareComponents()
    {
        return array();
    }

    /**
     * Override this method to declare any controllers your extension provides.
     *
     * This should return an associative array, where the key is the slug of the controller,
     * and the value is the name of a class that implements the controller interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\controllers\ControllerInterface
     *
     * @return array
     */
    protected function declareControllers()
    {
        return array();
    }

    /**
     * Override this method to declare any models your extension provides.
     *
     * This should return an associative array, where the key is the slug of the model,
     * and the value is the name of a class that implements the model interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\models\ModelInterface
     *
     * @return array
     */
    protected function declareModels()
    {
        return array();
    }

    /**
     * Override this method to declare any views your extension provides.
     *
     * This should return an associative array, where the key is the slug of the view,
     * and the value is the name of a class that implements the view interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\views\ViewInterface
     *
     * @return array
     */
    protected function declareViews()
    {
        return array();
    }

    /**
     * Override this method to declare any libraries your extension provides.
     *
     * This should return an associative array, where the key is the slug of the library,
     * and the value is the name of a class that implements the Sanctity base interface.
     *
     * @see \mopsyd\sanctity\interfaces\SanctityInterface
     *
     * @return array
     */
    protected function declareLibs()
    {
        return array();
    }

    /**
     * Override this method to declare any factories your extension provides.
     *
     * This should return an associative array, where the key is the slug of the factory,
     * and the value is the name of a class that implements the factory interface.
     *
     * @see \mopsyd\sanctity\interfaces\libs\factory\FactoryInterface
     *
     * @return array
     */
    protected function declareFactories()
    {
        return array();
    }

    /**
     * Override this method to declare any template paths your extension provides.
     *
     * This should return an array with values equal to any template paths
     * you want to register, relative to the declared extension root.
     * Template paths may not contain backwards filepath navigation.
     *
     * An associative key is not required for template registration.
     * Your extension name will be used for the key of the template group
     * resulting from generation of your template set.
     *
     * @return array
     */
    protected function declareTemplates()
    {
        return array();
    }

    /**
     * Override this method to declare any actions your extension needs to register.
     *
     * This should return an associative array, where the key is the hook you
     * want to run the action on, and the value is an array of all callbacks
     * that need to fire on that hook, in the order of preference.
     *
     * You may register multiple methods against a single hook in this manner,
     * and they will all fire in order on one single binding. They will fire
     * immediately after the core Sanctity actions have resolved for the
     * same action, if any are present.
     *
     * @return array
     */
    protected function declareActions()
    {
        return array();
    }

    /**
     * Override this method to declare any filters your extension needs to register.
     *
     * This should return an associative array, where the key is the filter you
     * want to provide callbacks for, and the value is an array of all callbacks
     * that need to fire on that filter, in order of preference.
     *
     * You may register multiple methods against a single filter in this manner,
     * and they will all fire in order in one single binding. They will fire
     * immediately after the core Sanctity filters have resolved for the same
     * filter, if any are present.
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareFilters()
    {
        return array();
    }

    /**
     * Override this method to declare any layouts your extension provides.
     *
     * Layouts must contain a manifest.json file in their root directory,
     * which provides a map of its assets and configurations. You may declare
     * any previously existing layout as a parent layout and inherit all
     * settings from that layout that you do not explicitly define.
     * Layouts that do not declare a parent use the default layout as
     * their parent (it is not possible to create a layout other than
     * the default that does not have a parent).
     *
     * This insures that no miscellaneous settings are not correctly resolved.
     * You may override all of the settings of the default if you care to,
     * but any that are missed will use the default (or whichever parent you
     * declare, which in turn may have a parent, and eventually has the
     * default as a parent).
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareLayouts()
    {
        return array();
    }

    /**
     * Override this method to declare any configurations your extension provides.
     *
     * This should return an associative array, where the key is the config id,
     * and the value either an array of config data, a relative filepath to a
     * json file that contains the config data, or a callback that will generate
     * the configuration data dynamically. This flexibility allows for
     * configurations to be generated a number of ways, including from a
     * flat file, from application logic, or by being pulled from a cache
     * or data layer.
     *
     * Configurations that have a previously existing key will be recursively
     * replaced into the existing config. Configs that do not have a previously
     * existing key will be containerized and retained independently in the
     * configuration manager. This process does not alter the origin data of
     * existing configs, so registration needs to occur on each runtime,
     * or the default will be used in place of the extension config.
     *
     * @see \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
     *
     * @return array
     */
    protected function declareConfigs()
    {
        return array();
    }

    /**
     * Returns the current site wrapper instance for the theme.
     *
     * This will return the currently active instance of the Sanctity front controller.
     * This is only made available after registration has occurred correctly.
     *
     * This should be used by extensions to insure that overrides by child themes
     * are obtained correctly, instead of having to do a bunch of wonky logic
     * to hunt for the right one. Child themes may or may not extend the
     * primary site object, and will still work fine if they don't,
     * so use this and don't do guesswork.
     *
     * As long as the extension is properly registered,
     * this will return the correct object.
     *
     * @return \Sanctity
     */
    protected function getSite()
    {
        return $this->site;
    }

    private function _addNamespacePrefix()
    {
        $this->registerNamespacePrefix( $this::getPackageNamespace() );
    }

    /**
     * Packages the extension declarations,
     * and prepares them for the model to extract.
     */
    private function _initializeExtension()
    {
        $declarations = array(
            'actions' => $this->declareActions(),
            'components' => $this->declareComponents(),
            'configs' => $this->declareConfigs(),
            'controllers' => $this->declareControllers(),
            'factories' => $this->declareFactories(),
            'filters' => $this->declareFilters(),
            'layouts' => $this->declareLayouts(),
            'libs' => $this->declareLibs(),
            'models' => $this->declareModels(),
            'templates' => $this->declareTemplates(),
            'views' => $this->declareViews(),
        );
        foreach ( $declarations as
            $key =>
            $declaration )
        {
            $this->$key = false;
            if ( !empty( $declaration ) )
            {
                $this->$key = $this::containerize( $declaration );
            }
        }
    }

}
