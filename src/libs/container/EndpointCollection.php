<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\container;

/**
 * Endpoint Collection
 *
 * The endpoint collection represents an index of endpoints within a single api
 * or work queue. It should contain a set of endpoint containers that reflect
 * all of the known api calls that can be made against a specific api, or a
 * set of api calls from multiple apis that can be iterated over to perform
 * complex requests across unrelated apis for an expressedly related internal
 * purpose that needs to leverage one or more external apis.
 *
 * These collections can also be used to house endpoints for the same host
 * origin that should be exposed to the frontend, imparting how to phone home
 * to frontend scripts without the need for them to hardcode any endpoint data
 * in their own logic. This makes both the frontend and backend codebases
 * specifically more portable and less opinionated.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @final
 */
final class EndpointCollection
extends AbstractCollection
implements \mopsyd\sanctity\interfaces\libs\container\EndpointCollectionInterface
{
    const EXPECTED_CONTAINER_TYPE = 'mopsyd\\sanctity\\interfaces\\libs\\container\\EndpointContainerInterface';
}
