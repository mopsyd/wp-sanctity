<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\container;

/**
 * Abstract Collection
 *
 * The abstract collection sets the basis for
 * containers that contain other containers.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractCollection
    extends AbstractContainer
    implements \mopsyd\sanctity\interfaces\libs\container\CollectionInterface
{

    const EXPECTED_CONTAINER_TYPE = 'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface';

    public function getExpectedContainerType()
    {
        return $this::EXPECTED_CONTAINER_TYPE;
    }

    /**
     * <Abstract Collection Object Instantiation Trigger Method>
     * Blocks instantiation if the declared container type
     * does not correctly extend the ContainerInterface.
     *
     * @param array $args The arguments to be passed into the constructor.
     * @param string $method The internal method that the operation arose from.
     *     This function will not fire if it does not receive this argument
     *     as itself or the constructor.
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the declared container type is not a valid extension of the ContainerInterface.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the adapter has not properly initialized the class prior to use.
     *     Many of the internals require the adapter to have been properly passed
     *     through static dependency injection prior to usage.
     *     If this has not occurred, internals will break, generate errors,
     *     or cause erratic behavior (via parent class).
     */
    protected function _onContainerInstantiation( $args, $method = null )
    {
        parent::_onContainerInstantiation( $args, $method );
        $interface = trim( $this->getExpectedContainerType(), '\\' );
        $expected = 'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface';
        if ( !( in_array( $expected, class_implements( $interface ) ) || $interface
            === $expected ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured. Declared container interface '
                . '[%2$s] is not a valid extension of [%3$s]. Please report '
                . 'this issue to the developer of [%1$s] so it can be corrected.',
                get_class( $this ), $interface, $expected )
            );
        }
    }

    /**
     * <Abstract Collection Setter Trigger Method>
     * Prevents values from being set that do not implement
     * the declared container interface correctly.
     *
     * @param string $key The passed key that the setter is supposed to have
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $value
     * @param string $method The internal method that the operation arose from
     * @return void
     *
     * @throws \mopsyd\sanctity\libs\exception\ContainerException
     *     A container exception is raised if provided value
     *     is not a valid container that the collection is built to handle.
     */
    protected function _onSet( $key, $value, $method = null )
    {
        parent::_onSet( $key, $value, $method );
        $expected = $this->getExpectedContainerType();
        if ( !( is_object( $value ) && ( $value instanceof $expected ) ) )
        {
            d($value, $expected, debug_backtrace());
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Invalid dependency supplied in [%1$s]. '
                . 'Provided value identified by key '
                . '[%2$s] must be an instance of [%3$s].', get_class( $this ),
                $key, $expected )
            );
        }
    }

}
