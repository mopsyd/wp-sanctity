<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\container;

/**
 * Abstract Container
 *
 * Is it an object? Is it an array? Is it json? Is it a dependency injection container?
 *
 * It's all of the things, and yet nothing. Lol. (see the trait for the actual logic).
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractContainer
    implements \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
{
    /**
     * Containerize all of the things.
     */
    use \mopsyd\sanctity\traits\ContainerUtility;

    /**
     * Debug all of the things. Safely for production though.
     */
    use \mopsyd\sanctity\traits\Debugger;

    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

    /**
     * Makes containers compatible with standard factorization.
     *
     * @param type $size
     * @param type $contents
     * @return type
     */
    public static function init( $size = null, array $contents = array() )
    {
        if ( is_null( $size ) )
        {
            $size = -1;
        }
        $class = get_called_class();
        $instance = new $class( $size );
        return $instance->fromArray( $contents );
    }

    /**
     * Used to cast a container to a different type.
     *
     * This method creates an instance of itself from the contents
     * of a provided container, so that container contents can be
     * easily transferred between container types without
     * additional effort by external logic.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container
     * @return type
     */
    public static function from( \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container )
    {
        $class = get_called_class();
        $size = $container->getSize();
        return $class::init( $size, $container->toArray() );
    }

    /**
     * Returns all container debug information if debug mode is enabled,
     * or false if it is not.
     *
     * @return array|false
     */
    public static function getDebug()
    {
        return self::debugGetAll();
    }

}
