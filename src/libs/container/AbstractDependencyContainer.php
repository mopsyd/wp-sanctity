<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\container;

/**
 * Abstract Dependency Container
 *
 * The abstract dependency container sets the basis for
 * dependency injection containers within the Sanctity system.
 *
 * This container enforces full compliance with Psr-11.
 * All dependency containers used within this system
 * should extend from this class.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractDependencyContainer
    extends AbstractContainer
    implements \mopsyd\sanctity\interfaces\libs\container\DependencyContainerInterface
{

    /**
     * <Abstract Dependency Container Setter Trigger Method>
     * Prevents values from being set that do not fully conform to the Psr-11 spec.
     *
     * @param string $key The passed key that the setter is supposed to have
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $value
     * @param string $method The internal method that the operation arose from
     * @return void
     *
     * @throws \mopsyd\sanctity\libs\exception\ContainerException
     *     A container exception is raised if provided value
     *     is not an object, or if the key is not a string.
     */
    protected function _onSet( $key, $value, $method = null )
    {
        parent::_onSet( $key, $value, $method );
        if ( !( is_string( $key ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Invalid key supplied in [%1$s]. '
                . 'Provided key must be a string, received [%2$s].',
                get_class( $this ), gettype( $key ) )
            );
        }
        if ( !( is_object( $value ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\ContainerException(
            sprintf( 'Invalid dependency supplied in [%1$s]. '
                . 'Provided value identified by key '
                . '[%2$s] must be an object.', get_class( $this ), $key )
            );
        }
    }

}
