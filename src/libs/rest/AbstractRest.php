<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\rest;

/**
 * AbstractRest
 * Provides baseline abstraction for rest server and client functionality.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.2.0
 */
abstract class AbstractRest
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\rest\RestInterface
{

    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\EndpointContainer';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * String Theory
     */
    use \mopsyd\sanctity\traits\StringUtility;

    private static $endpoints;
    private static $request_methods_valid = array(
        'get',
        'post',
        'put',
        'delete',
        'options',
        'trace',
        'head',
        'connect',
        'patch'
    );
    private static $rest_initialized = false;

    /**
     * Abstract Rest Constructor
     *
     * Pretty much just pulls the endpoint config,
     * stores a static reference to it, and that's about it.
     */
    public function __construct()
    {
        $this->_initializeRest();
        parent::__construct();
    }

    /**
     * Returns the null-state defaults for an api endpoint, which constitutes
     * all of the keys expected to be present for containerization. By default,
     * this will provide an unsecured http request to the same http host that
     * this script is running on, to their default front page. That is not
     * incredibly useful in most cases, so you need to fill in the blanks to
     * make this do anything spectacular. This is meant to insure that endpoint
     * data is always containerized with expected keys specifically, not to
     * automatically build your api for you.
     *
     * @note This method defers to the getters for all keys, and does not
     *     directly set its own data into the array. If you modify one of those
     *     and do not return a null state when null parameters are presented,
     *     then you will modify your default across all apis that pass through it.
     *     This may or may not be preferable, depending on your use case. This is
     *     intended to have one class represent each api, in which case it works well.
     *     If you have a class handling multiple apis, this will incur some technical
     *     overhead to dance around this approach, so it is suggested to proved
     *     classes to apis in a 1:1 format to avoid excessive internal complexity.
     * @return array
     */
    protected function getEndpointDefaults()
    {
        return array(
            'protocol' => $this->getRequestProtocol(),
            'host' => $this->getRequestHost(),
            'method' => $this->getRequestMethod(),
            'auth' => $this->getRequestAuthorizationRequired(),
            'cross-origin' => $this->getRequestCrossOrigin(),
            'uri' => $this->getRequestUri(),
            'port' => $this->getRequestPort(),
            'user' => $this->getRequestUser(),
            'password' => $this->getRequestPassword(),
            'format' => $this->getRequestFormat(),
            'headers' => $this->getRequestHeaders(),
            'cookies' => $this->getRequestCookies(),
            'message' => $this->getRequestMessage(),
            'callback' => $this->getRequestCallback(),
            'key' => $this->getApplicationKey(),
            'secret' => $this->getApplicationSecret(),
            'application_id' => $this->getApplicationId()
        );
    }

    /**
     * Used to determine the request protocol.
     *
     * This will generally be http or https
     * By default it provides http if not otherwise provided.
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string http|https|ftp|etc You CAN use this for non-http requests,
     *     but that is not tested or offically condoned in how this logic is
     *     presented. The abstraction is cohesive enough to probably handle it,
     *     but it's entirely on you to handle the data exchange if you do so. This
     *     class is intended for http requests, not for email, smtp, ftp, etc,
     *     even though they pretty much use the same sort of messaging format.
     *     If you need that sort of functionality, you are much better off
     *     explicitly pulling down a library in Composer that is intended for
     *     that purpose than trying to roll your own protocol handler.
     */
    protected function getRequestProtocol( $request_key = null, $previous = null )
    {
        return 'http';
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string Returns the host name or array of hostnames that need to
     *     be authenticated for cross-origin CSRF protection. The default is
     *     the same host as the script is running on (which isn't going to work
     *     for any 3rd party remote api, so you need to provide this value in
     *     that case). If you return false, cross-origin will not be utilized.
     *     If you are handling a request with a lot of redirects to establish a
     *     trust chain, it is best to disable it rather than attempt to whitelist
     *     any number of servers that may or may not resolve in the same order or
     *     in the same number from request to request. This often happens when
     *     authenticating against SAML or similar mechanisms, who will defer to
     *     numerous external authorities for validation. Cross Origin will
     *     typically break those requests unless you maintain a very verbose
     *     list of the entire auth chain.
     */
    protected function getRequestCrossOrigin( $request_key = null,
        $previous = null )
    {
        return $this::HTTP_HOST;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return bool return true if an auth mechanism is required, and false if
     *     it is not. If false, all other authentication details will be ignored.
     *     This typically only applies to broad generalized apis that do not provide
     *     any sensitive data.
     */
    protected function getRequestAuthorizationRequired( $request_key = null,
        $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string The hostname that the api endpoint needs to contact.
     *     The default is the same server this script is running on, which will
     *     be false if you are accessing this from CLI, because the HTTP host
     *     is not declared in those cases by the server. If you have internal
     *     endpoints that you need this script to be aware of from any cli run,
     *     you will need to explicitly provide it a configuration that declares
     *     the host for it to work.
     */
    protected function getRequestHost( $request_key = null, $previous = null )
    {
        return $this::HTTP_HOST;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string get|post|put|delete|options|trace|head Capital or lowercase,
     *     will always be cast to lowercase when interpreted, and cast to uppercase
     *     when served.
     */
    protected function getRequestMethod( $request_key = null, $previous = null )
    {
        return 'get';
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string The request uri, which is appended to the host.
     *     This must include all of the uri string, including the get string
     *     if the request is not explicitly a "GET" request (get requests do
     *     not need to include this, but a POST/PUT/DELETE request that utilizes
     *     get parameters do need to provide it).
     *     The default is just a single forward slash, which corresponds to a
     *     well formed default endpoint for the given host.
     */
    protected function getRequestUri( $request_key = null, $previous = null )
    {
        return '/';
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return int Returns the outbound port.
     *     Typically 80 for unsecured http, and 443 for ssl.
     *     The default behavior is to always return port 80.
     */
    protected function getRequestPort( $request_key = null, $previous = null )
    {
        return 80;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string|null Returns the username portion of a uri string for an
     *     endpoint using an authority as part of its uri schema.
     *     This is often the case when Basic Auth is used in conjunction
     *     with other methods (such as a private git server, which would be
     *     something like git@servername or similar, and typically also
     *     requires a ssh key in addition to this). It is also often used
     *     in legacy apis that use basic plaintext auth in conjunction with
     *     a password, or for access credentials to endpoints not expected to
     *     be made public over the internet. This should return null if this
     *     is not applicable.
     */
    protected function getRequestUser( $request_key = null, $previous = null )
    {
        return null;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return null|string Returns the request password if the request follows
     *     the authority format instead of utilizing a more secure authentication
     *     schema. This is often the case for legacy apis as well as non-http protocols.
     *     However secure http protocols will typically use OAuth/OpenId/SAML/etc
     *     in place of this form of authentication, and it is strongly suggested
     *     not to leverage this format of authentication for your own endpoints
     *     unless they are strictly internal and are not publicly exposed in
     *     transmission over the internet (eg: if your endpoint lives on the
     *     same box it's probably ok, if it has to resolve against any
     *     external DNS, it's probably not).
     *     This should return null if this is not applicable.
     */
    protected function getRequestPassword( $request_key = null, $previous = null )
    {
        return null;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string Returns a string that designates the expected request format
     *     (eg: json, xml, html, plaintext, etc)
     */
    protected function getRequestFormat( $request_key = null, $previous = null )
    {
        return 'json';
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return array Returns an associative array of headers to send with the request,
     *     where the key is the header name, and the value is the header body.
     *     Header bodies must be already formatted when returned from this method
     *     if they exist. If the key of the return value is numeric, the header
     *     will be assumed to be fully encapsulated in the value, and the key will
     *     not be prefixed. This is useful if you have to pass the same header
     *     numerous times for a valid request, otherwise they should be key/value
     *     format.
     */
    protected function getRequestHeaders( $request_key = null, $previous = null )
    {
        return array();
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return array|bool If no cookies are required for the request, return false.
     *     Otherwise return an array of cookies, which for multipart requests will
     *     be available from in the `$previous` parameter.
     */
    protected function getRequestCookies( $request_key = null, $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string|array|bool If a message body is to be provided,
     *     it should be returned here. Depending on the api, this can be
     *     either a string, an array, or not needed at all. It will be
     *     formatted into the format described under `getRequestFormat`
     *     if it is an array (eg json will be json encoded, xml will be
     *     turned into an xml string, etc).
     */
    protected function getRequestMessage( $request_key = null, $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string|false Returns the application id string if one is required,
     *     which is generally the case when you are leveraging an api key for a
     *     registered connection with a 3rd party that requires prior application
     *     registration (eg: Facebook Open Graph, Google Web Apps, etc). If you are
     *     calling a general web api that does not require client prior
     *     authentication through their own internal system,
     *     this should return false.
     */
    protected function getApplicationId( $request_key = null, $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string Represents the secret key obtained either by direct registration,
     *     or from an Oauth handshake that represents the remote user session.
     *     If this is not relevant, return false.
     */
    protected function getApplicationSecret( $request_key = null,
        $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string|false Returns the application token, or the user token,
     *     if either of those is relevant. Some apis require an application id,
     *     token, and secret, some only one, and some a combination of two.
     *     This logic provides the means to provide all three as required.
     *     If an application key is not required, return false.
     */
    protected function getApplicationKey( $request_key = null, $previous = null )
    {
        return false;
    }

    /**
     *
     * @param string|null $request_key If the default is being obtained,
     *     this will be null. If it is requesting information about a specific
     *      endpoint, it will provide the slug identifier for that endpoint.
     * @param array|null $previous If there is no prior request, this will be null.
     *     If there is a prior request, this will contain the response data from
     *     the last request.
     * @return string|bool Returns the callback uri for multi-part handshakes
     *     if one exits, or false if one does not. This is for Oauth/OpenId/SAML
     *     authentication handshakes, which generally require several back and forth
     *     independent requests between servers.
     */
    protected function getRequestCallback( $request_key = null, $previous = null )
    {
        return false;
    }

    /**
     * Preflights a final uri to insure it is actually a valid structure worth
     * the overhead of checking with a remote call.
     *
     * @param type $url
     * @return bool
     */
    protected function checkUrl( $url )
    {
        if ( !$url || !is_string( $url ) || !preg_match( '/^http(s)?:\/\/[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i',
                $url ) )
        {
            return false;
        }
        return true;
    }

    protected function buildEndpoint( $details )
    {

    }

    protected function validateProtocol( $protocol )
    {
        return in_array( $protocol, array(
            'http',
            'https' ) );
    }

    protected function validateHost( $host )
    {
        // no-op
    }

    protected function validateUri( $uri )
    {
        // no-op
    }

    protected function validateMethod( $method )
    {
        // no-op
    }

    protected function validateAuth( $auth )
    {
        // no-op
    }

    protected function validatePort( $port )
    {
        // no-op
    }

    protected function validateUser( $user )
    {
        // no-op
    }

    protected function validatePassword( $password )
    {
        // no-op
    }

    protected function validateFormat( $format )
    {
        // no-op
    }

    protected function validateHeaders( $auth )
    {
        // no-op
    }

    protected function validateCookies( $cookies )
    {
        // no-op
    }

    protected function validateMessage( $message )
    {
        // no-op
    }

    protected function validateCallback( $callback )
    {
        // no-op
    }

    protected function validateKey( $key )
    {
        // no-op
    }

    protected function validateSecret( $secret )
    {
        // no-op
    }

    protected function validateAppId( $id )
    {
        // no-op
    }

    /**
     * Sets the baseline internals up for use if they are not already set up.
     * This only occurs once per runtime.
     */
    private function _initializeRest()
    {
        if ( !self::$rest_initialized )
        {
            $config = $this->load( 'library', 'config\\RestConfig' );
            self::$endpoints = $config;
            self::$rest_initialized = true;
        }
    }

    /**
     * Builds the correct endpoint uri based on the provided configuration.
     *
     * @param type $handle
     * @param type $endpoints
     */
    private function _constructEndpointUri( $handle, $endpoints )
    {

    }

}
