<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\rest;

/**
 * Abstract Rest Client
 * Provides baseline abstraction for rest client usage.
 *
 * This is written with the intention of being eventually used as an internal
 * driver for a Psr-7 response object, although it does not currently populate
 * any such object. It's output is formatted so that it can pretty much generate
 * a response that can be used to package a response object without further
 * formatting.
 *
 * @note Psr-7 is not currently directly implemented into Sanctity but it will
 *     be in the very near future. Its rest logic is written with the explicit
 *     intention of facilitating this transition smoothly.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.2.0
 */
abstract class AbstractRestClient
    extends AbstractRest
    implements \mopsyd\sanctity\interfaces\libs\rest\RestClientInterface
{

    /**
     * This is the kind of thing that makes Functional programmers cry.
     */
    use \mopsyd\sanctity\traits\StatePattern;

    private static $response_parse_methods = array(
        // For head requests. Does not retain the body.
        'null' => 'parseNullResponse',
        // JSON parses to array on receipt
        'application/json' => 'parseJsonResponse',
        // XML will parse to array, currently just returns the xml string
        'application/xml' => 'parseXmlResponse',
        // HTML will parse to a DOMDocument wrapper (stringable), currently just returns the html string
        'text/html' => 'parseHtmlResponse',
        // plaintext just returns the payload unmodified. This is also the default for unknown mimes
        'text/plain' => 'parsePlaintextResponse',
        // CSV will parse to array, currently just returns the csv string
        'application/csv' => 'parseCsvResponse',
    );

    /**
     * Represents the raw header data while a return request is occurring.
     * This is only used as a container for raw header data internally prior
     * to parsing, and is reset back to an empty state when parsing is
     * completed.
     *
     * @var array
     */
    private $response_headers_raw = array();

    /**
     * The response headers returned with the request in their parsed format.
     * These should be directly compatible with the Psr-7 spec, and be able to
     * be entered into a Psr-7 response object directly without incurring an
     * error.
     * @var array
     */
    private $response_headers = array();

    /**
     * The response mime type.
     * @var string
     */
    private $response_content_type = null;

    /**
     * The raw response body.
     * @var string
     */
    private $response_body_raw = null;

    /**
     * The parsed response body, if applicable. May be any format expected
     * by the response, but will generally be parsed into an array if at
     * all possible.
     * @var mixed
     */
    private $response_body = array();

    /**
     * The HTTP response code for the request. In the vast majority of cases
     * this indicates whether the request was successful. However some cases
     * will provide a status field in the response and otherwise always return
     * a 200 header, in which case a subclass needs to handle the success
     * determination manually, as internals will trigger a success state
     * on a 200 header that responds in the expected format.
     * @var int
     */
    private $response_code = array();

    /**
     * An array of headers to send with the request if any.
     * If empty, the header portion is omitted.
     * @var array
     */
    private $request_headers = array();

    /**
     * Any cookies that need to be sent to the remote with a request.
     * @note This is not implemented yet. Currently an in-memory-only
     * default cookie jar is utilized.
     * @var array
     */
    private $request_cookies = array();

    /**
     * The request body payload intended to be sent, if any.
     * @var null|string|resource
     */
    private $request_body = array();

    /**
     * The request protocol, typically http or https
     * @var string http|https|ftp|etc
     */
    private $request_protocol = null;

    /**
     * The request method
     * @var string get|post|put|delete|options|connect|trace|head
     */
    private $request_method = null;

    /**
     * The host for the request, including the protocol, host,
     * and authority (if any), but not the request uri.
     * @var string
     */
    private $request_host = null;

    /**
     * The request uri for the remote, minus the host, authority, and protocol.
     * @var string
     */
    private $request_uri = null;

    /**
     * The user agent used for the request. By default, the same user agent as
     * the end user is used for all requests that occur from an http call origin,
     * and a default firefox header is used for requests that do not originate
     * from http.
     * @var string
     */
    private $request_user_agent = null;

    /**
     * The curl handler that is utilized to handle a request during
     * the process of sending, receiving, and parsing it.
     *
     * @var resource
     */
    private $curl_handler = null;

    /**
     * Designates the state of the request along the following lines:
     * - uninitialized: being unformatted, idle state.
     * - pending: being not yet sent or fully built. Data is partial and not complete enough to send yet.
     * - initialized: being fully built but not sent.
     * - sent: being sent but no response yet. Awaiting the remote host.
     * - response: response has been received but is not fully parsed yet.
     * - complete: fully handled and ready to use externally.
     * @var string
     */
    private $request_state = 'uninitialized';
    private $request_container;
    private $response_container;

    public function __construct( $dependencies = null, $args = array() )
    {
        $this->initializeStatePattern();
        parent::__construct();
    }

    /**
     * Resets the curl wrapper.
     */
    public function reset()
    {
        $this->updateState( 'uninitialized' );
        $this->updateState( 'pending' );
        return $this;
    }

    /**
     * Packages the required data that is needed for
     * the rest client to send a valid request.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request
     */
    public function package( \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request,
        array $arguments = array() )
    {
        foreach ( $this->getEndpointDefaults() as
            $key =>
            $value )
        {
            if ( !$request->has( $key ) )
            {
                $request[$key] = $value;
            } elseif ( is_array( $request->get( $key ) ) )
            {
                $existing = $request->get( $key );
                $request[$key] = array_replace_recursive( $value, $existing );
            }
        }
        $this->validateRequest( $request, $arguments );
        $this->request_container = $request;
        $this->updateState( 'initialized' );
        return $this;
    }

    public function send()
    {
        $this->updateState( 'sent' );
        return $this;
    }

    public function getRequest()
    {
        return $this->request_container;
    }

    public function getResponse()
    {
        return $this->response_body;
    }

    public function getHeaders()
    {
        return $this->response_headers;
    }

    public function getStatusCode()
    {
        return $this->response_code;
    }

    public function getMime()
    {
        return $this->response_content_type;
    }

    /**
     * Declares the valid states for the rest client.
     *
     * These correspond to the methods that will fire at various
     * steps of the curl handling process.
     *
     * @return array
     */
    protected function declareValidStates()
    {
        return array(
            'uninitialized' => 'resetRestClient',
            'pending' => 'idle',
            'initialized' => 'prepareCurl',
            'sent' => 'sendRequest',
            'response' => 'parseResponse',
            'complete' => 'packageResponse',
        );
    }

    /**
     * Declares the default state. This is the state the object is set to
     * when `resetState` is called.
     * @return null|string
     */
    protected function declareDefaultState()
    {
        return 'uninitialized';
    }

    /**
     * Gets an endpoint resource.
     *
     * May or may not require authentication, depending on the circumstances.
     */
    public function get()
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true, // grab the response headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_COOKIEFILE => '', // keep cookies in memory only
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            CURLOPT_ENCODING => "", // handle compressed
            CURLOPT_USERAGENT => $this->request_user_agent,
            CURLOPT_AUTOREFERER => true, // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 10, // time-out on connect
            CURLOPT_TIMEOUT => 10, // time-out on response
            CURLOPT_VERBOSE => 1, // get the body and the headers
            CURLOPT_HEADERFUNCTION => array(
                $this,
                'headerLine' ), // parse headers so they can be removed from the raw body after the fact
        );
        $this->_setCurlOptions( $options );
        $this->_executeRequest();
        $this->_releaseCurl();
    }

    /**
     * Posts data to an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use,
     * but is often used to establish authentication also.
     *
     * It is usually a safe bet that authentication needs to either be
     * provided with the request or obtained prior to the request.
     */
    public function post()
    {
        d( '@todo: aClling a REST post method.' );
    }

    /**
     * Places an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use.
     *
     * It is usually a safe bet that authentication needs
     * to be obtained prior to the request.
     */
    public function put()
    {
        d( '@todo: Calling a REST put method.' );
    }

    /**
     * Deletes an endpoint resource.
     *
     * Typically requires an authentication handshake prior to use.
     *
     * It is usually a safe bet that authentication needs
     * to be obtained prior to the request.
     */
    public function delete()
    {
        d( '@todo: Calling a REST delete method.' );
    }

    /**
     * Requests the options available for further requests.
     *
     * It's excellent when an endpoint implements this because their api is
     * automatically discoverable, but most of them don't leverage it in any
     * coherent manner or at all in most cases.
     *
     * Usually does not require authentication.
     * The response may be expanded if authentication is provided.
     */
    public function options()
    {
        $options = array(
            CURLOPT_CUSTOMREQUEST => 'OPTIONS',
            CURLOPT_RETURNTRANSFER => true, // grab the response headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_COOKIEFILE => '', // keep cookies in memory only
            CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
            CURLOPT_ENCODING => "", // handle compressed
            CURLOPT_USERAGENT => $this->request_user_agent,
            CURLOPT_AUTOREFERER => true, // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 10, // time-out on connect
            CURLOPT_TIMEOUT => 10, // time-out on response
            CURLOPT_VERBOSE => 1, // get the body and the headers
            CURLOPT_HEADERFUNCTION => array(
                $this,
                'headerLine' )
        );
        $this->_setCurlOptions( $options );
        $this->_executeRequest();
        $this->_releaseCurl();
    }

    /**
     * Requests that an endpoint server mirror the request back to the client.
     * Typically regarded as unsafe, but provided for completion as it is part
     * of the http spec still currently. Provided internals do not use this,
     * but you may use it for your own definitions even if it is discouraged
     * to do so.
     *
     * It is usually the safest bet not to use this and to assume
     * it is disabled in the majority of cases.
     */
    public function trace()
    {
        d( '@todo: Calling a REST trace method.' );
        exit;
    }

    /**
     * Maintains an open channel request.
     *
     * Typically used only for proxying and command line utilites that
     * hold an open socket channel, such as for a chat app or continuous
     * reporting module. This is here for completion, you will probably not
     * use it for anything Sanctity related unless you are doing something
     * highly custom.
     *
     * It is usually a safe bet to assume this is not implemented unless
     * explicitly stated in an api doc that it is.
     */
    public function connect()
    {
        d( '@todo: Calling a REST connect method.' );
    }

    /**
     * Pulls only the headers for a request.
     *
     * This is a very fast method for preflighting a request, and is performant
     * on the client side for checking if a request can resolve before committing
     * to pulling a large subset data payload and incurring the expense of parsing
     * a large volume of data. Typically not required on small payloads, but a
     * pragmatic option on potentially large payloads. Response time may still
     * take a while if the endpoint does not implement separation between head
     * and get requests well (they may still parse the whole logic internally
     * instead of just sending the headers, in which case the time savings is
     * negigible, but the payload savings is still substantial if it doesn't resolve).
     */
    public function head()
    {
        d( '@todo: Calling a REST head method.' );
    }

    /**
     * Applies a partial modification to a resource.
     *
     * It is usually safest to assume this is not implemented,
     * as it is a reasonably new addition to the http spec.
     * It incurs less cost than a post or put call where it is implemented,
     * and should be used in place of those when the option presents itself
     * and it is pragmatic to do so.
     */
    public function patch()
    {
        d( '@todo: Calling a REST head method.' );
    }

    /**
     * This is used for header parsing automation with CURLOPT_HEADERFUNCTION.
     *
     * It is passed to the curl handler, and is generally not useful for public
     * consumption unless you are trying to manually parse a raw header that is
     * split by line break for some reason. It facilitates the internals
     * retaining the header and body in one request to perform more efficiently
     * without data loss, and is only declared public so it is reachable
     * by cURL as a callback.
     *
     * @example curl_setopt($this->curl_handler, CURLOPT_HEADERFUNCTION, array($this, 'headerLine' ) );
     * @param type $curl
     * @param type $header_line
     */
    public function headerLine( $curl, $header_line )
    {
        $this->response_headers_raw[] = $header_line;
        return strlen( $header_line );
    }

    /**
     * State method that designates to wait for the
     * user to package the remaining response data.
     */
    protected function idle()
    {
        // no-op. Overrides may choose to do something here though.
    }

    /**
     * State method that prepares the curl handle
     * for the outbound request.
     */
    protected function prepareCurl()
    {
        $this->_instantiateCurl();
    }

    /**
     * State method that fires the actual request.
     */
    protected function sendRequest()
    {
        $method = $this->request_method;
        $this->$method();
        $this->updateState( 'response' );
    }

    /**
     * State method that parses the raw response.
     */
    protected function parseResponse()
    {
        // parse stuff, then...
        $this->parseResponseHeaders();
        $this->_parseResponseBody();
        $this->updateState( 'complete' );
    }

    /**
     * State method that packages the parsed response into the return format.
     */
    protected function packageResponse()
    {
        // No-op until Psr-7 implementation. Patience padowan.
    }

    /**
     * State method that resets all parameters to their null state
     * to prepare for another request.
     */
    protected function resetRestClient()
    {
        $this->response_headers_raw = array();
        $this->response_headers = array();
        $this->response_content_type = null;
        $this->response_body_raw = null;
        $this->response_body = array();
        $this->response_code = array();
        $this->request_headers = array();
        $this->request_cookies = array();
        $this->request_body = array();
        $this->request_protocol = null;
        $this->request_method = null;
        $this->request_host = null;
        $this->request_uri = null;
        $this->request_user_agent = null;
        $this->curl_handler = null;
        $this->request_container = null;
        $this->response_container = null;
    }

    /**
     * Validates that a given request is correct, and can result in
     * a potentially successful exchange with a remote host.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request
     *     The endpoint to contact
     * @param array $arguments
     *     The arguments provided for the endpoint
     */
    protected function validateRequest( \mopsyd\sanctity\interfaces\libs\container\EndpointContainerInterface $request,
        array $arguments = array() )
    {
        /**
         * Check that required arguments are all provided.
         */
        foreach ( $request['required'] as
            $key =>
            $required )
        {
            // Required args must be provided and also must be correctly formatted.
            if ( !array_key_exists( $key, $arguments ) )
            {
//                d( $arguments, $key );
//                exit;
                throw new \mopsyd\sanctity\libs\exception\SanctityException(
                sprintf( 'Error encountered in [%1$s]. Required rest '
                    . 'parameter [%2$s] was not provided.', get_class( $this ),
                    $key )
                );
            }
            $this->_typecheckArgument( $required, $arguments[$key] );
        }
        foreach ( $request['optional'] as
            $key =>
            $optional )
        {
            // Optional args don't have to be provided, but they must
            // be correctly formatted if they are.
            if ( array_key_exists( $key, $arguments ) )
            {
                $this->_typecheckArgument( $required, $arguments[$key] );
            }
        }
        $this->_interpolateArguments( $request, $arguments );
        $this->checkUrl( $this->request_container['uri'] );
        $this->request_protocol = $this->request_container['protocol'];
        $this->request_method = $this->request_container['method'];
        $this->request_host = $this->request_container['host'];
        $this->request_uri = $this->request_container['uri'];
        $this->request_user_agent = ( array_key_exists( 'HTTP_USER_AGENT',
                $_SERVER ) )
            ? $_SERVER['HTTP_USER_AGENT'] // Use the user's normal user agent if possible.
            : "Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3"; // Use a generic user agent for cli requests.
    }

    protected function parseNullResponse()
    {
        // For head-only responses
        $this->response_body = null;
    }

    protected function parseJsonResponse()
    {
        $this->response_body = json_decode( $this->response_body_raw, 1 );
    }

    protected function parseXmlResponse()
    {
        // Will parse with the DOMDocument parser shortly
        $this->response_body = $this->response_body_raw;
    }

    protected function parseHtmlResponse()
    {
        // Will parse with the DOMDocument parser shortly
        $this->response_body = $this->response_body_raw;
    }

    protected function parsePlaintextResponse()
    {
        // no-op
        $this->response_body = $this->response_body_raw;
    }

    protected function parseCsvResponse()
    {
        // Will get a csv parser in place here shortly
        $this->response_body = $this->response_body_raw;
    }

    protected function parseResponseHeaders()
    {
        $headers = array();
        foreach ( $this->response_headers_raw as
            $header )
        {
            if ( strpos( $header, 'HTTP/' ) === 0 || trim( $header ) === '' )
            {
                // Skip the response code, we already have this.
                continue;
            }
            $key = strtolower( substr( $header, 0, strpos( $header, ':' ) ) );
            $value = trim( substr( $header, strlen( $key ) + 1 ) );
            $headers[$key] = $value;
        }
        $this->response_headers = $headers;
    }

    private function _instantiateCurl()
    {
        $this->curl_handler = curl_init( $this->request_container['uri'] );
    }

    private function _releaseCurl()
    {
        curl_close( $this->curl_handler );
        $this->curl_handler = null;
    }

    private function _setCurlOptions( array $options )
    {
        curl_setopt_array( $this->curl_handler, $options );
    }

    private function _executeRequest()
    {
        $this->response_body_raw = curl_exec( $this->curl_handler );
        $this->response_code = curl_getinfo( $this->curl_handler,
            CURLINFO_HTTP_CODE );
        $this->response_content_type = curl_getinfo( $this->curl_handler,
            CURLINFO_CONTENT_TYPE );
    }

    private function _generateCurlHeaders()
    {
        return array();
    }

    private function _generateCurlCookies()
    {
        return array();
    }

    private function _generateCurlBody()
    {
        return '';
    }

    private function _typecheckArgument( $argument, $provided )
    {

    }

    /**
     * Interpolates arguments into the request body that need to be part
     * of the uri for the rest endpoint to consume appropriately.
     *
     * This occurs frequently in get strings and some api endpoint uris.
     * This allows all such arguments to be treated identically to the
     * standard arument array without regard to how they are presented
     * to the api by internals.
     *
     * @param \mopsyd\sanctity\interfaces\libs\container\EndpointContainer $endpoint
     * @param array $arguments
     * @return void
     * @internal
     */
    private function _interpolateArguments( $endpoint, $arguments )
    {
        $this->arguments = $arguments;
        $this->request_container = $endpoint;
        foreach ( $this->request_container as
            $key =>
            $value )
        {
            if ( is_string( $value ) && preg_match_all( '/\{[A-Za-z0-9\_\-]{1,}\}/',
                    $value, $matches ) )
            {
                foreach ( $matches[0] as
                    $match )
                {
                    $k = trim( $match, '{}' );
                    if ( !array_key_exists( $k, $arguments ) )
                    {
                        throw new \mopsyd\sanctity\libs\exception\SanctityException(
                        sprintf( 'Error encountered in [%1$s]. Expected key '
                            . '[%2$s] not found in [%3$s].', get_class( $this ),
                            $k, '$arguments' )
                        );
                    }
                    $value = str_replace( $match, $arguments[$k], $value );
                }
                $this->request_container[$key] = $value;
            }
        }
    }

    /**
     * Calls the correct known parse method for the given content type
     * and request type, if a known parse method is available. If not,
     * it treats the payload as plaintext and returns it unmodified.
     * @return void
     * @internal
     */
    private function _parseResponseBody()
    {
        $content_type = 'text/plain';
        if ( $this->request_method === 'head' )
        {
            // Head requests do not contain a body to parse.
            $content_type = 'null';
        }
        if ( array_key_exists( 'content-type', $this->response_headers ) )
        {
            foreach ( self::$response_parse_methods as
                $mime =>
                $method )
            {
                if ( strpos( $this->response_content_type, $mime ) === 0 )
                {
                    $content_type = $mime;
                    break;
                }
            }
        }
        $method = self::$response_parse_methods[$content_type];
        $this->$method();
    }

}
