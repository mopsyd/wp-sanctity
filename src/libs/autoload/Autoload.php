<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\autoload;

/**
 * Sanctity Autoloader
 * This is a simple Psr-4 autoloader for
 * use cases where Composer is not available.
 *
 * This acts as a redundancy for any
 * issues in the composer load process.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class Autoload
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\SanctityInterface
{

    protected static $prefixes = array();
    private $_is_registered = false;
    private static $sanctity_registered = false;

    /**
     * Static initialization for the autoloader.
     * The instance created by this method is not retained locally.
     * It is registered into the spl_stack and then returned.
     * None of that singleton nonsense. We want this class to be unit testable.
     * @return mopsyd\sanctity\libs\autoload\Autoload
     */
    public static function init( $dependencies = null, $args = array() )
    {
        $instance = parent::init( $dependencies, $args );
        if ( !self::$sanctity_registered )
        {
            //If not already registered onto the
            //autoload stack, register this package
            //automatically.
            $instance->addNamespace( 'mopsyd\\sanctity',
                SANCTITY_BASEDIR . 'src' . DIRECTORY_SEPARATOR );
            self::$sanctity_registered = true;
        }
        return $instance;
    }

    public function __construct( $dependencies = null, array $args = array() )
    {
        foreach ( $args as
            $namespace =>
            $path )
        {
            $this->addNamespace( $namespace, $path );
        }
        $this->register();
    }

    public function isRegistered()
    {
        return $this->_is_registered;
    }

    public function register()
    {
        spl_autoload_register( array(
            $this,
            'loadClass' ) );
        $this->_is_registered = true;
    }

    public function addNamespace( $prefix, $base_dir, $prepend = false )
    {
        // normalize namespace prefix
        $prefix = trim( $prefix, '\\' ) . '\\';

        // normalize the base directory with a trailing separator
        $base_dir = rtrim( $base_dir, '\\/' ) . DIRECTORY_SEPARATOR;
        if ( !is_readable( $base_dir ) )
        {
            throw new \InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. Cannot map Psr4 namespace [%s] because the provided directory [%s] %s.',
                __METHOD__, $prefix, $base_dir,
                ( is_dir( $base_dir )
                    ? 'is not readable'
                    : 'does not exist' ) ) );
        }
        if ( isset( self::$prefixes[$prefix] ) === false )
        {
            self::$prefixes[$prefix] = array();
        }
        if ( $prepend )
        {
            array_unshift( self::$prefixes[$prefix], $base_dir );
        } else
        {
            array_push( self::$prefixes[$prefix], $base_dir );
        }
    }

    public function loadClass( $class )
    {
        $prefix = $class;
        while ( false !== $pos = strrpos( $prefix, '\\' ) )
        {
            $prefix = substr( $class, 0, $pos + 1 );
            $relative_class = substr( $class, $pos + 1 );
            $mapped_file = $this->loadMappedFile( $prefix, $relative_class );
            if ( $mapped_file )
            {
                return $mapped_file;
            }
            $prefix = rtrim( $prefix, '\\' );
        }
        return false;
    }

    public function getPath( $namespace )
    {
        $namespace = trim( $namespace, '\\' ) . '\\';
        if ( !array_key_exists( $namespace, self::$prefixes ) )
        {
            return false;
        }
        $directories = array();
        foreach ( self::$prefixes[$namespace] as
            $key =>
            $directory )
        {
            $directories[] = $directory;
        }
        if ( count( $directories ) === 1 )
        {
            return array_shift( $directories );
        }
        return $directories;
    }

    public function getNamespace( $path )
    {
        if ( !is_string( $path ) || !realpath( $path ) )
        {
            return false;
        }
        $path = realpath( $path ) . DIRECTORY_SEPARATOR;
        foreach ( self::$prefixes as
            $namespace =>
            $set )
        {
            foreach ( $set as
                $set_key =>
                $directory )
            {
                if ( $directory === $path )
                {
                    return rtrim( $namespace, '\\' );
                }
            }
        }
        return false;
    }

    protected function loadMappedFile( $prefix, $relative_class )
    {
        if ( isset( self::$prefixes[$prefix] ) === false )
        {
            return false;
        }
        foreach ( self::$prefixes[$prefix] as
            $base_dir )
        {
            $file = $base_dir
                . str_replace( '\\', DIRECTORY_SEPARATOR, $relative_class )
                . '.php';
            if ( $this->requireFile( $file ) )
            {
                return $file;
            }
        }
        return false;
    }

    protected function requireFile( $file )
    {
        if ( file_exists( $file ) )
        {
            require $file;
            return true;
        }
        return false;
    }

}
