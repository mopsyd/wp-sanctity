<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\parser;

/**
 * XmlParser
 * Parses HTML into a filterable format instead of relying on
 * sketchy regex patterns that choke on HTML constantly.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @see https://stackoverflow.com/a/1732454/1288121
 *     HE COMES! Never use regex for HTML. Ever.
 *     Except that one time? No, not that time either.
 */
class HtmlParser
    extends \mopsyd\sanctity\AbstractBase
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    private $subject;
    private $attributes;
    private $xpath;
    private $errors = array();
    private $is_wrapped = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct();
    }

    /**
     * Scopes the object to a provided DOM string.
     *
     * @param \DOMDocument $content
     * @return \mopsyd\sanctity\libs\parser\HtmlParser
     */
    public function fromDom( \DOMDocument $content )
    {
        $this->subject = $content;
        return $this;
    }

    /**
     * Converts an html string into a DOMDocument object.
     *
     * @param type $content
     * @return \mopsyd\sanctity\libs\parser\HtmlParser
     */
    public function parse( $content )
    {
        $this->subject = $this->_getDomObject( $content );
        return $this;
    }

    /**
     * Casts the working result to a string and returns it.
     *
     * @return string
     */
    public function get()
    {
        return $this->_resolveStringOutput();
    }

    public function getByTag( $tag )
    {
        $collection = array();
        $results = $this->subject->getElementsByTagName( $tag );
        foreach ( $results as
            $key =>
            $node )
        {
            $dom = $this->_getDomObject( '' );
            $dom->appendChild( $dom->importNode( $node->cloneNode( true ), true ) );
            $collection[] = rtrim( str_replace( '<div></div>', null,
                    $dom->saveHTML() ) );
        }
        return $collection;
    }

    public function removeByTag( $tag )
    {
        $results = $this->subject->getElementsByTagName( $tag );
        $remove = array();
        foreach ( $results as
            $key =>
            $node )
        {
            $remove[$key] = $node;
        }
        foreach ( $remove as
            $key =>
            $node )
        {
            $remove[$key]->parentNode->removeChild( $node );
        }
        return $this;
    }

    /**
     * Used as a temporary error handler to supress constant errors that
     * occur when parsing non-well formed html, which is usually going
     * to happen when a bunch of noobs are constantly trying to regex filter it
     * because they don't know how to code properly.
     *
     * For the millionth time, DO NOT USE REGEX ON HTML. NEVER EVER.
     *
     * @param type $errno
     * @param type $errstr
     * @param type $errfile
     * @param type $errline
     */
    public function parseErrorCapture( $errno, $errstr, $errfile, $errline )
    {
        $this->errors[] = array(
            $errno,
            $errstr,
            $errfile,
            $errline );
    }

    private function _getDomObject( $content )
    {
        set_error_handler( array(
            $this,
            'parseErrorCapture' ) );
        $dom = new \DOMDocument();
        $dom->loadHtml( mb_convert_encoding( $this->_resolveDomWrapper( $content ),
                'HTML-ENTITIES', 'UTF-8' ),
            LIBXML_COMPACT | LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
        restore_error_handler();
        return $dom;
    }

    private function _ResolveDomWrapper( $content )
    {
        $result = $content;
        if ( !$this->_checkDocType( $content ) )
        {
            // If the provided string does not contain a doctype, it will be
            // temporarily wrapped in a div to prevent an illegal dom string
            // that will cause DOMDocument to incorrectly shuffle it's
            // internal nodes. This element is removed after the fact
            // if it is applied.
            $result = '<div>'
                . $content
                . '</div>';
            $this->is_wrapped = true;
        }
        return $result;
    }

    private function _checkDocType( $content )
    {
        return strpos( $content, '<!DOCTYPE ' ) === 0;
    }

    private function _resolveStringOutput()
    {
        $result = $this->subject->saveHTML();
        if ( $this->is_wrapped )
        {
            $result = substr( $result, strlen( '<div>' ) );
            $result = substr( $result, 0,
                strlen( $result ) - strlen( '</div>' ) - 1 );
        }
        return $result;
    }

}
