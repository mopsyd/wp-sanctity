<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\sass;

/**
 * Sass Compiler Adapter
 *
 * This class performs compiling and serving of Sass Stylesheets.
 *
 * This class is laid out to only allow safe operations against known
 * cache directories. It does not permit unscoped use,
 * or user defined path definitions. It also provides a means of
 * registering security filters that evaluate the provided source,
 * which may break any parse operation if any exploit patterns are detected.
 * Security filters only evaluate, and do not modify the raw source,
 * so they themselves cannot be used for exploitative purposes either.
 *
 * As this class saves persistent data to disk/cache/etc, there is a good deal
 * of security extensibility in place to insure that no malicious raw SASS
 * source is handled without being verified as legit.
 *
 * Provided source is not internally escaped or modified.
 * It's either a pass/fail. It's already clean, or it's no good.
 * Rather than attempting to ambiguously correct malformed data,
 * this class just takes the "only give me clean raw source" approach,
 * so no filters that are themselves exploits, or corrupting mutations
 * to the sass source are possible either.
 *
 * This allows for very speedy prototyping of
 * CSS that is dynamically generated, and is also clean,
 * served securely, and only consists of trusted input.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 *
 * @note Security checks may be registered with
 *     the `registerSecurityFilter` method.
 */
class Sass
    extends \mopsyd\sanctity\adapters\AbstractAdapter
{

    const FILE_SUFFIX = 'scss';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Paths where SASS stylesheets are stored.
     * These are relative to the Sanctity root directory.
     * @var array
     */
    private static $cache_paths = array(
        // Branding programmatically generated tweaks
        'branding' => 'etc/cache/sass/branding/',
        // Style programmatically generated tweaks
        'style' => 'etc/cache/sass/style/',
        // Layout programmatically generated tweaks
        'layout' => 'etc/cache/sass/layout/',
        // User edited stylesheets from the editor in the admin
        'editor' => 'etc/cache/sass/uploads/',
    );

    /**
     * An array of callables that evaluate the provided source
     * for any known exploit patterns.
     *
     * These may be registered externally, and all provided source
     * will have to pass all of them to be saved to disk or cache.
     *
     * Format: key = string "handle", value = callable (should take source as its single parameter, and raise any exception on failure).
     *
     * Any exception raised by an exploit filter will be interpreted as a fail,
     * regardless of the exception.
     *
     * All exceptions raised by exploit filters will
     * be recast to a standardized `SanctityException`.
     *
     * @var array
     */
    private static $source_integrity_rules = array();

    /**
     * This is the directory used for caching compiled sass,
     * if the server is active.
     *
     * @note This functionality will have very serious performance implications
     *     for your server if you have a large amount of traffic, because requests
     *     utilizing this functionality cannot be cached at all.
     *     For this reason, it is off by default and requires an
     *     explicit programmatic directive to enable.
     *     The default Sanctity functionality always compiles static source
     *     and serves that.
     *     You should probably also do that if you don't want your site
     *     running slow as death under load, but it's up to you.
     *     This platform also does not apply opinion to what you want
     *     to do with it, which is why this option exists.
     *     Large volume sites should absolutely never ever
     *     ever ever ever use this. Sites on very cheap
     *     economy shared hosting should also never ever ever ever use this.
     *     Only use it if you have decent hosting and also don't have a huge
     *     traffic base, or if you have a very robust load balancing setup
     *     that can handle it in an enterprise cloud distributed setup,
     *     like elastic beanstalk behind cloudfront for example.
     *
     * @var string
     */
    private static $compile_dir;

    /**
     * This is the directory used for generating literal css files that
     * are web accessible, if the server is not active. Operations that
     * update backend parameters that alter custom styles will output
     * into a servable file within this directory, which is just a
     * standard css file at this point.
     *
     * This is the suggested usage, as it relieves strain on the backend
     * by only serving static css, which only is generated when an
     * administrative option that requires changes occurs. The option to
     * serve dynamic css is supported, but should be avoided unless you
     * have a pretty beefy server installation to handle the load,
     * or a very low traffic footprint that is not going to crush
     * your webserver.
     *
     * @var string
     */
    private static $css_dir;

    /**
     * Sass Color Module
     * @var \Leafo\ScssPhp\Colors
     */
    private static $colors;

    /**
     * For auto-initialization, in case it gets called prior
     * to manually calling it for some purpose.
     * @var type
     */
    private static $adapter_initialized = false;

    /**
     * Sass Compiler
     * @var \Leafo\ScssPhp\Compiler
     */
    private $compiler;

    /**
     * The current scope of the sass compiler. This has to be set
     * before calling any operations.
     *
     * @var string
     */
    private $scope;

    /**
     * Sass Adapter Constructor
     *
     * @param type $dependencies (optional) not used.
     *     The adapter fetches its own dependencies, as it is specifically
     *     meant to abstract their use.
     *     There's nothing to inject here.
     * @param array $args (optional) May optionally be passed an array
     *     with the key `scope`, in which case it will scope itself to
     *     the provided key on instantiation. This can also be called
     *     after instantiation manually, but must be done before any
     *     other functionality is available. This is not enforced in
     *     the constructor to facilitate prototyping of sass adapters
     *     for numerous models from one active instance for performance.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initializeAdapter();
        if ( array_key_exists( 'scope', $args ) )
        {
            $this->setTemplateScope( $args['scope'] );
        }
        parent::__construct();
    }

    /**
     * Sets the scope to a known cache directory. For security purposes,
     * almost no other functionality of this class is accessible until
     * this is done appropriately.
     *
     * @param string $type A key corresponding to a known scope.
     *     They will be the keys of the static class property `cache_paths`,
     *     and correspond to directories relative to the base
     *     Sanctity install directory.
     *     Child themes still use this directory and do not generate their own
     *     to avoid excessive complexity in implementation.
     *
     * @throws \mopsyd\lib\exception\SanctityException
     *     If the requested scope is not valid.
     *     Only known cache paths may be requested.
     *     There currently is no mechanism to create new ones.
     *     This may be added in the future, but for now it is
     *     intended to perform only within expected bounds.
     */
    public function setTemplateScope( $type )
    {
        $valid = array_keys( self::$cache_paths );
        if ( !in_array( $type, $valid ) )
        {
            throw new \mopsyd\lib\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Provided '
                . 'path [%2$s] is not valid.', get_class( $this ), $type )
            );
        }
        $this->scope = SANCTITY_BASEDIR . self::$cache_paths[$type];
        $this->compiler->setImportPaths( $this->scope );
    }

    /**
     * Returns the directory that the Sass Adapter is currently scoped to,
     * or false if it is unscoped.
     *
     * If this returns false, pretty much every other method is going
     * to raise an exception until you call `setTemplateScope`
     * with a valid scope.
     *
     * For file security, as this class interacts directly with the filebase,
     * it is only allowed to scope to a small number of known cache directories
     * that are already in secure locations, and are not accessible elsewhere.
     * You may not add additional directories, as doing so would open up the
     * possiblity of code injection attacks.
     *
     * @return bool|string
     */
    public function getTemplateScope()
    {
        try
        {
            $this->_checkSourceScope();
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            return false;
        }
        return $this->scope;
    }

    /**
     * Registers a security filter for SASS source input.
     *
     * Any raw source provided must be verified by all existing filter callbacks
     * prior to being saved or otherwise handled by the SASS compiler.
     *
     * These may be registered by anything, and may not be
     * unregistered once provided.
     *
     * Security filters only evaluate source on a pass/fail basis.
     * Return values are ignored, so there is no leeway to use the
     * security filter itself as a security exploit.
     *
     * Filters may not be replaced. Attempting to register a different
     * security filter under the same key will raise an exception,
     * but you may provide the exact same filter without issue.
     * This prevents any scripts from removal of expected security parameters,
     * though numerous scripts attempting to register the same security filter
     * is not restricted. This prevents any exploits from nulling expected
     * security rules that already exist. There is no mechanism to remove,
     * alter, or override any registered security filter. You may only add
     * additional ones.
     *
     * @param type $key An identifying key for the filter.
     * @param type $callable Any callable. Should take the raw source as its
     *     first parameter, and should not require any additional parameters
     *     after the first parameter. Return values are ignored. Filters are
     *     expected to raise an exception if an error occurs. Any exception will
     *     be interpreted as a security exploit being detected, and will block
     *     all further parsing of the raw source. No exception will
     *     be interpreted as a pass, and the source will be passed to the next
     *     subsequent filter until none are left to evaluate it. It will only
     *     be parsed if all filters resolve.
     *
     * @note Malformed filters will probably block all source from being parsed.
     *     Write clean filters or you will break the SASS functionality completely.
     *     This script does not evaluate why any exception was raised, it simply
     *     interpets all exceptions as unacceptable raw source.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If a non-callable filter is provided.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If an attempt is made to replace an existing filter with
     *     a different callback than previously declared.
     *     Filters may only be added, not removed or modified.
     *     This will not occur if you provide the same filter
     *     with an identical callback, but any alternate callback
     *     will trigger this.
     */
    public function registerSecurityFilter( $key, $callable )
    {
        if ( !is_callable( $callable ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered at [%1$s]. '
                . 'Provided security filter under key [%2$s] '
                . 'is not callable.', get_class( $this ), $key )
            );
        }
        if ( array_key_exists( $key, self::$source_integrity_rules )
            && self::$source_integrity_rules[$key] === $callable )
        {
            // Already got this one.
            return;
        } elseif ( array_key_exists( $key, self::$source_integrity_rules )
            && self::$source_integrity_rules[$key] !== $callable )
        {
            // Replacement of existing filters is not allowed.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered at [%1$s]. '
                . 'Provided security filter under key [%2$s] '
                . 'has already been provided with a different callback. '
                . 'Security filters may not be replaced, but you may add '
                . 'an alternate filter under a different key.',
                get_class( $this ), $key )
            );
        }
        self::$source_integrity_rules[$key] = $callable;
    }

    /**
     * Compiles a Sass file in the current scope,
     * and returns the compiled source.
     *
     * @param string $key A Sass file key within the current scope.
     * @return string
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the Sass Adapter is currently unscoped.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given file does not exist in the current scope.
     */
    public function compile( $key )
    {
        $this->_checkSourceScope();
        $this->_checkSourceFile( $key );
        $source = $this->_getSourceContents( $key );
        // Even source saved to disk is verified for security.
        // In the event an exploit script from the command line
        // attempts to register malicious source directly into
        // the cache dir, it will still be blocked if it violates
        // any rulesets.
        $this->_validateSourceIntegrity( $source );
        return $this->compiler->compile( $source );
    }

    /**
     * Gets the raw Sass file source.
     *
     * This is typically used internally to perform edits to the source,
     * or to return user generated sass source for the editor in the admin area,
     * if it is enabled.
     *
     * This method returns only the uncompiled source.
     * This generally cannot be served directly on the frontend
     * as a valid stylesheet.
     *
     * If you want an actual stylesheet, use the `compile` method instead.
     *
     * @param string $key A Sass file key within the current scope.
     * @return string
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the Sass Adapter is currently unscoped.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given file does not exist in the current scope.
     */
    public function raw( $key )
    {
        $this->_checkSourceScope();
        $this->_checkSourceFile( $key );
        return file_get_contents( $this->_makeFilePath( $key ) );
    }

    /**
     * Returns an array of all of the files in the
     * currently scoped directory.
     *
     * @return array
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the Sass Adapter is currently unscoped.
     */
    public function index()
    {
        $this->_checkSourceScope();
        $path = $this->_makeFilePath();
        $index = array();
        $files = glob( $path . '*', GLOB_MARK );
        foreach ( $files as
            $file )
        {
            $key = rtrim( basename( $file ), DIRECTORY_SEPARATOR );
            if ( strpos( $key, $this::FILE_SUFFIX ) !== false )
            {
                $key = substr( $key, 0,
                    strrpos( $key, '.' . $this::FILE_SUFFIX ) );
            }
            $index[$key] = $file;
        }
        return $index;
    }

    /**
     * Creates or overwrites a sass stylesheet.
     *
     * @param string $key The file to create or overwrite.
     * @param string $source The raw sass source
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the Sass Adapter is currently unscoped.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the currently scoped source directory is not
     *     readable or writable.
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any source integrity filter checks return an error,
     *     indicating that a potential exploit is present.
     *     Filters may be assigned externally by any class interacting
     *     with this logic, and ALL filters must pass without raising
     *     an exception to allow any result to be saved.
     *     Any null source is not evaluated, as there is nothing to evaluate.
     *     Any non-null source is evaluated, even if it is an empty string.
     */
    public function save( $key, $source = null )
    {
        $this->_checkSourceScope();
        $this->_checkSourceReadable();
        $this->_checkSourceWritable();
        // All raw source is treated as inherently untrusted.
        // Raw source must pass all registered security filters
        // to be saved. Retrieved source also has to pass these,
        // so even source externally modified will not be parsed
        // if it breaks the rules.
        $this->_validateSourceIntegrity( $source );
        $path = $this->_makeFilePath( $key );
        $fh = fopen( $path, 'w+b' );
        fwrite( $fh, $source );
        fclose( $fh );
    }

    /**
     * Saves compiled source to the local css directory as a normalized CSS file.
     *
     * @param string $key The SASS stylesheet to parse.
     *
     * @return array Returns the path data used to conform to how
     *     the rest of the system goes about queuing stylesheets.
     *     This can be appended directly to the compiled config details
     *     to enable the source to be queued normally as if it were a
     *     declared resource that ships with the installation.
     */
    public function saveCompiled( $key )
    {
        $this->_checkSourceScope();
        $this->_checkSourceReadable( $key );
        $this->_checkDistReadable();
        $this->_checkDistWritable();
        $path = $this->_makeDistPath( $key );
        $source = $this->compile( $key );
        $fh = fopen( $path, 'w+b' );
        fwrite( $fh, $source );
        fclose( $fh );
        return $this->_makeStyleIndex( $key );
    }

    public function getCompiledIndex()
    {
        $old_scope = $this->scope;
        $compiled = array();
        foreach ( array_keys( self::$cache_paths ) as
            $scope )
        {
            $this->setTemplateScope( $scope );
            $key_base = $scope;
            $files = glob( $this->_makeDistPath() . $scope . '-*.css' );
            if ( empty( $files ) )
            {
                continue;
            }
            foreach ( $files as
                $file )
            {
                $handle = substr( basename( $file ), strlen( $scope . '-' ) );
                $handle = substr( $handle, 0, strpos( $handle, '.css' ) );
                $compiled[$scope . '-' . $handle] = $this->_makeStyleIndex( $handle );
            }
        }
        $this->scope = $old_scope;
        return $compiled;
    }

    /**
     * Deletes a sass file from the cache directory.
     *
     * @param string $key The file handle to remove
     * @return bool Returns true if the file was deleted,
     *     and false if it did not exist.
     */
    public function delete( $key )
    {
        $this->_checkSourceScope();
        try
        {
            $this->_checkSourceReadable( $key );
        } catch ( \mopsyd\sanctity\libs\exception\SanctityException $e )
        {
            return false;
        }
        unlink( $this->_makeFilePath( $key ) );
        return true;
    }

    /**
     * Performs the initial setup for the sass compiler.
     * @return void
     * @internal
     */
    private function _initializeAdapter()
    {
        $this->compiler = new \Leafo\ScssPhp\Compiler();
        if ( !self::$adapter_initialized )
        {
            self::$colors = new \Leafo\ScssPhp\Colors();
            self::$compile_dir = SANCTITY_BASEDIR
                . 'etc' . DIRECTORY_SEPARATOR
                . 'cache' . DIRECTORY_SEPARATOR
                . 'sass' . DIRECTORY_SEPARATOR
                . 'compiled' . DIRECTORY_SEPARATOR;
            self::$css_dir = SANCTITY_BASEDIR
                . 'dist' . DIRECTORY_SEPARATOR
                . 'local' . DIRECTORY_SEPARATOR;
            self::$adapter_initialized = true;
        }
    }

    /**
     * Prevents usage when not scoped to a known cache directory.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the sass compiler is not currently scoped.
     *     This means it will not know where to look for files.
     * @internal
     */
    private function _checkSourceScope()
    {
        if ( is_null( $this->scope ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Template scope is not set.',
                get_class( $this ) )
            );
        }
    }

    /**
     * Checks if the path provided in the current scope is readable.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the resolved filepath is not readable or does not exist.
     * @internal
     */
    private function _checkSourceReadable( $file = null )
    {
        $path = $this->_makeFilePath( $file );
        if ( !is_readable( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Template path at [%1$s] is not readable.',
                get_class( $this ), $path )
            );
        }
    }

    /**
     * Checks if the path provided in the current scope is writable.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the resolved filepath does not have write access or does not exist.
     * @internal
     */
    private function _checkSourceWritable( $file = null )
    {
        $path = $this->_makeFilePath( $file );
        if ( !is_writable( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Template path at [%1$s] is not readable.',
                get_class( $this ), $path )
            );
        }
    }

    /**
     * Checks if the public path for compiled CSS is readable.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the resolved filepath is not readable or does not exist.
     * @internal
     */
    private function _checkDistReadable( $file = null )
    {
        $path = $this->_makeDistPath( $file );
        if ( !is_readable( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Template path at [%1$s] is not readable.',
                get_class( $this ), $path )
            );
        }
    }

    /**
     * Checks if the public path for compiled CSS is writable.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the resolved filepath does not have write access or does not exist.
     * @internal
     */
    private function _checkDistWritable( $file = null )
    {
        $path = $this->_makeDistPath( $file );
        if ( !is_writable( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Template path at [%1$s] is not readable.',
                get_class( $this ), $path )
            );
        }
    }

    /**
     * Prevents usage when the provided file is not found.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided file is not found or not readable.
     * @internal
     */
    private function _checkSourceFile( $file )
    {
        $path = $this->_makeFilePath( $file );
        if ( !is_readable( $path ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. '
                . 'File [%2$s] is missing or not readable.', get_class( $this ),
                $path )
            );
        }
    }

    /**
     * Gets the contents of the provided file handle.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the sass compiler is not currently scoped.
     *     This means it will not know where to look for files.
     * @internal
     */
    private function _getSourceContents( $file )
    {
        $this->_makeFilePath( $file );
        return file_get_contents( $this->_makeFilePath( $file ) );
    }

    /**
     * Creates the absolute file path from the provided file,
     * applies the suffix, and applies an underscore if it is
     * within a subdirectory of the root scope path.
     *
     * @param string $file
     * @return string
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If reverse file path navigation attempts exist in the resolved string.
     *     Path declarations are only allowed within the defined directories.
     *     No reverse navigation is permitted to avoid security implications,
     *     and also to prevent any excessive complexity when nuking a
     *     cache directory and recreating it.
     * @internal
     */
    private function _makeFilePath( $file = null )
    {
        $base = $this->scope;
        $suffix = null;
        if ( !is_null( $file ) )
        {
            $filename = basename( $file );
            $suffix = substr( $file, 0, strrpos( $file, $filename ) );
            if ( strpos( $file, DIRECTORY_SEPARATOR ) !== false )
            {
                $filename = '_' . ltrim( $filename, '_' );
            }
            $filename .= '.' . $this::FILE_SUFFIX;
            $suffix = $suffix . $filename;
        }
        $path = $base . $suffix;
        $this->_checkPathHacks( $path );
        return $path;
    }

    /**
     * Creates the absolute file path for a compiled publicly servable CSS file.
     *
     * This method is only called internally, so it does not need to preflight
     * provided paths like the source mapping method does. It only uses currently
     * valid paths corresponding to the relative location of the source directory.
     *
     * @param string $file
     * @return string
     * @internal
     */
    private function _makeDistPath( $file = null )
    {
        $base = self::$css_dir;
        $suffix = null;
        if ( !is_null( $file ) )
        {
            $prefix = array_search( substr( $this->scope,
                    strlen( SANCTITY_BASEDIR ) ), self::$cache_paths );
            $filename = $prefix . '-' . basename( $file );
            $suffix = substr( $file, 0, strrpos( $file, $filename ) );
            if ( strpos( $file, $this::FILE_SUFFIX ) !== false )
            {
                $filename = str_replace( $this::FILE_SUFFIX, null, $filename );
            }
            $filename .= '.css';
            $suffix = $suffix . $filename;
        }
        $path = $base . $suffix;
        return $path;
    }

    /**
     * Blocks all attempts at reverse navigation in a file path.
     *
     * Implementing logic interacting with this class is expected to know
     * its relative source paths explicitly, and resolve them correctly
     * prior to passing them.
     *
     * No reverse navigation is permitted at any portion of the file path.
     * This prevents any attempt to use this class to save files outside of
     * the known cache directories, which already have appropriate
     * security measures in place to avoid exploitation.
     *
     * @param string $path The file path to evaluate
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     Any attempt at reverse navigation will generate an exception
     *     and block further operation. This occurs on all supplied paths
     *     in this class, so there is no way to sneak anything past this.
     * @internal
     */
    private function _checkPathHacks( $path )
    {
        $path = str_replace( $path, '\\', DIRECTORY_SEPARATOR );
        if ( strpos( $path, '..' ) !== false )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. '
                . 'Reverse path navigation is not permitted. '
                . 'Caching may only occur within the provided directories '
                . 'or their child directories for server security purposes. '
                . 'Evaluating path [%2%s].', get_class( $this ), $path )
            );
        }
    }

    /**
     * Validates source does not contain exploits.
     *
     * @param string $source
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any programmatic exploits are detected in the provided Sass souce.
     *     This method will fire whenever any raw source is provided, and will
     *     continually be updated with additional rulesets that validate against
     *     any known exploit patterns.
     *     Source will not be saved to disk or cache if any are detected.
     * @internal
     */
    private function _validateSourceIntegrity( $source )
    {
        if ( is_null( $source ) )
        {
            return;
        }
        foreach ( self::$source_integrity_rules as
            $key =>
            $filter )
        {
            try
            {
                call_user_func( $filter, $source );
            } catch ( \Exception $e )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf(
                    'Error encountered in [%1$s]. Source integrity filter '
                    . 'detected a potential exploit. Parsing has been halted. '
                    . 'Filter message: [%2$s].', get_class( $this ),
                    $e->getMessage()
                ), $e->getCode(), $e );
            }
        }
    }

    private function _makeStyleIndex( $file )
    {
        $readable_name = basename( $file );
        $dist_base = $this->_makeDistPath( $file );
        $relative_scope = array_search( substr( $this->_makeFilePath(),
                strlen( SANCTITY_BASEDIR ) ), self::$cache_paths );
        $relative_path = str_replace( SANCTITY_BASEDIR, null, $dist_base );
        $uri = $this::getAdapter()->get( 'uri', 'sanctity' ) . $relative_path;
        $details = array(
            'name' => ucwords( $relative_scope ) . ' CSS Profile: ' . ucwords( $readable_name ),
            'handle' => $relative_scope . '-' . $readable_name,
            'thirdparty' => false,
            'details' => array(
                'source' => array(
                    'full' => $relative_path,
                    'min' => false,
                    'map' => false
                ),
                'version' => $this::PACKAGE_VERSION,
                'dependencies' => array(
                    'frontend'
                ),
                'media' => 'screen'
            ),
        );
        return $details;
    }

}
