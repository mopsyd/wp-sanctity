<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\exception;

/**
 * Sanctity Platform Failure Exception
 *
 * Exception thrown by Sanctity if the underlying platform is compromised,
 * and continuing runtime may lead to permanent damage to your site,
 * the database getting wrecked, or other terrible things
 * that are really hard to fix.
 *
 * Seeing this exception means most likely one of the following occurred:
 *
 * - You recently updated, and the core developers introduced
 *     something stupid and need to fix it.
 *
 * - You are running a WAAAAAAAAAAAY outdated version and trying
 *     to forcibly use things that don't work at all on your version,
 *     and they are slowly destroying your website.
 *
 * - You are running WAAAAAAAAAAY outdated plugins/extensions/modules/what-have-you
 *     that have not been supported for ages, and they are slowly destroying your site
 *     because they are not using the current api and are causing
 *     significant damage to your database and/or filebase.
 *
 * - Your site has a virus and needs to be cleaned up.
 *     Get yourself Sucuri, Wordfence, or what have you when you
 *     get it cleaned so it doesn't happen again.
 *
 * - You hacked your core.
 *     You are bad and you should feel bad.
 *
 * - You hired a hack developer who hacked your core.
 *     He is bad and he should feel bad.
 *     Also don't ever pay that guy to do anything again ever.
 *
 * - You have installed a really crappy, badly written plugin/extension/module
 *     that you need to delete pronto.
 *
 * - You have a managed hosting account on a really poor choice of webserver hosts,
 *     and your system administrator is an idiot, or the hosting company is trying
 *     to charge you a premium for stuff that should be free,
 *     and has hacked your site on purpose to squeeze money out of you.
 *     Find better hosting.
 *
 * In the event that it is a confirmed bug with your platform (Wordpress/Magento/Joomla/etc),
 * this usually indicates that this package needs to be updated,
 * or the adapter driving it does anyhow.
 *
 * If you encounter this exception, you should post it on the
 * Sanctity bug tracker so we can update to account for whatever they broke,
 * and ALSO post a link on the bug tracker for your platform you are running
 * Sanctity on. If they try to pass the buck back to us, point out that
 * this exception is ONLY raised when the platform itself fails.
 * You can copy and past this text along with the stacktrace from
 * the exception to support your case.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @note This exception only occurs when an error can be
 *     100% isolated to a bug in Wordpress itself, or instability due to
 *     unauthorized modifications to core code that is detectable at runtime.
 */
class PlatformFailureException
    extends \LogicException
    implements \mopsyd\sanctity\interfaces\SanctityExceptionInterface
{

    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;

}
