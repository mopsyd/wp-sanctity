<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\exception;

/**
 * Sanctity Doing It Wrong Exception
 * Exception thrown by Sanctity if its api is misused
 * by an extending 3rd party author.
 *
 * This is a play on the Wordpress internal "_doing_it_wrong",
 * which fires when plugins don't behave.
 *
 * Likewise, this exception is thrown when an extension registered with
 * Sanctity doesn't behave. It is reserved explicitly for handling of extensions,
 * and does not otherwise occur.
 *
 * This exception will halt whatever is going on immediately with no further
 * execution any time it is encountered. Sanctity does not try to guess how
 * to work around broken code, it just turns it off permanently until it works.
 * You can run broken extensions without them being automatically disabled only
 * if you have debug mode enabled, which is required for fixing them.
 * If it has a clean run in debug mode, it will no longer be disabled
 * when debug mode is turned back off again.
 *
 * If you are a developer and you encounter this exception
 * at any time in your own logic, fix your code. If you are encountering this
 * exception for any reason, your code is not production ready, and should not
 * be sold, pushed live, or released to a stable branch until corrected.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class DoingItWrongException
    extends \SanctityBrokenClassException
{
    
}
