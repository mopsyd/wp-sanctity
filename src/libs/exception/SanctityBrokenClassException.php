<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\exception;

/**
 * Sanctity Broken Class Exception
 * Exception thrown by Sanctity if its api is misused by an extending author.
 *
 * Basically this means a 3rd party working with this code derped, not
 * the author of the theme, and also not the Wordpress team. Do not blame
 * either of them in this case.
 *
 * If you encounter this exception and you are not a developer,
 * you need to enter a bug ticket with whoever wrote the extension of Sanctity,
 * not the author of Sanctity. This exception is only thrown when a bug is
 * present, and should never be encountered under any other circumstances.
 * If the developer tells you it is not a bug, they are wrong.
 *
 * If you are a developer and you encounter this exception
 * at any time in your own logic, fix your code. If you are encountering this
 * exception for any reason, your code is not production ready, and should not
 * be sold, pushed live, or released to a stable branch until corrected.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
class SanctityBrokenClassException
    extends \RuntimeException
    implements \mopsyd\sanctity\interfaces\SanctityExceptionInterface
{

    /**
     * To keep viable over time, one must learn to adapt.
     *
     * This provides compatibility with the platform adapter,
     * which does all of the things related to the system this
     * codebase is running on top of, so that the internals of this
     * system remain portable across multiple systems.
     *
     * DRY and all that.
     */
    use \mopsyd\sanctity\traits\AdapterCompatibility;
}
