<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\sanitizer;

/**
 * Sanitizer
 *
 * This class does abstract routing of sanitization methods.
 *
 * All sanitization can simply point at this single class
 * and resolve against a custom sanitizer callback,
 * or against a default if no specific sanitizer
 * is registered for a given input.
 *
 * This allows for very speedy prototyping of additional options
 * and user inputs with maximum code reuse.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class Sanitizer
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\SanctityInterface
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    private static $sanitizers;

    /**
     * For auto-initialization, in case it gets called prior
     * to manually calling it for some purpose.
     * @var type
     */
    private static $sanitizer_initialized = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initializeSanitizer();
        parent::__construct();
        self::$sanitizer_initialized = true;
    }

    /**
     * Lets you throw sanitizer calls at the class dynamically,
     * without the need to write a custom function every time.
     *
     * It will call the sanitizer keyed to the name of the method
     * in the first parameter if it exists, and fall back to default
     * definitions by the value of `gettype` if it does not detect a
     * specific method keyed to the method provided. If it cannot
     * find a suitable method in its registered set, defaults,
     * or by querying a controller or model, it will null the
     * result and return that.
     *
     * @param string $name The name of the "method" called.
     *     This triggers a dynamic lookup internally.
     * @param array $arguments An array containing exactly one key value pair.
     * @return mixed This method will always return something, but if it doesn't
     *     find what you asked it for it is going to return a nulled version of
     *     whatever you gave it (empty array, empty string, false if a boolean, etc).
     */
    public static function __callStatic( $name, $arguments )
    {
        self::_autoInit();
        if ( array_key_exists( $name, self::$sanitizers ) )
        {
            $method = self::$sanitizers[$name];
            return call_user_func_array( $method, $arguments );
        }
        $class = get_called_class();
        return self::_getNulledResult( gettype( $arguments ) );
    }

    public static function listSanitizers()
    {
        return array_keys( self::$sanitizers );
    }

    public function get( $key )
    {
        if ( array_key_exists( $key, self::$sanitizers ) )
        {
            return self::$sanitizers[$key];
        }
        return false;
    }

    public function set( $key, $value )
    {
        if ( is_callable( $value ) )
        {
            self::$sanitizers[$key] = $value;
            return;
        }
        throw new \mopsyd\sanctity\libs\exception\SanctityException(
        sprintf( 'Invalid callable passed to [%1$s]. '
            . 'Cannot use this method as a sanitizer.', get_class( $this ) ) );
    }

    protected static function sanitizeColor( $color )
    {
        if ( is_null( $color ) || $color === '' )
        {
            return null;
        }
        return $color;
    }

    protected static function sanitizeSocial( $social )
    {
        $result = false;
        if ( is_array( $social ) && array_key_exists( 'handle', $social ) )
        {
            $result = array(
                'handle' => $social['handle'],
                'icon' => ( array_key_exists( 'icon', $social ) )
                ? $social['icon']
                : null,
                'endpoint' => ( array_key_exists( 'endpoint', $social ) && is_string( $social['endpoint'] ) )
                ? $social['endpoint']
                : null,
                'enabled' => ( array_key_exists( 'enabled', $social )
                && $social['enabled']
                && ( array_key_exists( 'endpoint', $social )
                    && is_string( $social['endpoint'] )
                    && $social['enabled'] ) )
                ?: false,
                'valid' => ( array_key_exists( 'valid', $social )
                && $social['valid']
                && ( array_key_exists( 'endpoint', $social )
                    && is_string( $social['endpoint'] )
                    && $social['valid'] ) )
                ?: false,
                'deeplink' => false,        // Deeplinking is for app integration, and is not yet implemented.
                'deeplink-uri' => null,     // Deeplinking is for app integration, and is not yet implemented.
                'autoverify' => ( array_key_exists( 'autoverify', $social )
                && $social['autoverify'] ), // Determines if automatic verification of the profile is possible.
            );
        }
        return $result;
    }

    private function _initializeSanitizer()
    {
        if ( is_null( self::$sanitizers ) )
        {
            self::$sanitizers = $this->_loadDefaultSanitizers();
        }
    }

    /**
     * The default Wordpress sanitizers are always available.
     *
     * @return array
     * @internal
     */
    private function _loadDefaultSanitizers()
    {
        return array(
            'color' => get_called_class() . '::sanitizeColor',
            'social' => get_called_class() . '::sanitizeSocial',
            'email' => 'sanitize_email',
            'file' => 'sanitize_file_name',
            'html' => 'sanitize_html_class',
            'key' => 'sanitize_key',
            'meta' => 'sanitize_meta',
            'mime' => 'sanitize_mime',
//            'option' => 'sanitize_option',                    //Needs work for multiple parameters
            'orderby' => 'sanitize_sql_orderby',
            'text' => 'sanitize_text_field',
            'textarea' => 'sanitize_textarea_field',
            'title' => 'sanitize_title',
            'title_query' => 'sanitize_title_for_query',
            'title_dashes' => 'sanitize_title_with_dashes',
            'user' => 'sanitize_user',
        );
    }

    private static function _getNulledResult( $type )
    {
        switch ( $type )
        {
            case 'string':
                return '';
            case 'boolean':
                return false;
            case 'array':
                return array();
            case 'object':
                return new \stdClass();
            case 'integer':
                return 0;
            case 'float':
                return 0.0;
            case 'resource':
                //This opens a read-only memory stream to an empty memory wrapper.
                //It resolves the resource type, but is completely unusable,
                //and will not yeild anything if printed.
                return fopen( "php://memory", "r" );
            default:
                return null;
                break;
        }
    }

    /**
     * Forces the object to instantiate and fire its initialization sequence
     * if it has not fired at least one time.
     *
     * This method is called at the beginning of all public static methods,
     * to insure proper instantiation occurs and the internals are set
     * correctly prior to use.
     *
     * @return void
     * @internal
     */
    private static function _autoInit()
    {
        if ( !self::$sanitizer_initialized )
        {
            self::init();
        }
    }

}
