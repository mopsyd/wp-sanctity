<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\chain;

/**
 * Abstract Work Chain
 *
 * Provides the foundational abstraction for effective implementation of the
 * work chain logic, which allows adapters to be used as chainable constructs
 * using methods that reflect natural language.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class AbstractWorkChain
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\logic\WorkChainInterface
{

    /**
     * The Adapter currently scoped to the work chain.
     *
     * @var \mopsyd\sanctity\interfaces\adapter\AdapterInterface
     */
    private $adapter;

    /**
     * Prior subjects, which are retained in the event that a previous value has
     * to be used to fulfill some conditional or control block logic in the queue.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private $subject_queue;

    /**
     * The key currently pointed to by the work queue
     * @var int|string
     */
    private $subject_key;

    /**
     * The current subject pointer. This is the value that automatically
     * gets handed off to the next method called.
     * @var int
     */
    private $subject_pointer;

    /**
     * The previous subject pointer. This is used to facilitate the
     * chain working ahead ofits current index for control schemas.
     * @var int
     */
    private $previous_pointer;

    /**
     * Represents a set of conditional blocks that apply altered status
     * to the subject pointer when they occur.
     * @var array
     */
    private $control_blocks = array();

    /**
     * Represents a set of triggers for loop control, which determines how
     * iteration should occur over the rest of the work chain.
     * @var array
     */
    private $loop_blocks = array();

    /**
     * Represents whether or not results should be packaged
     * into the provided package container.
     *
     * @var bool
     */
    private $use_package = false;

    /**
     * Represents a package injected into the work chain to be populated with results.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     */
    private $package;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
        $this->_reset();
    }

    // -------------------------------------------------------------------------
    //                      Scoping Methods
    //
    // These methods are chainable, and represent a change in the adapter that
    // the chain is operating against. The adapter may be swapped out at
    // various points within the chain to produce alternate results.
    // Subsequent methods in the chain must adhere to the api of the adapter
    // that is currently scoped.
    //
    // -------------------------------------------------------------------------

    /**
     * Scoping method.
     * This method is used to scope the work chain to an adapter,
     * which will then be used to operate against internally as
     * the subject that the chain references to enact further methodology.
     *
     * @param \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface $adapter
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function scope( \mopsyd\sanctity\interfaces\adapters\SanctityAdapterInterface $adapter )
    {
        $this->adapter = $adapter;
        return $this;
    }

    // -------------------------------------------------------------------------
    //                      Control Methods
    //
    // These methods are chainable, and represent conditional logic within
    // the work chain. These are used to provide control blocks for how the
    // chain operates. These do not alter chain operation or prevent the chain
    // from running (except the `when` method, which can halt the chain entirely),
    // but rather scope the subject pointer when various events or conditions
    // are met.
    //
    // -------------------------------------------------------------------------

    /**
     * Control Method.
     * Means "Monitor the work chain results, and when the result of any
     * operation equals the condition, run this method and set the
     * subject pointer to the result."
     *
     * This is essentially an `if` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function on( $condition, $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    /**
     * Control Method.
     * This must immediately follow an `on` call.
     *
     * Means "Monitor the work chain results, and if the previous `on` method
     * is not equal but this is equal to the result when the result of any
     * operation equals the condition, run this method and set the
     * subject pointer to the result."
     *
     * This is essentially an `elseif` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function elseOn( $condition, $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    /**
     * Control Method.
     * This must immediately follow an `on` call or an `elseOn` call.
     *
     * Means "Monitor the work chain results, and if the previous `on` or `elseOn` method
     * is not equal to the result, run this method and set the subject pointer to the result."
     *
     * This is essentially an `elseif` statement that continually monitors the chain.
     * It does not update the subject pointer until it fires.
     *
     * @param type $condition
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function otherwise( $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    /**
     * Control Method.
     * This means "Run this Work Chain break method when this condition equals
     * the current subject pointer, and do not proceed with the rest of the chain".
     *
     * For example, you might say that when you get a string,
     * call `render` and stop the chain. You might also say when you get a callable,
     * call `execute` and stop the chain.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @param string $command A break method that exists in the work chain,
     *     not the scoped adapter. This method does not reference the adapter,
     *     it references `self`. Only break methods may be called via this logic
     *     to keep usage at least somewhat sane.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function when( $condition, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    // -------------------------------------------------------------------------
    //                      Looping Methods
    //
    // These methods are chainable, and designate how iteration occurs for the
    // rest of the chain beyond the point they are declared. ALL remaining methods
    // in the chain will be called on each iteration, so this should be used
    // with care.
    //
    // -------------------------------------------------------------------------

    /**
     * Looping Method.
     * Means "Iterate over the subject pointer with this adapter command,
     * and run the rest of the chain up until a break method is encountered
     * on each result".
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function each( $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    /**
     * Looping Method.
     * This means "Continue the next operation until the result
     * equals the provided condition, and run the rest of the chain
     * up until a break method is encountered on each result".
     *
     * Essentially this means run a `while` loop on the next thing done with
     * the provided argument as its break condition.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @param int $limit This is a failsafe breakpoint for iteration.
     *     If it is any value greater than zero, iteration will be halted
     *     if it proceeds beyond that number of iterations to prevent
     *     infinite recursive loop bugs.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function until( $condition, $limit = -1 )
    {
        $this->_validateAdapter();
        // Not yet implemented
        return $this;
    }

    /**
     * Looping Method.
     * This may be called immediately after `until` to add additional
     * conditions that will break the `while` loop.
     *
     * This may be called any number of times immediately after an `until` call,
     * and will continue to add additional break conditions without starting the loop.
     *
     * @param mixed $condition A condition to monitor for.
     *     May be a callable, interface, class, php variable type or pseudotype,
     *     or literal value. Literal values must not be equal to a valid class name,
     *     interface name, callable name, or php variable type or pseudotype,
     *     or else it will resolve against those, and you will not get the
     *     result you want.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function orUntil( $condition )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        // Not yet implemented
        return $this;
    }

    // -------------------------------------------------------------------------
    //                      Binding Methods
    //
    // These methods are chainable, and designate that an underlying
    // adapter method should be called at this point in the work chain.
    // These are the typical execution methods that fire underlying logic
    // that the work chain is wrapping.
    //
    // -------------------------------------------------------------------------

    /**
     * Binding Method.
     * Means "Use the result of the last method as the argument for this one".
     *
     * This is the standard chainable binding,
     * and represents the most common usage.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function to( $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        $result = $this->adapter->$method( $command,
            $this->subject_queue->get( $this->subject_pointer ) );
        $this->_updateSubjectPointer( $result );
        $this->_updatePreviousSubjectPointer();
        return $this;
    }

    /**
     * Binding Method.
     * Scopes the chain to the provided argument without obtaining
     * any result from the adapter, and without regard to the prior
     * subject pointer.
     *
     * @param type $args The subject to set the current subject pointer to.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function from( $args )
    {
        $this->_validateAdapter();
        $this->_updateSubjectPointer( $args );
        $this->_updatePreviousSubjectPointer();
        return $this;
    }

    /**
     * Binding Method.
     * Chains a method with arbitrary arguments,
     * and sets the result as the subject pointer.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @paran mixed $args The subject to provide to the method being called.
     */
    public function with( $method, $command, $args )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        $result = $this->adapter->$method( $command, $args );
        $this->_updateSubjectPointer( $result );
        $this->_updatePreviousSubjectPointer();
        return $this;
    }

    /**
     * Binding method.
     * Packages the results of the operation into the provided container
     * instead of scoping the subject pointer to it.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @param \mopsyd\sanctity\interfaces\libs\container\ContainerInterface $container
     *     A container object to package the result of the operation into.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function into( $method, $command,
        \mopsyd\sanctity\interfaces\libs\container\ContainerInterface &$container )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        $result = $this->adapter->$method( $command, $args );
        $container[] = $result;
        return $this;
    }

    /**
     * Binding method.
     * This method means "Use the last parameter for this also",
     * and will use the same subject pointer that the prior
     * method in the chain did.
     *
     * This method does not update the prior pointer tracker, so it can be called
     * multiple times and consistently operate on the same result until another
     * method that does update the prior tracker is called at some point in the chain.
     *
     * @param string $method The method to call on the scoped adapter.
     *     Must be a method that or callable that the scoped adapter
     *     can directly execute natively.
     * @param string $command The command for the adapter,
     *     which corresponds to the first argument of the adapter.
     *     This is used by the adapter to select the correct worker
     *     to delegate the operation to. This must ba valid for
     *     the provided method parameter.
     * @return \mopsyd\sanctity\interfaces\logic\WorkChainInterface
     */
    public function also( $method, $command )
    {
        $this->_validateAdapter();
        $this->_validateAdapterMethod( $method );
        $result = $this->adapter->$method( $command,
            $this->subject_queue->get( $this->previous_pointer ) );
        $this->_updateSubjectPointer( $result );
        return $this;
    }

    // -------------------------------------------------------------------------
    //                      Break Methods
    //
    // These methods are ALWAYS non chainable within the scope of the work chain.
    // They are used to finalize a chain statement, representing the end of
    // a logical expression. Most allow you to pick up where you left off
    // and continue, with the exception of the `reset` method, which clears all
    // internals of the chain back to a completely fresh state.
    //
    // -------------------------------------------------------------------------

    /**
     * Break method that discards results entirely,
     * and resets the chain back to a clean state.
     *
     * This method MUST be non-blocking.
     *
     * This method returns nothing, and calling it
     * MUST NOT be chainable to another method.
     *
     * @return void
     */
    public function reset()
    {
        $this->_reset();
    }

    /**
     * Break method that prints the result from the prior operation.
     * Returns true if printing is possible and occurred successfully,
     * returns false otherwise. Attempts to print non printable parameters
     * result in an exception.
     *
     * This method MUST return a boolean value, and MUST NOT
     * be chainable to another method when called.
     *
     * This method MUST NOT reset the chain.
     *
     * @return bool
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function render()
    {
        $this->validateRender();
        echo $this->subject_queue->get( $this->subject_pointer );
        return true;
    }

    /**
     * Break method that executes the result from the prior operation.
     * Returns true if execution resolved, and false otherwise.
     * Attempts to execute a non-executable parameter results in an exception.
     *
     * This method MUST return the result of executing the current subject pointer.
     *
     * This method MUST NOT reset the chain.
     *
     * @return bool
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     */
    public function execute()
    {
        $this->validateExecution();
        $obj = $this->subject_queue->get( $this->subject_pointer );
        return $obj();
    }

    /**
     * Break method that returns the result obtained from the prior operation.
     *
     * This method may ONLY be chainable if the resulting parameter
     * is an object that provides its own chainable methods.
     * Any other value MUST NOT be chainable.
     *
     * This method MUST be non-blocking.
     *
     * This method MUST NOT reset the chain.
     *
     * @return mixed
     */
    public function result()
    {
        return $this->subject_queue->get( $this->subject_pointer );
    }

    /**
     * Break method that returns all of the results from the entire chain since
     * its creation or last reset, whichever is more recent.
     *
     * This method is not chainable.
     *
     * This method MUST be non-blocking.
     *
     * This method MUST NOT reset the chain.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     *     The container will have enumerated keys that list the order of
     *     operations of the chain in the order they were entered into the queue.
     *     This will typically return a generic container, which should be cast into
     *     the type you want, or used as a pool to extract and further distribute
     *     values from. Calling this method does not cast to any specific
     *     special container type.
     */
    public function getAll()
    {
        return $this->subject_queue;
    }

    /**
     * Validates whether a render operation on the
     * current subject pointer is possible.
     * @return void
     * @throws \mopsyd\libs\exception\SanctityException
     *     If the current subject pointer is not a renderable result
     */
    protected function validateRender()
    {
        if ( !is_scalar( $this->subject_queue->get( $this->subject_pointer ) )
            && !is_null( $this->subject_queue->get( $this->subject_pointer ) )
            && !( is_object( $this->subject_queue->get( $this->subject_pointer )
                && method_exists( $this->subject_queue->get( $this->subject_pointer ),
                    '__toString' ) ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Current subject pointer '
                . 'is not a printable value', get_class( $this ) )
            );
        }
    }

    /**
     * Validates whether a execution operation on the
     * current subject pointer is possible.
     * @return void
     * @throws \mopsyd\libs\exception\SanctityException
     *      If the current subject pointer is not an executable or callable result
     */
    protected function validateExecution()
    {
        if ( !is_callable( $this->subject_queue->get( $this->subject_pointer ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Current subject pointer '
                . 'is not a callable subject.', get_class( $this ) )
            );
        }
    }

    /**
     * Updates the current subject pointer, or packages the
     * current package, if packaging is enabled.
     * @param mixed $value
     * @return void
     * @internal
     */
    private function _updateSubjectPointer( $value )
    {
        if ( $this->use_package )
        {
            $this->package[] = $value;
        } else
        {
            $this->subject_key = count( $this->subject_queue );
            $this->subject_queue[$this->subject_key] = $value;
            $this->subject_pointer = $this->subject_key;
        }
    }

    /**
     * Updates the previous subject pointer.
     * This is tracked separately,
     * so that multiple operations against
     * a single value can be chained with `also`
     */
    private function _updatePreviousSubjectPointer()
    {
        $this->previous_key = $this->subject_key - 1;
    }

    /**
     * Validates that a work chain method that requires a
     * currently scoped adapter can be called.
     *
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If there is no currently scoped adapter, then the request
     *     cannot proceed on most work chain methods.
     * @internal
     */
    private function _validateAdapter()
    {
        if ( is_null( $this->adapter ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. The current work chain '
                . 'cannot proceed because it is not scoped to an adapter.',
                get_class( $this ) )
            );
        }
    }

    /**
     * Verifies that the provided method can actually be called against
     * the existing adapter, which circumvents errors prone to being
     * encountered through advanced abstraction.
     *
     * @param string $method
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the provided method does not exist or is not callable
     *     in the scope of the currently held adapter.
     * @internal
     */
    private function _validateAdapterMethod( $method )
    {
        if ( !is_callable( array(
                $this->adapter,
                $method ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered in [%1$s]. Provided method [%2$s] '
                . 'is not callable in the scope of [%3$s].', get_class( $this ),
                $method, get_class( $this->adapter )
            )
            );
        }
    }

    /**
     * Resets the work queue to its default status.
     * @return void
     * @internal
     */
    private function _reset()
    {
        $this->adapter = null;
        $this->subject_key = null;
        $this->subject_queue = $this::containerize( array() );
        $this->loop_blocks = array();
        $this->control_blocks = array();
        $this->package = null;
        $this->use_package = false;
        $this->subject_key = 0;
        $this->subject_pointer = 0;
        $this->previous_pointer = 0;
    }

}
