<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\config;

/**
 * Environment Config
 *
 * This is the class that stores all of the
 * environment configuration data about the current site execution.
 * This information has no setter. It is packaged by the abstract config,
 * and represents a snapshot of the current runtime environment.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class EnvironmentConfig
    extends AbstractConfig
{

    private static $config_initialized = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
        $this->_initCoreConfig();
    }

    private function _initEnvironmentConfig()
    {
        if ( !self::$config_initialized )
        {
            self::$config_initialized = true;
        }
    }

}
