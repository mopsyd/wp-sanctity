<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\config;

/**
 * Abstract Extension
 * Provides an abstract basis for extension on the Sanctity MVC system.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
abstract class AbstractConfig
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\config\ConfigInterface
{

    /**
     * The abstract config does not provide a config file,
     * because it fetches all of the configs.
     */
    const CONFIG_FILE = false;

    /**
     * This constant represents a key in the master set of data,
     * if it is not false. If provided, the underlying abstraction of this
     * class will automatically populate the local container with its data
     * when the object instantiates.
     */
    const CONFIG_KEY = false;

    /**
     * Declares the base configuration container used by child classes
     * to build their configuration set if they add details outside the
     * json scope that are not defined by the parent class.
     *
     * This does generally does not need to be overridden in this class tree,
     * though there are a few exceptions, such as the context.
     */
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\ConfigurationContainer';

    /**
     * Represents the class used to package the containers into as a multi-part collection.
     */
    const COLLECTION_CLASS = 'container\\ConfigurationCollection';

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * This gets set to true after the first initialization,
     * which designates to subsequent object instantiations
     * to skip the process.
     *
     * @var bool
     */
    private static $config_initialized = false;

    /**
     * This is the master collection, which contains all containers of configurations.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ConfigurationCollectionInterface
     */
    private static $collection;

    /**
     * The declared theme prefix, which does not necessarily correspond to the
     * folder name that the system uses to generate the slug.
     *
     * @var string
     */
    private static $prefix;

    /**
     * The slug of the child theme.
     *
     * The options table sets this based on the folder name.
     *
     * @var string
     */
    private static $slug;

    /**
     * The root slug for the parent theme.
     *
     * The options table stes this based on the folder name of the parent.
     *
     * @var string
     */
    private static $root_slug;

    /**
     * Represents the locally scoped configuration.
     *
     * @var \mopsyd\sanctity\libs\container\ConfigurationContainer
     */
    private $local_data;

    /**
     *
     * @param type $dependencies
     * @param type $args
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the config class has not properly set its configuration key constant,
     *     it will not be allowed to instantiate.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
        $this->_initCollection();
        $this->_initConfig();
        $this->_loadLocalized();
    }

    /**
     * Fetches the declared configuration from the database or config file,
     * and returns it.
     *
     * Returns false on key not found, otherwise returns the key.
     *
     * @return bool|\mopsyd\sanctity\libs\container\ConfigurationContainer
     */
    public function get( $key = null )
    {
        if ( !$this::CONFIG_KEY )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured.'
                . 'Expected declaration of class constant [%2$s] is not set in this class. '
                . 'The default value must be overridden to fetch data from the master config collection.',
                get_class( $this ), 'CONFIG_KEY' )
            );
        }
        if ( is_null( $key ) )
        {
            return $this->local_data;
        }
        if ( !$this->local_data->has( $key ) )
        {
            return false;
        }
        return $this->local_data->get( $key );
    }

    /**
     * Internal setter for updated configuration values. On set operations,
     * the set data will be immediately available across all instances
     * of the configuration.
     *
     * @param int|string $key
     * @param mixed $value
     */
    protected function set( $key, $value )
    {
        $this->local_data[$key] = $value;
        $this->package( $this->local_data );
    }

    protected function fetch()
    {
        if ( !$this::CONFIG_KEY )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured.'
                . 'Expected declaration of class constant [%2$s] is not set in this class. '
                . 'The default value must be overridden to fetch data from the master config collection.',
                get_class( $this ), 'CONFIG_KEY' )
            );
        }
        $this->local_data = self::$collection->get( $this::CONFIG_KEY );
    }

    protected function package( $data )
    {
        if ( !$this::CONFIG_KEY )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured.'
                . 'Expected declaration of class constant [%2$s] is not set in this class. '
                . 'The default value must be overridden to add data to the master config collection.',
                get_class( $this ), 'CONFIG_KEY' )
            );
        }
        if ( !( is_object( $data ) && ( $data instanceof \mopsyd\sanctity\interfaces\libs\container\ConfigurationContainerInterface ) ) )
        {
            self::$collection[$this::CONFIG_KEY] = self::containerize( $data );
        }
        self::$collection[$this::CONFIG_KEY] = $data;
        $this->local_data = $data;
    }

    /**
     * Initializes the master container, if it has not already been initialized.
     *
     * This operation runs once per runtime and does not repeat.
     *
     * @return void
     * @internal
     */
    private function _initCollection()
    {
        if ( is_null( self::$collection ) )
        {
            self::$collection = $this->load( 'library',
                $this::COLLECTION_CLASS );
        }
    }

    /**
     * Initializes the baseline information, if it has not already been initialized.
     *
     * This operation runs once per runtime and does not repeat.
     *
     * @return void
     * @internal
     */
    private function _initConfig()
    {
        if ( !self::$config_initialized )
        {
            $container_class = $this::CONTAINER_CLASS;
            $plugins = array();
            foreach ( get_option( 'active_plugins' ) as
                $plugin )
            {
                $plugins[substr( $plugin, 0,
                        strpos( $plugin, DIRECTORY_SEPARATOR ) )] = true;
            }
            self::$collection['plugins'] = self::containerizeInto( $container_class,
                    $plugins );
            $parent = get_option( 'template' );
            $child = get_option( 'stylesheet' );
            $parent_data = $this->_initRootThemeData( $parent );
            $child_data = $this->_initChildThemeData( $child, $parent );
            self::$prefix = $this->getThemePrefix();
            self::$slug = $child;
            self::$collection['theme_mods'] = self::containerizeInto( $container_class,
                    ($child_data
                    ? array_replace_recursive( $parent_data, $child_data )
                    : $parent_data ) );
            $this->_buildConfigs( $parent, $child );
        }
    }

    private function _buildConfigs( $parent, $child )
    {
        $data = $this->_initializeConfigs( $parent );
        if ( $parent !== $child )
        {
            $child_data = $this->_initializeConfigs( $child );
            foreach ( $child_data as
                $key =>
                $dataset )
            {
                if ( array_key_exists( $key, $data ) )
                {
                    $data[$key] = array_replace_recursive( $data[$key], $dataset );
                }
            }
        }
        foreach ( $data as
            $key =>
            $value )
        {
            self::$collection[$key] = self::containerize( $value );
        }
    }

    private function _initializeConfigs( $dir )
    {
        $configs = array();
        $config_files = glob( get_theme_root() . DIRECTORY_SEPARATOR . $dir . '/config/*.json' );
        foreach ( $config_files as
            $file )
        {
            $key = substr( basename( $file ), 0,
                strpos( basename( $file ), '.json' ) );
            $data = json_decode( file_get_contents( $file ), 1 );
            $configs[$key] = $data;
        }
        return $configs;
    }

    private function _initRootThemeData( $slug )
    {
        $data = get_option( 'theme_mods_' . $slug );
        return $data;
    }

    private function _initChildThemeData( $slug, $parent )
    {
        if ( $slug === $parent )
        {
            return false;
        }
        $data = get_option( 'theme_mods_' . $slug );
        return $data;
    }

    private function _loadLocalized()
    {
        try
        {
            $this->fetch();
        } catch ( \Psr\Container\NotFoundExceptionInterface $e )
        {
            $this->local_data = self::containerize( array() );
        }
    }

}
