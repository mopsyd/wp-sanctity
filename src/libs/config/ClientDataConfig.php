<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\config;

/**
 * Client Data Config
 *
 * This is the class that stores all of the
 * configuration data for data that needs to be fed to the frontend
 * with the page for client side script consumption.
 *
 * The data in this container will be immediately available to scripts without
 * the need for ajax callbacks. Scripts are expected to provide a unique
 * identifying slug on the frontend that corresponds to the key they
 * registered on the backend with, and have that available in their "data_key"
 * property on the frontend javascript object, which allows them to retrieve
 * this data from the Sanctity frontend controller. Data on the frontend is
 * only provided to a single object prototype, at which point additional
 * javascript objects cannot obtain the data without an identical prototype
 * to the original claimant. This insures that data is only distributed to
 * scripts that should receive it.
 *
 * Attempts to claim data not matching the correct object on the frontend will
 * result in a TypeError in the javascript code, which blocks access to
 * the data set.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class ClientDataConfig
    extends AbstractConfig
{

    const CONFIG_KEY = 'client-data';

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
    }

    /**
     * Enables setter operations for this config, as it is not saved
     * anywhere and is handled only during runtime.
     * @param string $key
     * @param mixed $context The context must be json encodable to be read client side.
     *     Containers, arrays, null, and scalar values all work fine.
     */
    public function set( $key, $context )
    {
        parent::set( $key, $context );
    }

    /**
     * Proxies getter operations to the parent method.
     * @param type $key
     * @return mixed
     */
    public function get( $key = null )
    {
        return parent::get( $key );
    }
}
