<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\config;

/**
 * Context Config
 *
 * This is the class that stores all of the
 * runtime contextual configuration meant to feed into the template.
 * This config does not originate from a source, and must be fully
 * packaged by the controller prior to the view being able to do
 * anything with it.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 * @final
 */
final class ContextConfig
    extends AbstractConfig
    implements \mopsyd\sanctity\interfaces\libs\config\ContextConfigInterface
{

    /**
     * Declares the base contextual collection container,
     * so that universal distribution of the context can occur.
     *
     * This does not need to be overridden in this class tree.
     */
    const CONTAINER_CLASS = 'mopsyd\\sanctity\\libs\\container\\ContextContainer';
    const CONFIG_KEY = 'context';

    private static $context_initialized = false;

    public function __construct( $dependencies = null, $args = array() )
    {
        parent::__construct( $dependencies, $args );
        $this->_initializeContext();
    }

    public function set( $key, $context )
    {
        $expected = 'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface';
        if ( is_object( $context ) && ( $context instanceof $expected ) )
        {
            $context = $context->toArray();
        }
        if ( !is_array( $context ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Set error encountered in [%1$s]. '
                . 'Supplied context must be an array or '
                . 'an instance of [%2$s].', get_class( $this ), $expected )
            );
        }
        parent::set( $key, $this::containerize( $context ) );
    }

    public function delete( $key )
    {
        $context = $this->get();
        if ( $context->has( $key ) )
        {
            unset( $context[$key] );
            $this->package( $context );
            return true;
        }
        return false;
    }

    public function get( $key = null )
    {
        return parent::get( $key );
    }

    /**
     * Swaps the underlying generalized container with the dedicated
     * context collection, which enforces that context setters throughout
     * the system provide a dataset with contextual integrity.
     *
     * This operation only happens one time when the context config
     * is first initialized.
     *
     * @return void
     * @internal
     */
    private function _initializeContext()
    {
        if ( !self::$context_initialized )
        {
            $container = $this->load( 'library', 'container\\ContextCollection' );
            $this->package( $container );
            self::$context_initialized = true;
        }
    }

}
