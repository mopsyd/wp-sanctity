<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\component;

/**
 * Abstract Component
 *
 * Provides abstraction for representing sections of
 * page content as an isolated component.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractComponent
    extends \mopsyd\sanctity\AbstractBase
    implements \mopsyd\sanctity\interfaces\libs\component\ComponentInterface
{

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Extend all of the things.
     */
    use \mopsyd\sanctity\traits\ExtensionLoader;

    /**
     * Render all of the things.
     */
    use \mopsyd\sanctity\traits\RenderAdapterCompatibility;

    /**
     * Represents whether the baseline component initialization has occurred.
     * @var bool
     */
    private static $components_initialized = false;

    /**
     * Represents any subcomponents of the component, if any exist.
     *
     * @var \mopsyd\sanctity\interfaces\libs\container\ComponentCollectionInterface
     */
    private $subcomponents;

    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initializeComponents();
        parent::__construct( $dependencies, $args );
    }

    /**
     * Runs the baseline initialization for components.
     * @return void
     * @internal
     */
    private function _initializeComponent()
    {
        if ( !self::$components_initialized )
        {
            self::$components_initialized = true;
        }
    }

}
