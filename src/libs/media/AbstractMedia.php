<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\media;

/**
 * Abstract Media Object
 *
 * Base abstract for registration of themestuffs
 * the right way through the wordpress api (eg: scripts, stylesheets, fonts, etc).
 *
 * This class provides underlying abstraction to treat Wordpress media assets
 * in an identical manner by models, controllers, and views.
 *
 * Instances of this logic should be considered to be an injectable container
 * that can fully resolve the operations with Wordpress required to operate
 * against the underlying resources they reflect, and return an accurate
 * representation of its details to the calling class in a uniform way,
 * without violating Wordpress best practices internally.
 *
 * Response data should be both standardized to a format that the
 * theme logic understands, and also be properly sanitized and escaped
 * in accordance with Wordpress expectations.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractMedia
    extends \mopsyd\sanctity\AbstractBase
{

    /**
     * Override this constant to declare a media type.
     */
    const MEDIA_TYPE = false;

    /**
     * Override this constant to declare a media suffix (eg: js, css, png, svg, etc)
     */
    const MEDIA_SUFFIX = false;

    /**
     * This points to the default media source config file for this theme.
     * Child themes may override this, but there is no need for internals
     * of this theme to do so scoped from this class onward.
     */
    const CONFIG_FILE = 'sources.json';

    /**
     * Represents a single dependency, array of dependencies,
     * or container of dependencies, or null if none exist.
     *
     * Specific implementation is left to the child class.
     * This may not be needed in the majority of cases
     * as far as media objects go, although it might
     * represent an instance of WP_Query or something
     * for attachments.
     *
     * @var mixed
     */
    private $dependencies;

    /**
     * Represents the filtered and sanitized details of a correctly
     * formatted set of parameters. Validation and sanitization are enforced
     * prior to setting this value.
     *
     * @var array
     */
    private $args = array();

    /**
     * Represents the uri path to the web accessible resource.
     * Validation and sanitization are enforced prior to setting this value.
     * @var string
     */
    private $uri;

    /**
     * Represents the identifying handle of the resource, typically an ID or slug.
     * Validation and sanitization are enforced prior to setting this value.
     * @var string
     */
    private $handle;

    /**
     * One Factory To Rule Them All!
     */
    use \mopsyd\sanctity\traits\LoaderUtility;

    /**
     * Media object constructor.
     *
     * @param mixed $dependencies (optional) Default is optional.
     *     Child classes may require dependencies based on their own criteria.
     *     All dependencies (or lack thereof) will be passed through the internal
     *     dependency validation filter prior to instantiation being allowed,
     *     which must designate that they are valid.
     *
     * @param array $args (optional) Any arguments required to instantiate
     *     the object correctly other than dependencies. Each argument will be
     *     passed through the internal argument validation filter prior to
     *     allowing instantiation to proceed. If any argument is invalid,
     *     instantiation will be blocked with an exception.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If either the dependencies or arguments do not validate correctly.
     */
    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_filterDependencyValidation( $dependencies );
        $this->_filterArgumentValidation( $args );
        $this->_setupObject( $dependencies, $args );
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns an array of keys that are acceptable when the `add` method is called.
     *
     * Custom parameters may be passed into the constructor to load
     * an object directly. This method may be called statically to preflight
     * a set of parameters prior to attempting to instantiate an instance.
     *
     * Keys returned by this method are expected to be accepted if they meet
     * the correct format.
     *
     * Keys not returned by this method are expected to be ignored or discarded
     * prior to scoping an object to the provided arguments.
     *
     * Keys that are provided by this method are expected to be validated prior
     * to internal scoping, and raise an exception if they are malformed.
     *
     * @return array
     */
    public static function accept()
    {
        $class = get_called_class();
        return array_keys( $class::getDefaults() );
    }

    /**
     * This method should be usable to generate the correct uri representation
     * of the media for embedding into html directly. This object should be able
     * to be echoed directly in a `src` or `href` tag, and return the properly
     * escaped, fully qualified uri via this method.
     *
     * It is the responsibility of the implementation to insure that the uri
     * is always set if a given set of parameters is valid.
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getUri();
    }

    /**
     * Gets the canonicalized handle for the media asset.
     * @return string
     */
    public function getUri()
    {
        return esc_url( $this->uri );
    }

    /**
     * Gets the canonicalized handle for the media asset.
     * @return string
     */
    public function getHandle()
    {
        return esc_attr( $this->handle );
    }

    /**
     * Loads a known media asset by key.
     *
     * The key can be an integer ID value or a string based slug,
     * depending what the individual media object represents.
     *
     * If the protected `checkKey` method returns false, then this method MUST
     * raise an exception. If the protected `checkKey` method returns true,
     * then this method MUST scope the object to the expected key
     * without raising an exception, and return itself for chainable operations.
     *
     * @param int|string $key The key that identifies the desired resource.
     * @return $this
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the key is not known, this exception must be raised.
     *     If a different exception is raised, it will be allowed
     *     to bubble up and crash the program, so that developers
     *     immediately know to fix their mistake.
     */
    public function load( $key )
    {
        return $this;
    }

    /**
     * Completes initialization after validation of injected parameters has passed.
     *
     * This method is automatically called when the validation filters resolve
     * for injected parameters, and should be expected to be fully non-blocking
     * in and of itself.
     *
     * If this method is called again without changing state,
     * it should not produce any additional changes.
     *
     * @return $this This method should return self for method chaining.
     */
    public function initialize()
    {
        //no-op
//        d( $this );
        return $this;
    }

    /**
     * Registers the object with any internal Wordpress operations required
     * to recognize it. For example, if the object represents a javascript file,
     * then calling this method should result in the file being properly queued,
     * provided the method is called within the scope of the correct hook.
     *
     * If this method represents an image, it should be added to the media library
     * without duplicating an identical image that has the exact same details.
     *
     * This theme does not provide any functionality for altering the database
     * or media library outside of the scope of the theme, but child themes
     * overriding this theme are provided with the means to do so with
     * minimal logic of their own being written if desired by extending this class,
     * and populating the register method with whatever logic is required to
     * accomplish what they are trying to do.
     *
     * @return $this This method should return self for method chaining.
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If registration fails, this method should throw the expected exception,
     *     and should be capable of predicting failure and pre-empting calls
     *     that would generate an error.
     */
    public function register()
    {
        return $this;
    }

    /**
     * This method adds an additional known asset to the internal media queue.
     *
     * This method does not scope the queue to the provided asset,
     * but does add it so that calling the `load` method on the
     * same handle will cause the object to be scoped to the
     * specified handle. Further instances of the media object
     * of the same class or its children should be able to
     * immediately scope to the same provided set of details
     * by calling `load` with the same key at any time
     * during the same runtime.
     *
     * @param string $handle
     * @param array $details
     * @return $this
     */
    public function add( $handle, $details )
    {
        $this->_filterArgumentValidation( $details );
        $this->args[$handle] = $details;
        $this->addResource( $handle, $details );
        return $this;
    }

    /**
     * Delete a custom handle from the internal registry of known resources.
     *
     * This cannot delete resources that are obtained with the config,
     * but can delete resources that are loaded manually at runtime.
     *
     * Whether or not this deletes the actual item, or only the
     * internal reference to the item is up to the specific
     * implementation of this abstract.
     *
     * It should be assumed that the resource will not be actually deleted
     * unless the child class explicitly states that it does so.
     *
     * @param type $handle
     */
    public function remove( $handle )
    {
        if ( array_key_exists( $handle, $this->args ) )
        {
            unset( $this->args[$handle] );
        }
    }

    /**
     * This should produce the usable set of details
     * from the provided arguments, so that all internals
     * are prepared for external use.
     *
     * @return $this
     */
    public function compile()
    {
        return $this;
    }

    /**
     * This method uses the standard wordpress `esc_url` method on a finalized uri.
     * This method is called when `setUri` is called, which is the only way for
     * a child class to set the uri correctly for this class.
     * @return string
     * @final
     */
    final protected function escapeUri( $uri )
    {
        return esc_url( $uri );
    }

    /**
     * This method uses the standard wordpress `esc_attr` method on a finalized handle.
     * This method is called when `setHandle` is called, which is the only way for
     * a child class to set the internal handle correctly for this class.
     * @return string
     * @final
     */
    final protected function escapeHandle( $handle )
    {
        return esc_attr( $handle );
    }

    /**
     * This is the internal setter for the uri,
     * which is returned when `__toString` is called.
     *
     * This method may be overridden, but the private uri parameter returned
     * will always be returned escaped, even if that is also overridden.
     *
     * @param string $uri
     */
    protected function setUri( $uri )
    {
        $this->uri = $this->escapeUri( $uri );
    }

    protected function setHandle( $handle )
    {
        $this->handle = $this->escapeHandle( $handle );
    }

    /**
     * Override this method to allow addition of additional queued assets
     * that should be universally recognized as legitimate.
     *
     * Resources added in this manner have already passed validation, and should
     * be universally available across all instances of the child object.
     */
    abstract protected function addResource( $handle, $details );

    /**
     * Returns a boolean determination as to whether the key can
     * be automatically loaded from the internal configuration stash.
     *
     * If this returns true, then the `load` method should resolve
     * correctly when presented with the same key without raising an exception.
     *
     * If this returns false, then the `load` method should be expected
     * to raise an exception when called with the same key.
     *
     * @param string|int $key
     * @return bool
     */
    protected function checkKey( $key )
    {
        return false;
    }

    /**
     * Override this method to provide a validation method
     * for parameters passed as dependencies.
     *
     * If this method does not return true,
     * instantiation will be blocked.
     *
     * @param type $dependencies
     * @return bool
     */
    protected function validateDependencies( $dependencies = null )
    {
        return true;
    }

    /**
     * Override this method to provide a validation method
     * for parameters passed as arguments.
     *
     * If this method does not return true, instantiation will be blocked.
     *
     * This method will be passed each argument individually,
     * and must be configured to know how to properly validate
     * any given key that the object might receive.
     *
     * If multiple keys are allowed, then this method should
     * route them to the correct internal validation method.
     *
     * @param int|string $key The argument key
     * @param mixed $arg The argument value
     * @return bool
     */
    protected function validateArgs( $key, $arg )
    {
        return true;
    }

    /**
     * Returns a boolean determination as to whether
     * the media object has a specific key.
     *
     * @param type $key
     * @return bool
     */
    protected function hasArg( $key )
    {
        return array_key_exists( $key, $this->args );
    }

    /**
     * Sets an argument into the media object.
     *
     * @param string $key The key to set
     * @param mixed $value The value for the key
     * @return void
     */
    protected function setArg( $key, $value )
    {
        $this->args[$key] = $value;
    }

    /**
     * Retrieves an argument by its key identifier.
     *
     * @param type $key
     * @return mixed
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the key is not found. Use `hasArg` to determine
     *     if this is safe first.
     */
    protected function getArg( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->args;
        }
        if ( !array_key_exists( $key, $this->args ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Unable to retrieve expected arg [%1$s] in [%2$s].', $key,
                get_class( $this ) ) );
        }
        return $this->args[$key];
    }

    /**
     * Gets a dependency the object has been injected with.
     *
     * @param string $key
     * @return object
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the given key does not exist as a known dependency owned
     *     by the current object.
     */
    protected function getDependency( $key = null )
    {
        if ( is_null( $key ) )
        {
            return $this->dependencies;
        }
        if ( is_null( $dependencies ) || !array_key_exists( $key,
                $this->dependencies ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Unable to retrieve expected dependency [%1$s] in [%2$s].',
                $key, get_class( $this ) ) );
        }
        return $this->dependencies[$key];
    }

    /**
     * Returns an associative array of the default acceptable arguments to present
     * back to the requester, which should have any required keys pre-populated
     * with their default values.
     *
     * Required keys should be left null. Any optional keys should be filled with
     * whatever their default value is to be non-colliding for the purpose they
     * would typically be used for.
     *
     * @return array
     */
    protected static function getDefaults()
    {
        return array();
    }

    /**
     * Internal setter for the parameters received from the constructor, if any.
     *
     * This method fires after the validation methods for the two have resolved
     * without throwing an exception or returning false, and calls the
     * initialize method if everything checks out.
     *
     * This method also filters out any keys that the class instance does not
     * explicitly declare as acceptable, so that only valid arguments will be
     * provided to the object.
     *
     * @param mixed $dependencies
     * @param array $args
     * @return void
     * @internal
     */
    private function _setupObject( $dependencies = null, $args = array() )
    {
        $this->dependencies = $dependencies;
        $accept = $this::accept();
        foreach ( $args as
            $key =>
            $arg )
        {
            if ( !in_array( $key, $accept ) )
            {
                unset( $args[$key] );
            }
        }
        $this->args = $args;
        $this->initialize();
    }

    /**
     * Performs the validation method on any dependencies passed.
     *
     * @param mixed $dependencies
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If dependencies don't pass validation
     * @internal
     */
    private function _filterDependencyValidation( $dependencies )
    {
        if ( !$this->validateDependencies( $dependencies ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( 'Invalid dependencies provided in [%1$s].',
            get_class( $this ) );
        }
    }

    /**
     * Performs the validation method on each argument passed.
     *
     * @param array $args
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If any arguments don't pass validation
     * @internal
     */
    private function _filterArgumentValidation( $args )
    {
        $invalid = array();
        foreach ( $args as
            $key =>
            $arg )
        {
            if ( !$this->validateArgs( $key, $arg ) )
            {
                $invalid[$key] = $arg;
            }
        }
        if ( !empty( $invalid ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException( sprintf( 'Instance of [%1$s] cannot instantiate due to invalid arguments [%2$s].',
                get_class( $this ), implode( ', ', array_keys( $invalid ) ) ) );
        }
    }

}
