<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\media;

/**
 * Javascript Handler
 *
 * Enqueues javascript correctly when the register method is called.
 * Does not locally make any decisions about what or when to queue.
 * Expects to be told when to queue by a controller or model.
 *
 * Typical usage:
 *
 * @example $script->load('script-slug')->compile()->register();
 *
 * @note Prefixes are automatically applied to non-3rd-party scripts,
 *     and omitted for 3rd-party libraries so they do not get included
 *     multiple times across various themes and plugins.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class Javascript
    extends AbstractMedia
{

    /**
     * Override this constant to declare a media type.
     */
    const MEDIA_TYPE = 'script';
    const MEDIA_SUFFIX = 'js';
    const CONFIG_KEY = 'scripts';

    /**
     * Represents all of the scripts declared by the theme in local files.
     * These can be directly accessed immediately by keyword using the `load` method.
     * @var array
     */
    private static $local_scripts;

    /**
     * Represents any custom scripts not known to the theme registered at runtime.
     * These can be entered directly, at which point they will be available by
     * keyword any time thereafter during runtime.
     * @var array
     */
    private static $custom_scripts = array();

    /**
     * Represents a shared pool of script handles that are already
     * registered for the current runtime, so that duplicate registration
     * does not occur.
     * @var array
     */
    private static $registered_scripts = array();

    /**
     * Additional arguments this object accepts in addition
     * to the wordpress standard arguments.
     * @var array
     */
    private static $additional_arguments = array(
        'thirdparty' => 'boolean',
        'inline' => 'boolean',
        'minified' => 'boolean',
        'code' => 'string',
    );

    /**
     * Whether or not the resource represents a 3rd party asset.
     * 3rd party assets are not prefixed, to alleviate the likelihood
     * of duplication by the wordpress render engine.
     * @var bool
     */
    private $thirdparty = false;

    /**
     * Represents the script version, if any.
     * @var bool
     */
    private $version = false;

    /**
     * Represents whether to show the script in the footer or the header.
     * @var bool
     */
    private $footer = true;

    /**
     * Represents whether the current resource is locally served or remotely served.
     * If remote, its DNS value will be added to the prefetch array.
     * @var bool
     */
    private $local = true;

    /**
     * Represents the source code of inline scripts.
     * @var bool
     */
    private $source = null;

    /**
     * Represents whether the current resource is an inline script.
     * @var bool
     */
    private $inline = false;

    /**
     * Represents any dependencies of the current subject, if any.
     * @var array
     */
    private $dependency_scripts = array();

    /**
     * Represents whether to prefer minified or uncompressed resources.
     * If true, the minified file will be queued if one is available.
     * If false, the unminified file will always be queued, and scoping
     * to the resource will be rejected if an unminified version is not
     * available.
     * @var bool
     */
    private $prefer_minified = true;

    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initializeLocalScripts();
        $this->_initializeCustomScripts( $args );
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns an array of keys that are acceptable when the `add` method is called.
     *
     * Custom parameters may be passed into the constructor to load
     * an object directly. This method may be called statically to preflight
     * a set of parameters prior to attempting to instantiate an instance.
     *
     * Keys returned by this method are expected to be accepted if they meet
     * the correct format.
     *
     * Keys not returned by this method are expected to be ignored or discarded
     * prior to scoping an object to the provided arguments.
     *
     * Keys that are provided by this method are expected to be validated prior
     * to internal scoping, and raise an exception if they are malformed.
     *
     * @return array
     */
    public static function accept()
    {
        return array_merge( parent::accept(), self::$additional_arguments );
    }

    public function initialize()
    {
        $args = $this->getArg();
//        d( $args );
//        exit();
        return parent::initialize();
    }

    /**
     * Loads a javascript file by key.
     *
     * The key should be a slug corresponding to the script, without a prefix.
     * Prefixes will be automatically applied based on the current theme slug
     * (parent or child, whichever is active).
     *
     * @param int|string $key The key that identifies the desired resource.
     * @return $this
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the key is not known, this exception must be raised.
     */
    public function load( $key )
    {
        if ( !$this->checkKey( $key ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Script key [%1$s] is not known in [%2$s].',
                esc_attr( $key ), get_class( $this ) )
            );
        }
        $details = $this->_fetchResource( $key );
        $this->inline = false;
        if ( ( array_key_exists( 'inline', $details ) && $details['inline'] ) )
        {
            //inline
            $this->inline = true;
        }
        $this->dependency_scripts = array();
        if ( array_key_exists( 'dependencies', $details['details'] ) )
        {
            $this->dependency_scripts = $details['details']['dependencies'];
        }
        $this->thirdparty = false;
        if ( array_key_exists( 'thirdparty', $details ) )
        {
            $this->thirdparty = $details['thirdparty'];
        }
        $this->version = false;
        if ( array_key_exists( 'version', $details['details'] ) )
        {
            $this->version = $details['details']['version'];
        }
        $this->footer = true;
        if ( array_key_exists( 'footer', $details['details'] ) )
        {
            //Javascript doesn't load right in the Wordpress admin footer.
            //
            //Because reasons.
            //
            //Lol wordpress.
            $this->footer = is_admin()
                ? false
                : $details['details']['footer'];
        }
        $this->setHandle( $this->thirdparty
                ? $details['handle']
                : $this->getThemePrefix() . '-' . $details['handle']  );
        if ( !array_key_exists( 'inline', $details ) || !$details['inline'] )
        {
            $this->setUri( $this->_resolveUri( $details['details']['source'] ) );
        } else
        {
            $this->source = '/** ' . $this->getThemePrefix() . '-' . $details['handle'] . ' */ ' . PHP_EOL . $details['details']['source'];
        }
        return $this;
    }

    /**
     * This should produce the usable set of details
     * from the provided arguments, so that all internals
     * are prepared for external use, and queue any dependency
     * scripts if they are not already queued.
     *
     * This should be called prior to register.
     *
     * @param type $args
     */
    public function compile()
    {
        return $this;
    }

    /**
     * Enqueues, registers, prefixes, sanitizes, escapes, and otherwise
     * handles a script being properly added, including all of its
     * declared dependencies, if they were provided.
     *
     * This method must be called during the appropriate wordpress hook,
     * but should always queue properly if it is.
     *
     * @return $this
     */
    public function register()
    {
        if ( !is_null( $this->getHandle() ) && !in_array( $this->getHandle(),
                self::$registered_scripts ) )
        {
            $class = get_class( $this );
            //Make sure all dependency scripts are already queued.
            $dependencies = array();
            foreach ( $this->dependency_scripts as
                $dependency )
            {
                $dependencies[] = $class::init()
                    ->load( $dependency )
                    ->compile()
                    ->register()
                    ->getHandle();
            }
            $defaults = $this->getDefaults();
            $payload = array(
                'handle' => esc_attr( $this->getHandle() ),
                'source' => $this->inline
                ? esc_js( $this->source )
                : esc_url( $this->getUri() ),
                'dependencies' => $dependencies,
                'version' => $this->version,
                'footer' => $this->footer,
            );
            if ( $this->inline )
            {
                // register the inline script
                if ( $this->footer )
                {
                    // It gets queued after "placeholder-footer"
                    wp_add_inline_script( $this->getThemePrefix() . '-placeholder-footer',
                        $this->source, 'after' );
                } else
                {
                    // It gets queued after "placeholder-header"
                    wp_add_inline_script( $this->getThemePrefix() . '-placeholder-header',
                        $this->source, 'after' );
                }
            } else
            {
                //register normally
                $register = wp_register_script(
                    $payload['handle'], $payload['source'],
                    $payload['dependencies'], $payload['version'],
                    $payload['footer']
                );
                $enqueued = wp_enqueue_script(
                    $payload['handle'], $payload['source'],
                    $payload['dependencies'], $payload['version'],
                    $payload['footer']
                );
                $payload['registered'] = $register;
                $payload['enqueued'] = $enqueued;
            }
            $this->debugStash( $payload, 'script_registered' );
            self::$registered_scripts[] = $this->getHandle();
        }
        return parent::register();
    }

    /**
     * Adds additional script resources at runtime
     * that can be referenced by key across all Stylesheet
     * objects as a valid resource.
     *
     * @return void
     */
    protected function addResource( $handle, $details )
    {
        self::$custom_scripts[$handle] = $details;
    }

    /**
     * Returns a boolean determination as to whether the key can
     * be automatically loaded from the internal configuration stash.
     *
     * If this returns true, then the `load` method should resolve
     * correctly when presented with the same key without raising an exception.
     *
     * If this returns false, then the `load` method should be expected
     * to raise an exception when called with the same key.
     *
     * @param string|int $key
     * @return bool
     */
    protected function checkKey( $key )
    {
        $config = $this->getConfig();
        return array_key_exists( $key, $config ) || array_key_exists( $key,
                self::$custom_scripts );
    }

    protected function validateDependencies( $dependencies = null )
    {
        if ( is_null( $dependencies ) )
        {
            return true;
        }
        return false;
    }

    /**
     * Checks javascript args, to insure they are correctly formatted if provided.
     *
     * @param int|string $key The argument key
     * @param mixed $arg The argument value
     * @return bool
     */
    protected function validateArgs( $key, $arg )
    {
        switch ( $key )
        {
            case "handle":
                return is_int( $arg ) || (is_string( $arg ) && preg_match( '/^[a-z0-9-_]+$/',
                        $arg ));
                break;
            case "thirdparty":
            case "inline":
            case "minified":
                return in_array( $arg,
                    array(
                    true,
                    false,
                    0,
                    1 ) );
                break;
            case "details":
                //At least the source param must exist in details.
                //The rest can be default, but this one has to be there.
                return (is_array( $arg )
                    && array_key_exists( 'source', $arg )
                    );
                break;
            default:
                //It'll get filtered out automatically if it's not known,
                //and inline code will be escaped properly before output anyhow.
                return true;
        }
    }

    /**
     * Returns the default acceptable parameters for queuing scripts with Wordpress.
     *
     * The handle and source must be provided,
     * but the rest of the parameters are optional.
     *
     * @return array
     */
    protected static function getDefaults()
    {
        return array(
            'handle' => null,
            'source' => null,
            'dependencies' => array(),
            'version' => false,
            'footer' => false
        );
    }

    private function _initializeLocalScripts()
    {
        if ( is_null( self::$local_scripts ) )
        {
            self::$local_scripts = $this->getConfig();
        }
    }

    private function _initializeCustomScripts( $args )
    {
        if ( is_null( self::$custom_scripts ) )
        {
            self::$custom_scripts = $this->getConfig();
        }
        if ( empty( $args ) )
        {
            return;
        }
        if ( !array_key_exists( 'handle', $args ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Missing [id] for provided custom script in [%1$s]',
                get_class( $this ) )
            );
        }
        foreach ( $args as
            $key =>
            $arg )
        {
            $this->validateArgs( $key, $arg );
        }
        $id = esc_attr( $args['handle'] );
        self::$custom_scripts[$id] = array_replace( $this->getDefaults(), $args );
    }

    private function _resolveUri( $source )
    {
        if ( $this->prefer_minified && array_key_exists( 'min', $source ) && $source['min'] )
        {
            $base = $source['min'];
        } else
        {
            $base = $source['full'];
        }
        $child_dir = trailingslashit( get_stylesheet_directory() );
        if ( is_readable( $child_dir . $base ) )
        {
            return $this::getAdapter()->get( 'uri', 'override' ) . $base;
        }
        return $this::getAdapter()->get( 'uri', 'sanctity' ) . $base;
    }

    private function _fetchResource( $handle )
    {
        $scripts = array_replace_recursive( self::$local_scripts,
            self::$custom_scripts );
        if ( !array_key_exists( $handle, $scripts ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Script [%1$s] is not registered in [%2$s].', $handle,
                get_class( $this ) )
            );
        }
        return $scripts[$handle];
    }

    private function _formatKeys( $details )
    {

    }

}
