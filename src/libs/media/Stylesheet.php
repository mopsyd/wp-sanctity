<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\libs\media;

/**
 * Stylesheet Handler
 *
 * Enqueues css stylesheets correctly when the register method is called.
 * Does not locally make any decisions about what or when to queue.
 * Expects to be told when to queue by a controller or model.
 *
 * Typical usage:
 *
 * @example $style->load('script-slug')->compile()->register();
 *
 * @note Prefixes are automatically applied to non-3rd-party stylesheets,
 *     and omitted for 3rd-party libraries so they do not get included
 *     multiple times across various themes and plugins.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
class Stylesheet
    extends AbstractMedia
{

    /**
     * Override this constant to declare a media type.
     */
    const MEDIA_TYPE = 'style';
    const MEDIA_SUFFIX = 'css';
    const CONFIG_KEY = 'styles';

    /**
     * Represents all of the stylesheets declared by the theme in local files.
     * These can be directly accessed immediately by keyword using the `load` method.
     * @var array
     */
    private static $local_styles;

    /**
     * Represents any custom stylesheets not known to the theme registered at runtime.
     * These can be entered directly, at which point they will be available by
     * keyword any time thereafter during runtime.
     * @var array
     */
    private static $custom_styles = array();

    /**
     * Represents a shared pool of stylesheet handles that are already
     * registered for the current runtime, so that duplicate registration
     * does not occur.
     * @var array
     */
    private static $registered_styles = array();

    /**
     * Additional arguments this object accepts in addition
     * to the wordpress standard arguments.
     * @var array
     */
    private static $additional_arguments = array(
        'thirdparty' => 'boolean',
        'inline' => 'boolean',
        'minified' => 'boolean',
        'code' => 'string',
    );

    /**
     * Whether or not the resource represents a 3rd party asset.
     * 3rd party assets are not prefixed, to alleviate the likelihood
     * of duplication by the wordpress render engine.
     * @var bool
     */
    private $thirdparty = false;

    /**
     * Represents the stylesheet version, if any.
     * @var bool
     */
    private $version = false;

    /**
     * Represents the output media for the stylesheet.
     * @var bool
     */
    private $media = 'any';

    /**
     * Represents whether the current resource is locally served or remotely served.
     * If remote, its DNS value will be added to the prefetch array.
     * @var bool
     */
    private $local = true;

    /**
     * Represents the source code of inline styles.
     * @var bool
     */
    private $source = null;

    /**
     * Represents whether the current resource is an inline stylesheet.
     * @var bool
     */
    private $inline = false;

    /**
     * Represents any dependencies of the current subject, if any.
     * @var array
     */
    private $dependency_styles = array();

    /**
     * Represents whether to prefer minified or uncompressed resources.
     * If true, the minified file will be queued if one is available.
     * If false, the unminified file will always be queued, and scoping
     * to the resource will be rejected if an unminified version is not
     * available.
     * @var bool
     */
    private $prefer_minified = true;

    public function __construct( $dependencies = null, $args = array() )
    {
        $this->_initializeLocalStyles();
        parent::__construct( $dependencies, $args );
    }

    /**
     * Returns an array of keys that are acceptable when the `add` method is called.
     *
     * Custom parameters may be passed into the constructor to load
     * an object directly. This method may be called statically to preflight
     * a set of parameters prior to attempting to instantiate an instance.
     *
     * Keys returned by this method are expected to be accepted if they meet
     * the correct format.
     *
     * Keys not returned by this method are expected to be ignored or discarded
     * prior to scoping an object to the provided arguments.
     *
     * Keys that are provided by this method are expected to be validated prior
     * to internal scoping, and raise an exception if they are malformed.
     *
     * @return array
     */
    public static function accept()
    {
        return array_merge( parent::accept(), self::$additional_arguments );
    }

    public function initialize()
    {
        $args = $this->getArg();
//        d( $args );
//        exit();
        return parent::initialize();
    }

    /**
     * Loads a stylesheet file by key.
     *
     * The key should be a slug corresponding to the stylesheet, without a prefix.
     * Prefixes will be automatically applied based on the current theme slug
     * (parent or child, whichever is active).
     *
     * @param int|string $key The key that identifies the desired resource.
     * @return $this
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If the key is not known, this exception must be raised.
     */
    public function load( $key )
    {
        if ( !$this->checkKey( $key ) )
        {
            d( $key, $this->getConfig(), self::$custom_styles );
            exit;
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Stylesheet key [%1$s] is not known in [%2$s].',
                esc_attr( $key ), get_class( $this ) )
            );
        }
        $details = $this->_fetchResource( $key );
        $this->inline = false;
        if ( ( array_key_exists( 'inline', $details ) && $details['inline'] ) )
        {
            //inline
            $this->inline = true;
        }
        $this->dependency_styles = array();
        if ( array_key_exists( 'dependencies', $details['details'] ) )
        {
            $this->dependency_styles = $details['details']['dependencies'];
        }
        $this->thirdparty = false;
        if ( array_key_exists( 'thirdparty', $details ) )
        {
            $this->thirdparty = $details['thirdparty'];
        }
        $this->version = false;
        if ( array_key_exists( 'version', $details['details'] ) )
        {
            $this->version = $details['details']['version'];
        }
        $this->media = 'any';
        if ( array_key_exists( 'media', $details['details'] ) )
        {
            $this->media = $details['details']['media'];
//            $this->media = is_admin()
//                ? false
//                : $details['details']['media'];
        }
        $this->setHandle( $this->thirdparty
                ? $details['handle']
                : $this->getThemePrefix() . '-' . $details['handle']  );
        $this->setUri( $this->_resolveUri( $details['details']['source'] ) );
        return $this;
    }

    /**
     * This should produce the usable set of details
     * from the provided arguments, so that all internals
     * are prepared for external use, and queue any dependency
     * stylesheets if they are not already queued.
     *
     * This should be called prior to register.
     *
     * @param type $args
     */
    public function compile()
    {
        return $this;
    }

    /**
     * Enqueues, registers, prefixes, sanitizes, escapes, and otherwise
     * handles a stylesheet being properly added, including all of its
     * declared dependencies, if they were provided.
     *
     * This method must be called during the appropriate wordpress hook,
     * but should always queue properly if it is.
     *
     * @return $this
     */
    public function register()
    {
        if ( !is_null( $this->getHandle() ) && !in_array( $this->getHandle(),
                self::$registered_styles ) )
        {
            $class = get_class( $this );
            //Make sure all dependency scripts are already queued.
            $dependencies = array();
            foreach ( $this->dependency_styles as
                $dependency )
            {
                $dependencies[] = $class::init()
                    ->load( $dependency )
                    ->compile()
                    ->register()
                    ->getHandle();
            }
            $defaults = $this->getDefaults();
            $payload = array(
                'handle' => esc_attr( $this->getHandle() ),
                'source' => $this->inline
                ? $this->source                 //There is no known css escape filter in Wordpress as of this writing.
                : esc_url( $this->getUri() ),
                'dependencies' => $dependencies,
                'version' => $this->version,
                'media' => $this->media,
            );
            if ( $this->inline )
            {
                d( 'register inline' );
                //register the inline script
            } else
            {
                //register normally
                $register = wp_register_style(
                    $payload['handle'], $payload['source'],
                    $payload['dependencies'], $payload['version'],
                    $payload['media']
                );
                $enqueued = wp_enqueue_style(
                    $payload['handle'], $payload['source'],
                    $payload['dependencies'], $payload['version'],
                    $payload['media']
                );
                $payload['registered'] = $register;
                $payload['enqueued'] = $enqueued;
            }
            $this->debugStash( $payload, 'stylesheet_registered' );
            self::$registered_styles[] = $this->getHandle();
        }
        return parent::register();
    }

    /**
     * Adds additional stylesheet resources at runtime
     * that can be referenced by key across all Stylesheet
     * objects as a valid resource.
     *
     * @return void
     */
    protected function addResource( $handle, $details )
    {
        self::$custom_styles[$handle] = $details;
    }

    /**
     * Returns a boolean determination as to whether the key can
     * be automatically loaded from the internal configuration stash.
     *
     * If this returns true, then the `load` method should resolve
     * correctly when presented with the same key without raising an exception.
     *
     * If this returns false, then the `load` method should be expected
     * to raise an exception when called with the same key.
     *
     * @param string|int $key
     * @return bool
     */
    protected function checkKey( $key )
    {
        $config = $this->getConfig();
        return array_key_exists( $key, $config ) || array_key_exists( $key,
                self::$custom_styles );
    }

    protected function validateDependencies( $dependencies = null )
    {
        if ( is_null( $dependencies ) )
        {
            return true;
        }
        return false;
    }

    /**
     * Checks stylesheet args, to insure they are correctly formatted if provided.
     *
     * @param int|string $key The argument key
     * @param mixed $arg The argument value
     * @return bool
     */
    protected function validateArgs( $key, $arg )
    {
        switch ( $key )
        {
            case "handle":
                return is_int( $arg ) || (is_string( $arg ) && preg_match( '/^[a-z0-9-_]+$/',
                        $arg ));
                break;
            case "thirdparty":
            case "inline":
            case "minified":
                return in_array( $arg,
                    array(
                    true,
                    false,
                    0,
                    1 ) );
                break;
            case "media":
                //Matches valid media declarations and single line media query declarations.
                return is_string( $arg )
                    && (in_array( $key,
                        array(
                        'any',
                        'all',
                        'screen',
                        'print' ) )
                    || preg_match( '//@media([^{]+)\{([\s\S]+?})\s*}/g/', $arg )
                    || preg_match( '/^\s*[a-zA-Z\-]+\s*[:]{1}\s[a-zA-Z0-9\s.#]+[;]{1}/',
                        $arg ) );
                break;
            case "inline":
            case "minified":
                return in_array( $arg,
                    array(
                    true,
                    false,
                    0,
                    1 ) );
                break;
            case "details":
                //At least the source param must exist in details.
                //The rest can be default, but this one has to be there.
                return (is_array( $arg )
                    && array_key_exists( 'source', $arg )
                    );
                break;
            default:
                //It'll get filtered out automatically if it's not known,
                //and inline code will be escaped properly before output anyhow.
                return true;
        }
    }

    /**
     * Returns the default acceptable parameters for queuing stylesheets with Wordpress.
     *
     * The handle and source must be provided,
     * but the rest of the parameters are optional.
     *
     * @return array
     */
    protected static function getDefaults()
    {
        return array(
            'handle' => null,
            'source' => null,
            'dependencies' => array(),
            'version' => null,
            'media' => 'any'
        );
    }

    private function _initializeLocalStyles()
    {
        if ( is_null( self::$local_styles ) )
        {
            self::$local_styles = $this->getConfig();
        }
    }

    private function _resolveUri( $source )
    {
        if ( $this->prefer_minified && array_key_exists( 'min', $source ) && $source['min'] )
        {
            $base = $source['min'];
        } else
        {
            $base = $source['full'];
        }
        $child_dir = trailingslashit( get_stylesheet_directory() );
        if ( is_readable( $child_dir . $base ) )
        {
            return $this::getAdapter()->get( 'uri', 'override' ) . $base;
        }
        return $this::getAdapter()->get( 'uri', 'sanctity' ) . $base;
    }

    private function _fetchResource( $handle )
    {
        $scripts = array_replace_recursive( self::$local_styles,
            self::$custom_styles );
        if ( !array_key_exists( $handle, $scripts ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Stylesheet [%1$s] is not registered in [%2$s].', $handle,
                get_class( $this ) )
            );
        }
        return $scripts[$handle];
    }

    private function _formatKeys( $details )
    {

    }

}
