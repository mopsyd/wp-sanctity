<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if ( !defined( 'ABSPATH' ) && !getenv('CI') )
{
    die();
}

/**
 * Polly filled in all the blanks that the deprecated PHP versions lacked,
 *
 * so they could pretend they were keeping with the times if only for a moment.
 *
 *
 * Makes old versions of PHP behave.
 *
 * This file provides native emulation of standard php functions
 * that exist in more recent versions through 7.1.
 *
 * It assumes a PHP install of at least 5.5 (5.4 and lower are not supported by this package)
 *
 * These functions are not prefixed on purpose, because they are emulating native
 * functionality of more recent PHP versions, and should transparently be usable
 * in the global scope, because they would exist there anyways if you were using
 * a more recent version of PHP.
 *
 * This package avoids use of *language constructs* that do not exist in legacy PHP versions,
 * because those cannot be emulated at runtime in legacy PHP versions (eg: type hinting from PHP 7+, etc).
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */


/**
 * Provides compatibility for iterable objects, which can be used
 * in place of `is_array` to support modern iterable constructs
 * including iterators, generators, etc that are not arrays
 * but can still be used correctly in `foreach`.
 *
 * @param mixed $obj The subject to evaluate
 * @return bool Returns `true` if the provided `$obj` can safely
 *     be used in a loop using `foreach`, etc.
 */
if ( !function_exists( 'is_iterable' ) )
{

    function is_iterable( $obj )
    {
        return is_array( $obj ) || ( is_object( $obj ) && ( $obj instanceof \Traversable ) );
    }

}

if ( !function_exists( 'array_column' ) )
{

    /**
     * Returns the values from a single column of the input array, identified by
     * the $columnKey.
     *
     * Optionally, you may provide an $indexKey to index the values in the returned
     * array by the values from the $indexKey column in the input array.
     *
     * @param array $input A multi-dimensional array (record set) from which to pull
     *                     a column of values.
     * @param mixed $columnKey The column of values to return. This value may be the
     *                         integer key of the column you wish to retrieve, or it
     *                         may be the string key name for an associative array.
     * @param mixed $indexKey (Optional.) The column to use as the index/keys for
     *                        the returned array. This value may be the integer key
     *                        of the column, or it may be the string key name.
     * @return array
     */
    function array_column( $input = null, $columnKey = null, $indexKey = null )
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();
        if ( $argc < 2 )
        {
            trigger_error( "array_column() expects at least 2 parameters, {$argc} given",
                E_USER_WARNING );
            return null;
        }
        if ( !is_array( $params[0] ) )
        {
            trigger_error(
                'array_column() expects parameter 1 to be array, ' . gettype( $params[0] ) . ' given',
                E_USER_WARNING
            );
            return null;
        }
        if ( !is_int( $params[1] )
            && !is_float( $params[1] )
            && !is_string( $params[1] )
            && $params[1] !== null
            && !(is_object( $params[1] ) && method_exists( $params[1],
                '__toString' ))
        )
        {
            trigger_error( 'array_column(): The column key should be either a string or an integer',
                E_USER_WARNING );
            return false;
        }
        if ( isset( $params[2] )
            && !is_int( $params[2] )
            && !is_float( $params[2] )
            && !is_string( $params[2] )
            && !(is_object( $params[2] ) && method_exists( $params[2],
                '__toString' ))
        )
        {
            trigger_error( 'array_column(): The index key should be either a string or an integer',
                E_USER_WARNING );
            return false;
        }
        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null)
            ? (string) $params[1]
            : null;
        $paramsIndexKey = null;
        if ( isset( $params[2] ) )
        {
            if ( is_float( $params[2] ) || is_int( $params[2] ) )
            {
                $paramsIndexKey = (int) $params[2];
            } else
            {
                $paramsIndexKey = (string) $params[2];
            }
        }
        $resultArray = array();
        foreach ( $paramsInput as
            $row )
        {
            $key = $value = null;
            $keySet = $valueSet = false;
            if ( $paramsIndexKey !== null && array_key_exists( $paramsIndexKey,
                    $row ) )
            {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }
            if ( $paramsColumnKey === null )
            {
                $valueSet = true;
                $value = $row;
            } elseif ( is_array( $row ) && array_key_exists( $paramsColumnKey,
                    $row ) )
            {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }
            if ( $valueSet )
            {
                if ( $keySet )
                {
                    $resultArray[$key] = $value;
                } else
                {
                    $resultArray[] = $value;
                }
            }
        }
        return $resultArray;
    }

}

if ( !function_exists( 'json_last_error_msg' ) )
{

    function json_last_error_msg()
    {
        static $ERRORS = array(
            JSON_ERROR_NONE => 'No error',
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX => 'Syntax error',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded'
        );

        $error = json_last_error();
        return isset( $ERRORS[$error] )
            ? $ERRORS[$error]
            : 'Unknown error';
    }

}