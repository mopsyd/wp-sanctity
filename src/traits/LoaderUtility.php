<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Base Loader
 *
 * Provides the foundational logic for loading classes dynamically to other traits.
 *
 * This trait is only providing underlying abstraction, and is not intended to
 * be used directly by a class. It is meant as a dependency of other traits,
 * who provide shaped opinion as to what should be loaded based on their own
 * scope.
 *
 * The underlying mechanism of this logic works regardless of namespace,
 * but requires that Psr-4 is implemented, and that the directory structure
 * between namespaces is identical.
 *
 * For example, if prefixes `mopsyd\sanctity` and `vendor\package` are registered,
 * and a request for `$this->_loadClass('autoload\\Autoload', 'libs');` is called,
 *
 * Then a class that exists under either `mopsyd\sanctity\libs\autoload\Autoload`
 * or `vendor\package\libs\autoload\Autoload` will be found correctly. In either case,
 * it must also satisfy the required interface keyed to 'libs'.
 *
 * If all of these conditions are met, then it will return the class.
 * If all valid options are exhausted, it will throw an instance of
 * `\InvalidArgumentException` that bears the
 * `mopsyd\sanctity\interfaces\SanctityExceptionInterface`
 *
 * Additional support for path mappings may be added at a later date,
 * but this is flexible enough for the time being. Individual factories
 * can implement mapping protocols in the mean time if required.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
trait LoaderUtility
{

    /**
     * Represents an array of classes already loaded.
     * For consistency's sake, any classes that already exist will not be
     * loaded again, to insure that objects instantiated at different times
     * across Wordpress's event driven system can all access the same object
     * instances.
     *
     * This is not an ideal resolution, but the other options would be to pass
     * everything by reference or do everything statically, which would be
     * significantly more messy and bug prone.
     *
     * @var array
     */
    private static $loaded_classes = array();

    /**
     * One factory to rule them all!
     * @var \mopsyd\sanctity\factory\FactoryFactory
     */
    private static $factory;

    /**
     * Sets the factory factory, so loading classes dynamically is universally enabled.
     *
     * This object is fixed, and does not allow for overrides by interface.
     * You may register new factories with the provided FactoryFactory,
     * but you may not override the FactoryFactory.
     *
     * @param \mopsyd\sanctity\factory\FactoryFactory $factory
     */
    public static function setFactory( \mopsyd\sanctity\factory\FactoryFactory $factory )
    {
        self::$factory = $factory;
    }

    /**
     * Loads and returns an instance of a class dynamically.
     *
     * Classes that have already been loaded will be returned directly without
     * being loaded again. This is so consistent object instances can work
     * seamlessly across Wordpress's event driven architecture without
     * cluttering the global scope, passing everything by reference, or having
     * to do everything with static properties or procedural spaghetti.
     *
     * This can be overridden in any child class if this behavior is inappropriate
     * to the current scope, and may get factored out entirely if the architecture
     * can be made entirely stateless.
     *
     * @param string $type The type to load. Valid types are controller, model,
     *     view, library, factory, extension, and router
     * @param string $class The class name to return. This should be the suffix
     *     that resolves to it's namespace segment after its type.
     * @return mopsyd\sanctity\interfaces\SanctityInterface
     */
    protected function load( $type, $class, $dependencies = null,
        $args = array() )
    {
        if ( !is_string( $type ) && is_string( $class ) )
        {
            self::debugDump(array($type, $class));
        }
        $key = $type . '\\' . $class;
        if ( array_key_exists( $key, self::$loaded_classes ) )
        {
            $class = self::$loaded_classes[$key];
            return $class::init( $dependencies );
        }
        $factory = ucfirst( $type ) . 'Factory';
        $instance = self::$factory->load( $factory )->load( $class, $args,
            $dependencies );
        self::$loaded_classes[$key] = $instance;
        return $instance;
    }

}
