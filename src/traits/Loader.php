<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Base Loader
 *
 * Provides the foundational logic for loading classes dynamically to other traits.
 *
 * This trait is only providing underlying abstraction, and is not intended to
 * be used directly by a class. It is meant as a dependency of other traits,
 * who provide shaped opinion as to what should be loaded based on their own
 * scope.
 *
 * The underlying mechanism of this logic works regardless of namespace,
 * but requires that Psr-4 is implemented, and that the directory structure
 * between namespaces is identical.
 *
 * For example, if prefixes `mopsyd\sanctity` and `vendor\package` are registered,
 * and a request for `$this->_loadClass('autoload\\Autoload', 'libs');` is called,
 *
 * Then a class that exists under either `mopsyd\sanctity\libs\autoload\Autoload`
 * or `vendor\package\libs\autoload\Autoload` will be found correctly. In either case,
 * it must also satisfy the required interface keyed to 'libs'.
 *
 * If all of these conditions are met, then it will return the class.
 * If all valid options are exhausted, it will throw an instance of
 * `\InvalidArgumentException` that bears the
 * `mopsyd\sanctity\interfaces\SanctityExceptionInterface`
 *
 * Additional support for path mappings may be added at a later date,
 * but this is flexible enough for the time being. Individual factories
 * can implement mapping protocols in the mean time if required.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
trait Loader
{

    private static $_namespace_prefixes = array(
        'sanctity' => 'mopsyd\\sanctity'
    );
    private static $_valid_type_interfaces = array(
        'libs' => 'mopsyd\\sanctity\\interfaces\\SanctityInterface',
        'controllers' => 'mopsyd\\sanctity\\interfaces\\controllers\\SanctityControllerInterface',
        'models' => 'mopsyd\\sanctity\\interfaces\\models\\SanctityModelInterface',
        'views' => 'mopsyd\\sanctity\\interfaces\\views\\SanctityViewInterface',
        'extensions' => 'mopsyd\\sanctity\\interfaces\\libs\\extension\\ExtensionInterface',
        'factory' => 'mopsyd\\sanctity\\interfaces\\libs\\factory\\FactoryInterface',
        'router' => 'mopsyd\\sanctity\\interfaces\\libs\\router\\SanctityRouterInterface',
        'wordpress' => 'mopsyd\\sanctity\\interfaces\\libs\\wordpress\\WordpressAdapterInterface',
        'twig' => 'mopsyd\\sanctity\\interfaces\\libs\\twig\\TwigAdapterInterface',
    );

    public static function registerNamespacePrefix( $prefix )
    {
        if ( !is_string( $prefix ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid namespace prefix supplied in instance of [%1$s]. Expected [string], but received [%2$s]',
                get_class( $this ), gettype( $prefix ) )
            );
        }
        $prefix = trim( $prefix, '\\' );
        if ( !in_array( $prefix, self::$_namespace_prefixes ) )
        {
            self::$_namespace_prefixes[] = $prefix;
        }
    }

    private function _loadClass( $name, $type, $dependencies = null, $args = array() )
    {
        $this->_typecheckLoaderClassName( $name );
        $this->_typecheckLoaderClassType( $type );
        foreach ( self::$_namespace_prefixes as
            $prefix )
        {
            $class = $this->_constructClassName( $prefix, $name, $type );
            if ( !$this->_validateType( $class, $type ) )
            {
                continue;
            }
            $instance = $class::init( $dependencies, $args );
            return $instance;
        }
        //If all options have been exhausted, report an error.
        throw new \mopsyd\sanctity\libs\exception\SanctityException(
        sprintf( 'Class [%1$s] of type [%2$s] not found in any known namespace. Requested in [%3$s].',
            $name, $type, get_class( $this ) )
        );
    }

    private function _typecheckLoaderClassName( $name )
    {
        if ( !is_string( $name ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid class name supplied in instance of [%1$s]. '
                . 'Expected [string], but received [%2$s]', get_class( $this ),
                gettype( $name ) )
            );
        }
    }

    private function _typecheckLoaderClassType( $type )
    {
        if ( !is_string( $type ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid class type supplied in instance of [%1$s]. '
                . 'Expected [string], but received [%2$s]', get_class( $this ),
                gettype( $type ) )
            );
        }
    }

    private function _constructClassName( $prefix, $name, $type )
    {
        $class = $prefix . '\\' . $type . '\\' . $name;
        return $class;
    }

    private function _validateType( $class, $type )
    {
        return class_exists( $class ) //Must be an actual class
            //Validation interface for the given type must be known to this object
            && array_key_exists( $type, self::$_valid_type_interfaces )
            //Validation interface must exist on the given class
            && in_array( self::$_valid_type_interfaces[$type],
                class_implements( $class ) );
    }

}
