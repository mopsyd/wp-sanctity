<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Generic State Pattern
 *
 * Basic generic implementation of the State design pattern.
 *
 * This is used for providing abstraction for stateful operations
 * that require specific steps to be taken on certain states.
 *
 * As this is an event driven system, this often becomes a pretty pressing need.
 * This approach cannot really be avoided while still retaining the capacity to
 * represent any number of underlying platforms. An event driven system can
 * represent a non-event driven underlying utility, but the reverse is not
 * really true in most cases.
 *
 * Some classes will expand on this with a heavily customized approach,
 * as design patterns usefulness are usually more pragmatically applied
 * on a case by case than as a general strict mandate.
 * This is just the generic drop in default mixin for immediate use.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait StatePattern
{

    private $default_state = null;
    private $valid_states = array();
    private $state;

    /**
     * Triggers the callback assigned to the state
     *
     * @param string $state
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If an invalid state is passed. The provided state must be in
     *     the declared set, or the default. The default does not require
     *     a callback, which allows a reset action to take no action without
     *     assigning a nulled method to it. All other states require a valid
     *     callback and must exist in the valid states array.
     */
    public function updateState( $state )
    {
        $this->_validateState( $state );
        $this->state = $state;
        $this->onStateChange();
    }

    /**
     * Triggers the state change callback.
     * @return void
     */
    public function onStateChange()
    {
        if ( $this->state === $this->default_state && !array_key_exists( $this->state,
                $this->valid_states ) )
        {
            // Null reset state. Do nothing.
            return;
        }
        $callback = $this->valid_states[$this->state];
        $this->$callback();
    }

    /**
     * Returns the current state identifier.
     *
     * @return null|string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Resets the state back to the default.
     *
     * If there is no callback assigned to the default nothing else occurs.
     * If there is, that callback will be called. This provides the
     * flexibility to reset without action or alternately call
     * a resetter method that clears the internals,
     * whichever use case is more appropriate to the implementation.
     * @return void
     */
    public function resetState()
    {
        $this->updateState( $this->default_state );
    }

    /**
     * Declares the default state. This is the state the object is set to
     * when `resetState` is called.
     * @return null|string
     */
    protected function declareDefaultState()
    {
        return null;
    }

    /**
     * Declares the valid states for the object.
     *
     * These must be an associative array, where the key is the state marker,
     * and the value is a valid callback from the implementing class.
     * This is checked when the state pattern initialization method is called,
     * and will result in an exception if not configured correctly to prevent
     * state based errors that are difficult to debug from occurring.
     * @return array
     */
    protected function declareValidStates()
    {
        return array();
    }

    /**
     * Initializes the state pattern. Call this in your constructor.
     * @return void
     */
    protected function initializeStatePattern()
    {
        $states = $this->declareValidStates();
        $this->_checkStateIntegrity( $states );
        $this->default_state = $this->declareDefaultState();
        $this->valid_states = $states;
        $this->resetState();
    }

    /**
     * Insures that the called state is a valid declaration.
     *
     * @param string $state
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     If a state is not valid. This is thrown when `updateState` fires,
     *     and prevents state change that would result in a missing key error.
     */
    private function _validateState( $state )
    {
        if ( $state === $this->default_state )
        {
            // The default state is always valid.
            return;
        }
        if ( !in_array( $state, array_keys( $this->valid_states ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Error encountered at [%1$s]. Provided state [%2$s] '
                . 'is not in the declared valid states [%3$s]',
                get_class( $this ), $state,
                implode( ', ', array_keys( $this->valid_states ) ) )
            );
        }
    }

    /**
     * Verifies the integrity of the callbacks bound to the state handles.
     *
     * @param array $states
     * @return void
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the implementing class has made an invalid callback declaration
     *     when it declares its state callbacks. This will result in a blocking
     *     exception to prevent runtime errors.
     * @internal
     */
    private function _checkStateIntegrity( $states )
    {
        foreach ( $states as
            $key =>
            $method )
        {
            if ( !is_callable( array(
                    $this,
                    $method ) ) )
            {
                throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
                sprintf( 'Class [%1$s] is misconfigured. Declared state [%2$s] does '
                    . 'not provide a valid callback in the current scope.',
                    get_class( $this ), $method )
                );
            }
        }
    }

}
