<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Debugger Trait
 *
 * Provides the capacity for classes to register debug values securely.
 *
 * This trait will silently discard debug data if debug mode is off,
 * so logging debug information can be done by classes without regard
 * to checking for debug information visibility individually.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
trait Debugger
{

    private static $debug_enabled = false;
    private static $debug_initialized = false;

    /**
     * A pointer to the context config, which is used to insure
     * that debug data is globally accessible.
     *
     * @var \mopsyd\sanctity\libs\config\ContextConfig
     */
    private static $debug_data;

    /**
     *
     */
    private static function _checkDebug()
    {
        self::_debugInit();
        return self::$debug_enabled;
    }

    /**
     *
     * @return \ReflectionClass
     */
    protected static function debugReflect()
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        if ( (isset( $this ) ) )
        {
            return new \ReflectionClass( $this );
        }
        return new \ReflectionClass( get_called_class() );
    }

    protected static function debugSnapshot()
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        $results = array();
        $trace = debug_backtrace();
        array_shift( $trace );
        $ref = self::debugReflect();
        $results['comment'] = $ref->getDocComment();
        $results['properties'] = !( isset( $this ) )
            ? '<< object not instantiated >>'
            : get_object_vars( $this );
        $results['constants'] = $ref->getConstants();
        $results['interfaces'] = $ref->getInterfaces();
        $results['methods'] = $ref->getMethods();
        $results['stack'] = $trace;
        $results['reflector'] = $ref;
        $results['source'] = (string) $ref;
        return $results;
    }

    protected static function debugTrace( $mode = 0, $print = false,
        $exit = false )
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        $trace = debug_backtrace( $mode );
        array_shift( $trace );
        if ( $print )
        {

        }
    }

    protected static function debugDump( $value, $print = true, $exit = false,
        $origin = null )
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        if ( is_null( $origin ) )
        {
            $trace = debug_backtrace( 1 );
            $origin = self::_getTraceOriginMarkup( $trace[1] );
        } else
        {
            $origin = self::_getTraceOriginMarkup( $origin );
        }
        if ( !$print )
        {
            ob_start();
        }
        if ( function_exists( 'd' ) )
        {
            d( $value );
        } else
        {
            echo '<div class="debug-dump"><pre><strong>Debug</strong><hr><br><code>' . PHP_EOL;
            var_dump( $value );
            echo '</code><br><hr></pre>' . $origin . '</div>' . PHP_EOL;
        }
        if ( !$print )
        {
            return ob_get_clean();
        }
        if ( $exit )
        {
            exit();
        }
    }

    protected static function debugStash( $value, $key = null,
        $context = 'default' )
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        $trace = debug_backtrace( 1 );
        $caller = $trace[1];
        $class = get_called_class();
        $debug_data = self::$debug_data->get( 'debug' );
        $data = $debug_data->get( $class );
        if ( is_null( $key ) )
        {
            $data[$class][$context][] = $value;
        } else
        {
            $data[$class][$context][$key][] = $value;
        }
        $debug_data[$class] = $data;
        self::$debug_data->set( 'debug', $debug_data );
    }

    protected static function debugGetAll()
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        return self::$debug_data->get( 'debug' );
    }

    protected static function debugGet( $key = null, $context = null )
    {
        self::_debugInit();
        if ( !self::_checkDebug() )
        {
            return;
        }
        $class = get_called_class();
        $data = self::$debug_data->get( 'debug' );
        $subject = $data->get( $class );
        if ( is_null( $key ) && is_null( $context ) )
        {
            return $subject;
        }
        if ( !array_key_exists( $context, $subject ) )
        {
            return false;
        }
        $subject = $subject[$context];
        if ( !is_null( $key ) && !array_key_exists( $key, $subject ) )
        {
            return false;
        }
        if ( !is_null( $key ) )
        {
            return $subject[$key];
        }
        return $subject;
    }

    private static function _debugInitClass()
    {
        $class = get_called_class();
        if ( !self::$debug_data->get( 'debug' )->has( $class ) )
        {
            $data = self::$debug_data->get( 'debug' );
            $data[$class] = array();
            self::$debug_data->set( 'debug', $data );
        }
    }

    /**
     * Initializes the debugger, if Wordpress debugging is enabled.
     */
    private static function _debugInit()
    {
        if ( !self::$debug_initialized )
        {
            self::$debug_data = new \mopsyd\sanctity\libs\config\ContextConfig();
            if ( !self::$debug_data->get( 'debug' ) )
            {
                $data = new \mopsyd\sanctity\libs\container\ContextContainer();
                self::$debug_data->set( 'debug', $data );
            }
            self::$debug_enabled = defined( 'WP_DEBUG' ) && WP_DEBUG;
            self::$debug_initialized = true;
        }
        self::_debugInitClass();
    }

    private static function _getTraceOriginMarkup( $trace )
    {
        $markup = '<div><hr><sub><strong>Point of Origin</strong></sub><br>' . PHP_EOL;
        $markup .= '<sub><strong>Method</strong> <pre>[%1$s::%2$s]</pre> at <strong>line</strong> <pre>[%3$s]</pre> of <strong>file</strong> <pre>[%4$s] </sub>' . PHP_EOL;
        $markup .= '</div>' . PHP_EOL;
        return sprintf( $markup, $trace['class'], $trace['function'],
            $trace['line'], $trace['file'] );
    }

}
