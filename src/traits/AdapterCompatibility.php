<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Adapter Compatibility
 *
 * Provides the foundational logic for interaction with the platform adapter.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
trait AdapterCompatibility
{

    private static $platform_adapter;

    /**
     * Sets the platform adapter, so it is available locally within the object.
     *
     * All objects that contain this trait can both receive the adapter
     * through dependency injection, and provide it to other objects
     * once it has been set.
     *
     * @param \mopsyd\sanctity\interfaces\adapters\platform\PlatformAdapterInterface $adapter
     */
    public static function setAdapter( \mopsyd\sanctity\interfaces\adapters\platform\PlatformAdapterInterface $adapter )
    {
        self::$platform_adapter = $adapter;
    }

    /**
     * Returns the stored platform adapter.
     * 
     * @return \mopsyd\sanctity\interfaces\adapters\platform\PlatformAdapterInterface
     */
    public static function getAdapter()
    {
        return self::$platform_adapter;
    }

}
