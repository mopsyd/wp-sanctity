<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace mopsyd\sanctity\traits;

/**
 * Container Packager Utility
 *
 * Wrap it up in a pretty bow and ship it.
 *
 * Provides automated containerization,
 * with an extension method to declare a
 * different container class if needed.
 *
 * @see \mopsyd\sanctity\libs\container\AbstractContainer
 * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
 * @see \mopsyd\sanctity\traits\ContainerUtility
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
trait ContainerPackagerUtility
{

    /**
     * Packages an array in an interable, countable, array-accessible, serializable,
     * json-serializable, Psr-11 compliant container object.
     *
     * These container objects can be used interchangeably as both an object and an array.
     *
     * @example $container['id'] === $container->id //evaluates to true
     *
     * @param array $subject The contents to package in a container.
     * @param int $count The limit to how large the container can be.
     *     The default is -1 (no limit). Any integer value greater than -1
     *     will prevent the container from accepting any more arguments
     *     than the defined limit.
     *     This limit is also set on recasts automatically, unless the limit
     *     is explicitly loosened programmatically on an active container instance.
     *
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     *
     * @see \mopsyd\sanctity\libs\container\AbstractContainer
     *     This is the provided base container, which you may extend
     *     for whatever purpose you need.
     *
     * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     *     This is the interface that all internals check for to insure that
     *     a container is valid at the basic level. Specialized containers
     *     are validated against futher interfaces, but all of those extend
     *     this one, and for general containerization purposes, this is the
     *     one that you need to comply with.
     *
     * @see \mopsyd\sanctity\traits\ContainerUtility
     *     This trait provides all of the functionality of the container.
     *     Any class can validate as a container by implementing the above
     *     interface and using this trait with no additional work, provided
     *     there are no method collisions. If you have your own class that
     *     needs to operate as a container, but it is not possible to extend
     *     from the provided abstract due to single inheritance considerations,
     *     then use this trait and the interface instead, and everything's gravy.
     *
     * @note The container class provides both a hook and a filter
     *     (internally, not through wordpress) that can be extended
     *     to automatically validate (through the hook)
     *     or filter contents (through the filter)
     *     at EVERY SINGLE STEP OF ITS INTERNALS.
     *     By default, none of these are activated, but if you have
     *     a special purpose container that needs to insure explicit data integrity,
     *     or an explicit output format, this is a simple answer that
     *     does not require filtering contents through an external class.
     *     This can be easily wired into the typical wordpress sanitization filters
     *     or escaping mechanisms to fully automate away data sanitization without
     *     additional work in operating classes,
     *     by just creating specialized containers meant for specific contents.
     */
    protected static function containerize( $subject = array(), $count = -1 )
    {
        $expected = 'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface';
        if ( is_object( $subject )
            && ( $subject instanceof $expected ) )
        {
            $subject = $subject->toArray();
        }
        if ( !is_array( $subject ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Invalid parameters passed in [%1$s]. '
                . 'Provided containerization subject must '
                . 'be an array or an instance of [%2$s]', get_class( $this ),
                $expected )
            );
        }
        $class = get_called_class();
        $container_class = $class::declareContainerClass();
        self::_validateContainerClass( $container_class );
        $instance = new $container_class( $count );
        return $instance->fromArray( $subject );
    }

    /**
     * Allows for a runtime determinable containerization strategy
     * without overriding the default container.
     *
     * If your class needs to be able to distribute multiple container types,
     * use this method in any context where the association needs to be
     * dynamically determined by logic outside this trait.
     *
     * Otherwise it functions identically to how `containerize` works,
     * except it takes an arbitrary container class name as its first parameter.
     *
     * @param string $container_class The name of any class
     *     that implements \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @param array $subject
     * @param int $count
     * @return \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityException
     *     As this method allows for a dynamically assigned class to be called
     *     at runtime, it throws a lower priority exception than the standard
     *     `containerize` method, however the conditions are identical
     *     (your provided class must be fully qualified and implement the container interface).
     */
    protected function containerizeInto( $container_class,
        array $subject = array(), $count = -1 )
    {
        $class = get_called_class();
        try
        {
            self::_validateContainerClass( $container_class );
        } catch ( \mopsyd\sanctity\libs\exception\SanctityBrokenClassException $e )
        {
            //This is not a broken class because its determined at runtime, not compile time.
            //
            //In keeping with the package philosophy
            //that `BrokenClassException`'s should never be caught,
            //this method must return a standard handleable exception instead.
            throw new \mopsyd\sanctity\libs\exception\SanctityException(
            sprintf( 'Provided container class [%1$s] is not a valid '
                . 'implementation of [%2$s] in [%3$s].', $class,
                'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface',
                get_class( $this ) )
            );
        }
        $instance = new $container_class( $count );
        return $instance->fromArray( $subject );
    }

    /**
     * This method returns the name of the container class distributed by this method.
     *
     * It may be overridden to use an alternate, specialized container
     * in any given class without additional work.
     *
     * The provided class must be an absolute classname, and it must be a class
     * that implements `mopsyd\sanctity\interfaces\libs\container\ContainerInterface`.
     *
     * @return string
     *
     * @see \mopsyd\sanctity\libs\container\AbstractContainer
     * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @see \mopsyd\sanctity\traits\ContainerUtility
     */
    protected static function declareContainerClass()
    {
        $class = get_called_class();
        if ( defined( $class . '::CONTAINER_CLASS' ) )
        {
            return $class::CONTAINER_CLASS;
        }
        return 'mopsyd\\sanctity\\libs\\container\\Container';
    }

    /**
     * Validates that the declared container class is a valid implementation of
     * the provided container interface, to enforce maximum interoperability.
     *
     * Any class can satisfy this requirement by doing one of the two following options:
     *
     * - Extend the provided abstract (see below).
     *     No additional method declarations are required.
     *
     * - Use the provided trait, and implement the provided interface
     *     on your own class (see below).
     *     No additional method declarations are required.
     *
     * This method may not be overridden, as the container class is integral
     * to the stable functionality of the system.
     *
     * @return void
     *
     * @throws \mopsyd\sanctity\libs\exception\SanctityBrokenClassException
     *     If the declared container does not implement the expected interface.
     * @see \mopsyd\sanctity\libs\container\AbstractContainer
     * @see \mopsyd\sanctity\interfaces\libs\container\ContainerInterface
     * @see \mopsyd\sanctity\traits\ContainerUtility
     * @internal
     * @final
     */
    final private static function _validateContainerClass( $container_class )
    {
        $class = get_called_class();
        $expected_interface = 'mopsyd\\sanctity\\interfaces\\libs\\container\\ContainerInterface';
        $container = $class::declareContainerClass();
        if ( !(class_exists( $container )
            && in_array( $expected_interface, class_implements( $container ) ) ) )
        {
            throw new \mopsyd\sanctity\libs\exception\SanctityBrokenClassException(
            sprintf( 'Class [%1$s] is misconfigured. Declared container class [%2$s] '
                . 'is not a valid implementation of [%3$s]. Please report this '
                . 'issue to the developer of [%1$s].', $class, $container,
                $expected_interface )
            );
        }
    }

}
