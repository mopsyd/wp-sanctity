<?php
/**
 * Theme Name: Sanctity
 * Theme URI: https://mopsyd.com/projects/wordpress/sanctity
 * Author: Brian Dayhoff
 * Author URI: https://mopsyd.com/
 * Description: Sanctity is an unopinionated, fully responsive, MVC driven theme engine powered by Bootstrap 4 for Wordpress, that puts the site owner in full control of their output display without making any decisions for them about how their site should look. This theme was written entirely from the ground up with best practices for Wordpress, modern PHP, modern Javascript, and modern CSS in mind, and implements all of these in its expression explicitly. Also it's free, fast, flexible, secure, scalable, fully integrated with standard Wordpress functionality, and very easy to extend however you want to. This theme represents the official implementation of this framework.
 * Version: 0.2.0
 * License: MIT
 * License URI: https://opensource.org/licenses/MIT
 * Tags: bootstrap, bootstrap 4, responsive, optimized, mvc, twig, minimalist, clean, base template, modern, object-oriented
 * Text Domain: sanctity
 *
 * Third party plugins that hijack the theme will call wp_head() to get the header template.
 * We use this to start our output buffer and render into the view/page-plugin.twig template in footer.php
 *
 * If you're not using a plugin that requries this behavior (ones that do include Events Calendar Pro and
 * WooCommerce) you can delete this file and footer.php
 *
 * @package  WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
if (extension_loaded('zlib'))
{
    //Use compression if available
    ob_start('gz_handler');
} else {
    //If the zlib compression library is not available,
    //use an uncompressed buffer.
    //@todo notify Wordpress Integrity if it is installed that zlib was not detected
    ob_start();
}
\Sanctity::renderPart( 'header', $data );