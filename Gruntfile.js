module.exports = function (grunt) {

    // Load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Configure
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        build: '<%= new Date().getTime() %>',
        product: '<%= _.capitalize(pkg.name) %>',
        copyright: 'Copyright (c) <%= grunt.template.today("yyyy") %> Brian Dayhoff <mopsyd@me.com>.',
        // Tasks
        copy: {
            build: {
                files: [
                    {
                        src: ["node_modules/webfontloader/**"], 
                        dest: "dist/vendor/webfontloader/"
                    },
                    {
                        src: ["node_modules/mdbootstrap/**"], 
                        dest: "dist/vendor/mdbootstrap/"
                    },
                    {
                        src: ["etc/source/media/img/*"], 
                        dest: "dist/media/img/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js",
                        src: "*.js",
                        dest: "dist/js/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/frontend",
                        src: "**",
                        dest: "dist/js/frontend/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/backend",
                        src: "**",
                        dest: "dist/js/backend/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/admin/settings",
                        src: "**",
                        dest: "dist/js/admin/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/admin",
                        src: "*.js",
                        dest: "dist/js/admin/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/login",
                        src: "**",
                        dest: "dist/js/login/"
                    },
                    {
                        expand: true,
                        cwd: "etc/source/js/modules",
                        src: "**",
                        dest: "dist/js/modules/"
                    },
                ]
            }
        },
        sass: {
            build: {
                options: {
                    sourcemap: true
                },
                files: [{
                        expand: true,
                        cwd: 'etc/source/sass/',
                        src: ['**/*.scss'],
                        dest: 'dist/css',
                        ext: '.css'
                    }]
            }
        },
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['last 2 versions']
                    })
                ]
            },
            dist: {
                frontend: {
                    src: 'dist/css/frontend.css'
                },
                backend: {
                    src: 'dist/css/backend.css'
                },
                login: {
                    src: 'dist/css/login.css'
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                        expand: true,
                        cwd: 'dist/css',
                        src: ['*.css', '!*.min.css'],
                        dest: 'dist/css',
                        ext: '.min.css'
                    }]
            }
        },
        concat: {
            build: {
                options: {
                    // define a string to put between each file in the concatenated output
                    separator: ';'
                },
                dist: {
                    // the files to concatenate
                    src: ['etc/source/js/**/*.js'],
                    // the location of the resulting JS file
                    dest: 'dist/js'
                }
            }
        },
        uglify: {
            options: {
                sourcemap: true,
                banner: '/*! <%= _.capitalize(pkg.name) %> Copyright (c) <%= grunt.template.today("yyyy") %> Brian Dayhoff <mopsyd@me.com>. Released under the <% pkg.license %> license. */',
            },
            build: {
                files: [
                    grunt.file.expandMapping([
                    'etc/source/js/*.js'
                ], 'dist/js/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                }),
                    grunt.file.expandMapping([
                    'etc/source/js/frontend/**/*.js'
                ], 'dist/js/frontend/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                }),
                grunt.file.expandMapping([
                    'etc/source/js/backend/**/*.js'
                ], 'dist/js/backend/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                }),
                grunt.file.expandMapping([
                    'etc/source/js/login/**/*.js'
                ], 'dist/js/login/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                }),
                grunt.file.expandMapping([
                    'etc/source/js/modules/**/*.js'
                ], 'dist/js/modules/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                }),
                grunt.file.expandMapping([
                    'etc/source/js/admin/**/*.js'
                ], 'dist/js/admin/', {
                    flatten: true,
                    rename: function (destBase, destPath) {
                        return destBase + destPath.replace('.js', '.min.js');
                    }
                })
            ]
            },
            test: {}
        },
        jshint: {
            // define the files to lint
            files: ['Gruntfile.js', 'etc/source/js/**/*.js', 'etc/test/**/*.js'],
            // configure JSHint (documented at http://www.jshint.com/docs/)
            options: {
                // more options here if you want to override JSHint defaults
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },
        qunit: {
            files: ['etc/test/**/*.html']
        },
        watch: {
            css: {
                files: 'etc/source/sass/**/*.scss',
                tasks: ['sass:build', 'postcss', 'cssmin']
            },
            js: {
                files: 'etc/source/js/**/*.js',
                tasks: ['jshint', 'qunit', 'concat:build', 'uglify:build']
            }
        }
    });

    // Register Grunt tasks
    grunt.registerTask('test', [
        'jshint',
        'qunit'
    ]);
    grunt.registerTask('build', [
        'copy:build', 
        'sass:build', 
        'postcss', 
        'cssmin', 
        'concat:build', 
        'uglify:build'
    ]);
    grunt.registerTask('default', [
        'watch'
    ]);
};
