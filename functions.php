<?php
/**
 * Theme Name: Sanctity
 * Theme URI: https://mopsyd.com/projects/wordpress/sanctity
 * Author: Brian Dayhoff
 * Author URI: https://mopsyd.com/
 * Description: Sanctity is an unopinionated, fully responsive, MVC driven theme engine powered by Bootstrap 4 for Wordpress, that puts the site owner in full control of their output display without making any decisions for them about how their site should look. This theme was written entirely from the ground up with best practices for Wordpress, modern PHP, modern Javascript, and modern CSS in mind, and implements all of these in its expression explicitly. Also it's free, fast, flexible, secure, scalable, fully integrated with standard Wordpress functionality, and very easy to extend however you want to. This theme represents the official implementation of this framework.
 * Version: 0.2.0
 * License: MIT
 * License URI: https://opensource.org/licenses/MIT
 * Tags: bootstrap, bootstrap 4, responsive, optimized, mvc, twig, minimalist, clean, base template, modern, object-oriented
 * Text Domain: sanctity
 *
 * @package WordPress
 * @subpackage  mopsyd/wp-sanctity
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @category wordpress
 * @subcategory themes
 * @version 0.2.0
 * @since 0.1.0
 */
if ( !defined( 'ABSPATH' ) && !getenv('CI') )
{
    die();
}
if ( !defined( 'SANCTITY_BASEDIR' ) )
{
    //Set the base directory constant.
    define( 'SANCTITY_BASEDIR', getenv('CI_BASEDIR') ?:
        __DIR__ . DIRECTORY_SEPARATOR );
    //Run the preflight, declarations, autoloader, etc.
    require_once SANCTITY_BASEDIR . 'etc/setup.php';
}
//Off we go.
new \Sanctity();
/**
 * We now return to your regularly scheduled Wordpressery.
 */