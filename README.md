
# Sanctity

Sanctity is a fully responsive, object-oriented presentation layer for Wordpress
based upon Bootstrap 4 and Twig, with a very solid frontend and backend representation.
It is meant to act as an independent presentation layer that provides a standardized
object oriented approach to theme development, while abstracting all of the Wordpress
internals into a dedicated adapter that conforms to Wordpress best practices,
thereby decoupling the direct logic from the usage of the platform.

This project will shortly be extended to also allow plugin development using it
as an underlying engine, but for the time being it only supports theming.

It does however support driving theme logic from an external system instead of
using just Wordpress internals which means you can use it to push presentation
logic from a broader Laravel, Symphony, etc application layer, and have it work
seamlessly with Wordpress internals without having to fiddle further with the
Wordpress api. So there's that.


## About Sanctity

This project uses a spin on MVC that I refer to as MVAC *(Model, View, Adapter, Controller)*,
which is pretty much a take on MVC designed with interoperability in mind,
by using all direct interaction with the underlying platform through the *Platform Adapter*,
and leaving the MVC components of the build as directors of logic to forward to the adapter,
or aggregators of logic retrieved from it. The external integration operates
through the MVC aspect, and the MVC classes direct all platform specific details
to the *Platform Adapter*, which has a standardized interface for enacting operations.

*TLDR;* That means you get to leverage the platform without actually knowing the platform.
So if you rock at PHP, javascript, CSS, or what have you and are clueless about WordPress,
you don't actually have to learn it to do your thing effectively.

### What It Does

Sanctity allows you to use a single consistent Api to represent the presentation layer of a platform,
with the goal of providing a uniform approach to presentation across platforms as development matures.
It is meant to allow a uniform approach to making an underlying content management system
or application look and feel as intended in its outward representation, and facilitate a standard
for creating stylistic and UI components for more opinionated underlying platforms in a uniform way,
so that developers of frontend or presentation layer logic can more freely move between platforms
without the burden of re-learning the ins and outs of a specific platform.

It is an abstraction layer that separates the specifics of the underlying platform
from the design process first and foremost, with the goal of being able to be used
interchangeably across multiple platforms through one interface, allowing people
who use it as a design or development tool to work on platforms they are not
familiar with, similar to how Bootstrap abstracts away the nitty-gritty aspects
of CSS so you can focus on layout, or how jQuery abstracts out the nitty-gritty
aspects of javascript so you can focus on your own application specific frontend logic.
This does mostly the same thing for an established web system, so you can lay out
how you want it to look and feel without regard to its internals or existing api,
and mostly stick to the design tools you are familiar with without having to dive
too deep into backend logic or spend hours on Google and Stack Overflow looking up
how to do this or that with any specific PHP-based platform.

**Note:** This is the end goal of this project. As this is a pretty enormous undertaking,
development as a fully functional Wordpress theme is proof of concept for this approach.
When the Wordpress logic has been fully separated into the Platform Adapter (it mostly is already),
and a consistent interface for the adapter is finalized, then integration will commence
for other platforms via development of dedicated platform adapters for those systems
(eg: Magento, Joomla, Drupal, PHPBB, WikiMedia, etc).
As of now, it does nearly all of the things that a Wordpress theme should be expected
to fulfill and is nearly fully compliant with the official Wordpress theme review guidelines,
with very minimal implementation of Wordpress specifics outside of the Adapter layer.

*NOTE* This thing does way too much stuff to be accepted directly into the WordPress repo,
despite following their best practices. The major breaking point is that themes are expected
to only do it "The WordPress Way", and we don't roll that way. We roll the modern, interoperable,
object-oriented, portable, unit-tested software way. That doesn't explicitly fly with the
higher ups at WordPress, who still support PHP 5.3 think namespaces are icky, globals are good,
and numerous other really, really bad practices that this package does not condone,
but works around for the sake of making WordPress work as expected on it.
Particularly, the notion of allowing a completely separate system to integrate
seamlessly with the WordPress frontend is not ever going to fly with the
official WordPress repo, and doing so is quite a lot more than a plugin is
going to effectively handle by itself. Despite this, the WordPress standards are
pretty much followed explicitly, except where they are inferior to better and more
accepted security, interoperability, performance, or scalability standards,
in which case those are preferred.

The underlying MVC engine is primarily designed to defer to the adapter to determine
an order of events and how to check for nuance or extension and then apply standardized
logic to execute the final output via the Render Adapter, which is the layer that
actually prints your presentation. All of these parts remain decoupled as much as
is possible to insure their eventual portability, with the end goal of having the
core remain a stateless driver for an adapter that translates the specifics of the
platform it is running on into one external interface that works across systems.


### What It Doesn't Do

Sanctity at its core is not opinionated. It does not make decisions about what output
should consist of, and does not apply much of a *you have to do it my way* approach
to anything in its external api. It follows a policy of opinionless presentation,
and needs to be told what to do, or it doesn't do it. This is not an out-of-the-box
polished theme. This is a blank canvas for a rich responsive experience with the
capacity to leverage current industry standard design tools to whatever effect
you want in a way that that maintains portability across platforms through a
single programmatic approach **so you can easily build the final polished theme
without worrying about how to make it work on the underlying platform**.
You still have to tell it to show what you want though. You are, however,
very well armed to do so, and can turn the barebones out-of-the-box look
into a polished parallaxy responsive layout in about ten minutes.

Sanctity is not a drop in solution to not having to hire a designer or frontend guy.
However, it will present tools that will allow your designer and/or frontend guy to
maximize their productivity and not trip up over more technical considerations,
and it will provide as maximized integration through its Adapter layer for the
supported options of any given platform about how to adjust their presentation layer,
so that you can configure it as much as is possible through the native api of that
platform if you are not a programmer and just want a means to make your existing
system do all of the things as far as presentation is concerned. Note that if
your platform does not support *the thing*, then you still need to hire a designer
and/or developer to integrate *the thing*.

### How Does It Work?

There are *eight primary class types* in this project:

- `Controllers` make all of the decisions and do none of the work.
    They delegate anything that requires thinking to a model or a library.
    The first class that loads, which is primarily what you interact with is the `FrontController`.
    This then loads a `SubController` that does the lions share of the logic
    regarding the page presentation. Both of these are controllers though.
- `Models` do all of the logic, do not make decisions, and do not do work.
    Anything that requires utilizing the platform gets handed off to the `PlatformAdapter`.
    Anything that requires non-platform oriented work gets handed off to a `Library`.
    Anything that gets returned gets stuffed into a `Container`.
- `Adapters` do all of the interaction with any external component that presents
    any complexity that requires more than one class to account for
    and is not strictly a PHP construct. If it can be handled in one class (like PDO),
    its probably a `Library`. If it needs a bunch of classes, it's an `Adapter`.
    Interaction with the underlying platform occurs through the `PlatformAdapter`.
    Interaction with the templating engine occurs through the `RenderAdapter`.
    They are the translation layer for interacting with some external component,
    and only that.
- `Libraries` do all of the work that does not require an external component,
    or is straightforward enough to be encapsulated in a single class.
    They make none of the decisions and do none of the logic.
    They are just utilities to get things done, and don't much care why
    you asked them to do it.
- `Views` make none of the decisions and perform none of the logic.
    They handle all of the things that shows up in the client without exception,
    and do nothing else without exception.
    Basically if it is visible in what the client gets at all, either directly or
    through inspecting the page, it's the View's problem to make sure it got there right.
    In contrast to some other systems, this also includes cookies and headers.
    However, the View does not **decide** what they should contain *(decisions are controller territory)*,
    it just makes sure what it got turns into them if need be, that the
    status codes/headers are correct for whatever is getting handed off
    to the `RenderAdapter` to print, etc. It then throws whatever it has left
    at the `RenderAdapter`, which outputs the actual printed content.
- `Containers` contain stuff. They are just packages of data that got returned.
    All of the `Containers` in this system are inherently built on the Psr-11 spec,
    though they are not explicitly dependency injection containers per-se
    (though those are also included), and also include data containers.
    By default, they are iterable, serializable, json-encodable, can be
    interchangeably used like an object or an array without any distinction,
    and can be configured to automatically filter output or alias keys when
    retrieval commands are passed to them. They are a subset of Psr-11 containers
    that are hyper-charged with functionality to make them as flexible as possible.
    However they do not do work and do not make decisions or perform logic.
    They just get stuffed full of things and return it in a manner congruent
    with their scope, which may include applying filters or such,
    but otherwise they are just boxes you stuff things in.
    `Collections` are a subset of containers that only contain other
    containers, and should be considered to be composite when encountered,
    but they themselves are also just containers.
- `Components` represent a significant chunk of page content, and are tasked
    with providing information about how to insure that chunk of the page
    is properly represented.
    They are expected to be able to provide the scripts, styles, column count for
    responsive layout grid, css classes to throw at the render engine to make their
    outer and inner elements look right, any data that has to be thrown to the
    frontend to enact with their scripts, and whatever else is required to
    generally make them show up seamlessly. They are only concerned with their
    own display and are otherwise ignorant of all other details of the system,
    and generally are externally told what to be populated with whenever
    it is feasible to do so.
- `Extensions` extend Sanctity. They provide one or more of the aforementioned types,
    and designate when and where to use them. Extensions are *strictly opt-in* for
    the end user running the site, and must be enabled prior to doing anything.
    Extensions can represent *a plugin that wants to utilize Sanctity's underlying api*,
    an *external system that wants to feed data from outside the platform into it,
    so that it is interpreted as if it were native*, a *standard PHP library that
    extends this package*, or any other construct that wants to work with this
    codebase without directly extending its internals for any reason.
    Note that *opt in for an Extension is separate from turning a plugin on*.
    Enabling a plugin does not automatically enable its extensions for Sanctity.
    Those have to be separately flipped on. Otherwise the plugins functionality
    remains segregated from how Sanctity works, but they can both still run on
    their own at the same time and do their own thing. Sanctity is written with
    the philosophy of *zero assumptions about what the user wants*, and enforces
    this in its api. If the user wants your extension turned on, they will turn it on.
    Don't attempt to force it or your extension will get blacklisted. Likewise,
    the `PlatformAdapter` is leveraged to facilitate this by whatever means the
    existing platform it represents designates as appropriate, which means that
    if there is any native option on that specific platform to turn the extension
    on and off, it will be off by default until turned on through the platforms
    normal settings api for end users.


### Project Goals

Below are the primary project goals for this system, and represent the way
that its ongoing development should be expected to proceed.


#### Full Separation of Concerns

The underlying goal of this is to create a layer of middleware that presents
a single consistent outward api and remains portable between common platforms,
provided a *Platform Adapter* has been written for any given platform, or a *Render Adapter*
has been written for any given templating engine, with neither having any direct
correlation or coupling with the other.

Wordpress is the testbed for this approach, as it currently constitutes
about 22% of all websites currently hosted on the internet, and presents
an opportunity to test this approach on a widely used platform that has
a famously difficult API to tame.

Full separation of concerns means that chunks of logic are not mixed across the board,
throughout every possible aspect of this project that it is realistically
feasible to separate them (this is actually really hard sometimes working
with WordPress spaghetti-code, and api functions that were only ever designed
to print stuff and don't provide a means to get the data without echoing it
in a final filtered format). There are handful of places where it is required
to parse opinionated output as XML and strip the data that should have been
provided separately back out of output, or hit up the database directly when
there is no actual method to get something that isn't buried in a ten page
internal method that isn't documented or accessible in neat little parts.
This means emulating the "no-opinion" part when there are definitely
piles of opinion, and only piles of opinion otherwise available.
Due to this, a really good static caching mechanism was put in place to
drastically reduce the overhead of repeated operations of this sort,
so they always get done only one time per runtime as long as no
underlying data changed.

The presentation layer is tasked with presenting and only with presenting,
which is what this system is for. The templating engine is tasked with
printing content and only that, which is what the `RenderAdapter` is for.
The platform itself is tasked with running its own system how it was built to do,
and interaction with that for the sake of presentation is done explicitly through
the `PlatformAdapter` to completely separate the platform api from any decisions
about presentation as much as is possible. Likewise, the Javascript api should
be usable without regard to the backend and vice versa, and the backend api
should be usable without regard to the platform or render engine.

Likewise, when the `PlatformAdapter` api is finalized, it should ideally also
work as a standalone component that does not require the rest of Sanctity to run,
so that it can be leveraged for whatever purpose to control the interaction with
the platform it represents, regardless of whether that entails presentation or not.
Development of the current Wordpress adapter is taken from this approach, where it
encompases a standardized approach to utilizing all of the api components of
Wordpress without having to actually learn how to use Wordpress functions.
It will be broken off into a separate repo when it is completed.


#### Standards Driven Api

Sanctity's primary goal is to present an outward API that follows all
existing accepted standards for modern PHP development,
conforms to Psr standards explicitly, and presents an easy to use
outward representation that remains portable (or potentially portable) across platforms.
It is intended to act as a wrapper for an existing opinionated system,
and allow it to be utilized externally as a *Subroutine* of a larger system,
or present a consistently easy means of developing complex logic on a single platform
in a way that remains maintainable over time.

Presently, the underlying distribution of data is strictly done with Psr-11 containers,
classes are loaded through Psr-4 (with or without composer support, either or),
and further support for Psr-3, Psr-7, Psr-13, etc is planned, but not yet implemented.
Support for the A+/Promise api is also planned as the frontend developement matures.


#### Uniformity Between the Server and Client

This means that regardless of whether you are developing on the frontend or backend with this system,
it should pretty much *feel* the same, nuances of PHP vs Javascript considered.

Sanctity is configured by default to set DNS prefetch tags for all offsite scripts
registered through it automatically, and will also be presenting a module to pre-authorize
all scripts registered through it with cross-origin rights automatically, when it gets
to the point to do the HTTP header module. The purpose of this is to not have to worry
much about whether you are presenting your frontend data securely, and allow security
to be automatically handled without having holes, missing this or that, or otherwise
being a pain to manage. This includes mixed content warnings, cross-origin errors,
dns-prefetch, and numerous other headaches tracking down that one thing that just
doesn't seem to want to show up and you can't figure out why.

Sanctity presents an interface for easily binding frontend components to backend logic,
and automatically piping data dedicated to a specific page component to the frontend,
where it can be immediately consumed via a corresponding Javascript module.
This approach does not require a script to *phone home* to obtain its baseline data,
it is printed directly into source with the page, and distributed immediately
to any scripts bound to it. This allows frontend scripts to remain decoupled
from any backend concerns, fully removes the need to print inline source
for controlling frontend logic with dynamic values printed into them,
and allows for expected data to be present immediately without any need
to worry about cross-origin considerations, visibility of the current user,
or other such concerns, which is already handled appropriately on the backend.
This also means you can keep cookies securely inaccessible to Javascript,
and don't need to set headers to securely transmit data to the frontend either.

You do not need the Wordpress REST api to do this. Sanctity provides its own
REST api out of the box that is more robust than the one you get from the plugin.
Also it doesn't require any plugins to use, but it does require that your requests
come from the same origin host for security purposes (you can write an extension
to loosen this constraint if you want), send correct nonces and content type headers,
and you know, normal web security stuff. This is also significantly faster than the
AdminAjax approach, as it skips all of the redundant loading, validates using normal
web standards and doesn't try to guess what almost good enough means with a ton of
filters (it's either 100% right and authorizes correctly or get lost),
and gets straight to the point getting you back what you want, even if the
wordpress runtime is not far enough along to get it normally. It will just hit
the database with PDO on its own if it has to wait, so that way you don't have to wait.
It does however check access rights for users using the normal WordPress role system
for any requests that are not explicitly public. Data validation is done with an
"expected request payload only" approach rather than a "filter it until it's good enough"
approach like wordpress would normally do. In most cases, Sanctity already knows what
acceptable answers are for any given REST request, who is allowed to make them,
and what format they should be in, and if it's not explicitly on the list exactly
the way the list says it ought to be, get lost.

This lowers the overhead of developing a strong frontend system, by removing the
need to develop a parallel rest api for simple operations that need variant data,
but do not need to make repeated calls back to the server, and also presents the
opportunity to avoid the overhead of repeated authorization calls, as auth tokens
can be injected into scripts directly instead of requiring another round trip
to the server to obtain, while still presenting the means to avoid CSRF and insure
that sensitive data is only present for an authenticated user in their own scope.
It also means that you can cache ALL OF YOUR SCRIPTS in a cdn or static cache,
and still have them get what they need without issue. This is more performant
than using a library like Backbone to build collections, as it does not require
round trips back to the server to collect data which thereby lessens the strain
on your server for handling multiple concurrent requests for a single page load.
It also does not exclude using a tool to interact with the server via a REST api,
so you can easily do both if you wish.

The Sanctity frontend Javascript component is tasked with consuming and distributing this data,
which is set in the window object immediately prior to the Sanctity javascript class,
and is immediately fetched and wiped from the global scope thereafter,
and encapsulated in an enclosed javascript variable in the Sanctity build factory closure
which is not outwardly accessible. The system is configured to insure that the
metadata is **always** included in source immediately prior to the Sanctity script class,
so that it remains in the DOM only long enough for the underlying handler to grab
it and remove it. Any other component that requires access to this data must authorize
itself through the provided auth module, which has a subset of allowed scripts that
was already provided on the backend, to insure that sensitive credentials are not
accidentally leaked to inappropriate scripts. Scripts may then present their dataset
to the rest of the scripts on the page however they deem neccessary, which is how you
should approach feeding this data to any included scripts not managed by Sanctity directly.

This can be easily used to package a script with API endpoints,
so that it only calls what is appropriate for its purpose,
or may be used to set data directly and remove the need
for api calls entirely. Either or.


#### Strictly Unopinionated Presentation

This theme provides a very simple clean layout with appropriate native hook bindings,
and does not make too any assumptions about your website other than that, but it provides
a very robust and flexible api for extension and integration. Both frontend and backend
devs should enjoy working with it, and fullstack devs should find the frontend and backend experience
nearly identical in its representation. The frontend javascript is loosely class based,
and the backend PHP is loosely prototypical to facilitate this,
so that both have a similar representation despite the inherent differences between
Javascript and PHP.
Both were written entirely from scratch to facilitate this parallelistic feel,
with no external dependencies or 3rd party libraries included on either end
that would detract from this.

The default presentation of the page presents only a barebones,
strictly responsive wrapper for the content, and does not apply
any opinion about how that data should be presented other than
that it should be mobile ready, align correctly on the page,
and present a blank slate for further design and development.
The only opinions about presentation that are made by default
are the following:

- The menus need to work on both mobile display as well as desktop.
    This includes support for multi-level dropdowns, which Bootstrap
    does not natively support.
- The content expected to be on the page will be on the page, as determined by the platform.
    In keeping with the "zero opinion" policy, it is not appropriate to tell the platform
    not to work the way it expects to work without being programmatically directed to do so,
    or configured to do so by an end user.
- The system separates *layout* and *style* by default in frontend render.
    This lets you fiddle with either without disrupting the other.
- Frontend libraries that apply unwarranted styles that make the page not look
    like a blank canvas out of the box for you to do whatever you want with are removed.
    A means to put them back is provided when this is neccessary.
- Representation of the markup follows the best practices for CSS styling,
    where *all attributes are assigned to classes*, and then *classes are assigned to the DOM*.
    By default, there is little or no inline styling presented, and little or no styling affixed
    to an explicit ID, with the exception of a small number of edge cases where this is unavoidable
    due to the need to correct pre-existing non-compliant styles from other utilities.
- Some inconsistencies between Bootstrap responsive breakpoints and WordPress responsive breakpoints
    are smoothed out to favor the BootStrap ones. A number of other WordPress design hiccups are
    also corrected, like the chronic issue of the admin bar overlapping sticky headers.
- The only cases where CSS is applied directly with Javascript are the cases where CSS has to
    be overridden that is already applied by other Javascript, making it impossible to do
    with a stylesheet, and only for the purpose of returning the content to a blank canvas state.
- This system's frontend is written in ES6, and will not have backwards-compatible
    scripting put into place for any browsers older than IE 10. The 11% of people
    who don't have an ES6 compatible browser can click the upgrade button or not use it.


#### Best Practices are Rigidly Enforced

Sanctity does not allow best practices to be optional.
There is zero tolerance for ambiguity unless a programmatic means of resolving
it into a definite intent is explicitly presented. There is no guessing on content
submitted, there is no "I think I can figure out what you meant". It's right or
it's not. If it's not, bounce.

It treats both its own api and the underlying Api of the platform it is running
on in this manner.

##### Sanctity-Specific best practices

Sanctity's internals are driven by a strict interface-enforced object relationship,
where the internals will *only* accept an object as a parameter that bears the
correct interface, but will *always* accept any object that bears the correct
interface in all cases that are allowed to be extended. Sanctity only verifies objects
by interface unless it is evaluating a platform construct that doesn't use interfaces.
In the majority of these cases, an extension object is used in place of the native one
that does have an interface whenever possible.

Methods that do not require an explicit class or type directly in their parameter declaration
will rigorously typecheck the provided arguments before doing any logic.
This is often used interchangeably, as some things require explicit results (statically typed),
and other things may take numerous similar results (such as anything that is a container OR an array,
or "give me an id, user object, or container representing a user, any of those is good enough"),
in which case all possible matches will be checked in a duck-typed style.
This latter approach is more prevalent in the public api (to provide additional
flexibility in standard useage), and the former is more present
in the internal api (to enforce data integrity).

To prevent the need to make a billion little factories for every little thing,
the constructor for Sanctity objects has been standardized to accept two parameters,
where the first is `dependencies`, which is an optional dependency, dependency container,
or array of dependencies, and the second is `args`, which is an array of arguments to pass
into the constructor. These are both optional, and require the object itself to throw
an exception from its constructor if it requires one or both and they are not present.
Dependency injection is generally done through dedicated methods statically into base classes,
so that all child classes have the required dependency on hand immediately
upon instantiation through standardized getter methods.
Most dependency injection only occurs at the lower level of architectural logic,
and is otherwise transparent at the leaf level, as the api is standardized enough
that most logic follows an orderly list of expected operations based on defined
interfaces regardless of the actual implementor of that interface.

Sanctity has a *distinct separation between read and write operations*.
A **read** operation will **always** return a *container object* that does not
provide any methods to alter the data directly.
Whereas Wordpress (and numerous other platforms) distribute objects that are
capable of both **read** *and* **write**, Sanctity *only returns data containers*
on **read**, and requires a separate call to enact changes. This prevents accidental
state mutatation, because you have to explicitly call a write to get one,
instead of iterating over an object and then "whoops, I deleted my database".

##### Platform-Specific best practices

All known ambiguity sent to the platform that would generate
an error is blocked with an exception, and not allowed to proceed, even if the
underlying platform would take it and just not do it right.
Deprecated platform features are not supported beyond the
version they were deprecated in.

## Wordpress Features

Sanctity provides both an MVC frontend and backend. They are both home grown,
and not based on any existing backend (Symfony, Laravel, etc), or frontend
(Angular, etc), however they are both solid and easy to integrate with.
This package aims to have as few actual dependencies as possible to minimize
the potential for breakage and constant bug fixes, so it's only real dependencies
are Twig and Wordpress itself. It's sole frontend dependencies are jQuery,
from which the root level Sanctity frontend controller inherits its prototype,
FontAwesome for a handful of icon displays in the settings page (not critical if it's missing),
and Bootstrap 4 (of course, as the package is based on that).

These components are preconfigured to speak with each other seamlessly,
which allows data registered in the backend to be automatically consumed
and present in the frontend with no additional work. Nonces, frontend
component settings, and what have you can all be easily registered into
the frontend object with little fuss.

Both the frontend and the admin area can be templated with Twig, with Bootstrap goodness available for both.
By default, Sanctity only templates *the settings page* of the admin,
but you can *easily prototype an admin theme* by extending on this.
The `AbstractAdminController` provides placeholders for exactly that (which do nothing by default),
but all fire in the same order as the frontend (with regard to the nuances of the backend
render api of the platform), so you can style your backend by just overriding
those and providing an alternate controller. 

The *login page* also has a dedicated placeholder controller for this exact same purpose,
although it also does nothing by default.

Consistent site branding (logo, color scheme, typography, etc) can be applied through both
the frontend and the backend seamlessly. The backend display can be configured or disabled
entirely if you wish as well (it's off by default, this system does not make assumptions about your website).

All dependencies are preconfigured to be drawn locally by default, but support swapping out official CDN's with a redundancy fallback to local sources if the CDN is not available for some reason. Additional CDN's can be registered in the theme settings, and can also be set as replacements for existing scripts, even from other packages if required (provided the user has correct authentication rights to make such a request).

All dependencies are distributed precompiled, minified, and gz compression is enabled by default if it is available on your system for all source rendered. This is non-blocking if it is not available. By default, uncompressed source is served. Minified source can be toggled programatically or through the admin area settings.

The system grants you the option to create and display custom error pages based on a
static snapshot of your own existing page, which can be used in the event your site is down,
and also provides the means to produce dynamically generated error pages.
This covers both cases where handleable errors like a 404 page occur, as well
as serious site issues like a 500 error crashing the page because the database
is unreachable. Both of these are covered. Dynamic error pages can be styled like
any other page, cover 404's by default, and can optionally also cover 401, 403, and 500 errors.

Static error pages fire in a shutdown function, and serve a static html page that is
compiled with sprintf hooks to dynamically inject content into specific areas.
These will be displayed even if Wordpress would normally whitescreen (as long as the
shutdown process does not also throw an error prior to display). In the event this
happens, all render falls back to the provided MVC render engine, so buggy plugins
and incompatible updates will not interfere with display. This works even if your database
is down. If your platform crashes *before the theme even loads*,
you will still get a whitescreen.
If *some other code registered a shutdown process that also throws an error
prior to the theme registering its own*, you will still get a whitescreen.
In all other cases, you will at least get a static error page. If debug mode is enabled,
the backtrace for the error that killed the page will also be displayed.
This is configurable to also check and only display by user permission.
Although the user permissions are not available after shutdown normally,
a static snapshot of user authorization is obtained as early in runtime as possible,
so this data has already been collected in most cases by the time an error occurs.

## Requirements

- PHP 7.0+ (it might run on 5.6, but development is targeting 7.0
    in order to leverage Twig 2 without issue). Current tests work well on 5.6,
    but supporting 5.6 is not a project goal, and the first blocking issue that
    cannot be resolved on 5.6 will result in all further versions immediately
    requiring 7.0 explicitly. It also is not running its Unit testing on a 5.6 build
    (as it is targeting 7.0), so support for issues related to running 5.6 should not
    be expected to be a priority.
- mb_string (it will preflight if this is not installed and display a default page
    that informs you of the issue, but you will have to disable the theme if it is not present).
    As the current build only provides Twig as a render engine, it requires mb_string,
    which is a dependency of Twig. As abstraction of a vanilla php render type is developed,
    this requirement should be expected to be removed in a future release.
    Most servers already have this available, but it bears mentioning because
    occasionally it is missing.

## Installing the Theme

There is a bash build script provided to run a full build. You need at least Node.js and NPM locally to run it.
It will pull composer, run it and trash it afterwards if you don't have that, or use your local copy if you do.

- `git clone https://gitlab.com/mopsyd/wp-sanctity.git`
- `cd path/to/sanctity`
- `cd etc/bin`
- `chmod +x build.sh`
- `build.sh`
- profit.

Note that this is not the finalized repo structure. The repo is currently laid out for active development, and the current file structure is going to wind up getting packed into a `src` directory shortly, but this will build everything out in its current state just fine, and also fix all of the directory permissions and such for you.


### Building from Composer

(nope, not yet, but very soon)


### Plugin Integrations

Currently it supports some features from Jetpack, Askimet Offload S3,
WordFence, Yoast, Smush, Hummingbird, and a number of other commonplace plugins,
and will eventually support all official Wordpress plugins explicitly,
and most extremely common and popular other plugins at least somewhat.
**(for the record, WooCommerce support is definitely being added, as is ACF)**.
It should be noted that this project is a *presentation layer*, and
*is not going to do a lot of complicated server logic for you*,
as that is plugin territory.

However, it is not out of line to allow plugin functionality to be
represented directly *in the context of presentation, where it is expected to do so*,
and this will likely be added. It will also *always be an opt-in*,
which means it needs to be turned on explicitly before it does anything,
even if you have compatible plugins installed. This system does not block plugins
from working, but it will not allow them to integrate natively with it without
explicit user consent, regardless of what the plugin has to say about it.

The policy with plugins is that *only stable and well known plugins that exist
in the Wordpress repo will be officially integrated into the project directly*,
and any other plugins will have to use the provided *Extension Api*
to hook into Sanctity. Furthermore, plugins that have *proven instabilities,
exploitable code, or numerous unresolved end user gripes* will
be **entirely blacklisted from using the Extension Api**, and will not run at all
on it unless you explicitly put your site in debug mode. You can still run them
parallel to it, but they will not integrate natively with or without an extension
if they are known to be unstable, unsafe, or introduce exploitable behavior
into the site (looking at at those guys that enable php being eval'd from
page source specifically, among others). This project puts security and
performance first, and does not condone sloppy or reckless programming practices.


## Development Integration and Extension

This is mostly in place for the time being, but this is all the documentation you get for now.
The source itself is written with the doc-block comments very explicitly about what everything does,
which will be ported to documentation when everything is stable. For now though, the source basically
is the manual, so look over it if you want to know how things work.

Below constitutes some notes that could not be put in the source docblock comments
for one reason or another.

## Developer Notes

### Frontend Development

If you are doing frontend development with this system, it has a gruntfile and
npm preconfigured, so edits to the javascript or SASS source will automatically
compile and minify. QUnit tests run on all saves by default when you use
`grunt watch` to insure you do not push unstable code accidentally.
If you are not used to working under a constant monitor of unit testing,
this can be cumbersome. If that is the case, too bad. It makes your code
not suck, and you really should be doing it anyhow.

#### Javascript Notes

- The root level dedicated controller for the theme is `sanctity`. It is available from `window.sanctity`, and also as a native jQuery extension (for method chaining). It inherits jQuery's prototype by default. The working objects it returns through operations *do not* inherit jQuery's prototype. They only inherit the prototype required to fulfill their purpose. This is written primarily using ES6 classes, and everything is pretty explicitly scoped by default to their intended purpose.
- Subclasses of the `sanctity` object have direct private access to any page data queued from the backend. This is the only way to obtain this, as the factory for the root object wipes all traces of it from the page after it acquires it. You will need to work with the provided api or extend it to access it.
- The root level controller object will also contain a page controller. The frontend, backend, and login page each have their own dedicated page controller class, though they share the same api for the most part. All pages will have the correct page controller present by default, prepopulated with any data it requires to operate correctly.
- Development source is located in the "etc/source/js" directory from the project root. It compiles into "dist/js" when its grunt task resolves. Both minified and uncompressed source is available.
- The provided dependencies are not loaded by the theme unless the CDNs fail. This means that editing the provided 3rd party source will probably do nothing in 99% of any given page load that occurs.
- The package.json and Gruntfile.js both reside in the root of the project.
- Direct access to the development source is blocked by an .htaccess for security. You will not be able to include dev assets directly from the theme on a live site without running the build ("grunt build" from the command line).
- There is an empty script called `placeholder` which queues in two locations on every page. Do not remove it or you will break all of the things. As Wordpress has a screwball way of enqueuing inline scripts (which is how the data is fed from the backend), and the underlying platform designates that an inline script can *only be queued directly before or after an existing non-inline script*, these are used as anchor points for when this needs to occur. If you unqueue them then none of the expected data will be on the page, and everything relying on it will break. Fair warning.
- Note that way more time has been spent on the backend framework than the frontend as of this writing. I will shift gears to the Javascript side when the nuances of the backend MVC are hammered out and are stable.

#### CSS Notes

- The page stylesheet source is compiled from SASS, which is located in the "etc/source/sass" directory.
- The compiled source resolves to "dist/css"
- There is a built in IDE for editing SASS on the fly, which sorta works as of now, but needs a bit more work to be fully operational.
- There is one minified output file for frontend, backend, and login page loads.
- Some custom SASS styles are built based on user preferences and compiled on the fly. These all use expected class names that are not disruptive, and do not directly style elements or print inline styles whatsoever. Most of these require a control class for their classes to do anything to further reduce the likelihood of collision with any other libraries you are using.
- Fallback sources are provided in the "dist" folder. These are compiled via Bower if they are not present. The Bower file resides in the root folder of the project. All project dependencies are managed by Bower unless they do not exist in the Bower repository (such as Material Design), in which case they are resolved with NPM.
- Direct access to the development source is blocked by an .htaccess for security. You will not be able to include dev assets directly from the theme on a live site without running the build ("grunt build" from the command line). This is a security measure to prevent sniffing frontend sources (mainly test files) for vulnerabilities. The project may be resturctured to factor this out in the future as the build step matures.
- There is a whole SASS skeleton for scaffolding stylistic considerations in place. It provides a number of structural utilities, stylistic utilities, some fun classes for automating pure CSS parallax effects, among other things.
- Typekit/webfontloader support is on the shortlist for development. It's in there now, but it's not integrated into the backend yet.

### Backend Development

The root level class to interact with is `\Sanctity`.
This class should provide you with a public API suitable for any direct logic
you wish to apply to the theme. There is an extension class you can extend to
add additional support, and the abstract model, controller, or view can also
be extended. Registration of 3rd party MVC components is not yet complete,
but should be prior to release. Registration of extensions will be available
quite a bit prior to that though.

UPDATE: *The extension api is almost done. Extensions register fine, but there's a bit more work in the event flow needed to make their provisions run natively*.

*see `mopsyd\sanctity\extensions\ExampleExtension` for reference/boilerplate's sake. There's very little you have to provide to write an extension, because the underlying abstraction kicks ass.*

Queueing data for it to be supplied to the `RenderAdapter` (Twig) is done with the controller `addContextData` method. This is how you push all data down to the templates. You don't need to do anything else, but you must supply it an array or container for it to be accepted. Key must be a string. No numeric keys (Psr-11 spec).
Queueing data for it to be supplied to javascript is done with the `bindClientData` method. All classes have this method, but it should primarily be used only by controllers, models, or components to keep things orderly and maintainable. You don't need to do anything else, but you must supply it an array or container for it to be accepted. Key must be a string. No numeric keys (Psr-11 spec). Scripts consuming the data on the frontend need to have their "package" property accessible and with the same value as the key you registered the backend data with, or the data will not be accessible to them. The key should be canonical, meaning lower case alphanumeric with dashes or underscores. Slashes are also acceptable, as package names may be used as the script id, which may contain slashes. Do not use backslashes. They have to be double encoded for the data array to resolve into json correctly, which means it has to be interpreted differently on the frontend. That is a chore you and your frontend guy don't need to introduce to yourselves.

Loading Twig templates in the Admin area is done with the AdminController's `loadAdminTemplate` method.

The Twig render component already has extension support set up, and is overloadable at runtime by default. You may package additional extensions, functions, tests, filters, etc at any point prior to the View class being called to render the page for the first time. Whenever that happens is last call, as per Twig's documentation. By default this does not occur until Wordpress fires one of the actual template requests to the theme, at which point all backend logic should be long since done. If you cut it off by the `wp_loaded` hook, you're pretty safe. This will likely be enforced programmatically in the future.

The PlatformAdapter basically does all of the WordPress things without you ever having to touch the WordPress api directly. It is located at `mopsyd\sanctity\libs\wordpress\WordpressAdapter`.

There are native methods provided to add actions, filters, contextual data, javascript, css, Twig filters,
Twig extensions, and numerous other additional logic sources from any extension of the AbstractController.
These methods are all prefixed with `declare`, and automatically queue your resources at the correct
Wordpress hook or filter point without any additional work.

Apigen documentation compiles into the "etc/docs" folder if you need it.
You can run it at any time from the project root using `apigen generate`,
as the config file is already appropriately configured to point to the right place.
This directory is view protected by default, so you will need to put a
"file_get_contents" shortcode in or something to view it from a live site,
or symlink it from elsewhere.

## Other Resources

* [Bootstrap 4 Cheatsheet](https://hackerthemes.com/bootstrap-cheatsheet/)


