-- 
-- The MIT License
--
-- @author Brian Dayhoff <mopsyd@me.com>
-- @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
-- @license http://opensource.org/licenses/MIT The MIT License (MIT)
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
-- THE SOFTWARE.
--

-- -------------------------------------------
-- This file installs the Sanctity Database
-- -------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `_sanctity_branding`
--

DROP TABLE IF EXISTS `_sanctity_branding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '',
  `profile` varchar(32) NOT NULL DEFAULT '',
  `section` varchar(32) NOT NULL,
  `category` varchar(32) NOT NULL,
  `scope` varchar(32) NOT NULL DEFAULT '',
  `context` varchar(32) DEFAULT '',
  `value` varchar(256) NOT NULL,
  `title` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(256) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`profile`,`section`,`category`,`scope`,`context`),
  KEY `_sanctity_branding_ibfk_1` (`profile`,`section`),
  KEY `category` (`category`),
  KEY `scope` (`scope`),
  KEY `context` (`context`),
  CONSTRAINT `_sanctity_branding_ibfk_1` FOREIGN KEY (`profile`, `section`) REFERENCES `_sanctity_branding_profiles` (`slug`, `section`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_branding_ibfk_2` FOREIGN KEY (`category`) REFERENCES `_sanctity_branding_categories` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_branding_ibfk_3` FOREIGN KEY (`scope`) REFERENCES `_sanctity_branding_scopes` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_branding_ibfk_4` FOREIGN KEY (`context`) REFERENCES `_sanctity_branding_profiles_contexts` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='branding and identity rules that get applied to pages to determine how it displays';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding`
--

LOCK TABLES `_sanctity_branding` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_branding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_branding_categories`
--

DROP TABLE IF EXISTS `_sanctity_branding_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'an identifying key for the branding category',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable title for the branding category',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional short description of the branding category',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='categories of branding rules, used to allow the end user to work with them easily';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding_categories`
--

LOCK TABLES `_sanctity_branding_categories` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_branding_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_branding_profiles`
--

DROP TABLE IF EXISTS `_sanctity_branding_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding_profiles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying key of the profile',
  `section` varchar(32) NOT NULL DEFAULT 'frontend' COMMENT 'the section of the site that the profile affects',
  `type` varchar(32) NOT NULL DEFAULT 'default' COMMENT 'the branding profile type, which corresponds to a value in _sanctity_branding_profile_types',
  `enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'whether or not the profile is enabled',
  `created` datetime NOT NULL COMMENT 'the date the profile was created',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'the time the profile was last updated',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable title for the profile',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional brief description for the profile',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`section`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='profiles that branding rulesets are bound to';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding_profiles`
--

LOCK TABLES `_sanctity_branding_profiles` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_branding_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_branding_profiles_contexts`
--

DROP TABLE IF EXISTS `_sanctity_branding_profiles_contexts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding_profiles_contexts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) DEFAULT NULL COMMENT 'an identifying key for the branding profile scope',
  `title` varchar(128) DEFAULT NULL COMMENT 'a human readable title',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional short description',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='allows branding rules to work across numerous taxonomies, posts, users, etc';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding_profiles_contexts`
--

LOCK TABLES `_sanctity_branding_profiles_contexts` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding_profiles_contexts` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_branding_profiles_contexts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_branding_profiles_types`
--

DROP TABLE IF EXISTS `_sanctity_branding_profiles_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding_profiles_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying key of the branding profile',
  `section` varchar(32) NOT NULL COMMENT 'the site section to which the profile applies',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT 'the display name of the branding profile',
  `description` varchar(256) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'whether the profile is enabled or not',
  `created` datetime NOT NULL COMMENT 'date of creation',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'last updated',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`section`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='the types of profile that can be created';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding_profiles_types`
--

LOCK TABLES `_sanctity_branding_profiles_types` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding_profiles_types` DISABLE KEYS */;
INSERT INTO `_sanctity_branding_profiles_types` VALUES (1,'default','frontend','Default Frontend Profile','This is the default profile. This is always active while the theme itself is active.',1,'2018-03-19 08:24:35','2018-03-19 08:33:08',1,1),(3,'default','backend','Default Backend Profile','This is the default backend profile. This is active when the backend theme is enabled.',0,'2018-03-19 08:29:42','2018-03-19 08:33:49',1,1),(4,'subdomain','frontend','Subdomain Profile','This is the subdomain profile. If this is enabled, it allows different branding and identity settings on the frontend per subdomain.',0,'2018-03-19 08:30:41','2018-03-19 08:36:17',1,1),(5,'subdomain','backend','Subdomain Backend Profile','This is the back subdomain profile. If the backend theme is enabled and this is also enabled, it allows separate dashboard themes and branding per subdomain.',0,'2018-03-19 08:30:58','2018-03-19 08:36:00',1,1),(6,'user','frontend','User Profile','This is the user level profile. If this is enabled, it allows separate branding and identity settings per logged in user on the frontend.',0,'2018-03-19 08:31:46','2018-03-19 08:36:46',1,1),(7,'user','backend','User Backend Profile','This is the user level backend profile. If this is enabled, it allows separate branding and identity profiles per logged in user in the dashboard area.',0,'2018-03-19 08:32:06','2018-03-19 08:37:15',1,1);
/*!40000 ALTER TABLE `_sanctity_branding_profiles_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_branding_scopes`
--

DROP TABLE IF EXISTS `_sanctity_branding_scopes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_branding_scopes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'an identifying key for the branding context',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable title for the branding category',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional short description of the branding category',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='page level elements that constrain how a branding rule operates';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_branding_scopes`
--

LOCK TABLES `_sanctity_branding_scopes` WRITE;
/*!40000 ALTER TABLE `_sanctity_branding_scopes` DISABLE KEYS */;
INSERT INTO `_sanctity_branding_scopes` VALUES (1,'menu','Menu Context','Constrains a branding option to menus',1,1),(2,'header','Header Context','Constrains a branding option to the header',1,1),(3,'footer','Footer Context','Constrains a branding option to the footer',1,1),(4,'link','Link Context','Constrains a branding option to links',1,1),(5,'content','Content Context','Constrains a branding option to page body content',1,1),(6,'image','Image Context','Constrains a branding option to images',1,1),(7,'form','Form Context','Constrains a branding option to forms',1,1),(8,'comment','Comment Context','Constrains a branding option to comments',1,1),(9,'sidebar','Sidebar Context','Constrains a branding option to sidebars',1,1),(10,'logo','Logo Context','Constrains a branding option to logos',1,1),(11,'avatar','Profile Avatar Context','Constrains a branding option to user avatars',1,1),(12,'widget','Widget Context','Constrains a branding option to widgets',1,1),(13,'dashboard','Dashboard Context','Constrains a branding option to the admin dashboard',1,1),(14,'login','Login Context','Constrains a branding option to the login page',1,1),(15,'error','Error Page Context','Constrains a branding option to error pages',1,1),(16,'article','Article Context','Constrains a branding option to articles',1,1),(17,'post','Post Context','Constrains a branding option to single posts',1,1);
/*!40000 ALTER TABLE `_sanctity_branding_scopes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_cache`
--

DROP TABLE IF EXISTS `_sanctity_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_cache` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(64) NOT NULL DEFAULT '' COMMENT 'identifying key. The only reason this table has this is so that it will delete automatically when its index record is deleted in _sanctity_cache_bindings',
  `payload` text NOT NULL COMMENT 'this is the actual cache entry, which is stored in a low-memory text field so it caches on disk statically and the database only maintains a pointer to it. This table may grow quite large with these, so the actual cache reference table is broken off into a much more lightweight table',
  PRIMARY KEY (`id`),
  CONSTRAINT `_sanctity_cache_ibfk_1` FOREIGN KEY (`id`) REFERENCES `_sanctity_cache_bindings` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='the actual cache';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_cache`
--

LOCK TABLES `_sanctity_cache` WRITE;
/*!40000 ALTER TABLE `_sanctity_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_cache_bindings`
--

DROP TABLE IF EXISTS `_sanctity_cache_bindings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_cache_bindings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL DEFAULT '' COMMENT 'unique cache key',
  `hash` varchar(256) NOT NULL DEFAULT '' COMMENT 'sha1 of the cache element',
  `item_id` int(11) unsigned NOT NULL COMMENT 'link to the primary key of the cache table',
  `entered` datetime NOT NULL COMMENT 'the date the cache item was entered',
  `ttl` int(11) NOT NULL DEFAULT '86400' COMMENT 'time to live. this will wipe the element automatically when this is exceeded to keep the cache fresh.',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'the type of element that the cache represents',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  UNIQUE KEY `item_id` (`item_id`),
  KEY `type` (`type`),
  CONSTRAINT `_sanctity_cache_bindings_ibfk_1` FOREIGN KEY (`type`) REFERENCES `_sanctity_cache_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='index of current cache keys, and how long it takes them to expire';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_cache_bindings`
--

LOCK TABLES `_sanctity_cache_bindings` WRITE;
/*!40000 ALTER TABLE `_sanctity_cache_bindings` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_cache_bindings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_cache_types`
--

DROP TABLE IF EXISTS `_sanctity_cache_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_cache_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'this is an identifying key for the cache type, and acts as a control for types that can be cached. A cache must have a suffix fom this table in order to be entered into the Sanctity cache. Its content may also be validated prior to entering the cache record to prevent data pollution',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='types of elements allowed to be cached';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_cache_types`
--

LOCK TABLES `_sanctity_cache_types` WRITE;
/*!40000 ALTER TABLE `_sanctity_cache_types` DISABLE KEYS */;
INSERT INTO `_sanctity_cache_types` VALUES (2,'css'),(1,'html'),(4,'javascript'),(3,'scss');
/*!40000 ALTER TABLE `_sanctity_cache_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_integrations`
--

DROP TABLE IF EXISTS `_sanctity_integrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_integrations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugin` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying slug of the 3rd party plugin. Should generally correspond to its name in the wordpress svn repo, or its equivalent unique identifier.',
  `min_version` varchar(16) DEFAULT NULL COMMENT 'the minimum version of the plugin to accept the binding as compatible for. If null, no minimum version check.',
  `max_version` varchar(16) DEFAULT NULL COMMENT 'the maximum compatible version of the plugin to check the binding for. If null, no maximum version check.',
  `binding` varchar(32) NOT NULL DEFAULT '' COMMENT 'the binding identifier. A single plugin may not have two of the same binding names, but different plugins can have the same binding name.',
  `allowed` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if false, the binding is disabled. This setting is user configurable.',
  `slug` varchar(64) NOT NULL DEFAULT '' COMMENT 'the unique identifying slug of the binding. This value should be singularly unique, but it may vary across installations if a preexisting name is already taken.',
  `title` varchar(128) NOT NULL DEFAULT 'Untitled Binding' COMMENT 'the human readable title of the plugin binding',
  `enabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'if false, the binding is disabled. This setting is changed programmatically by either the plugin or by sanctity. Incompatible versions will disable it this way. A plugin may request to have its binding enabled, but Sanctity has the final say on whether this is enabled. A plugin may always disable its binding, but may not enable it without authorization.',
  `added` datetime NOT NULL COMMENT 'the date the binding was added',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'the last updated timestamp of the binding',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`plugin`,`binding`),
  CONSTRAINT `_sanctity_integrations_ibfk_1` FOREIGN KEY (`plugin`) REFERENCES `_sanctity_integrations_plugins` (`plugin`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='plugin bindings currently registered to interact with Sanctity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_integrations`
--

LOCK TABLES `_sanctity_integrations` WRITE;
/*!40000 ALTER TABLE `_sanctity_integrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_integrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_integrations_plugins`
--

DROP TABLE IF EXISTS `_sanctity_integrations_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_integrations_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `plugin` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying slug of the plugin. This is its unique identifier. This should generally correspond to its name within the wordpress repo, or equivalent unique identifier.',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'the human readable name of the plugin',
  `enabled` tinyint(1) DEFAULT '0' COMMENT 'whether or not this plugin is enabled. This is the master kill switch for all integration bindings for a specific plugin, and is tracked separately from its binding level integrations. If this is off, none of its bindings will resolve. This may be enabled or disabled by the user, and will automatically disable if incompatibilities or security issues arise. In that event, it will not be reenabled unless the user specifically overrides it, or the plugin updates its version.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`plugin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='plugins allowed to interract with Sanctity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_integrations_plugins`
--

LOCK TABLES `_sanctity_integrations_plugins` WRITE;
/*!40000 ALTER TABLE `_sanctity_integrations_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_integrations_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_metadata`
--

DROP TABLE IF EXISTS `_sanctity_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_metadata` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `meta_key` varchar(64) NOT NULL DEFAULT '' COMMENT 'Provides the key name of the metadata value',
  `meta_category` varchar(32) NOT NULL DEFAULT 'core' COMMENT 'Represents the metadata category that the record is bound to. this corresponds to a slug value from _sanctity_metadata_categories',
  `meta_value` varchar(255) NOT NULL DEFAULT '' COMMENT 'Provides the metadata value',
  `created` datetime NOT NULL COMMENT 'Provides the date the meta key was created',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Provides the date the key was last updated',
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_key` (`meta_key`,`meta_category`),
  KEY `meta_category` (`meta_category`),
  CONSTRAINT `_sanctity_metadata_ibfk_1` FOREIGN KEY (`meta_category`) REFERENCES `_sanctity_metadata_categories` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='superfluous metadata that does not warrant its own table, but also doesnt belong in a wordpress table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_metadata`
--

LOCK TABLES `_sanctity_metadata` WRITE;
/*!40000 ALTER TABLE `_sanctity_metadata` DISABLE KEYS */;
INSERT INTO `_sanctity_metadata` VALUES (1,'version','core','0.1.0','2018-03-19 03:39:52','2018-03-19 03:40:03'),(2,'php-version-required','core','70000','2018-03-19 03:39:52','2018-03-19 03:41:10'),(3,'wordpress-version-required','core','4.9.0','2018-03-19 03:39:52','2018-03-19 03:41:27');
/*!40000 ALTER TABLE `_sanctity_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_metadata_categories`
--

DROP TABLE IF EXISTS `_sanctity_metadata_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_metadata_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'This is the root level identifier for metadata tracked by Sanctity. Almost all other tables are constrained by these keys. Important tables will restrict deletion, to insure that an accidental delete does not cascade across the entire dataset. A metadata category must be added to extend further functionality on the system.',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT 'a human readable title for the metadata category, mostly provided as a convenience to developers. This does not show up in the wordpress backend.',
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COMMENT='the primary categories of data tracked by sanctity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_metadata_categories`
--

LOCK TABLES `_sanctity_metadata_categories` WRITE;
/*!40000 ALTER TABLE `_sanctity_metadata_categories` DISABLE KEYS */;
INSERT INTO `_sanctity_metadata_categories` VALUES (1,'core','Sanctity Core Metadata'),(2,'settings','Sanctity Settings Metadata'),(3,'branding','Sanctity Branding Metadata'),(4,'frontend','Sanctity Frontend Metadata'),(5,'backend','Sanctity Backend Metadata'),(6,'login','Sanctity Login Metadata'),(7,'cdn','Sanctity CDN Metadata'),(8,'template','Sanctity Template Metadata'),(9,'integration','Sanctity Integration Metadata'),(10,'cache','Sanctity Cache Metadata'),(11,'dashboard','Sanctity Dashboard Metadata');
/*!40000 ALTER TABLE `_sanctity_metadata_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_remotes`
--

DROP TABLE IF EXISTS `_sanctity_remotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_remotes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'a unique identifying string for the endpoint',
  `endpoint` varchar(255) NOT NULL DEFAULT '' COMMENT 'the base dns name of the endpoint, without a uri attached',
  `type` varchar(16) NOT NULL DEFAULT '' COMMENT 'the remote type, which corresponds to a value in _sanctity_remotes_types. If the endpoint serves multiple content types, it will need multiple entries in this table.',
  `name` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable name for the endpoint.',
  `description` varchar(256) DEFAULT NULL COMMENT 'a short optional description for the endpoint',
  `added` datetime NOT NULL COMMENT 'the date the endpoint was added',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'a timestamp of the time the endpoint was last updated',
  `active` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'a boolean switch to activate or deactivate the endpoint. this setting is user configurable.',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `type` (`type`),
  CONSTRAINT `_sanctity_remotes_ibfk_1` FOREIGN KEY (`type`) REFERENCES `_sanctity_remotes_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='registered dns endpoints';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_remotes`
--

LOCK TABLES `_sanctity_remotes` WRITE;
/*!40000 ALTER TABLE `_sanctity_remotes` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_remotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_remotes_types`
--

DROP TABLE IF EXISTS `_sanctity_remotes_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_remotes_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '' COMMENT 'a unique identifying string for the remote type, typically corresponding to the type of content it serves',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT 'a human readable title for the endpoint',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional brief description of the endpoint',
  `enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'whether or not the endpoint is disabled. This is the master switch for a specific endpoint, and all of its routes and content types will be inaccessible if this is disabled (although their individual settings do not change when this occurs)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='types of remote connections Sanctity is allowed to make';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_remotes_types`
--

LOCK TABLES `_sanctity_remotes_types` WRITE;
/*!40000 ALTER TABLE `_sanctity_remotes_types` DISABLE KEYS */;
INSERT INTO `_sanctity_remotes_types` VALUES (1,'css','CSS CDN Endpoint','A remote store of static stylesheet content',1),(2,'javascript','Javascript CDN Endpoint','A remote store of static javascript content',1),(3,'font','Font CDN Endpoint','A remote store of static font content',1),(4,'img','Image CDN Endpoint','A remote store of static image content',1);
/*!40000 ALTER TABLE `_sanctity_remotes_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings`
--

DROP TABLE IF EXISTS `_sanctity_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying key, which corresponds to a value in _sanctity_settings_valid. This is only unique within the scope of a combination of this, type, section, and subsection, which represents its domain.',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings type, which corresponds to a value in _sanctity_settings_types',
  `section` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings section, which corresponds to a value in _sanctity_settings_sections',
  `subsection` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings subsection, which corresponds to a value in _sanctity_settings_subsections',
  `display` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable display name',
  `description` varchar(256) DEFAULT NULL COMMENT 'a short description',
  `value` varchar(128) DEFAULT NULL COMMENT 'the current value',
  `created` datetime NOT NULL COMMENT 'date created',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'last updated timestamp',
  `auth` varchar(64) NOT NULL DEFAULT 'edit_theme_options' COMMENT 'the wordpress acl permission required for the setting to display for the viewer',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`section`,`subsection`,`type`),
  KEY `_sanctity_settings_ibfk_1` (`subsection`,`section`,`type`),
  CONSTRAINT `_sanctity_settings_ibfk_1` FOREIGN KEY (`subsection`, `section`, `type`) REFERENCES `_sanctity_settings_subsections` (`slug`, `section`, `type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='the current values for settings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings`
--

LOCK TABLES `_sanctity_settings` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings_defaults`
--

DROP TABLE IF EXISTS `_sanctity_settings_defaults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings_defaults` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'Tne slug. This is unique only when also combined with type, section, and subsection, which represents its domain.',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'The settings type, from _sanctity_settings_types',
  `section` varchar(32) NOT NULL DEFAULT '' COMMENT 'The settings section, from _sanctity_settings_sections',
  `subsection` varchar(32) NOT NULL DEFAULT '' COMMENT 'The settings subsection, from _sanctity_settings_subsections',
  `value_pointer` int(11) unsigned DEFAULT NULL COMMENT 'References the primary key of a value from _sanctity_settings_valid',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`type`,`section`,`subsection`),
  KEY `value` (`value_pointer`),
  CONSTRAINT `_sanctity_settings_defaults_ibfk_1` FOREIGN KEY (`value_pointer`) REFERENCES `_sanctity_settings_valid` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_settings_defaults_ibfk_2` FOREIGN KEY (`slug`, `type`, `section`, `subsection`) REFERENCES `_sanctity_settings_valid` (`slug`, `type`, `section`, `subsection`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='the default values for settings';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings_defaults`
--

LOCK TABLES `_sanctity_settings_defaults` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings_defaults` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_settings_defaults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings_sections`
--

DROP TABLE IF EXISTS `_sanctity_settings_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings_sections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'The unique key of the settings section. These are completely unique.',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'The readable name',
  `description` varchar(256) DEFAULT NULL COMMENT 'A short optional description',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='the part of the site that the settings apply to';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings_sections`
--

LOCK TABLES `_sanctity_settings_sections` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings_sections` DISABLE KEYS */;
INSERT INTO `_sanctity_settings_sections` VALUES (1,'frontend','Frontend Settings Type','Frontend settings affect how the frontend public facing website is presented'),(2,'backend','Backend Admin Settings Type','Backend settings affect how the admin dashboard theme is presented, and whether it is enabled'),(3,'login','Login Page Settings Type','Login settings affect how the login page display is presented'),(4,'core','Sanctity Core Type','Core settings affect the behavior of Sanctity under the hood. Most of these are not user configurable.'),(5,'global','Sanctity Global Type','Global settings affect the entire installation, and are the default when no other specific option is set for a given section.');
/*!40000 ALTER TABLE `_sanctity_settings_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings_subsections`
--

DROP TABLE IF EXISTS `_sanctity_settings_subsections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings_subsections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying key of the subsection',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'the type of the subsection, which corresponds to a value in _sanctity_settings_types',
  `section` varchar(32) NOT NULL DEFAULT '' COMMENT 'the section that the subsection belongs to',
  `template` varchar(32) DEFAULT NULL COMMENT 'the template that displays the form subsection',
  `auth` varchar(64) NOT NULL DEFAULT 'edit_theme_options' COMMENT 'the wordpress acl permission required to edit the form subsection',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT 'the subsection title',
  `description` varchar(256) DEFAULT NULL COMMENT 'the description that precedes the form subsection',
  `icon` varchar(64) DEFAULT NULL COMMENT 'the icon, if any',
  `order` int(11) NOT NULL COMMENT 'the order that the subsection appears on the form',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`section`,`type`),
  KEY `type` (`type`),
  KEY `section` (`section`),
  KEY `template` (`template`),
  CONSTRAINT `_sanctity_settings_subsections_ibfk_1` FOREIGN KEY (`type`) REFERENCES `_sanctity_settings_types` (`slug`) ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_settings_subsections_ibfk_2` FOREIGN KEY (`section`) REFERENCES `_sanctity_settings_sections` (`slug`) ON UPDATE CASCADE,
  CONSTRAINT `_sanctity_settings_subsections_ibfk_3` FOREIGN KEY (`template`) REFERENCES `_sanctity_templates` (`slug`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COMMENT='subsections of the primary settings sections';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings_subsections`
--

LOCK TABLES `_sanctity_settings_subsections` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings_subsections` DISABLE KEYS */;
INSERT INTO `_sanctity_settings_subsections` VALUES (3,'overview','dashboard','global','dashboard-overview','edit_theme_options','At A Glance',NULL,NULL,0),(4,'about','dashboard','global','dashboard-about','edit_theme_options','About Sanctity','A brief background on what Sanctity is all about.',NULL,1),(5,'release-notes','dashboard','global','dashboard-release','edit_theme_options','Release Notes','Here\'s what\'s new with the most recent release.',NULL,2),(6,'documentation','dashboard','global','dashboard-documentation','edit_theme_options','Documentation','Everything you ever wanted to know about how to use your theme to maximize your site\'s awesome.',NULL,3),(7,'upcoming','dashboard','global','dashboard-upcoming','edit_theme_options','Upcoming Features','Take a look at some of the things planned for upcoming releases.',NULL,4),(8,'color-scheme','branding','global','branding-color-scheme','edit_theme_options','Branding Color Scheme','Configure branding colors for use throughout your site, and create custom rules about how they interact with each other.',NULL,0),(9,'logo-settings','branding','global',NULL,'edit_theme_options','Logo and Tagline Settings','This page allows you to configure your logo, site title, tagline, and how they are displayed throughout the site. This panel is more in depth than the default customizer panel, but you may also use that if you wish. Whatever\'s clever.',NULL,0),(10,'typography','branding','global',NULL,'edit_theme_options','Typography Settings','Manage webfonts in your layout, and designate which content blocks, pages, layouts, or page elements they apply to.',NULL,0),(11,'identity','branding','global',NULL,'edit_theme_options','Brand Identity Settings','Manage how your home page, about page, and author page layouts are displayed. Keep that public face fresh.',NULL,0),(12,'cdn-settings','settings','global','settings-cdn','edit_theme_options','CDN Settings','Manage scripts, fonts, and stylesheets from remote sources, or pull them down locally to use offline for development.',NULL,0),(13,'debug-settings','settings','global','settings-debug','edit_theme_options','Debug Settings','Debug your site in style. Decide, who it should be visible to, how it is presented, and when/where it should apply.',NULL,0),(14,'templates','settings','global',NULL,'edit_theme_options','Template Settings','Customize layouts, create templates, and designate which content to display within them. If you don\'t like the options, roll your own.',NULL,0),(15,'content-serving','settings','global',NULL,'edit_theme_options','Content Optimization Settings','Determine how content is served from your site. It can be served locally, live, cached, statically, or from remotes. No plugins required.',NULL,0),(16,'static-error-pages','settings','global',NULL,'edit_theme_options','Static Error Page Settings','Customize static error pages to display if your site goes down, so nobody ever sees a whitescreen again under any circumstances.',NULL,0),(17,'updates','settings','global',NULL,'edit_theme_options','Update Settings','Determine how you want theme updates to be dealt with, and whether or not you want to be nagged to run updates if you have auto-update disabled.',NULL,0),(18,'shortcode-settings','settings','global','settings-shortcodes','edit_theme_options','Shortcode Integration Settings','Configure how shortcodes integrate with your theme, add shortcode bindings that can be used directly within templates, or create shortcodes that render templates on the fly from this page.',NULL,0),(19,'bootstrap-skin','frontend','frontend','frontend-bootstrap-skin','edit_theme_options','Bootstrap Skin','Select the skin you would like to display on the frontend of your website.',NULL,0),(20,'backend-enable','backend','backend','backend-enable','edit_theme_options','Enable Backend Theme','If enabled, this will replace the default Wordpress admin theme with the Sanctity Boostrap-based dashboard. Additional options will be enabled if this is turned on.',NULL,0),(21,'bootstrap-skin','backend','backend','backend-bootstrap-skin','edit_theme_options','Bootstrap Skin','Select the skin you would like to display on the backend of your website (this option has no effect if the backend theme is disabled).',NULL,0),(22,'login-enable','backend','backend',NULL,'edit_theme_options','Enable Login Page Theme','If enabled, this will replace the default Wordpress login page layout with the Sanctity Boostrap-based login page. Additional options will be enabled if this is turned on.',NULL,0);
/*!40000 ALTER TABLE `_sanctity_settings_subsections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings_types`
--

DROP TABLE IF EXISTS `_sanctity_settings_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'A unique identifying term for the settings type',
  `icon` varchar(64) DEFAULT NULL COMMENT 'an optional fontawesome icon. Glyphicons also work if they are loaded on the page (they aren''t by default)',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT 'a human readable title',
  `description` varchar(256) DEFAULT NULL COMMENT 'a short optional description',
  `ordering` int(3) unsigned NOT NULL COMMENT 'designates the order of priority that the type is listed by when rendered on the page',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  CONSTRAINT `_sanctity_settings_types_ibfk_1` FOREIGN KEY (`slug`) REFERENCES `_sanctity_metadata_categories` (`slug`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='the primary settings types, corresponding to the display menu in the backend';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings_types`
--

LOCK TABLES `_sanctity_settings_types` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings_types` DISABLE KEYS */;
INSERT INTO `_sanctity_settings_types` VALUES (1,'dashboard','fas fa-tachometer-alt','Sanctity Dashboard',NULL,0),(2,'settings','fas fa-cog','Sanctity Settings',NULL,1),(3,'frontend','fas fa-globe','Sanctity Frontend Layout',NULL,2),(4,'backend','fas fa-desktop','Sanctity Backend Layout',NULL,3),(5,'branding','fas fa-paint-brush','Sanctity Branding',NULL,4),(6,'integration','fab fa-wordpress-simple','Sanctity Integration',NULL,5);
/*!40000 ALTER TABLE `_sanctity_settings_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_settings_valid`
--

DROP TABLE IF EXISTS `_sanctity_settings_valid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_settings_valid` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'the identifying key of the setting to constrain',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings type, which corresponds to a value in _sanctity_settings_types',
  `section` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings section, which corresponds to a value in _sanctity_settings_sections',
  `subsection` varchar(32) NOT NULL DEFAULT '' COMMENT 'the settings subsection, which corresponds to a value in _sanctity_settings_subsections',
  `allowed` varchar(256) NOT NULL DEFAULT '' COMMENT 'Values that are allowed for the given setting. A null value indicates unrestricted.',
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`,`type`,`section`,`subsection`),
  KEY `_sanctity_settings_valid_ibfk_1` (`subsection`,`section`,`type`),
  CONSTRAINT `_sanctity_settings_valid_ibfk_1` FOREIGN KEY (`subsection`, `section`, `type`) REFERENCES `_sanctity_settings_subsections` (`slug`, `section`, `type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='valid settings for all settings that have fixed expected values';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_settings_valid`
--

LOCK TABLES `_sanctity_settings_valid` WRITE;
/*!40000 ALTER TABLE `_sanctity_settings_valid` DISABLE KEYS */;
/*!40000 ALTER TABLE `_sanctity_settings_valid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_templates`
--

DROP TABLE IF EXISTS `_sanctity_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '' COMMENT 'an identifying key for the template. This combined with its section creates its unique identifier.',
  `section` varchar(32) NOT NULL DEFAULT '' COMMENT 'the section that the template applies to (eg frontend, backend, login, etc). This is constrained by _sanctity_settings_sections',
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT 'a human readable name for the template',
  `path` varchar(256) NOT NULL DEFAULT '' COMMENT 'the disk path of the template, relative to the site installation',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `section` (`section`),
  CONSTRAINT `_sanctity_templates_ibfk_1` FOREIGN KEY (`section`) REFERENCES `_sanctity_settings_sections` (`slug`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COMMENT='templates registered with Sanctity';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_templates`
--

LOCK TABLES `_sanctity_templates` WRITE;
/*!40000 ALTER TABLE `_sanctity_templates` DISABLE KEYS */;
INSERT INTO `_sanctity_templates` VALUES (2,'dashboard-overview','backend','Dashboard Overview','components/tabs/overview.twig',0,0),(3,'dashboard-about','backend','Dashboard About','components/tabs/about.twig',0,0),(4,'dashboard-release','backend','Dashboard Release Notes','components/tabs/release.twig',0,0),(5,'dashboard-documentation','backend','Dashboard Documentation','components/tabs/documentation.twig',0,0),(6,'dashboard-upcoming','backend','Dashboard Upcoming Features','components/tabs/upcoming.twig',0,0),(7,'branding-color-scheme','backend','Branding Color Scheme Editor','components/forms/branding-colors.twig',0,0),(8,'settings-cdn','backend','CDN Settings','components/forms/cdn-settings.twig',0,0),(9,'settings-debug','backend','Debug Settings','components/forms/debug-settings.twig',0,0),(10,'settings-shortcodes','backend','Shortcode Settings','components/forms/shortcode-settings.twig',0,0),(11,'frontend-bootstrap-skin','backend','Skin Settings','components/forms/template-settings.twig',0,0),(12,'backend-enable','backend','Enable Backend Theme','components/forms/backend-enable.twig',0,0),(13,'backend-bootstrap-skin','backend','Skin Settings','components/forms/template-settings.twig',0,0);
/*!40000 ALTER TABLE `_sanctity_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_sanctity_templates_layouts`
--

DROP TABLE IF EXISTS `_sanctity_templates_layouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `_sanctity_templates_layouts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) DEFAULT NULL COMMENT 'an identifying key for the branding profile layout',
  `section` varchar(32) NOT NULL DEFAULT 'frontend' COMMENT 'the site section the layout applies to',
  `title` varchar(128) DEFAULT NULL COMMENT 'a human readable title',
  `description` varchar(256) DEFAULT NULL COMMENT 'an optional short description',
  `display` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `editable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`,`section`),
  KEY `section` (`section`),
  CONSTRAINT `_sanctity_templates_layouts_ibfk_1` FOREIGN KEY (`section`) REFERENCES `_sanctity_settings_sections` (`slug`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COMMENT='layouts for pages, both registered with wordpress and private';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_sanctity_templates_layouts`
--

LOCK TABLES `_sanctity_templates_layouts` WRITE;
/*!40000 ALTER TABLE `_sanctity_templates_layouts` DISABLE KEYS */;
INSERT INTO `_sanctity_templates_layouts` VALUES (1,'default','frontend','Default Layout','This is the default layout',1,0),(2,'dashboard-default','backend','Default Dashboard Layout','This is the default backend dashboard layout',1,0),(3,'blank-slate','frontend','Blank Slate','This is a blank template in a responsive wrapper container with no padding or margins.',1,0),(4,'paper-trail','frontend','Paper Trail','This is a single column layout with side margins reminiscent of a paper document. Good for distraction free reading.',1,0),(5,'quarter-left','frontend','Quarter Left','This is a responsive layout with a left sidebar occupying 1/4th of the content area, and the primary content occupying the remaining 3/4ths.',1,0),(6,'quarter-right','frontend','Quarter Right','This is a responsive layout with a right sidebar occupying 1/4th of the content area, and the primary content occupying the remaining 3/4ths.',1,0),(7,'the-grail','frontend','A Grail, You Say?','This is a three column responsive layout with two equal fixed width sidebars, and a flexbox center column. The content blocks are aligned so the primary content always displays first in the markup for better search indexing.',1,0),(8,'parallax-one','frontend','Parallax One','This is a responsive parallax layout with an alternate vertical scroll speed for the background.',1,0),(9,'parallax-two','frontend','Parallax Two','This is a responsive parallax layout with background images that shuffle like slides as the page progresses.',1,0),(10,'panoramic','frontend','Panorama','This is a responsive parallax layout with a horizontal parallax effect in the background as the foreground scrolls vertically.',1,0),(11,'page-one','frontend','Page One','This is a responsive one-page layout, that combines posts into a single page seamlessly.',1,0),(12,'home-base','frontend','Home Base','This is a front page showcase layout with a jumbotron and configurable tile content. The jumbotron can contain a fixed background, slideshow, call to action, or any combination of these.',1,0),(13,'applicator','frontend','Applicator','This is a responsive mobile device oriented layout that looks like a web app.',1,0),(14,'sentinel','login','Sentinel','This is a the default responsive login page layout.',1,0),(15,'something-corporate','frontend','Something Corporate','This is a responsive business page layout.',1,0),(16,'fake-news','frontend','Fake News','This is a responsive editorial oriented layout.',1,0),(17,'pound-the-pavement','frontend','Pound The Pavement','This is a responsive layout for a personal professional profile site.',1,0),(18,'tip-twenty','frontend','Tip Twenty','This is a responsive home page layout with a full page background image container before the primary content.',1,0),(19,'hock','frontend','Hock','This is a responsive product showcase suitable for a shop or ecommerce site.',1,0),(20,'who-needs-pixels','frontend','Pixel Imperfect','This is a responsive full flexbox layout with zero pixel measurements. Stretches seamlessly on everything, and angers people who rely on photoshop wireframes.',1,0);
/*!40000 ALTER TABLE `_sanctity_templates_layouts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;