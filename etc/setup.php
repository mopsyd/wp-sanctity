<?php

/*
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

if ( !defined( 'ABSPATH' ) && !getenv( 'CI' ) )
{
    die();
}

/**
 * This is the Sanctity setup file.
 * All of the prep work for bootload
 * prior to the actual system taking over
 * lives here.
 */
/**
 * <Define the baseline environment for the current request>
 *
 * The following constants compile directly into the baseline interface,
 * and allow all Sanctity classes a simplified way to access details about
 * the current request environment without reaching out of scope
 * or calling methods.
 *
 * This insures that all classes can remain entirely decoupled and not need
 * to be aware of another class or method to obtain this common information,
 * and also that if someone fucks with the globals at any time after
 * the theme compiles, it still has an accurate snapshot of the page literal.
 *
 * These constants compile into the root Sanctity interface, so no further
 * programatic checks for them are required, and no mutation of values
 * is possible at any point during runtime.
 */
// Snapshot of the current literal page uri.
//
// In a unit test environment, this will be defined by the test case.
define( 'SANCTITY_URI_LITERAL',
    getenv( 'CI_URI_LITERAL' )
        ?:
        ( array_key_exists( 'HTTP_HOST', $_SERVER ) && array_key_exists( 'REQUEST_URI',
            $_SERVER )
            ? $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
            : false )  );

//
// The HTTP Host, or `false` if the current request originated from CLI.
//
// This is used to set access control headers for ajax requests,
// among other things, without needing to evaluate it again with a function call.
//
define( 'SANCTITY_HTTP_HOST', ( array_key_exists( 'HTTP_HOST', $_SERVER )
            ? ( 'http' . ( ( array_key_exists( 'HTTPS', $_SERVER ) // SSL detection. If not defined, nope.
            && $_SERVER['HTTPS']  // Unix servers will use true or false for SSL.
            && strtolower( $_SERVER['HTTPS'] ) !== 'off' )  // Windows servers will use 'on' or 'off' because windows is stupid.
                ? 's'
                : null ) . '://' . $_SERVER['HTTP_HOST'] )
            : false  ) );

// Snapshot of the current HTTP request method.
//
// In a unit test environment, this will be defined by the test case.
define( 'SANCTITY_REQUEST_METHOD',
    getenv( 'CI_REQUEST_METHOD' )
        ?:
        ( array_key_exists( 'REQUEST_METHOD', $_SERVER )
            ? strtolower( $_SERVER['REQUEST_METHOD'] )
            : false )  );

// Snapshot the current microtime hash, for logging purposes.
// This allows the entire Sanctity codebase to isolate its
// logging to a single preset runtime instance,
// so seemingly unrelated errors that turn up can be
// isolated by hash for correlative purposes.
//
// This will only generate a false positive if this line
// is encountered at the exact same nanosecond
// by two concurrent requests.
//
// In a CI environment, this will be the build ID.
//
define( 'SANCTITY_RUNTIME_INSTANCE_HASH',
    getenv( 'CI_BUILD_NUMBER' )
        ?:
        sha1( microtime( true ) )  );

// Determines if the current request is ajax. This allows for quick routing to
// the ajax controller instead of the standard controller.
define( 'SANCTITY_AJAX',
    getenv( 'CI_AJAX_TEST' )
        ?: ( (array_key_exists( 'HTTP_X_REQUESTED_WITH', $_SERVER )
        && !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] )
        && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest')
            ? true
            : false )  );

// If composer is not present, the custom autoloader
// will take care of everything anyways.
// Don't sweat it.
if ( file_exists( SANCTITY_BASEDIR . 'vendor/autoload.php' ) )
{
    // Handles dynamically loading all of the classes without using include.
    require_once SANCTITY_BASEDIR . 'vendor/autoload.php';
}
// Procedural api for our garden variety Wordpress user.
require_once SANCTITY_BASEDIR . 'src/spaghetti/spaghetti.php';

// Aunt Polly was always up to date with the hottest trends.
// Compatibility polyfills for super old PHP versions.
require_once SANCTITY_BASEDIR . 'src/spaghetti/polyfill.php';

// Load all of the things that the autoloader needs loaded to compile,
// in the event Composer is not present.
require_once SANCTITY_BASEDIR . 'src/traits/AdapterCompatibility.php';
require_once SANCTITY_BASEDIR . 'src/traits/Debugger.php';
require_once SANCTITY_BASEDIR . 'src/traits/ContainerPackagerUtility.php';
require_once SANCTITY_BASEDIR . 'src/interfaces/SanctityInterface.php';
require_once SANCTITY_BASEDIR . 'src/AbstractBase.php';
require_once SANCTITY_BASEDIR . 'src/libs/autoload/Autoload.php';
// Fallback autoloader. Used if the one above doesn't work for some reason. If the one above does work it does nothing.
// Initialize the fallback autoloader. Everything else can work seamlessly after this.
\mopsyd\sanctity\libs\autoload\Autoload::init();

// Make sure all dependencies are in order prior to launch.
require_once SANCTITY_BASEDIR . 'etc/preflight.php';

// Load the front controller class.
require_once SANCTITY_BASEDIR . 'src/Sanctity.php';
