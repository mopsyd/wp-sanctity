/* 
 * Sanctity Login Javascript.
 * 
 * @link https://mopsyd.com/projects/wordpress/sanctity
 * @file This javascript controls the UI for the login page.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright 2018 Brian Dayhoff
 * @package mopsyd/wp-sanctity
 * @version 0.2.0
 * @license MIT
 * @since 0.1.0
 */


