/* 
 * Sanctity Settings Page Javascript
 * 
 * @link https://mopsyd.com/projects/wordpress/sanctity
 * @file This javascript controls the UI for the admin settings pages.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright 2018 Brian Dayhoff
 * @package mopsyd/wp-sanctity
 * @version 0.2.0
 * @license MIT
 * @since 0.1.0
 */

/*jshint esversion: 6 */

(function (protoObj) {

    "use strict";

    let scope = "admin";
    let _proto = protoObj || Sanctity;
    let abstract = _proto(this).abstract('page');
    let token = false;
    let lib = {
        initialize: function ()
        {
            if (this.is_initialized === true)
            {
                return false;
            }
            this.menus.initialize();
            this.ui.initialize();
            this.is_initialized = true;
            return true;
        },
        menus: {
            initialize: function ()
            {
                this.submenus.initialize();
            },
            submenus: {
                is_initialized: false,
                initialize: function () {
                    if (this.is_initialized === true)
                    {
                        return false;
                    }
                    this.is_initialized = true;
                    return true;
                }
            }
        },
        ui: {
            tooltips: null,
            popovers: null,
            initialize: function ()
            {
                this.tooltips = jQuery('[data-toggle="tooltip"]');
                this.popovers = jQuery('[data-toggle="popover"]');
                this.tooltips.tooltip();
                this.popovers.popover({
                    trigger: "click hover"
                });
            }
        }
    };
    class SanctitySettings extends abstract {

        static context()
        {
            return scope;
        }

        constructor(key, data, selector) {
            super(key, data);
            return this;
        }

        initializeAssets(event, instance)
        {
            lib.initialize();
        }

        compileContext(event, instance)
        {

        }
    }
    token = _proto(this).register('controller', SanctitySettings);
})(Sanctity);
