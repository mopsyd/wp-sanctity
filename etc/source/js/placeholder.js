/**
 * Sanctity Placeholder
 * 
 * This is an empty script file that gets queued into the head of the document,
 * so that the page metadata inline script can queue into the head correctly.
 * This is a workaround for WordPress's general inability to queue inline without
 * a remote script to anchor it to.
 * Do not remove or unqueue this script.
 */
