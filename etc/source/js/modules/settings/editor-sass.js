/* 
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*jshint esversion: 6 */

(function (protoObj) {

    "use strict";

    let scope = "sass-editor";
    let _proto = protoObj || Sanctity;
    let abstract = _proto(this).abstract('component');
    let token = false;
    let lib = {
        initialize: function ()
        {
            if (this.is_initialized === true)
            {
                return false;
            }
            this.editor.initialize();
            this.menu.initialize();
            this.filebase.initialize();
            this.is_initialized = true;
            return true;
        },
        editor: {
            ace: null,
            element: null,
            options: {
                mode: "ace/mode/scss",
                theme: "ace/theme/gruvbox",
                selectionStyle: "text"
            },
            is_initialized: false,
            initialize: function ()
            {
                if (this.is_initialized)
                {
                    return false;
                }
                this.element = jQuery('[data-context="sanctity-sass-editor-viewport"]');
                this.ace = ace.edit(this.element.attr('id'), this.options);
                this.ace.setHighlightActiveLine(true);
                this.ace.setShowPrintMargin(true);
                this.ace.session.setUseSoftTabs(true);
                this.ace.session.setTabSize(4);
                this.is_initialized = true;
                return true;
            }
        },
        menu: {
            is_initialized: false,
            menu: {},
            initialize: function ()
            {
                if (this.is_initialized)
                {
                    return false;
                }
                this.is_initialized = true;
                return true;
            }
        },
        filebase: {
            is_initialized: false,
            files: {},
            initialize: function ()
            {
                if (this.is_initialized)
                {
                    return false;
                }
                this.is_initialized = true;
                return true;
            }
        }
    };
    class SassEditor extends abstract {

        static context()
        {
            return scope;
        }

        constructor(key, data, selector) {
            super(key, data, selector);
            let self = this;
            lib.initialize();
            return this;
        }
    }
    token = _proto(this).register('component', SassEditor);
})(Sanctity);
