/* 
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*jshint esversion: 6 */

(function (protoObj) {

    "use strict";

    let scope = "bootstrap-skin";
    let _proto = protoObj || Sanctity;
    let abstract = _proto(this).abstract('component');
    let token = false;
    class SkinSettings extends abstract {

        static context()
        {
            return scope;
        }

        constructor(key, data, selector) {
            super(key, data, selector);
            let self = this;
            let control = jQuery('[data-context="skinpicker-selector"]');
            let viewport = jQuery('[data-context="skinpicker-viewport"]');
            let title = jQuery('[data-context="skinpicker-title"]');
            let description = jQuery('[data-context="skinpicker-description"]');
            let demo = jQuery('[data-context="skinpicker-demo"]');
            let expected_prefix = "bootstrap-";
            let skin_options = {
                "bootstrap": {
                    "title": "Default Bootstrap 4 Skin",
                    "description": "The Default look and feel of Bootstrap 4",
                    "image": viewport.attr("src"),
                    "preview": "#"
                }
            };
            let update_skin = function (value, opts)
            {
                viewport.attr('src', opts[value].image);
                title.text(opts[value].title);
                description.text(opts[value].description);
                demo.attr('href', opts[value].preview);
            };
            for (let i in this.bootswatch.themes)
            {
                if (this.bootswatch.themes.hasOwnProperty(i))
                {
                    skin_options[expected_prefix + this.bootswatch.themes[i].name.toLowerCase()] = {
                        "title": this.bootswatch.themes[i].name,
                        "description": this.bootswatch.themes[i].description,
                        "image": this.bootswatch.themes[i].thumbnail,
                        "preview": this.bootswatch.themes[i].preview
                    };
                }
            }
            control.change(function () {
                update_skin(jQuery(this).val(), skin_options);
            }).change();
            return this;
        }
    }
    token = _proto(this).register('component', SkinSettings);
})(Sanctity);