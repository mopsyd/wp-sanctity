/* 
 * The MIT License
 *
 * Copyright 2018 Brian Dayhoff <mopsyd@me.com>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*jshint esversion: 6 */

(function (protoObj) {

    "use strict";

    let scope = "style-colors";
    let _proto = protoObj || Sanctity;
    let abstract = _proto(this).abstract('component');
    let token = false;
    let lib = {
        initialize: function ()
        {
            if (this.is_initialized === true)
            {
                return false;
            }
            this.is_initialized = true;
            return true;
        }
    };
    class ColorSettings extends abstract {

        static context()
        {
            return scope;
        }

        constructor(key, data, selector) {
            super(key, data, selector);
            let self = this;
            lib.initialize();
            (function () {
                var form = jQuery('form#style-colors');
                var form_submit = jQuery('[data-context="' + form.attr('id') + '-submit"]');
                var form_reset = jQuery('[data-context="' + form.attr('id') + '-reset"]');
                var formlib = {
                    fields: {
                        nonce: jQuery('[data-context="' + form.attr('id') + '-nonce"]'),
                        referer: jQuery('[data-context="' + form.attr('id') + '-referer"]'),
                        binding: jQuery('[data-context="' + form.attr('id') + '-binding"]'),
                        /**
                         * The existing values for the form fields that need
                         * to be actually submitted.
                         * 
                         * There are a lot of input fields for swatches to trigger UI
                         * events that should not be sent back to the server.
                         * This represents the elements that are actually needed.
                         */
                        values: {},
                        /**
                         * The default values collected upon page load. These are retained
                         * as a rollback point for when the reset button is clicked,
                         * so it doesn't just empty the form, it actually restores it
                         * back to what was originally saved and presented with the page.
                         */
                        defaults: {}
                    },
                    /**
                     * Cleans up the form prior to submit, and scrubs swatch fields,
                     * so it doesn't overload the server with a bzillion post vars.
                     * @param {type} data
                     * @returns {undefined}
                     */
                    cleanup: function (data)
                    {

                    },
                    /**
                     * Performs the actual form submission, after isolating
                     * only the values that are needed.
                     * 
                     * @param Event event
                     * @returns false|undefined
                     */
                    submit: function (event)
                    {
                        var uri = window.location;
                        var data = {}, i;
                        event.preventDefault();
                        data[formlib.fields.nonce.attr('name')] = formlib.fields.nonce.val();
                        data[formlib.fields.referer.attr('name')] = formlib.fields.referer.val();
                        data[formlib.fields.binding.attr('name')] = formlib.fields.binding.val();
                        for (i in formlib.fields.values)
                        {
                            if (formlib.fields.values.hasOwnProperty(i))
                            {
                                data[i] = formlib.fields.values[i];
                            }
                        }
                        jQuery.post({
                            beforeSend: this.callback.before,
                            url: uri,
                            data: data

                        }).done(this.callback.done).fail(this.callback.fail);
                        return false;
                    },
                    /**
                     * Handles submission callback responses.
                     */
                    callback: {
                        /**
                         * Fires before the submit call.
                         */
                        before: function (xhr)
                        {
                            console.groupCollapsed('WOOO Lets ship off some Ajax Action');
                            console.info('(It\'s Francis actually.)');
                            console.info('Data Received');
                            console.dir(xhr);
                            console.groupEnd();
                        },
                        /**
                         * Fires on form submit success.
                         * @returns undefined
                         */
                        done: function (data)
                        {
                            console.groupCollapsed('Received Ajax Response From Server');
                            console.info('Data Received');
                            console.dir(data);
                            console.groupEnd();
                            formlib.collect();
                            formlib.reset();
                        },
                        /**
                         * Fires on form submit failure.
                         * @returns undefined
                         */
                        fail: function (data)
                        {
                            console.groupCollapsed('Received Failing Ajax Response From Server!');
                            console.info('Data Received');
                            console.dir(data);
                            console.groupEnd();
                        }
                    },
                    /**
                     * Resets the form inputs back to the values that existed
                     * when the page originally loaded.
                     * 
                     * @param Event event
                     * @returns undefined
                     */
                    reset: function (event)
                    {
                        var i;
                        for (i in formlib.fields.defaults)
                        {
                            if (formlib.fields.defaults.hasOwnProperty(i))
                            {
                                jQuery('#' + i + '-input').val(formlib.fields.defaults[i]);
                            }
                        }
                    },
                    /**
                     * Collects the initial form values when the page loads,
                     * so reset actions put them back to how they were instead
                     * of just emptying the field, which may not reflect the
                     * existing state of the color profile, and could corrupt
                     * the site color style if submitted otherwise.
                     * 
                     * @returns undefined
                     */
                    collect: function ()
                    {
                        var valid = form.find('input[type=text].form-control');
                        valid.each(function () {
                            var k = jQuery(this).attr('id').replace('-color-input', '');
                            var v = jQuery(this).val();
                            formlib.fields.defaults[k] = v;
                            formlib.fields.values[k] = v;
                        });
                    }
                };
                // Initialize data tracking
                formlib.collect();
                // Bind the reset handler
                form_reset.each(function () {
                    jQuery(this).on('click', formlib.reset);
                });
                // Bind the submit handler
                form.submit(function (event) {
                    event.preventDefault();
                    formlib.submit(event);
                    return false;
                });
                /**
                 * The following constitutes the individual behavioral aspects of
                 * the color picker components in the isolated sense.
                 * 
                 * This is all UI stuff that is localized to the individual
                 * field being worked with at any given moment.
                 */
                jQuery('.spectrum-color-picker').each(function () {
                    var spectrum_element = jQuery(this);
                    var text_input = jQuery('[name="' + jQuery(this).attr('data-context') + '"]');
                    var color_input = jQuery('[name="' + jQuery(this).attr('data-context') + '-preview"]');
                    var swatch_container = jQuery('#' + jQuery(this).attr('data-context') + '-swatches-wrapper');
                    var ui_switch = '#' + jQuery(this).attr('data-context') + '-ui-switch';
                    var swatch_original = jQuery('[data-context="' + jQuery(this).attr('data-context') + '-original"]');
                    var reset_input = jQuery('[name="' + jQuery(this).attr('data-context') + '-reset"]');
                    var options_input = jQuery('#' + jQuery(this).attr('data-context') + '-reset');
                    var popover = jQuery('[data-context="' + jQuery(this).attr('data-context') + '-info"]');
                    var lib = {
                        use_native: false,
                        /**
                         * Updates the swatch set, tracking field, spectrum value,
                         * and hidden color input field that integrates with
                         * native browser functionality.
                         * @param tinycolor color
                         * @returns Boolean|undefined
                         */
                        update: function (color)
                        {
                            formlib.fields.values[text_input.attr('id').replace('-color-input', '')] = text_input.val();
                            if (color === null)
                            {
                                return false;
                            }
                            var hex = color.toHexString();
                            this.stats.rgb(color);
                            this.inputs.update(color);
                            this.swatch.update_light(hex);
                            this.swatch.update_dark(hex);
                            this.swatch.update_saturated(hex);
                            this.swatch.update_desaturated(hex);
                        },
                        /**
                         * Triggers the native UI of the users browser to take over
                         * in place of the provided Spectrum colorpicker, which
                         * provides much more robust selection utilities based
                         * on the actual machine using the page instead of
                         * localized scripts, which are limited to only the
                         * scope of the page content and DOM (the native browser tools are not).
                         * @param Event event
                         * @returns undefined
                         */
                        exec_native: function (event)
                        {
                            var value = color_input.val();
                            var target = jQuery(jQuery(this).parent().attr('data-target'));
                            formlib.fields.values[text_input.attr('id').replace('-color-input', '')] = text_input.val();
                            target.val(value);
                            spectrum_element.spectrum('set', value);
                            lib.update(spectrum_element.spectrum('get'));
                        },
                        /**
                         * Handles statistical elements for the individual component container.
                         */
                        stats: {
                            format: jQuery('#' + jQuery(this).attr('data-context') + '-format'),
                            red: jQuery('#' + jQuery(this).attr('data-context') + '-red'),
                            green: jQuery('#' + jQuery(this).attr('data-context') + '-green'),
                            blue: jQuery('#' + jQuery(this).attr('data-context') + '-blue'),
                            alpha: jQuery('#' + jQuery(this).attr('data-context') + '-alpha'),
                            /**
                             * Updates the RGBA display values
                             * 
                             * @param tinycolor color
                             * @returns undefined
                             */
                            rgb: function (color)
                            {
                                var rgb = color.toPercentageRgb();
                                this.format.text(color.getFormat());
                                this.red.text(rgb.r);
                                this.green.text(rgb.g);
                                this.blue.text(rgb.b);
                                this.alpha.text((rgb.a * 100) + '%');
                            }
                        },
                        /**
                         * Updates the input fields.
                         */
                        inputs: {
                            update: function (color)
                            {
                                swatch_original.val(color.toHexString());
                                text_input.val(color.toRgbString());
                                color_input.val(color.toHexString());
                                formlib.fields.values[text_input.attr('id').replace('-color-input', '')] = text_input.val();
                            }
                        },
                        /**
                         * The following control the behavior of the swatch set.
                         * @note The UI provided by Sanctity for this purpose explicitly
                         *     avoids utilizing the typical approach to color display
                         *     based on HSV/HSL commonly used in computer graphics,
                         *     because there are a ton of noted discrepancies with
                         *     visual display that cause accessiblity issues and other
                         *     visual glitches. Instead, the behavior of Sanctity is
                         *     modeled on the traditional notions of Color Theory,
                         *     and works explicitly with tints, tones, shades, and
                         *     pigments, exactly like a traditional painter would
                         *     approach colors.
                         *     This actually required the use of two color libraries
                         *     to resolve correctly, but the end result is a much more
                         *     realistic and visually accurate representation of
                         *     how color interacts with the human eye.
                         *     Yes I'm a color theory geek, and I aced that class
                         *     in art school, despite being a programmer and not
                         *     really explicitly an artist in career practice.
                         */
                        swatch: {
                            /**
                             * Updates the color shade swatches using Chroma. Spectrum's tinycolor
                             * implementation of this functionality chokes, but Chroma is solid.
                             * 
                             * @param string color
                             * @returns undefined
                             */
                            update_dark: function (color)
                            {
                                var selector = spectrum_element.attr('data-context') + '-darker-';
                                var localized_color = color;
                                var element;
                                var i;
                                for (i = 1; i <= 10; i++)
                                {
                                    element = jQuery('#' + selector + i);
                                    localized_color = this.darken(element, localized_color);
                                }
                            },
                            /**
                             * Updates the color tint swatches using Chroma.
                             * This is actually the only one of the set that Spectrum
                             * gets right, but for consistency's sake Chroma performs
                             * the logic and Spectrum provides the control module,
                             * and they don't otherwise mix.
                             * 
                             * @param string color
                             * @returns undefined
                             */
                            update_light: function (color)
                            {
                                var selector = spectrum_element.attr('data-context') + '-lighter-';
                                var localized_color = color;
                                var element;
                                var i;
                                for (i = 1; i <= 10; i++)
                                {
                                    element = jQuery('#' + selector + i);
                                    localized_color = this.lighten(element, localized_color);
                                }
                            },
                            /**
                             * Updates the color saturation swatches using Chroma. Spectrum's tinycolor
                             * implementation of this functionality chokes, but Chroma is solid.
                             * 
                             * @param string color
                             * @returns undefined
                             */
                            update_saturated: function (color)
                            {
                                var selector = spectrum_element.attr('data-context') + '-saturated-';
                                var localized_color = color;
                                var element;
                                var i;
                                for (i = 1; i <= 10; i++)
                                {
                                    element = jQuery('#' + selector + i);
                                    localized_color = this.saturate(element, localized_color);
                                }
                            },
                            /**
                             * Updates the color tone swatches using Chroma. Spectrum's tinycolor
                             * implementation of this functionality chokes, but Chroma is solid.
                             * 
                             * @param string color
                             * @returns undefined
                             */
                            update_desaturated: function (color)
                            {
                                var selector = spectrum_element.attr('data-context') + '-desaturated-';
                                var localized_color = color;
                                var element;
                                var i;
                                for (i = 1; i <= 10; i++)
                                {
                                    element = jQuery('#' + selector + i);
                                    localized_color = this.desaturate(element, localized_color);
                                }
                            },
                            /**
                             * This creates a single shade swatch tile, and returns
                             * the updated RGB string for the next iteration.
                             * 
                             * @param jQuery element The element selector for the color input field
                             * @param string color The RGB string of the color
                             * @returns string The updated RGB color string
                             */
                            darken: function (element, color)
                            {
                                var new_color = chroma(color).darken(0.5);
                                element.val(new_color);
                                return new_color;
                            },
                            /**
                             * This creates a single tint swatch tile, and returns
                             * the updated RGB string for the next iteration.
                             * 
                             * @param jQuery element The element selector for the color input field
                             * @param string color The RGB string of the color
                             * @returns string The updated RGB color string
                             */
                            lighten: function (element, color)
                            {
                                var new_color = chroma(color).brighten(0.5);
                                element.val(new_color);
                                return new_color;
                            },
                            /**
                             * This creates a single saturation swatch tile, and returns
                             * the updated RGB string for the next iteration.
                             * 
                             * @param jQuery element The element selector for the color input field
                             * @param string color The RGB string of the color
                             * @returns string The updated RGB color string
                             */
                            saturate: function (element, color)
                            {
                                var new_color = chroma(color).saturate(0.5);
                                element.val(new_color);
                                return new_color;
                            },
                            /**
                             * This creates a single tone swatch tile, and returns
                             * the updated RGB string for the next iteration.
                             * 
                             * @param jQuery element The element selector for the color input field
                             * @param string color The RGB string of the color
                             * @returns string The updated RGB color string
                             */
                            desaturate: function (element, color)
                            {
                                var new_color = chroma(color).desaturate(0.5);
                                element.val(new_color);
                                return new_color;
                            },
                        }
                    };
                    var spectrum_options = {

                        showInitial: true,
                        showInput: true,
                        allowEmpty: true,
                        showAlpha: true,
                        showPalette: true,
                        showSelectionPalette: true,
                        hideAfterPaletteSelect: true,
                        preferredFormat: "rgb",
                        chooseText: "Yeah!",
                        cancelText: "Nah.",
                        containerClassName: 'sanctity-spectrum-panel',
                        replacerClassName: 'sanctity-spectrum-toggle m-auto',
                        palette: [
                            ['black', 'white', 'blanchedalmond'],
                            ['rgb(255, 128, 0);', 'hsv 100 70 50', 'lightyellow']
                        ],
                        selectionPalette: ["red", "green", "blue"],
                        /**
                         * Fires the full chain of updates for state change
                         * of the bound input field, all swatches, the profile
                         * statistics, etc.
                         * @param tinycolor color
                         * @returns undefined
                         * @note This is triggered separately also when in
                         *     native UI mode, but it goes through the exact
                         *     same routine.
                         */
                        change: function (color)
                        {
                            lib.update(color);
                        },
                        /**
                         * Cancels the Spectrum UI if we are in native system mode.
                         * @param tinycolor color
                         * @returns Boolean|undefined
                         */
                        beforeShow: function (color)
                        {
                            if (lib.use_native)
                            {
                                color_input.trigger('click');
                                return false;
                            }
                        }
                    };
                    /**
                     * Makes the info boxes work.
                     * This apparently can't be done with just markup,
                     * unlike most Bootstrap things.
                     */
                    popover.popover({
                        trigger: 'click hover'
                    });
                    // Binds the toggle between web and native selector UI.
                    jQuery(ui_switch).change(function (event)
                    {
                        lib.use_native = lib.use_native ? false : true;
                    });
                    // Fires the native UI binding for whatever
                    // color picker the user's system provides.
                    color_input.change(function (event)
                    {
                        lib.exec_native(event);
                    });
                    // Binds the spectrum element.
                    spectrum_element = jQuery(this).spectrum(spectrum_options);

                    // Binds swatch click events to the spectrum picker.
                    swatch_container.find('.sanctity-swatch-field').on('click', function (event)
                    {
                        var value = jQuery(this).val();
                        var target = jQuery(jQuery(this).parent().attr('data-target'));
                        target.val(value);
                        spectrum_element.spectrum('set', value);
                        lib.update(spectrum_element.spectrum('get'));
                        event.preventDefault();
                        return false;
                    });
                    // Binds reset master form control clicks to this component, and directs it
                    // to update its internal values and swatch set to whatever the reset value is.
                    form_reset.on('click', function () {
                        var value = formlib.fields.defaults[text_input.attr('id').replace('-color-input', '')];
                        formlib.fields.values[text_input.attr('id').replace('-color-input', '')] = value;
                        spectrum_element.spectrum('set', value);
                        lib.update(spectrum_element.spectrum('get'));
                        if (value === "")
                        {
                            swatch_container.find('.sanctity-swatch-field').val('#ffffff');
                        }
                    });
                    // Binds text field updates to the submit datastore set.
                    text_input.change(function () {
                        formlib.fields.values[jQuery(this).attr('id').replace('-color-input', '')] = jQuery(this).val();
                    });
                    // Binds the reset handler for clearing the input area.
                    reset_input.on('click', function (event)
                    {
                        var value = '';
                        text_input.val(value);
                        spectrum_element.spectrum('set', value);
                        lib.update(spectrum_element.spectrum('get'));
                        swatch_container.find('.sanctity-swatch-field').val('#ffffff');
                        event.preventDefault();
                        return false;
                    });
                    spectrum_element.spectrum('set', text_input.val());
                    lib.update(spectrum_element.spectrum('get'));
                });
            })();
            return this;
        }
    }
    token = _proto(this).register('component', ColorSettings);
})(Sanctity);