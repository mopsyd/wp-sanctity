/* 
 * Sanctity Frontend Javascript
 * 
 * @link https://mopsyd.com/projects/wordpress/sanctity
 * @file This javascript controls the UI for the frontend of the site
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright 2018 Brian Dayhoff
 * @package mopsyd/wp-sanctity
 * @version 0.2.0
 * @license MIT
 * @since 0.1.0
 */

/*jshint esversion: 6 */

(function (protoObj) {

    "use strict";

    let scope = "frontend";
    let _proto = protoObj || Sanctity;
    let abstract = _proto(this).abstract('page');
    let token = false;
    let lib = {
        initialize: function ()
        {
            if (this.is_initialized === true)
            {
                return false;
            }
            this.menus.initialize();
            this.ui.initialize();
            this.is_initialized = true;
            return true;
        },
        menus:
                {
                    initialize: function ()
                    {
                        this.submenus.initialize();
                    },
                    submenus: {
                        is_initialized: false,
                        initialize: function () {
                            if (this.is_initialized === true)
                            {
                                return false;
                            }
                            //Fixes the wonky dropdown menu behavior because Bootstrap hates nested dropdowns.
                            jQuery('.dropdown-menu button.dropdown-toggle').on('click', function (e) {
                                if (!jQuery(this).next().hasClass('show')) {
                                    jQuery(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
                                }
                                var subMenu = jQuery(this).next(".dropdown-menu");
                                subMenu.toggleClass('show');
                                jQuery(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                                    jQuery('.dropdown-submenu .show').removeClass("show");
                                });
                                return false;
                            });
                            this.is_initialized = true;
                            return true;
                        }
                    }
                },
        ui: {
            tooltips: null,
            popovers: null,
            initialize: function ()
            {
                this.tooltips = jQuery('[data-toggle="tooltip"]');
                this.popovers = jQuery('[data-toggle="popover"]');
                this.tooltips.tooltip();
                this.popovers.popover();
            }
        }
    };
    class SanctityFrontend extends abstract {

        static context()
        {
            return scope;
        }

        constructor(key, data, selector) {
            super(key, data);
            return this;
        }

        initializeAssets(event, instance)
        {
            lib.initialize();
        }

        compileContext(event, instance)
        {

        }
    }
    token = _proto(this).register('controller', SanctityFrontend);
})(Sanctity);
