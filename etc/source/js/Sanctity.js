/* 
 * Sanctity.js
 * 
 * Sanctity.js is developed to present a nearly identical frontend usage api
 * to the PHP api on the backend, nuances between PHP and Javascript notwithstanding.
 * To create a relatively consistent approach to development across both the
 * frontend and backend, the frontend is written in class based ES6, so that
 * its object definitions and extension logic mostly mirror the approach used
 * on the backend. Likewise, the backend api is developed with this goal in mind,
 * and presents a much more "javascripty" feel than PHP typically does, having
 * a number of underlying patterns put in place to emulate the prototypical
 * approach used by frontend code. The end result is that aside from incongruent
 * language constructs, most aspects of the full package have a reasonably
 * identical frontend and backend representation, with a very similar usage
 * and extension api. The goal of this is to make it approachable for both
 * frontend developers to dive into backend code and for backend developers
 * to dive into the frontend without any real confusion or incoherence about
 * what anything is intended to do.
 * 
 * There are of course some discrepancies between the two that cannot be
 * effectively emulated on one side or the other, however the overall api
 * should be basically mirrored on both sides. They are both prototypical,
 * class based, and event driven, and the provided api is pretty much
 * consistent on both sides. They are both highly cohesive with each other
 * and very loosely coupled to any specific dependencies, which allows for
 * either side to be used in conjunction or as a standalone construct.
 * 
 * @link https://mopsyd.com/projects/sanctity
 * @file This javascript contains common javascript used across all individual source compile instances for javascript libraries leveraging Sanctity.
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright 2018 Brian Dayhoff
 * @package mopsyd/wp-sanctity
 * @version 0.2.0
 * @license MIT
 * @since 0.1.0
 */

/*jshint esversion: 6 */

(function (baseObj, domObj) {

    "use strict";
    /**
     * The primary package name. This value should never change.
     * @type String
     */
    const PACKAGE = 'Sanctity';

    /**
     * The current release version for the frontend library.
     * 
     * This value should only change when the frontend library is updated.
     * It should generally reflect the backend release version, but it is a
     * distinct possibility that one or the other will not have the
     * same release version at some point, so they are tracked separately.
     * @type String
     */
    const VERSION = '0.2.0';

    /**
     * The window object. This represents the setting point for the final
     * Sanctity instance after the factory resolves.
     * 
     * @type window
     */
    const dest = baseObj || window;
    /**
     * The DOM object. This represents the document, XML, or other related schema.
     * @type document
     */
    const dom = domObj || document;

    /**
     * Represents the primary instance.
     * @type Sanctity
     */
    let sanctity;

    /**
     * Represents the raw data provided by the backend when the page was generated.
     * 
     * @type Object
     */
    let _data = dest.sanctity_page_data || {};

    /**
     * Represents the frontend unique fingerprint.
     * This is used to identify a single clientside request.
     * If the backend provides a fingerprint it will share the same
     * fingerprint as the backend, which represents a single collective effort
     * to serve a page, and allows both sides to easily identify the root request
     * and what is appropriate to the scope of that request.
     * If not, it will just generate its own.
     * @type String
     */
    const _key = _data.fingerprint || (function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 64; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    })();

    /**
     * The DOM element for the page metadata injected into page source directly
     * when the page is served. This is removed from the DOM immediately after
     * the bootstrap process occurs, and its data remains encapsulated in a
     * read-only constant within this object so it is not externally mutable,
     * and can be distributed consistently as needed without being externally
     * polluted.
     * @type Element
     */
    let _elem = document.querySelector('#sanctity-page-data');

    /**
     * Set to true after the primary bootstrap has occurred to prevent it
     * from occurring again in any given page load. This prevents the internal
     * library init method from firing again, which cascades through several
     * other individual init methods for more granular modules.
     * 
     * @type Boolean
     */
    let is_bootstrapped = false;

    /**
     * This value is determined by the server,
     * and corresponds to its own debug settings.
     * This automatically keeps the frontend library's
     * debug settings synchronous with the backend to
     * provide a more unified development environment.
     * This also automatically supresses debug messages
     * from Sanctity.js in production.
     * @type Boolean
     */
    let sanctity_debug = _data.debug || false;

    /**
     * Provides the foundational abstraction for streamlined frontend
     * representation of the Sanctity web stack.
     * 
     * This is the root class, and is not distributed externally to this
     * encapsulated instance. It uses an identically named facade in the
     * outer scope to access it, and is referenced internally for calls
     * to accomplish logic. This preserves visibility and keeps the data
     * passed to the frontend secure from rogue scripts jacking it.
     * 
     * @type SanctityClass
     */
    class SanctityClass
    {

        static context()
        {
            return false;
        }

        static classType()
        {
            return false;
        }

        constructor(args)
        {
            let params = args || {}, token, prop, i;
            let real = Object.getOwnPropertyNames(params);
            let context = this.constructor.context();
            let class_type = this.constructor.classType();
            let self = this;
            let binding = function (event) {
                if (dom.readyState == "interactive" || dom.readyState == "complete")
                {
                    // The dom is immediately available.
                    self.initialize(event, self);
                    if (dom.readyState == "complete")
                    {
                        // Full operation can be called immediately.
                        self.onLoad(event, self);
                    }
                }
            };
            this.package = PACKAGE;
            this.version = VERSION;
            this.initialization_tasks = [];
            this.load_tasks = [];
            this.context = context || this.constructor.context();
            this.initialized = false;
            this.loaded = false;
            token = lib.authorize.verify(this);
            this.data = lib.data.package(token, this);
            if (typeof args === "object")
            {
                for (prop in real)
                {
                    // Set arguments as local parameters.
                    // Do not override existing class properties,
                    // and do not include the prototype.
                    if (real.hasOwnProperty(prop) && typeof params[real[prop]] !== "undefined" && !(real[prop] in this))
                    {
                        this[real[prop]] = params[real[prop]];
                    }
                }
            }
            if (!(this instanceof AbstractFactory))
            {
                this.factory = new FactoryFactory(this.constructor.classType());
            }
            lib.bind('ready', binding);
            lib.bind('load', binding);
            binding();
        }

        /**
         * Sets the subject id of the instance.
         * 
         * This must be a string, and must be unique.
         * This is called in the constructor of all subclasses of SanctityClass,
         * which registers the unique instance into the internal registry
         * of objects, so it can be easily indexed to track page bindings.
         * 
         * Internals use this to prevent duplicate factorization of
         * existing bindings. If a preexisting binding exists,
         * a clone of that binding will be returned instead of a new instance.
         * The prevents any extraneous stray objects from duplicate
         * scope declaration, and helps assist performance overhead.
         * 
         * The backend uses a similar mechanism to prevent duplicate calls
         * for data bindings also within the server to data layer scope.
         * The internal mechanics differ slightly, but the outward api
         * is nearly identical.
         * 
         * @param string id
         * @returns undefined
         */
        setSubject(id)
        {
            if (typeof id !== "string")
            {
                throw new TypeError('Invalid subject ID. Expected [string], but received [' + lib.library.toType(id) + ']');
            }
            this.subject = id;
        }

        /**
         * Loads a Sanctity class through the Factory Api.
         * 
         * Unless the class is actually a factory,
         * in which case it provides the factory api.
         * @param string type The class type. Should correspond to the value of
         *     the `classType()` method of one of the eight base class types.
         * @param string subject The subject class. Should correspond to the value
         *     of the `context()` method of the desired class within the defined class type.
         * @param string key (optional) The key to provide to the requested class,
         *     if it requires one. Most do, some do not. See the api for the individual
         *     class being requested for more information.
         * @param object data (optional) The data to provide to the requested class, if any.
         *     Most classes do not require this, but can operate on it if it is provided.
         * @param Element selector (optional) The selector to provide to the requested class,
         *     if any. Most classes will use `self` as their selector if not provided one.
         *     Some classes do not require one.
         * @returns SanctityClass All returned class objects are instances of SanctityClass.
         */
        load(type, subject, key, data, selector)
        {
            return this.factory.load(type).load(subject, key, data, selector);
        }

        /**
         * Fires when the subject provided in the authorize method has resolved,
         * or immediately if it has already loaded.
         * 
         * This will automatically kick off any initialization for the
         * child class by simply overriding this method.
         * 
         * @param Event event
         * @param SanctityClass instance
         * @returns undefined
         */
        initialize(event, instance)
        {
            if (instance.initialized === true || this.initialized === true)
            {
                // do not duplicate initialization.
                return;
            }
            instance.executeInitializationTasks(event, instance);
            instance.initialized = true;
            this.initialized = true;
        }

        /**
         * Fires when the dom and all resources have completed their load behavior,
         * indicating all page assets are now available.
         * 
         * @param Event event
         * @param SanctityClass instance
         */
        onLoad(event, instance)
        {
            if (instance.loaded === true || this.loaded === true)
            {
                // do not duplicate load operations.
                return;
            }
            instance.executeLoadTasks(event, instance);
            instance.loaded = true;
            this.loaded = true;
        }

        /**
         * Initialization method. This is called automatically from `initialize`.
         * The distinction is that `initialize` is bound directly as an event binding,
         * whereas this method is not.
         * 
         * The behavior is separated to allow initialization to be
         * overridden without breaking internals.
         * 
         * The default behavior of this method is to loop through all of the
         * tasks registered with `registerInitializationTask` and execute each
         * of them in the order they were registered.
         * 
         * @param Event event
         * @param SanctityClass instance
         * @returns undefined
         */
        executeInitializationTasks(event, instance)
        {
            for (let task of this.initialization_tasks)
            {
                task(event, instance);
            }
        }

        /**
         * Page load method. This is called automatically from `onLoad`.
         * The distinction is that `onLoad` is bound directly as an event binding,
         * whereas this method is not.
         * 
         * The behavior is separated to allow initialization to be
         * overridden without breaking internals.
         * 
         * The default behavior of this method is to loop through all of the
         * tasks registered with `registerLoadTask` and execute each
         * of them in the order they were registered.
         * 
         * @param Event event
         * @param SanctityClass instance
         * @returns undefined
         */
        executeLoadTasks(event, instance)
        {
            for (let task of this.load_tasks)
            {
                task(event, instance);
            }
        }

        /**
         * Registers a task to run on document ready state.
         * 
         * These will be executed in the same order they are registered.
         * Each task is passed the event as its first parameter, and the
         * active instance of this class as its second parameter.
         * 
         * @param function callback
         * @returns undefined
         */
        registerInitializationTask(callback)
        {
            if (typeof callback !== 'function')
            {
                throw new TypeError('Invalid initialization task supplied. Expected [function] but received [' + lib.library.toType(callback) + ']');
            }
            this.initialization_tasks.push(callback);
        }

        /**
         * Registers a task to run on window load state.
         * 
         * These will be executed in the same order they are registered.
         * Each task is passed the event as its first parameter, and the
         * active instance of this class as its second parameter.
         * 
         * @param function callback
         * @returns undefined
         */
        registerLoadTask(callback)
        {
            if (typeof callback !== 'function')
            {
                throw new TypeError('Invalid load task supplied. Expected [function] but received [' + lib.library.toType(callback) + ']');
            }
            this.load_tasks.push(callback);
        }
    }

    // -------------------------------------------------------------------------
    // Abstraction
    // -------------------------------------------------------------------------

    /**
     * <Abstract Controller>
     * Provides an abstract basis for controllers.
     * 
     * All frontend classes in Sanctity.js or its extensions that make
     * decisions about the page at large are controllers. Controllers differ
     * from components in that they affect the interaction of the entire page
     * rather than only a small subset of it. Components may request that the
     * controller update the page state as need arises (such as submitting a form),
     * but do not update the page state themselves.
     * 
     * A page will always have one FrontController, which is not externally exposed,
     * and one or more PageControllers, which represent specific considerations
     * about how the page itself is managed. The FrontController is tasked with
     * loading all of the appropriate PageControllers and packaging them with
     * their dependencies, but otherwise does not alter the page itself unless
     * a significant error in a page controller occurs.
     * 
     * The outward facing Sanctity api generally internally defers to the
     * FrontController for authorization and action, but does not expose the
     * object externally. Instead it acts as a facade for interaction with it,
     * so sensitive internal data is not globalized.
     * 
     * In cases where an internal object needs to be passed externally without
     * prior authorization, it will be wrapped in a Proxy that blocks direct
     * access to its internals that are not intended to be publicly exposed.
     * 
     * @type AbstractController
     */
    class AbstractController extends SanctityClass
    {

        static context()
        {
            return 'abstract-controller';
        }

        static classType()
        {
            return 'controller';
        }

        constructor(key, data, selector) {
            super(data);
            this.setSubject(key || this.constructor.context());
            this.selector = selector || dom;
            this.registerInitializationTask(this.initializeAssets);
            this.registerInitializationTask(this.compileContext);
            return this;
        }

        initializeAssets(event, instance)
        {

        }

        compileContext(event, instance)
        {

        }
    }

    /**
     * <Abstract Model>
     * Provides an abstract basis for models.
     * 
     * Models handle data exchange, getting, setting, formatting, saving and
     * retrieving from various sources. They may interact with libraries that
     * act as connectors to various data endpoints, but are tasked with
     * providing a single object to interact with that represents a specific
     * data subset, and encapsulates all changes and formatting required to
     * present that data or update it as expected by the rest of the
     * frontend logic.
     * 
     * Models may typically interact with local or session storage, rest objects,
     * remote endpoints, or external storage protocol such as redis, memcached,
     * lambda, etc, depending on how they are configured.
     * 
     * @type AbstractModel
     */
    class AbstractModel extends SanctityClass
    {

        static context()
        {
            return 'abstract-model';
        }

        static classType()
        {
            return 'model';
        }

        constructor(key, data, selector) {
            super(data);
            this.setSubject(key);
            this.selector = selector || dom;
            this.readystate = this.selector.readyState;
            return this;
        }
    }

    /**
     * <Abstract View>
     * Provides an abstract basis for views.
     * 
     * Views in the context of Sanctity.js are used to effect changes
     * in the visual display of the page.
     * 
     * Whereas a backend view represents all of the content sent to the client,
     * a frontend view represents an encapsulated portion of content,
     * and acts as a means of directing its display.
     * 
     * These differ from components, in that the view only presents visual state,
     * and does not have opinion about the underlying logic that affects
     * alterations to that state. A component will typically leverage one
     * or more views to update its representation, but the component itself
     * contains the logic that decides when to trigger such changes.
     * 
     * @type AbstractView
     */
    class AbstractView extends SanctityClass
    {

        static context()
        {
            return 'abstract-model';
        }

        static classType()
        {
            return 'view';
        }

        constructor(key, data, selector) {
            super(data);
            this.setSubject(key);
            this.selector = selector || dom;
            this.readystate = this.selector.readyState;
            return this;
        }
    }

    /**
     * <Abstract Library>
     * Provides an abstract basis for libraries.
     * 
     * Libraries represent classes that perform work without opinion or state
     * within a given scope. They may also act as proxies for non-class constructs
     * that do the same, as the vast majority of javascript libraries are not
     * written in ES6 classes.
     * 
     * This pretty much corresponds to the backend Sanctity definition of a library,
     * but is optimized for frontend interaction.
     * 
     * Backend libraries are not automatically represented on the frontend,
     * and vice versa.
     * 
     * @type AbstractLibrary
     */
    class AbstractLibrary extends SanctityClass
    {

        static context()
        {
            return 'abstract-library';
        }

        static classType()
        {
            return 'library';
        }

        constructor(key, data, selector) {
            super(data);
            this.setSubject(key);
            this.selector = selector || dom;
            this.readystate = this.selector.readyState;
            return this;
        }
    }

    /**
     * <Abstract Extension>
     * Provides an abstract basis for extensions.
     * 
     * Extensions are backend constructs that provide additional
     * server-side functionality. Frontend extensions represent this
     * functionality as a set of native frontend classes that are
     * automatically associated with the tasks the backend designates
     * that it has been extended to accomplish. These typically do not
     * need to be overridden, as the correct backend logic is generally
     * provided when the frontend library is instantiated.
     * 
     * If you are using Sanctity.js as a standalone construct,
     * you may need to define your own extension definitions though.
     * 
     * @type AbstractExtension
     */
    class AbstractExtension extends SanctityClass
    {

        static context()
        {
            return 'abstract-extension';
        }

        static classType()
        {
            return 'extension';
        }

        constructor(key, data, selector) {
            super(data);
            this.setSubject(key);
            this.selector = selector || dom;
            this.readystate = this.selector.readyState;
            return this;
        }
    }

    /**
     * <Abstract Module>
     * Provides an abstract basis for modules.
     * 
     * Modules represent opinion provided to interact with a particular platform.
     * They are used as decorators for other existing classes to leverage the
     * capabilities of a specific platform that the library is interacting with.
     * These are typically registered on the backend by the platform adapter,
     * and provide instruction for the frontend as to how to interact with the
     * native platform api.
     * 
     * Scripts from 3rd party frontend libraries can also be loaded as modules,
     * and will be wrapped in a module instance for streamlined interaction.
     * This allows the Sanctity frontend to operate as expected entirely agnostic
     * of any other script coupling that would otherwise be in place.
     * 
     * @type AbstractModule
     */
    class AbstractModule extends SanctityClass
    {

        static context()
        {
            return 'abstract-module';
        }

        static classType()
        {
            return 'module';
        }

        constructor(key, data) {
            super(key, data);
            let self = this;
            this.setSubject(key);
            this.selector = dom;
            if (typeof data.src !== "undefined" && data.src)
            {
                this.src = data.src;
                this.dependencies = data.dependencies || false;
                this.async = data.async || true;
                this.type = data.type || 'application/javascript';
                this.script = this.selector.createElement('script');
                this.script.type = this.type;
                this.script.src = this.src;
                this.script.async = this.async || true;
                this.selector.head.appendChild(this.script);
                front_controller.registerModule(this);
            }
            return this;
        }

        setData(data)
        {
            this.data = data;
        }
    }

    /**
     * <Abstract Component>
     * 
     * Provides an abstract basis for representing a single page content component
     * as an Object uniformly.
     * 
     * Components are distinct sections of dom elements and their corresponding ui,
     * encapsulated as a single object tasked with handling all aspects of change
     * and interaction with that element without disrupting other components
     * that are not children of the current scope.
     * 
     * Components can be composite, meaning they "own" child components and
     * trigger changes in those components as required. when parent-scope
     * events occur.
     * 
     * This corresponds to the backend Component representation, and is
     * designed to be integrated with its corresponding backend component
     * if one exists without an real need to expose this dependency outside
     * the scope of the object. This can also be used to represent frontend
     * components that do not have a backend representation with an
     * identical api to those that do, and facilitates a uniform approach
     * to intercommunication between parent and child components as needed,
     * which can just operate as a composite cluster and maintain
     * appropriate visibility as their own independent stack.
     * 
     * Components are not required to have a backend representation,
     * however if they want to automatically leverage the backend
     * connection api, they need to have a corresponding handler class
     * on the backend to avoid the overhead of manually rolling a set
     * of rest bindings on the frontend.
     * If such a class exists, the component will automatically
     * be packaged with its corresponding details required to request
     * interaction with the backend.
     * 
     * @type AbstractComponent
     */
    class AbstractComponent extends SanctityClass
    {

        static context()
        {
            return 'abstract-component';
        }

        static classType()
        {
            return 'component';
        }

        constructor(key, data, selector) {
            super(data);
            let self = this;
            if (typeof key !== "string")
            {
                throw new TypeError('Invalid key passed in [' + this.constructor.name + ']. Expected string.');
            } else if (typeof data !== "object")
            {
                throw new TypeError('Invalid data passed in [' + this.constructor.name + ']. Expected object.');
            } else if (!(selector instanceof Element))
            {
                console.dir(selector);
                console.dir(selector instanceof Element);
                throw new TypeError('Invalid selector passed in [' + this.constructor.name + ']. Expected [Element].');
            }
            this.type = data.type;
            this.context = data.context;
            this.element = data.element;
            this.selector = selector;
            if (typeof lib.data.data.components[this.id] === "undefined")
            {
                lib.data.data.components[this.id] = {};
            }
            this.data = lib.data.data.components[this.id] = lib.proxy.register(this.id, lib.data.data.components[this.id], {
                get: function (obj, prop)
                {
                    return prop in obj ?
                            obj[prop] : false;
                },
                set: function (obj, key, value)
                {
                    obj[key] = value;
                    return true;
                }
            });
            this.intersector = new lib.factory.utilities.observer(self.intersect);
            this.intersector.bind(this.selector);
            this.mouse = new lib.events.elements.mouse();
            this.drag = new lib.events.elements.drag();
            this.animation = new lib.events.elements.animation();
            this.transition = new lib.events.elements.transition();
            this.contexts = {};
            this.setSubject(key);
            return this;
        }

        /**
         * Intersection Observer Callback Event.
         * 
         * This fires when an intersection event occurs in the current scope.
         * 
         * @param array event
         * @param IntersectionObserver observer
         * @returns undefined
         */
        intersect(event, observer)
        {
//            console.groupCollapsed('Component Intersect Event');
//            console.info('Event');
//            console.dir(event);
//            console.info('Observer');
//            console.dir(observer);
//            console.groupEnd();
        }
    }

    /**
     * <Abstract Factory>
     * Provides an abstract basis for building complex class instances.
     * 
     * This is built to pretty much mirror the behavior of
     * the backend factory as much as is possible. Roughly the only distinction
     * is that namespaces are dot separated instead of backslash separated.
     * 
     * @type AbstractFactory
     */
    class AbstractFactory extends SanctityClass
    {

        static context()
        {
            return 'abstract-factory';
        }

        static classType()
        {
            return 'factory';
        }

        static baseClass()
        {
            return SanctityClass;
        }

        static subjects()
        {
            return lib.factory.factories[this.context()];
        }

        constructor(key, data)
        {
            super(data || {});
            this.setSubject(key || this.constructor.context());
            return this;
        }

        load(type, subject, key, data, selector)
        {
            let subjects = this.constructor.subjects();
            let valid = false;
            let base, instance;
            for (let i in subjects)
            {
                if (subjects.hasOwnProperty(i) && type === i)
                {
                    valid = true;
                    break;
                }
            }
            if (!valid)
            {
                throw new ReferenceError('Invalid type [' + type + '] supplied. Valid types are [' + subjects.toString() + '].');
            }
            base = subjects[type];
            instance = new base(subject, key, data, selector);
            return instance;
        }

        /**
         * Registers a class that is in scope to be automatically loaded via factorization.
         * 
         * The provided class must extend the abstract defined
         * by the factory it is being registered against.
         * 
         * @param string type A keyword slug identifier for the class being registered
         * @param SanctityClass subject The class instance to register
         * @return string Returns an identifying token which can be used
         *     to unregister the class and remove all internal reference to it.
         *     This prevents any unauthorized scripts from messing with internals,
         *     provided you keep the token passed back out of the global scope.
         * @throws TypeError if the provided type is not a string,
         *     or the provided subject is not a class.
         * @throws ReferenceError if the provided class is out of scope for the current factory
         * @throws TypeError if the type is not a string
         */
        register(type, subject)
        {
            if (typeof type !== "string")
            {
                throw new TypeError('Provided type is not valid. Expected [string], but received [' + lib.library.toType(type) + '].');
            }

        }

        /**
         * Unregisters a class from factorization.
         * 
         * The provided token corresponds to one passed when the `register`
         * method was called, and will internally be matched against the correct asset.
         * Factories can only unregister assets in their own scope. This method
         * will return true if unregistration is successful, and false if the token
         * is out of scope or the asset is already unregistered.
         * 
         * If the token is entirely invalid, a ReferenceError will be raised.
         * 
         * @param string token A token previously provided that designates registration for a class.
         *     If the class has already been unregistered, passing its token will not
         *     generate an error. An error will be generated if the token passed was
         *     never associated with any registration within the current runtime.
         * @return boolean Returns true if the asset was unregistered, and false
         *     if it was already unregistered or the token is valid but out of scope
         *     for this class type.
         * @throws TypeError If the provided token is not a string.
         * @throws ReferenceError If an invalid token is supplied. Tokens are generated
         *     during the current runtime, and retained even if the asset bound
         *     to them is unregistered. If a token is provided that has never
         *     been associated with any class during the current runtime,
         *     then this error will occur. If any past or present class was
         *     associated with the token, no error will occur.
         */
        unregister(token)
        {
            let existing = lib.authorize.lookupToken(token);
        }

    }

    // -------------------------------------------------------------------------
    // Controllers
    // -------------------------------------------------------------------------

    /**
     * <Sanctity Front Controller>
     * Provides a basis for controlling the interoperation of various class elements.
     * 
     * This class is not externally distributed. It leverages any extensions that have
     * been externally registered to determine how the frontend operates.
     * It cannot be overridden or replaced.
     * 
     * The public facing Sanctity api generally defers directly to the active
     * instance of this class to perform its internal logic.
     * 
     * @type SanctityFrontController
     */
    class SanctityFrontController extends AbstractController
    {

        static context()
        {
            return 'front-controller';
        }

        static classType()
        {
            return 'front-controller';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            this.page = null;
            this.user = null;
            this.auth = null;
            this.rest = null;
            this.debug = null;
            this.errors = null;
            this.log = null;
            this.extension_classes = [];
            this.module_classes = [];
            this.component_classes = [];
            this.extensions = {};
            this.modules = {};
            this.components = {};
            this.registerInitializationTask(this.initializeExtensions);
            this.registerInitializationTask(this.initializeModules);
            this.registerInitializationTask(this.initializeComponents);
            return this;
        }

        setPage(page)
        {
            this.page = page;
        }

        setUser(user)
        {
            this.user = user;
        }

        setRest(rest)
        {
            this.rest = rest;
        }

        setAuth(auth)
        {
            this.auth = auth;
        }

        registerExtension(extension)
        {
            if (!(extension.prototype instanceof AbstractExtension))
            {
                throw new TypeError('Invalid extension passed. Expected instance of [' + AbstractExtension.prototype.name + ']');
            }
            this.extension_classes.push(extension);
        }

        registerModule(module)
        {
            let data = false;
            if (!(module instanceof AbstractModule))
            {
                throw new TypeError('Invalid module passed. Expected instance of [' + AbstractModule.prototype.name + ']');
            }
            if (lib.data.data.hasOwnProperty(module.subject))
            {
                data = lib.data.data[module.subject];
                module.setData(data);
            }
            lib.authorize.registerModuleData(module.subject, data);
            this.module_classes.push(module);
        }

        registerComponent(component)
        {
            if (!(component.prototype instanceof AbstractComponent))
            {
                throw new TypeError('Invalid component passed. Expected instance of [' + AbstractComponent.prototype.name + ']');
            }
            this.component_classes.push(component);
        }

        registerFactory(factory)
        {
            if (!(factory.prototype instanceof AbstractFactory))
            {
                throw new TypeError('Invalid factory passed. Expected instance of [' + AbstractFactory.prototype.name + ']');
            }
        }

        registerController(controller)
        {
            let instance;
            if (!(controller.prototype instanceof AbstractController))
            {
                throw new TypeError('Invalid controller passed. Expected instance of [' + AbstractController.prototype.name + ']');
            }
            if (lib.page.scope === controller.context())
            {
                instance = new controller(this.constructor.context());
                this.setPage(controller);
            }
        }

        registerLibrary(library)
        {
            if (!(library.prototype instanceof AbstractLibrary))
            {
                throw new TypeError('Invalid library passed. Expected instance of [' + AbstractLibrary.prototype.name + ']');
            }
        }

        registerModel(model)
        {
            if (!(model.prototype instanceof AbstractModel))
            {
                throw new TypeError('Invalid model passed. Expected instance of [' + AbstractModel.prototype.name + ']');
            }
        }

        registerView(view)
        {
            if (!(view.prototype instanceof AbstractView))
            {
                throw new TypeError('Invalid view passed. Expected instance of [' + AbstractView.prototype.name + ']');
            }
        }

        initializeComponents( event, instance )
        {
            let selector;
            let data = {};
            let obj;
            if (instance.component_classes.length > 0)
            {
                for (let component of instance.component_classes)
                {
                    if (typeof lib.authorize.modules[component.context()] !== "undefined")
                    {
                        data = lib.authorize.modules[component.context()];
                    }
                    if (typeof lib.data.data.components[component.context()] === "undefined")
                    {
                        lib.data.data.components[component.context()] = data;
                    }
                    selector = dom.getElementById(component.context());
                    try
                    {
                        obj = new component(component.context(), data, selector);
                        instance.components[component.context()] = obj;
                        console.groupCollapsed('Registering component');
                        console.info('Component Instance');
                        console.dir(obj);
                        console.info('Component Data');
                        console.dir(data);
                        console.info('Component Selector');
                        console.dir(selector);
                        console.groupEnd();
                        lib.components.register(component.context(), obj);
                        instance.components[component.context()] = obj;
                    } catch (error)
                    {
                        console.warn(error);
                    }
                }
            }
        }

        initializeModules( event, instance )
        {
            if (instance.module_classes.length > 0)
            {
                for (let module of instance.module_classes)
                {

                }                
            }
        }

        initializeExtensions( event, instance )
        {
            if (instance.extension_classes.length > 0)
            {
                for (let extension of instance.extension_classes)
                {

                }
            }
        }
    }

    /**
     * <Page Controller>
     * Provides a basis for page runtime direction on the client side.
     * 
     * This class acts as the foundation for a primary page controller,
     * which can be extended by scripts that are intended to act as the
     * authoritative director for any given page. This class provides
     * abstraction that allows the page controller to obtain and use
     * any data from the backend and communicate with it as required
     * without much further consideration in implementing logic.
     * 
     * If no other page controller is presented by extension by the time
     * the dom resolves, this is the one that will be used to control the page.
     * 
     * @type SanctityPage
     */
    class SanctityPage extends AbstractController
    {

        static context()
        {
            return 'page';
        }

        constructor(key, data, selector) {
            super(key, data, selector);
            return this;
        }
    }

    // -------------------------------------------------------------------------
    // Models
    // -------------------------------------------------------------------------

    // @todo

    // -------------------------------------------------------------------------
    // Views
    // -------------------------------------------------------------------------

    // @todo

    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------

    class SanctityModule extends AbstractModule
    {
        static context()
        {
            return 'sanctity-module';
        }

        constructor(key, data) {
            super(key, data);
            return this;
        }
    }

    // -------------------------------------------------------------------------
    // Extensions
    // -------------------------------------------------------------------------

    /**
     * <Sanctity Extension>
     * Provides a basis for extensions to be represented clientside.
     * 
     * This allows extensions to Sanctity on the backend to have a
     * frontend representation that mirrors their backend registration
     * on the frontend, and provides the same visibility and cohesion
     * to frontend components that their backend logic does on the server,
     * so that their representation is consistent across both the
     * frontend and backend.
     * 
     * Extensions that do not provide their own custom extension class
     * are represented by this class. There is generally no need to override this,
     * but the option is there for any edge cases that arise.
     * 
     * @type SanctityExtension
     */
    class SanctityExtension extends AbstractExtension
    {

        static context()
        {
            return 'extension';
        }

        constructor(key, data, selector) {
            super(key, data, selector);
            return this;
        }
    }

    // -------------------------------------------------------------------------
    // Components
    // -------------------------------------------------------------------------

    /**
     * <Page Component>
     * Generic page component. All components declared in the dom that do not
     * have a distinct component handler class use this one.
     * 
     * This class provides the generic basis for a page component to establish
     * its parent/child associations, defer to its parent, and control its children.
     * It does not by default provide any real opinion about what ought to happen
     * within any given component, but does allow directives to bubble upwards or
     * cascade downwards as required. Components that have a specialized purpose
     * should generally override this or one of the other provided component
     * classes to establish their specific goals.
     * 
     * @type SanctityComponent
     */
    class SanctityComponent extends AbstractComponent
    {

        static context()
        {
            return 'component';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Form Component>
     * Provides a basis for representing a single page form
     * as an Object uniformly.
     * 
     * These objects are used as form controllers, and handle validation,
     * clearing, submission, and any other ui events required for the form.
     * 
     * @type SanctityForm
     */
    class SanctityForm extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            this.fform = new lib.events.elements.form();
            this.focus = new lib.events.elements.focus();
            return this;
        }
    }

    /**
     * <Header Component>
     * Provides a basis for representing a the page header bar as a component.
     * 
     * This component will typically contain the primary navigation, logo,
     * site title, and some ui control buttons as its children, and organizes
     * them into a single component element.
     * 
     * @type SanctityHeader
     */
    class SanctityHeader extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Sidebar Component>
     * Provides a basis for representing a sidebar as a component.
     * 
     * This component will typically contain one or more menus, widgets, forms,
     * or text blocks, which it organizes as its children into a single
     * component instance.
     * 
     * Individual elements in a sidebar are also components. This component
     * can organize updates across all of its children, and show or hide the
     * sidebar itself as needed, if a control mechanism to do so is presented.
     * 
     * @type SanctitySidebar
     */
    class SanctitySidebar extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Footer Component>
     * Provides a basis for representing a footer as a component.
     * 
     * This component represents a footer. A page may contain one or more footers,
     * and the footer component is tasked with organizing any sub-components
     * that exist within it.
     * 
     * Footers are pretty versatile as to what content that they are intended
     * to display, so footer components may effect a broad scope of child content.
     * They also can detect when the end of the page has been reached via their
     * intersection event, and trigger any metrics or callbacks that should fire
     * when the user has reached the end of the page body.
     * 
     * @type SanctityFooter
     */
    class SanctityFooter extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Widget Component>
     * Provides a basis for representing a widget as a component.
     * 
     * A widget in the Sanctity sense is a individual, isolated portion of page
     * content that encapsulates its own logic and does not contain child
     * components. The platform itself may have its own definition of widgets
     * that may correspond to this or differ slightly, depending on the platform itself.
     * 
     * For reference, WordPress widgets have roughly a 1:1 definition to these in terms of scope.
     * Sanctity widgets can be automatically bound to backend logic and fully
     * encapsulate it internally without having to write piles of additional
     * scripting to do so. They also can be dynamically populated or repopulated
     * based on server considerations or events on the page itself.
     * 
     * @type SanctityWidget
     */
    class SanctityWidget extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Menu Component>
     * Provides a basis for representing a menu as a component.
     * 
     * Menu components represent navigation links, navbars,
     * and other such constructs. They are tasked with insuring that the
     * navigation controls are appropriate to the current viewport and
     * device type, and with updating the menu dynamically if any authorization
     * changes occur that provide new links without the overhead of reloading
     * the page (for example if the user logs in over ajax and requires new links,
     * the menu component should be capable of repainting the menu to reflect
     * this without a page reload).
     * 
     * Menu components can be overridden to provide alternate navigation logic
     * on the fly. This allows for a great deal of flexibility as to how
     * navigation is presented from one device to another, or even based
     * on simple viewport size changes without the need to reconstruct
     * the dom itself.
     * 
     * @type SanctityMenu
     */
    class SanctityMenu extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Submenu Component>
     * Provides a basis for representing a submenu as a component.
     * 
     * Submenu components are always children of menu components.
     * They handle how a specific dropdown set of links is handled.
     * As it may be required to present separate dropdowns with different
     * display options, individual submenus can be overridden to alter
     * display from one navigation submenu to another without disrupting
     * the parent menu itself.
     * 
     * Submenu components cannot be instantiated without providing a parent menu
     * that they are to defer to for open and close events. Those events may be
     * triggered by either javascript or CSS, with the latter being monitored
     * via a css animation listener, and the former being monitored via a
     * click/touch listener.
     * 
     * @type SanctityForm
     */
    class SanctitySubMenu extends SanctityMenu
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Page Title Component>
     * Provides a basis for representing a page title container as a component.
     * 
     * This component represents the H1 element, it's wrapper, and any other
     * identifying elements that designate the pages distinct heading identity.
     * This component is tasked with any ui that is required to display the
     * heading information as the page is scrolled, any related page
     * content links, categories, tags, author details, or other metadata,
     * and presenting any of these that are appropriate to display in the
     * current page scope.
     * 
     * @type SanctityForm
     */
    class SanctityPageTitle extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Page Body Component>
     * Provides a basis for representing a page content body container as a component.
     * 
     * The page body component represents the primary content of the page.
     * This component manages selection, cut/paste, and other direct interactions
     * that the user has with the primary content. For example if clicking the
     * primary content should hide the rest of the page to make it easier to read,
     * that would be this component that triggers that event, as well as unhiding
     * them when focus shifts elsewhere. It also has control over markup, styling,
     * link display and interaction, and any other interactive bindings within the
     * content body. It can monitor reading progress through the content and report
     * metrics on how much of the content was consumed, how long it took to read,
     * specific portions of content that had extended focus, etc.
     * 
     * @type SanctityForm
     */
    class SanctityPageBody extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Comment Component>
     * Provides a basis for representing a page content body container as a component.
     * 
     * This component represents a single comment on the page content.
     * It is tasked with display, revealing any required metadata, and
     * determining the organization of the coment into a thread or node
     * of a tree as required. It also should manage the ui for end user
     * responses to the comment, and generate a comment form component
     * as required. This component also may be required to defer to a
     * 3rd party commenting system such as Disquis or Facebook,
     * but still appropriately represent the rendered comments as
     * individual comment components when they render on the page.
     * 
     * @type SanctityForm
     */
    class SanctityComment extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Banner Component>
     * Provides a basis for representing a page leading image banner as a component.
     * 
     * This component represents a portion of media preceding the page, which may
     * be a wrapper for a text element, a call to action, a slideshow, a jumbotron,
     * a media wrapper, or any combination of these. It may or may not also contain
     * the site branding or page title depending on the layout settings.
     * Banner components are frequently used to display a background image or video,
     * parallax content, or other striking media, and overlay text or form elements
     * on top of it to define the immediate page display. They may also act as the
     * viewport for a web application or game which provides its control mechanisms
     * through some other ui.
     * 
     * @type SanctityForm
     */
    class SanctityBanner extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Parallax Component>
     * Provides a basis for representing a page leading image banner as a component.
     * 
     * This component manages the operation of depth and perspective of parallax overlays.
     * It insures that the correct CSS transitions and animations are applied,
     * and typically avoids manually moving images via javascript, which is
     * programmatically expensive and heavy on the end user cpu.
     * 
     * Sanctity provides a number of CSS classes that are compiled specifically
     * to manage parallax content, and this component is tasked with applying
     * and removing them as needed, to allow the CSS to performantly apply
     * actual display, but allow the flexibility of javascript control over
     * the process and event bindings also.
     * 
     * @type SanctityForm
     */
    class SanctityParallax extends SanctityComponent
    {
        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    // -------------------------------------------------------------------------
    // Libraries
    // -------------------------------------------------------------------------

    /**
     * <Debug Library>
     * Provides a basis for debugging operations that mantain a synchronous
     * state with backend production settings.
     * 
     * This insures that development and production settings are consistently
     * applied between the frontend and backend, insuring that debugging code
     * runs in a uniform way on the frontend to how it is defined
     * on the backend, so that development efforts have a uniform layer
     * of visibility between both frontend and backend implementation,
     * which inherently respects the current server state of development,
     * staging or production across the entire web stack.
     * 
     * Debug components are off by default, unless instructed by the server
     * to be enabled, or by a controller explicitly registered to enable
     * frontend debug despite the backend not designating debug mode as enabled.
     * 
     * The default behavior is to use production settings synchronous with the
     * backend definition, which does not display debug information unless the
     * webserver designates that the server is a development or unit testing
     * environment.
     * 
     * @type SanctityDebug
     */
    class SanctityDebug extends AbstractLibrary
    {

        static context()
        {
            return 'debug';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Authorization Library>
     * Provides the basis for a uniform authentication layer that
     * works seamlessly with the backend authentication layer.
     * 
     * This library is tasked with authenticating requests of all sorts,
     * including requests against the Sanctity api for specific objects,
     * requests by users for content or updates, and requests by scripts
     * for specific data to work with. It allots access tokens where access
     * is permitted that can act as keys scoped to the specific authorization
     * rights of the request. These keys persist for the same runtime and only
     * the same runtime. Scripts working with Sanctity auth are expected to
     * retain their keys once provisioned and use them for repeated requests.
     * 
     * The auth layer is intended to operate in strict conjunction with
     * the backend authorization protocol, so that end user authentication
     * has a uniform representation on both the frontend and backend,
     * and both frontend and backend developers are presented with a
     * nearly identical api for working with it. It is also used to emulate
     * class visibility that is not normally a consideration of Javascript,
     * to make it operate more like how private/protected/static aspects of
     * the backend language do.
     * 
     * @type SanctityAuth
     */
    class SanctityAuth extends AbstractLibrary
    {

        static context()
        {
            return 'auth';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <User Library>
     * Provides a basis for representing the current user, or another user as a uniform
     * Javascript Object.
     * 
     * This logic can authenticate and provide visibility
     * and access credentials in the current scope in accordance with the
     * backend definition of the user, without exposing underlying logical
     * considerations about generating representation to other unrelated logic.
     * 
     * The default instance of this will typically reflect the user currently
     * viewing the page as defined by the server, and contain just enough
     * information about them to designate how to authenticate their requests
     * over rest and provide very basic public information, but not more than that.
     * The object itself can then be used to fetch additional more private
     * details directly from the backend as appropriate to the current page
     * scope and access rights.
     * 
     * This is used in place of cookies, so that the server does not need to
     * expose the session cookie to Javascript directly, and can retain login
     * security from CSRF or script injection attacks that would normally seek
     * to steal this information and jack the users session.
     * 
     * @type SanctityUser
     */
    class SanctityUser extends AbstractLibrary
    {

        static context()
        {
            return 'user';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    /**
     * <Rest Library>
     * Provides a basis for uniform communication with the backend
     * or remote rest endpoints.
     * 
     * This represents an Ajax layer that is capable of determining
     * the available backend api available directly in accordance with
     * the backend definition of visibility and authentication,
     * and can act as a wrapper for 3rd party rest objects
     * such as Backbone.js. This allows for a uniform approach to
     * rest and ajax calls throughout the broader implementation
     * consistently.
     * 
     * @type SanctityRest
     */
    class SanctityRest extends AbstractLibrary
    {

        static context()
        {
            return 'rest';
        }

        constructor(key, data, selector)
        {
            super(key, data, selector);
            return this;
        }
    }

    // -------------------------------------------------------------------------
    // Factories
    // -------------------------------------------------------------------------

    /**
     * <FactoryFactory - Frontend Edition>
     * Factorizes factories to factorize further factorization.
     * Almost as meta as the backend edition, but not quite.
     * 
     * They work pretty much identically though, despite being written
     * in two separate languages.
     * 
     * @type FactoryFactory
     */
    class FactoryFactory extends AbstractFactory
    {

        static context()
        {
            return 'factory-factory';
        }

        static baseClass()
        {
            return AbstractFactory;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ControllerFactory extends AbstractFactory
    {

        static context()
        {
            return 'controller-factory';
        }

        static baseClass()
        {
            return AbstractController;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ModelFactory extends AbstractFactory
    {

        static context()
        {
            return 'model-factory';
        }

        static baseClass()
        {
            return AbstractModel;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ViewFactory extends AbstractFactory
    {

        static context()
        {
            return 'view-factory';
        }

        static baseClass()
        {
            return AbstractView;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class LibraryFactory extends AbstractFactory
    {

        static context()
        {
            return 'library-factory';
        }

        static baseClass()
        {
            return AbstractLibrary;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ExtensionFactory extends AbstractFactory
    {

        static context()
        {
            return 'extension-factory';
        }

        static baseClass()
        {
            return AbstractExtension;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ComponentFactory extends AbstractFactory
    {

        static context()
        {
            return 'component-factory';
        }

        static baseClass()
        {
            return AbstractComponent;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    class ModuleFactory extends AbstractFactory
    {

        static context()
        {
            return 'module-factory';
        }

        static baseClass()
        {
            return AbstractModule;
        }

        constructor(key, data)
        {
            super(key, data);
            return this;
        }
    }

    /**
     * Internal Library
     * 
     * This represents any bootstrap and/or master state properties used
     * to facilitate execution of individual objects. These functions and
     * properties can be referenced by provided external abstraction without
     * being externally exposed, and constitute factories, constructors,
     * and templates for various creational patterns used in object
     * instantiation.
     * 
     * @type Object
     */
    var lib = {
        /**
         * The active Sanctity root instance.
         * Passed into the initialize method from the root factory.
         */
        root: null,
        /**
         * The viewport, typically a window object.
         * Passed into the initialize method from the root factory.
         */
        viewport: dest,
        /**
         * The DOM tree, typically a document object.
         * Passed into the initialize method from the root factory.
         */
        dom: dom,
        /**
         * Initializes the internal library during the factorization process,
         * to set up internals for baseline object interaction.
         * 
         * @param Sanctity instance
         * @returns Boolean Returns false if already initialized,
         *     or true if initialization occurred.
         */
        initialize: function (instance, viewport, dom, data)
        {
            if (is_bootstrapped)
            {
                return false;
            }
            if (!(instance instanceof Sanctity))
            {
                throw new TypeError("Invalid root instance provided to internal registration.");
            }
            this.root = instance;
            this.viewport = viewport;
            this.dom = dom;
            this.bind('ready', this.context.initialize);
            // Data must be initialized prior to any class instantiation.
            this.data.initialize(data);
            // Authorization must be initialized prior to any class instantiation.
            this.authorize.initialize();
            // Package the root instance with it's authorization intact.
            instance = this.data.package(this.authorize.tokens.root, instance);
            // Set the baseline auth tokens for global properties
            this.authorize.elements.root = instance;
            this.authorize.elements.window = viewport;
            this.authorize.elements.dom = dom;
            // Set the baseline abstract as a reference point for registering further abstracts.
            this.authorize.elements.class = SanctityClass;
            // Initialize modules
            this.modules.initialize(data);
            // Initialize the internal utility library
            this.library.initialize(this.viewport);
            // Initialize any extensions passed from the backend
            this.extensions.initialize(data);
            // Initialize the event binding object factory
            this.events.initialize(this.viewport, this.dom);
            // Initialize the debugger, if it is enabled
            this.debug.initialize(data);
            // Initialize the baseline factorization definitions
            this.factory.initialize(this.root);
            // Initialize the metric tracking internals
            this.metrics.initialize(viewport);
            // Initialize the rest endpoint registry
            this.rest.initialize(data);
            // Initialize the current user
            this.users.initialize(data);
            // Initialize the views
            this.views.initialize(this.viewport);
            // Initialize the error tracker
            this.errors.initialize(data);
            // Initialize the components defined in the DOM
            this.components.initialize(this.dom, this.viewport);
            // Initialize the page controller internals
            this.page.initialize(data);
            is_bootstrapped = true;
            return true;
        },
        /**
         * Binds a callback to the dom resolution status.
         * 
         * @param string event "ready" or "load"
         * @param callable callback
         */
        bind: function (type, callback)
        {
            if (!(type === 'ready' || type === 'load'))
            {
                throw new TypeError('Invalid type. Expected [ready|load], but received [' + type + ']');
            }
            if (typeof callback !== 'function')
            {
                throw new TypeError('Invalid callback. Expected [function], but received [' + lib.library.toType(callback) + ']');
            }
            this.dom.addEventListener('readystatechange', function (event)
            {
                switch (lib.dom.readyState) {
                    case "interactive":
                        if (type === 'ready')
                        {
                            callback(event);
                        }
                        break;
                    case "complete":
                        if (type === 'load')
                        {
                            callback(event);
                        }
                        break;
                }
            });
        },
        /**
         * Loads additional module scripts based on component needs.
         */
        modules: {
            elements: {},
            data: {},
            is_initialized: false,
            initialize: function (data)
            {
                let details;
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (data && typeof data.modules === "object")
                    {
                        for (let key in data.modules)
                        {
                            if (data.modules.hasOwnProperty(key))
                            {
                                details = this.prepareDetails(data.modules[key]);
                                this.elements[key] = new SanctityModule(key, details);
                            }
                        }
                    }
                    this.is_initialized = true;
                }
            },
            prepareDetails: function (data)
            {
                let details = {};
                if (typeof data === "string")
                {
                    details = {
                        src: data,
                        async: true,
                        type: 'application/javascript',
                        dependencies: false
                    };
                } else
                {
                    details = {
                        src: data.src || false,
                        async: data.async || true,
                        type: data.src || 'application/javascript',
                        dependencies: data.dependencies || false
                    };
                }
                return details;
            },
            register: function (key, module)
            {

            }
        },
        /**
         * Provides polyfill support for some newer javascript features that
         * may not be implemented in all browsers. As this library pretty much
         * is based on the current ES6 standard (which is already three years old),
         * there are a handful of browsers in the wild that do not correctly
         * implement some of the features used here.
         */
        polyfill: {
            /**
             * Local Storage support for legacy browsers.
             */
            localstorage: (function () {
                /* jshint ignore:start */
                if (!window.localStorage) {
                    Object.defineProperty(window, "localStorage", new (function () {
                        var aKeys = [], oStorage = {};
                        Object.defineProperty(oStorage, "getItem", {
                            value: function (sKey) {
                                return sKey ? this[sKey] : null;
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "key", {
                            value: function (nKeyId) {
                                return aKeys[nKeyId];
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "setItem", {
                            value: function (sKey, sValue) {
                                if (!sKey) {
                                    return;
                                }
                                document.cookie = escape(sKey) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "length", {
                            get: function () {
                                return aKeys.length;
                            },
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "removeItem", {
                            value: function (sKey) {
                                if (!sKey) {
                                    return;
                                }
                                document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        Object.defineProperty(oStorage, "clear", {
                            value: function () {
                                if (!aKeys.length) {
                                    return;
                                }
                                for (var sKey in aKeys) {
                                    document.cookie = escape(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                                }
                            },
                            writable: false,
                            configurable: false,
                            enumerable: false
                        });
                        this.get = function () {
                            var iThisIndx;
                            for (var sKey in oStorage) {
                                iThisIndx = aKeys.indexOf(sKey);
                                if (iThisIndx === -1) {
                                    oStorage.setItem(sKey, oStorage[sKey]);
                                } else {
                                    aKeys.splice(iThisIndx, 1);
                                }
                                delete oStorage[sKey];
                            }
                            for (aKeys; aKeys.length > 0; aKeys.splice(0, 1)) {
                                oStorage.removeItem(aKeys[0]);
                            }
                            for (var aCouple, iKey, nIdx = 0, aCouples = document.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
                                aCouple = aCouples[nIdx].split(/\s*=\s*/);
                                if (aCouple.length > 1) {
                                    oStorage[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
                                    aKeys.push(iKey);
                                }
                            }
                            return oStorage;
                        };
                        this.configurable = false;
                        this.enumerable = true;
                    })());
                }
                /* jshint ignore:end */
            })()
        },
        /**
         * Handles common functions used for utility purposes.
         */
        library: {
            elements: {},
            is_initialized: false,
            initialize: function (object)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            },
            /**
             * Returns the type of a variable as a string.
             * 
             * @param mixed obj
             * @returns string
             */
            toType: function (obj)
            {
                return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
            },
            /**
             * Checks if a given variable is a class instance
             * 
             * @param mixed obj
             * @returns boolean
             */
            isClass: function (obj)
            {
                return typeof obj === 'function' && /^\s*class\s+/.test(obj.toString());
            },
            device: {
                pixelRatio: function () {
                    var ratio = 1;
                    if (dest.screen.systemXDPI !== undefined && dest.screen.logicalXDPI !== undefined && dest.screen.systemXDPI > dest.screen.logicalXDPI)
                    {
                        ratio = dest.screen.systemXDPI / dest.screen.logicalXDPI;
                    } else if (dest.devicePixelRatio !== undefined) {
                        ratio = dest.devicePixelRatio;
                    }
                    return ratio;
                }
            },
            validate: {
                type: function (supplied, expected)
                {
                    if (typeof supplied !== expected)
                    {
                        throw new TypeError('Invalid argument supplied. Expected [' + expected + '] but recieved [' + lib.library.toType(supplied) + '].');
                    }
                },
                instance: function (supplied, expected)
                {
                    if (!(typeof supplied === "object" && (object instanceof expected)))
                    {
                        throw new TypeError('Invalid argument supplied. Expected instance of [' + expected.name + '] but recieved [' + (typeof supplied === "object" ? supplied.prototype.name : lib.library.toType(supplied)) + '].');
                    }
                }
            },
            url: {
                host: function (uri)
                {
                    let url = new URL(uri);
                    console.dir(host);
                    return url.hostname;
                },
                path: function (uri)
                {
                    let url = new URL(uri);
                    return url.pathname;
                },
            }
        },
        /**
         * Handles authorization of objects presented through the external api
         * that request data, and determines if their request should be honored.
         */
        authorize: {
            /**
             * Defines the baseline abstracts that will be checked when new classes
             * are registered that are not already known to the internals.
             * 
             * If it doesn't extend from one of these, no data for you.
             */
            abstracts: {
                "front-controller": SanctityFrontController,
                "controller": AbstractController,
                "model": AbstractModel,
                "view": AbstractView,
                "library": AbstractLibrary,
                "module": AbstractModule,
                "component": AbstractComponent,
                "extension": AbstractExtension,
                "factory": AbstractFactory
            },
            elements: {},
            /**
             * Represents data sets for pre-authorized modules that are loaded
             * dynamically client side. These have already been validated as a
             * whitelisted external script, and are pre-authorized to receive
             * backend data securely. Data for these scripts is only served on
             * pages where the script is expected, and the script is already
             * represented as an authorized module object prior to loading,
             * so it can grab its data immediately.
             */
            modules: {},
            /**
             * Active tokens and their associated data keys.
             */
            tokens: {
                "root": null,
                "window": null,
                "dom": null,
                "modules": {}
            },
            /**
             * All tokens that have existed during the current runtime.
             * This is used to check if an expired token is passed,
             * which does not provide data but also does not result in an error.
             */
            token_cache: [],
            is_initialized: false,
            initialize: function ()
            {
                if (this.is_initialized == true)
                {
                    // Prevent duplicate root key generation.
                    return;
                }
                this.tokens.root = this.generateToken();
                this.tokens.window = this.generateToken();
                this.tokens.dom = this.generateToken();
                this.is_initialized = true;
            },
            /**
             * Verifies that a given object is valid to pass to internals.
             * 
             * If the object is recognized, this will return a token for it.
             * If not, it will return false.
             * 
             * Classes that are valid but do not have a token assignment as of
             * yet will be assigned one automatically when this method fires,
             * which corresponds to their declared context.
             * 
             * @param object|SanctityClass object
             * @returns boolean|string
             */
            verify: function (object)
            {
                let i, valid = false, data_key = null, token = null;
                if (typeof object !== "object" && !(typeof object !== "undefined" && typeof object.prototype !== "undefined" && object.prototype instanceof SanctityClass) && !(typeof object === "string" && this.tokens.modules.hasOwnProperty(object) && this.modules.hasOwnProperty(object)))
                {
                    return false;
                }
                /**
                 * The primary Sanctity instance has full access.
                 */
                if (object instanceof Sanctity)
                {
                    if (!this.elements.hasOwnProperty(object))
                    {
                        this.elements.sanctity = object;
                    }
                    data_key = "root";
                    return this.tokens.root;
                }
                /**
                 * Modules are pre-authorized expected external scripts.
                 * They are internally represented as a class object prior
                 * to loading, so the module itself can pass its key and
                 * receive its data without having to do the class extension
                 * dance.
                 * 
                 * Modules can only be authorized by the backend or by an
                 * existing registered class. They are authorized only on the
                 * pages where they are expected to exist.
                 */
                if (object instanceof AbstractModule)
                {
                    // Covers initial object registration of a module
                    valid = true;
                    data_key = object.subject;
                    if (!this.tokens.modules.hasOwnProperty(data_key))
                    {
                        token = this.generateToken();
                        this.tokens.modules[data_key] = token;
                    }
                    return this.tokens.modules[data_key];
                } else if (typeof object === "string" && this.tokens.modules.hasOwnProperty(object) && this.modules.hasOwnProperty(object))
                {
                    // Covers expected passkey authorization of a module
                    valid = true;
                    return this.tokens.modules[object];
                }
                /**
                 * Extensions of the SanctityClass have access
                 * to data within their scope, as this is the
                 * general utility for external extension.
                 */
                if (object instanceof SanctityClass)
                {
                    if (object instanceof this.abstracts[object.constructor.classType()])
                    {
                        valid = true;
                        data_key = object.constructor.context();
                        if (!this.tokens.hasOwnProperty(data_key))
                        {
                            // Generate an authorization token for the unregistered class instance.
                            token = this.generateToken();
                            this.tokens[data_key] = token;
                        }
                    }
                    return this.tokens[data_key];
                }
                /**
                 * Check if the subject is an uninstantiated class
                 * declaration that extends the abstract.
                 * 
                 * This will not be packaged until instantiated,
                 * but it will be staged as a valid class if it exists.
                 */
                if (object.prototype instanceof SanctityClass)
                {
                    valid = true;
                    data_key = object.context();
                    if (!this.tokens.hasOwnProperty(data_key))
                    {
                        // Generate an authorization token for the unregistered class instance.
                        token = this.generateToken();
                        this.tokens[data_key] = token;
                    }
                    return this.tokens[data_key];
                }
                /**
                 * Items individually registered for authorization by an instance
                 * of SanctityClass may or may not authorize for data. If they do,
                 * they will be in the container being iterated over.
                 */
                for (i in this.elements)
                {
                    if (this.elements.hasOwnProperty(i) && (object == this.elements[i] || (object instanceof this.elements[i].constructor)))
                    {
                        if (!this.tokens.hasOwnProperty(i))
                        {
                            this.tokens[i] = this.generateToken();
                        }
                        return this.tokens[i];
                    }
                }
                // Nothing else is given internal data.
                return false;
            },
            /**
             * Generates a unique token to assign
             * to an object for authentication.
             * 
             * @returns string
             */
            generateToken: function () {
                let text = "";
                const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_-+={[}].,/";
                for (let i = 0; i < 64; i++)
                {
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                this.token_cache.push(text);
                return text;
            },
            /**
             * Looks up a given token, and provides its data binding key if one exists.
             * If the provided token is expired, this will return false. If the provided
             * token is not a string or was not internally generated at any point during
             * runtime it will throw a ReferenceError, which prevents unauthorized access
             * to data. This error is not caught by internals, so unauthorized access
             * calls are immediately broken.
             * 
             * @param string token
             * @returns String|Boolean
             */
            lookupToken: function (token)
            {
                var i, valid = false;
                if (typeof token !== 'string')
                {
                    throw new TypeError('Invalid token. Expected [string], but received [' + lib.library.toType(token) + ']');
                }
                for (i of this.token_cache)
                {
                    if (token === i)
                    {
                        valid = true;
                        break;
                    }
                }
                if (!valid)
                {
                    throw new ReferenceError('Invalid token. Provided token [' + token + '] has not been used at any point during this runtime.');
                }
                for (i in this.tokens)
                {
                    if (this.tokens.hasOwnProperty(i) && this.tokens[i] == token)
                    {
                        return i;
                    } else if (this.tokens.modules.hasOwnProperty(i) && this.tokens.modules[i] == token)
                    {
                        return i;
                    }
                }
                return false;
            },
            registerElement(token, element)
            {
                let key, classes, instances;
                if (typeof token !== 'string')
                {
                    console.dir(token);
                    throw new TypeError('Invalid token. Expected [string], but received [' + lib.library.toType(token) + ']');
                }
                if (typeof element !== "object" && !(typeof element !== "undefined" && typeof element.prototype !== "undefined" && element.prototype instanceof SanctityClass))
                {
                    console.dir(element);
                    throw new TypeError('Invalid element. Expected [object|SanctityClass], but received [' + lib.library.toType(element) + ']');
                }
                key = this.lookupToken(token);
                if (typeof object === "object")
                {
                    this.elements[key] = element;
                    return;
                }
                classes = lib.factory.factories[element.classType() + '-factory'];
                if (!classes.hasOwnProperty(element.context()) || (classes.hasOwnProperty(element.context()) && classes[element.context()] !== element))
                {
                    // Place the new class or replace the existing option.
                    lib.factory.factories[element.classType() + '-factory'][element.context()] = element;
                }
                this.setNewClassInstance(element.classType(), element);
                return true;
            },
            registerModuleData(key, data)
            {
                this.modules[key] = data;
            },
            authorizeModule(key)
            {
                if (!this.modules.hasOwnProperty(key))
                {
                    return false;
                }
                return this.modules[key];
            },
            setNewClassInstance: function (type, subject)
            {
                switch (type)
                {
                    case 'controller':
                        front_controller.registerController(subject);
                        break;
                    case 'model':
                        front_controller.registerModel(subject);
                        break;
                    case 'library':
                        front_controller.registerLibrary(subject);
                        break;
                    case 'view':
                        front_controller.registerView(subject);
                        break;
                    case 'factory':
                        front_controller.registerFactory(subject);
                        break;
                    case 'extension':
                        front_controller.registerExtension(subject);
                        break;
                    case 'module':
                        front_controller.registerModule(subject);
                        break;
                    case 'component':
                        front_controller.registerComponent(subject);
                        break;
                    default:
                        throw new TypeError('Unknown class instance passed. Cannot register an unknown instance.');
                }
            }
        },
        /**
         * Represents specific dom nodes marked as interesting via "data-context"
         * tags by the backend. These represent elements that are expected to be
         * handled by frontend logic to drive ui events.
         */
        context: {
            elements: {},
            contexts: [],
            is_initialized: false,
            /**
             * 
             * @param {type} object
             */
            initialize: function ()
            {
                if (lib.context.is_initialized == true)
                {
                    // Root context collection can be expensive on large pages
                    // and should only occur one time.Methods adding additional
                    // contextual DOM elements should register them rather than
                    // trying to make them automatically discoverable.
                    return;
                }
                lib.context.collect();
                lib.context.is_initialized = true;
//                console.groupCollapsed('Context Getter Testing');
//                console.info('Testing Context Getter');
//                console.dir(lib.context.get('page-header'));
//                console.groupEnd();
            },
            /**
             * Collects the page contexts into a set of query selectors,
             * so they can be worked with by extending elements. This is
             * part of the driving logic for indexing component elements,
             * and allows for the component driven logic to quickly index
             * what is relevant or not to its scope.
             */
            collect: function ()
            {
                if (!this.is_initialized)
                {
                    // Only do this if not already initialized.
                    this.contexts = new Set(document.querySelectorAll('[data-context]'));
                }
            },
            /**
             * Gets a specific context, or group of contexts
             * that match the given contextual key from the
             * internal context registry.
             * 
             * This method can only be called after the DOM
             * has resolved correctly.
             * @param string context
             * @return Boolean|Set|querySelector Returns false if not found,
             *     a query selector if only a single entry is found, and a
             *     set if multiple entries are found.
             */
            get: function (context)
            {
                var i, entry, map = [], result;
                if (!this.is_initialized)
                {
                    throw new ReferenceError('Context resolution is not accessible until the DOM has resolved');
                }
                if (typeof context !== "string")
                {
                    throw new TypeError('Invalid context ID. Expected [string], but received [' + lib.library.toType(context) + ']');
                }
                for (let entry of this.contexts.values())
                {
                    if (context === entry.attributes['data-context'].nodeValue)
                    {
                        map.push(entry);
                    }
                }
                if (map.length == 0)
                {
                    return false;
                } else if (map.length === 1)
                {
                    return map[0];
                }
                return new Set(map);
            },
            /**
             * Registers a query selector into the
             * internal context registry.
             * 
             * @param querySelector query
             * @returns Boolean
             */
            set: function (query)
            {
                if (query in this.contexts)
                {
                    return false;
                }
                if (!(typeof query === 'object' && query instanceof querySelector))
                {
                    throw new TypeError('Invalid context query object. Expected [querySelector].');
                }
                if (typeof query.attributes['data-context'] === "undefined")
                {
                    throw new ReferenceError('Invalid context query object. The provided querySelector does not contain a data-context attribute.');
                }
                if (this.contexts.has(query))
                {
                    return false;
                }
                this.contexts.push(query);
                return true;
            }
        },
        /**
         * Storehouse for data pulled from remote. Handles storage, local caching,
         * cache busting, and update bindings, so updates receive updated data,
         * but otherwise data is not requested from endpoints more than once in
         * a given runtime if it has not changed.
         */
        data: {
            data: null,
            elements: {},
            is_initialized: false,
            initialize: function (data)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.data = data;
                    this.data.components = this.data.components || {};
                    this.data.modules = this.data.modules || {};
                    this.data.extensions = this.data.extensions || {};
                    this.is_initialized = true;
                }
            },
            package: function (token, subject)
            {
                var key;
                if (!token)
                {
                    // This occurs during the initial bootstrap.
                    // It will be manually set in this case.
                    return subject;
                }
                if (this.data === null)
                {
                    // Data has not been declared yet.
                    return false;
                }
                key = lib.authorize.lookupToken(token);
                if (!key || key == 'global' || key == 'root')
                {
                    //Nothing to do
                    return subject;
                }
                if (typeof this.data[key] === 'undefined')
                {
                    // Set the data key manually, so it can be further populated.
                    // This is a valid class object, which indicates it should be able
                    // to store and retrieve data within its scope without issue.
                    this.data[key] = {};
                }
                return lib.proxy.register(key, this.data[key]);
            }
        },
        /**
         * Handles registration of extensions from the backend, and the
         * factorization of their frontend representation through the
         * SanctityExtension class.
         */
        extensions: {
            elements: {},
            is_initialized: false,
            initialize: function (object)
            {
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles registration of page components as individual elements,
         * tracks their parent/child association, and handles factorization
         * of new instance of them through the SanctityComponent class.
         */
        components: {
            is_initialized: false,
            elements: {},
            element_details: {},
            initialize: function (dom, viewport)
            {
                let components;
                let component;
                let data = {};
                // Do not duplicate initialization.
                if (!this.is_initialized)
                {
                    components = dom.querySelectorAll('[data-component]');
                    for (let element of components)
                    {
                        data = this.prepareComponentData(element);
                        component = this.loadComponent(data, element);
                        this.elements[data.id] = component;
                        this.element_details[data.id] = data;
                    }
                    this.is_initialized = true;
                }
            },
            prepareComponentData(selector)
            {
                let data = {
                    "id": selector.attributes['data-component'].nodeValue,
                    "type": selector.attributes['data-component-type'].nodeValue,
                    "element": selector.tagName.toLowerCase(),
                    "context": (typeof selector.attributes['data-context'] !== "undefined") ? selector.attributes['data-context'].nodeValue : false
                };
                return data;
            },
            loadComponent(data, selector)
            {
                let component;
                let component_type = lib.factory.classes.component;
                switch (data.type)
                {
                    case "form":
                        component_type = lib.factory.classes.form;
                        break;
                    case "header":
                        component_type = lib.factory.classes.header;
                        break;
                    case "footer":
                        component_type = lib.factory.classes.footer;
                        break;
                    case "sidebar":
                        component_type = lib.factory.classes.sidebar;
                        break;
                    case "menu":
                        component_type = lib.factory.classes.menu;
                        break;
                    case "page-title":
                        component_type = lib.factory.classes.title;
                        break;
                    case "page-body":
                        component_type = lib.factory.classes.body;
                        break;
                    case "banner":
                        component_type = lib.factory.classes.banner;
                        break;
                    case "parallax":
                        component_type = lib.factory.classes.parallax;
                        break;
                    case "widget":
                        component_type = lib.factory.classes.widget;
                        break;
                }
                try
                {
                    component = new component_type(data.id, data, selector);
                } catch (error)
                {
                    lib.errors.errors.push(error);
                    console.error(error);
                    console.warn('Could not resolve component [' + data.id + ']!');
                    return false;
                }
                return component;
            },
            /**
             * Registers a component as a valid instance
             * of page content representation.
             * 
             * @param string handle
             * @param SanctityComponent component
             */
            register: function (handle, component)
            {
                if (typeof handle !== 'string')
                {
                    throw new TypeError('Invalid handle. Expected [string], but received [' + lib.library.toType(handle) + ']');
                } else if (!(component instanceof SanctityComponent))
                {
                    throw new TypeError('Invalid component passed. Expected [SanctityComponent].');
                }
                this.elements[handle] = component;
            },
            /**
             * Removes a reference to a page component.
             * 
             * @param string handle
             */
            unregister: function (handle)
            {
                delete this.elements[handle];
            }
        },
        /**
         * Handles event dispatching, and triggers other internals that need
         * to occur when event state changes occur.
         */
        events: {
            /**
             * Event binding keys. These are used with `on`, `this.bind`, and `this.release` methods.
             * 
             * Sancity event objects are structured in such a way that the official event handles and
             * the shorthand notation presented by these keys are interchangeable.
             * 
             * The categorys below reflect the documentation available on MDN.
             * Only standard, non-deprecated events are included.
             * Additional events can be added with a module if desired.
             * 
             * @link https://developer.mozilla.org/en-US/docs/Web/Events
             */
            keys: {
                "resource": {
                    "cached": "cached",
                    "error": "error",
                    "abort": "abort",
                    "beforeunload": "beforeunload",
                    "unload": "unload"
                },
                "network": {
                    "online": "online",
                    "offline": "offline"
                },
                "focus": {
                    "focus": "focus",
                    "blur": "blur"
                },
                "websocket": {
                    "open": "open",
                    "message": "message",
                    "error": "error",
                    "close": "close"
                },
                "session": {
                    "show": "pageshow",
                    "hide": "pagehide",
                    "pop": "popstate"
                },
                "animation": {
                    "start": "animationstart",
                    "end": "animationend",
                    "each": "animationiteration"
                },
                "transition": {
                    "start": "transitionstart",
                    "end": "transitionend",
                    "run": "transitionrun",
                    "cancel": "transitioncancel"
                },
                "form": {
                    "reset": "reset",
                    "submit": "submit"
                },
                "print": {
                    "before": "beforeprint",
                    "after": "afterprint"
                },
                "text": {
                    "start": "compositionstart",
                    "update": "compositionupdate",
                    "end": "compositionend"
                },
                "viewport": {
                    "fullscreen": "fullscreenchange",
                    "error": "fullscreenerror",
                    "resize": "resize",
                    "scroll": "scroll"
                },
                "clipboard": {
                    "cut": "cut",
                    "copy": "copy",
                    "paste": "paste"
                },
                "keyboard": {
                    "keyup": "keyup",
                    "keydown": "keydown",
                    "keypress": "keypress"
                },
                "mouse": {
                    "enter": "mouseenter",
                    "over": "mouseover",
                    "move": "mousemove",
                    "down": "mousedown",
                    "up": "mouseup",
                    "aux": "auxclick",
                    "click": "click",
                    "doubleclick": "dblclick",
                    "menu": "contextmenu",
                    "wheel": "wheel",
                    "leave": "mouseleave",
                    "out": "mouseout",
                    "select": "select",
                    "error": "pointerlockerror"
                },
                "drag": {
                    "start": "dragstart",
                    "drag": "drag",
                    "end": "dragend",
                    "enter": "dragenter",
                    "over": "dragover",
                    "leave": "dragleave",
                    "drop": "drop"
                },
                "media": {
                    "duration": "durationchange",
                    "metadata": "loadedmetadata",
                    "data": "loadeddata",
                    "canplay": "canplay",
                    "playthrough": "canplaythrough",
                    "end": "ended",
                    "empty": "emptied",
                    "stall": "stalled",
                    "suspend": "suspend",
                    "play": "play",
                    "playing": "playing",
                    "pause": "pause",
                    "wait": "waiting",
                    "seek": "seeking",
                    "seeked": "seeked",
                    "rate": "ratechange",
                    "time": "timeupdate",
                    "complete": "complete",
                    "audioprocess": "audioprocess"
                },
                "progress": {
                    "begin": "loadstart",
                    "progress": "progress",
                    "error": "error",
                    "timeout": "timeout",
                    "abort": "abort",
                    "load": "load",
                    "end": "loadend"
                }
            },
            viewport: null,
            dom: null,
            is_initialized: false,
            elements: {},
            initialize: function (viewport, dom)
            {
                let abstract;
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.viewport = viewport;
                    this.dom = dom;
                    abstract = this.abstract;
                    for (let key in this.events)
                    {
                        if (this.events.hasOwnProperty(key))
                        {
                            this.elements[key] = this.events[key](this.abstract);
                        }
                    }
                    this.is_initialized = true;
                }
            },
            /**
             * Event Handler Abstract
             * 
             * This provides the abstract basis for all other event handler objects.
             * 
             * Sanctity event handlers allow multiple bindings to be tracked
             * for a single selector, and allow the entire group to be put
             * to sleep and woken up without releasing them. This allows
             * for a great deal of flexibility in terms of memory overhead
             * management throughout runtime.
             * 
             * @return AbstractEvent
             */
            abstract: (function () {
                let cat = false;
                let bindings = {};
                let selectors = {};
                let options = {
                    sleep: false
                };
                let library = {
                    initialize: function ()
                    {
                        if (this.category === false)
                        {
                            throw new InternalError('Attempted to instantiate an unextended abstract in [' + this.constructor.name + ']. Concrete extensions must supply a category of valid event types.');
                        }
                        if (typeof this.category !== "string" || !lib.events.keys.hasOwnProperty(this.category))
                        {
                            throw new InternalError('Declared event category [' + this.category + '] is not valid.');
                        }
                        this.valid = lib.events.keys[this.category];
                        for (let key of Object.keys(this.valid))
                        {
                            this.bindings[key] = {};
                            this.selectors[key] = {};
                        }
                    },
                    /**
                     * Binds a selector event for intersection observation.
                     * 
                     * @param string handle An identifier for the callback
                     * @param Function callback A function to fire on a click event
                     * @param Element selector A dom selector to observe for click events
                     * @returns undefined
                     */
                    bind: function (handle, type, callback, selector)
                    {
                        let self = this;
                        library.verifyHandle(handle);
                        library.verifyCallback(callback);
                        library.verifySelector(selector, this.validSelector);
                        type = library.verifyType(type, this.valid);
                        this.bindings[type][handle] = function (event) {
                            if (!self.options.sleep)
                            {
                                callback(event);
                            }
                        };
                        this.selectors[type][handle] = selector;
                        this.selectors[type][handle].addEventListener(this.valid[type], this.bindings[type][handle]);
                    },
                    /**
                     * Releases a click event binding. This will retain any other bindings that are currently in place.
                     */
                    release: function (handle, type)
                    {
                        library.verifyHandle(handle);
                        type = library.verifyType(type, this.valid);
                        if (typeof this.bindings[type][handle] !== "undefined" && this.bindings[type][handle] !== null)
                        {
                            this.selectors[type][handle].removeEventListener(this.valid[type], this.bindings[type][handle]);
                            delete this.selectors[type][handle];
                            delete this.bindings[type][handle];
                        }
                    },
                    /**
                     * Temporarily disables all click bindings.
                     * 
                     * They are retained for re-enabling without the overhead of re-linking them.
                     */
                    sleep: function ()
                    {
                        this.options.sleep = true;
                    },
                    /**
                     * Re-enables existing click bindings when in a sleep state.
                     */
                    wakeup: function ()
                    {
                        this.options.sleep = false;
                    },
                    /**
                     * Remove all existing bindings so the event handler can be garbage collected.
                     * 
                     * This method should be called prior to using `delete` on a event handler,
                     * so it does not have extraneous references pointing to it by event bindings
                     * that will be exceptionally difficult to track down and remove otherwise.
                     * 
                     * Event Handlers work by encapsulating an instance of themselves
                     * in an event binding, and then deferring to the registered method
                     * if the object is not asleep. This means that destruction requires
                     * unregistering all event bindings, which is preferable behavior
                     * for the destructor anyhow.
                     */
                    destroy: function ()
                    {
                        for (let type in this.bindings)
                        {
                            if (this.bindings.hasOwnProperty(type))
                            {
                                for (let handle in this.bindings[type])
                                {
                                    if (this.bindings[type].hasOwnProperty(handle))
                                    {
                                        // Release all existing bindings so there are no 
                                        // closure encapsulated references to this object.
                                        this.release(handle, type);
                                    }
                                }
                            }
                        }
                    },
                    /**
                     * Verifies the type, and standardizes it to the expected shorthand key.
                     * 
                     * If the type is out of scope or not known, it will throw a ReferenceError instead.
                     */
                    verifyType: function (type, valid)
                    {
                        let keys = Object.keys(valid);
                        for (let key of keys)
                        {
                            if (type === key || (valid.hasOwnProperty(key) && type === valid[key]))
                            {
                                return key;
                            }
                        }
                        throw new ReferenceError('Provided type [' + type + '] is not valid.');
                    },
                    /**
                     * Verifies that a callback can be used for an event binding correctly.
                     */
                    verifyCallback: function (callback)
                    {
                        if (typeof callback !== 'function')
                        {
                            throw new TypeError('Invalid callback. Expected [function], but received [' + lib.library.toType(callback) + ']');
                        }
                    },
                    /**
                     * Verifies that the provided selector is the expected valid type.
                     * 
                     * Generally this is an instance of Element, but individual overrides
                     * can provide a different selector instance in their prototype if need be,
                     * which makes this flexible enough to account for numerous binding types.
                     */
                    verifySelector: function (selector, valid)
                    {
                        if (typeof selector !== "object" || !(selector instanceof valid))
                        {
                            throw new TypeError('Invalid selector passed. Expected [' + valid.name + '].');
                        }
                    },
                    /**
                     * Verifies that the handle for a binding is a string.
                     * These are used as object keys, so they have to be strings.
                     */
                    verifyHandle: function (handle)
                    {
                        if (typeof handle !== 'string')
                        {
                            throw new TypeError('Invalid handle. Expected [string], but received [' + lib.library.toType(handle) + ']');
                        }
                    }
                };
                function AbstractEvent()
                {
                    this.bindings = this.bindings || bindings;
                    this.selectors = this.selectors || selectors;
                    this.options = this.options || options;
                    return this;
                }
                AbstractEvent.prototype.validSelector = Element;
                AbstractEvent.prototype.category = cat;
                AbstractEvent.prototype.bind = library.bind;
                AbstractEvent.prototype.release = library.release;
                AbstractEvent.prototype.sleep = library.sleep;
                AbstractEvent.prototype.wakeup = library.wakeup;
                AbstractEvent.prototype.destroy = library.destroy;
                AbstractEvent.prototype.initialize = library.initialize;
                AbstractEvent.prototype.constructor = AbstractEvent;
                return AbstractEvent;
            })(),
            /**
             * Provides individual concrete event binding handlers.
             * These each handle a disinct subset of clientside event
             * interactions.
             */
            events: {
                /**
                 * Produces a function that handles resource event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ResourceEvent
                 */
                resource: function (abstract)
                {
                    let cat = 'resource';
                    let library = {};
                    let proto = abstract;
                    function ResourceEvent()
                    {
                        if (!(this instanceof ResourceEvent))
                        {
                            return new ResourceEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ResourceEvent.prototype = proto.prototype;
                    ResourceEvent.prototype.constructor = ResourceEvent;
                    return ResourceEvent;
                },
                /**
                 * Produces a function that handles network event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns NetworkEvent
                 */
                network: function (abstract)
                {
                    let cat = 'network';
                    let library = {};
                    let proto = abstract;
                    function NetworkEvent()
                    {
                        if (!(this instanceof NetworkEvent))
                        {
                            return new NetworkEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    NetworkEvent.prototype = proto.prototype;
                    NetworkEvent.prototype.constructor = NetworkEvent;
                    return NetworkEvent;
                },
                /**
                 * Produces a function that handles focus event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns FocusEvent
                 */
                focus: function (abstract)
                {
                    let cat = 'focus';
                    let library = {};
                    let proto = abstract;
                    function FocusEvent()
                    {
                        if (!(this instanceof FocusEvent))
                        {
                            return new FocusEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    FocusEvent.prototype = proto.prototype;
                    FocusEvent.prototype.constructor = FocusEvent;
                    return FocusEvent;
                },
                /**
                 * Produces a function that handles websocket event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns WebSocketEvent
                 */
                websocket: function (abstract)
                {
                    let cat = 'websocket';
                    let library = {};
                    let proto = abstract;
                    function WebSocketEvent()
                    {
                        if (!(this instanceof WebSocketEvent))
                        {
                            return new WebSocketEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    WebSocketEvent.prototype = proto.prototype;
                    WebSocketEvent.prototype.constructor = WebSocketEvent;
                    return WebSocketEvent;
                },
                /**
                 * Produces a function that handles session event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns SessionEvent
                 */
                session: function (abstract)
                {
                    let cat = 'session';
                    let library = {};
                    let proto = abstract;
                    function SessionEvent()
                    {
                        if (!(this instanceof SessionEvent))
                        {
                            return new SessionEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    SessionEvent.prototype = proto.prototype;
                    SessionEvent.prototype.constructor = SessionEvent;
                    return SessionEvent;
                },
                /**
                 * Produces a function that handles css animation event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns AnimationEvent
                 */
                animation: function (abstract)
                {
                    let cat = 'animation';
                    let library = {};
                    let proto = abstract;
                    function AnimationEvent()
                    {
                        if (!(this instanceof AnimationEvent))
                        {
                            return new AnimationEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    AnimationEvent.prototype = proto.prototype;
                    AnimationEvent.prototype.constructor = AnimationEvent;
                    return AnimationEvent;
                },
                /**
                 * Produces a function that handles css transition event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns TransitionEvent
                 */
                transition: function (abstract)
                {
                    let cat = 'transition';
                    let library = {};
                    let proto = abstract;
                    function TransitionEvent()
                    {
                        if (!(this instanceof TransitionEvent))
                        {
                            return new TransitionEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    TransitionEvent.prototype = proto.prototype;
                    TransitionEvent.prototype.constructor = TransitionEvent;
                    return TransitionEvent;
                },
                /**
                 * Produces a function that handles form event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns FormEvent
                 */
                form: function (abstract)
                {
                    let cat = 'form';
                    let library = {};
                    let proto = abstract;
                    function FormEvent()
                    {
                        if (!(this instanceof FormEvent))
                        {
                            return new FormEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    FormEvent.prototype = proto.prototype;
                    FormEvent.prototype.constructor = FormEvent;
                    return FormEvent;
                },
                /**
                 * Produces a function that handles print event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns PrintEvent
                 */
                print: function (abstract)
                {
                    let cat = 'print';
                    let library = {};
                    let proto = abstract;
                    function PrintEvent()
                    {
                        if (!(this instanceof PrintEvent))
                        {
                            return new PrintEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    PrintEvent.prototype = proto.prototype;
                    PrintEvent.prototype.constructor = PrintEvent;
                    return PrintEvent;
                },
                /**
                 * Produces a function that handles text composition event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns TextEvent
                 */
                text: function (abstract)
                {
                    let cat = 'text';
                    let library = {};
                    let proto = abstract;
                    function TextEvent()
                    {
                        if (!(this instanceof TextEvent))
                        {
                            return new TextEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    TextEvent.prototype = proto.prototype;
                    TextEvent.prototype.constructor = TextEvent;
                    return TextEvent;
                },
                /**
                 * Produces a function that handles viewport resize event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ViewportEvent
                 */
                view: function (abstract)
                {
                    let cat = 'viewport';
                    let library = {};
                    let proto = abstract;
                    function ViewportEvent()
                    {
                        if (!(this instanceof ViewportEvent))
                        {
                            return new ViewportEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ViewportEvent.prototype = proto.prototype;
                    ViewportEvent.prototype.constructor = ViewportEvent;
                    return ViewportEvent;
                },
                /**
                 * Produces a function that handles clipboard event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ClipboardEvent
                 */
                clipboard: function (abstract)
                {
                    let cat = 'clipboard';
                    let library = {};
                    let proto = abstract;
                    function ClipboardEvent()
                    {
                        if (!(this instanceof ClipboardEvent))
                        {
                            return new ClipboardEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ClipboardEvent.prototype = proto.prototype;
                    ClipboardEvent.prototype.constructor = ClipboardEvent;
                    return ClipboardEvent;
                },
                /**
                 * Produces a function that handles keyboard event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns KeyboardEvent
                 */
                keyboard: function (abstract)
                {
                    let cat = 'keyboard';
                    let library = {};
                    let proto = abstract;
                    function KeyboardEvent()
                    {
                        if (!(this instanceof KeyboardEvent))
                        {
                            return new KeyboardEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    KeyboardEvent.prototype = proto.prototype;
                    KeyboardEvent.prototype.constructor = KeyboardEvent;
                    return KeyboardEvent;
                },
                /**
                 * Produces a function that handles mouse event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns MouseEvent
                 */
                mouse: function (abstract)
                {
                    let cat = 'mouse';
                    let library = {};
                    let proto = abstract;
                    function MouseEvent()
                    {
                        if (!(this instanceof MouseEvent))
                        {
                            return new MouseEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    MouseEvent.prototype = proto.prototype;
                    MouseEvent.prototype.constructor = MouseEvent;
                    return MouseEvent;
                },
                /**
                 * Produces a function that handles drag and drop event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns DragEvent
                 */
                drag: function (abstract)
                {
                    let cat = 'drag';
                    let library = {};
                    let proto = abstract;
                    function DragEvent()
                    {
                        if (!(this instanceof DragEvent))
                        {
                            return new DragEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    DragEvent.prototype = proto.prototype;
                    DragEvent.prototype.constructor = DragEvent;
                    return DragEvent;
                },
                /**
                 * Produces a function that handles media event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns MediaEvent
                 */
                media: function (abstract)
                {
                    let cat = 'media';
                    let library = {};
                    let proto = abstract;
                    function MediaEvent()
                    {
                        if (!(this instanceof MediaEvent))
                        {
                            return new MediaEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    MediaEvent.prototype = proto.prototype;
                    MediaEvent.prototype.constructor = MediaEvent;
                    return MediaEvent;
                },
                /**
                 * Produces a function that handles progress and loading event bindings.
                 * 
                 * @param AbstractEvent abstract
                 * @returns ProgressEvent
                 */
                progress: function (abstract)
                {
                    let cat = 'progress';
                    let library = {};
                    let proto = abstract;
                    function ProgressEvent()
                    {
                        if (!(this instanceof ProgressEvent))
                        {
                            return new ProgressEvent();
                        }
                        proto.call(this);
                        this.category = cat;
                        this.initialize();
                        return this;
                    }
                    ProgressEvent.prototype = proto.prototype;
                    ProgressEvent.prototype.constructor = ProgressEvent;
                    return ProgressEvent;
                }
            }
        },
        /**
         * Handles the master debug protocol, as determined by the backend,
         * which is in turn determined by the underlying webserver settings
         * on the origin server.
         */
        debug: {
            elements: {},
            enabled: false,
            is_initialized: false,
            data: null,
            initialize: function (data)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (typeof data.debug !== "undefined")
                    {
                        this.enabled = data.debug;
                    }
                    if (this.enabled)
                    {
                        this.data = {};
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles generation of new objects, and proper packaging of
         * their prototypes and properties prior to being externally
         * disributed.
         */
        factory: {
            /**
             * Designates the classes expected to be provided as the abstract
             * basis for a factory to accept registration.
             * 
             * These should correspond to the result of the `abstract()` method
             * for the given factory, and are the same class instance that is
             * validated against when attempting to register a class for
             * factorization.
             */
            "factory-abstracts": {
                "factory-factory": AbstractFactory,
                "controller-factory": AbstractController,
                "model-factory": AbstractModel,
                "view-factory": AbstractView,
                "library-factory": AbstractLibrary,
                "module-factory": AbstractModule,
                "extension-factory": AbstractExtension,
                "component-factory": AbstractComponent
            },
            /**
             * Acts as a static registry of classes authorized for factory classes to load.
             * This allows for dynamic registration without the overhead of maintaining
             * ongoing active objects as singletons, and potentially mutating them
             * across use cases.
             */
            factories: {
                "factory-factory": {
                    "factory": FactoryFactory,
                    "controller": ControllerFactory,
                    "model": ModelFactory,
                    "view": ViewFactory,
                    "library": LibraryFactory,
                    "module": ModuleFactory,
                    "component": ComponentFactory,
                    "extension": ExtensionFactory,
                },
                "controller-factory": {
                    "page": SanctityPage
                },
                "model-factory": {},
                "library-factory": {
                    "auth": SanctityAuth,
                    "user": SanctityUser,
                    "debug": SanctityDebug,
                    "rest": SanctityRest
                },
                "view-factory": {},
                "component-factory": {
                    "component": SanctityComponent,
                    "form": SanctityForm,
                    "header": SanctityHeader,
                    "sidebar": SanctitySidebar,
                    "footer": SanctityFooter,
                    "widget": SanctityWidget,
                    "menu": SanctityMenu,
                    "submenu": SanctitySubMenu,
                    "page-title": SanctityPageTitle,
                    "page-body": SanctityPageBody,
                    "comment": SanctityComment,
                    "banner": SanctityBanner,
                    "parallax": SanctityParallax
                },
                "module-factory": {},
                "extension-factory": {}
            },
            classes: {
                "front-controller": SanctityFrontController,
                "factory-factory": FactoryFactory,
                "controller-factory": ControllerFactory,
                "model-factory": ModelFactory,
                "view-factory": ViewFactory,
                "library-factory": LibraryFactory,
                "module-factory": ModuleFactory,
                "component-factory": ComponentFactory,
                "extension-factory": ExtensionFactory,
                "auth": SanctityAuth,
                "page": SanctityPage,
                "component": SanctityComponent,
                "debug": SanctityDebug,
                "extension": SanctityExtension,
                "form": SanctityForm,
                "header": SanctityHeader,
                "footer": SanctityFooter,
                "sidebar": SanctitySidebar,
                "menu": SanctityMenu,
                "submenu": SanctitySubMenu,
                "title": SanctityPageTitle,
                "body": SanctityPageBody,
                "banner": SanctityBanner,
                "parallax": SanctityParallax,
                "comment": SanctityComment,
                "widget": SanctityWidget,
                "rest": SanctityRest,
                "user": SanctityUser,
            },
            element: null,
            is_initialized: false,
            initialize: function (object)
            {
                this.element = object;
            },
            load: function (type, object)
            {

            },
            /**
             * Registers a factory for automation of class generation.
             * 
             * The provided factory must be an instance of the factory abstract.
             * Internals will be populated automatically to set up the scope
             * that the factory covers.
             * 
             * @param AbstractFactory factory
             * @return string token
             */
            registerFactory(factory)
            {

            },
            /**
             * 
             * @param string classname one of the classes to return.
             * @returns SanctityClass Returns a subclass of SanctityClass directly,
             *     to be used as a basis for extension.
             */
            abstract: function (classname)
            {
                if (!(this.classes.hasOwnProperty(classname)))
                {
                    throw new TypeError('Invalid Sanctity class [' + classname + ']');
                }
                return this.classes[classname];
            },
            utilities: {
                /**
                 * Intersection Handler Factory
                 */
                observer: (function ()
                {
                    /**
                     * @type IntersectionObserver|Function This will be a null function
                     *     if the current browser does not support IntersectionObservers
                     */
                    var observer = null;
                    var lib = {
                        options: {
                            root: null,
                            rootMargin: '0px',
                            threshold: 0,
                        },
                        /**
                         * Creates an IntersectionObserver, or nulls it if there is
                         * no supported functionality in the current browser scope.
                         * 
                         * @param Function callback A callback to fire when an intersection event occurs
                         * @param string margin Margin around the root. Can have values similar to the CSS margin property, e.g. "10px 20px 30px 40px" (top, right, bottom, left)
                         * @param int threshold The threshold to fire the intersection event on
                         * @returns Boolean
                         */
                        initialize: function (callback, margin, threshold)
                        {
                            if (typeof IntersectionObserver == 'undefined')
                            {
                                // Webkit and super old IE browsers do not support
                                // this functionality currently.
                                this.observer = this.fallback;
                                return false;
                            }
                            this.options.root = this.options.root;
                            this.options.rootMargin = margin || this.options.rootMargin;
                            this.options.threshold = threshold || this.options.threshold;
                            try
                            {
                                this.observer = new IntersectionObserver(callback, this.options);
                            } catch (error)
                            {
                                lib.errors.errors.push(error);
                                console.error(error);
                                return false;
                            }
                            return true;
                        },
                        /**
                         * Binds a selector event for intersection observation.
                         * 
                         * @param Element element
                         * @param Function callback
                         * @returns undefined
                         */
                        bind: function (element, callback)
                        {
                            this.callback = callback;
                            this.observer.observe(element, this.callback);
                        },
                        /**
                         * Releases an event from intersection observation.
                         */
                        release: function (element)
                        {
                            this.observer.unobserve(element);
                        },
                        /**
                         * Disables the observer entirely
                         */
                        disable: function ()
                        {
                            this.observer.disconnect();
                        },
                        /**
                         * Fallback observer emulated for legacy support that
                         * can still run ES6 for whatever reason.
                         * 
                         * @param {type} event
                         * @param {type} options
                         * @returns undefined
                         */
                        fallback: function (event, options)
                        {
                            //no-op
                        }
                    };
                    /**
                     * Sanctity Intersection Observer Constructor
                     * 
                     * Observes intersection events, and fires bound callbacks when they occur.
                     * 
                     * @param type root Dom element used for checking the visibility of the target.
                     * @param string margin Margin around the root. Can have values similar to the CSS margin property, e.g. "10px 20px 30px 40px" (top, right, bottom, left)
                     * @param int|array threshold
                     * @returns SanctityIntersectObserver
                     */
                    var SanctityIntersectObserver = function (callback, margin, threshold)
                    {
                        this.observer = observer;
                        this.options = lib.options;
                        this.bindings = {};
                        this.initialize(callback, margin, threshold);
                        return this;
                    };
                    SanctityIntersectObserver.prototype.initialize = lib.initialize;
                    SanctityIntersectObserver.prototype.fallback = lib.fallback;
                    SanctityIntersectObserver.prototype.bind = lib.bind;
                    SanctityIntersectObserver.prototype.constructor = SanctityIntersectObserver;
                    return SanctityIntersectObserver;
                })(),
                /**
                 * Event Handler Factory
                 * @param string category
                 * @param string type
                 */
                event: function (category, type) {
                    let event_category;
                    let event_type;
                    let valid = lib.event.keys;
                    let valid_categories = Object.keys(valid);
                    let valid_types;
                    let category_valid = false;
                    let type_valid = false;
                    for (let cat of valid_categories)
                    {
                        if (category === cat)
                        {
                            category_valid = true;
                            type_valid = Object.keys(valid[category]);
                            break;
                        }
                    }
                    if (!category_valid)
                    {
                        throw new ReferenceError('Supplied event category [' + category + '] is not valid. Valid categories are [' + valid_categories.toString() + '].');
                    }
                    for (let t of type_valid)
                    {
                        if (type === t)
                        {
                            // The shorthand was passed.
                            type_valid = true;
                            break;
                        } else if (type === valid[category][t])
                        {
                            // The standard notation was passed.
                            type = valid[category][t];
                            type_valid = true;
                            break;
                        }
                    }

                }
            }
        },
        /**
         * Handles active state metrics generated by page interaction and events,
         * and performs the underlying logic to crunch it.
         */
        metrics: {
            elements: {},
            is_initialized: false,
            data: {},
            initialize: function (object)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles baseline operations for managing the page as an entity, and
         * determining which object has active access as its controller.
         */
        page: {
            element: null,
            details: null,
            scope: false,
            secure: false,
            host: null,
            url: null,
            is_initialized: false,
            page_class: SanctityPage,
            init_page: true,
            initialize: function (data)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (typeof data.page !== "undefined")
                    {
                        this.details = data.page;
                    }
                    if (typeof data.scope !== "undefined")
                    {
                        this.scope = data.scope;
                    }
                    this.secure = dest.location.protocol === 'https:' ? true : false;
                    this.host = dest.location.host;
                    this.url = dest.location.href;
                    lib.data.data.page.secure = this.secure;
                    lib.data.data.page.host = this.host;
                    lib.data.data.page.uri = this.url;
                    this.is_initialized = true;
                }
            },
            register: function (page)
            {
                if (lib.library.isClass(page) && page.prototype instanceof SanctityPage)
                {
                    // Is valid, needs instantiation
                    this.page_class = page;
                    return true;
                } else if (typeof page === object && page instanceof SanctityPage)
                {
                    // Is valid, preconstructed
                    this.page_class = page;
                    this.init_page = false;
                }
                // Other cases are not valid.
                if (sanctity_debug)
                {
                    console.groupCollapsed('Evaluating bad page instance');
                    console.info('supplied page class');
                    console.dir(page);
                    console.info('expected page class instance');
                    console.dir(SanctityPage);
                    console.info('class evaluation');
                    console.log(lib.library.isClass(page));
                    console.info('instance evaluation');
                    console.log(page instanceof SanctityPage);
                    console.groupEnd();
                }
                throw new TypeError('Invalid page class provided. Expected instance of [SanctityPage].');
            }
        },
        /**
         * Factorizes Proxy objects for objects generated, which are generally
         * returned in place of internals directly to retain abstraction
         * and visibility.
         */
        proxy: {
            elements: {},
            /**
             * Proxy Factory
             * 
             * @param type object
             * @returns undefined
             */
            initialize: function (object)
            {
                var callbacks = this.callbacks;
                var subject = object;
                return (function ()
                {
                    var SanctityProxyObject = callbacks;
                    return SanctityProxyObject;
                })();
            },
            register: function (key, object, callbacks)
            {
                if ((key in this.elements))
                {
                    // Do not double proxy objects.
                    return this.elements[key];
                }
                var methods = callbacks || this.initialize(object);
                this.elements[key] = new Proxy(object, methods);
                return this.elements[key];
            },
            callbacks: {
                construct: function (selector, args)
                {
                    this.selector = selector;
                    this.args = args;
                    return this;
                },
                getPrototypeOf: function (obj)
                {
                    return obj.prototype;
                },
                setPrototypeOf: function (obj, proto)
                {
                    obj.prototype = proto;
                    return true;
                },
                apply: function (obj, thisArg, args)
                {
                    return true;
                },
                get: function (obj, prop)
                {
                    return prop in obj ?
                            obj[prop] : false;
                },
                set: function (obj, key, value)
                {
                    obj[key] = value;
                    return true;
                },
                has: function (obj, key)
                {
                    return (key in obj) ?
                            true : false;
                },
                defineProperty: function (obj, key, value)
                {
                    Object.defineProperty(obj, key, value);
                    return obj;
                },
                getOwnPropertyDescriptor: function (obj, key)
                {
                    return Object.getOwnPropertyDescriptor(obj, key);
                },
                isExtensible: function (obj)
                {
                    return Reflect.isExtensible(obj);
                },
                preventExtensions: function (obj)
                {
                    obj.canEvolve = false;
                    Object.preventExtensions(obj);
                    return true;
                },
                ownKeys: function (obj)
                {
                    return Reflect.ownKeys(target);
                },
                deleteProperty: function (obj, key)
                {
                    if (key in obj)
                    {
                        delete obj[key];
                    }
                }
            },
        },
        /**
         * Handles Rest and ajax call operations to various endpoints,
         * and maintains knowledge of access control protocol.
         */
        rest: {
            elements: {
                collections: null,
                models: null
            },
            keys: {},
            controller: null,
            host: null,
            fingerprint: null,
            is_initialized: false,
            initialize: function (data)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (typeof data.host !== "undefined")
                    {
                        if (typeof wp !== "undefined" && typeof wp.api !== "undefined")
                        {
                            this.elements.collections = wp.api.collections;
                            this.elements.models = wp.api.models;
                        }
                        this.host = data.host;
                        this.fingerprint = _key;
                        this.controller = new SanctityRest('root', {
                            host: this.host,
                            fingerprint: this.fingerprint
                        });
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles the distribution and tracking of user objects as Object entities.
         */
        users: {
            elements: {},
            current_user: {},
            is_initialized: false,
            initialize: function (data)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    if (typeof data.user !== "undefined")
                    {
                        this.current_user = new SanctityUser('current-user', data.user);
                    }
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles bindings for visual effects and window ui changes.
         */
        views: {
            elements: {},
            is_initialized: false,
            initialize: function (object)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            },
            layout: {},
            style: {},
            branding: {},
            typography: {},
            integrations: {}
        },
        /**
         * Handles errors, logging, reporting, and error handler registration.
         */
        errors: {
            elements: {},
            errors: [],
            is_initialized: false,
            initialize: function (object)
            {
                // Do not duplicate initialization
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            }
        },
        /**
         * Handles ajax calls back to the server based on the
         * defined endpoint(s) passed from the server.
         */
        ajax: {
            endpoints: {},
            request_methods: [
                "get",
                "post",
                "put",
                "delete",
                "options",
                "trace",
                "head",
                "patch",
                "connect",
            ],
            is_initialized: false,
            initialize: function (data)
            {
                if (!this.is_initialized)
                {
                    this.is_initialized = true;
                }
            },
            parseEndpoints: function (endpoints)
            {

            },
            getRequest: function (data)
            {
                return new this.envelope('get', data);
            },
            postRequest: function (data)
            {
                return new this.envelope('post', data);
            },
            putRequest: function (data)
            {
                return new this.envelope('put', data);
            },
            deleteRequest: function (data)
            {
                return new this.envelope('delete', data);
            },
            optionsRequest: function (data)
            {
                return new this.envelope('options', data);
            },
            traceRequest: function (data)
            {
                return new this.envelope('trace', data);
            },
            headRequest: function (data)
            {
                return new this.envelope('head', data);
            },
            patchRequest: function (data)
            {
                return new this.envelope('patch', data);
            },
            connectRequest: function (data)
            {
                return new this.envelope('connect', data);
            },
            /**
             * Ajax Envelope Factory
             */
            envelope: (function ()
            {
                let library = {
                    validateType: function (type)
                    {
                        lib.library.validate.type(type, "string");
                        for (let req of lib.ajax.request_methods)
                        {
                            if (type === req)
                            {
                                return true;
                            }
                        }
                        throw new ReferenceError('Invalid Ajax request method [' + type + '] supplied. Valid types are [' + lib.ajax.request_methods.toString() + '].');
                    },
                    validateOptions: function (options)
                    {
                        if (typeof options !== "object" && !(typeof options === "object" && (options instanceof Proxy)))
                        {
                            throw new TypeError('Invalid request options supplied. Expected [object|Proxy] but received [' + lib.library.toType(options) + '].');
                        }
                    },
                    getHeaders: function (type, options, endpoint)
                    {
                        let headers = {
                            'X-Requested-With': 'XMLHttpRequest',
                            'Client-Runtime-Fingerprint': _key, // On same origin only
                            'Accept': '*',
                            'DPR': lib.library.device.pixelRatio(), // On same origin only
                            'Viewport-Width': dest.innerWidth         // On same origin only
                        };
                        switch (type)
                        {
                            case "get":
                                break;
                            case "post":
                                break;
                            case "put":
                                break;
                            case "delete":
                                break;
                            case "options":
                                break;
                            case "trace":
                                break;
                            case "head":
                                break;
                            case "patch":
                                break;
                            case "connect":
                                break;
                        }
                        return headers;
                    },
                    // Placeholder callback. Does nothing.
                    nullCallback: function () {},
                    // Placeholder XHR filter. Just returns whatever it got.
                    nullXhrFilter: function (instance, xhr) {
                        return xhr;
                    },
                    // Placeholder response content filtering method.
                    // Just returns whatever it got.
                    nullResponseFilter: function (data, instance) {
                        return data;
                    },

                };
                function AjaxEnvelope(type, options)
                {
                    let req = (typeof type === "function") ? type() : type;
                    let opt = (typeof options === "function") ? options() : (options || {});
                    let valid_opt = false;
                    let self = this;
                    library.validateType(req);
                    library.validateOptions(opt);
                    this.type = req;
                    this.options = opt;
                    this.endpoint = null;
                    this.headers = {};
                    this.data = null;
                    this.transformFilter = library.nullResponseFilter;
                    this.xhr = new XMLHttpRequest();
                    this.beforeCallback = library.nullXhrFilter;
                    this.pendingCallback = library.nullCallback;
                    this.errorCallback = library.nullCallback;
                    this.startCallback = library.nullCallback;
                    this.doneCallback = library.nullCallback;
                    this.abortCallback = library.nullCallback;
                    this.timeoutCallback = library.nullCallback;
                    this.alwaysCallback = library.nullCallback;
                    this.parseOptions();
                    return this;
                }
                AjaxEnvelope.prototype = {
                    before: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.beforeCallback = callback;
                        return this;
                    },
                    onBefore: function (instance, event, xhr)
                    {
                        return instance.beforeCallback(instance, event, xhr);
                    },
                    pending: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.pendingCallback = callback;
                        return this;
                    },
                    onPending: function (instance, event, xhr)
                    {
                        let completion = ((event.lengthComputable) ? event.loaded / event.total * 100 : NaN);
                        return instance.pendingCallback(instance, event, xhr, completion);
                    },
                    error: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.errorCallback = callback;
                        return this;
                    },
                    onError: function (instance, event, xhr)
                    {
                        return instance.errorCallback(instance, event, xhr);
                    },
                    done: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.doneCallback = callback;
                        return this;
                    },
                    onDone: function (instance, event, xhr)
                    {
                        return instance.doneCallback(instance, event, xhr);
                    },
                    start: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.startCallback = callback;
                        return this;
                    },
                    onStart: function (instance, event, xhr)
                    {
                        return instance.startCallback(instance, event, xhr);
                    },
                    timeout: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.timeoutCallback = callback;
                        return this;
                    },
                    onTimeout: function (instance, event, xhr)
                    {
                        return instance.timeoutCallback(instance, event, xhr);
                    },
                    always: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.alwaysCallback = callback;
                        return this;
                    },
                    onAlways: function (instance, event, xhr)
                    {
                        return instance.alwaysCallback(instance, event, instance.xhr);
                    },
                    abort: function (callback)
                    {
                        lib.library.validate.type(callback, "function");
                        this.abortCallback = callback;
                        return this;
                    },
                    onAbort(instance, event, xhr)
                    {
                        return instance.abortCallback(instance, event, xhr);
                    },
                    send: function ()
                    {
                        let headers = library.getHeaders(this.type, this.options, this.endpoint);
                        let self = this;
                        this.xhr.open(this.type, this.endpoint);
                        for (let header in headers)
                        {
                            if (headers.hasOwnProperty(header))
                            {
                                this.xhr.setRequestHeader(header, headers[header]);
                            }
                        }
                        this.xhr.onloadstart = function (event) {
                            return self.onStart(self, event, self.xhr);
                        };
                        this.xhr.onabort = function (event) {
                            return self.onAbort(self, event, self.xhr);
                        };
                        this.xhr.onerror = function (event) {
                            return self.onError(self, event, self.xhr);
                        };
                        this.xhr.onload = function (event) {
                            return self.onDone(self, event, self.xhr);
                        };
                        this.xhr.onloadend = function (event) {
                            return self.onAlways(self, event, self.xhr);
                        };
                        this.xhr.onprogress = function (event) {
                            return self.onPending(self, event, self.xhr);
                        };
                        this.xhr.onabort = function (event) {
                            return self.onAbort(self, event, self.xhr);
                        };
                        this.xhr.ontimeout = function (event) {
                            return self.onTimeout(self, event, self.xhr);
                        };
                        this.xhr.send();
                        return this;
                    },
                    setEndpoint: function (endpoint)
                    {
                        let uri;
                        lib.library.validate.type(endpoint, "string");
                        try {
                            // Attempt to construct full uri
                            uri = new URL(endpoint);
                        } catch (error)
                        {
                            // Attempt to construct relative uri
                            endpoint = endpoint.replace(/^\//, "");
                            uri = new URL(lib.data.data.host + '/' + endpoint);
                        }
                        console.info('Endpoint');
                        console.dir(uri);
                        this.endpoint = uri;
                        return this;
                    },
                    setHeaders: function (headers)
                    {
                        lib.library.validate.type(headers, "object");
                        this.headers = headers;
                        return this;
                    },
                    setData: function (data)
                    {
                        lib.library.validate.type(data, "object");
                        this.data = data;
                        return this;
                    },
                    filterResponseData: function (instance, data)
                    {
                        let filterType = typeof instance.transformFilter;
                        if (filterType === "array" || filterType === "object")
                        {
                            for (let filter of instance.transformFilter)
                            {
                                data = filter(data, instance);
                            }
                        } else if (typeof instance.transformFilter === "function")
                        {
                            data = instance.transformFilter(data, instance);
                        }
                        return data;
                    },
                    parseOptions: function ()
                    {
                        for (let key in this.options)
                        {
                            if (this.options.hasOwnProperty(key))
                            {
                                switch (key)
                                {
                                    case 'url':
                                        this.setEndpoint(this.options[key]);
                                        break;
                                    case 'data':
                                        this.setData(this.options[key]);
                                        break;
//                                    case 'dataType':
//                                        this.setDataType(this.options[key]);
//                                        break;
                                    case 'headers':
                                        this.setHeaders(this.options[key]);
                                        break;
                                    case 'before':
                                        this.before(this.options[key]);
                                        break;
                                    case 'pending':
                                        this.pending(this.options[key]);
                                        break;
                                    case 'error':
                                        this.error(this.options[key]);
                                        break;
                                    case 'start':
                                        this.start(this.options[key]);
                                        break;
                                    case 'done':
                                        this.done(this.options[key]);
                                        break;
                                    case 'timeout':
                                        this.timeout(this.options[key]);
                                        break;
                                    case 'always':
                                        this.always(this.options[key]);
                                        break;
                                    case 'abort':
                                        this.abort(this.options[key]);
                                        break;
//                                    case 'filter':
//                                        this.setFilter(this.options[key]);
//                                        break;
                                }
                            }
                        }
                        return this;
                    }
                };
                AjaxEnvelope.prototype.constructor = AjaxEnvelope;
                return AjaxEnvelope;
            })()
        }
    };
    var front_controller = new SanctityFrontController('root');
    /**
     * Primary Sanctity Api
     * 
     * This is the function that is distributed externally as the public api.
     * It defers and distributes other declarations from this script as needed
     * when it's api is leveraged.
     * 
     * @param string id A slug name to represent the current instance.
     * @param Object selector The object to scope the Sanctity object to.
     * @param Object proxy Proxy arguments, corresponding to the arguments
     *     for a ES6 Proxy object. These are optional. Defaults will be
     *     provided if not given, which is generally advised. However you
     *     may supply your own proxy methods also, but the factory will
     *     not fire if you grant your own.
     * @returns Sanctity
     */
    function Sanctity(id, selector, proxy)
    {
        var token;
        if (!(this instanceof Sanctity))
        {
            return new Sanctity(id, selector, proxy);
        }
        this.id = id || 'root';
        this.selector = selector || dest; // Use the root window object if no selector is passed
        this.proxy = proxy || lib.proxy.initialize(this.selector);
        token = lib.authorize.verify(this.selector);
        this.selector = lib.data.package(token, this.selector);
        return this;
    }
    Sanctity.prototype = {
        package: PACKAGE,
        version: VERSION,
        /**
         * Returns the abstract of the specified type for external extension.
         * 
         * This does not require instantiation of a Sanctity instance.
         * 
         * @param string name
         * @returns SanctityClass
         */
        abstract: function (name)
        {
            return lib.factory.abstract(name);
        },
        /**
         * Returns the authorization determination for the supplied object.
         * This is a boolean determination as to whether it can request
         * secure data through internals or not.
         * 
         * Sanctity instances can be scoped to non-authorized objects,
         * but those objects cannot request secure data. It's other unsecured
         * functionality can still be leveraged normally by them though.
         * 
         * Properly extended Sanctity classes will always authorize.
         * Non-Sanctity objects can authorize if they were registered
         * within any Sanctity class with a registration token for them,
         * which tracks their registration back to the already authorized
         * class that is extending its access rights to the external object.
         * 
         * @param SanctityClass|Object element
         */
        authorize: function (element)
        {
            if (typeof element === "object")
            {
                var auth = lib.authorize.verify(element);
                return auth;
            }
            return false;
        },
        /**
         * Initializes an object, and packages it with any data it
         * is authorized to receive. If the object is not authorized,
         * it is just returned as is. A warning will be raised in the
         * console to denote when this occurs.
         * @param Object element
         * @returns Object
         */
        initialize: function (token, element)
        {
            return element;
        },
        /**
         * Builds a proxy for the currently scoped object,
         * which has access to internals that the object does not,
         * but presents an api externally as if it were the scoped
         * object being worked with directly.
         * 
         * @returns Proxy
         */
        build: function ()
        {
            return lib.proxy.register(this.id, this.selector, this.proxy);
        },
        /**
         * Registers a custom class extension. This should be an ES6 class that obtained its abstract
         * by using `sanctity().abstract()`, which will insure that it validates and authorizes correctly
         * and is populated with the correct data values from the server for its scope.
         * 
         * @param string type Should designate the key for identificaton purposes of the registered class
         * 
         * @param SanctityClass subject The class instance. This can be either instantiated or not.
         *     If not, it will be automatically instantiated, but will not have access to internally
         *     scoped values from your own closure instance. If this is a concern, it should be
         *     instantiated prior to being registered.
         *     
         * @return boolean|string A unique identifying token for the registered asset
         *     will be returned if it is valid. If it is not valid, false will be returned.
         */
        register: function (type, subject)
        {
            let token = lib.authorize.verify(subject);
            let registration = false;
            if (token && lib.authorize.modules.hasOwnProperty(subject)) {
                return token;
            } else if (token)
            {
                registration = lib.authorize.registerElement(token, subject);
                return token;
            }
            return false;
        },
        /**
         * Unregisters a previously registered instance
         * @param string token This should be the unique token returned when
         *     registered. This measure prevents out-of-scope scripts from
         *     unregistering assets that do not belong to them.
         *     This is the only parameter you need to supply to unregister.
         *     The internals will find and remove the correct asset without
         *     further consideration.
         * @param object|SanctityClass The subject to unregister.
         *     Must be a known object or instance of Sanctity Class.
         *     It will only be unregistered if it corresponds to the
         *     given token.
         *     
         * @return boolean returns true if successfully unregistered,
         *     and false if not found or unregistration could not be
         *     completed (such as trying to unregister an active page
         *     controller without an available substitute).
         *     
         * @throws ReferenceError If a token mismatch is provided.
         *     The token must be one previously handed off.
         *     Tokens are retained even after unregistration of the
         *     assets attached to them, so the internals are capable
         *     of checking if a token was ever valid, even when it is
         *     not currently in use.
         */
        unregister: function (token, object)
        {
            return this;
        }
    };
    Sanctity.prototype.constructor = Sanctity;

    lib.authorize.initialize();
    /**
     * Primary Instance
     */
    sanctity = new Sanctity('root', dest);
    /**
     * Initialize the internal library.
     */
    lib.initialize(sanctity, dest, dom, _data);
    /**
     * Get the updated, data packaged root instance.
     */
    sanctity = lib.root;

    console.groupCollapsed('Final Sanctity Setup');
    console.info('Sanctity Root');
    console.dir(sanctity);
    console.info('Sanctity Window Instance');
    console.dir(Sanctity);
    console.info('Sanctity Front Controller');
    console.dir(front_controller);
    console.info('Sanctity Library');
    console.dir(lib);
//    try
//    {
//        console.info('Testing Ajax Library');
//        let ajax_test = new lib.ajax.envelope('get', {
//            url: '/wp-admin/admin.php?page=sanctity-integration',
//            before: function (instance, xhr)
//            {
//                console.groupCollapsed('Evaluating Ajax Before');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('XHR');
//                console.dir(xhr);
//                console.groupEnd();
//                return xhr;
//            },
//            pending: function (instance, event, xhr, completion)
//            {
//                console.groupCollapsed('Evaluating Ajax Completion');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('Event');
//                console.dir(event);
//                console.info('XHR');
//                console.dir(xhr);
//                console.info('Completion');
//                console.dir(completion);
//                console.groupEnd();
//            },
//            abort: function (instance, event, xhr)
//            {
//                console.groupCollapsed('Evaluating Ajax Abort');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('Event');
//                console.dir(event);
//                console.info('XHR');
//                console.dir(xhr);
//                console.groupEnd();
//            },
//            error: function (instance, event, xhr)
//            {
//                console.groupCollapsed('Evaluating Ajax Error');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('Event');
//                console.dir(event);
//                console.info('XHR');
//                console.dir(xhr);
//                console.groupEnd();
//            },
//            done: function (instance, event, xhr)
//            {
//                console.groupCollapsed('Evaluating Ajax Completion');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('Event');
//                console.dir(event);
//                console.info('XHR');
//                console.dir(xhr);
//                console.groupEnd();
//            },
//            always: function (instance, event, xhr)
//            {
//                console.groupCollapsed('Evaluating Ajax Always');
//                console.info('Instance');
//                console.dir(instance);
//                console.info('Event');
//                console.dir(event);
//                console.info('XHR');
//                console.dir(xhr);
//                console.groupEnd();
//            }
//        }).send();
//    } catch (error)
//    {
//        console.warn(error);
//        console.info('Lol nope.');
//    }
    console.groupEnd();

    if (typeof _elem !== "undefined" && _elem !== null)
    {
        _elem.parentNode.removeChild(_elem);
    }
    if (typeof dest.sanctity_page_data !== undefined)
    {
        delete dest.sanctity_page_data;
    }

    if (typeof dest.Sanctity === "undefined")
    {
        dest.Sanctity = Sanctity;
    }

    return sanctity;
})(window, document);
