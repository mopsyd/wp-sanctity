<?php

/*
 * This file preflights the server and wordpress installation
 * to insure that all Sanctity dependencies are in place.
 */
if ( !defined( 'ABSPATH' ) && !getenv('CI') )
{
    die();
}
/**
 * Check that Twig is available.
 *
 * @note This theme will be made usable in the absence of Twig support eventually,
 *     once the RenderAdapter is completed properly and the rest of the
 *     theme logic is fully decoupled from the render aspect, though functionality
 *     will be significantly limited on installations that cannot run Twig.
 *     This will also allow extensions to register other templating engines
 *     like Blade or Smarty as valid alternatives, which is a project goal.
 */
if (!class_exists('Twig_Environment')) {
    add_action('admin_notices', function() {
        echo '<div class="error"><p>This theme requires Twig as a dependency.</p></div>';
    });
    http_response_code(500);
    add_filter('template_include', function($template) {
        return get_stylesheet_directory() . '/static/pages/no-twig.html';
    });

    return;
}
