#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autobuild us some Sanctity Composer Dependencies!
cd $build_dir;
printf "\033[0;36mChecking Composer...\033[0m\n";
if command -v composer >/dev/null 2>&1;
then
  declare -r cleanup_remove_composer=false;
  printf "\033[0;36mComposer detected locally at "$( which composer )".\033[0m\n";
  declare -r composer_binary=$( command -v composer );
else
  printf "\033[1;33mComposer not found. Pulling from remote as a workaround.\033[0m\n";
  source $bin_dir"/build/install/_composer.sh";
  cd $build_dir;
fi
printf "\033[0;36mInstalling Composer Dependencies.\033[0m\n";
$composer_binary install --prefer-dist --no-suggest --optimize-autoloader #--no-dev;
$composer_binary dump-autoload -o

