#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autofetch us some Composer!
#!/bin/sh

declare -r cleanup_remove_composer=true; # This means we need to trash composer at the end, because it was not natively installed.
cd $bin_dir;
composer_install_expected_signature="$(curl https://composer.github.io/installer.sig)"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
composer_install_signature="$(php -r "echo hash_file('SHA384', 'composer-setup.php');")"
if [ "$composer_install_expected_signature" != "$composer_install_signature" ]
then
    >&2 printf "\033[0;31mERROR: Invalid Composer installer signature from remote repo. Build failed!\033[0m\n\n"
    rm composer-setup.php
    exit 1
fi
php composer-setup.php --quiet;
rm composer-setup.php;
declare -r composer_binary=$( echo $(pwd)"/composer.phar" );
# That should just about do it.
cd $build_dir;