#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autobuild us some Sanctity directories!
cd $build_dir;
if [ ! -d $build_dir"/dist/local" ];
then
  printf "\033[0;36mGenerating custom script directory.\033[0m\n";
  mkdir $build_dir"/dist/local"
fi
if [ ! -d $build_dir"/etc/cache" ];
then
  printf "\033[0;36mGenerating cache directory.\033[0m\n";
  mkdir $build_dir"/etc/cache"
fi
if [ ! -d $build_dir"/etc/cache/sass" ];
then
  printf "\033[0;36mGenerating SASS cache directories.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass"

fi
if [ ! -d $build_dir"/etc/cache/sass/compiled" ];
then
  printf "\033[0;36mGenerating SASS compiled source directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass/compiled"

fi
if [ ! -d $build_dir"/etc/cache/sass/style" ];
then
  printf "\033[0;36mGenerating SASS style directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass/style"

fi
if [ ! -d $build_dir"/etc/cache/sass/branding" ];
then
  printf "\033[0;36mGenerating SASS branding directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass/branding"

fi
if [ ! -d $build_dir"/etc/cache/sass/layout" ];
then
  printf "\033[0;36mGenerating SASS layout directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass/layout"

fi
if [ ! -d $build_dir"/etc/cache/sass/uploads" ];
then
  printf "\033[0;36mGenerating SASS uploads directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/sass/uploads"

fi
if [ ! -d $build_dir"/etc/cache/tpl" ];
then
  printf "\033[0;36mGenerating template cache directories.\033[0m\n";
  mkdir $build_dir"/etc/cache/tpl"
fi
if [ ! -d $build_dir"/etc/cache/tpl/twig" ];
then
  printf "\033[0;36mGenerating twig template cache directory.\033[0m\n";
  mkdir $build_dir"/etc/cache/tpl/twig"
fi