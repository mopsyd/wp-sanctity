#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets clean us up some Sanctity build clutter!
cd $build_dir;
printf "\033[0;36mCleaning up.\033[0m\n";
if $cleanup_remove_composer;
then
  printf "\033[0;36mRemoving temporary Composer binary.\033[0m\n";
  rm $composer_binary;
fi
# todo
