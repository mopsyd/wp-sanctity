#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autobuild us some Sanctity NPM dependencies!
cd $build_dir;
printf "\033[0;36mChecking Node.js...\033[0m\n";
if command -v node >/dev/null 2>&1;
then
  # Installing Node.js for the user is beyond the scope of this build tool.
  printf "\033[0;36mNode.js detected locally at "$( command -v node )".\033[0m\n";
  if command -v npm >/dev/null 2>&1;
  then
    declare -r cleanup_remove_npm=false;
    printf "\033[0;36mNPM detected locally at "$( command -v npm )".\033[0m\n";
    declare -r npm_binary=$( command -v npm );
  else
    # NPM installs with node. This should never occur, but it will if someone has a seriously broken machine.
    >&2 printf "\033[0;31mERROR: This step cannot be completed without a valid local NPM install. Build failed!\033[0m\n"
    >&2 printf "\033[0;31mERROR: Additionally, NPM installs with Node.js. If you have Node.js and not NPM, your Node.js install is likely broken and needs a reinstall.\033[0m\n"
    exit 1
  fi
else
  >&2 printf "\033[0;31mERROR: This step cannot be completed without a valid local Node.js install. Build failed!\033[0m\n\n"
  exit 1
fi
printf "\033[0;36mInstalling NPM Dependencies.\033[0m\n";
npm install --loglevel warn # --production;
#npm shrinkwrap; # Later, after finalizing the full build setup.
