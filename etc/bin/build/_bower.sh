#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autobuild us some Sanctity Bower Dependencies!
cd $build_dir;
printf "Installing Bower Dependencies.\n";
bower install # --production;
