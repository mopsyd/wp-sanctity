#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets run us some Sanctity Grunt tasks!
cd $build_dir;
printf "\033[0;36mRunning Grunt Build Tasks.\033[0m\n";
grunt build;
