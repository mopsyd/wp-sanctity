#!/usr/bin/env bash

# Copyright 2018 Brian Dayhoff <mopsyd@me.com>
# Released under the MIT license

# WOOOO Lets autobuild us some Sanctity!
declare -r bin_dir=$(pwd);
declare -r build_dir=$( cd "../../" && pwd && cd $bin_dir );

printf "\033[0;36mSanctity Build Utility.\033[0m\n";
printf "\033[0;36m--------------------------------------------------\033[0m\n";
printf "\033[0;36mCopyright Brian Dayhoff, 2018.\033[0m\n";
printf "\033[0;36mLicensed under the MIT License.\033[0m\n";
printf "\033[0;36m--------------------------------------------------\033[0m\n\n";
# Performs the Composer installation portion of the build.
# If Composer is not locally available, it will pull a copy, use that, and trash it when it's done.
# If Composer is locally available, it will just use the local copy.
source $bin_dir"/build/_composer.sh";
# Performs the NPM installation portion of the build.
# If either Node.js or NPM is not locally available, it will exit the build with an error.
source $bin_dir"/build/_npm.sh";
# If the prior two steps completed correctly, there should be no issue from this point on.

# Performs the Bower installation portion of the build.
source $bin_dir"/build/_bower.sh";
# Performs the Grunt build tasks.
source $bin_dir"/build/_grunt.sh";
# Generates temporary directories and sets the file permissions correctly.
source $bin_dir"/build/_paths.sh";
# Generates documentation.
source $bin_dir"/build/_docs.sh";
# Generates internationalization translations.
source $bin_dir"/build/_i18n.sh";
# Cleans up everything not accounted for by Grunt.
source $bin_dir"/build/_cleanup.sh";
# Packages the distributable.
source $bin_dir"/build/_package.sh";
# Completed.
printf "\033[0;36mBuild Completed!\033[0m\n\n";
exit 0;